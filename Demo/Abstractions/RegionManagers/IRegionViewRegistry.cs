﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionViewRegistry.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Collections.Generic;

namespace Demo.Regions
{
    public interface IRegionViewRegistry
    {
        event EventHandler<ViewRegisteredEventArgs> ContentRegistered;

        void RegisterViewWithRegion(string regionName, Type viewType);
        IEnumerable<object> GetContents(string name);
        void RegisterViewWithRegion(string regionName, Func<object> getContentDelegate);
    }
}

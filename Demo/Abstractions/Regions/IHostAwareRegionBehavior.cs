

using System.Windows;

namespace Demo.Regions
{
    public interface IHostAwareRegionBehavior : IRegionBehavior
    {
        DependencyObject HostControl { get; set; }
    }
}
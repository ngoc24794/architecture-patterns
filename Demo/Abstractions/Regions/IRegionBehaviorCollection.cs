﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionBehaviorCollection.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Collections.Generic;

namespace Demo.Regions
{
    public interface IRegionBehaviorCollection : IEnumerable<KeyValuePair<string, IRegionBehavior>>
    {
        void Add(string key, IRegionBehavior regionBehavior);

        bool ContainsKey(string key);

        IRegionBehavior this[string key] { get; }
    }
}

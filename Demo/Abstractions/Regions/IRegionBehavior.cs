﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionBehavior.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace Demo.Regions
{
    public interface IRegionBehavior
    {
        IRegion Region { get; set; }

        void Attach();
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegion.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.ComponentModel;

namespace Demo.Regions
{
    public interface IRegion : INotifyPropertyChanged
    {
        IRegionBehaviorCollection Behaviors { get; }
        string Name { get; set; }
        IViewsCollection ActiveViews { get; }
        IViewsCollection Views { get; }
        IRegionManager RegionManager { get; set; }

        IRegionManager Add(object view);
        void Activate(object view);
    }
}

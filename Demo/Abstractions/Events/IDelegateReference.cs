﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IDelegateReference.cs
**
** Description: Đại diện cho một tham chiếu đến một Delegate
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;

namespace Demo.Regions
{
    /// <summary>
    /// Đại diện cho một tham chiếu đến một <see cref="Delegate"/>
    /// </summary>
    internal class IDelegateReference
    {
        /// <summary>
        /// Trả về <see cref="Delegate"/> được tham chiếu.
        /// </summary>
        Delegate Target { get; }
    }
}

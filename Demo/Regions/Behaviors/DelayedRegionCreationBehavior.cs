using System;
using System.Windows;

namespace Demo.Regions.Behaviors
{
    public class DelayedRegionCreationBehavior
    {
        private readonly RegionAdapterMappings regionAdapterMappings;
        private WeakReference elementWeakReference;
        private bool regionCreated;

        public DelayedRegionCreationBehavior(RegionAdapterMappings regionAdapterMappings)
        {
            this.regionAdapterMappings = regionAdapterMappings;
            this.RegionManagerAccessor = new DefaultRegionManagerAccessor();
        }

        public IRegionManagerAccessor RegionManagerAccessor { get; set; }

        public DependencyObject TargetElement
        {
            get { return this.elementWeakReference != null ? this.elementWeakReference.Target as DependencyObject : null; }
            set { this.elementWeakReference = new WeakReference(value); }
        }

        public void Attach()
        {
            this.RegionManagerAccessor.UpdatingRegions += this.OnUpdatingRegions;
            this.WireUpTargetElement();
        }

        public void Detach()
        {
            this.RegionManagerAccessor.UpdatingRegions -= this.OnUpdatingRegions;
            this.UnWireTargetElement();
        }

        public void OnUpdatingRegions(object sender, EventArgs e)
        {
            this.TryCreateRegion();
        }

        private void TryCreateRegion()
        {
            DependencyObject targetElement = this.TargetElement;
            if (targetElement == null)
            {
                this.Detach();
                return;
            }

            if (targetElement.CheckAccess())
            {
                this.Detach();

                if (!this.regionCreated)
                {
                    string regionName = this.RegionManagerAccessor.GetRegionName(targetElement);
                    CreateRegion(targetElement, regionName);
                    this.regionCreated = true;
                }
            }
        }

        protected virtual IRegion CreateRegion(DependencyObject targetElement, string regionName)
        {
            // Build the region
            IRegionAdapter regionAdapter = this.regionAdapterMappings.GetMapping(targetElement.GetType());
            IRegion region = regionAdapter.Initialize(targetElement, regionName);

            return region;
        }

        private void ElementLoaded(object sender, RoutedEventArgs e)
        {
            this.UnWireTargetElement();
            this.TryCreateRegion();
        }

        private void WireUpTargetElement()
        {
            if (this.TargetElement is FrameworkElement element)
            {
                element.Loaded += this.ElementLoaded;
                return;
            }

            if (this.TargetElement is FrameworkContentElement fcElement)
            {
                fcElement.Loaded += this.ElementLoaded;
                return;
            }
        }

        private void UnWireTargetElement()
        {
            if (this.TargetElement is FrameworkElement element)
            {
                element.Loaded -= this.ElementLoaded;
                return;
            }

            if (this.TargetElement is FrameworkContentElement fcElement)
            {
                fcElement.Loaded -= this.ElementLoaded;
                return;
            }
        }
    }
}
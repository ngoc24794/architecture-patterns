﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionBehaviorCollection.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/


using System.Collections;
using System.Collections.Generic;

namespace Demo.Regions.Behaviors
{
    public class RegionBehaviorCollection : IRegionBehaviorCollection
    {
        private readonly IRegion region;
        private readonly Dictionary<string, IRegionBehavior> behaviors = new Dictionary<string, IRegionBehavior>();

        public RegionBehaviorCollection(IRegion region)
        {
            this.region = region;
        }

        public IRegionBehavior this[string key]
        {
            get { return this.behaviors[key]; }
        }

        public void Add(string key, IRegionBehavior regionBehavior)
        {
            behaviors.Add(key, regionBehavior);
            regionBehavior.Region = this.region;

            regionBehavior.Attach();
        }

        public bool ContainsKey(string key)
        {
            return behaviors.ContainsKey(key);
        }

        public IEnumerator<KeyValuePair<string, IRegionBehavior>> GetEnumerator()
        {
            return behaviors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return behaviors.GetEnumerator();
        }
    }
}

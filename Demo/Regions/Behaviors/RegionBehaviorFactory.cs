using IoCPattern;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Demo.Regions.Behaviors
{
    public class RegionBehaviorFactory : IRegionBehaviorFactory
    {
        private readonly IServiceLocator serviceLocator;
        private readonly Dictionary<string, Type> registeredBehaviors = new Dictionary<string, Type>();

        public RegionBehaviorFactory(IServiceLocator serviceLocator)
        {
            this.serviceLocator = serviceLocator;
        }

        public void AddIfMissing(string behaviorKey, Type behaviorType)
        {
            if (this.registeredBehaviors.ContainsKey(behaviorKey))
            {
                return;
            }

            this.registeredBehaviors.Add(behaviorKey, behaviorType);
        }

        public IRegionBehavior CreateFromKey(string key)
        {
            return (IRegionBehavior)this.serviceLocator.Resolve(this.registeredBehaviors[key]);
        }


        public IEnumerator<string> GetEnumerator()
        {
            return this.registeredBehaviors.Keys.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public bool ContainsKey(string behaviorKey)
        {
            return this.registeredBehaviors.ContainsKey(behaviorKey);
        }
    }
}

using System;
using System.Collections.Generic;

namespace Demo.Regions
{
    public class RegionAdapterMappings
    {
        private readonly Dictionary<Type, IRegionAdapter> mappings = new Dictionary<Type, IRegionAdapter>();

        public void RegisterMapping(Type controlType, IRegionAdapter adapter)
        {
            mappings.Add(controlType, adapter);
        }

        public IRegionAdapter GetMapping(Type controlType)
        {
            Type currentType = controlType;

            while (currentType != null)
            {
                if (mappings.ContainsKey(currentType))
                {
                    return mappings[currentType];
                }
                currentType = currentType.BaseType;
            }
            return null;
        }
    }
}
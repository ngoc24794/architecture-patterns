﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegion.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/


using Demo.Regions.Behaviors;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Demo.Regions
{
    public class Region : IRegion
    {
        private ObservableCollection<ItemMetadata> itemMetadataCollection;
        private string name;
        private ViewsCollection views;
        private ViewsCollection activeViews;
        private IRegionManager regionManager;

        public Region()
        {
            this.Behaviors = new RegionBehaviorCollection(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public IRegionBehaviorCollection Behaviors { get; private set; }


        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }

        public virtual IViewsCollection Views
        {
            get
            {
                if (this.views == null)
                {
                    this.views = new ViewsCollection(ItemMetadataCollection, x => true);
                }

                return this.views;
            }
        }

        public virtual IViewsCollection ActiveViews
        {
            get
            {
                if (this.views == null)
                {
                    this.views = new ViewsCollection(ItemMetadataCollection, x => true);
                }

                if (this.activeViews == null)
                {
                    this.activeViews = new ViewsCollection(ItemMetadataCollection, x => x.IsActive);
                }

                return this.activeViews;
            }
        }

        public IRegionManager RegionManager
        {
            get
            {
                return this.regionManager;
            }

            set
            {
                if (this.regionManager != value)
                {
                    this.regionManager = value;
                    this.OnPropertyChanged("RegionManager");
                }
            }
        }

        protected virtual ObservableCollection<ItemMetadata> ItemMetadataCollection
        {
            get
            {
                if (this.itemMetadataCollection == null)
                {
                    this.itemMetadataCollection = new ObservableCollection<ItemMetadata>();
                }

                return this.itemMetadataCollection;
            }
        }

        public IRegionManager Add(object view)
        {
            return this.Add(view, null, false);
        }

        public IRegionManager Add(object view, string viewName)
        {
            return this.Add(view, viewName, false);
        }

        public virtual IRegionManager Add(object view, string viewName, bool createRegionManagerScope)
        {
            IRegionManager manager = createRegionManagerScope ? this.RegionManager.CreateRegionManager() : this.RegionManager;
            this.InnerAdd(view, viewName, manager);
            return manager;
        }

        public virtual void Remove(object view)
        {
            ItemMetadata itemMetadata = this.GetItemMetadataOrThrow(view);

            this.ItemMetadataCollection.Remove(itemMetadata);

            DependencyObject dependencyObject = view as DependencyObject;
            if (dependencyObject != null && Regions.RegionManager.GetRegionManager(dependencyObject) == this.RegionManager)
            {
                dependencyObject.ClearValue(Regions.RegionManager.RegionManagerProperty);
            }
        }

        public void RemoveAll()
        {
            foreach (var view in Views)
            {
                Remove(view);
            }
        }

        public virtual void Activate(object view)
        {
            ItemMetadata itemMetadata = this.GetItemMetadataOrThrow(view);

            if (!itemMetadata.IsActive)
            {
                itemMetadata.IsActive = true;
            }
        }

        public virtual void Deactivate(object view)
        {
            ItemMetadata itemMetadata = this.GetItemMetadataOrThrow(view);

            if (itemMetadata.IsActive)
            {
                itemMetadata.IsActive = false;
            }
        }

        public virtual object GetView(string viewName)
        {
            ItemMetadata metadata = this.ItemMetadataCollection.FirstOrDefault(x => x.Name == viewName);

            if (metadata != null)
            {
                return metadata.Item;
            }

            return null;
        }


        private void InnerAdd(object view, string viewName, IRegionManager scopedRegionManager)
        {
            ItemMetadata itemMetadata = new ItemMetadata(view);
            if (!string.IsNullOrEmpty(viewName))
            {
                itemMetadata.Name = viewName;
            }

            if (view is DependencyObject dependencyObject)
            {
                Regions.RegionManager.SetRegionManager(dependencyObject, scopedRegionManager);
            }

            this.ItemMetadataCollection.Add(itemMetadata);
        }

        private ItemMetadata GetItemMetadataOrThrow(object view)
        {
            ItemMetadata itemMetadata = ItemMetadataCollection.FirstOrDefault(x => x.Item == view);

            return itemMetadata;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

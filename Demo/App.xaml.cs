﻿using Demo.Regions;
using Demo.Regions.Behaviors;
using IoCPattern;
using System.Windows;
using System.Windows.Controls;

namespace Demo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        IContainer _container;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            _container = new Container();
            _container.RegisterSingleton<IServiceLocator, ServiceLocator>();
            _container.RegisterSingleton<RegionAdapterMappings, RegionAdapterMappings>();
            _container.RegisterSingleton<IRegionManager, RegionManager>();
            _container.RegisterSingleton<IRegionViewRegistry, RegionViewRegistry>();
            _container.RegisterSingleton<IRegionBehaviorFactory, RegionBehaviorFactory>();
            _container.Register<ContentControlRegionAdapter>();
            _container.Register<DelayedRegionCreationBehavior>();
            _container.Register<AutoPopulateRegionBehavior>();
            RegionAdapterMappings regionAdapterMappings = _container.Resolve<RegionAdapterMappings>();
            if (regionAdapterMappings != null)
            {
                regionAdapterMappings.RegisterMapping(typeof(ContentControl), _container.Resolve<ContentControlRegionAdapter>());
            }

            var defaultRegionBehaviorTypesDictionary = _container.Resolve<IRegionBehaviorFactory>();

            if (defaultRegionBehaviorTypesDictionary != null)
            {
                defaultRegionBehaviorTypesDictionary.AddIfMissing(AutoPopulateRegionBehavior.BehaviorKey,
                                                  typeof(AutoPopulateRegionBehavior));
            }

            _container.Register<MainWindow>();
            _container.Register<UserControl1>();
            IRegionManager regionManager = _container.Resolve<IRegionManager>();
            Application.Current.MainWindow = _container.Resolve<MainWindow>();
            RegionManager.SetRegionManager(Application.Current.MainWindow, regionManager);

            regionManager.RegisterViewWithRegion("ContentRegion", typeof(UserControl1));
            RegionManager.UpdateRegions();
            Application.Current.MainWindow.Show();
        }
    }
}

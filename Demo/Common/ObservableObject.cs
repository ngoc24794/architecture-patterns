

using System.ComponentModel;
using System.Windows;

namespace Demo.Regions
{
    public class ObservableObject<T> : FrameworkElement, INotifyPropertyChanged
    {
        public static readonly DependencyProperty ValueProperty =
                DependencyProperty.Register("Value", typeof(T), typeof(ObservableObject<T>), new PropertyMetadata(ValueChangedCallback));

        public event PropertyChangedEventHandler PropertyChanged;

        public T Value
        {
            get { return (T)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private static void ValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObservableObject<T> thisInstance = ((ObservableObject<T>)d);
            thisInstance.PropertyChanged?.Invoke(thisInstance, new PropertyChangedEventArgs("Value"));
        }
    }
}
﻿using INV.Elearning.Core.Architecture.IoC;
using INV.Elearning.Core.Architecture.Modularity;
using INV.Elearning.Core.Architecture.Regions;

namespace Module1
{
    public class Module1 : IModule
    {
        private readonly IContainer _container;
        private readonly IRegionManager _regionManager;

        public Module1(IContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
        }

        public void RegisterTypes()
        {
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(UserControl1));
        }
    }
}

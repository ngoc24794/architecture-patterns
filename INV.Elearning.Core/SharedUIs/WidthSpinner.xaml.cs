﻿using INV.Elearning.Controls;
using System.Windows;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for WidthSpinner.xaml
    /// </summary>
    public partial class WidthSpinner : Spinner
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public WidthSpinner()
        {
            InitializeComponent();
            this.Style = TryFindResource("SpinnerStyle") as Style;
        }
    }
}

﻿using System.Windows.Input;
using INV.Elearning.Controls;
using INV.Elearning.Core.Helper;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for InsertShapeInRibbonGallery.xaml
    /// </summary>
    public partial class InsertShapeInRibbonGallery : InRibbonGallery
    {
        public InsertShapeInRibbonGallery()
        {
            InitializeComponent();
            this.ItemsSource = ShapeHelper.Shapes;
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            this.SelectedIndex = -1;
        }
    }
}

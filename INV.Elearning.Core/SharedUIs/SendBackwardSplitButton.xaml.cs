﻿using INV.Elearning.Controls;
using INV.Elearning.Core.AppCommands;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for SendBackwardSplitButton.xaml
    /// </summary>
    public partial class SendBackwardSplitButton : SplitButton
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public SendBackwardSplitButton()
        {
            InitializeComponent();
        }
    }
}

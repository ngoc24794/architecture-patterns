﻿using INV.Elearning.Controls;
using System.Windows.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for RotationDropDownButton.xaml
    /// </summary>
    public partial class RotationDropDownButton : DropDownButton
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public RotationDropDownButton()
        {
            InitializeComponent();
            this.Template = TryFindResource("RibbonDropDownButtonControlTemplate") as ControlTemplate;
        }
    }
}

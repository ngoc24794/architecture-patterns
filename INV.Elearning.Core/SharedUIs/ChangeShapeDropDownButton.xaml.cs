﻿using INV.Elearning.Controls;
using INV.Elearning.Core.Helper;
using System.Linq;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for InsertShapeDropDownButton.xaml
    /// </summary>
    public partial class ChangeShapeDropDownButton : DropDownButton
    {
        public ChangeShapeDropDownButton()
        {
            InitializeComponent();
            gallery.ItemsSource = ShapeHelper.Shapes.Where(x=>x.GroupName != "Lines");
        }
    }
}

﻿using INV.Elearning.Controls;
using INV.Elearning.Core.Helper;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for InsertShapeDropDownButton.xaml
    /// </summary>
    public partial class InsertShapeDropDownButton : DropDownButton
    {
        public InsertShapeDropDownButton()
        {
            InitializeComponent();
            gallery.ItemsSource = ShapeHelper.Shapes;
        }
    }
}

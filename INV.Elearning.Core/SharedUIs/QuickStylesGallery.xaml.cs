﻿using INV.Elearning.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for QuickStylesGallery.xaml
    /// </summary>
    public partial class QuickStylesGallery : Gallery
    {
        public QuickStylesGallery()
        {
            InitializeComponent();
        }
    }
}

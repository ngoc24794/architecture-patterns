﻿using INV.Elearning.Controls;
using System.Windows.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for ArrangeDropDownButton.xaml
    /// </summary>
    public partial class ArrangeDropDownButton : DropDownButton
    {
        public ArrangeDropDownButton()
        {
            InitializeComponent();
            this.Template = TryFindResource("RibbonDropDownButtonControlTemplate") as ControlTemplate;
        }
    }
}

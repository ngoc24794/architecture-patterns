﻿using INV.Elearning.Controls;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for SlidePropertiesWindow.xaml
    /// </summary>
    public partial class SlidePropertiesWindow : ElearningWindow, INotifyPropertyChanged
    {

        private ObservableCollection<string> _slideAdvancedChoices;
        /// <summary>
        /// Danh sách lựa chọn slide advances
        /// </summary>
        public ObservableCollection<string> SlideAdvancedChoices
        {
            get { return _slideAdvancedChoices ?? (_slideAdvancedChoices = new ObservableCollection<string>()); }
            set { _slideAdvancedChoices = value; }
        }

        private ObservableCollection<string> _revisitingChoices;
        /// <summary>
        /// Danh sách lựa chọn revisiting
        /// </summary>
        public ObservableCollection<string> RevisitingChoices
        {
            get { return _revisitingChoices ?? (_revisitingChoices = new ObservableCollection<string>()); }
            set { _revisitingChoices = value; }
        }

        private ObservableCollection<string> _playerFeaturesChoices;

        public ObservableCollection<string> PlayerFeaturesChoices
        {
            get { return _playerFeaturesChoices ?? (_playerFeaturesChoices = new ObservableCollection<string>()); }
            set { _playerFeaturesChoices = value; }
        }


        private PageConfig _pageConfig;
        /// <summary>
        /// Page Config
        /// </summary>
        public PageConfig PageConfig
        {
            get { return _pageConfig; }
            set { _pageConfig = value; OnPropertyChanged("PageConfig"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }

        public SlidePropertiesWindow()
        {
            InitializeComponent();

            if ((Application.Current as IAppGlobal).SelectedSlide is SlideBase slideBase)
            {
                if (slideBase.PageConfig == null)
                {
                    slideBase.PageConfig = new PageConfig();
                }
                PageConfig = slideBase.PageConfig.Clone() as PageConfig;
            }

            SlideAdvancedChoices.Clear();
            SlideAdvancedChoices.Add(FileHelper.FindResource("CORESLIDE_Automatically")?.ToString());
            SlideAdvancedChoices.Add(FileHelper.FindResource("CORESLIDE_ByUser")?.ToString());

            RevisitingChoices.Clear();
            RevisitingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_AutomaticDecide")?.ToString());
            RevisitingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_Reset")?.ToString());
            RevisitingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_Resume")?.ToString());

            PlayerFeaturesChoices.Clear();
            PlayerFeaturesChoices.Add(FileHelper.FindResource("CORESLIDE_PlayerDefault")?.ToString());
            PlayerFeaturesChoices.Add(FileHelper.FindResource("CORESLIDE_Custom")?.ToString());
        }

        private void cbbAdvance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbbAdvance.SelectedIndex)
            {
                case 0:
                    PageConfig.AdvanceMode = SlideAdvanceMode.Auto;
                    break;
                case 1:
                    PageConfig.AdvanceMode = SlideAdvanceMode.User;
                    break;
                default:
                    break;
            }
        }



        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if ((Application.Current as IAppGlobal).SelectedSlide is SlideBase slideBase)
            {
                slideBase.PageConfig = PageConfig;
            }
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cbbRevisiting_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbbRevisiting.SelectedIndex)
            {
                case 0:
                    PageConfig.RevisitMode = RevisitMode.Auto;
                    break;
                case 1:
                    PageConfig.RevisitMode = RevisitMode.Initial;
                    break;
                case 2:
                    PageConfig.RevisitMode = RevisitMode.Save;
                    break;
                default:
                    break;
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            PageConfig.MenuEnable = true;
            PageConfig.ResourcesEnable = true;
            PageConfig.SeekbarEnable = true;
            PageConfig.GlosarryEnable = true;
            PageConfig.NotesEnable = true;
        }

        //private void cbbfeatures_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    switch (cbbfeatures.SelectedIndex)
        //    {
        //        case 0:
        //            PageConfig.PlayerFeaturesMode = PlayerFeaturesMode.Default;
        //            break;
        //        case 1:
        //            PageConfig.PlayerFeaturesMode = PlayerFeaturesMode.Custom;
        //            break;
        //        default:
        //            break;
        //    }
        //}
    }
}

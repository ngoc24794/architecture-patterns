﻿using INV.Elearning.Controls;
using System.Windows.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for QuickStylesDropDownButton.xaml
    /// </summary>
    public partial class QuickStylesDropDownButton : DropDownButton
    {
        public QuickStylesDropDownButton()
        {
            InitializeComponent();
            this.Template = TryFindResource("RibbonDropDownButtonControlTemplate") as ControlTemplate;
        }
    }
}

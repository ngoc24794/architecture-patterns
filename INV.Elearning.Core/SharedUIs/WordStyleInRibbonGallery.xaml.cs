﻿using INV.Elearning.Controls;
using System.Windows.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for WordStyleInRibbonGallery.xaml
    /// </summary>
    public partial class WordStyleInRibbonGallery : InRibbonGallery
    {
        public WordStyleInRibbonGallery()
        {
            InitializeComponent();
            this.Template = TryFindResource("InRibbonGalleryControlTemplate") as ControlTemplate;
        }
    }
}

﻿

namespace INV.Elearning.Core.SharedUIs
{
    using INV.Elearning.Controls;
    using INV.Elearning.Core.Helper.Effect;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ShapeEffectsDropDownButton.xaml
    /// </summary>
    public partial class ShapeEffectsDropDownButton : DropDownButton
    {
        public ShapeEffectsDropDownButton()
        {
            InitializeComponent();
            this.Template = TryFindResource("RibbonDropDownButtonControlTemplate") as ControlTemplate;
            menuItem.Style = refMenuItem.Style = glowMenuItem.Style = softMenuItem.Style = TryFindResource("MenuItemLargeStyle") as Style;
            gallery.ItemsSource = EffectHelper.ShadowIcons;
            refGallery.ItemsSource = EffectHelper.ReflectionIcons;
            glowGallery.ItemsSource = EffectHelper.GlowIcons;
           softGallery.ItemsSource = EffectHelper.SoftEdgeIcons;
        }
    }
}

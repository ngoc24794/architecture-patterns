﻿using INV.Elearning.Controls;
using System.Windows.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for AlignDropDownButton.xaml
    /// </summary>
    public partial class AlignDropDownButton : DropDownButton
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public AlignDropDownButton()
        {
            InitializeComponent();
            this.Template = TryFindResource("RibbonDropDownButtonControlTemplate") as ControlTemplate;
        }
    }
}

﻿

namespace INV.Elearning.Core.SharedUIs
{
    using INV.Elearning.Controls;
    using INV.Elearning.Core.Helper;
    using INV.Elearning.Core.View;
    using INV.Elearning.Core.ViewModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for ShapeFillSplitButton.xaml
    /// </summary>
    public partial class ShapeFillSplitButton : SplitButton
    {
        public ShapeFillSplitButton()
        {
            InitializeComponent();
            Binding _binding = new Binding("SelectedColor");
            _binding.Source = home_ShapeFillColor;
            this.SetBinding(CommandParameterProperty, _binding);
        }

        private void SelectedColorChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            FormatShapeCommands.ShapeFillCommand.Execute(home_ShapeFillColor.SelectedColor);
        }

        private void SplitButton_DropDownOpened(object sender, System.EventArgs e)
        {
            if ((Application.Current as IAppGlobal).SelectedElements.Count == 1 && (Application.Current as IAppGlobal).SelectedItem is IBorderSupport borderSupport && borderSupport.Fill is ColorSolidBrush colorSolid)
            {
                home_ShapeFillColor.SelectedColor = new SolidColor() { Color = colorSolid.Color.ToString() };
            }
            else
            {
                home_ShapeFillColor.SelectedColor = null;
            }

        }
    }
}

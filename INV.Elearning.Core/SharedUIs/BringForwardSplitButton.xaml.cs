﻿using INV.Elearning.Controls;
using INV.Elearning.Core.AppCommands;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for BringForwardSplitButton.xaml
    /// </summary>
    public partial class BringForwardSplitButton : SplitButton
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public BringForwardSplitButton()
        {
            InitializeComponent();
        }
    }
}

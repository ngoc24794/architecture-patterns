﻿using INV.Elearning.Controls;
using System.Windows.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for GroupDropDownButton.xaml
    /// </summary>
    public partial class GroupDropDownButton : DropDownButton
    {
        /// <summary>
        /// HÀm khởi tạo mặc định
        /// </summary>
        public GroupDropDownButton()
        {
            InitializeComponent();
            this.Template = TryFindResource("RibbonDropDownButtonControlTemplate") as ControlTemplate;
        }
    }
}

﻿using INV.Elearning.Controls;
using System;
using System.Windows;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for FormatRibbonTabItem.xaml
    /// </summary>
    public partial class FormatRibbonTabItem : RibbonTabItem
    {
        public FormatRibbonTabItem()
        {
            if (Application.Current?.Resources != null && !Application.Current.Resources.Contains("COREFORMATRIBBON_Title"))
            {
                ResourceDictionary resourceDictionary = new ResourceDictionary();
                resourceDictionary.Source = new Uri(@"/INV.Elearning.Core;component/Resources/Languages/Language.xaml");
                Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
            }

            InitializeComponent();
        }
    }
}

﻿using INV.Elearning.Controls;
using System.Windows;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for HeightSpinner.xaml
    /// </summary>
    public partial class HeightSpinner : Spinner
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public HeightSpinner()
        {
            InitializeComponent();
            this.Style = TryFindResource("SpinnerStyle") as Style;
        }
    }
}

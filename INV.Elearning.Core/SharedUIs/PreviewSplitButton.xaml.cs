﻿using INV.Elearning.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for PreviewSplitButton.xaml
    /// </summary>
    public partial class PreviewSplitButton : SplitButton
    {
        public PreviewSplitButton()
        {
            InitializeComponent();
        }
    }
}

﻿using INV.Elearning.Controls;
using System.Windows.Controls;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for QuickStylesInRibbonGallery.xaml
    /// </summary>
    public partial class QuickStylesInRibbonGallery : InRibbonGallery
    {
        public QuickStylesInRibbonGallery()
        {
            InitializeComponent();
            this.Template = TryFindResource("InRibbonGalleryControlTemplate") as ControlTemplate;
        }
    }
}

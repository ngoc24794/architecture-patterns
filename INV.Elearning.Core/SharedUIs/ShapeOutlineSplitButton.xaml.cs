﻿
namespace INV.Elearning.Core.SharedUIs
{
    using INV.Elearning.Controls;
    using INV.Elearning.Core.ViewModel;
    using System.Windows.Data;

    /// <summary>
    /// Interaction logic for ShapeOutlineSplitButton.xaml
    /// </summary>
    public partial class ShapeOutlineSplitButton : SplitButton
    {
        public ShapeOutlineSplitButton()
        {
            InitializeComponent();
            Binding _binding = new Binding("SelectedColor");
            _binding.Source = home_ShapeOutlineColor;
            this.SetBinding(CommandParameterProperty, _binding);
        }

        private void SelectedColorChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            FormatShapeCommands.ShapeBrushCommand.Execute(home_ShapeOutlineColor.SelectedColor);
        }
    }
}

﻿using INV.Elearning.Controls;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace INV.Elearning.Core.SharedUIs
{
    /// <summary>
    /// Interaction logic for SlideLayerProperties.xaml
    /// </summary>
    public partial class SlideLayerProperties : ElearningWindow, INotifyPropertyChanged
    {
        private PageLayerConfig _pageLayerConfig;
        /// <summary>
        /// Model Config Layer
        /// </summary>
        public PageLayerConfig PageLayerConfig
        {
            get { return _pageLayerConfig; }
            set { _pageLayerConfig = value; OnPropertyChanged("PageLayerConfig"); }
        }

        private ObservableCollection<string> _allowSeekingChoices;
        /// <summary>
        /// Danh sách lựa chọn Allow Seeking
        /// </summary>
        public ObservableCollection<string> AllowSeekingChoices
        {
            get { return _allowSeekingChoices ?? (_allowSeekingChoices = new ObservableCollection<string>()); }
            set { _allowSeekingChoices = value; }
        }

        private ObservableCollection<string> _whenRevisitingChoices;
        /// <summary>
        /// Danh sách lựa chọn When revisiting
        /// </summary>
        public ObservableCollection<string> WhenRevisitingChoices
        {
            get { return _whenRevisitingChoices ?? (_whenRevisitingChoices = new ObservableCollection<string>()); }
            set { _whenRevisitingChoices = value; }
        }




        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }

        /// <summary>
        /// hàm khởi tạo
        /// </summary>
        public SlideLayerProperties()
        {
            InitializeComponent();
            if ((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout is LayoutBase layoutBase)
            {
                if (!layoutBase.IsMainLayout)
                {
                    if (layoutBase.LayoutConfig == null)
                    {
                        layoutBase.LayoutConfig = new PageLayerConfig();
                    }
                    PageLayerConfig = layoutBase.LayoutConfig.Clone() as PageLayerConfig;
                }
            }

            AllowSeekingChoices.Clear();
            AllowSeekingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_AutomaticDecide")?.ToString());
            AllowSeekingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_Yes")?.ToString());
            AllowSeekingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_No")?.ToString());

            WhenRevisitingChoices.Clear();
            WhenRevisitingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_AutomaticDecide")?.ToString());
            WhenRevisitingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_Reset")?.ToString());
            WhenRevisitingChoices.Add(FileHelper.FindResource("CORESLIDELAYER_Resume")?.ToString());
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if ((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout is LayoutBase layoutBase)
            {
                if (!layoutBase.IsMainLayout)
                {
                    layoutBase.LayoutConfig = PageLayerConfig;
                }
            }
            Close();
        }

        private void cbbAllowSeeking_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbbAllowSeeking.SelectedIndex)
            {
                case 0:
                    PageLayerConfig.AllowSeekingMode = AllowSeekingMode.Auto;
                    break;
                case 1:
                    PageLayerConfig.AllowSeekingMode = AllowSeekingMode.Yes;
                    break;
                case 2:
                    PageLayerConfig.AllowSeekingMode = AllowSeekingMode.No;
                    break;
                default:
                    break;
            }
        }

        private void cbbWhenRevisiting_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbbWhenRevisiting.SelectedIndex)
            {
                case 0:
                    PageLayerConfig.RevisitMode = RevisitMode.Auto;
                    break;
                case 1:
                    PageLayerConfig.RevisitMode = RevisitMode.Initial;
                    break;
                case 2:
                    PageLayerConfig.RevisitMode = RevisitMode.Save;
                    break;
                default:
                    break;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}

﻿using System.Windows.Media.Imaging;

namespace INV.Elearning.Core.Model
{
    public interface IUpdatePropertyByUndo
    {
        /// <summary>
        /// Hàm cập nhật dữ liệu từ Undo
        /// </summary>
        void OnPropertyChangeByUndo();
    }
}

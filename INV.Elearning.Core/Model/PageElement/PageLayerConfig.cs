﻿using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("plCfg")]
    /// <summary>
    /// Cấu hình cho Slide
    /// </summary>
    public class PageLayerConfig : RootModel
    {
        /// <summary>
        /// Ẩn tất cả các Layer khác
        /// </summary>
        [XmlAttribute("hdOlays")]
        public bool HideOtherLayer { get; set; }
        /// <summary>
        /// Ẩn các đối tượng của Layer chính
        /// </summary>
        [XmlAttribute("hdOsblay")]
        public bool HideObjectsOnBaseLayer { get; set; }
        /// <summary>
        /// Ẩn Layer khi kết thúc timeline
        /// </summary>
        [XmlAttribute("hdFis")]
        public bool HideWhenTimelineFinished { get; set; }
        /// <summary>
        /// Ngăn chặn Click trên đối tượng ở Layer chính
        /// </summary>
        [XmlAttribute("preClk")]
        public bool PreventClickToObjectsOnBaseLayer { get; set; }
        /// <summary>
        /// Dừng Timeline ở Layer chính
        /// </summary>
        [XmlAttribute("pauLayb")]
        public bool PauseTimeOfBaseLayer { get; set; }
        /// <summary>
        /// Chế độ tìm kiếm
        /// </summary>
        [XmlAttribute("alSek")]
        public AllowSeekingMode AllowSeekingMode { get; set; }
        /// <summary>
        /// Chế độ quay lại trang
        /// </summary>
        [XmlAttribute("rvMo")]
        public RevisitMode RevisitMode { get; set; }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new PageLayerConfig()
            {
                AllowSeekingMode = AllowSeekingMode,
                HideObjectsOnBaseLayer = HideObjectsOnBaseLayer,
                HideOtherLayer = HideOtherLayer,
                HideWhenTimelineFinished = HideWhenTimelineFinished,
                PauseTimeOfBaseLayer = PauseTimeOfBaseLayer,
                PreventClickToObjectsOnBaseLayer = PreventClickToObjectsOnBaseLayer,
                RevisitMode = RevisitMode
            };
        }
    }

    /// <summary>
    /// Chế độ
    /// </summary>
    public enum AllowSeekingMode
    {
        Auto,
        Yes,
        No
    }

    /// <summary>
    /// Chế độ quay lại xem trang
    /// </summary>
    public enum RevisitMode
    {
        Auto,
        Initial,
        Save
    }
}
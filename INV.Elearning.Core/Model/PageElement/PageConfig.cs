﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Windows;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{

    public class ChangedConfigurationsEventArgs : EventArgs
    {
        public ChangedConfigurationsEventArgs(ConfigurationAtrribute configurationAtrribute, object newValue, object oldValue)
        {
            ConfigurationAtrribute = configurationAtrribute;
            NewValue = newValue;
            OldValue = oldValue;
        }

        public ConfigurationAtrribute ConfigurationAtrribute { get; internal set; }
        public object NewValue { get; internal set; }
        public object OldValue { get; internal set; }
    }

    public enum ConfigurationAtrribute
    {
        PreviousButtonEnable,
        NextButtonEnable,
        SubmitButtonEnable,
        PreviousSwipeEnable,
        NextSwipeEnable
    }

    /// <summary>
    /// Lớp lưu trữ cài đặt cho slide
    /// </summary>
    [Serializable]
    [XmlRoot("pgCfg")]
    public class PageConfig : RootModel
    {
        /// <summary>
        /// Sự kiện thông tin cấu hình thay đổi
        /// </summary>
        public event EventHandler<ChangedConfigurationsEventArgs> ChangedConfigurations;

        /// <summary>
        /// Hiển thị nút quay lại
        /// </summary>
        [XmlAttribute("shPre")]
        public bool PreviousButtonEnable
        {
            get => _previousButtonEnable;
            set
            {
                if (_previousButtonEnable != value)
                {

                    ChangedConfigurations?.Invoke(this, new ChangedConfigurationsEventArgs(ConfigurationAtrribute.PreviousButtonEnable, value, _previousButtonEnable));
                }
                _previousButtonEnable = value;
            }
        }
        /// <summary>
        /// Hiển thị nút tiếp theo
        /// </summary>
        [XmlAttribute("shNex")]
        public bool NextButtonEnable
        {
            get => _nextButtonEnable;
            set
            {
                if (_nextButtonEnable != value)
                {
                    ChangedConfigurations?.Invoke(this, new ChangedConfigurationsEventArgs(ConfigurationAtrribute.NextButtonEnable, value, _nextButtonEnable));
                }
                _nextButtonEnable = value;
            }
        }
        /// <summary>
        /// Hiển thị nút Submit
        /// </summary>
        [XmlAttribute("shSub")]
        public bool SubmitButtonEnable
        {
            get => _submitButtonEnable;
            set
            {
                if (_submitButtonEnable != value)
                {
                    ChangedConfigurations?.Invoke(this, new ChangedConfigurationsEventArgs(ConfigurationAtrribute.SubmitButtonEnable, value, _submitButtonEnable));
                }
                _submitButtonEnable = value;
            }
        }
        /// <summary>
        /// Cho phép vuốt quay lại
        /// </summary>
        [XmlAttribute("swPre")]
        public bool PreviousSwipeEnable
        {
            get => _previousSwipeEnable;
            set
            {
                if (_previousSwipeEnable != value)
                {
                    ChangedConfigurations?.Invoke(this, new ChangedConfigurationsEventArgs(ConfigurationAtrribute.PreviousSwipeEnable, value, _previousSwipeEnable));
                }
                _previousSwipeEnable = value;
            }
        }
        /// <summary>
        /// Cho phép vuốt tiếp theo
        /// </summary>
        [XmlAttribute("swNex")]
        public bool NextSwipeEnable
        {
            get => _nextSwipeEnable;
            set
            {
                if (_nextSwipeEnable != value)
                {
                    ChangedConfigurations?.Invoke(this, new ChangedConfigurationsEventArgs(ConfigurationAtrribute.NextSwipeEnable, value, _nextSwipeEnable));
                }
                _nextSwipeEnable = value;
            }
        }
        /// <summary>
        /// Chế độ xem lại
        /// </summary>
        [XmlAttribute("rvMo")]
        public RevisitMode RevisitMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("adMo")]
        public SlideAdvanceMode AdvanceMode { get; set; }

        private PlayerFeaturesMode playerFeaturesMode;
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("pfMo")]
        public PlayerFeaturesMode PlayerFeaturesMode
        {
            get { return playerFeaturesMode; }
            set { playerFeaturesMode = value; OnPropertyChanged("PlayerFeaturesMode"); }
        }


        private bool _menuEnable;
        /// <summary>
        /// Cho phép player hiển thị menu
        /// </summary>
        [XmlAttribute("me")]
        public bool MenuEnable
        {
            get { return _menuEnable; }
            set { _menuEnable = value; OnPropertyChanged("MenuEnable"); }
        }


        private bool _glossaryEnable;
        /// <summary>
        /// Cho phép player hiển thị Glosarry
        /// </summary>
        [XmlAttribute("gl")]
        public bool GlosarryEnable
        {
            get { return _glossaryEnable; }
            set { _glossaryEnable = value; OnPropertyChanged("GlosarryEnable"); }
        }


        private bool _seekEnable;
        /// <summary>
        /// Cho phép player hiển thị seekbar
        /// </summary>
        [XmlAttribute("se")]
        public bool SeekbarEnable
        {
            get { return _seekEnable; }
            set { _seekEnable = value; OnPropertyChanged("SeekbarEnable"); }
        }


        private bool _resourcesEnable;
        /// <summary>
        /// Cho phép player hiển thị resources
        /// </summary>
        [XmlAttribute("res")]
        public bool ResourcesEnable
        {
            get { return _resourcesEnable; }
            set { _resourcesEnable = value; OnPropertyChanged("ResourcesEnable"); }
        }


        private bool _noteEnable;
        private bool _previousButtonEnable = true;
        private bool _nextButtonEnable = true;
        private bool _submitButtonEnable;
        private bool _previousSwipeEnable = true;
        private bool _nextSwipeEnable = true;

        public PageConfig()
        {
            ChangedConfigurations += OnChangedConfigurations;
        }

        private void OnChangedConfigurations(object sender, ChangedConfigurationsEventArgs e)
        {
            //
        }

        /// <summary>
        /// Cho phép player hiển thị notes
        /// </summary>
        [XmlAttribute("not")]
        public bool NotesEnable
        {
            get { return _noteEnable; }
            set { _noteEnable = value; OnPropertyChanged("NotesEnable"); }
        }


        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new PageConfig()
            {
                AdvanceMode = AdvanceMode,
                NextButtonEnable = NextButtonEnable,
                PreviousButtonEnable = PreviousButtonEnable,
                PreviousSwipeEnable = PreviousSwipeEnable,
                NextSwipeEnable = NextSwipeEnable,
                RevisitMode = RevisitMode,
                SubmitButtonEnable = SubmitButtonEnable,
                PlayerFeaturesMode = PlayerFeaturesMode,
                MenuEnable = MenuEnable,
                GlosarryEnable = GlosarryEnable,
                SeekbarEnable = SeekbarEnable,
                ResourcesEnable = ResourcesEnable,
                NotesEnable = NotesEnable
            };
        }
    }

    public enum SlideAdvanceMode
    {
        Auto,
        User
    }

    public enum PlayerFeaturesMode
    {
        Default,
        Custom
    }
}

﻿
using System.Collections.Generic;
using System.Xml.Serialization;
using System;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.ViewModel;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp đối tượng layout
    /// </summary>
    [Serializable]
    [XmlRoot("layout")]
    public class PageLayer : RootModel
    {
        /// <summary>
        /// Danh sách các đối tượng con
        /// </summary>
        private List<ObjectElementBase> _children = null;
        private bool _isSelected = false;
        private bool _isVisibled = false;
        private Image _thumbnailURI = null;
        private bool _isEnabled = false;
        private ColorBase _background = null;
        private PageLayerConfig _setting = null;
        private Timing _timing = null;
        private List<TriggerableDataObjectBase> _triggerData;
        private bool _canShowInMenu = true;
        private string _themeLayoutOwnerID = null;
        /// <summary>
        /// Lấy danh sách đối tượng con
        /// </summary>
        [XmlArray("child")]
        public List<ObjectElementBase> Children
        {
            get
            {
                return _children ?? (_children = new List<ObjectElementBase>());
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính đang được lựa chọn
        /// </summary>
        [XmlIgnore]
        public bool IsSelected { get => _isSelected; set => _isSelected = value; }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính có được hiển thị hay không
        /// </summary>
        [XmlAttribute("visibile")]
        public bool IsVisibled { get => _isVisibled; set => _isVisibled = value; }

        /// <summary>
        /// Đường dẫn chứa tập tin hình ảnh
        /// </summary>
        [XmlElement("thumb")]
        public Image ThumbnailURI { get => _thumbnailURI; set => _thumbnailURI = value; }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính đang khóa cho đối tượng
        /// </summary>
        [XmlAttribute("enable")]
        public bool IsEnabled { get => _isEnabled; set => _isEnabled = value; }

        /// <summary>
        /// Màu nền cho layout
        /// </summary>
        [XmlElement("bg")]
        public ColorBase Background
        {
            get { return _background; }
            set { _background = value; OnPropertyChanged("Background"); }
        }
        //public ColorBase Background { get => _background; set => _background = value; }

        /// <summary>
        /// ID Layout được chọn
        /// </summary>
        [XmlAttribute("layid")]
        public string ThemeLayoutOwnerID { get => _themeLayoutOwnerID; set => _themeLayoutOwnerID = value; }

        /// <summary>
        /// Cài đặt cấu hình cho Slide
        /// </summary>
        [XmlElement("set")]
        public PageLayerConfig Setting { get => _setting ?? (_setting = new PageLayerConfig()); set => _setting = value; }

        /// <summary>
        /// Cài đặt thời gian trình chiếu cho đối tượng
        /// </summary>
        [XmlElement("tim")]
        public Timing Timing
        {
            get
            {
                return _timing ?? (_timing = new Timing());
            }
            set => _timing = value;
        }

        /// <summary>
        /// Thuộc tính hiển thị/không hiển thị trên menu
        /// </summary>
        [XmlAttribute("cSm")]
        public bool CanShowInMenu { get => _canShowInMenu; set => _canShowInMenu = value; }

        [XmlArray("trigLst"), XmlArrayItem("trig")]
        public List<TriggerableDataObjectBase> TriggerData { get => _triggerData ?? (_triggerData = new List<TriggerableDataObjectBase>()); set => _triggerData = value; }
        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _result = new PageLayer();
            Copy(_result);
            return _result;
        }


        /// <summary>
        /// Sao chép dữ liệu
        /// </summary>
        /// <param name="target"></param>
        public virtual void Copy(PageLayer target)
        {
            target.Name = this.Name;
            foreach (var item in this.Children)
            {
                if (item != null)
                    target.Children.Add(item.Clone() as ObjectElementBase);
            }
            target.CanShowInMenu = this.CanShowInMenu;
            target.ID = this.ID;
            target.IsEnabled = this.IsEnabled;
            target.IsSelected = this.IsSelected;
            target.IsVisibled = this.IsVisibled;
            target.Timing = this.Timing?.Clone() as Timing;
            target.ThumbnailURI = this.ThumbnailURI?.Clone() as Image;
            target.Background = this.Background?.Clone() as ColorBase;
            target.TriggerData = new List<TriggerableDataObjectBase>();
            foreach (var item in TriggerData)
            {
                if (item is TriggerableDataObjectBase trigger)
                {
                    if (trigger.Clone() is TriggerableDataObjectBase clonedTrigger)
                    {
                        target.TriggerData.Add(clonedTrigger);
                    }
                }
            }
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaLoad(IMediaDeserialize mediSerialize)
        {
            if (mediSerialize != null)
            {
                this.ThumbnailURI?.IMediaLoad(mediSerialize);
                this.Background?.IMediaLoad(mediSerialize);
                foreach (var element in Children)
                {
                    element.IMediaLoad(mediSerialize);
                }
            }

        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaSave(IMediaSerialize mediSerialize)
        {
            if (mediSerialize != null)
            {
                this.ThumbnailURI?.IMediaSave(mediSerialize);
                this.Background?.IMediaSave(mediSerialize);
                foreach (var element in Children)
                {
                    if (element != null)
                        element.IMediaSave(mediSerialize);
                }
            }

        }
    }

    /// <summary>
    /// Giao diện cho dữ liệu
    /// </summary>
    public interface IEPlaceHolder { }
}

﻿

namespace INV.Elearning.Core.Model
{
    using INV.Elearning.Animations;
    using INV.Elearning.Core.Model.Theme;
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp trừu tượng cho một trang tài liệu chuẩn
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(NormalPage))]
    public abstract class PageElementBase : RootModel
    {
        private string _rID = string.Empty;

        private PageLayer _mainLayer;

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Layer chính cho đối tượng
        /// </summary>
        [XmlElement("main")]
        public PageLayer MainLayer
        {
            get { return _mainLayer; }
            set { _mainLayer = value; OnPropertyChanged("MainLayer"); }
        }

        private bool _isTitle;
        /// <summary>
        /// Có ẩn hiện khung title hay không
        /// </summary>
        [XmlAttribute("istit")]
        public bool IsTitle
        {
            get { return _isTitle; }
            set { _isTitle = value; OnPropertyChanged("IsTitle"); }
        }

        private bool _isFooters;
        /// <summary>
        /// Có hiện khung Footers hay không
        /// </summary>
        [XmlAttribute("isfoo")]
        public bool IsFooters
        {
            get { return _isFooters; }
            set { _isFooters = value; OnPropertyChanged("IsFooters"); }
        }

        private bool _isText;
        /// <summary>
        /// Có hiện khung text hay không
        /// </summary>
        [XmlAttribute("istext")]
        public bool IsText
        {
            get { return _isText; }
            set { _isText = value; }
        }

        private bool _isDate;
        /// <summary>
        /// Có hiện khung Date hay không
        /// </summary>
        [XmlAttribute("isdate")]
        public bool IsDate
        {
            get { return _isDate; }
            set { _isDate = value; }
        }

        private bool _isSlideNumber;
        /// <summary>
        /// Có hiện khung Slide Number hay không
        /// </summary>
        [XmlAttribute("isslidenumber")]
        public bool IsSlideNumber
        {
            get { return _isSlideNumber; }
            set { _isSlideNumber = value; }
        }


        private LayoutType _selectLayoutType;
        /// <summary>
        /// Kiểu Layout được chọn
        /// </summary>
        [XmlAttribute("slt")]
        public LayoutType SelectLayoutType
        {
            get { return _selectLayoutType; }
            set { _selectLayoutType = value; }
        }

        private string _idLayout;
        /// <summary>
        /// ID Của Layout đang được chọn
        /// </summary>
        [XmlAttribute("idL")]
        public string IDLayout
        {
            get { return _idLayout; }
            set { _idLayout = value; }
        }


        private bool _isSelected;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính đang được lựa chọn
        /// </summary>
        [XmlIgnore]
        public bool IsSelected
        {
            get => _isSelected; set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        private bool _canShowInMenu = true;
        /// <summary>
        /// Cờ trạng thái hiển thị/không hiển thị trên menu lúc xuất bản
        /// </summary>
        [XmlAttribute("cSm")]
        public bool CanShowInMenu
        {
            get { return _canShowInMenu; }
            set { _canShowInMenu = value; }
        }


        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính mã trang tài liệu
        /// </summary>
        [XmlAttribute("rID")]
        public string RID { get => _rID; set => _rID = value; }

        private ColorBase _background = null;
        private List<PageLayer> _pageLayers = null;
        private Transiton _transition;

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính màu nền
        /// </summary>
        [XmlElement("bgr")]
        public ColorBase Background { get => _background; set => _background = value; }

        /// <summary>
        /// Hiệu ứng chuyển động
        /// </summary>
        [XmlElement("transi")]
        public Transiton Transition { get => _transition ?? (_transition = new NoneTransition()); set => _transition = value; }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính các lớp hiển thị cho trang
        /// </summary>
        [XmlArray("layers")]
        public List<PageLayer> PageLayers { get => _pageLayers ?? (_pageLayers = new List<PageLayer>()); }

        /// <summary>
        /// Nội dung ghi chú
        /// </summary>
        [XmlElement("nt")]
        public string Note { get; set; }

        private bool _isHideBackground;
        /// <summary>
        /// Có hiện hình ảnh background hay không
        /// </summary>
        [XmlAttribute("isH")]
        public bool IsHideBackground
        {
            get { return _isHideBackground; }
            set { _isHideBackground = value; }
        }

        private bool _isFollowBackground = true;
        [XmlAttribute("isFol")]
        public bool IsFollowBackground
        {
            get { return _isFollowBackground; }
            set { _isFollowBackground = value; }
        }

        private string _textFooter;
        /// <summary>
        /// TextFooter
        /// </summary>
        [XmlAttribute("txtFoot")]
        public string TextFooter
        {
            get { return _textFooter; }
            set { _textFooter = value; }
        }



        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính config cho đối tượng
        /// </summary>
        public PageConfig PageConfig { get; set; }

        /// <summary>
        /// Sao chép dữ liệu
        /// </summary>
        /// <param name="target"></param>
        public virtual void Copy(PageElementBase target)
        {
            target.Background = this.Background?.Clone() as ColorBase;
            target.SelectLayoutType = SelectLayoutType;
            target.IDLayout = IDLayout;
            target.ID = this.ID;
            target.MainLayer = this.MainLayer?.Clone() as PageLayer;
            target.Name = this.Name;
            target.Transition = this.Transition?.Clone() as Transiton;
            target.PageConfig = this.PageConfig?.Clone() as PageConfig;
            target.CanShowInMenu = this.CanShowInMenu;
            target.Note = this.Note;
            target.IsDate = this.IsDate;
            target.IsFooters = this.IsFooters;
            target.IsSlideNumber = this.IsSlideNumber;
            target.IsText = this.IsText;
            target.IsTitle = this.IsTitle;
            target.IsHideBackground = this.IsHideBackground;
            target.IsFollowBackground = this.IsFollowBackground;
            target.TextFooter = this.TextFooter;
            this.PageLayers.ForEach(x => target.PageLayers.Add(x.Clone() as PageLayer));
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaLoad(IMediaDeserialize mediSerialize)
        {
            if (mediSerialize != null)
            {
                this.Background?.IMediaLoad(mediSerialize);
                this.MainLayer?.IMediaLoad(mediSerialize);
                foreach (var layer in this.PageLayers)
                {
                    layer.IMediaLoad(mediSerialize);
                }
            }
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaSave(IMediaSerialize mediSerialize)
        {
            if (mediSerialize != null)
            {
                this.Background?.IMediaSave(mediSerialize);
                this.MainLayer?.IMediaSave(mediSerialize);
                foreach (var layer in this.PageLayers)
                {
                    layer.IMediaSave(mediSerialize);
                }
            }
        }
    }
}

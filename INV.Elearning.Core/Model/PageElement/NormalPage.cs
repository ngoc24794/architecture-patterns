﻿using INV.Elearning.Core.Model.Theme;
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp đại diện cho một trang tài liệu bình thường <br/>
    /// Trang tài liệu sẽ chứa các đối tượng Hình ảnh, Văn bản, Video ...
    /// </summary>
    [Serializable]
    [XmlRoot("npage")]
    public class NormalPage : PageElementBase
    {
        /// <summary>
        /// Nhân đôi một đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _result = new NormalPage();
            base.Copy(_result);
            return _result;
        }
    }
}

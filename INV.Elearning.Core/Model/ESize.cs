﻿using System;
using System.Windows;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Kiểu đối tượng dữ liệu kích thước
    /// </summary>
    [Serializable]
    [XmlRoot("size")]
    public class ESize
    {
        private double _width;
        /// <summary>
        /// Kích thước chiều dài
        /// </summary>
        [XmlAttribute("w")]
        public double Width
        {
            get { return _width; }
            set { _width = value; }
        }

        private double _height;
        /// <summary>
        /// Kích thước chiều rộng
        /// </summary>
        [XmlAttribute("h")]
        public double Height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// Hàm khởi tạo
        /// </summary>
        public ESize() { }

        /// <summary>
        /// Hàm khởi tạo
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public ESize(double width, double height)
        {
            this.Width = width;
            this.Height = height;
        }

        /// <summary>
        /// Chuyển đổi giữa các dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Size Converter(ESize data)
        {
            if (data == null) return new Size(0, 0);
            return new Size(data.Width, data.Height);
        }

        /// <summary>
        /// Chuyển đổi giữa các dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ESize Converter(Size data)
        {
            return new ESize() { Width = data.Width, Height = data.Height };
        }
    }
}

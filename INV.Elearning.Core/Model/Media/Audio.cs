﻿

namespace INV.Elearning.Core.Model.Media
{
    using INV.Elearning.Core.Helper;
    using System;
    using System.Xml.Serialization;
    using static INV.Elearning.Core.Model.MediaContentType;

    /// <summary>
    /// Lớp lưu trữ dữ liệu Media
    /// </summary>
    [Serializable]
    [XmlRoot("audio")]
    public class Audio : IMedia
    {
        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new Audio()
            {
                Extention = this.Extention,
                Path = this.Path,
                FileSize = this.FileSize,
                MD5Hash = this.MD5Hash,
                OriginPath = this.OriginPath,
                Name = this.Name
            };
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public Audio()
        {

        }

        /// <summary>
        /// Hàm khởi tạo có tham số
        /// </summary>
        /// <param name="filePath">Đường dẫn tập tin hình ảnh</param>
        public Audio(string filePath, bool isReadProperties = false)
        {
            OpenFile(filePath, isReadProperties);
        }
        /// <summary>
        /// Mở thông tin của một tập tin
        /// </summary>
        /// <param name="filePath"></param>
        public void OpenFile(string filePath, bool isReadProperties)
        {
            Extention = FileHelper.GetExtension(filePath);
            if (isReadProperties)
            {
                long _size = 0;
                MD5Hash = FileHelper.GetMd5(filePath, out _size);
                FileSize = _size;
            }
            OriginPath = Path = filePath;
            Name = FileHelper.GetFileName(filePath);
            ContentType = Extention == "mid" ? AudioType.MID : Extention == "mp3" ? AudioType.MP3 : Extention == "mp4" ? AudioType.MP4:
                Extention == "wav" ? AudioType.WAV:
                Extention == "webbm" ? AudioType.WEBM
                : $"audio/{Extention}";
        }
    }
}

﻿
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    public abstract class IMedia : RootModel
    {
        private string _extention = string.Empty;
        private long _fileSize = 0;
        private string _originPath = string.Empty;
        private string _path = string.Empty;
        private string _md5Hash = string.Empty;
        private string _id = string.Empty;
        private string _rID = string.Empty;
        private string _contentType = string.Empty;

        /// <summary>
        /// Đuôi mở rộng của tập tin
        /// </summary>
        [XmlAttribute("ext")]
        public string Extention
        {
            get
            {
                return _extention;
            }

            set { _extention = value; }
        }

        /// <summary>
        /// Thông số kích thước
        /// </summary>
        [XmlAttribute("size")]
        public long FileSize
        {
            get
            {
                return _fileSize;
            }

            set { _fileSize = value; }
        }

        /// <summary>
        /// Mã MD5
        /// </summary>
        [XmlAttribute("md5")]
        public string MD5Hash
        {
            get
            {
                return _md5Hash;
            }

            set { _md5Hash = value; }
        }

        /// <summary>
        /// Đường dẫn tập tin gốc
        /// </summary>
        [XmlAttribute("orgPath")]
        public string OriginPath
        {
            get
            {
                return _originPath;
            }

            set
            {
                _originPath = value;
            }
        }

        /// <summary>
        /// Đường dẫn hiện tại
        /// </summary>
        [XmlAttribute("path")]
        public string Path
        {
            get
            {
                return _path;
            }

            set
            {
                _path = value;
            }
        }

        /// <summary>
        /// Mã quan hệ
        /// </summary>
        [XmlAttribute("rId")]
        public string RID
        {
            get
            {
                return _rID;
            }

            set
            {
                _rID = value;
            }
        }

        /// <summary>
        /// Nội dung loại tập tin
        /// </summary>
        [XmlAttribute("fType")]
        public string ContentType { get => _contentType; set => _contentType = value; }

        /// <summary>
        /// Phương thức tải lại dữ liệu từ gói
        /// </summary>
        /// <param name="mediaSerialize"></param>
        public override void IMediaLoad(IMediaDeserialize mediaSerialize)
        {
            if (mediaSerialize != null)
            {
                mediaSerialize.Invoke(this);
            }
        }

        /// <summary>
        /// Phương thức lưu trữ dữ liệu xuống gói
        /// </summary>
        /// <param name="mediaSerialize"></param>
        public override void IMediaSave(IMediaSerialize mediaSerialize)
        {
            if (mediaSerialize != null)
            {
                mediaSerialize.Invoke(this);
            }
        }
    }

    /// <summary>
    /// Lớp lưu trữ thông tin các loại tập tin
    /// </summary>
    public class MediaContentType
    {
        private const string Image = "image";
        private const string Video = "video";
        private const string Audio = "audio";
        private const string App = "application";

        /// <summary>
        /// Tập tin hình ảnh
        /// </summary>
        public class ImageType
        {
            public const string JPEG = Image + "/jpeg";
            public const string PNG = Image + "/png";
            public const string BITMAP = Image + "/bmp";
            public const string GIFF = Image + "/gif";
            public const string TIFF = Image + "/tiff";
        }

        /// <summary>
        /// Tập tin video
        /// </summary>
        public class VideoType
        {
            public const string AVI = Video + "/x-msvideo";
            public const string MP4 = Video + "/mp4";
            public const string WMV = Video + "/x-ms-wmv";
        }

        /// <summary>
        /// Tập tin âm thanh
        /// </summary>
        public class AudioType
        {
            public const string MP3 = Audio + "/mpeg";
            public const string MP4 = Audio + "/mp4";
            public const string WAV = Audio + "/wav";
            public const string MID = Audio + "/mid";
            public const string WEBM = Audio + "/webm";
        }

        /// <summary>
        /// Tập tin khác
        /// </summary>
        public class ApplicationType
        {
            public const string FLASH = App + "/x-shockwave-flash";
        }
    }
}

﻿

namespace INV.Elearning.Core.Model
{
    using INV.Elearning.Core.Helper;
    using System;
    using System.IO;
    using System.Xml.Serialization;
    using static INV.Elearning.Core.Model.MediaContentType;

    /// <summary>
    /// Lớp lưu trữ dữ liệu Media
    /// </summary>
    [Serializable]
    [XmlRoot("img")]
    public class Image : IMedia
    {
        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new Image()
            {
                Extention = this.Extention,
                Path = this.Path,
                FileSize = this.FileSize,
                MD5Hash = this.MD5Hash,
                OriginPath = this.OriginPath,
                Name = this.Name
            };
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public Image()
        {

        }

        /// <summary>
        /// Hàm khởi tạo có tham số
        /// </summary>
        /// <param name="filePath">Đường dẫn tập tin hình ảnh</param>
        public Image(string filePath, bool isReadProperties = false)
        {
            OpenFile(filePath, isReadProperties);
        }
        /// <summary>
        /// Mở thông tin của một tập tin
        /// </summary>
        /// <param name="filePath"></param>
        public void OpenFile(string filePath, bool isReadProperties)
        {
            //Tien 03-08-2018
            if (!File.Exists(filePath) && !filePath.Contains("pack://application:,,,/INV.Elearning.Resources;component/Resources/Images/Texture/")) return;
            Extention = FileHelper.GetExtension(filePath);
            if (isReadProperties)
            {
                long _size = 0;
                MD5Hash = FileHelper.GetMd5(filePath, out _size);
                FileSize = _size;
            }
            OriginPath = Path = filePath;
            Name = FileHelper.GetFileName(filePath);
            ContentType = Extention == "jpeg" ? ImageType.JPEG : Extention == "jpg" ? ImageType.JPEG : Extention == "png" 
                ? ImageType.PNG : Extention == "bmp" ? ImageType.BITMAP : Extention == "gif" ? ImageType.GIFF : $"image/{Extention}";
        }
    }
}

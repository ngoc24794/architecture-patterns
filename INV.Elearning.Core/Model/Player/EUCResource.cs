﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("eucres")]
    public class EUCResource : RootModel
    {
        private ObservableCollection<EResource> _eResources;
        /// <summary>
        /// Danh sách EResources
        /// </summary>
        [XmlArray("liseres"), XmlArrayItem("res")]
        public ObservableCollection<EResource> EResources
        {
            get { return _eResources ?? (_eResources = new ObservableCollection<EResource>()); }
            set { _eResources = value; }
        }

        private string _description;
        /// <summary>
        /// Description của Resource
        /// </summary>
        [XmlAttribute("des")]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public override IElearningElement Clone()
        {
            EUCResource eUCResource = new EUCResource();
            foreach (EResource item in EResources)
            {
                eUCResource.EResources.Add(item.Clone() as EResource);
            }
            eUCResource.Description = Description;
            return eUCResource;
        }
    }
}

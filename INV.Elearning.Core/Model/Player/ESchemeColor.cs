﻿using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Player;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("escheme")]
    public class ESchemeColor : RootModel
    {
        private string _groupName;
        /// <summary>
        /// GroupName
        /// </summary>
        [XmlAttribute("grColor")]
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        private bool _isCustom;
        /// <summary>
        /// Kiểm tra xem màu có phải là do người dùng thêm hay không
        /// </summary>
        [XmlAttribute("isCus")]
        public bool IsCustom
        {
            get { return _isCustom; }
            set { _isCustom = value; }
        }



        private string _nameSchemeColor;
        /// <summary>
        /// Têm của EScheme
        /// </summary>
        [XmlAttribute("nameSce")]
        public string NameSchemeColor
        {
            get { return _nameSchemeColor; }
            set { _nameSchemeColor = value; }
        }

        private ObservableCollection<EBaseColor> _colorSchemes;
        /// <summary>
        /// Danh sách màu
        /// </summary>
        [XmlArray("listSchemeCol"),XmlArrayItem("cols")]
        public ObservableCollection<EBaseColor> ColorSchemes
        {
            get { return _colorSchemes ?? (_colorSchemes = new ObservableCollection<EBaseColor>()); }
            set { _colorSchemes = value; }
        }

        private ESchemeColor _selectedScheme;
        [XmlElement("sele")]
        public ESchemeColor SelectedScheme
        {
            get { return _selectedScheme; }
            set { _selectedScheme = value; }
        }

        private ETemplateColor _templatecolor;
        /// <summary>
        /// Template Color
        /// </summary>
        [XmlElement("templat")]
        public ETemplateColor TemplateColor
        {
            get { return _templatecolor ?? (_templatecolor = new ETemplateColor()); }
            set { _templatecolor = value; }
        }


        public override IElearningElement Clone()
        {
            ESchemeColor eSchemeColor = new ESchemeColor();
            eSchemeColor.NameSchemeColor = NameSchemeColor;
            eSchemeColor.GroupName = GroupName;
            eSchemeColor.IsCustom = IsCustom;
            foreach (EBaseColor item in ColorSchemes)
            {
                eSchemeColor.ColorSchemes.Add(item.Clone() as EBaseColor);
            }
            eSchemeColor.SelectedScheme = SelectedScheme;
            eSchemeColor.TemplateColor = TemplateColor;
            return eSchemeColor;
        }
    }
}

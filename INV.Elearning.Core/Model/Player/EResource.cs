﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("eRes")]
    public class EResource : RootModel
    {
        private string _title;
        /// <summary>
        /// Tiêu đề Resources
        /// </summary>
        [XmlAttribute("tit")]
        public string Title
        {
            get { return _title; }
            set { _title = value;  }
        }

        private bool _isURL = true;
        /// <summary>
        /// Có URL hay không
        /// </summary>
        [XmlAttribute("isUrl")]
        public bool IsURL
        {
            get { return _isURL; }
            set
            {
                _isURL = value;            
            }
        }

        private string _url = "";
        /// <summary>
        /// Đường dẫn URL
        /// </summary>
        [XmlAttribute("url")]
        public string URL
        {
            get { return _url; }
            set { _url = value;  }
        }

        private bool _isFile;
        /// <summary>
        /// Có File hay ko
        /// </summary>
        [XmlAttribute("isFile")]
        public bool IsFile
        {
            get { return _isFile; }
            set
            {
                _isFile = value;
            
            }
        }

        private string _file;
        /// <summary>
        /// Đường dẫn File
        /// </summary>
        [XmlAttribute("file")]
        public string File
        {
            get { return _file; }
            set { _file = value; }
        }

        public void Copy(EResource target)
        {
            target.Title = Title;
            target.IsFile = IsFile;
            target.File = File;
            target.IsURL = IsURL;
            target.URL = URL;
        }

        public override IElearningElement Clone()
        {
            EResource eResource = new EResource();
            Copy(eResource);
            return eResource;
        }
    }
}

﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("ucbase")]
    public class EUCBaseColor : RootModel
    {
        private ObservableCollection<ESchemeColor> eSchemeColors;
        /// <summary>
        /// Danh sách màu
        /// </summary>
        [XmlArray("lstColor"),XmlArrayItem("cols")]
        public ObservableCollection<ESchemeColor> ESchemeColors
        {
            get { return eSchemeColors ?? (eSchemeColors = new ObservableCollection<ESchemeColor>()); }
            set { eSchemeColors = value; }
        }

        private ESchemeColor _selectedScheme;
        /// <summary>
        /// Màu được chọn
        /// </summary>
        [XmlElement("select")]
        public ESchemeColor SelectedScheme
        {
            get { return _selectedScheme; }
            set { _selectedScheme = value; }
        }


        public override IElearningElement Clone()
        {
            EUCBaseColor eUCBaseColor = new EUCBaseColor();
            foreach (ESchemeColor item in ESchemeColors)
            {
                eUCBaseColor.ESchemeColors.Add(item.Clone() as ESchemeColor);
            }
            eUCBaseColor.SelectedScheme = SelectedScheme;
            return eUCBaseColor;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model.Player
{
    [Serializable]
    [XmlRoot("funccata")]
    public class EFunctionCatalog : RootModel
    {
        private string _name;
        /// <summary>
        /// NameCatalog
        /// </summary>
        [XmlAttribute("nameCata")]
        public string NameCatalog
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _index;
        /// <summary>
        /// Index
        /// </summary>
        [XmlAttribute("Ind")]
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }

        private bool _selected;
        /// <summary>
        /// Selected
        /// </summary>
        [XmlAttribute("selec")]
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        private string _pathData;
        /// <summary>
        /// PathData
        /// </summary>
        [XmlAttribute("pd")]
        public string PathData
        {
            get { return _pathData; }
            set { _pathData = value; }
        }

        private string _keyLanguage;
        /// <summary>
        /// Key Language
        /// </summary>
        [XmlAttribute("kLang")]
        public string KeyLanguage
        {
            get { return _keyLanguage; }
            set { _keyLanguage = value; }
        }


        public void Copy(EFunctionCatalog target)
        {
            target.NameCatalog = NameCatalog;
            target.Selected = Selected;
            target.Index = Index;
            target.PathData = PathData;
            target.KeyLanguage = KeyLanguage;
        }


        public override IElearningElement Clone()
        {
            EFunctionCatalog eFunction = new EFunctionCatalog();
            Copy(eFunction);
            return eFunction;
        }
    }
}

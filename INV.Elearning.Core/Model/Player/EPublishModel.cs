﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// EnumDurationType
    /// </summary>
    public enum DurationType
    {
        CaculateAutomatically,
        Custom
    }
    /// <summary>
    /// EnumDateTime
    /// </summary>
    public enum DateType
    {
        LastPublishes,
        Custom
    }

    /// <summary>
    /// Enum của LMS Output
    /// </summary>
    public enum LMSOutput
    {
        SCORM12,
        SCORM2004,
        TinCanAPI
    }

    /// <summary>
    /// Kiểu public
    /// </summary>
    public enum PublishType
    {
        Web,
        LMS,
        CD
    }

    /// <summary>
    /// Enum trạng thái LMSStatus
    /// </summary>
    public enum LMSReportStatus
    {
        PassedIncomplete,
        PassedFailed,
        CompleteIncomplete,
        CompleteFailed
    }

    /// <summary>
    /// Enum Edition
    /// </summary>
    public enum LMSEdition
    {
        SecondEdition,
        ThirdEdition,
        FourthEdition
    }

    /// <summary>
    /// Enum tracking
    /// </summary>
    public enum TrackingStatus
    {
        NumberOfSlides,
        QuizResult
    }

    [Serializable]
    [XmlRoot("pubmo")]
    public class EPublishModel : RootModel
    {
        private string _title;
        /// <summary>
        /// Title của Web
        /// </summary>
        [XmlAttribute("tit")]
        public string Title
        {
            get { return _title; }
            set { _title = value; OnPropertyChanged("Title"); }
        }

        private string _folder;
        /// <summary>
        /// Folder của Web
        /// </summary>
        [XmlAttribute("fol")]
        public string Folder
        {
            get { return _folder; }
            set { _folder = value; OnPropertyChanged("Folder"); }
        }

        private string _description;
        /// <summary>
        /// Description của Web
        /// </summary>
        [XmlAttribute("des")]
        public string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged("Description"); }
        }

        private string _author;
        /// <summary>
        /// Author
        /// </summary>
        [XmlAttribute("aut")]
        public string Author
        {
            get { return _author; }
            set { _author = value; OnPropertyChanged("Author"); }
        }

        private string _identifier;
        /// <summary>
        /// Identifier
        /// </summary>
        [XmlAttribute("ide")]
        public string Identifier
        {
            get { return _identifier; }
            set { _identifier = value; OnPropertyChanged("Identifier"); }
        }

        private string _keyWord;
        /// <summary>
        /// KeyWord
        /// </summary>
        [XmlAttribute("key")]
        public string KeyWord
        {
            get { return _keyWord; }
            set { _keyWord = value; OnPropertyChanged("KeyWord"); }
        }

        private string _email;
        /// <summary>
        /// Email
        /// </summary>
        [XmlAttribute("ema")]
        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }

        private string _webSite;
        /// <summary>
        /// Website
        /// </summary>
        [XmlAttribute("web")]
        public string Website
        {
            get { return _webSite; }
            set { _webSite = value; OnPropertyChanged("Website"); }
        }

        private string _duration;
        /// <summary>
        /// Duration
        /// </summary>
        [XmlAttribute("dur")]
        public string Duration
        {
            get { return _duration; }
            set { _duration = value; OnPropertyChanged("Duration"); }
        }

        private DurationType durationType;
        [XmlAttribute("durt")]
        public DurationType DurationType
        {
            get { return durationType; }
            set
            {
                durationType = value;
                OnPropertyChanged("DurationType");
            }
        }


        private string _date;
        /// <summary>
        /// Date
        /// </summary>
        [XmlAttribute("dat")]
        public string Date
        {
            get { return _date; }
            set { _date = value; OnPropertyChanged("Date"); }
        }

        private DateType dateType;
        [XmlAttribute("datt")]
        public DateType DateType
        {
            get { return dateType; }
            set
            {
                dateType = value;
                OnPropertyChanged("DateType");
            }
        }


        private string _version;
        /// <summary>
        /// Version
        /// </summary>
        [XmlAttribute("ver")]
        public string Version
        {
            get { return _version; }
            set { _version = value; OnPropertyChanged("Version"); }
        }

        private string _imgName;
        /// <summary>
        /// ImgName
        /// </summary>
        [XmlAttribute("imgname")]
        public string ImgName
        {
            get { return _imgName; }
            set { _imgName = value; OnPropertyChanged("ImgName"); }
        }


        private Image _imgPath;
        /// <summary>
        /// Đường dẫn IMG
        /// </summary>
        [XmlElement("img")]
        public Image ImgPath
        {
            get { return _imgPath ?? (_imgPath = new Image()); }
            set { _imgPath = value; OnPropertyChanged("ImgPath"); }
        }

        private PublishType _publishType;
        /// <summary>
        /// Kiểu publish
        /// </summary>
        [XmlAttribute("pubty")]
        public PublishType PublishType
        {
            get { return _publishType; }
            set { _publishType = value; OnPropertyChanged("PublishType"); }
        }

        private LMSOutput lMSOutput;
        /// <summary>
        /// Kiểu LMS Output
        /// </summary>
        [XmlAttribute("lmsOut")]
        public LMSOutput LMSOutput
        {
            get { return lMSOutput; }
            set { lMSOutput = value; OnPropertyChanged("LMSOutput"); }
        }

        private string _versionLMSCourse;
        /// <summary>
        /// Version LMSCourse
        /// </summary>
        [XmlAttribute("verLMS")]
        public string VersionLMSCourse
        {
            get { return _versionLMSCourse; }
            set { _versionLMSCourse = value; OnPropertyChanged("VersionLMSCourse"); }
        }

        private string _durationLMSCourse;
        /// <summary>
        /// Duration LMS Course
        /// </summary>
        [XmlAttribute("durLMS")]
        public string DurationLMSCourse
        {
            get { return _durationLMSCourse; }
            set { _durationLMSCourse = value; OnPropertyChanged("DurationLMSCourse"); }
        }

        private string _titleLMSLesson;
        /// <summary>
        /// LMS Lesson Course Title
        /// </summary>
        [XmlAttribute("titLMS")]
        public string TitleLMSLesson
        {
            get { return _titleLMSLesson; }
            set { _titleLMSLesson = value; OnPropertyChanged("TitleLMSLesson"); }
        }

        private string _identifierLMSLesson;
        /// <summary>
        /// LMS Lesson Identifier
        /// </summary>
        [XmlAttribute("idLMS")]
        public string IdentifierLMSLesson
        {
            get { return _identifierLMSLesson; }
            set { _identifierLMSLesson = value; OnPropertyChanged("IdentifierLMSLesson"); }
        }


        private LMSReportStatus _lmsReportStatus;
        /// <summary>
        /// LMS Report Status
        /// </summary>
        [XmlAttribute("llmsts")]
        public LMSReportStatus LMSReportStatus
        {
            get { return _lmsReportStatus; }
            set { _lmsReportStatus = value; OnPropertyChanged("LMSReportStatus"); }
        }


        private LMSEdition _lmsEdition;
        /// <summary>
        /// LMS Edition
        /// </summary>
        [XmlAttribute("lmsedi")]
        public LMSEdition LMSEdition
        {
            get { return _lmsEdition; }
            set { _lmsEdition = value; OnPropertyChanged("LMSEdition"); }
        }

        private string _launchURl;
        /// <summary>
        /// Launch URL
        /// </summary>
        [XmlAttribute("url")]
        public string LaunchURL
        {
            get { return _launchURl; }
            set { _launchURl = value; OnPropertyChanged("LaunchURL"); }
        }

        private string _creator;
        /// <summary>
        /// Creator
        /// </summary>
        [XmlAttribute("cre")]
        public string Creator
        {
            get { return _creator; }
            set { _creator = value; OnPropertyChanged("Creator"); }
        }

        private string _fileNameURL;
        /// <summary>
        /// FileNameURL
        /// </summary>
        [XmlAttribute("fina")]
        public string FileNameURL
        {
            get { return _fileNameURL; }
            set { _fileNameURL = value; OnPropertyChanged("FileNameURL"); }
        }

        private bool _isCustomQuality;
        /// <summary>
        /// Có thiết đặt qualitycustom hay không
        /// </summary>
        [XmlAttribute("icq")]
        public bool IsCustomQuality
        {
            get { return _isCustomQuality; }
            set
            {
                _isCustomQuality = value;
                //if (!value)
                //{
                //    VideoQuality = 5;
                //    ImageQuality = 80;
                //    AudioBitrate = 56;
                //}
                OnPropertyChanged("IsCustomQuality");
            }
        }

        private int _videoQuality = 5;
        /// <summary>
        /// Chất lượng video
        /// </summary>
        [XmlAttribute("vq")]
        public int VideoQuality
        {
            get { return _videoQuality; }
            set { _videoQuality = value; OnPropertyChanged("VideoQuality"); }
        }

        private int _audioBitrate = 56;
        /// <summary>
        /// Audio Bitrate
        /// </summary>
        [XmlAttribute("ab")]
        public int AudioBitrate
        {
            get { return _audioBitrate; }
            set { _audioBitrate = value; OnPropertyChanged("AudioBitrate"); }
        }

        private int _imageQuality = 80;
        /// <summary>
        /// Chất lượng hình ảnh
        /// </summary>
        [XmlAttribute("iq")]
        public int ImageQuality
        {
            get { return _imageQuality; }
            set { _imageQuality = value; OnPropertyChanged("ImageQuality"); }
        }

        private bool _isOptimizeAudioVolune;
        /// <summary>
        /// IsOptimizeAudioVolume
        /// </summary>
        [XmlAttribute("ioav")]
        public bool IsOptimizeAudioVolume
        {
            get { return _isOptimizeAudioVolune; }
            set { _isOptimizeAudioVolune = value; OnPropertyChanged("IsOptimizeAudioVolume"); }
        }

        private int _numberOfSlides;
        /// <summary>
        /// Số slide tối thiểu có thể xem khi xuất dạng SCORM
        /// </summary>
        [XmlAttribute("nos")]
        public int NumberOfSlides
        {
            get { return _numberOfSlides; }
            set { _numberOfSlides = value; OnPropertyChanged("NumberOfSlides"); }
        }

        private string _idResultSlide;
        /// <summary>
        /// Id của result SLide được chọn
        /// </summary>
        [XmlAttribute("idresu")]
        public string IDResultSlide
        {
            get { return _idResultSlide; }
            set { _idResultSlide = value; OnPropertyChanged("IDResultSlide"); }
        }

        private TrackingStatus _trackingStatus;
        /// <summary>
        /// Trạng thái của tracking
        /// </summary>
        [XmlAttribute("trs")]
        public TrackingStatus TrackingStatus
        {
            get { return _trackingStatus; }
            set { _trackingStatus = value; OnPropertyChanged("TrackingStatus"); }
        }

        private bool _isPublishModelChanged = false;
        /// <summary>
        /// Kiểm tra xem có thay đổi giá trị của xuất bản hay không
        /// </summary>
        [XmlAttribute("ipmc")]
        public bool IsPublishModelChanged
        {
            get { return _isPublishModelChanged; }
            set { _isPublishModelChanged = value; OnPropertyChanged("IsPublishModelChanged"); }
        }

        /// <summary>
        /// Hàm copy dữ liệu
        /// </summary>
        /// <param name="target"></param>
        public virtual void Copy(EPublishModel target)
        {
            target.Title = Title;
            target.Folder = Folder;
            target.Author = Author;
            target.Date = Date;
            target.Description = Description;
            target.Duration = Duration;
            target.Email = Email;
            target.ID = ID;
            target.Identifier = Identifier;
            target.KeyWord = KeyWord;
            target.Name = Name;
            target.Version = Version;
            target.Website = Website;
            target.DurationType = DurationType;
            target.DateType = DateType;
            target.PublishType = PublishType;
            target.LMSOutput = LMSOutput;
            target.VersionLMSCourse = VersionLMSCourse;
            target.DurationLMSCourse = DurationLMSCourse;
            target.LMSReportStatus = LMSReportStatus;
            target.LMSEdition = LMSEdition;
            target.TitleLMSLesson = TitleLMSLesson;
            target.IdentifierLMSLesson = IdentifierLMSLesson;
            target.LaunchURL = LaunchURL;
            target.Creator = Creator;
            target.FileNameURL = FileNameURL;
            target.ImgName = ImgName;
            target.ImgPath = ImgPath.Clone() as Image;
            target.IsCustomQuality = IsCustomQuality;
            target.VideoQuality = VideoQuality;
            target.AudioBitrate = AudioBitrate;
            target.ImageQuality = ImageQuality;
            target.IsOptimizeAudioVolume = IsOptimizeAudioVolume;
            target.NumberOfSlides = NumberOfSlides;
            target.IDResultSlide = IDResultSlide;
            target.TrackingStatus = TrackingStatus;
            target.IsPublishModelChanged = IsPublishModelChanged;
        }

        public override void IMediaLoad(IMediaDeserialize mediaSerialize)
        {
            base.IMediaLoad(mediaSerialize);
            ImgPath?.IMediaLoad(mediaSerialize);
        }

        public override void IMediaSave(IMediaSerialize mediaSerialize)
        {
            base.IMediaSave(mediaSerialize);
            ImgPath?.IMediaSave(mediaSerialize);
        }
        public override IElearningElement Clone()
        {
            var _result = new EPublishModel();
            this.Copy(_result);
            return _result;
        }
    }
}

﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("ucSav")]
    public class UCSaveColor : RootModel
    {
        private ESchemeColor eSchemeColor;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("escheme")]
        public ESchemeColor ESchemeColor
        {
            get { return eSchemeColor; }
            set { eSchemeColor = value; }
        }

        private bool _isAccepted;
        [XmlAttribute("isacc")]
        public bool IsAccepted
        {
            get { return _isAccepted; }
            set { _isAccepted = value; }
        }

        public void Copy(UCSaveColor uCSaveColor)
        {
            uCSaveColor.ESchemeColor = ESchemeColor;
            uCSaveColor.IsAccepted = IsAccepted;
        }

        public override IElearningElement Clone()
        {
            UCSaveColor uCSaveColor = new UCSaveColor();
            Copy(uCSaveColor);
            return uCSaveColor;
        }
    }
}

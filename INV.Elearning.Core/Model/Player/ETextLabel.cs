﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{

    [Serializable]
    [XmlRoot("eText")]
    public class ETextLabel : RootModel
    {
        private int _id;
        [XmlAttribute("Id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _buttonMessages;
        [XmlAttribute("btnmes")]
        public string ButtonMessages
        {
            get { return _buttonMessages; }
            set { _buttonMessages = value; }
        }

        private string _customText;
        [XmlAttribute("custxt")]
        public string CustomText
        {
            get { return _customText; }
            set { _customText = value; }
        }

        public void Copy(ETextLabel eTextLabel)
        {
            eTextLabel.ButtonMessages = ButtonMessages;
            eTextLabel.CustomText = CustomText;
            eTextLabel.Id = Id;
        }

        public override IElearningElement Clone()
        {
            ETextLabel eTextLabel = new ETextLabel();
            Copy(eTextLabel);
            return eTextLabel;
        }
    }
}

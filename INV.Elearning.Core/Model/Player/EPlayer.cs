﻿using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("epl")]
    public class EPlayer : RootModel
    {
        private string _rid;
        /// <summary>
        /// RID
        /// </summary>
        [XmlAttribute("rid")]
        public string RID
        {
            get { return _rid; }
            set { _rid = value; }
        }


        private EFeatures _efeatures;
        /// <summary>
        /// Dữ liệu Features
        /// </summary>
        [XmlElement("efea")]
        public EFeatures EFeatures
        {
            get { return _efeatures ?? (_efeatures = new EFeatures()); }
            set { _efeatures = value; }
        }

        private EUCGlosarry _eUCGlossary;
        /// <summary>
        /// Dữ liệu Glossary
        /// </summary>
        [XmlElement("eucglo")]
        public EUCGlosarry EUCGlossary
        {
            get { return _eUCGlossary ?? (_eUCGlossary = new EUCGlosarry()); }
            set { _eUCGlossary = value; }
        }

        private EUCResource _eUCResource;
        /// <summary>
        /// Dữ liệu Resource
        /// </summary>
        [XmlElement("eucres")]
        public EUCResource EUCResource
        {
            get { return _eUCResource ?? (_eUCResource = new EUCResource()); }
            set { _eUCResource = value; }
        }

        private EUCTextLabel _eUCTextLabel;
        /// <summary>
        /// Dữ liệu Text Label
        /// </summary>
        [XmlElement("euctext")]
        public EUCTextLabel EUCTextLabel
        {
            get { return _eUCTextLabel ?? (_eUCTextLabel = new EUCTextLabel()); }
            set { _eUCTextLabel = value; }
        }

        private EUCBaseColor eUCBaseColor;
        /// <summary>
        /// Dữ liệu Color
        /// </summary>
        [XmlElement("EUcColor")]
        public EUCBaseColor EUCBaseColor
        {
            get { return eUCBaseColor ?? (eUCBaseColor = new EUCBaseColor()); }
            set { eUCBaseColor = value; }
        }

        private UCSaveColor _ucSaveColor;
        /// <summary>
        /// Dữ liệu Save Color
        /// </summary>
        [XmlElement("ucsave")]
        public UCSaveColor UCSaveColor
        {
            get { return _ucSaveColor ?? (_ucSaveColor = new UCSaveColor()); }
            set { _ucSaveColor = value; }
        }

        private EPublishModel ePublishModel;
        /// <summary>
        /// Dữ liệu publish
        /// </summary>
        [XmlElement("epub")]
        public EPublishModel EPublishModel
        {
            get { return ePublishModel ?? (ePublishModel = new EPublishModel()); }
            set { ePublishModel = value; }
        }

        


        public void Copy(EPlayer target)
        {
            target.EFeatures = EFeatures.Clone() as EFeatures;
            target.EUCGlossary = EUCGlossary.Clone() as EUCGlosarry;
            target.EUCResource = EUCResource.Clone() as EUCResource;
            target.EUCTextLabel = EUCTextLabel.Clone() as EUCTextLabel;
            target.EUCBaseColor = EUCBaseColor.Clone() as EUCBaseColor;
            target.UCSaveColor = UCSaveColor.Clone() as UCSaveColor;
            target.EPublishModel = EPublishModel.Clone() as EPublishModel;
        }

        public override void IMediaLoad(IMediaDeserialize mediaSerialize)
        {
            base.IMediaLoad(mediaSerialize);
            EFeatures.ImgLogo.IMediaLoad(mediaSerialize);
        }

        public override void IMediaSave(IMediaSerialize mediaSerialize)
        {
            base.IMediaSave(mediaSerialize);
            EFeatures.ImgLogo.IMediaSave(mediaSerialize);
        }

        public override IElearningElement Clone()
        {
            EPlayer ePlayer = new EPlayer();
            Copy(ePlayer);
            return ePlayer;
        }
    }
}

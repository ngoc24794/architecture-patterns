﻿using INV.Elearning.Core.Model;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    public enum GroupName
    {
        Main,
        TopBottomBar,
        ButtonInactiveTab,
        SidebarPopups,
        SeekbarVolumeControl
    }

    [Serializable]
    [XmlRoot("ebacol")]
    public class EBaseColor : RootModel
    {
        private string _keyColor;
        /// <summary>
        /// Key của Color
        /// </summary>
        /// 
        [XmlAttribute("key")]
        public string KeyColor
        {
            get { return _keyColor; }
            set { _keyColor = value; }
        }

        private string _nameColor;
        /// <summary>
        /// Tên của Color
        /// </summary>
        /// 
        [XmlAttribute("namcol")]
        public string NameColor
        {
            get { return _nameColor; }
            set { _nameColor = value; }
        }

        private string _groupNames;
        /// <summary>
        /// Tên Group Color
        /// </summary>
        /// 
        [XmlAttribute("grna")]
        public string GroupNames
        {
            get { return _groupNames; }
            set { _groupNames = value; }
        }


        private string _presetName;
        /// <summary>
        /// Biến PresetName lấy dữ liệu từ ô TextBlock
        /// </summary>
        /// 
        [XmlAttribute("pres")]
        public string PresetName
        {
            get { return _presetName; }
            set { _presetName = value; }
        }

        public void Copy(EBaseColor eBaseColor)
        {
            eBaseColor.KeyColor = KeyColor;
            eBaseColor.NameColor = NameColor;
            eBaseColor.GroupNames = GroupNames;
            eBaseColor.PresetName = PresetName;
        }
        public override IElearningElement Clone()
        {
            EBaseColor eBaseColor = new EBaseColor();
            Copy(eBaseColor);
            return eBaseColor;
        }
    }
}

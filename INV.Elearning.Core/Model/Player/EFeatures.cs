﻿using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Player;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("efea")]
    public class EFeatures : RootModel
    {
        //private string _imgPath;
        ///// <summary>
        ///// Đường dẫn Image
        ///// </summary>
        //[XmlAttribute("img")]
        //public string ImgPath
        //{
        //    get { return _imgPath; }
        //    set { _imgPath = value; }
        //}

        private bool _isTitleEnabled;
        /// <summary>
        /// Ẩn hiện title
        /// </summary>
        [XmlAttribute("isTit")]
        public bool IsTitleEnabled
        {
            get { return _isTitleEnabled; }
            set { _isTitleEnabled = value; }
        }

        private string _title;
        /// <summary>
        /// Cài đặt nội dung Title
        /// </summary>
        [XmlAttribute("tit")]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private bool _isSubjectEnabled;
        /// <summary>
        /// Ẩn hiện subject
        /// </summary>
        [XmlAttribute("isSub")]
        public bool IsSubjectEnable
        {
            get { return _isSubjectEnabled; }
            set { _isSubjectEnabled = value; }
        }

        private string _subject;
        /// <summary>
        /// Cài đặt nội dung subject
        /// </summary>
        [XmlAttribute("sub")]
        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }


        private bool _isWriterEnabled;
        /// <summary>
        /// Ẩn hiện người biên soạn
        /// </summary>
        [XmlAttribute("isWri")]
        public bool IsWriterEnabled
        {
            get { return _isWriterEnabled; }
            set { _isWriterEnabled = value; }
        }

        private string _writer;
        /// <summary>
        /// Cài đặt nội dung người biên soạn
        /// </summary>
        [XmlAttribute("wri")]
        public string Writer
        {
            get { return _writer; }
            set { _writer = value; }
        }

        private bool _isSearchEnabled;
        /// <summary>
        /// Ẩn hiện thanh search
        /// </summary>
        [XmlAttribute("isSea")]
        public bool IsSearchEnabled
        {
            get { return _isSearchEnabled; }
            set { _isSearchEnabled = value; }
        }

        private string _search;
        /// <summary>
        /// Cài đặt nội dung thanh search
        /// </summary>
        [XmlAttribute("sea")]
        public string Search
        {
            get { return _search; }
            set { _search = value; }
        }

        private bool _isLogoEnabled;
        /// <summary>
        /// Ẩn hiện Logo
        /// </summary>
        [XmlAttribute("islo")]
        public bool IsLogoEnabled
        {
            get { return _isLogoEnabled; }
            set { _isLogoEnabled = value; }
        }

        private string _logo;
        /// <summary>
        /// Cài đặt nội dung logo
        /// </summary>
        [XmlAttribute("logo")]
        public string Logo
        {
            get { return _logo; }
            set { _logo = value; }
        }

        private Image _ImgLogo;
        /// <summary>
        /// Chứa hình ảnh
        /// </summary>
        [XmlElement("logoIMG")]
        public Image ImgLogo
        {
            get { return _ImgLogo ?? (_ImgLogo = new Image()); }
            set { _ImgLogo = value; }
        }


        private bool _isVolumeEnabled;
        /// <summary>
        /// Ẩn hiện thanh Volume
        /// </summary>
        [XmlAttribute("isvol")]
        public bool IsVolumeEnabled
        {
            get { return _isVolumeEnabled; }
            set { _isVolumeEnabled = value; }
        }

        private bool _isNotesEnabled;
        /// <summary>
        /// Ẩn hiện menu Note
        /// </summary>
        [XmlAttribute("isNote")]
        public bool IsNotesEnabled
        {
            get { return _isNotesEnabled; }
            set { _isNotesEnabled = value; }
        }

        private bool _isStatusBarEnabled;
        /// <summary>
        /// Ẩn hiện thanh status
        /// </summary>
        [XmlAttribute("isStatus")]
        public bool IsStatusBarEnabled
        {
            get { return _isStatusBarEnabled; }
            set { _isStatusBarEnabled = value; }
        }

        private ObservableCollection<EFunctionCatalog> _catalogs;
        /// <summary>
        /// List Catalog
        /// </summary>
        [XmlArray("lstCata"), XmlArrayItem("funcca")]
        public ObservableCollection<EFunctionCatalog> Catalogs
        {
            get { return _catalogs ?? (_catalogs = new ObservableCollection<EFunctionCatalog>()); }
            set { _catalogs = value; }
        }

        private EFunctionCatalog _selectedCatalog;
        /// <summary>
        /// SelectedCatalog
        /// </summary>
        [XmlElement("selecata")]
        public EFunctionCatalog SelectedCatalog
        {
            get { return _selectedCatalog ?? (_selectedCatalog = new EFunctionCatalog()); }
            set { _selectedCatalog = value; }
        }

        private bool _isSearch = true;
        /// <summary>
        /// Thuộc tính IsSearch
        /// </summary>
        [XmlAttribute("iss")]
        public bool IsSearch
        {
            get { return _isSearch; }
            set { _isSearch = value; }
        }

        private bool _isSeekbarEnabled = true;
        /// <summary>
        /// Thuộc tính iseekbar
        /// </summary>
        [XmlAttribute("isseek")]
        public bool IsSeekbarEnabled
        {
            get { return _isSeekbarEnabled; }
            set { _isSeekbarEnabled = value; }
        }

        private bool _isRepeat;
        /// <summary>
        /// Thuộc tính IsRepeat
        /// </summary>
        [XmlAttribute("isrep")]
        public bool IsRepeat
        {
            get { return _isRepeat; }
            set { _isRepeat = value; }
        }



        /// <summary>
        /// Hàm copy dữ liệu
        /// </summary>
        /// <param name="target"></param>
        public void Copy(EFeatures target)
        {
            target.IsLogoEnabled = IsLogoEnabled;
            target.IsNotesEnabled = IsNotesEnabled;
            target.IsSearchEnabled = IsSearchEnabled;
            target.IsStatusBarEnabled = IsStatusBarEnabled;
            target.IsSubjectEnable = IsSubjectEnable;
            target.IsTitleEnabled = IsTitleEnabled;
            target.IsVolumeEnabled = IsVolumeEnabled;
            target.IsWriterEnabled = IsWriterEnabled;
            target.IsRepeat = IsRepeat;
            target.Logo = Logo;
            target.Search = Search;
            target.Subject = Subject;
            target.Title = Title;
            target.Writer = Writer;
            target.IsSearch = IsSearch;
            target.IsSeekbarEnabled = IsSeekbarEnabled;
            target.ImgLogo = ImgLogo.Clone() as Image;
            target.SelectedCatalog = SelectedCatalog.Clone() as EFunctionCatalog;

        }

        public override void IMediaLoad(IMediaDeserialize mediaSerialize)
        {
            base.IMediaLoad(mediaSerialize);
            this.ImgLogo?.IMediaLoad(mediaSerialize);
        }

        public override void IMediaSave(IMediaSerialize mediaSerialize)
        {
            base.IMediaSave(mediaSerialize);
            this.ImgLogo?.IMediaSave(mediaSerialize);
        }


        public override IElearningElement Clone()
        {
            EFeatures eFeatures = new EFeatures();
            Copy(eFeatures);
            foreach (EFunctionCatalog item in Catalogs)
            {
                eFeatures.Catalogs.Add(item.Clone() as EFunctionCatalog);
            }
            return eFeatures;
        }
    }
}

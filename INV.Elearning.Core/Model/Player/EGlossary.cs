﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("eglo")]
    public class EGlossary : RootModel
    {
        private string _term;
        /// <summary>
        /// Term trong Description
        /// </summary>
        [XmlAttribute("term")]
        public string Term
        {
            get { return _term; }
            set { _term = value; }
        }

        private string _definition;
        /// <summary>
        /// Definition trong Description
        /// </summary>
        [XmlAttribute("def")]
        public string Definition
        {
            get { return _definition; }
            set { _definition = value;  }
        }

        public void Copy(EGlossary eGlossary)
        {
            eGlossary.Term = Term;
            eGlossary.Definition = Definition;
        }

        public override IElearningElement Clone()
        {
            EGlossary eGlossary = new EGlossary();
            Copy(eGlossary);
            return eGlossary;
        }
    }
}

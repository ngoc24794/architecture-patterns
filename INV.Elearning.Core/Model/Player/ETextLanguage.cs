﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model.Player
{
    [Serializable]
    [XmlRoot("eTxtLan")]
    public class ETextLanguage : RootModel
    {
        private string _languale;
        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        [XmlAttribute("lang")]
        public string Language
        {
            get { return _languale; }
            set { _languale = value; }
        }

        private ObservableCollection<ETextLabel> _listTextLable;
        /// <summary>
        /// List text label
        /// </summary>
        [XmlArray("lsttxt"), XmlArrayItem("txtlb")]
        public ObservableCollection<ETextLabel> ListTextLabel
        {
            get { return _listTextLable ?? (_listTextLable = new ObservableCollection<ETextLabel>()); }
            set { _listTextLable = value; }
        }


        public override IElearningElement Clone()
        {
            ETextLanguage textLanguage = new ETextLanguage();
            textLanguage.Language = Language;
            foreach (ETextLabel item in ListTextLabel)
            {
                textLanguage.ListTextLabel.Add(item.Clone() as ETextLabel);
            }
            return textLanguage;
        }
    }
}

﻿using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Player;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("euctext")]
    public class EUCTextLabel : RootModel
    {
        private ETextLanguage _selectedLanguage;
        /// <summary>
        /// Ngôn ngữ được lựa chọn
        /// </summary>
        [XmlElement("seleLang")]
        public ETextLanguage SelectedLanguage
        {
            get { return _selectedLanguage ?? (_selectedLanguage = new ETextLanguage()); }
            set { _selectedLanguage = value; }
        }


        public override IElearningElement Clone()
        {
            EUCTextLabel eTextLabel = new EUCTextLabel();
            eTextLabel.SelectedLanguage = SelectedLanguage;
            return eTextLabel;
        }
    }
}

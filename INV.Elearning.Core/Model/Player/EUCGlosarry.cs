﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("ucGlo")]
    public class EUCGlosarry : RootModel
    {
        private ObservableCollection<EGlossary> _glossaries;
        /// <summary>
        /// List Glossary
        /// </summary>
        [XmlArray("lisglos"), XmlArrayItem("funcca")]
        public ObservableCollection<EGlossary> EGlossaries
        {
            get { return _glossaries ?? (_glossaries = new ObservableCollection<EGlossary>()); }
            set { _glossaries = value; }
        }

       

        public override IElearningElement Clone()
        {
            EUCGlosarry eUCGlosarry = new EUCGlosarry();
            foreach (EGlossary item in EGlossaries)
            {
                eUCGlosarry.EGlossaries.Add(item.Clone() as EGlossary);
            }
            return eUCGlosarry;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model.Player
{
    [Serializable]
    [XmlRoot("templ")]
    public class ETemplateColor : RootModel
    {
        #region Key màu
        [XmlElement("pagebg")]
        public EBaseColor PageBackgroundMain { get; set; }
        [XmlElement("playerbg")]
        public EBaseColor PlayerBackgroundMain { get; set; }
        [XmlElement("topbotbarbg")]
        public EBaseColor BackgroundTopBottomBar { get; set; }
        [XmlElement("topbotbartxt")]
        public EBaseColor TextTopBottomBar { get; set; }
        [XmlElement("bgbtn")]
        public EBaseColor BackgroundButtonInactiveTab { get; set; }
        [XmlElement("txticonbtn")]
        public EBaseColor TextIconButtonInactiveTab { get; set; }
        [XmlElement("hvbgbtn")]
        public EBaseColor HoverbackgroundButtonInactiveTab { get; set; }
        [XmlElement("hvtxtbtn")]
        public EBaseColor HovertexticonButtonInactiveTab { get; set; }
        [XmlElement("sidebarbg")]
        public EBaseColor BackgroundSidebarPopups { get; set; }
        [XmlElement("txtside")]
        public EBaseColor TextSidebarPopups { get; set; }
        [XmlElement("hvitmbg")]
        public EBaseColor HoveritembackgroundSidebarPopups { get; set; }
        [XmlElement("hvitmside")]
        public EBaseColor HoveritemtextSidebarPopups { get; set; }
        [XmlElement("selesidebg")]
        public EBaseColor SelecteditembackgroundSidebarPopups { get; set; }
        [XmlElement("seletxtside")]
        public EBaseColor SelecteditemtextSidebarPopups { get; set; }
        [XmlElement("visititmside")]
        public EBaseColor VisiteditemtextSidebarPopups { get; set; }
        [XmlElement("hyper")]
        public EBaseColor HyperlinkSidebarPopups { get; set; }
        [XmlElement("seekbg")]
        public EBaseColor BackgroundSeekbarVolumeControl { get; set; }
        [XmlElement("playseekbg")]
        public EBaseColor PlaybackSeekbarVolumeControl { get; set; }
        #endregion

        public void Copy(ETemplateColor target)
        {
            target.BackgroundButtonInactiveTab = BackgroundButtonInactiveTab.Clone() as EBaseColor;
            target.BackgroundSeekbarVolumeControl = BackgroundSeekbarVolumeControl.Clone() as EBaseColor;
            target.BackgroundSidebarPopups = BackgroundSidebarPopups.Clone() as EBaseColor;
            target.BackgroundTopBottomBar = BackgroundTopBottomBar.Clone() as EBaseColor;
            target.HoverbackgroundButtonInactiveTab = HoverbackgroundButtonInactiveTab.Clone() as EBaseColor;
            target.HoveritembackgroundSidebarPopups = HoveritembackgroundSidebarPopups.Clone() as EBaseColor;
            target.HoveritemtextSidebarPopups = HoveritemtextSidebarPopups.Clone() as EBaseColor;
            target.HovertexticonButtonInactiveTab = HovertexticonButtonInactiveTab.Clone() as EBaseColor;
            target.HyperlinkSidebarPopups = HyperlinkSidebarPopups.Clone() as EBaseColor;
            target.PageBackgroundMain = PageBackgroundMain.Clone() as EBaseColor;
            target.PlaybackSeekbarVolumeControl = PlaybackSeekbarVolumeControl.Clone() as EBaseColor;
            target.SelecteditembackgroundSidebarPopups = SelecteditembackgroundSidebarPopups.Clone() as EBaseColor;
            target.SelecteditemtextSidebarPopups = SelecteditemtextSidebarPopups.Clone() as EBaseColor;
            target.TextIconButtonInactiveTab = TextIconButtonInactiveTab.Clone() as EBaseColor;
            target.TextSidebarPopups = TextSidebarPopups.Clone() as EBaseColor;
            target.TextTopBottomBar = TextTopBottomBar.Clone() as EBaseColor;
            target.VisiteditemtextSidebarPopups = VisiteditemtextSidebarPopups.Clone() as EBaseColor;
            target.PlayerBackgroundMain = PlayerBackgroundMain.Clone() as EBaseColor;
        }

        public override IElearningElement Clone()
        {
            ETemplateColor eTemplateColor = new ETemplateColor();
            Copy(eTemplateColor);
            return eTemplateColor;
        }
    }
}

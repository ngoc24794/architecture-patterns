﻿

namespace INV.Elearning.Core.Model
{
    using System.ComponentModel;
    using System.Xml.Serialization;
    using System;
    using System.IO.Packaging;
    using System.Linq;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Lớp dẫn xuất từ interface INotifyPropertyChanged để được phép thực hiện cơ chế Binding
    /// </summary>
    [Serializable]
    public abstract class RootModel : INotifyPropertyChanged, IElearningElement
    {
        private string _name;
        private string _id;

        /// <summary>
        /// Cài đặt hoặc lấy tên của đối tượng
        /// </summary>
        [XmlAttribute("name")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Cài đặt hoặc lấy id của đối tượng
        /// </summary>
        [XmlAttribute("id")]
        public string ID
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        /// <summary>
        /// Gọi sự kiện thay đổi nội dung của thuộc tính
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public abstract IElearningElement Clone();

        /// <summary>
        /// Lưu trữ media.<br/>
        /// Các phương thức có sử dụng kiểu <see cref="IMedia"/> bắt buộc phải khai báo cụ thể để có thể lưu ra tập tin
        /// </summary>
        /// <param name="mediSerialize"></param>
        public virtual void IMediaSave(IMediaSerialize mediaSerialize)
        {
        }

        /// <summary>
        /// Đọc lại media.<br/>
        /// Các phương thức có sử dụng kiểu <see cref="IMedia"/> bắt buộc phải khai báo cụ thể để có thể lưu ra tập tin
        /// </summary>
        /// <param name="mediSerialize"></param>
        public virtual void IMediaLoad(IMediaDeserialize mediaSerialize) { }

        /// <summary>
        /// Delegate hỗ trợ lưu trữ tập tin media
        /// </summary>
        /// <param name="_media"></param>
        /// <param name="pagePart"></param>
        /// <param name="zipPackage"></param>
        public delegate void IMediaSerialize(IMedia _media);

        /// <summary>
        /// Delegate hỗ trợ đọc lại tập tin media
        /// </summary>
        /// <param name="_media"></param>
        /// <param name="pagePart"></param>
        /// <param name="zipPackage"></param>
        public delegate void IMediaDeserialize(IMedia _media);
    }
}

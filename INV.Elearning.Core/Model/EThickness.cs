﻿

namespace INV.Elearning.Core.Model
{
    using System.Xml.Serialization;
    using System;

    [Serializable]
    [XmlRoot("thickness")]
    public class EThickness
    {
        private double _top = 0.0;
        private double _left = 0.0;
        private double _bottom = 0.0;
        private double _right = 0.0;

        /// <summary>
        /// Lấy hoặc cài đặt căn đầu
        /// </summary>
        [XmlAttribute("t")]
        public double Top
        {
            get
            {
                return _top;
            }

            set
            {
                _top = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt căn trái
        /// </summary>
        [XmlAttribute("l")]
        public double Left
        {
            get
            {
                return _left;
            }

            set
            {
                _left = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt căn dưới
        /// </summary>
        [XmlAttribute("b")]
        public double Bottom
        {
            get
            {
                return _bottom;
            }

            set
            {
                _bottom = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt căn phải
        /// </summary>
        [XmlAttribute("r")]
        public double Right
        {
            get
            {
                return _right;
            }

            set
            {
                _right = value;
            }
        }
    }
}

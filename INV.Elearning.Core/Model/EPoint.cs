﻿
using System.Windows;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Điểm trong tài liệu
    /// </summar>
    [Serializable]
    [XmlRoot("ept")]
    public class EPoint
    {
        /// <summary>
        /// Tọa độ X
        /// </summary>
        private double _x = 0;
        /// <summary>
        /// Tọa độ Y
        /// </summary>
        private double _y = 0;

        /// <summary>
        /// Khởi tạo hàm không tham số
        /// </summary>
        public EPoint()
        {

        }

        /// <summary>
        /// Khởi tạo hàm có tham số
        /// </summary>
        /// <param name="x">Tọa độ X</param>
        /// <param name="y">Tọa độ Y</param>
        public EPoint(double x, double y)
        {
            _x = x;
            _y = y;
        }

        /// <summary>
        /// Lấy hoặc cài đặt tọa độ X
        /// </summary>
        [XmlAttribute("x")]
        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt tọa độ Y
        /// </summary>
        [XmlAttribute("y")]
        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
            }
        }

        /// <summary>
        /// Chuyển đổi sang kiểu Point của hệ thống
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Point Converter(EPoint data)
        {
            return new Point(data.X, data.Y);
        }

        /// <summary>
        /// Chuyển đổi từ kiểu Point của hệ thống
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static EPoint Converter(Point data)
        {
            return new EPoint(data.X, data.Y);
        }
    }
}

﻿using System;
using System.Windows.Media;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("border")]
    public class EBorder : RootModel
    {
        private double _borderThickness = 0.0;
        private ColorBase _borderBrush = null;
        private string _pathData = null;
        private CompoundType _compoundType = CompoundType.Simple;
        private DashType _dashType = DashType.Solid;
        private PenLineCap _capType = PenLineCap.Square;
        private PenLineJoin _joinType = PenLineJoin.Bevel;

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính độ dày viền
        /// </summary>
        [XmlAttribute("thick")]
        public double BorderThickness
        {
            get
            {
                return _borderThickness;
            }

            set
            {
                _borderThickness = Math.Max(0, value);
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính màu viền
        /// </summary>
        [XmlElement("brus")]
        public ColorBase BorderBrush
        {
            get
            {
                return _borderBrush;
            }

            set
            {
                _borderBrush = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính dữ liệu đường Path
        /// </summary>
        [XmlElement("data")]
        public string PathData
        {
            get
            {
                return _pathData;
            }

            set
            {
                _pathData = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính kiểu viền
        /// </summary>
        [XmlAttribute("cotype")]
        public CompoundType CompoundType
        {
            get
            {
                return _compoundType;
            }

            set
            {
                _compoundType = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt kiểu nét viền
        /// </summary>
        [XmlAttribute("dtype")]
        public DashType DashType
        {
            get
            {
                return _dashType;
            }

            set
            {
                _dashType = value;
            }
        }

        [XmlAttribute("catype")]
        public PenLineCap CapType
        {
            get
            {
                return _capType;
            }

            set
            {
                _capType = value;
            }
        }


        /// <summary>
        /// Lấy hoặc cài đặt màu nền cho đối tượng
        /// </summary>
        [XmlElement("bgr")]
        public ColorBase Fill
        {
            set; get;
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Join
        /// </summary>
        [XmlAttribute("join")]
        public PenLineJoin JoinType { get => _joinType; set => _joinType = value; }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new EBorder()
            {
                BorderBrush = this.BorderBrush?.Clone() as ColorBase,
                Fill = this.Fill?.Clone() as ColorBase,
                BorderThickness = this.BorderThickness,
                CapType = this.CapType,
                CompoundType = this.CompoundType,
                DashType = this.DashType,
                PathData = this.PathData,
                JoinType = this.JoinType
            };
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaLoad(IMediaDeserialize mediSerialize)
        {
            this.Fill?.IMediaLoad(mediSerialize);
            this.BorderBrush?.IMediaLoad(mediSerialize);
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaSave(IMediaSerialize mediSerialize)
        {
            this.Fill?.IMediaSave(mediSerialize);
            this.BorderBrush?.IMediaSave(mediSerialize);
        }
    }

    public enum CompoundType
    {
        [XmlEnum("simple")]
        Simple,
        [XmlEnum("double")]
        Double,
        [XmlEnum("thickThin")]
        ThickThin,
        [XmlEnum("thinThick")]
        ThinThick,
        [XmlEnum("triple")]
        Triple
    }

    public enum DashType
    {
        [XmlEnum("solid")]
        Solid,
        [XmlEnum("rdD")]
        RoundDot,
        [XmlEnum("sqD")]
        SquareDot,
        [XmlEnum("ds")]
        Dash,
        [XmlEnum("dsD")]
        DashDot,
        [XmlEnum("lgD")]
        LongDash,
        [XmlEnum("lgDd")]
        LongDashDot,
        [XmlEnum("lgDd2")]
        LongDashDotDot
    }
}

﻿

namespace INV.Elearning.Core.Model
{
    using System.Xml.Serialization;
    using System;
    using System.Collections.ObjectModel;
    using INV.Elearning.Core.Model.ObjectElement;
    using INV.Elearning.Core.Model.Theme;
    using INV.Elearning.Core.Timeline;
    using System.Windows;
    using INV.Elearning.Core.View;

    /// <summary>
    /// Đối tượng thuộc khung chuẩn
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(EStandardElement))]
    [XmlInclude(typeof(GroupElement))]
    [XmlInclude(typeof(EThemeShapeBackground))]
    [XmlRoot("obj")]
    public abstract class ObjectElementBase : RootModel, ITriggerDataObject
    {
        private double _angle = 0.0;
        private double _height = 20.0;
        private double _left = 20.0;
        private double _top = 20.0;
        private double _width = 20.0;
        private int _zOder = 0;
        private bool _isSelected = false;
        private bool _isGraphicBackground = false;
        private Timing _timing;
        private EAnimation _animations;

        private bool _isShow = true;
        private bool _isCustomGraphics = true;

        [XmlAttribute("sho")]
        public bool IsShow
        {
            get { return _isShow; }
            set
            {
                _isShow = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính góc quay của 1 đối tượng
        /// </summary>
        [XmlAttribute("ang")]
        public double Angle
        {
            get
            {
                return _angle;
            }

            set
            {
                _angle = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính chiều cao khung
        /// </summary>
        [XmlAttribute("hei")]
        public double Height
        {
            get
            {
                return _height;
            }

            set
            {
                _height = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính căn trái của đối tượng
        /// </summary>
        [XmlAttribute("lef")]
        public double Left
        {
            get
            {
                return _left;
            }

            set
            {
                _left = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính căn trên của đối tượng
        /// </summary>
        [XmlAttribute("top")]
        public double Top
        {
            get
            {
                return _top;
            }

            set
            {
                _top = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính chiều rộng khung
        /// </summary>
        [XmlAttribute("wid")]
        public double Width
        {
            get
            {
                return _width;
            }

            set
            {
                _width = value;
            }
        }

        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính được chọn lựa của đối tượng
        /// </summary>
        [XmlIgnore]
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }

            set
            {
                _isSelected = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính thứ tự ZIndex của khung
        /// </summary>
        [XmlAttribute("zod")]
        public int ZOder
        {
            get
            {
                return _zOder;
            }

            set
            {
                _zOder = value;
            }
        }

        /// <summary>
        /// Hiệu ứng chuyển động
        /// </summary>
        [XmlElement("anis")]
        public EAnimation Animations {
            get => 
                _animations ?? (_animations = new EAnimation());
            set => _animations = value; }

        /// <summary>
        /// Khóa
        /// </summary>
        [XmlAttribute("lock")]
        public bool IsLocked { get; set; }

        /// <summary>
        /// Đảo chiều X
        /// </summary>
        [XmlAttribute("scX")]
        public bool IsScaleX { get; set; }

        /// <summary>
        /// Đảo chiều Y
        /// </summary>
        [XmlAttribute("scY")]
        public bool IsScaleY { get; set; }

        /// <summary>
        /// Thời gian
        /// </summary>
        [XmlElement("timing")]
        public Timing Timing { get => _timing ?? (_timing = new Timing()); set => _timing = value; }

        #region TriggerData
        [XmlArray("trigLst"), XmlArrayItem("trig")]
        /// <summary>
        /// Danh sách Trigger
        /// </summary>
        public ObservableCollection<TriggerableDataObjectBase> TriggerData { get; set; }

        [XmlAttribute("isG")]
        /// <summary>
        /// Lấy hoawhcj cài đặt thuộc tính là đối tượng nền
        /// </summary>
        public bool IsGraphicBackground { get => _isGraphicBackground; set => _isGraphicBackground = value; }
        #endregion
        /// <summary>
        /// Thuộc tính ẩn hiện
        /// </summary>
        [XmlAttribute("visi")]
        public Visibility Visibility { get; set; }
        /// <summary>
        /// Kiểu place holder
        /// </summary>
        [XmlAttribute("ple")]
        public PlaceHolderEnum PlaceHolderType { get; set; }

        /// <summary>
        /// Kiểm tra xem object bên slide master có phải tự thêm vào hay không
        /// </summary>
        [XmlAttribute("isCuGr")]
        public bool IsCustomGraphics { get => _isCustomGraphics; set => _isCustomGraphics = value; }
        /// <summary>
        /// Đếm số element cùng kiểu
        /// </summary>
        [XmlAttribute("elecount")]
        public int ElementCount { get; set; }

        /// <summary>
        /// Key ngôn ngữ cho text của Slide Master
        /// </summary>
        [XmlAttribute("kLAn")]
        public string KeyLanguage { get; set; }
        public virtual void Copy(ObjectElementBase target)
        {
            target.IsShow = IsShow;
            target.IsLocked = IsLocked;
            target.Angle = this.Angle;
            target.Animations = this.Animations?.Clone() as EAnimation;
            target.Height = this.Height;
            target.IsLocked = this.IsLocked;
            target.Left = this.Left;
            target.Name = this.Name;
            target.Top = this.Top;
            target.PlaceHolderType = PlaceHolderType;
            target.ID = this.ID;
            target.Width = this.Width;
            target.ZOder = this.ZOder;
            target.IsScaleX = this.IsScaleX;
            target.IsScaleY = this.IsScaleY;
            target.Timing = this.Timing?.Clone() as Timing;
            target.IsGraphicBackground = this.IsGraphicBackground;
            target.Visibility = this.Visibility;
            target.ElementCount = ElementCount;
            target.IsCustomGraphics = IsCustomGraphics;
            target.KeyLanguage = KeyLanguage;
            target.TriggerData = new ObservableCollection<TriggerableDataObjectBase>();
            if (TriggerData != null)
                foreach (var trigger in TriggerData)
                {
                    if (trigger != null)
                        target.TriggerData.Add(trigger.Clone() as TriggerableDataObjectBase);
                }
        }
    }

    /// <summary>
    /// Giao diện cho đối tượng văn bản
    /// </summary>
    public interface IETextContent
    {

    }
}

﻿

namespace INV.Elearning.Core.Model
{
    using System.Collections.ObjectModel;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp lưu trữ các trạng thái của đối tượng
    /// </summary>
    [XmlRoot("stage")]
    public class ElementStage : RootModel
    {
        private bool _isSelected = false;
        private ObservableCollection<ObjectElementBase> _children = null;
        private bool _isDefault = false;
        private Image _thumbnail = null;

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính đang được lựa chọn
        /// </summary>
        [XmlIgnore]
        public bool IsSelected { get => _isSelected; set => _isSelected = value; }

        /// <summary>
        /// Lấy danh sách các đối tượng được chứa trong Style
        /// </summary>
        [XmlElement("child")]
        public ObservableCollection<ObjectElementBase> Children { get => _children ?? (_children = new ObservableCollection<ObjectElementBase>()); }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính mặc định cho đối tượng
        /// </summary>
        [XmlAttribute("isdf")]
        public bool IsDefault { get => _isDefault; set => _isDefault = value; }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính hình đại diện cho đối tượng
        /// </summary>
        [XmlElement("thb")]
        public Image Thumbnail { get => _thumbnail; set => _thumbnail = value; }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return null;
        }
    }
}

﻿

namespace INV.Elearning.Core.Model.ObjectElement
{
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System;

    [XmlRoot("gr")]
    [Serializable]
    public class GroupElement : ObjectElementBase
    {
        /// <summary>
        /// Danh sách thành viên trong nhóm
        /// </summary>
        private List<ObjectElementBase> _members = null;

        /// <summary>
        /// Lấy danh sách thành viên trong nhóm
        /// </summary>
        [XmlArray("mems")]
        public List<ObjectElementBase> Members
        {
            get
            {
                return _members ?? (_members = new List<ObjectElementBase>());
            }
        }
        
        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _result = new GroupElement();
            base.Copy(_result);
            this.Members.ForEach(x => _result.Members.Add(x.Clone() as ObjectElementBase));

            return _result;
        }
    }
}

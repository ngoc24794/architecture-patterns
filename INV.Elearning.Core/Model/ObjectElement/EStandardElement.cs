﻿
using INV.Elearing.Controls.Shapes;
using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp cung cấp các đối tượng có tùy chỉnh đường bao, hiệu ứng khung.
    /// </summary>
    [Serializable]
    [XmlRoot("stObj")]
    public class EStandardElement : ObjectElementBase
    {
        private ObservableCollection<FrameEffectBase> _frameEffects;
        /// <summary>
        /// Danh sách các hiệu ứng khung
        /// </summary>
        [XmlArrayItem("rfl", typeof(ReflectionEffect)), XmlArrayItem("sd", typeof(ShadowEffect)),
            XmlArrayItem("se", typeof(SoftEdgeEffect)), XmlArrayItem("gl", typeof(GlowEffect))]
        [XmlArray("effs")]
        public ObservableCollection<FrameEffectBase> FrameEffects
        {
            get { return _frameEffects ?? (_frameEffects = new ObservableCollection<FrameEffectBase>()); }
            set { _frameEffects = value; }
        }

        private EBorder _border;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính đường viền
        /// </summary>
        [XmlElement("bor")]
        public EBorder Border
        {
            get { return _border; }
            set { _border = value; }
        }

        private ShapeInfo _shapePresent;
        /// <summary>
        /// Lưu trữ dữ liệu cho Shape
        /// </summary>
        [XmlElement("shp")]
        public ShapeInfo ShapePresent
        {
            get { return _shapePresent; }
            set { _shapePresent = value; }
        }

        /// <summary>
        /// Lấy hoặc cài đặt giống kiểu đổ màu của Slide
        /// </summary>
        [XmlAttribute("lkSdl")]
        public bool IsLikeSlideFill { get; set; }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _result = new EStandardElement();
            this.Copy(_result);

            return _result;
        }

        /// <summary>
        /// Cpoy dữ liệu vào đối tượng
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override void Copy(ObjectElementBase target)
        {
            base.Copy(target);
            if (target is EStandardElement data)
            {
                data.Border = this.Border.Clone() as EBorder;
                data.FrameEffects = new ObservableCollection<FrameEffectBase>();
                foreach (var item in this.FrameEffects)
                {
                    data.FrameEffects.Add(item.Clone() as FrameEffectBase);
                }
                data.ShapePresent = this.ShapePresent;
            }
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaLoad(IMediaDeserialize mediSerialize)
        {
            this.Border?.IMediaLoad(mediSerialize);
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaSave(IMediaSerialize mediSerialize)
        {
            this.Border?.IMediaSave(mediSerialize);
        }
    }
}

﻿using INV.Elearning.Animations;
using System.Collections.Generic;
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Class lưu trữ dữ liệu hiệu ứng
    /// </summary>
    [Serializable]
    [XmlRoot("ani")]
    public class EAnimation : RootModel
    {
        /// <summary>
        /// Hiệu ứng đi vào
        /// </summary>
        [XmlElement("entr")]
        private Animation _entrance;
        public Animation Entrance
        {
            get =>
               _entrance ?? (_entrance = new NoneAnimation());
            set => _entrance = value;
        }
        /// <summary>
        /// Hiệu ứng đi ra
        /// </summary>
        [XmlElement("ext")]
        private Animation _exit;
        public Animation Exit
        {
            get =>
               _exit ?? (_exit = new NoneAnimation());
            set => _exit = value;
        }
        private List<DataMotionPath> _montionPaths;
        /// <summary>
        /// Danh sách các hiệu ứng đường Path
        /// </summary>
        [XmlArray("pts")]
        public List<DataMotionPath> MotionPaths
        {
            get { return _montionPaths ?? (_montionPaths = new List<DataMotionPath>()); }
            set { _montionPaths = value; }
        }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            EAnimation _result = new EAnimation();
            _result.Entrance = this.Entrance?.Clone() as Animation;
            _result.Exit = this.Exit?.Clone() as Animation;
            this.MotionPaths.ForEach(x => _result.MotionPaths?.Add(x?.Clone() as DataMotionPath));
            return _result;
        }
    }
}

﻿

namespace INV.Elearning.Core.Model
{
    public interface IElearningElement
    {
        /// <summary>
        /// Mã đối tượng
        /// </summary>
        string ID { set; get; }

        /// <summary>
        /// Tên đối tượng
        /// </summary>
        string Name { set; get; }

        /// <summary>
        /// Nhân bản một đối tượng
        /// </summary>
        /// <returns>Trả về một đối tượng mới với các thông số giống như bản gốc</returns>
        IElearningElement Clone();
    }
}

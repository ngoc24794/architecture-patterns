﻿

namespace INV.Elearning.Core.Model
{
    using System.Collections.ObjectModel;
    using System;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    using INV.Elearning.Core.Model.Theme;

    /// <summary>
    /// Lớp lưu trữ tài liệu
    /// </summary>
    [Serializable]
    [XmlRoot("doc")]
    public class ElearningDocument
    {
        /// <summary>
        /// Thông tin phiên bản
        /// </summary>
        private string _version = string.Empty;

        /// <summary>
        /// Danh sách các tài liệu có trong chương trình
        /// </summary>
        private List<PageElementBase> _pages = null;

        /// <summary>
        /// Lấy hoặc cài đặt thông tin phiên bản
        /// </summary>
        [XmlAttribute("ver")]
        public string Version
        {
            get
            {
                return _version;
            }

            set
            {
                _version = value;
            }
        }

        /// <summary>
        /// Lấy danh sách tài liệu có trong chương trình
        /// </summary>
        [XmlArray("pages")]
        public List<PageElementBase> Pages
        {
            get
            {
                return _pages ?? (_pages = new List<PageElementBase>());
            }
        }

        private ListVariables _variableData;

        /// <summary>
        /// Lấy danh sách biến số có trong chương trình
        /// </summary>
        [XmlElement("varData")]
        public ListVariables VariableData { get => _variableData ?? (_variableData = new ListVariables()); set => _variableData = value; }

        /// <summary>
        /// Lấy hoặc cài đặt thông tin cấu hình cho Player
        /// </summary>
        [XmlAnyElement("pst")]
        public EPlayer PlayerSetting { get; set; }
        /// <summary>
        /// Lấy hoặc cài đặt thông tin Theme đang chọn
        /// </summary>
        [XmlIgnore]
        public ETheme SelectedTheme { get; set; }

        /// <summary>
        /// Lấy thông tin màu được chọn
        /// </summary>
        [XmlIgnore]
        public EColorManagment SelectedColor { get; set; }
        /// <summary>
        /// Lấy thông tin font được chọn
        /// </summary>
        [XmlIgnore]
        public EFontfamily SelectedFont { get; set; }

        private ObservableCollection<ETheme> _listETheme;
        /// <summary>
        /// ListETheme
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<ETheme> ListETheme
        {
            get { return _listETheme ?? (_listETheme = new ObservableCollection<ETheme>()); }
            set { _listETheme = value; }
        }


    }
}

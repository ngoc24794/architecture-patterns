﻿namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Giao diện lưu trữ cấu hình Player
    /// </summary>
    public interface IPlayerDataSetting
    {
        /// <summary>
        /// Lấy hoặc cài đặt dữ liệu
        /// </summary>
        EPlayer Data { set; get; }

        /// <summary>
        /// Tải lại ViewModel từ dữ liệu
        /// </summary>
        /// <param name="data"></param>
        void LoadData(EPlayer data);

        /// <summary>
        /// Cập nhật lại dữ liệu
        /// </summary>
        void RefreshData();
    }
}

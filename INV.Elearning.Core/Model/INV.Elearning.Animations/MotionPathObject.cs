﻿using System;
using INV.Elearing.Controls.Shapes;
using INV.Elearning.Core.View;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Helper;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Animations
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ObjectElementX.cs
    // Description: Lớp được mở rộng từ ObjectElement đại diện cho một đối tượng trên Slide
    // Develope by : Nguyen Van Ngoc
    // History:
    // 11/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp được mở rộng từ ObjectElement đại diện cho một đối tượng trên Slide
    /// </summary>
    public class MotionPathObject : ObjectElement, IMotionPathObject, ICloneable
    {
        public MotionPathObject()
        {
            AngleChanged += OnAngleChanged;
            LocationChanged += OnAngleChanged;
            Data = new DataMotionPath();
            ID = ObjectElementsHelper.RandomString(13);
        }

        /// <summary>
        /// Hàm tĩnh
        /// </summary>
        static MotionPathObject()
        {
            RuleConfigurationProperty.OverrideMetadata(typeof(MotionPathObject), new PropertyMetadata(new ObjectElementRule() { CanGroup = false, CanScale = true }));
            VisibilityProperty.OverrideMetadata(typeof(MotionPathObject), new PropertyMetadata(Visibility.Hidden));
        }

        private void OnAngleChanged(object sender, ObjectElementEventArg e)
        {
            MotionPathObject motionPathObject = sender as MotionPathObject;
            if (motionPathObject?.Content is FrameworkElement content)
            {
                content.InvalidateVisual();
            }
        }

        public object Clone()
        {
            return new MotionPathObject()
            {
                ID = ID,
                CopiedId = ID,
                Top = Top,
                Left = Left,
                Width = Width,
                Height = Height,
                Angle = Angle,
                IsScaleX = IsScaleX,
                IsScaleY = IsScaleY,
                Owner = Owner,
                Animation = Animation,
                Content = Content
            };
        }

        /// <summary>
        /// Cập nhật thay đổi ...
        /// </summary>
        protected override void UpdateScale()
        {
            if (!this.IsLoaded) return;
            ScaleTransform _scale = null;
            if (this.Content != null)
            {
                if ((this.Content as FrameworkElement).LayoutTransform is ScaleTransform _transform)
                {
                    _transform.ScaleX = this.IsScaleX ? -1 : 1;
                    _transform.ScaleY = this.IsScaleY ? -1 : 1;
                    _scale = _transform;
                }
                else
                    _scale = new ScaleTransform(this.IsScaleX ? -1 : 1, this.IsScaleY ? -1 : 1);
                if (this.RuleConfiguration.CanScale)
                    (this.Content as FrameworkElement).LayoutTransform = _scale;
            }
            base.UpdateScale();
        }

        /// <summary>
        /// Xác định đối tượng sở hữu đường cong này.
        /// Được sử dụng để thêm hiệu ứng cho đối tượng 
        /// sở hữu thông qua việc thêm hiệu ứng cho đường cong này.
        /// </summary>
        [XmlIgnore]
        public ObjectElement Owner { get; set; }

        /// <summary>
        /// Hiệu ứng mà đường cong này sở hữu
        /// </summary>
        [XmlIgnore]
        public IMotionPathAnimation Animation { get; set; }

        #region XmlSerializable Properties
        //[XmlAttribute("owner")]
        //public string XOwner { get => Owner?.ID; set { if (Owner != null) Owner.ID = value; } }
        [XmlElement("aniEffect")]
        public MotionPathAnimation XAnimation { get => Animation as MotionPathAnimation; set => Animation = value; }
        #endregion

        public DataMotionPath GetData()
        {
            ConverterMotionPath.ConverterPathToDataPath(this, out DataMotionPath dataMotionPath);
            // dataMotionPath.XOwner = XOwner;
            dataMotionPath.ID = ID;
            dataMotionPath.AnimationPath = XAnimation;
            dataMotionPath.Top = Top;
            dataMotionPath.Left = Left;
            dataMotionPath.Width = Width;
            dataMotionPath.Height = Height;
            dataMotionPath.Rotation = Angle;
            dataMotionPath.IsScaleX = IsScaleX;
            dataMotionPath.IsScaleY = IsScaleY;
            dataMotionPath.Name = TargetName;
            if ((this as ObjectElement).Container as FrameworkElement is Panel container)
            {
                foreach (var child in container.Children)
                {
                    if (child is ShapeBase shape)
                    {
                        dataMotionPath.ShapesBase = shape.GetInfo() as MotionPathInfoBase;
                    }
                }
            }
            return dataMotionPath;
        }

        public override void RefreshData()
        {
            Data = GetData();
            //if (Data != null)
            //{
            //    Data.ID = ID;
            //    Data.Top = Top;
            //    Data.Left = Left;
            //    Data.Width = Width;
            //    Data.Height = Height;
            //    Data.Angle = Angle;
            //    Data.IsScaleX = IsScaleX;
            //    Data.IsScaleY = IsScaleY;
            //    if (Content is ShapeBase shape && Data is DataMotionPath dataMotionPath)
            //    {
            //        dataMotionPath.ShapesBase = shape.GetInfo() as MotionPathInfoBase;
            //    }
            //    Data.Animations = new EAnimation
            //    {
            //        MotionPaths = new System.Collections.Generic.List<DataMotionPath>()
            //    };
            //    DataMotionPath data = GetData();
            //    if (data != null)
            //    {
            //        Data.Animations.MotionPaths.Add(data.Clone() as DataMotionPath);
            //    }
            //}
        }

        public override void UpdateUI(ObjectElementBase data)
        {
            if (data is DataMotionPath dataMotionPath)
            {
                ObjectElement owner = FindElement(dataMotionPath.XOwner);
                if (owner != null)
                {
                    CopiedId = dataMotionPath.ID;
                    (Application.Current as IAppGlobal).ConverterDataPathToPath(dataMotionPath, this, owner);
                    ID = ObjectElementsHelper.RandomString(13);
                    Owner = owner;
                }
            }
        }

        private void GenerateMotionPathObject(MotionPathObject motionPathObject, ShapeBase shape, Rect rect, double rotation, bool isScaleX, bool isScaleY)
        {
            motionPathObject.Content = shape;
            motionPathObject.Width = rect.Width;
            motionPathObject.Height = rect.Height;
            motionPathObject.Left = rect.Left;
            motionPathObject.Top = rect.Top;
            motionPathObject.Angle = rotation;
            motionPathObject.IsScaleX = isScaleX;
            motionPathObject.IsScaleY = isScaleY;
            Binding bindWidth = new Binding()
            {
                Path = new PropertyPath("Width"),
                Source = motionPathObject
            };

            Binding bindHeight = new Binding()
            {
                Path = new PropertyPath("Height"),
                Source = motionPathObject
            };

            Binding bindIsSelected = new Binding()
            {
                Path = new PropertyPath("IsSelected"),
                Source = motionPathObject
            };

            BindingOperations.SetBinding(shape, ShapeBase.WidthProperty, bindWidth);
            BindingOperations.SetBinding(shape, ShapeBase.HeightProperty, bindHeight);
            BindingOperations.SetBinding(shape, ShapeBase.IsSelectedProperty, bindIsSelected);
        }

        private ObjectElement FindElement(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            ObjectElement objectElement = FindInLayer((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout, id);
            // nếu đối tượng được tìm thấy trong layout hiện hành thì dừng tìm
            if (objectElement != null)
            {
                return objectElement;
            }
            // tìm tiếp trong slide hiện hành
            objectElement = FindInSlide((Application.Current as IAppGlobal).SelectedSlide, id);
            if (objectElement != null)
            {
                return objectElement;
            }
            foreach (SlideBase slide in (Application.Current as IAppGlobal).DocumentControl?.Slides)
            {
                objectElement = FindInSlide(slide, id);
                if (objectElement != null)
                {
                    return objectElement;
                }
            }
            return null;
        }

        private ObjectElement FindInSlide(SlideBase slide, string id)
        {
            ObjectElement objectElement = FindInLayer(slide.MainLayout, id);
            // nếu đối tượng được tìm thấy trong MainLayout thì dừng tìm
            if (objectElement != null)
            {
                return objectElement;
            }
            // tìm tiếp trong các layout
            foreach (LayoutBase item in slide.Layouts)
            {
                objectElement = FindInLayer(item, id);
                // nếu đối tượng được tìm thấy trong layout này thì dừng tìm
                if (objectElement != null)
                {
                    return objectElement;
                }
            }

            return null;
        }

        private ObjectElement FindInLayer(LayoutBase layer, string id)
        {
            foreach (ObjectElement item in layer?.Elements)
            {
                // nếu đối tượng được tìm thấy thì dừng tìm và trả về đối tượng đó
                if (item.ID == id)
                {
                    return item;
                }
            }

            return null;
        }
    }
}

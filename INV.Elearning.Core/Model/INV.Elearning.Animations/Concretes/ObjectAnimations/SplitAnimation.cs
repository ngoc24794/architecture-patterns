﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("split")]
    public class SplitAnimation : DirectionAnimation, ISplitAnimation
    {
        public SplitAnimation() : base()
        {
        }

        public SplitAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public SplitAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public SplitAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.SplitAnimation;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new SplitDirectionOption(),
                new MotionDirectionOptionGroup(AnimationType)
            };
            EffectOptions.Add(new SequenceOption());
        }

        [XmlAttribute("splitDir")]
        public eSplitDirection SplitDirection
        {
            get
            {
                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == KeyLanguage.SplitDirectionOption)
                    {
                        foreach (var itemEffect in item.Values)
                        {
                            if (itemEffect.IsSelected)
                            {
                                if (KeyLanguage.HorizontalIn.ToString().Equals(itemEffect.Name))
                                {
                                    return eSplitDirection.HorizontalIn;
                                }
                                else if (KeyLanguage.HorizontalOut.ToString().Equals(itemEffect.Name))
                                {
                                    return eSplitDirection.HorizontalOut;
                                }else if (KeyLanguage.VerticalIn.ToString().Equals(itemEffect.Name))
                                {
                                    return eSplitDirection.VerticalIn;
                                }
                                else if (KeyLanguage.VerticalOut.ToString().Equals(itemEffect.Name))
                                {
                                    return eSplitDirection.VerticalOut;
                                }

                            }
                        }
                    }

                }
                return eSplitDirection.VerticalIn;

            }
            set
            {
                SetOption(KeyLanguage.SplitDirectionOption, value);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("spin")]
    public class SpinAnimation : DirectionAnimation, ISpinAnimation
    {
        public SpinAnimation()
        {
        }

        public SpinAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public SpinAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public SpinAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.SpinAnimation;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new SpinDirectionOption(),
                new AmountOption(),
                new MotionDirectionOptionGroup(AnimationType)
            };

           
                EffectOptions.Add(new SequenceOption());
        }

        [XmlAttribute("spinDir")]
        public eSpinDirection SpinDirection
        {
            get
            {
                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == KeyLanguage.SpinDirectionOption)
                    {
                        foreach (var itemEffect in item.Values)
                        {
                            if (itemEffect.IsSelected)
                            {
                                if (KeyLanguage.Clockwise.ToString().Equals(itemEffect.Name))
                                {
                                    return eSpinDirection.Clockwise;
                                }
                                else if (KeyLanguage.CounterClockwise.ToString().Equals(itemEffect.Name))
                                {
                                    return eSpinDirection.CounterClockwise;
                                }
                            }
                        }
                    }
                }
                return eSpinDirection.Clockwise;
            }
            set
            {
                SetOption(KeyLanguage.SpinDirectionOption, value);
            }
        }
        [XmlAttribute("amount")]
        public eAmount Amount
        {
            get
            {
                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == KeyLanguage.AmountOption)
                    {
                        foreach (var itemEffect in item.Values)
                        {
                            if (itemEffect.IsSelected)
                            {
                                if (KeyLanguage.Full.ToString().Equals(itemEffect.Name))
                                {
                                    return eAmount.Full;
                                }
                                else if (KeyLanguage.Half.ToString().Equals(itemEffect.Name))
                                {
                                    return eAmount.Half;
                                }else if (KeyLanguage.Quarter.ToString().Equals(itemEffect.Name))
                                {
                                    return eAmount.Quater;
                                }
                                else if (KeyLanguage.Two.ToString().Equals(itemEffect.Name))
                                {
                                    return eAmount.Two;
                                }
                            }
                        }
                    }
                }
                return eAmount.Full;
            }
            set
            {
                SetOption(KeyLanguage.AmountOption, value);
            }
        }
       
    }
}

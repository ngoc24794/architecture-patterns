﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ScribbleAnimation.cs
    // Description: Chuyển động theo đường scribble
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    [XmlRoot("scribble")]
    public class ScribbleAnimation : MotionPathAnimation
    {
        public ScribbleAnimation() : base()
        {
        }

        public ScribbleAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public ScribbleAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public ScribbleAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.ScribbleAnimation;
        }

        [XmlIgnore]
        public override string GroupName => KeyLanguage.CustomMotionPathGroup;
    }
}

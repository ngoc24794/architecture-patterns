﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("grow")]
    public class GrowAnimation : DirectionAnimation, IAnimation
    {
        public GrowAnimation() : base()
        {

        }

        public GrowAnimation(string name, TimeSpan duration):base(name, duration)
        {

        }

        public GrowAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.GrowAnimation;
        }

        public GrowAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }
    }
}

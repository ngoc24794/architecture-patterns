﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("float")]
    public class FloatAnimation : Animation, IFloatAnimation
    {

        public FloatAnimation() : base()
        {
        }

        public FloatAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public FloatAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public FloatAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            if (AnimationType == eAnimationType.Entrance)
                Name = KeyLanguage.FloatInAnimation;
            else
                Name = KeyLanguage.FloatOutAnimation;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new FloatDirectionOption()
            };
            EffectOptions.Add(new SequenceOption());
        }
        [XmlAttribute("floatDir")]
        public eFloatDirection FloatDirection
        {
            get
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == KeyLanguage.FloatDirectionOption)
                        {
                            foreach (var itemEffect in item.Values)
                            {
                                if (itemEffect.IsSelected)
                                {
                                    if (KeyLanguage.FloatDown.ToString().Equals(itemEffect.Name))
                                    {
                                        return eFloatDirection.FloatDown;
                                    }
                                    else if (KeyLanguage.FloatUp.ToString().Equals(itemEffect.Name))
                                    {
                                        return eFloatDirection.FloatUp;
                                    }

                                }
                            }
                        }

                    }
                return eFloatDirection.FloatUp;

            }
            set
            {
                SetOption(KeyLanguage.FloatDirectionOption, value);
            }
        }
    }
}

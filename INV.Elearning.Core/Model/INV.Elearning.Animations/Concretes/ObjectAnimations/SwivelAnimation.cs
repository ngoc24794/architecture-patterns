﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("swivel")]
    public class SwivelAnimation : DirectionAnimation, IDirectionAnimation
    {
        public SwivelAnimation():base()
        {
        }

        public SwivelAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public SwivelAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.SwivelAnimation;
        }

        public SwivelAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("multi")]
    public class MultiAnimation : Animation
    {
        public MultiAnimation() : base()
        {
        }

        public MultiAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public MultiAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public MultiAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.MultiAnimation;
        }
    }
}

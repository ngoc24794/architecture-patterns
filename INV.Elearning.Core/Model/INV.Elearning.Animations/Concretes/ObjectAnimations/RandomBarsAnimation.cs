﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("randomBars")]
    public class RandomBarsAnimation : DirectionAnimation, IRandomBarsAnimation
    {
        public RandomBarsAnimation()
        {
        }

        public RandomBarsAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public RandomBarsAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public RandomBarsAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.RandomBarsAnimation;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new RandomBarsDirectionOption(),
                new MotionDirectionOptionGroup(AnimationType)
            };


            EffectOptions.Add(new SequenceOption());

        }

        [XmlAttribute("randomDir")]
        public eRandomBarsDirection RandomBarsDirection
        {
            get
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == KeyLanguage.RandomBarsDirectionOption)
                        {
                            foreach (var itemEffect in item.Values)
                            {
                                if (itemEffect.IsSelected)
                                {
                                    if (KeyLanguage.Horizontal.ToString().Equals(itemEffect.Name))
                                    {
                                        return eRandomBarsDirection.Horizontal;
                                    }
                                    else if (KeyLanguage.Vertical.ToString().Equals(itemEffect.Name))
                                    {
                                        return eRandomBarsDirection.Vertical;
                                    }
                                }
                            }
                        }
                    }
                return eRandomBarsDirection.Horizontal;
            }
            set
            {
                SetOption(KeyLanguage.RandomBarsDirectionOption, value);
            }
        }

    }
}

﻿using System;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SquareAnimation.cs
    // Description: Chuyển động theo hình chữ nhật
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    [XmlRoot("square")]
    public class SquareAnimation : MotionPathAnimation
    {
        public SquareAnimation() : base()
        {
        }

        public SquareAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public SquareAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public SquareAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.SquareAnimation;
        }

        [XmlIgnore]
        public override string GroupName => KeyLanguage.ShapesMotionPathGroup;
    }
}

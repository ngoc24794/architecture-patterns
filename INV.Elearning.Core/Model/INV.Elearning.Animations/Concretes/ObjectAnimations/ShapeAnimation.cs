﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("shape")]
    public class ShapeAnimation : DirectionAnimation, IShapeAnimation
    {
        public ShapeAnimation() : base()
        {
        }

        public ShapeAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public ShapeAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public ShapeAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {

            
            Name = KeyLanguage.ShapeAnimation;
          //  AnimationType = animationType;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new ShapeDirectionOption(),
                new ShapesOption(),
                new MotionDirectionOptionGroup(AnimationType)
            };
                EffectOptions.Add(new SequenceOption());
            
        }
        [XmlAttribute("shapeDir")]
        public eShapeDirection ShapeDirection
        {
            get
            {

                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == KeyLanguage.ShapeDirectionOption)
                    {
                        foreach (var itemEffect in item.Values)
                        {
                            if (itemEffect.IsSelected)
                            {
                                if (KeyLanguage.In.ToString().Equals(itemEffect.Name))
                                {
                                    return eShapeDirection.In;
                                }
                                else if (KeyLanguage.Out.ToString().Equals(itemEffect.Name))
                                {
                                    return eShapeDirection.Out;
                                }
                            }
                        }
                    }

                }
                return eShapeDirection.In;

            }
            set
            {
                SetOption(KeyLanguage.ShapeDirectionOption, value);
            }
        }
        [XmlAttribute("shapes")]
        public eShapes Shapes
        {
            get
            {

                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == KeyLanguage.ShapesOption)
                    {
                        foreach (var itemEffect in item.Values)
                        {
                            if (itemEffect.IsSelected)
                            {
                                if (KeyLanguage.Box.ToString().Equals(itemEffect.Name))
                                {
                                    return eShapes.Box;
                                }
                                else if (KeyLanguage.Circle.ToString().Equals(itemEffect.Name))
                                {
                                    return eShapes.Circle;
                                }else if (KeyLanguage.Diamond.ToString().Equals(itemEffect.Name))
                                {
                                    return eShapes.Diamond;
                                }
                                else if (KeyLanguage.Plus.ToString().Equals(itemEffect.Name))
                                {
                                    return eShapes.Plus;
                                }
                            }
                        }
                    }

                }
                return eShapes.Circle;

            }
            set
            {
                SetOption(KeyLanguage.ShapesOption, value);
            }
        }
        //[XmlAttribute("motionDir")]
        //public eMotionDirection MotionDirection { get; set; }
    }
}

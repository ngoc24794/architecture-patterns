﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("wheel")]
    public class WheelAnimation : DirectionAnimation, IWheelAnimation
    {
        public WheelAnimation() : base()
        {
        }

        public WheelAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public WheelAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public WheelAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.WheelAnimation;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new SpokesOption(),
           
                new MotionDirectionOptionGroup(AnimationType)
            };
            EffectOptions.Add(new SequenceOption());
        }

        [XmlAttribute("spokes")]
        public eSpokes Spokes
        {
            get
            {

                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == KeyLanguage.SpokesOption)
                    {
                        foreach (var itemEffect in item.Values)
                        {
                            if (itemEffect.IsSelected)
                            {
                                if (KeyLanguage.OneSpoke.ToString().Equals(itemEffect.Name))
                                {
                                    return eSpokes.OneSpoke;
                                }
                                else if (KeyLanguage.TwoSpokes.ToString().Equals(itemEffect.Name))
                                {
                                    return eSpokes.TwoSpokes;
                                }
                                else if (KeyLanguage.ThreeSpokes.ToString().Equals(itemEffect.Name))
                                {
                                    return eSpokes.ThreeSpokes;
                                }
                                else if (KeyLanguage.FourSpokes.ToString().Equals(itemEffect.Name))
                                {
                                    return eSpokes.FourSpokes;
                                }
                                else if (KeyLanguage.EightSpokes.ToString().Equals(itemEffect.Name))
                                {
                                    return eSpokes.EightSpokes;
                                }
                            }
                        }
                    }

                }
                return eSpokes.OneSpoke;

            }
            set
            {
                SetOption(KeyLanguage.SpokesOption, value);
            }
        }
       
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class AnimationInfo : IAnimationInfo, IDirectionAnimationInfo, IFloatAnimationInfo, IZoomAnimationInfo
    {
        [XmlAttribute("dur")]
        public double Duration { get; set; }
        [XmlAttribute("type")]
        public eAnimationType Type { get; set; }
        [XmlAttribute("seq")]
        public eSequence Sequence { get; set; }
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("md")]
        public eMotionDirection MotionDirection { get; set; }
        [XmlAttribute("fd")]
        public eFloatDirection FloatDirection { get; set; }
        [XmlAttribute("vp")]
        public eVanishingPoint VanishingPoint { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: EqualTriangleAnimation.cs
    // Description: Chuyển động theo hình tam giác
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    [XmlRoot("equalTriangle")]
    public class EqualTriangleAnimation : MotionPathAnimation
    {
        public EqualTriangleAnimation() : base()
        {
        }

        public EqualTriangleAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public EqualTriangleAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public EqualTriangleAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.EqualTriangleAnimation;
        }
        public override string GroupName => KeyLanguage.ShapesMotionPathGroup;
    }
}

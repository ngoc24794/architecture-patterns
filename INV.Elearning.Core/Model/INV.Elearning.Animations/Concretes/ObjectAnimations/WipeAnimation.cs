﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("wipe")]
    public class WipeAnimation : DirectionAnimation, IWipeAnimation
    {
        public WipeAnimation() : base()
        {
        }

        public WipeAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public WipeAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public WipeAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.WipeAnimation;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new WipeDirectionOption(),
                new MotionDirectionOptionGroup(AnimationType)
            };
            EffectOptions.Add(new SequenceOption());
        }

        [XmlAttribute("wipeDir")]
        public eWipeDirection WipeDirection
        {
            get
            {

                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == KeyLanguage.WipeDirectionOption)
                    {
                        foreach (var itemEffect in item.Values)
                        {
                            if (itemEffect.IsSelected)
                            {
                                if (KeyLanguage.Bottom.ToString().Equals(itemEffect.Name))
                                {
                                    return eWipeDirection.Bottom;
                                }
                                else if (KeyLanguage.Left.ToString().Equals(itemEffect.Name))
                                {
                                    return eWipeDirection.Left;
                                }else if (KeyLanguage.Right.ToString().Equals(itemEffect.Name))
                                {
                                    return eWipeDirection.Right;
                                }
                                else if (KeyLanguage.Top.ToString().Equals(itemEffect.Name))
                                {
                                    return eWipeDirection.Top;
                                }

                            }
                        }
                    }

                }
                return eWipeDirection.Bottom;

            }
            set
            {
                SetOption(KeyLanguage.WipeDirectionOption, value);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("fly")]
    public class FlyAnimation : DirectionAnimation
    {
        public FlyAnimation() : base()
        {

        }

        public FlyAnimation(string name, TimeSpan duration) : base(name, duration)
        {

        }

        public FlyAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            if (AnimationType == eAnimationType.Entrance)
                Name = KeyLanguage.FlyInAnimation;
            else
                Name = KeyLanguage.FlyOutAnimation;
            if (EffectOptions?.Count > 0)
            {
                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == "Enter")
                    {
                        foreach (var effect in item.Values)
                        {
                            if (effect.Name == "Bottom")
                            {
                                effect.IsSelected = true;
                            }
                            else
                            {
                                effect.IsSelected = false;
                            }

                        }
                    }
                }
            }
        }

        public FlyAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
            if (EffectOptions?.Count > 0)
            {
                foreach (var item in EffectOptions)
                {
                    if (item.GroupName == "Enter")
                    {
                        foreach (var effect in item.Values)
                        {
                            if (effect.Name == "Bottom")
                            {
                                effect.IsSelected = true;
                            }
                            else
                            {
                                effect.IsSelected = false;
                            }

                        }
                    }
                }
            }
        }
    }
}

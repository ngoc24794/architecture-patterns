﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: FreeformAnimation.cs
    // Description: Chuyển động theo đường tự do
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    [XmlRoot("freeform")]
    public class FreeformAnimation : MotionPathAnimation
    {
        public FreeformAnimation() : base()
        {
        }

        public FreeformAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public FreeformAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public FreeformAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.FreeformAnimation;
        }
        public override string GroupName => KeyLanguage.CustomMotionPathGroup;
    }
}

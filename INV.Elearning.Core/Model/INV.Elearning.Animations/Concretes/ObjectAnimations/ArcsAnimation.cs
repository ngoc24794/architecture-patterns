﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("arcs")]
    public class ArcsAnimation : MotionPathAnimation, IDirectionableAnimation
    {
        public ArcsAnimation() : base()
        {
        }

        public ArcsAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public ArcsAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public ArcsAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.ArcsAnimation;
        }

        public override string GroupName => KeyLanguage.BasicMotionPathGroup;

        protected override void InitalizeEffectOptions()
        {
            base.InitalizeEffectOptions();
            //EffectOptions.Insert(0, new PathDirectionOptionArcs());
        }

        [XmlAttribute("dir")]
        public eObjectAnimationOption Direction
        {
            get
            {
                return GetOption<eObjectAnimationOption>(KeyLanguage.PathDirectionOption, Enum.TryParse);
            }
            set
            {
                SetOption(KeyLanguage.PathDirectionOption, value);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: TrapezoidAnimation.cs
    // Description: Chuyển động theo đường tròn
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    [XmlRoot("trapezoid")]
    public class TrapezoidAnimation : MotionPathAnimation
    {
        public TrapezoidAnimation() : base()
        {
        }

        public TrapezoidAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public TrapezoidAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public TrapezoidAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.TrapezoidAnimation;
        }

        [XmlIgnore]
        public override string GroupName => KeyLanguage.ShapesMotionPathGroup;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class PathAnimationInfo : IPathAnimationInfo
    {
        public bool ReversePathDirection { get; set; }
        public bool RelativeStartPoint { get; set; }
        public bool OrientShapeToPath { get; set; }
        public ePathDirection Direction { get; set; }
        public eEasingPathDirection EasingPathDirection { get; set; }
        public eOrigin Origin { get; set; }
        public eSpeed Speed { get; set; }
        public string XName { get; set; }
        public double Duration { get; set; }
        public eAnimationType Type { get; set; }
        public eSequence Sequence { get; set; }
        public string Name { get; set; }
    }
}

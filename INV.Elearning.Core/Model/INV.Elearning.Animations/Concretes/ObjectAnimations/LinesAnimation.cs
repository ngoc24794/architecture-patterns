﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("line")]
    public class LinesAnimation : MotionPathAnimation, IDirectionableAnimation
    {
        public LinesAnimation() : base()
        {
        }

        public LinesAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public LinesAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        public LinesAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.LinesAnimation;
        }

        public override string GroupName => KeyLanguage.BasicMotionPathGroup;

        protected override void InitalizeEffectOptions()
        {
            base.InitalizeEffectOptions();
            //EffectOptions.Insert(0, new PathDirectionOption());
        }

        [XmlAttribute("dir")]
        public eObjectAnimationOption Direction { get => GetOption<eObjectAnimationOption>(KeyLanguage.PathDirectionOption, Enum.TryParse); set { SetOption(KeyLanguage.PathDirectionOption, value); } }
    }
}

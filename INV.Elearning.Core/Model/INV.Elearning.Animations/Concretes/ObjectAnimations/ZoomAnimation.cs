﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("zoom")]
    public class ZoomAnimation : Animation, IZoomAnimation
    {
        public ZoomAnimation():base()
        {
        }

        public ZoomAnimation(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public ZoomAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
        }

        public ZoomAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            Name = KeyLanguage.ZoomAnimation;
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new ZoomOption()
            };
            EffectOptions.Add(new SequenceOption());

        }

        [XmlAttribute("vp")]
        public eVanishingPoint VanishingPoint
        {
            get
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == KeyLanguage.VanishingPointOption|| item.GroupName==KeyLanguage.ZoomOption)
                        {
                            foreach (var itemEffect in item.Values)
                            {
                                if (itemEffect.IsSelected)
                                {
                                    if (KeyLanguage.ObjectCenter.ToString().Equals(itemEffect.Name))
                                    {
                                        return eVanishingPoint.ObjectCenter;
                                    }
                                    else if (KeyLanguage.SlideCenter.ToString().Equals(itemEffect.Name))
                                    {
                                        return eVanishingPoint.SlideCenter;
                                    }
                                }
                            }
                        }
                    }
                return eVanishingPoint.ObjectCenter;
            }
            set
            {
                SetOption(KeyLanguage.ZoomOption, value);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ObjectElementInfo.cs
    // Description: Đối tượng chứa thông tin của ObjectElement dùng để lưu/đọc xml
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/03/2018 : Create
    //---------------------------------------------------------------------------

    /// <summary>
    /// Đối tượng chứa thông tin của ObjectElement dùng để lưu/đọc xml
    /// </summary>
    [Serializable]
    public class ObjectElementInfo : IObjectElementInfo
    {
        [XmlElement("aniEffect")]
        public AnimationEffect Animation { get; set; }
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("s")]
        public bool IsSelected { get; set; }
        [XmlIgnore]
        public object Content { get; set; }
        [XmlAttribute("w")]
        public double Width { get; set; }
        [XmlAttribute("h")]
        public double Height { get; set; }
        [XmlAttribute("l")]
        public double Left { get; set; }
        [XmlAttribute("t")]
        public double Top { get; set; }
        [XmlAttribute("a")]
        public double Angle { get; set; }
        [XmlAttribute("sX")]
        public bool IsScaleX { get; set; }
        [XmlAttribute("sY")]
        public bool IsScaleY { get; set; }
    }

    public class ShapesInfo
    {
        [XmlArray("shapeLst"), XmlArrayItem("obj")]
        public List<ObjectElementInfo> Values { get; set; }
        public ShapesInfo() : base()
        {
            Values = new List<ObjectElementInfo>();
        }
    }
}

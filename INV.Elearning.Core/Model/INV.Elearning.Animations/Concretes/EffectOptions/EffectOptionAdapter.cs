﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class EffectOptionAdapter : IEffectOption, IEffectOptionGroup
    {

        public EffectOptionAdapter(IEffectOptionGroup parent, string image, IEffectOptionGroup values)
        {
            EffectOptionGroup = values;
            Image = image;
            GroupName = parent.GroupName;
        }
       

        public IEffectOptionGroup EffectOptionGroup { get; private set; }

        #region Implement IEffectOptionGroup

       
        public string GroupName { get; set; }
        [XmlIgnore]
        public List<IEffectOption> Values { get => EffectOptionGroup.Values; set => EffectOptionGroup.Values = value; }
        #endregion

        #region Implement IEffectOption
        
        public string Name { get => EffectOptionGroup.GroupName; set => EffectOptionGroup.GroupName = value; }
       
        public string Image { get{
                if ((Values.FirstOrDefault(c => c.IsSelected)) != null) return (Values.FirstOrDefault(c => c.IsSelected).Image);
                return "";
            } set => Values.FirstOrDefault(c => c.IsSelected).Image = value; }
        
        public string Value { get => Values.FirstOrDefault(c => c.IsSelected).Value; set => Values.FirstOrDefault(c => c.IsSelected).Value = value; }
        
        public bool IsSelected { get => Values.FirstOrDefault(c => c.IsSelected) != null ? Values.FirstOrDefault(c => c.IsSelected).IsSelected : false; set => Values.FirstOrDefault(c => c.IsSelected).IsSelected = value; }
        
        public eObjectAnimationOption Type { get; set; }
        #endregion
    }

}

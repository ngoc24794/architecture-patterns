﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class MotionDirectionOptionGroup : EffectOptionGroup, IEffectOptionGroup
    {
        public MotionDirectionOptionGroup(eAnimationType enimationType = eAnimationType.Entrance)
        {
            if (enimationType == eAnimationType.Entrance)
                GroupName = KeyLanguage.EnterOption;
            else
                GroupName = KeyLanguage.ExitOption;

            Values = new List<IEffectOption>()
            {
                new EffectOptionAdapter(this,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Enter.png", new MotionDirectionOption(enimationType))
            };
        }
    }
}

﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SplitDirectionOption.cs
    // Description: Tùy chọn cho hiệu ứng Split
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class SplitDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public SplitDirectionOption()
        {
            GroupName = KeyLanguage.SplitDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.HorizontalIn,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/HorizontalIn.png",eObjectAnimationOption.HorizontalIn.ToString(), true),
                new EffectOption(this, KeyLanguage.HorizontalOut,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/HorizontalOut.png",eObjectAnimationOption.HorizontalOut.ToString()),
                new EffectOption(this, KeyLanguage.VerticalIn,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/VerticalIn.png",eObjectAnimationOption.VerticalIn.ToString()),
                new EffectOption(this, KeyLanguage.VerticalOut,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/VerticalOut.png",eObjectAnimationOption.VerticalOut.ToString())
            };
        }
    }
}

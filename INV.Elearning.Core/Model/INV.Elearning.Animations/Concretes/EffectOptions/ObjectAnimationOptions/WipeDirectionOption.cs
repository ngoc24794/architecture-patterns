﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: WipeDirectionOption.cs
    // Description: Tùy chọn cho hiệu ứng Wipe
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class WipeDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public WipeDirectionOption()
        {
            GroupName = KeyLanguage.WipeDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Bottom,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromBottom.png",eObjectAnimationOption.Bottom.ToString(), true),
                new EffectOption(this, KeyLanguage.Left,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromLeft.png",eObjectAnimationOption.Left.ToString()),
                new EffectOption(this, KeyLanguage.Right,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromRight.png",eObjectAnimationOption.Right.ToString()),
                new EffectOption(this, KeyLanguage.Top,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromTop.png",eObjectAnimationOption.Top.ToString())
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class FloatDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public FloatDirectionOption()
        {
            GroupName = KeyLanguage.FloatDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.FloatUp,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FloatUp.png", eObjectAnimationOption.FloatUp.ToString(), true),
                new EffectOption(this, KeyLanguage.FloatDown,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FloatDown.png", eObjectAnimationOption.FloatDown.ToString())
            };
        }
    }
}

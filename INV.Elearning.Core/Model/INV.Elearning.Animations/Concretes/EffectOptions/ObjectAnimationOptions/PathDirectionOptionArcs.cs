﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class PathDirectionOptionArcs : EffectOptionGroup, IEffectOptionGroup
    {
        public PathDirectionOptionArcs()
        {
            GroupName = KeyLanguage.PathDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Down,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/DownArcs.png", eObjectAnimationOption.Down.ToString(), eObjectAnimationOption.Direction, true),
                new EffectOption(this, KeyLanguage.Left,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/LeftArcs.png", eObjectAnimationOption.Left.ToString(), eObjectAnimationOption.Direction),
                new EffectOption(this, KeyLanguage.Right,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/RightArcs.png", eObjectAnimationOption.Right.ToString(), eObjectAnimationOption.Direction),
                new EffectOption(this, KeyLanguage.Up,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/UpArcs.png", eObjectAnimationOption.Up.ToString(), eObjectAnimationOption.Direction)
            };
        }        
    }
}

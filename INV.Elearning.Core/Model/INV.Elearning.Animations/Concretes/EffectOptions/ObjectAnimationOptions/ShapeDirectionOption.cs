﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class ShapeDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public ShapeDirectionOption()
        {
            GroupName = KeyLanguage.ShapeDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.In,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/In.png", eObjectAnimationOption.In.ToString(), true),
                new EffectOption(this, KeyLanguage.Out,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Out.png", eObjectAnimationOption.Out.ToString())
            };
        }
    }
}

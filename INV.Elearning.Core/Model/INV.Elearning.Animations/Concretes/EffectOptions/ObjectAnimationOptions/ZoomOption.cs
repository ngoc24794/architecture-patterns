﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class ZoomOption : EffectOptionGroup, IEffectOptionGroup
    {
        public ZoomOption()
        {
            GroupName = KeyLanguage.ZoomOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.ObjectCenter,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/ObjectCenter.png", eObjectAnimationOption.ObjectCenter.ToString(), true),
                new EffectOption(this, KeyLanguage.SlideCenter,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/SlideCenter.png", eObjectAnimationOption.SlideCenter.ToString())
            };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class MotionDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public MotionDirectionOption(eAnimationType enimationType = eAnimationType.Entrance)
        {
            if (enimationType == eAnimationType.Entrance)
            {
                GroupName = KeyLanguage.EnterOption;
                Values = new List<IEffectOption>()
                {
                    new EffectOption(this, KeyLanguage.None,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/None.png",eObjectAnimationOption.None.ToString(), true),
                    new EffectOption(this, KeyLanguage.Bottom,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromBottom.png",eObjectAnimationOption.Bottom.ToString()),
                    new EffectOption(this, KeyLanguage.BottomLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromBottomLeft.png",eObjectAnimationOption.BottomLeft.ToString()),
                    new EffectOption(this, KeyLanguage.Left,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromLeft.png",eObjectAnimationOption.Left.ToString()),
                    new EffectOption(this, KeyLanguage.TopLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromTopLeft.png",eObjectAnimationOption.TopLeft.ToString()),
                    new EffectOption(this, KeyLanguage.Top,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromTop.png",eObjectAnimationOption.Top.ToString()),
                    new EffectOption(this, KeyLanguage.TopRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromTopRight.png",eObjectAnimationOption.TopRight.ToString()),
                    new EffectOption(this, KeyLanguage.Right,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromRight.png",eObjectAnimationOption.Right.ToString()),
                    new EffectOption(this, KeyLanguage.BottomRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromBottomRight.png",eObjectAnimationOption.BottomRight.ToString())
                };
            }

            else
            {
                GroupName = KeyLanguage.ExitOption;
                Values = new List<IEffectOption>()
                {
                    new EffectOption(this, KeyLanguage.None,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/None.png",eObjectAnimationOption.None.ToString(), true),
                    new EffectOption(this, KeyLanguage.Bottom,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromTop.png",eObjectAnimationOption.Bottom.ToString()),
                    new EffectOption(this, KeyLanguage.BottomLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromTopRight.png",eObjectAnimationOption.BottomLeft.ToString()),
                    new EffectOption(this, KeyLanguage.Left,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromRight.png",eObjectAnimationOption.Left.ToString()),
                    new EffectOption(this, KeyLanguage.TopLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromBottomRight.png",eObjectAnimationOption.TopLeft.ToString()),
                    new EffectOption(this, KeyLanguage.Top,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromBottom.png",eObjectAnimationOption.Top.ToString()),
                    new EffectOption(this, KeyLanguage.TopRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromBottomLeft.png",eObjectAnimationOption.TopRight.ToString()),
                    new EffectOption(this, KeyLanguage.Right,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromLeft.png",eObjectAnimationOption.Right.ToString()),
                    new EffectOption(this, KeyLanguage.BottomRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FromTopLeft.png",eObjectAnimationOption.BottomRight.ToString())
                };
            }




        }
    }
}

﻿
using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class EasingOptionGroup : EffectOptionGroup, IEffectOptionGroup
    {
        public EasingOptionGroup()
        {
            GroupName = KeyLanguage.EasingOption;
            Values = new List<IEffectOption>()
            {
                new EffectOptionAdapter(this,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Direction.png", new EasingPathDirectionOption()),
                new EffectOptionAdapter(this,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/VeryFast.png", new SpeedOption())
            };
        }
    }
}

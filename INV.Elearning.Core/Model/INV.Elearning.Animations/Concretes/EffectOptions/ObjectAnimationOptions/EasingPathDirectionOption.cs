﻿
using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class EasingPathDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public EasingPathDirectionOption()
        {
            GroupName = KeyLanguage.EasingPathDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.None,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/NonePath.png", eObjectAnimationOption.None.ToString(), eObjectAnimationOption.EasingDirection, true),
                new EffectOption(this, KeyLanguage.In,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/InPath.png", eObjectAnimationOption.In.ToString(), eObjectAnimationOption.EasingDirection),
                new EffectOption(this, KeyLanguage.Out,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/OutPath.png", eObjectAnimationOption.Out.ToString(), eObjectAnimationOption.EasingDirection),
                new EffectOption(this, KeyLanguage.InOut,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/InOutPath.png", eObjectAnimationOption.InOut.ToString(), eObjectAnimationOption.EasingDirection)
            };
        }
    }
}

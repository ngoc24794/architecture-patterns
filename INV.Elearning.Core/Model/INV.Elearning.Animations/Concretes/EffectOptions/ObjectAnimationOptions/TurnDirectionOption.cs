﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class TurnDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {

        
        public TurnDirectionOption()
        {
            GroupName = KeyLanguage.TurnDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Down,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/DownTurn.png",eObjectAnimationOption.Down.ToString(), eObjectAnimationOption.Direction, true),
                new EffectOption(this, KeyLanguage.DownRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/DownRightTurn.png",eObjectAnimationOption.DownRight.ToString(), eObjectAnimationOption.Direction),
                new EffectOption(this, KeyLanguage.Up,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/UpTurn.png",eObjectAnimationOption.Up.ToString(), eObjectAnimationOption.Direction),
                new EffectOption(this, KeyLanguage.UpRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/UpRightTurn.png",eObjectAnimationOption.UpRight.ToString(), eObjectAnimationOption.Direction)
            };
        }
    }
}
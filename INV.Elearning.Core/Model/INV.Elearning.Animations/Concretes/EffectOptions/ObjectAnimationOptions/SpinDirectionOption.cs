﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SpinDirectionOption.cs
    // Description: Tùy chọn cho hiệu ứng có xoay
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class SpinDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public SpinDirectionOption()
        {
            GroupName = KeyLanguage.SpinDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Clockwise,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Clockwise.png",eObjectAnimationOption.Clockwise.ToString(), true),
                new EffectOption(this, KeyLanguage.CounterClockwise,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/CounterClockwise.png",eObjectAnimationOption.CounterClockwise.ToString())
            };
        }
    }
}

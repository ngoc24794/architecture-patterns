﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class AmountOption : EffectOptionGroup, IEffectOptionGroup
    {
        public AmountOption()
        {
            GroupName = KeyLanguage.AmountOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Quarter,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/QuarterSpin.png",eObjectAnimationOption.Quater.ToString()),
                new EffectOption(this, KeyLanguage.Half,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/HalfSpin.png",eObjectAnimationOption.Half.ToString()),
                new EffectOption(this, KeyLanguage.Full,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/FullSpin.png",eObjectAnimationOption.Full.ToString(),true),
                new EffectOption(this, KeyLanguage.Two,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/TwoSpins.png",eObjectAnimationOption.Two.ToString())
            };
        }
    }
}
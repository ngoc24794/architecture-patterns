﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class OriginOption : EffectOptionGroup, IEffectOptionGroup
    {
        public OriginOption()
        {
            GroupName = KeyLanguage.OriginOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Locked,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Locked.png", eObjectAnimationOption.Locked.ToString(), eObjectAnimationOption.Origin),
                new EffectOption(this, KeyLanguage.UnLocked,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/UnLocked.png", eObjectAnimationOption.UnLocked.ToString(),eObjectAnimationOption.Origin, true)
            };
        }
    }
}

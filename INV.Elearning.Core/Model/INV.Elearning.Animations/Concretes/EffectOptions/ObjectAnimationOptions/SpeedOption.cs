﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SpeedOption.cs
    // Description: Tùy chọn cho hiệu ứng Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class SpeedOption : EffectOptionGroup, IEffectOptionGroup
    {
        public SpeedOption()
        {
            GroupName = KeyLanguage.SpeedOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Slow,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Slow.png",eObjectAnimationOption.Slow.ToString(), eObjectAnimationOption.Speed),
                new EffectOption(this, KeyLanguage.Medium,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Medium.png",eObjectAnimationOption.Medium.ToString(), eObjectAnimationOption.Speed, true),
                new EffectOption(this, KeyLanguage.Fast,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Fast.png",eObjectAnimationOption.Fast.ToString(), eObjectAnimationOption.Speed),
                new EffectOption(this, KeyLanguage.VeryFast,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/VeryFast.png",eObjectAnimationOption.VeryFast.ToString(), eObjectAnimationOption.Speed),
                new EffectOption(this, KeyLanguage.Bounce,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Bounce.png",eObjectAnimationOption.Bounce.ToString(), eObjectAnimationOption.Speed)
            };
        }
    }
}

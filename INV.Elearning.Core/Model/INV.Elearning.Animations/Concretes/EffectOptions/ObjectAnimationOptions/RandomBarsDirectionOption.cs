﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class RandomBarsDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public RandomBarsDirectionOption()
        {
            GroupName = KeyLanguage.RandomBarsDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Horizontal,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Horizontal.png",eTransitionOption.Horizontal.ToString(), true),
                new EffectOption(this, KeyLanguage.Vertical,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Vertical.png",eTransitionOption.Vertical.ToString())
            };
        }
    }
}

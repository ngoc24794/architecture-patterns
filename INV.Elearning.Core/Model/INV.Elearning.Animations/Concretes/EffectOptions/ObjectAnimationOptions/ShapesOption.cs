﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ShapesOption.cs
    // Description: Tùy chọn cho hiệu ứng Shape
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class ShapesOption : EffectOptionGroup, IEffectOptionGroup
    {
        public ShapesOption()
        {
            GroupName = KeyLanguage.ShapesOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Circle,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Circle.png", eObjectAnimationOption.Circle.ToString(), true),
                new EffectOption(this, KeyLanguage.Box,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Box.png", eObjectAnimationOption.Box.ToString()),
                new EffectOption(this, KeyLanguage.Diamond,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Diamond.png", eObjectAnimationOption.Diamond.ToString()),
                new EffectOption(this, KeyLanguage.Plus,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Plus.png", eObjectAnimationOption.Plus.ToString())
            };
        }
    }
}

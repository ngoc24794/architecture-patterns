﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class SequenceOption : EffectOptionGroup, IEffectOptionGroup
    {
        public SequenceOption()
        {
            GroupName = KeyLanguage.SequenceOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.OneObject,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/AsOneObject.png",eObjectAnimationOption.OneObject.ToString(), true),
                new EffectOption(this, KeyLanguage.Paragraph,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/ByParagraph.png",eObjectAnimationOption.Paragraph.ToString())
            };
        }
    }
}

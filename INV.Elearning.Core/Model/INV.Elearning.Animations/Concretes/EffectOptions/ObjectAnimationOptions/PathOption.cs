﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class PathOption : EffectOptionGroup, IEffectOptionGroup
    {
        public PathOption()
        {
            GroupName = KeyLanguage.PathOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.ReversePathDirection,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/ReversePathDirection.png", eObjectAnimationOption.ReversePathDirection.ToString(), eObjectAnimationOption.ReversePathDirection),
                new EffectOption(this, KeyLanguage.RelativeStartPoint,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/RelativeStartPoint.png", eObjectAnimationOption.RelativeStartPoint.ToString(), eObjectAnimationOption.RelativeStartPoint),
                new EffectOption(this, KeyLanguage.OrientShapeToPath,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/OrientShapeToPath.png", eObjectAnimationOption.OrientShapeToPath.ToString(), eObjectAnimationOption.OrientShapeToPath)
            };
        }
    }
}

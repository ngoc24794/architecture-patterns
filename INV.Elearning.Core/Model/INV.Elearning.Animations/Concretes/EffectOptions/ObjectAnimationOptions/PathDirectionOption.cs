﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class PathDirectionOption : EffectOptionGroup, IEffectOptionGroup
    {
        public PathDirectionOption()
        {
            GroupName = KeyLanguage.PathDirectionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Down,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Down.png", eObjectAnimationOption.Down.ToString(), eObjectAnimationOption.Direction, true),
                new EffectOption(this, KeyLanguage.Left,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Left.png", eObjectAnimationOption.Left.ToString(), eObjectAnimationOption.Direction),
                new EffectOption(this, KeyLanguage.Right,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Right.png", eObjectAnimationOption.Right.ToString(), eObjectAnimationOption.Direction),
                new EffectOption(this, KeyLanguage.Up,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/Up.png", eObjectAnimationOption.Up.ToString(), eObjectAnimationOption.Direction)
            };
        }        
    }
}

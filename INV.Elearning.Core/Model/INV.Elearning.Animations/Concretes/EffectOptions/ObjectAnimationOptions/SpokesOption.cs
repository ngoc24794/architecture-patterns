﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SpokesOption.cs
    // Description: Tùy chọn cho hiệu ứng Wheel
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class SpokesOption : EffectOptionGroup, IEffectOptionGroup
    {
        public SpokesOption()
        {
            GroupName = KeyLanguage.SpokesOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.OneSpoke,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/1Spoke.png",eObjectAnimationOption.OneSpoke.ToString(), true),
                new EffectOption(this, KeyLanguage.TwoSpokes,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/2Spoke.png",eObjectAnimationOption.TwoSpokes.ToString()),
                new EffectOption(this, KeyLanguage.ThreeSpokes,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/3Spoke.png",eObjectAnimationOption.ThreeSpokes.ToString()),
                new EffectOption(this, KeyLanguage.FourSpokes,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/4Spoke.png",eObjectAnimationOption.FourSpokes.ToString()),
                new EffectOption(this, KeyLanguage.EightSpokes,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/8Spoke.png",eObjectAnimationOption.EightSpokes.ToString())
            };
        }
    }
}

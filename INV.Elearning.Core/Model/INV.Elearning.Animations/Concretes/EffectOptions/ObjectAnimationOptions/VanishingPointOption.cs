﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: VanishingPointOption.cs
    // Description: Tùy chọn cho hiệu ứng Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class VanishingPointOption : EffectOptionGroup, IEffectOptionGroup
    {
        public VanishingPointOption()
        {
            GroupName = KeyLanguage.VanishingPointOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.ObjectCenter,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/ObjectCenter.png",eObjectAnimationOption.ObjectCenter.ToString(), true),
                new EffectOption(this, KeyLanguage.SlideCenter,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/EffectOptions/SlideCenter.png",eObjectAnimationOption.SlideCenter.ToString())
            };
        }
    }
}

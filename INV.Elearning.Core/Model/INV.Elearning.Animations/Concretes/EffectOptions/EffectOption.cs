﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    [Serializable]
    public class EffectOption : IEffectOption, INotifyPropertyChanged
    {
        private bool _isSelected;

        public EffectOption(string name, string value)
        {
            Name = name;
            Image = string.Empty;
            Value = value;
            IsSelected = false;
        }

        public EffectOption(IEffectOptionGroup parent, string name, string image, string value, bool isSelected = false)
        {
            Name = name;
            Image = image;
            Value = value;
            IsSelected = isSelected;
            GroupName = parent.GroupName;
        }

        public EffectOption(IEffectOptionGroup parent, string name,string image, string value, eObjectAnimationOption type, bool isSelected = false)
        {
            Name = name;
            Image = image;
            Value = value;
            IsSelected = isSelected;
            GroupName = parent.GroupName;
            Type = type;
        }

        public string Name { get; set; }
        public string Image { get; set; }
        public string Value { get; set; }
        public bool IsSelected { get => _isSelected; set { _isSelected = value; OnPropertyChanged("IsSelected"); } }
        public string GroupName { get; set; }
        public eObjectAnimationOption Type { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}

﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: PushTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho Push Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class PushTransitionOption : IEffectOptionGroup
    {
        public PushTransitionOption() : base()
        {
            GroupName = KeyLanguage.PushTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.FromBottom,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromBottom.png",eTransitionOption.Bottom.ToString(), true),
                new EffectOption(this, KeyLanguage.FromLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromLeft.png",eTransitionOption.Left.ToString()),
                new EffectOption(this, KeyLanguage.FromRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromRight.png",eTransitionOption.Right.ToString()),
                new EffectOption(this, KeyLanguage.FromTop,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromTop.png",eTransitionOption.Top.ToString())
            };
        }

        public PushTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public PushTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: NewsFlashTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho NewsFlash Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class NewsFlashTransitionOption : IEffectOptionGroup
    {
        public NewsFlashTransitionOption()
        {
            GroupName = KeyLanguage.NewsFlashTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Clockwise,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/Clockwise.png",eTransitionOption.Clockwise.ToString(), true),
                new EffectOption(this, KeyLanguage.CounterClockwise,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/CounterClockwise.png",eTransitionOption.CounterClockwise.ToString())
            };
        }

        public NewsFlashTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public NewsFlashTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
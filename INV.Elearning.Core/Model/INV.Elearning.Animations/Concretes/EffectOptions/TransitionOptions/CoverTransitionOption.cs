﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CoverTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho Cover Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class CoverTransitionOption : IEffectOptionGroup
    {
        public CoverTransitionOption()
        {
            GroupName = KeyLanguage.CoverTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.FromRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromRight.png",eTransitionOption.Right.ToString(), true),
                new EffectOption(this, KeyLanguage.FromTop,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromTop.png",eTransitionOption.Top.ToString()),
                new EffectOption(this, KeyLanguage.FromLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromLeft.png",eTransitionOption.Left.ToString()),
                new EffectOption(this, KeyLanguage.FromBottom,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromBottom.png",eTransitionOption.Bottom.ToString()),
                new EffectOption(this, KeyLanguage.FromTopRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromTopRight.png",eTransitionOption.TopRight.ToString()),
                new EffectOption(this, KeyLanguage.FromBottomRight,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromBottomRight.png",eTransitionOption.BottomRight.ToString()),
                new EffectOption(this, KeyLanguage.FromTopLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromTopLeft.png",eTransitionOption.TopLeft.ToString()),
                new EffectOption(this, KeyLanguage.FromBottomLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromBottomLeft.png",eTransitionOption.BottomLeft.ToString())
            };
        }

        public CoverTransitionOption(string groupName) : this()
        {
            GroupName = groupName;

        }

        public CoverTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
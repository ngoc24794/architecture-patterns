﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CheckerBoardTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho CheckerBoard Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class CheckerBoardTransitionOption : IEffectOptionGroup
    {
        public CheckerBoardTransitionOption()
        {
            GroupName = KeyLanguage.CheckerBoardTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.FromLeft,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromLeftCheck.png",eTransitionOption.Left.ToString(), true),
                new EffectOption(this, KeyLanguage.FromTop,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/FromTopCheck.png",eTransitionOption.Top.ToString())
            };
        }

        public CheckerBoardTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public CheckerBoardTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
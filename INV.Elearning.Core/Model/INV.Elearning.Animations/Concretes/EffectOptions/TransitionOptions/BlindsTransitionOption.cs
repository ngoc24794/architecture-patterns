﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: BlindsTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho Blinds Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class BlindsTransitionOption : IEffectOptionGroup
    {
        public BlindsTransitionOption()
        {
            GroupName = KeyLanguage.BlindsTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.FromVertical,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/VerticalBlinds.png",eTransitionOption.Vertical.ToString(), true),
                new EffectOption(this, KeyLanguage.FromHorizontal,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/HorizontalBlinds.png",eTransitionOption.Horizontal.ToString())
            };
        }

        public BlindsTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public BlindsTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
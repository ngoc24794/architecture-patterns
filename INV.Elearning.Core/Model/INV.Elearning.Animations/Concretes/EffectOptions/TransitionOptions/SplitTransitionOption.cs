﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SplitTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho Split Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class SplitTransitionOption : IEffectOptionGroup
    {
        public SplitTransitionOption()
        {
            GroupName = KeyLanguage.SplitTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.VerticalOut,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/VerticalOut.png",eTransitionOption.VerticalOut.ToString(), true),
                new EffectOption(this, KeyLanguage.VerticalIn,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/VerticalIn.png",eTransitionOption.VerticalIn.ToString()),
                new EffectOption(this, KeyLanguage.HorizontalOut,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/HorizontalOut.png",eTransitionOption.HorizontalOut.ToString()),
                new EffectOption(this, KeyLanguage.HorizontalIn,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/HorizontalIn.png",eTransitionOption.HorizontalIn.ToString())
            };
        }

        public SplitTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public SplitTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
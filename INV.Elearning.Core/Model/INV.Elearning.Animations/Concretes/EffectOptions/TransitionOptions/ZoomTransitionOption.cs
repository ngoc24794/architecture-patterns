﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ZoomTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho Zoom Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class ZoomTransitionOption : IEffectOptionGroup
    {
        public ZoomTransitionOption()
        {
            GroupName = KeyLanguage.ZoomTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.In,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/In.png",eTransitionOption.In.ToString(), true),
                new EffectOption(this, KeyLanguage.Out,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/Out.png",eTransitionOption.Out.ToString())
            };
        }

        public ZoomTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public ZoomTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
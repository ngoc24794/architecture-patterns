﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RandomBarsTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho RandomBars Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class RandomBarsTransitionOption : IEffectOptionGroup
    {
        public RandomBarsTransitionOption()
        {
            GroupName = KeyLanguage.RandomBarsTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Vertical,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/Vertical.png",eTransitionOption.Vertical.ToString(), true),
                new EffectOption(this, KeyLanguage.Horizontal,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/Horizontal.png",eTransitionOption.Horizontal.ToString()),
            };
        }

        public RandomBarsTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public RandomBarsTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
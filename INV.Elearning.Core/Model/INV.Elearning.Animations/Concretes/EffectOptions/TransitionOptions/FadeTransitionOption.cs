﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: FadeTransitionOption.cs
    // Description: Tùy chọn hiệu ứng cho Fade Transition
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class FadeTransitionOption : IEffectOptionGroup
    {
        public FadeTransitionOption()
        {
            GroupName = KeyLanguage.FadeTransitionOption;
            Values = new List<IEffectOption>()
            {
                new EffectOption(this, KeyLanguage.Smoothly,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/Smoothly.png",eTransitionOption.Smoothly.ToString(), true),
                new EffectOption(this, KeyLanguage.ThroughBack,"/INV.Elearning.Core;component/Model/INV.Elearning.Animations/Images/Transition/ThroughBlack.png",eTransitionOption.ThroughBack.ToString()),

            };
        }

        public FadeTransitionOption(string groupName) : this()
        {
            GroupName = groupName;
        }

        public FadeTransitionOption(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<IEffectOption> Values { get; set; }
    }
}
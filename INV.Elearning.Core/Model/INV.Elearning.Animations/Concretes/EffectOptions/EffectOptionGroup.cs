﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace INV.Elearning.Animations
{
    [Serializable]

    public class EffectOptionGroup : IEffectOptionGroup
    {
        public EffectOptionGroup()
        {
        }

        public EffectOptionGroup(string groupName)
        {
            GroupName = groupName;
        }

        public EffectOptionGroup(string groupName, List<IEffectOption> values)
        {
            GroupName = groupName;
            Values = values;
        }

        [XmlAttribute]
        public string GroupName { get; set; }
        [XmlArray("Effect")]
        public List<IEffectOption> Values { get; set; }
    }
}
﻿using INV.Elearning.Animations;
using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.EffectOptions
{
    [Serializable]
    [XmlRoot("EfBa")]
    [XmlInclude(typeof(EffectOption))]
    [XmlInclude(typeof(EffectOptionAdapter))]
    public class EffectOptionBase : IEffectOption,INotifyPropertyChanged
    {
        [XmlAttribute("na")]
       // [XmlIgnore]
        public virtual string Name { get; set; }
        //[XmlAttribute("img")]
        [XmlIgnore]
        public virtual string Image { get; set; }
        [XmlAttribute("val")]
       // [XmlIgnore]
        public virtual string Value { get; set; }

        private bool _isSelected;

       

           [XmlAttribute("sel")]
       // [XmlIgnore]
        public virtual bool IsSelected { get => _isSelected; set { _isSelected = value; OnPropertyChanged("IsSelected"); } }
        //public bool IsSelected { get; set; }
          [XmlAttribute("gr")]
        //[XmlIgnore]
        public virtual string GroupName { get; set; }
        [XmlAttribute("ty")]
        //[XmlIgnore]
        public virtual eObjectAnimationOption Type { get; set; }
        //public string Name { get; set; }
        //public string Image { get; set; }
        //public string Value { get; set; }
        //public bool IsSelected { get => _isSelected; set { _isSelected = value; OnPropertyChanged("IsSelected"); } }
        //public string GroupName { get; set; }
        //public eObjectAnimationOption Type { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: BlindsTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class BlindsTransition : Transiton
    {
        public BlindsTransition() : base()
        {
        }

        public BlindsTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.BlindsTransition;
        }

        public BlindsTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public BlindsTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.BlindsTransition;
            GroupName = "Exciting";
        }

        public BlindsTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Exciting";
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new BlindsTransitionOption()
            };
        }
    }
}

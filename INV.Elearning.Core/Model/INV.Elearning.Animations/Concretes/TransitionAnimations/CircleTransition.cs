﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CircleTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class CircleTransition : Transiton
    {
        public CircleTransition() : base()
        {
        }

        public CircleTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.CircleTransition;
        }

        public CircleTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public CircleTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.CircleTransition;
            GroupName = "Sublte";
        }

        public CircleTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Sublte";
        }
        
    }
}

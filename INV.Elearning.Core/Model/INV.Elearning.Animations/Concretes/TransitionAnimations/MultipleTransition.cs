﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations.Concretes.TransitionAnimations
{
    public class MultipleTransition:Transiton
    {
        public MultipleTransition() : base()
        {
        }

        public MultipleTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.MultiAnimation;
        }

        public MultipleTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public MultipleTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.MultiAnimation;
        }

        public MultipleTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new FadeTransitionOption()
            };
        }
    }
}

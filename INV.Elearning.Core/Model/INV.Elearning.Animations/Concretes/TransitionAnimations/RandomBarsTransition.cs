﻿using System;
using System.Collections.Generic;
using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: RandomBarsTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class RandomBarsTransition : Transiton
    {
        public RandomBarsTransition() : base()
        {
        }

        public RandomBarsTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.RandomBarsTransition;
        }

        public RandomBarsTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public RandomBarsTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.RandomBarsTransition;
            GroupName = "Sublte";
        }

        public RandomBarsTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Sublte";
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new RandomBarsTransitionOption()
            };
        }
    }
}

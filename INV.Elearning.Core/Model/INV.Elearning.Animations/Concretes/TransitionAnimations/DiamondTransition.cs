﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DiamondTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class DiamondTransition : Transiton
    {
        public DiamondTransition() : base()
        {
        }

        public DiamondTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.DiamondTransition;
        }

        public DiamondTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public DiamondTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.DiamondTransition;
            GroupName = "Sublte";
        }

        public DiamondTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Sublte";
        }
        
    }
}

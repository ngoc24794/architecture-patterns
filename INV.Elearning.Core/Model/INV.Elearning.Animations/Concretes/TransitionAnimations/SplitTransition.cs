﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: SplitTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class SplitTransition : Transiton
    {
        public SplitTransition() : base()
        {
        }

        public SplitTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.SplitTransition;
        }

        public SplitTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public SplitTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.SplitTransition;
            GroupName = "Sublte";
        }

        public SplitTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Sublte";
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new SplitTransitionOption()
            };
        }
    }
}

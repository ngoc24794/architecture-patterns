﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: NoneTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class NoneTransition : Transiton
    {
        public NoneTransition() : base()
        {
            Name = KeyLanguage.NoneTransition;
        }

        public NoneTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.NoneTransition;
        }

        public NoneTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public NoneTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.NoneTransition;
            GroupName = "Sublte";
        }

        public NoneTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Sublte";
        }

        //protected override void InitalizeEffectOptions()
        //{
        //    EffectOptions = new List<IEffectOptionGroup>()
        //    {
        //        new FadeTransitionOption()
        //    };
        //}
    }
}

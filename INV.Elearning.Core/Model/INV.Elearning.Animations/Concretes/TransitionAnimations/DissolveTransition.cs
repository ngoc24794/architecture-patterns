﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DissolveTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class DissolveTransition : Transiton
    {
        public DissolveTransition() : base()
        {
        }

        public DissolveTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.DissolveTransition;
        }

        public DissolveTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public DissolveTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.DissolveTransition;
            GroupName = "Exciting";
        }

        public DissolveTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Exciting";
        }
        
    }
}

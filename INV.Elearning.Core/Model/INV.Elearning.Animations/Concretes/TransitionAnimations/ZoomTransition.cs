﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ZoomTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class ZoomTransition : Transiton
    {
        public ZoomTransition() : base()
        {
        }

        public ZoomTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.ZoomTransition;
        }

        public ZoomTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public ZoomTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.ZoomTransition;
            GroupName = "Exciting";
        }

        public ZoomTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Exciting";
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new ZoomTransitionOption()
            };
        }
    }
}

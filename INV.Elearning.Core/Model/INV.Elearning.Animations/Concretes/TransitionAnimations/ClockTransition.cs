﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ClockTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class ClockTransition : Transiton
    {
        public ClockTransition() : base()
        {
        }

        public ClockTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.ClockTransition;
        }

        public ClockTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public ClockTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.ClockTransition;
            GroupName = "Exciting";
        }

        public ClockTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Exciting";
        }
        
    }
}

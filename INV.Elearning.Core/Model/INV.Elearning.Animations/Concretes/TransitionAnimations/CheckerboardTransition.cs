﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: CheckerboardTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class CheckerboardTransition : Transiton
    {
        public CheckerboardTransition() : base()
        {
        }

        public CheckerboardTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.CheckerboardTransition;
        }

        public CheckerboardTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public CheckerboardTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
            Name = KeyLanguage.CheckerboardTransition;
            GroupName = "Exciting";
        }

        public CheckerboardTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            GroupName = "Exciting";
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new CheckerBoardTransitionOption()
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: NewsFlashTransition.cs
    // Description: Hiệu ứng chuyển trang
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------
    [Serializable]
    public class NewsFlashTransition : Transiton
    {
        public NewsFlashTransition() : base()
        {
        }

        public NewsFlashTransition(ITransitionableObject owner) : base(owner)
        {
            Name = KeyLanguage.NewsflashTransition;
        }

        public NewsFlashTransition(string name, TimeSpan duration) : base(name, duration)
        {
        }

        public NewsFlashTransition(TimeSpan duration, string image = "") : base(duration, image)
        {
        }

        public NewsFlashTransition(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            Name = KeyLanguage.NewsflashTransition;
            GroupName = "Sublte";
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new NewsFlashTransitionOption()
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlInclude(typeof(ArcsAnimation))]
    [XmlInclude(typeof(CircleAnimation))]
    [XmlInclude(typeof(CurveAnimation))]
    [XmlInclude(typeof(EqualTriangleAnimation))]
    [XmlInclude(typeof(FreeformAnimation))]
    [XmlInclude(typeof(LinesAnimation))]
    [XmlInclude(typeof(ScribbleAnimation))]
    [XmlInclude(typeof(SquareAnimation))]
    [XmlInclude(typeof(TrapezoidAnimation))]
    [XmlInclude(typeof(TurnsAnimation))]
    public abstract class MotionPathAnimation : Animation, IMotionPathAnimation
    {
        public MotionPathAnimation() : base()
        {
            AnimationType = eAnimationType.MotionPath;
        }

        public MotionPathAnimation(string name, TimeSpan duration) : base(name, duration)
        {
            AnimationType = eAnimationType.MotionPath;
        }

        public MotionPathAnimation(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            AnimationType = eAnimationType.MotionPath;
        }

        public MotionPathAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new OriginOption(),
                new EasingOptionGroup(),
                new PathOption()
            };
        }

        [XmlIgnore]
        public abstract string GroupName { get; }


        [XmlAttribute("owner")]
        public string XMotionPath
        {
            get
            {
                return Owner?.ID;
            }
        }
        [XmlAttribute("rev")]
        public bool ReversePathDirection
        {
            get
            {
                return GetOption(KeyLanguage.PathOption, ePath.ReversePathDirection.ToString());
            }

            set
            {
                SetOption(KeyLanguage.PathOption, ePath.ReversePathDirection.ToString(), value);
            }
        }
        [XmlAttribute("rel")]
        public bool RelativeStartPoint
        {
            get
            {
                return GetOption(KeyLanguage.PathOption, ePath.RelativeStartPoint.ToString());
            }

            set
            {
                SetOption(KeyLanguage.PathOption, ePath.RelativeStartPoint.ToString(), value);
            }
        }
        [XmlAttribute("ori")]
        public bool OrientShapeToPath
        {
            get
            {
                return GetOption(KeyLanguage.PathOption, ePath.OrientShapeToPath.ToString());
            }
            set
            {
                SetOption(KeyLanguage.PathOption, ePath.OrientShapeToPath.ToString(), value);
            }
        }
        [XmlAttribute("easingDir")]
        public eObjectAnimationOption EasingPathDirection
        {
            get
            {
                return GetOption<eObjectAnimationOption>(KeyLanguage.EasingPathDirectionOption, Enum.TryParse);
            }

            set
            {
                SetOption(KeyLanguage.EasingPathDirectionOption, value);
            }
        }
        [XmlAttribute("origin")]
        public eObjectAnimationOption Origin
        {
            get
            {
                return GetOption<eObjectAnimationOption>(KeyLanguage.OriginOption, Enum.TryParse);
            }
            set
            {
                SetOption(KeyLanguage.OriginOption, value);
            }
        }
        [XmlAttribute("speed")]
        public eObjectAnimationOption Speed
        {
            get
            {
                return GetOption<eObjectAnimationOption>(KeyLanguage.SpeedOption, Enum.TryParse);
            }

            set
            {
                SetOption(KeyLanguage.SpeedOption, value);
            }
        }
    }
}

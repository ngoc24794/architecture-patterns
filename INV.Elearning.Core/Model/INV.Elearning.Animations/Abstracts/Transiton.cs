﻿using System;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.Model;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlInclude(typeof(BlindsTransition))]
    [XmlInclude(typeof(CheckerboardTransition))]
    [XmlInclude(typeof(CircleTransition))]
    [XmlInclude(typeof(ClockTransition))]
    [XmlInclude(typeof(CoverTransition))]
    [XmlInclude(typeof(DiamondTransition))]
    [XmlInclude(typeof(DissolveTransition))]
    [XmlInclude(typeof(FadeTransition))]
    [XmlInclude(typeof(InTransition))]
    [XmlInclude(typeof(NewsFlashTransition))]
    [XmlInclude(typeof(NoneTransition))]
    [XmlInclude(typeof(OutTransition))]
    [XmlInclude(typeof(PlusTransition))]
    [XmlInclude(typeof(PushTransition))]
    [XmlInclude(typeof(RandomBarsTransition))]
    [XmlInclude(typeof(SplitTransition))]
    [XmlInclude(typeof(UnCoverTransition))]
    [XmlInclude(typeof(ZoomTransition))]
    public abstract class Transiton : Animation
    {
        [XmlIgnore]
        public new ITransitionableObject Owner { get; set; }
        public Transiton() : base()
        {
            AnimationType = eAnimationType.Slide;
        }

        public Transiton(string name, TimeSpan duration) : base(name, duration)
        {
            AnimationType = eAnimationType.Slide;
        }

        public Transiton(string name, TimeSpan duration, string image = "") : base(name, duration, image)
        {
            AnimationType = eAnimationType.Slide;

        }


        public Transiton(TimeSpan duration, string image = "") : this()
        {
        }

        public Transiton(ITransitionableObject owner) : this()
        {
            Owner = owner;
        }

        [XmlAttribute("opt")]
        public eTransitionOption Option { get => GetOption(); set => SetOption(value); }

        protected eTransitionOption GetOption()
        {
            if (EffectOptions != null)
            {
                foreach (IEffectOptionGroup group in EffectOptions)
                {
                    foreach (IEffectOption option in group.Values)
                    {
                        if (option.IsSelected)
                        {
                            if (Enum.TryParse(option.Value, out eTransitionOption value))
                                return value;
                        }
                    }
                }
            }
            return default(eTransitionOption);
        }

        protected void SetOption(eTransitionOption value)
        {
            if (EffectOptions != null)
                foreach (IEffectOptionGroup group in EffectOptions)
                {
                    foreach (IEffectOption option in group.Values)
                    {
                        option.IsSelected = false;
                        if (Enum.TryParse(option.Value, out eTransitionOption _value))
                        {
                            option.IsSelected = true;
                        }
                    }
                }
        }
        public string GroupName { get; set; }

        public override IElearningElement Clone()
        {
            var _animation = CloneTransitions.GetTransition(this.Name);
            CloneTransitions.CopyTransition(_animation, this);
            return _animation;

        }
    }


    public class CloneTransitions
    {
        public static void CopyTransition(Transiton transitionObject, Transiton transitionSelected)
        {
            transitionObject.Image = transitionSelected.Image;
            transitionObject.Duration = transitionSelected.Duration;
            transitionObject.Name = transitionSelected.Name;
            transitionObject.IsSequence = transitionSelected.IsSequence;
            if (transitionObject.EffectOptions != null && transitionSelected.EffectOptions != null)
                for (int i = 0; i < transitionObject.EffectOptions.Count; i++)
                {
                    for (int j = 0; j < transitionObject.EffectOptions[i].Values.Count; j++)
                    {
                        transitionObject.EffectOptions[i].Values[j].IsSelected = transitionSelected.EffectOptions[i].Values[j].IsSelected;
                    }
                    //  CopyEffectOptionGroup(transitionObject.EffectOptions[i] as EffectOptionGroup, transitionSelected.EffectOptions[i] as INV.Elearning.Animations.EffectOptionGroup);
                }


        }
        public static void CopyEffectOptionGroup(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {

                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        effectOptionAdapter.IsSelected = (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected;
                        j++;
                    }
                }
                else
                {
                    item.IsSelected = effectOptionSelected.Values[i].IsSelected;
                }

                i++;
            }
        }

        //public static void CopyEffectOption(EffectOption effectOptionObject, EffectOption effectOptionSelected)
        //{
        //   if (effectOptionObject != null && effectOptionSelected != null)
        //        effectOptionObject.IsSelected = effectOptionSelected.IsSelected;
        //}

        public static void CopySelectedEffectOption(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {

                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected = effectOptionAdapter.IsSelected;
                        j++;
                    }
                }
                else
                {
                    effectOptionSelected.Values[i].IsSelected = item.IsSelected;
                }

                i++;
            }
        }

        /// <summary>
        /// Trả về giá trị EffectOption khi so sánh 2 EffectOption
        /// </summary>
        /// <param name="effectOptionObject"></param>
        /// <param name="effectOptionSelected"></param>
        public static void CompareEffectOptionSelected(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {
                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        effectOptionAdapter.IsSelected = effectOptionAdapter.IsSelected && (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected;
                        j++;
                    }
                }
                else
                {
                    item.IsSelected = item.IsSelected && effectOptionSelected.Values[i].IsSelected;
                }

                i++;
            }
        }

        /// <summary>
        /// Khởi tạo giá trị của Transition với tên tương ứng
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Transiton GetTransition(string name)
        {

            switch (name)
            {
                case "None":
                    return new NoneTransition("None", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/None.png");
                case "Fade":
                    return new FadeTransition("Fade", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Fade.png");
                case "Push":
                    return new PushTransition("Push", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Grow.png");
                case "Split":
                    return new SplitTransition("Split", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Grow.png");
                case "RandomBars":
                    return new RandomBarsTransition("RandomBars", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FlyIn.png");
                case "Random Bars":
                    return new RandomBarsTransition("Random Bars", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FlyIn.png");
                case "Circle":
                    return new CircleTransition("Circle", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FloatIn.png");
                case "Diamond":
                    return new DiamondTransition("Diamond", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Split.png");
                case "Plus":
                    return new PlusTransition("Plus", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wipe.png");
                case "In":
                    return new InTransition("In", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Shape.png");
                case "Out":
                    return new OutTransition("Out", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wheel.png");
                case "UnCover":
                    return new UnCoverTransition("UnCover", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/RandomBars.png");
                case "Cover":
                    return new CoverTransition("Cover", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png");
                case "Newsflash":
                    return new NewsFlashTransition("Newsflash", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/SpinGrow.png");
                case "Dissolve":
                    return new DissolveTransition("Dissolve", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/GrowSpin.png");
                case "Checkerboard":
                    return new CheckerboardTransition("Checkerboard", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Zoom.png");
                case "Blinds":
                    return new BlindsTransition("Blinds", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Swivel.png");
                case "Clock":
                    return new ClockTransition("Clock", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Bounce.png");
                case "Zoom":
                    return new ZoomTransition("Zoom", TimeSpan.FromSeconds(2), " / INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Lines.png");
                default:
                    return new NoneTransition("None", TimeSpan.FromSeconds(2), "/INV.Elearning.Animations.UI;component/Images/Multiple.png");
            }
        }
    }
}

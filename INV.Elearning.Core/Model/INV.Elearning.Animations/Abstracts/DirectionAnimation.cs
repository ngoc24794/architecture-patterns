﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.View;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: DirectionAnimation.cs
    // Description: Tạo đối tượng animation có thuộc tính Direction
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/02/2018 : Create
    // 06/02/2018 : Modifer
    //---------------------------------------------------------------------------
    [Serializable]
    [XmlInclude(typeof(FadeAnimation))]
    [XmlInclude(typeof(FlyAnimation))]
    [XmlInclude(typeof(GrowAnimation))]
    [XmlInclude(typeof(GrowSpinAnimation))]
    [XmlInclude(typeof(SplitAnimation))]
    [XmlInclude(typeof(SwivelAnimation))]
    [XmlInclude(typeof(WipeAnimation))]
    [XmlInclude(typeof(ShapeAnimation))]
    [XmlInclude(typeof(WheelAnimation))]
    [XmlInclude(typeof(SpinAnimation))]
    [XmlInclude(typeof(SpinGrowAnimation))]
    [XmlInclude(typeof(RandomBarsAnimation))]


    public abstract class DirectionAnimation : Animation, IDirectionAnimation
    {
        public DirectionAnimation() : base()
        {
            InitalizeEffectOptions();
        }

        public DirectionAnimation(string name, TimeSpan duration) : base(name, duration)
        {
            InitalizeEffectOptions();
        }

        public DirectionAnimation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(name, duration, image, animationType, isSequence)
        {
            InitalizeEffectOptions();
        }
        public DirectionAnimation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : base(duration, image, animationType, isSequence)
        {
            InitalizeEffectOptions();
        }

        protected override void InitalizeEffectOptions()
        {
            EffectOptions = new List<IEffectOptionGroup>()
            {
                new MotionDirectionOption(AnimationType)
            };
            // if (IsSequence)
            EffectOptions.Add(new SequenceOption());
        }

        [XmlAttribute("dir")]
        public ePathDirection Direction
        {
            get
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == "Direction")
                        {
                            foreach (var itemEffect in item.Values)
                            {
                                if (itemEffect.IsSelected == true)
                                {
                                    if (KeyLanguage.Down.ToString().Equals(itemEffect.Name))
                                    {
                                        return ePathDirection.Down;
                                    }
                                    else if (KeyLanguage.Up.ToString().Equals(itemEffect.Name))
                                    {
                                        return ePathDirection.Up;
                                    }
                                    else if (KeyLanguage.Right.ToString().Equals(itemEffect.Name))
                                    {
                                        return ePathDirection.Right;
                                    }
                                    else if (KeyLanguage.Left.ToString().Equals(itemEffect.Name))
                                    {
                                        return ePathDirection.Left;
                                    }
                                }

                            }


                        }

                    }
                return ePathDirection.Down;
            }
            set { }
        }
        [XmlAttribute("motionDir")]
        public eMotionDirection MotionDirection
        {
            get
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == "Enter")
                        {

                            foreach (var itemEffectOption in item.Values)
                            {
                                if(itemEffectOption is EffectOptionAdapter)
                                {
                                    foreach (var itemEffect in (itemEffectOption as EffectOptionAdapter).Values)
                                    {
                                        if (itemEffect.IsSelected)
                                        {
                                            if (KeyLanguage.None.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.None;
                                            }
                                            else if (KeyLanguage.Bottom.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.Bottom;
                                            }
                                            else if (KeyLanguage.BottomLeft.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.BottomLeft;
                                            }
                                            else if (KeyLanguage.BottomRight.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.BottomRight;
                                            }
                                            else if (KeyLanguage.Left.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.Left;
                                            }
                                            else if (KeyLanguage.Right.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.Right;
                                            }
                                            else if (KeyLanguage.Top.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.Top;
                                            }
                                            else if (KeyLanguage.TopLeft.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.TopLeft;
                                            }
                                            else if (KeyLanguage.TopRight.ToString().Equals(itemEffect.Name))
                                            {
                                                return eMotionDirection.TopRight;
                                            }

                                        }
                                    }
                                }else
                                {
                                    if (itemEffectOption.IsSelected)
                                    {
                                        if (KeyLanguage.None.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.None;
                                        }
                                        else if (KeyLanguage.Bottom.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.Bottom;
                                        }
                                        else if (KeyLanguage.BottomLeft.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.BottomLeft;
                                        }
                                        else if (KeyLanguage.BottomRight.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.BottomRight;
                                        }
                                        else if (KeyLanguage.Left.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.Left;
                                        }
                                        else if (KeyLanguage.Right.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.Right;
                                        }
                                        else if (KeyLanguage.Top.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.Top;
                                        }
                                        else if (KeyLanguage.TopLeft.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.TopLeft;
                                        }
                                        else if (KeyLanguage.TopRight.ToString().Equals(itemEffectOption.Name))
                                        {
                                            return eMotionDirection.TopRight;
                                        }

                                    }
                                }

                                

                            }
                        }

                    }
                return eMotionDirection.None;
            }
            set
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == "Enter")
                        {
                            foreach (var itemEffect in item.Values)
                            {
                                if(itemEffect is EffectOptionAdapter)
                                {
                                    foreach (var itemAdapter in (itemEffect as EffectOptionAdapter).Values)
                                    {
                                        if (itemAdapter.Value.Equals(value.ToString()))
                                        {
                                            itemAdapter.IsSelected = true;
                                        }
                                        else
                                        {
                                            itemAdapter.IsSelected = false;
                                        }
                                    }
                                }
                                else
                                {
                                    if (itemEffect.Value.Equals(value.ToString()))
                                    {
                                        itemEffect.IsSelected = true;
                                    }
                                    else
                                    {
                                        itemEffect.IsSelected = false;
                                    }
                                }
                            }
                        }
                    }


            }
        }


    }
}

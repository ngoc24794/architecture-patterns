﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using INV.Elearing.Controls.Shapes;
using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: Animation.cs
    // Description: Lớp tạo đối tượng animation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 01/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp tạo đối tượng chuyển động "Animation"
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(ArcsAnimation))]
    [XmlInclude(typeof(BounceAnimation))]
    [XmlInclude(typeof(CircleAnimation))]
    [XmlInclude(typeof(CurveAnimation))]
    [XmlInclude(typeof(EqualTriangleAnimation))]
    [XmlInclude(typeof(FadeAnimation))]
    [XmlInclude(typeof(FloatAnimation))]
    [XmlInclude(typeof(FlyAnimation))]
    [XmlInclude(typeof(FreeformAnimation))]
    [XmlInclude(typeof(GrowAnimation))]
    [XmlInclude(typeof(GrowSpinAnimation))]
    [XmlInclude(typeof(LinesAnimation))]
    [XmlInclude(typeof(MultiAnimation))]
    [XmlInclude(typeof(NoneAnimation))]
    [XmlInclude(typeof(RandomBarsAnimation))]
    [XmlInclude(typeof(ScribbleAnimation))]
    [XmlInclude(typeof(ShapeAnimation))]
    [XmlInclude(typeof(SpinAnimation))]
    [XmlInclude(typeof(SplitAnimation))]
    [XmlInclude(typeof(SquareAnimation))]
    [XmlInclude(typeof(SwivelAnimation))]
    [XmlInclude(typeof(TrapezoidAnimation))]
    [XmlInclude(typeof(TurnsAnimation))]
    [XmlInclude(typeof(WheelAnimation))]
    [XmlInclude(typeof(WipeAnimation))]
    [XmlInclude(typeof(ZoomAnimation))]
    [XmlInclude(typeof(MotionPathAnimation))]
    [XmlInclude(typeof(DirectionAnimation))]
    //[XmlInclude(typeof(Transiton))]
    public abstract class Animation : RootModel, IAnimation
    {
        private TimeSpan _duration;

        public Animation()
        {
            InitalizeEffectOptions();
        }



        public Animation(string name, TimeSpan duration) : this()
        {
            Name = name;
            Duration = duration;
        }

        /// <summary>
        /// Hàm tạo
        /// </summary>
        /// <param name="name">Tên</param>
        /// <param name="duration">Thời gian diễn ra chuyển động</param>
        /// <param name="image">Biểu tượng</param>
        /// <param name="isMotionPath">Có phải hiệu ứng Motion Path không</param>
        /// <param name="isExit">Có phải hiệu ứng Exit không</param>
        /// <param name="isSequence">Có thuộc tính <see cref="Sequence"/> không</param>
        public Animation(string name, TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : this()
        {
            Name = name;
            Duration = duration;
            Image = image;
            AnimationType = animationType;
            IsSequence = isSequence;
        }

        public Animation(TimeSpan duration, string image = "", eAnimationType animationType = eAnimationType.Entrance, bool isSequence = false) : this()
        {
            Duration = duration;
            Image = image;
            AnimationType = animationType;
            IsSequence = isSequence;
        }

        ///// <summary>
        ///// Tên
        ///// </summary>
        //[XmlIgnore]
        //public string Name { get; set; }

        /// <summary>
        /// Thời gian diễn ra chuyển động
        /// </summary>
        [XmlIgnore]
        public TimeSpan Duration
        {
            get
            {
                return _duration;
            }

            set
            {
                _duration = value;
                OnPropertyChanged();
            }
        }
        [XmlAttribute("dur")]
        public double XDuration { get => Duration.TotalSeconds; set => Duration = TimeSpan.FromSeconds(value); }

        /// <summary>
        /// Biểu tượng
        /// </summary>
        [XmlIgnore]
        public string Image { get; set; }

        /// <summary>
        /// Có thuộc tính <see cref="Sequence"/> không
        /// </summary>
        [XmlIgnore]
        public bool IsSequence { get; set; }

        /// <summary>
        /// Thuộc tính Sequence
        /// </summary>
        [XmlAttribute("seq")]
        public eSequence Sequence
        {
            get
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == KeyLanguage.SequenceOption)
                        {
                            foreach (var itemEffect in item.Values)
                            {
                                if (itemEffect.IsSelected)
                                {
                                    if (KeyLanguage.OneObject.ToString().Equals(itemEffect.Name))
                                    {
                                        return eSequence.OneObject;
                                    }
                                    else if (KeyLanguage.Paragraph.ToString().Equals(itemEffect.Name))
                                    {
                                        return eSequence.Paragraph;
                                    }

                                }

                            }
                        }

                    }
                return eSequence.OneObject;
            }
            set
            {
                if (EffectOptions != null)
                    foreach (var item in EffectOptions)
                    {
                        if (item.GroupName == KeyLanguage.SequenceOption)
                        {
                            foreach (var itemEffect in item.Values)
                            {
                                if (itemEffect.Value.Equals(value.ToString()))
                                {
                                    itemEffect.IsSelected = true;
                                }
                                else
                                {
                                    itemEffect.IsSelected = false;
                                }
                            }
                        }

                    }


            }
        }

        [XmlIgnore]
        /// <summary>
        /// Danh sách tùy chọn của hiệu ứng
        /// </summary>
        public List<IEffectOptionGroup> EffectOptions { get; set; }

        /// <summary>
        /// Loại chuyển động
        /// </summary>
        [XmlAttribute("type")]
        public eAnimationType AnimationType { get; set; }

        [XmlIgnore]
        /// <summary>
        /// Đối tượng sở hữu chuyển động này
        /// </summary>
        public IAnimationableObject Owner { get; set; }
        [XmlAttribute("owner")]
        public string XOwner { get => Owner?.ID; }

        /// <summary>
        /// Khởi tạo các tùy chọn cho hiệu ứng
        /// </summary>
        protected virtual void InitalizeEffectOptions()
        {
            // được thi hành bởi các lớp dẫn xuất
        }

        protected delegate bool ParseCondition<TEnum>(string value, out TEnum result);
        protected TEnum GetOption<TEnum>(string groupName, ParseCondition<TEnum> tryParse)
        {
            IEffectOptionGroup effectOptionGroup = EffectOptions.FirstOrDefault(g => g.GroupName == groupName);
            if (effectOptionGroup != null)
            {
                foreach (IEffectOption option in effectOptionGroup.Values)
                {
                    if (option.IsSelected)
                    {
                        if (tryParse(option.Value, out TEnum value))
                            return value;
                    }
                }
            }
            else
            {
                foreach (IEffectOptionGroup group in EffectOptions)
                {
                    foreach (IEffectOption option in group.Values)
                    {
                        if (option is EffectOptionAdapter optionAdapter)
                        {
                            if (optionAdapter.Name == groupName)
                            {
                                foreach (IEffectOption item in optionAdapter.Values)
                                {
                                    if (item.IsSelected)
                                    {
                                        if (tryParse(item.Value, out TEnum value))
                                            return value;
                                    }
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }
            }

            return default(TEnum);
        }

        protected bool GetOption(string groupName, string optionValue)
        {
            IEffectOptionGroup effectOptionGroup = EffectOptions.FirstOrDefault(g => g.GroupName == groupName);
            if (effectOptionGroup != null)
            {
                foreach (IEffectOption option in effectOptionGroup.Values)
                {
                    if (option.Value == optionValue)
                    {
                        return option.IsSelected;
                    }
                }
            }
            else
            {
                foreach (IEffectOptionGroup group in EffectOptions)
                {
                    foreach (IEffectOption option in group.Values)
                    {
                        if (option is EffectOptionAdapter optionAdapter)
                        {
                            if (optionAdapter.Name == groupName)
                            {
                                foreach (IEffectOption item in optionAdapter.Values)
                                {
                                    if (item.Value == optionValue)
                                    {
                                        return item.IsSelected;
                                    }
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }
            }

            return false;
        }

        protected void SetOption<TEnum>(string groupName, TEnum value)
        {
            IEffectOptionGroup effectOptionGroup = EffectOptions.FirstOrDefault(g => g.GroupName == groupName);
            if (effectOptionGroup != null)
            {
                foreach (IEffectOption option in effectOptionGroup.Values)
                {
                    option.IsSelected = false;
                    if (option.Value == value.ToString())
                    {
                        option.IsSelected = true;
                    }
                }
            }
            else
            {
                foreach (IEffectOptionGroup group in EffectOptions)
                {
                    foreach (IEffectOption option in group.Values)
                    {
                        if (option is EffectOptionAdapter optionAdapter)
                        {
                            if (optionAdapter.Name == groupName)
                            {
                                foreach (IEffectOption item in optionAdapter.Values)
                                {
                                    item.IsSelected = false;
                                    if (item.Value == value.ToString())
                                    {
                                        item.IsSelected = true;
                                    }
                                }
                                return;
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        protected void SetOption(string groupName, string optionValue, bool value)
        {
            foreach (IEffectOptionGroup group in EffectOptions)
            {
                if (group.GroupName == groupName)
                    foreach (IEffectOption option in group.Values)
                    {
                        if (option.Value.ToString() == optionValue)
                        {
                            option.IsSelected = value;
                        }
                    }
            }

        }

        /// <summary>
        /// Nhân bản dữ liệu
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _animation = CloneAnimation.GetAnimation(this.Name, true);
            CloneAnimation.CopyAnimation(_animation, this);
            return _animation;
        }
    }

    /// <summary>
    /// Lớp hỗ trợ lưu, đọc dữ liệu hiệu ứng
    /// </summary>
    public static class CloneAnimation
    {

        public static void CopyObjectToAnimation(ObjectElement objectElement, Animation animationTarget, string typeAnimation)
        {
            if (typeAnimation == "Entrance")
            {
                if (objectElement == null || objectElement.EntranceAnimation == null) return;
                animationTarget = CloneAnimation.GetAnimation(objectElement.EntranceAnimation.Name, true);
                CopyAnimation(animationTarget, objectElement.EntranceAnimation as Animation);
            }
            else if (typeAnimation == "Exit")
            {
                if (objectElement == null || objectElement.ExitAnimation == null) return;
                animationTarget = CloneAnimation.GetAnimation(objectElement.ExitAnimation.Name, true);
                CopyAnimation(animationTarget, objectElement.ExitAnimation as Animation);
            }
        }

        public static void CopyAnimationToObject(Animation animationSource, ObjectElement objectElement, string typeAnimation)
        {
            if (typeAnimation == "Entrance")
            {
                if (animationSource == null || objectElement == null) return;
                objectElement.EntranceAnimation = CloneAnimation.GetAnimation(objectElement.EntranceAnimation.Name, true);
                CopyAnimation(objectElement.EntranceAnimation as Animation, animationSource);
            }
            else if (typeAnimation == "Exit")
            {
                if (animationSource == null || objectElement == null) return;
                objectElement.EntranceAnimation = CloneAnimation.GetAnimation(objectElement.ExitAnimation.Name, true);
                CopyAnimation(objectElement.ExitAnimation as Animation, animationSource);
            }

        }

        public static void CloneAnimationFull(Animation source, Animation target, string typeAnimation)
        {
            if (source == null) return;
            if (typeAnimation == "Entrance")
            {
                target = CloneAnimation.GetAnimation(source.Name, true);
                if (target?.Name != source.Name) return;
                CopyAnimation(target, source);
            }
            else if (typeAnimation == "Exit")
            {
                target = CloneAnimation.GetAnimation(source.Name, true);
                if (target?.Name != source.Name) return;
                CopyAnimation(target, source);
            }
        }

        public static void CopyAnimation(Animation animationObject, Animation animationSelected)
        {
            animationObject.Image = animationSelected.Image;
            animationObject.Duration = animationSelected.Duration;
            animationObject.Name = animationSelected.Name;
            animationObject.IsSequence = animationSelected.IsSequence;
            if (animationObject.EffectOptions != null && animationSelected.EffectOptions != null)
                for (int i = 0; i < animationObject.EffectOptions.Count; i++)
                {
                    CopyEffectOptionGroup(animationObject.EffectOptions[i] as EffectOptionGroup, animationSelected.EffectOptions[i] as INV.Elearning.Animations.EffectOptionGroup);
                }
            animationObject.AnimationType = animationSelected.AnimationType;
        }

        public static void ChangeDataAnimation(Animation animationObject)
        {
            if (animationObject is DirectionAnimation direction)
            {
                direction.MotionDirection = direction.MotionDirection;
            }
            if (animationObject is FloatAnimation floatA)
            {
                floatA.FloatDirection = floatA.FloatDirection;
                
            }

            if (animationObject is SplitAnimation splitA)
            {
                
                splitA.Direction = splitA.Direction;
            }
            if (animationObject is WipeAnimation wipeA)
            {
               
                wipeA.Direction = wipeA.Direction;
            }
            if (animationObject is ShapeAnimation shapeA)
            {
                shapeA.ShapeDirection = shapeA.ShapeDirection;
                shapeA.Direction = shapeA.Direction;
            }

            if (animationObject is WheelAnimation wheelA)
            {
                wheelA.Spokes = wheelA.Spokes;
                wheelA.Direction = wheelA.Direction;
            }

            if (animationObject is RandomBarsAnimation randomBarA)
            {
                
                randomBarA.Direction = randomBarA.Direction;
            }

            if (animationObject is SpinAnimation spinA)
            {
                spinA.Amount = spinA.Amount;
                spinA.Direction = spinA.Direction;
            }

            if (animationObject is SpinGrowAnimation spinGrowA)
            {
                spinGrowA.Amount = spinGrowA.Amount;
                spinGrowA.Direction = spinGrowA.Direction;
            }


            if (animationObject is SwivelAnimation swivelA)
            {
                
            }

            if (animationObject is ZoomAnimation zoomA)
            {
                zoomA.VanishingPoint = zoomA.VanishingPoint;
                
            }

           


        }

        public static void ConvertDataToAnimation(Animation animationObject)
        {
            if (animationObject is DirectionAnimation direction)
            {
                direction.MotionDirection = direction.MotionDirection;
                direction.Direction = direction.Direction;
            }

        }

        public static string GetNameEffectOption(IAnimation animation, string groupNameEffectOption)
        {
            // string result = "";
            if (animation.EffectOptions != null)
                foreach (var item in animation.EffectOptions)
                {
                    if (item.GroupName == groupNameEffectOption || ((groupNameEffectOption == "Speed" || groupNameEffectOption == "DirectionEasingPath") && item.GroupName == "Easing"))
                        if (item.Values[0] is EffectOption)
                        {
                            foreach (var item2 in item.Values)
                            {
                                if (item2.IsSelected == true)
                                {
                                    return item2.Name;
                                }
                            }

                        }
                        else if (item.Values[0] is EffectOptionAdapter)
                        {
                            foreach (var adapter in item.Values)
                            {
                                if (adapter.Name == groupNameEffectOption)
                                    foreach (var item2 in (adapter as EffectOptionAdapter).Values)
                                    {
                                        if (item2.IsSelected == true)
                                        {
                                            return item2.Name;
                                        }
                                    }
                            }
                        }
                }

            return "";
        }

        public static void CopyEffectOptionGroup(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {
                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        effectOptionAdapter.IsSelected = (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected;
                        j++;
                    }
                }
                else
                {
                    item.IsSelected = effectOptionSelected.Values[i].IsSelected;
                }
                i++;
            }
        }

        public static Animation SetEffectOption(Animation animation, string nameGroupEffectOption, string namEffectOptionSelected)
        {
            if (animation?.EffectOptions != null)
            {
                foreach (var groupEffectOption in animation.EffectOptions)
                {
                    if (groupEffectOption.GroupName == nameGroupEffectOption || (groupEffectOption.GroupName == "Easing" && (nameGroupEffectOption == "DirectionEasingPath" || nameGroupEffectOption == "Speed")))
                    {
                        foreach (var effectOption in groupEffectOption.Values)
                        {

                            if (effectOption is EffectOptionAdapter)
                            {
                                if (effectOption.Name == nameGroupEffectOption)
                                    foreach (var effectOptionAdapter in (effectOption as EffectOptionAdapter).Values)
                                    {
                                        if (effectOptionAdapter.Name == namEffectOptionSelected)
                                            effectOptionAdapter.IsSelected = true;
                                        else
                                            effectOptionAdapter.IsSelected = false;
                                    }
                            }
                            else
                            {
                                if (effectOption.Name == namEffectOptionSelected)
                                    effectOption.IsSelected = true;
                                else
                                    effectOption.IsSelected = false;
                            }
                        }
                        break;
                    }
                }
            }
            return animation;
        }

        public static void CopySelectedEffectOption(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {
                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected = effectOptionAdapter.IsSelected;
                        j++;
                    }
                }
                else
                {
                    effectOptionSelected.Values[i].IsSelected = item.IsSelected;
                }
                i++;
            }
        }

        /// <summary>
        /// Trả về giá trị EffectOption khi so sánh 2 EffectOption
        /// </summary>
        /// <param name="effectOptionObject"></param>
        /// <param name="effectOptionSelected"></param>
        public static void CompareEffectOptionSelected(EffectOptionGroup effectOptionObject, EffectOptionGroup effectOptionSelected)
        {
            int i = 0;
            foreach (var item in effectOptionObject.Values)
            {

                if (item is EffectOptionAdapter)
                {
                    int j = 0;
                    foreach (var effectOptionAdapter in (item as EffectOptionAdapter).Values)
                    {
                        effectOptionAdapter.IsSelected = effectOptionAdapter.IsSelected && (effectOptionSelected.Values[i] as EffectOptionAdapter).Values[j].IsSelected;
                        j++;
                    }
                }
                else
                {
                    item.IsSelected = item.IsSelected && effectOptionSelected.Values[i].IsSelected;
                }

                i++;
            }
        }


        public static Animation GetAnimation(string name, bool isSequence)
        {
            isSequence = true;
            switch (name)
            {
                case "None":
                    return new NoneAnimation("None", TimeSpan.FromSeconds(0), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/None.png");
                case "Fade":
                    return new FadeAnimation("Fade", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Fade.png", eAnimationType.Entrance, isSequence);
                case "Grow":
                    return new GrowAnimation("Grow", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Grow.png", eAnimationType.Entrance, isSequence);
                case "FlyIn":
                    return new FlyAnimation("FlyIn", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FlyIn.png", eAnimationType.Entrance, isSequence);
                case "FloatIn":
                    return new FloatAnimation("FloatIn", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FloatIn.png", eAnimationType.Entrance, isSequence);
                case "FlyOut":
                    return new FlyAnimation("FlyOut", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FlyIn.png", eAnimationType.Entrance, isSequence);
                case "FloatOut":
                    return new FloatAnimation("FloatOut", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/FloatIn.png", eAnimationType.Entrance, isSequence);
                case "Split":
                    return new SplitAnimation("Split", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Split.png", eAnimationType.Entrance, isSequence);
                case "Wipe":
                    return new WipeAnimation("Wipe", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wipe.png", eAnimationType.Entrance, isSequence);
                case "Shape":
                    return new ShapeAnimation("Shape", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Shape.png", eAnimationType.Entrance, isSequence);
                case "Wheel":
                    return new WheelAnimation("Wheel", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Wheel.png", eAnimationType.Entrance, isSequence);
                case "RandomBars":
                    var t = new RandomBarsAnimation("RandomBars", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/RandomBars.png", eAnimationType.Entrance, isSequence);
                    if (t.EffectOptions.Count() <= 2)
                    {
                        t.EffectOptions.Add(new SequenceOption());
                    }
                    return t;
                case "Spin":
                    return new SpinAnimation("Spin", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", eAnimationType.Entrance, isSequence);
                case "Spin&Grow":
                    return new SpinGrowAnimation("Spin&Grow", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/SpinGrow.png", eAnimationType.Entrance, isSequence);
                case "Grow&Spin":
                    return new GrowSpinAnimation("Grow&Spin", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/GrowSpin.png", eAnimationType.Entrance, isSequence);
                case "Zoom":
                    var zoom = new ZoomAnimation("Zoom", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Zoom.png", eAnimationType.Entrance, isSequence);
                    if (zoom.EffectOptions.Count() <= 1)
                    {
                        zoom.EffectOptions.Add(new SequenceOption());
                    }
                    return zoom;

                case "Swivel":
                    return new SwivelAnimation("Swivel", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Swivel.png", eAnimationType.Entrance, isSequence);
                case "Bounce":
                    return new BounceAnimation("Bounce", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Bounce.png", eAnimationType.Entrance, isSequence);
                case "Lines":
                    return new LinesAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Lines.png", eAnimationType.Entrance, isSequence);
                case "Arcs":
                    return new ArcsAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Arcs.png", eAnimationType.Entrance, isSequence);
                case "Turns":
                    return new TurnsAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Turns.png", eAnimationType.Entrance, isSequence);
                case "Circle":
                    return new CircleAnimation("Circle", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Circle.png");
                case "Square":
                    return new SquareAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Square.png", eAnimationType.Entrance, isSequence);
                case "Equal":
                    return new EqualTriangleAnimation("Equal Triangle", TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/EqualTriangle.png");
                case "Equal Triangle":
                    return new EqualTriangleAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Trapezoid.png", eAnimationType.Entrance, isSequence);
                case "Trapezoid":
                    return new TrapezoidAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Trapezoid.png", eAnimationType.Entrance, isSequence);
                case "Freeform":
                    return new FreeformAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Freeform.png", eAnimationType.Entrance, isSequence);
                case "Scribble":
                    return new ScribbleAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Scribble.png", eAnimationType.Entrance, isSequence);
                case "Curve":
                    return new CurveAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Animations/MontionPaths/Curve.png", eAnimationType.Entrance, isSequence);
                default:
                    return new MultiAnimation(TimeSpan.FromSeconds(1), "/INV.Elearning.Animations.UI;component/Images/Multiple.png", eAnimationType.Entrance, isSequence);
            }


        }
    }

    public class ConverterMotionPath
    {
        /// <summary>
        /// Dựng lại đối tượng 
        /// </summary>
        /// <param name="dataMotionPath"></param>
        /// <param name="motionPathObject"></param>
        public static void ConverterDataPathToPath(DataMotionPath dataMotionPath, out MotionPathObject motionPathObject, ObjectElement owner)
        {
            motionPathObject = null;
            (Application.Current as IAppGlobal)?.ConverterDataPathToPath(dataMotionPath, out motionPathObject, owner);
        }

        /// <summary>
        /// Lấy dữ liệu
        /// </summary>
        /// <param name="motionPathObject"></param>
        /// <param name="dataMotionPath"></param>
        public static void ConverterPathToDataPath(MotionPathObject motionPathObject, out DataMotionPath dataMotionPath)
        {

            dataMotionPath = new DataMotionPath();
            if ((motionPathObject as ObjectElement).Content is ShapeBase shape)
            {
                dataMotionPath.ShapesBase = shape.GetInfo() as MotionPathInfoBase;
            }
            dataMotionPath.XOwner = motionPathObject.Owner.ID;
            //dataMotionPath.Top = motionPathObject.Top;
            //dataMotionPath.Left = motionPathObject.Left;
            //dataMotionPath.Width = motionPathObject.Width;
            //dataMotionPath.Height = motionPathObject.Height;
            //dataMotionPath.IsScaleX = motionPathObject.IsScaleX;
            //dataMotionPath.IsScaleY = motionPathObject.IsScaleY;
            if (motionPathObject.XAnimation != null)
                dataMotionPath.AnimationPath = CloneAnimation.GetAnimation(motionPathObject.XAnimation.Name, true);
            CloneAnimation.CopyAnimation(dataMotionPath.AnimationPath, motionPathObject.XAnimation);
        }

    }
}

﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationFactory.cs
    // Description: Lớp tạo các Animation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 24/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp tạo các Animation
    /// </summary>
    [Serializable]
    public class AnimationFactory : AnimationFactoryBase
    {
        public override IAnimation CreateArcAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new ArcsAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateCircleAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new CircleAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateCurveAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new CurveAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateFreeformAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new FreeformAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateLineAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new LinesAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateScribbleAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new ScribbleAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateSquareAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new SquareAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateTrapesoidAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new TrapezoidAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateEqualTriangleAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new EqualTriangleAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateTurnsAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new TurnsAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateBounceAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new BounceAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateFadeAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new FadeAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateFloatAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new FloatAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateFlyAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new FlyAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateGrowAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new GrowAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateGrowSpinAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new GrowSpinAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateMultiAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new MultiAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateNoneAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new NoneAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateRandomBarsAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new RandomBarsAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateShapeAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new ShapeAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateSpinAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new SpinAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateSpinGrowAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new SpinGrowAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateSplitAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new SplitAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateSwivelAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new SwivelAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateWheelAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new WheelAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateWipeAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new WipeAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateZoomAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType)
        {
            return new ZoomAnimation(duration, "", animationType) { Owner = owner };
        }

        public override IAnimation CreateBlindsTransition(ITransitionableObject owner)
        {
            return new BlindsTransition(owner);
        }

        public override IAnimation CreateCheckerboardTransition(ITransitionableObject owner)
        {
            return new CheckerboardTransition(owner);
        }

        public override IAnimation CreateCircleTransition(ITransitionableObject owner)
        {
            return new CircleTransition(owner);
        }

        public override IAnimation CreateClockTransition(ITransitionableObject owner)
        {
            return new ClockTransition(owner);
        }

        public override IAnimation CreateCoverTransition(ITransitionableObject owner)
        {
            return new CoverTransition(owner);
        }

        public override IAnimation CreateDiamondTransition(ITransitionableObject owner)
        {
            return new DiamondTransition(owner);
        }

        public override IAnimation CreateDissolveTransition(ITransitionableObject owner)
        {
            return new DissolveTransition(owner);
        }

        public override IAnimation CreateFadeTransition(ITransitionableObject owner)
        {
            return new FadeTransition(owner);
        }

        public override IAnimation CreateInTransition(ITransitionableObject owner)
        {
            return new InTransition(owner);
        }

        public override IAnimation CreateNewsflashTransition(ITransitionableObject owner)
        {
            return new NewsFlashTransition(owner);
        }

        public override IAnimation CreateNoneTransition(ITransitionableObject owner)
        {
            return new NoneTransition(owner);
        }

        public override IAnimation CreateOutTransition(ITransitionableObject owner)
        {
            return new OutTransition(owner);
        }

        public override IAnimation CreatePlusTransition(ITransitionableObject owner)
        {
            return new PlusTransition(owner);
        }

        public override IAnimation CreatePushTransition(ITransitionableObject owner)
        {
            return new PushTransition(owner);
        }

        public override IAnimation CreateRandomBarsTransition(ITransitionableObject owner)
        {
            return new RandomBarsTransition(owner);
        }

        public override IAnimation CreateSplitTransition(ITransitionableObject owner)
        {
            return new SplitTransition(owner);
        }

        public override IAnimation CreateUnCoverTransition(ITransitionableObject owner)
        {
            return new UnCoverTransition(owner);
        }

        public override IAnimation CreateZoomTransition(ITransitionableObject owner)
        {
            return new ZoomTransition(owner);
        }
    }
}

﻿using INV.Elearning.Animations.Enums;
using System;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: AnimationFactoryBase.cs
    // Description: Lớp cơ sở để tạo AnimationFactory
    // Develope by : Nguyen Van Ngoc
    // History:
    // 24/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Lớp cơ sở để tạo AnimationFactory
    /// </summary>
    [Serializable]
    public abstract class AnimationFactoryBase
    {
        public abstract IAnimation CreateBounceAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateFadeAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateFloatAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateFlyAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateGrowAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateGrowSpinAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateMultiAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateNoneAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateRandomBarsAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateShapeAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateSpinAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateSpinGrowAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateSplitAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateSwivelAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateWheelAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateWipeAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateZoomAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateLineAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateArcAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateTurnsAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateCircleAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateSquareAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateEqualTriangleAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateTrapesoidAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateFreeformAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateScribbleAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateCurveAnimation(IAnimationableObject owner, TimeSpan duration, eAnimationType animationType);
        public abstract IAnimation CreateBlindsTransition(ITransitionableObject owner);
        public abstract IAnimation CreateCheckerboardTransition(ITransitionableObject owner);
        public abstract IAnimation CreateCircleTransition(ITransitionableObject owner);
        public abstract IAnimation CreateClockTransition(ITransitionableObject owner);
        public abstract IAnimation CreateCoverTransition(ITransitionableObject owner);
        public abstract IAnimation CreateDiamondTransition(ITransitionableObject owner);
        public abstract IAnimation CreateDissolveTransition(ITransitionableObject owner);
        public abstract IAnimation CreateFadeTransition(ITransitionableObject owner);
        public abstract IAnimation CreateInTransition(ITransitionableObject owner);
        public abstract IAnimation CreateNewsflashTransition(ITransitionableObject owner);
        public abstract IAnimation CreateNoneTransition(ITransitionableObject owner);
        public abstract IAnimation CreateOutTransition(ITransitionableObject owner);
        public abstract IAnimation CreatePlusTransition(ITransitionableObject owner);
        public abstract IAnimation CreatePushTransition(ITransitionableObject owner);
        public abstract IAnimation CreateRandomBarsTransition(ITransitionableObject owner);
        public abstract IAnimation CreateSplitTransition(ITransitionableObject owner);
        public abstract IAnimation CreateUnCoverTransition(ITransitionableObject owner);
        public abstract IAnimation CreateZoomTransition(ITransitionableObject owner);
    }
}

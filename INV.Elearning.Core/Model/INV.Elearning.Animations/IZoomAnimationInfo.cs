﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INV.Elearning.Animations
{
    public interface IZoomAnimationInfo : IAnimationInfo
    {
        eVanishingPoint VanishingPoint { get; set; }
    }
}
﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System;
using System.Windows;
using System.Xml.Serialization;

namespace INV.Elearning.Animations
{
    [Serializable]
    [XmlRoot("MotionPath")]
    public class DataMotionPath : ObjectElementBase
    {
        [XmlAttribute("t")]
        public double Top { get; set; }
        [XmlAttribute("l")]
        public double Left { get; set; }
        [XmlAttribute("w")]
        public double Width { get; set; }
        [XmlAttribute("h")]
        public double Height { get; set; }
        [XmlAttribute("rot")]
        public double Rotation { get; set; }
        [XmlAttribute("sx")]
        public bool IsScaleX { get; set; }
        [XmlAttribute("sy")]
        public bool IsScaleY { get; set; }
        //ID của đối tượng cha chứa đường path
        [XmlAttribute("idO")]
        public string XOwner { get; set; }
        [XmlElement("ani")]
        public Animation AnimationPath { get; set; }
        [XmlElement("shape")]
        public MotionPathInfoBase ShapesBase { get; set; }

        public override IElearningElement Clone()
        {
            var _result = new DataMotionPath();
            _result.ID = ID;
            _result.XOwner = this.XOwner;
            _result.Left = Left;
            _result.Top = Top;
            _result.Width = Width;
            _result.Height = Height;
            _result.Rotation = Rotation;
            _result.IsScaleX = IsScaleX;
            _result.IsScaleY = IsScaleY;
            _result.Name = Name;
            _result.Animations = Animations?.Clone() as EAnimation;
            if (this.AnimationPath != null)
            {
                _result.AnimationPath = CloneAnimation.GetAnimation(this.AnimationPath.Name, true);
                CloneAnimation.CopyAnimation(_result.AnimationPath, this.AnimationPath);
            }
            _result.ShapesBase = this.ShapesBase;
            return _result;

        }

        /// <summary>
        /// Dựng lại đối tượng
        /// </summary>
        /// <param name="dataMotionPath"></param>
        /// <returns></returns>
        public static MotionPathObject GetObject(DataMotionPath dataMotionPath, ObjectElement owner)
        {
            ConverterMotionPath.ConverterDataPathToPath(dataMotionPath, out MotionPathObject motionPathObject, owner);

            return motionPathObject;
        }
    }
}

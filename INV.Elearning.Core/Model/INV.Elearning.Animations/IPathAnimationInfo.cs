﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INV.Elearning.Animations
{
    public interface IPathAnimationInfo : IAnimationInfo
    {
        bool ReversePathDirection { get; set; }
        bool RelativeStartPoint { get; set; }
        bool OrientShapeToPath { get; set; }
        ePathDirection Direction { get; set; }
        eEasingPathDirection EasingPathDirection { get; set; }
        eOrigin Origin { get; set; }
        eSpeed Speed { get; set; }
    }
}
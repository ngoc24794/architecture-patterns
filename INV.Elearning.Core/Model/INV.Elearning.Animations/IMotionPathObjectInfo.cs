﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INV.Elearning.Animations
{
    public interface IMotionPathObjectInfo : IStageObject
    {
        PathAnimationInfo Animation { get; set; }
    }
}
﻿using INV.Elearning.Effects.Animations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INV.Elearning.Animations
{
    public interface IFloatAnimationInfo : IAnimationInfo
    {
        eFloatDirection FloatDirection { get; set; }
    }
}
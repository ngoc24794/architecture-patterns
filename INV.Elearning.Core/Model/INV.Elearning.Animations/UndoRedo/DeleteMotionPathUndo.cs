﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;

namespace INV.Elearning.Animations
{
    public class DeleteMotionPathUndo : StepBase
    {
        public DeleteMotionPathUndo(object source, LayoutBase layer)
        {
            Source = source;
            Layer = layer;
        }

        public LayoutBase Layer { get; set; }
        public override void RedoExcute()
        {
            Global.BeginInit();
            if (Source is MotionPathObject motionPathObject && motionPathObject.Owner is ObjectElement owner && Layer != null)
            {
                owner.MotionPaths?.Remove(motionPathObject);
                Layer.Children?.Remove(motionPathObject);
            }
            Global.EndInit();
        }

        public override void UndoExcute()
        {
            Global.BeginInit();
            if (Source is MotionPathObject motionPathObject && motionPathObject.Owner is ObjectElement owner && Layer != null)
            {
                if (owner.MotionPaths?.Contains(motionPathObject) == false)
                {
                    owner.MotionPaths?.Add(motionPathObject);
                    if (Layer.Children?.Contains(motionPathObject) == false)
                    {
                        Layer.Children.Add(motionPathObject);
                    }
                }
            }
            Global.EndInit();
        }
    }
}

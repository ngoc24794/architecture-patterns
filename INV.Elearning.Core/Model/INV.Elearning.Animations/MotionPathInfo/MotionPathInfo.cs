﻿using INV.Elearing.Controls.Shapes;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations
{
    [Serializable, XmlRoot("mpif")]
    public abstract class MotionPathInfoBase : ShapeInfo
    {
    }
}

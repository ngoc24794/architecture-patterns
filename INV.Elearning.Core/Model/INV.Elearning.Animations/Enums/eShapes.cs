﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eShapes
    {
        [XmlEnum("cc")]
        Circle,
        [XmlEnum("bx")]
        Box,
        [XmlEnum("dm")]
        Diamond,
        [XmlEnum("pl")]
        Plus
    }
}

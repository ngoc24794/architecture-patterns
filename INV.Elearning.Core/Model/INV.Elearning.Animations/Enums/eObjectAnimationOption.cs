﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations.Enums
{
    public enum eObjectAnimationOption
    {
        Quater,
        Half,
        Full,
        Two,
        Entrance,
        Exit,
        MotionPath,
        Slide,
        None,
        Top,
        Right,
        Bottom,
        Left,
        TopLeft,
        TopRight,
        BottomRight,
        BottomLeft,
        In,
        Out,
        InOut,
        Smoothly,
        ThroughBack,
        FloatUp,
        FloatDown,
        Locked,
        UnLocked,
        ReversePathDirection,
        RelativeStartPoint,
        OrientShapeToPath,
        Up,
        Down,
        Horizontal,
        Vertical,
        OneObject,
        Paragraph,
        Circle,
        Box,
        Diamond,
        Plus,
        Slow,
        Medium,
        Fast,
        VeryFast,
        Bounce,
        Clockwise,
        CounterClockwise,
        HorizontalIn,
        HorizontalOut,
        VerticalIn,
        VerticalOut,
        OneSpoke,       // 1
        TwoSpokes,      // 2
        ThreeSpokes,    // 3
        FourSpokes,     // 4
        EightSpokes,     // 8
        DownRight,
        UpRight,
        ObjectCenter,
        SlideCenter,
        Direction,
        Origin,
        EasingDirection,
        Speed,
        Path
    }
}

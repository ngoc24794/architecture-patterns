using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eSpeed
    {
        [XmlEnum("sl")]
        Slow,
        [XmlEnum("me")]
        Medium,
        [XmlEnum("fa")]
        Fast,
        [XmlEnum("vf")]
        VeryFast,
        [XmlEnum("bo")]
        Bounce
    }
}

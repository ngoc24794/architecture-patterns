﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace INV.Elearning.Animations.Enums
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: EVector.cs
    // Description: Vector được sử dụng khi làm việc với Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Vector được xác định bởi 2 điểm
    /// </summary>
    public struct EVector
    {
        /// <summary>
        /// Hàm tạo
        /// </summary>
        /// <param name="head">Điểm đầu</param>
        /// <param name="end">Điểm cuối</param>
        public EVector(Point head, Point end) : this()
        {
            Head = head;
            End = end;
        }

        /// <summary>
        /// Điểm đầu
        /// </summary>
        public Point Head { get; set; }

        /// <summary>
        /// Điểm cuối
        /// </summary>
        public Point End { get; set; }

        /// <summary>
        /// Trả về góc của vector này
        /// </summary>
        /// <returns>Trả về góc nghiêng của vector so với trục x. Giá trị trả về thuộc (0, 2PI)</returns>
        public double GetAngle()
        {
            Point point = ConvertToPoint();
            double
                x = point.X,
                y = point.Y;

            return EAtan(y, x);
        }

        /// <summary>
        /// Hỗ trợ tính atan
        /// </summary>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        private double EAtan(double y, double x)
        {
            if (x == 0)
            {
                if (y > 0)
                    return PI / 2;

                if (y < 0)
                    return 3 * PI / 2;

                if (y == 0)
                    return double.NaN;
            }

            double angle = Math.Atan(y / x);

            if (x < 0)
            {
                return angle + PI;
            }

            if (y < 0)
            {
                return angle + 2 * PI;
            }

            return angle;
        }

        /// <summary>
        /// Chuyển vector này thành điểm
        /// </summary>
        /// <returns></returns>
        public Point ConvertToPoint()
        {
            double
                x = End.X - Head.X,
                y = End.Y - Head.Y;

            return new Point(x, y);
        }

        public double GetLength()
        {
            Point point = ConvertToPoint();
            return Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));
        }

        public static EVector operator *(EVector eVector, double a)
        {
            Vector vector = (Vector)eVector.ConvertToPoint() * a;

            Point head = eVector.Head;
            Vector
                vHead = (Vector)head,
                vEnd = vHead + vector;
            Point end = (Point)vEnd;

            return new EVector(head, end);
        }

        public static EVector operator -(EVector eVector)
        {
            return new EVector(eVector.End, eVector.Head);
        }

        private const double PI = Math.PI;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations.Enums
{
    public enum eSplitOption
    {
        VerticalOut,
        VerticalIn,
        HorizontalOut,
        HorizontalIn
    }
}

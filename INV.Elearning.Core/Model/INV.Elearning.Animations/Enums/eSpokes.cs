﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eSpokes
    {
        [XmlEnum("1")]
        OneSpoke,       // 1
        [XmlEnum("2")]
        TwoSpokes,      // 2
        [XmlEnum("3")]
        ThreeSpokes,    // 3
        [XmlEnum("4")]
        FourSpokes,     // 4
        [XmlEnum("8")]
        EightSpokes     // 8
    }
}

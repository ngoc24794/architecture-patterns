﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eTransitionOption
    {
        [XmlEnum("sm")]
        Smoothly,
        [XmlEnum("th")]
        ThroughBack,
        [XmlEnum("b")]
        Bottom,
        [XmlEnum("l")]
        Left,
        [XmlEnum("r")]
        Right,
        [XmlEnum("t")]
        Top,
        [XmlEnum("vo")]
        VerticalOut,
        [XmlEnum("vi")]
        VerticalIn,
        [XmlEnum("ho")]
        HorizontalOut,
        [XmlEnum("hi")]
        HorizontalIn,
        [XmlEnum("v")]
        Vertical,
        [XmlEnum("h")]
        Horizontal,
        [XmlEnum("tr")]
        TopRight,
        [XmlEnum("br")]
        BottomRight,
        [XmlEnum("tl")]
        TopLeft,
        [XmlEnum("bl")]
        BottomLeft,
        [XmlEnum("cw")]
        Clockwise,
        [XmlEnum("ccw")]
        CounterClockwise,
        [XmlEnum("in")]
        In, 
        [XmlEnum("out")]
        Out
    }
}

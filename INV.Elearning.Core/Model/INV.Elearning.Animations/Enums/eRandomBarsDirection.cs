using System.Xml.Serialization;

namespace INV.Elearning.Effects.Animations
{
    public enum eRandomBarsDirection
	{
        [XmlEnum("hor")]
        Horizontal,
        [XmlEnum("ver")]
        Vertical
	}
}

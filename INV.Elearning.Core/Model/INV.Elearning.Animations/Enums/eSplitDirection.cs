﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eSplitDirection
    {
        [XmlEnum("hi")]
        HorizontalIn,
        [XmlEnum("ho")]
        HorizontalOut,
        [XmlEnum("vi")]
        VerticalIn,
        [XmlEnum("vo")]
        VerticalOut
    }
}

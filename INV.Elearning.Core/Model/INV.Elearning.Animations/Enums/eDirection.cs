﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eMotionDirection
    {
        [XmlEnum("none")]
        None,
        [XmlEnum("t")]
        Top,
        [XmlEnum("r")]
        Right,
        [XmlEnum("b")]
        Bottom,
        [XmlEnum("l")]
        Left,
        [XmlEnum("tl")]
        TopLeft,
        [XmlEnum("tr")]
        TopRight,
        [XmlEnum("br")]
        BottomRight,
        [XmlEnum("bl")]
        BottomLeft
    }
}

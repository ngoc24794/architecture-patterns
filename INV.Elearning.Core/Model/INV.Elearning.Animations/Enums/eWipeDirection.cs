﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eWipeDirection
    {
        [XmlEnum("t")]
        Top,
        [XmlEnum("b")]
        Bottom,
        [XmlEnum("l")]
        Left,
        [XmlEnum("r")]
        Right
    }
}

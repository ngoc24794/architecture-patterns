using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum ePathDirection
    {
        [XmlEnum("d")]
        Down,
        [XmlEnum("u")]
        Up,
        [XmlEnum("l")]
        Left,
        [XmlEnum("r")]
        Right
    }
}

using System.Xml.Serialization;

namespace INV.Elearning.Effects.Animations
{
    public enum eShapeDirection
	{
        [XmlEnum("in")]
        In,
        [XmlEnum("out")]
        Out
    }
}

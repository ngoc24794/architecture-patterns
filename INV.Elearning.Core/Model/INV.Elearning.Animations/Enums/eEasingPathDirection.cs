using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
	public enum eEasingPathDirection
	{
        [XmlEnum("none")]
        None,
        [XmlEnum("in")]
        In,
        [XmlEnum("out")]
        Out,
        [XmlEnum("inout")]
        InOut
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eOrigin
    {
        [XmlEnum("loc")]
        Locked,
        [XmlEnum("unl")]
        UnLocked
    }
}

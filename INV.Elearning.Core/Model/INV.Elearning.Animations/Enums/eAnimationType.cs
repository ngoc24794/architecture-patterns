﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
    public enum eAnimationType
    {
        [XmlEnum("en")]
        Entrance,
        [XmlEnum("ex")]
        Exit,
        [XmlEnum("mo")]
        MotionPath,
        [XmlEnum("sl")]
        Slide
    }
}

using System;
using System.Xml.Serialization;

namespace INV.Elearning.Effects.Animations
{
	public enum eFloatDirection
	{
        [XmlEnum("fu")]
        FloatUp,
        [XmlEnum("fd")]
        FloatDown
    }
}

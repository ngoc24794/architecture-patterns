using System;
using System.Xml.Serialization;

namespace INV.Elearning.Animations.Enums
{
	public enum eSequence
	{
        [XmlEnum("object")]
        OneObject,
        [XmlEnum("para")]
        Paragraph
    }
}

﻿using INV.Elearning.Effects.Animations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{
    public interface IFloatAnimation : IAnimation
    {
        eFloatDirection FloatDirection { get; set; }
    }
}

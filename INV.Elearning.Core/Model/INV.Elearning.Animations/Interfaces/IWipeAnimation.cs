﻿using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    public interface IWipeAnimation : IDirectionAnimation
    {
        eWipeDirection WipeDirection { get; set; }
    }
}

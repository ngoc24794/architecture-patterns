﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations
{
    public interface IObjectElementInfo : IStageObject
    {
        AnimationEffect Animation { get; set; }
    }

    public class AnimationEffect
    {
        [XmlElement("en")]
        public AnimationInfo EntranceAnimation { get; set; }
        [XmlElement("ex")]
        public AnimationInfo ExitAnimation { get; set; }
    }
}

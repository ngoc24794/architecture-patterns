﻿
using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: IRandomBarsAnimation.cs
    // Description: Interface cho RandomBarsAnimation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Create
    //---------------------------------------------------------------------------

    public interface IRandomBarsAnimation : IDirectionAnimation
    {
        eRandomBarsDirection RandomBarsDirection { get; set; }
    }
}

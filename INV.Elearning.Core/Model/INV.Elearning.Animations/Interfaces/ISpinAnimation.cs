﻿using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{
    public interface ISpinAnimation : IDirectionAnimation
    {
        eSpinDirection SpinDirection { get; set; }
        eAmount Amount { get; set; }
    }
}

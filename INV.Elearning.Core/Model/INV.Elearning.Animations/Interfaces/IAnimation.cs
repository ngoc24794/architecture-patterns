﻿using INV.Elearning.Animations.Enums;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: IAnimation.cs
    // Description: Interface cho đối tượng Animation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 06/02/2018 : Modifer
    //---------------------------------------------------------------------------

    public interface IAnimation
    {
        IAnimationableObject Owner { get; set; }
        string XOwner { get;  }
        string Name { get; set; }
        string Image { get; set; }
        eAnimationType AnimationType { get; set; }
        bool IsSequence { get; set; }
        eSequence Sequence { get; set; }
        double XDuration { get; set; }
        TimeSpan Duration { get; set; }
        List<IEffectOptionGroup> EffectOptions { get; set; }
    }
}

﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: IDirectionableAnimation.cs
    // Description: Interface cho đối tượng có thuộc tính Direction
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/02/2018 : Create
    //---------------------------------------------------------------------------

    public interface IDirectionableAnimation
    {
        eObjectAnimationOption Direction { get; set; }
    }
}

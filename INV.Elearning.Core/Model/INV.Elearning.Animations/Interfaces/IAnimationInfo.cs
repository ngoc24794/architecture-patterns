﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INV.Elearning.Animations
{
    public interface IAnimationInfo
    {
        double Duration { get; set; }

        eAnimationType Type { get; set; }

        eSequence Sequence { get; set; }

        string Name { get; set; }
    }
}
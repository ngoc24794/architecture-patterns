﻿using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace INV.Elearning.Animations
{
    public interface IStageObject : IObjectElement
    {
        //---------------------------------------------------------------------------
        // Các thuộc tính chung
        //---------------------------------------------------------------------------
        /// <summary>
        /// Tên
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Lưu nội dung của đối tượng. Nội dung có thể là hình vẽ, hình ảnh, phim,...
        /// </summary>
        object Content { get; set; }
        /// <summary>
        /// Chiều rộng
        /// </summary>
        double Width { get; set; }
        /// <summary>
        /// Chiều cao
        /// </summary>
        double Height { get; set; }
        bool IsScaleX { get; set; }
        bool IsScaleY { get; set; }
    }
}
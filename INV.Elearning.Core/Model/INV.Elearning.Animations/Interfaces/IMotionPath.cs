﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: IMotionPath.cs
    // Description: Interface cho đối tượng Motion Path
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/02/2018 : Create
    //---------------------------------------------------------------------------

    public interface IMotionPath
    {
        IMotionPathAnimation Owner { get; set; }
        IMotionPathObject Container { get; set; }
        string Data { get; set; }
        EVector VHead { get; set; }
        EVector VEnd { get; set; }
    }
}

﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Animations
{
    
    public interface IEffectOption
    {
        [XmlAttribute("na")]
        string Name { get; set; }
        [XmlAttribute("img")]
        string Image { get; set; }
        [XmlAttribute("val")]
        string Value { get; set; }
        [XmlAttribute("sel")]
        bool IsSelected { get; set; }
        [XmlAttribute("gr")]
        string GroupName { get; set; }
        [XmlAttribute("ty")]
        eObjectAnimationOption Type { get; set; }
    }
    
    public interface IEffectOptionGroup
    {
        [XmlAttribute("gr")]
        string GroupName { get; set; }
        [XmlArray("vals")]
        List<IEffectOption> Values { get; set; }
    }
}

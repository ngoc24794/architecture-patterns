﻿using INV.Elearning.Animations.Enums;
using INV.Elearning.Effects.Animations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{
    public interface IShapeAnimation : IDirectionAnimation
    {
        eShapeDirection ShapeDirection { get; set; }
        eShapes Shapes { get; set; }
    }
}

﻿using INV.Elearning.Core.View;

namespace INV.Elearning.Animations
{
    public interface IMotionPathObject
    {
        ObjectElement Owner { get; set; }
        IMotionPathAnimation Animation { get; set; }
        DataMotionPath GetData();
    }
}
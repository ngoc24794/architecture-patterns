﻿using INV.Elearning.Animations.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: IMotionPathAnimation.cs
    // Description: Interface cho đối tượng Motion Path Animation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/02/2018 : Create
    //---------------------------------------------------------------------------

    public interface IMotionPathAnimation : IAnimation
    {
    }
}

﻿using INV.Elearning.Animations.Enums;

namespace INV.Elearning.Animations
{
    public interface IZoomAnimation : IAnimation
    {
        eVanishingPoint VanishingPoint { get; set; }
    }
}

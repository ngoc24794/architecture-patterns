﻿using System.Collections.ObjectModel;

namespace INV.Elearning.Animations
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: IAnimationableObject.cs
    // Description: Đối tượng có Animation
    // Develope by : Nguyen Van Ngoc
    // History:
    // 26/02/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Đối tượng có Animation
    /// </summary>
    public interface IAnimationableObject
    {
        string ID { set; get; }
        /// <summary>
        /// Xác định đối tượng có đang sở hữu Animation nào hay không
        /// </summary>
        bool IsHasAnimation { get; set; }

        //---------------------------------------------------------------------------
        // Các Animation sở hữu
        //---------------------------------------------------------------------------
        IAnimation EntranceAnimation { get; set; }
        IAnimation ExitAnimation { get; set; }

        //---------------------------------------------------------------------------
        // Liên kết với các đường Motion Path, được sử dụng để: 
        // 1. định vị các đường Motion Path luôn bám theo khi đối tượng này di chuyển
        // 2. đọc các thuộc tính của MotionPathAnimation có trong đường MotionPath 
        // tương ứng
        //---------------------------------------------------------------------------
        ObservableCollection<IMotionPathObject> MotionPaths { get; set; }
    }
}
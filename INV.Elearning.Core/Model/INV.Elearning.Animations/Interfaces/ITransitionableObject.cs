﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Animations
{
    public interface ITransitionableObject
    {
        IAnimation Transition { get; set; }
    }
}

﻿
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp đối tượng hiệu ứng tỏa sáng
    /// </summary>
    [Serializable]
    [XmlRoot("glow")]
    public class GlowEffect : FrameEffectBase
    {
        private string _color;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính màu hiệu ứng
        /// </summary>
        [XmlAttribute("color")]
        public string Color
        {
            get { return _color; }
            set { _color = value; OnPropertyChanged("Color"); }
        }

        /// <summary>
        /// Nhân bản hiệu ứng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new GlowEffect() { Color = this.Color, Size = this.Size, Transparency = this.Transparency };
        }
    }
}

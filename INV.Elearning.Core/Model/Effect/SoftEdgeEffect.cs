﻿
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("softedge")]
    public class SoftEdgeEffect : FrameEffectBase
    {
        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new SoftEdgeEffect() { Size = this.Size };
        }
    }
}

﻿using INV.Elearning.Core.Helper.Effect;
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp lưu trữ hiệu ứng đổ bóng
    /// </summary>
    [Serializable]
    [XmlRoot("shadow")]
    public class ShadowEffect : FrameEffectBase
    {
        private string _color;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính màu sắc cho hiệu ứng
        /// </summary>
        [XmlAttribute("color")]
        public string Color
        {
            get { return _color; }
            set { _color = value; OnPropertyChanged("Color"); }
        }

        private double _blur;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính độ mờ của bóng
        /// </summary>
        [XmlAttribute("blur")]
        public double Blur
        {
            get { return _blur; }
            set { _blur = value; OnPropertyChanged("Blur"); }
        }

        private double _angle;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính góc quay
        /// </summary>
        [XmlAttribute("angle")]
        public double Angle
        {
            get { return _angle; }
            set { _angle = value; OnPropertyChanged("Angle"); }
        }

        private double _distance;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính khoảng cách
        /// </summary>
        [XmlAttribute("dist")]
        public double Distance
        {
            get { return _distance; }
            set { _distance = value; OnPropertyChanged("Distance"); }
        }


        #region Triều thêm

        private bool isSize;
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("isSize")]
        public bool IsSize
        {
            get { return isSize; }
            set
            {
                isSize = value;
                OnPropertyChanged("IsSize");
            }
        }

        private double _fakeSize;
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute("f")]
        public double FakeSize
        {
            get { return _fakeSize; }
            set
            {
                _fakeSize = value;
                OnPropertyChanged("FakeSize");
                //this.IsSize = true;
            }
        }

        private ShadowType _type;
        /// <summary>
        /// Kiểu đỗ bóng
        /// </summary>
        [XmlAttribute("type")]
        public ShadowType Type
        {
            get { return _type; }
            set { _type = value; OnPropertyChanged("Type"); }
        }

        private PerspectiveShadowEnum _perspectiveShadowType;
        /// <summary>
        /// 
        /// </summary>
        public PerspectiveShadowEnum PerspectiveShadowType
        {
            get { return _perspectiveShadowType; }
            set
            {
                _perspectiveShadowType = value;
                OnPropertyChanged("PerspectiveShadowType");
            }
        }
        #endregion

        #region Thêm 2408
        /// <summary>
        /// Data phần inner
        /// </summary>
        [XmlAttribute("dataInner")]
        private string _dataInner;

        public string DataInner
        {
            get { return _dataInner; }
            set { _dataInner = value; }
        }

        #endregion

        /// <summary>
        /// Nhân bản hiệu ứng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new ShadowEffect() { Angle = this.Angle, Blur = this.Blur, Color = this.Color, Distance = this.Distance, Size = this.Size, Transparency = this.Transparency, IsSize = this.IsSize, FakeSize = this.FakeSize, Type = this.Type, PerspectiveShadowType = this.PerspectiveShadowType, DataInner = this.DataInner };
        }
    }

    /// <summary>
    /// Các loại đổ bóng
    /// </summary>
    public enum ShadowType
    {
        /// <summary>
        /// Không đỗ bóng
        /// </summary>
        None,
        /// <summary>
        /// Đỗ bóng ngoài
        /// </summary>
        [XmlEnum("out")]
        Outer,
        /// <summary>
        /// Đỗ bóng trong
        /// </summary>
        [XmlEnum("in")]
        Inner,
        /// <summary>
        /// Đỗ bóng xiên
        /// </summary>
        [XmlEnum("per")]
        Perspective
    }
}

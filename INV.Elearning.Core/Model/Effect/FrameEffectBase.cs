﻿
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp các gốc cho các đối tượng hiệu ứng
    /// </summary>
    [Serializable]
    [XmlRoot("fEff")]
    public abstract class FrameEffectBase : RootModel
    {
        private double _transparency;
        /// <summary>
        /// Độ trong suốt của hiệu ứng
        /// </summary>
        [XmlAttribute("trans")]
        public double Transparency
        {
            get { return _transparency; }
            set { _transparency = value; OnPropertyChanged("Transparency"); }
        }

        private double _size;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính kích cỡ của hình chiếu hiệu ứng
        /// </summary>
        [XmlAttribute("size")]
        public double Size
        {
            get { return _size; }
            set { _size = value; OnPropertyChanged("Size"); }
        }

    }
}

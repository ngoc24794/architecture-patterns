﻿
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    [Serializable]
    [XmlRoot("refl")]
    public class ReflectionEffect : FrameEffectBase
    {
        private double _blur;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính mờ cho hiệu ứng
        /// </summary>
        [XmlAttribute("blur")]
        public double Blur
        {
            get { return _blur; }
            set { _blur = value; OnPropertyChanged("Blur"); }
        }

        private double _distance;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính khoảng cách của hiệu ứng
        /// </summary>
        [XmlAttribute("dist")]
        public double Distance
        {
            get { return _distance; }
            set { _distance = value; OnPropertyChanged("Distance"); }
        }

        /// <summary>
        /// Nhân bản một hiệu ứng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new ReflectionEffect() { Blur = this.Blur, Distance = this.Distance, Size = this.Size, Transparency = this.Transparency };
        }
    }
}

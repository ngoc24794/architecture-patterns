﻿namespace INV.Elearning.Core.Model.Theme
{
    public interface IThemeSupport
    {
        /// <summary>
        /// Cập nhật dữ liệu màu sắc khi các thông số màu của theme thay đổi
        /// </summary>
        void UpdateThemeColor();
        /// <summary>
        /// Cập nhật dữ liệu kiểu chữ
        /// </summary>
        void UpdateThemeFont();
    }

    public interface IThemeSupportExt : IThemeSupport
    {
        /// <summary>
        /// Cập nhật dữ liệu màu sắc khi khung nhìn thay đổi
        /// </summary>
        void UpdateLayout(ELayoutMaster eLayoutMaster);
    }
}

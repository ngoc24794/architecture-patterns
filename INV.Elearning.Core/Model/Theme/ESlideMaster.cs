﻿

namespace INV.Elearning.Core.Model.Theme
{
    using System;
    using System.Collections.ObjectModel;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp lưu trữ dữ liệu Slide Master
    /// </summary>
    [XmlRoot("slidemaster")]
    [Serializable]
    public class ESlideMaster : PageElementBase
    {


        private string _slideName;
        /// <summary>
        /// Tên của Slide
        /// </summary>
        [XmlAttribute("slideName")]
        public string SlideName
        {
            get { return _slideName; }
            set { _slideName = value; }
        }


        private string _themesName;
        /// <summary>
        /// Tên của theme
        /// </summary>
        [XmlAttribute("theName")]
        public string ThemesName
        {
            get { return _themesName; }
            set { _themesName = value; }
        }

        private int _themeCount = 0;
        /// <summary>
        /// Đếm số slide sử dụng theme
        /// </summary>
        [XmlIgnore]
        public int ThemeCount
        {
            get { return _themeCount; }
            set { _themeCount = value; }
        }


        private EColorManagment _colors;
        /// <summary>
        /// Colors
        /// </summary>
        [XmlElement("colors")]
        public EColorManagment Colors
        {
            get { return _colors ?? (_colors = new EColorManagment()); }
            set { _colors = value; }
        }

        private EFontfamily _selectedFont;
        /// <summary>
        /// SelectedFont
        /// </summary>
        [XmlElement("fonts")]
        public EFontfamily SelectedFont
        {
            get { return _selectedFont ?? (_selectedFont = new EFontfamily()); }
            set { _selectedFont = value; }
        }

        private string _selectedThemeID;
        /// <summary>
        /// ID của theme được chọn
        /// </summary>
        [XmlIgnore]
        public string SelectedThemeID
        {
            get { return _selectedThemeID; }
            set { _selectedThemeID = value; }
        }

        private bool _isUpdateFooter;
        /// <summary>
        /// Kiểm tra xem có cập nhật chân trang hay không
        /// </summary>
        [XmlAttribute("isUpFots")]
        public bool IsUpdateFooter
        {
            get { return _isUpdateFooter; }
            set { _isUpdateFooter = value; }
        }

        private bool _isUpdateSlideNumber;
        /// <summary>
        /// Kiểm tra xem có cập nhật số trang hay không
        /// </summary>
        [XmlAttribute("isUpSldNus")]
        public bool IsUpdateSlideNumber
        {
            get { return _isUpdateSlideNumber; }
            set { _isUpdateSlideNumber = value; }
        }

        private bool _isUpdateDateTime;
        /// <summary>
        /// Kiểm tra xem có cập nhật ngày giờ hay không
        /// </summary>
        [XmlAttribute("isUpDTs")]
        public bool IsUpdateDateTime
        {
            get { return _isUpdateDateTime; }
            set { _isUpdateDateTime = value; }
        }



        private ObservableCollection<ELayoutMaster> _layoutMasters;
        /// <summary>
        /// Danh sách các layout master
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<ELayoutMaster> LayoutMasters
        {
            get
            {
                if (_layoutMasters == null)
                {
                    _layoutMasters = new ObservableCollection<ELayoutMaster>();
                    _layoutMasters.CollectionChanged += _layoutMasters_CollectionChanged;
                }
                return _layoutMasters;
            }
            set
            {
                if (_layoutMasters != null)
                {
                    _layoutMasters.CollectionChanged -= _layoutMasters_CollectionChanged;
                }
                _layoutMasters = value;
                _layoutMasters.CollectionChanged += _layoutMasters_CollectionChanged;
            }
        }



        private void _layoutMasters_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (ELayoutMaster item in e.NewItems)
                    {
                        item.Parent = this;
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (ELayoutMaster item in e.OldItems)
                    {
                        item.Parent = null;
                    }
                    break;
            }
        }

        public override IElearningElement Clone()
        {
            ESlideMaster eSlideMaster = new ESlideMaster();
            base.Copy(eSlideMaster);
            eSlideMaster.Name = Name;
            eSlideMaster.ID = ID;
            eSlideMaster.SlideName = SlideName;
            eSlideMaster.IsTitle = IsTitle;
            eSlideMaster.IsText = IsText;
            eSlideMaster.IsDate = IsDate;
            eSlideMaster.IsFooters = IsFooters;
            eSlideMaster.IsSlideNumber = IsSlideNumber;
            eSlideMaster.ThemesName = ThemesName;
            eSlideMaster.ThemeCount = ThemeCount;
            eSlideMaster.MainLayer = MainLayer.Clone() as PageLayer;
            eSlideMaster.LayoutMasters.Clear();
            eSlideMaster.SelectedThemeID = SelectedThemeID;
            eSlideMaster.IsUpdateDateTime = IsUpdateDateTime;
            eSlideMaster.IsUpdateFooter = IsUpdateFooter;
            eSlideMaster.IsUpdateSlideNumber = IsUpdateSlideNumber;
            eSlideMaster.Colors = Colors?.Clone() as EColorManagment;
            eSlideMaster.SelectedFont = SelectedFont?.Clone() as EFontfamily;
            foreach (ELayoutMaster item in LayoutMasters)
            {
                eSlideMaster.LayoutMasters.Add(item.Clone() as ELayoutMaster);
            }
            return eSlideMaster;
        }


    }
}

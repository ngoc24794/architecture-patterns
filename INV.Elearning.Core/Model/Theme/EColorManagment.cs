﻿
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp quản lý các màu sắc của Variant
    /// </summary>
    [Serializable]
    [XmlRoot("mcolor")]
    public class EColorManagment : RootModel
    {
        private SolidColor _backgroundDark1 = null;
        private SolidColor _backgroundDark2 = null;
        private SolidColor _backgroundLight1 = null;
        private SolidColor _backgroundLight2 = null;
        private SolidColor _accent1 = null;
        private SolidColor _accent2 = null;
        private SolidColor _accent3 = null;
        private SolidColor _accent4 = null;
        private SolidColor _accent5 = null;
        private SolidColor _accent6 = null;
        private SolidColor _hyperlink = null;
        private SolidColor _followedHyperlink = null;
        private List<Color> _colors = null;
        private string _name = string.Empty;
        private string _group;


        /// <summary>
        /// Màu nền tối 1
        /// </summary>
        [XmlElement("bd1")]
        public SolidColor BackgroundDark1
        {
            get
            {
                return _backgroundDark1 ?? (_backgroundDark1 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounDark1")}",
                    SpecialName = "Background Dark 1",
                    Color = "#000000"
                });
            }

            set
            {
                _backgroundDark1 = value;
                _backgroundDark1.Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounDark1")}";
                _backgroundDark1.SpecialName = "Background Dark 1";
                OnPropertyChanged("BackgroundDark1");
            }
        }

        /// <summary>
        /// Màu nền tối 2
        /// </summary>
        [XmlElement("bd2")]
        public SolidColor BackgroundDark2
        {
            get
            {
                return _backgroundDark2 ?? (_backgroundDark2 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounDark2")}",
                    SpecialName = "Background Dark 2",
                    Color = "#1F497D",
                });
            }

            set
            {
                _backgroundDark2 = value;
                _backgroundDark2.Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounDark2")}";
                _backgroundDark2.SpecialName = "Background Dark 2";
                OnPropertyChanged("BackgroundDark2");
            }
        }

        /// <summary>
        /// Màu sáng 1
        /// </summary>
        [XmlElement("bl1")]
        public SolidColor BackgroundLight1
        {
            get
            {
                return _backgroundLight1 ?? (_backgroundLight1 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounLight1")}",
                    SpecialName = "Background Light 1",
                    Color = "#FFFFFF" });
            }

            set
            {
                _backgroundLight1 = value;
                _backgroundLight1.Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounLight1")}";
                _backgroundLight1.SpecialName = "Background Light 1";
                OnPropertyChanged("BackgroundLight1");
            }
        }

        /// <summary>
        /// Màu sáng 2
        /// </summary>
        [XmlElement("bl2")]
        public SolidColor BackgroundLight2
        {
            get
            {
                return _backgroundLight2 ?? (_backgroundLight2 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounLight2")}",
                    SpecialName = "Background Light 2",
                    Color = "#EEECE1"
                });
            }

            set
            {
                _backgroundLight2 = value;
                _backgroundLight2.Name = $"{Application.Current?.FindResource("ColorGallery_BackgrounLight2")}";
                _backgroundLight2.SpecialName = "Background Light 2";
                OnPropertyChanged("BackgroundLight2");
            }
        }

        /// <summary>
        /// Màu accent 1
        /// </summary>
        [XmlElement("a1")]
        public SolidColor Accent1
        {
            get
            {
                return _accent1 ?? (_accent1 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_Accent1")}",
                    SpecialName = "Accent 1",
                    Color = "#4F81BD" });
            }

            set
            {
                _accent1 = value;
                _accent1.Name = $"{Application.Current?.FindResource("ColorGallery_Accent1")}";
                _accent1.SpecialName = "Accent 1";
                OnPropertyChanged("Accent1");
            }
        }

        /// <summary>
        /// Màu accent 2
        /// </summary>
        [XmlElement("a2")]
        public SolidColor Accent2
        {
            get
            {
                return _accent2 ?? (_accent2 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_Accent2")}",
                    SpecialName = "Accent 2",
                    Color = "#C0504D"
                });
            }

            set
            {
                _accent2 = value;
                _accent2.Name = $"{Application.Current?.FindResource("ColorGallery_Accent2")}";
                _accent2.SpecialName = "Accent 2";
                OnPropertyChanged("Accent2");
            }
        }

        /// <summary>
        /// Màu accent 3
        /// </summary>
        [XmlElement("a3")]
        public SolidColor Accent3
        {
            get
            {
                return _accent3 ?? (_accent3 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_Accent3")}",
                    SpecialName = "Accent 3",
                    Color = "#9BBB59"
                });
            }

            set
            {
                _accent3 = value;
                _accent3.Name = $"{Application.Current?.FindResource("ColorGallery_Accent3")}";
                _accent3.SpecialName = "Accent 3";
                OnPropertyChanged("Accent3");
            }
        }

        /// <summary>
        /// Màu accent 4
        /// </summary>
        [XmlElement("a4")]
        public SolidColor Accent4
        {
            get
            {
                return _accent4 ?? (_accent4 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_Accent4")}",
                    SpecialName = "Accent 4",
                    Color = "#8064A2"
                });
            }

            set
            {
                _accent4 = value;
                _accent4.Name = $"{Application.Current?.FindResource("ColorGallery_Accent4")}";
                _accent4.SpecialName = "Accent 4";
                OnPropertyChanged("Accent4");
            }
        }

        /// <summary>
        /// Màu accent 5
        /// </summary>
        [XmlElement("a5")]
        public SolidColor Accent5
        {
            get
            {
                return _accent5 ?? (_accent5 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_Accent5")}",
                    SpecialName = "Accent 5",
                    Color = "#4BACC6"
                });
            }

            set
            {
                _accent5 = value;
                _accent5.Name = $"{Application.Current?.FindResource("ColorGallery_Accent5")}";
                _accent5.SpecialName = "Accent 5";
                OnPropertyChanged("Accent5");
            }
        }

        /// <summary>
        /// Màu accent 6
        /// </summary>
        [XmlElement("a6")]
        public SolidColor Accent6
        {
            get
            {
                return _accent6 ?? (_accent6 = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_Accent6")}",
                    SpecialName = "Accent 6",
                    Color = "#F79646"
                });
            }

            set
            {
                _accent6 = value;
                _accent6.Name = $"{Application.Current?.FindResource("ColorGallery_Accent6")}";
                _accent6.SpecialName = "Accent 6";
                OnPropertyChanged("Accent6");
            }
        }

        /// <summary>
        /// Màu cho các hyperlink
        /// </summary>
        [XmlElement("hl")]
        public SolidColor Hyperlink
        {
            get
            {
                return _hyperlink ?? (_hyperlink = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_Hyperlink")}", Color = "#0563C1",
                    SpecialName = "Hyperlink",
                });
            }

            set
            {
                _hyperlink = value;
                _hyperlink.Name = $"{Application.Current?.FindResource("ColorGallery_Hyperlink")}";
                _hyperlink.SpecialName = "Hyperlink";
                OnPropertyChanged("Hyperlink");
            }
        }

        /// <summary>
        /// Màu cho các hyperlink đã truy cập
        /// </summary>
        [XmlElement("fhl")]
        public SolidColor FollowedHyperlink
        {
            get
            {
                return _followedHyperlink ?? (_followedHyperlink = new SolidColor()
                {
                    Name = $"{Application.Current?.FindResource("ColorGallery_FollowedHyperlink")}",
                    SpecialName = "Followed Hyperlink",
                    Color = "#954F72"
                });
            }

            set
            {
                _followedHyperlink = value;
                _followedHyperlink.Name = $"{Application.Current?.FindResource("ColorGallery_FollowedHyperlink")}";
                _followedHyperlink.SpecialName = "Followed Hyperlink";
                OnPropertyChanged("FollowwedHyperlink");
            }
        }

        /// <summary>
        /// Danh sách màu
        /// </summary>  
        [XmlIgnore]
        public List<Color> Colors
        {
            get
            {
                if (_colors == null)
                    _colors = new List<Color>();
                else
                    _colors.Clear();

                _colors.Add((Color)ColorConverter.ConvertFromString(BackgroundDark1.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(BackgroundDark2.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(BackgroundLight1.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(BackgroundLight2.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(Accent1.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(Accent2.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(Accent3.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(Accent4.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(Accent5.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(Accent6.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(Hyperlink.Color));
                _colors.Add((Color)ColorConverter.ConvertFromString(FollowedHyperlink.Color));

                return _colors;
            }
        }

        private List<SolidColor> _solidColors = null;
        /// <summary>
        /// Danh sách màu kiểu <see cref="SolidColor"/>
        /// </summary>
        [XmlIgnore]
        public List<SolidColor> SolidColors
        {
            get
            {
                if (_solidColors == null)
                    _solidColors = new List<SolidColor>();
                else
                    _solidColors.Clear();

                _solidColors.Add(BackgroundDark1);
                _solidColors.Add(BackgroundDark2);
                _solidColors.Add(BackgroundLight1);
                _solidColors.Add(BackgroundLight2);
                _solidColors.Add(Accent1);
                _solidColors.Add(Accent2);
                _solidColors.Add(Accent3);
                _solidColors.Add(Accent4);
                _solidColors.Add(Accent5);
                _solidColors.Add(Accent6);
                _solidColors.Add(Hyperlink);
                _solidColors.Add(FollowedHyperlink);

                return _solidColors;
            }

            set
            {
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Tên nhóm
        /// </summary>
        public string Group
        {
            get
            {
                if(string.IsNullOrEmpty(_group))
                {
                    _group = "Customize";
                }
                return _group;
            }
            set => _group = value;
        }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new EColorManagment()
            {
                Accent1 = (SolidColor)this.Accent1.Clone(),
                Accent2 = (SolidColor)this.Accent2.Clone(),
                Accent3 = (SolidColor)this.Accent3.Clone(),
                Accent4 = (SolidColor)this.Accent4.Clone(),
                Accent5 = (SolidColor)this.Accent5.Clone(),
                Accent6 = (SolidColor)this.Accent6.Clone(),
                BackgroundDark1 = (SolidColor)this.BackgroundDark1.Clone(),
                BackgroundDark2 = (SolidColor)this.BackgroundDark2.Clone(),
                BackgroundLight1 = (SolidColor)this.BackgroundLight1.Clone(),
                BackgroundLight2 = (SolidColor)this.BackgroundLight2.Clone(),
                Hyperlink = (SolidColor)this.Hyperlink.Clone(),
                FollowedHyperlink = (SolidColor)this.FollowedHyperlink.Clone(),
                Name = this.Name
            };
        }
    }
}

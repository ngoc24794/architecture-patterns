﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model.Theme
{
    [XmlRoot("themeShape")]
    [Serializable]
    public class EThemeShapeBackground : EStandardElement
    {
        private bool _isShapeBackground;
        /// <summary>
        /// Shape có phải là background hay không
        /// </summary>
        [XmlAttribute("isBackground")]
        public bool IsShapeBackground
        {
            get { return _isShapeBackground; }
            set { _isShapeBackground = value; }
        }


        public override IElearningElement Clone()
        {

            EThemeShapeBackground eThemeShapeBackground = new EThemeShapeBackground();
            base.Copy(eThemeShapeBackground);
            eThemeShapeBackground.IsShapeBackground = IsShapeBackground;
            return eThemeShapeBackground;
        }
    }
}

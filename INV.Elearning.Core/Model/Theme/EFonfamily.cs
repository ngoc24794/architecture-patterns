﻿
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model.Theme
{
    /// <summary>
    /// Phông chữ cho Theme
    /// </summary>
    [XmlRoot("font")]
    public class EFontfamily : RootModel
    {
        private string _majorFont = "Segoe UI";
        private string _minorFont = "Segoe UI";
        private string _tagName = "";
        private bool _isTrueType = true;

        /// <summary>
        /// Phông chữ chính, dùng cho hiển thị title
        /// </summary>
        [XmlAttribute("mj")]
        public string MajorFont
        {
            get
            {
                return _majorFont;
            }

            set
            {
                _majorFont = value;
                OnPropertyChanged("MajorFont");
            }
        }

        /// <summary>
        /// Phông chữ phụ, dùng cho hiển thị content
        /// </summary>
        [XmlAttribute("mn")]
        public string MinorFont
        {
            get
            {
                return _minorFont;
            }

            set
            {
                _minorFont = value;
                OnPropertyChanged("MinorFont");
            }
        }

        /// <summary>
        /// Tên thẻ đặc trưng
        /// </summary>
        [XmlIgnore]
        public string TagName
        {
            get => _tagName;
            set => _tagName = value;
        }

        /// <summary>
        /// Kiểu phông chuẩn
        /// </summary>
        public bool IsTrueType { get => _isTrueType; set => _isTrueType = value; }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new EFontfamily() { Name = this.Name, MajorFont = this.MajorFont, MinorFont = this.MinorFont, TagName = this.TagName };
        }

        public override string ToString()
        {
            return MajorFont.ToString();
        }
    }
}

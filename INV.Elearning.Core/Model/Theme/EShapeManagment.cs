﻿
using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model.Theme
{
    /// <summary>
    /// Lớp quản thí các đối tượng hình vẽ của theme
    /// </summary>
    [Serializable]
    [XmlRoot("mshape")]
    public class EShapeManagment
    {
        private ObservableCollection<AThemeShape> _shapeLayouts = null;
        private ColorBinding _background = null;
        /// <summary>
        /// Lấy hoặc cài đặt danh sách các đối tượng Shape của nền trang
        /// </summary>
        [XmlArray("shapes")]
        [XmlArrayItem("img", Type = typeof(EThemeImage)), XmlArrayItem("pth", Type =typeof(EThemePath)), XmlArrayItem("rect", Type = typeof(EThemeRectangle)), XmlArrayItem("eslp", Type = typeof(EThemeEllipse))]
        public ObservableCollection<AThemeShape> ShapeLayouts
        {
            get
            {
                return _shapeLayouts ?? (_shapeLayouts = new ObservableCollection<AThemeShape>());
            }

            set
            {
                _shapeLayouts = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt màu nền trang
        /// </summary>
        [XmlElement("bgr")]
        public ColorBinding Background
        {
            get
            {
                return _background;
            }

            set
            {
                _background = value;
            }
        }
    }
}

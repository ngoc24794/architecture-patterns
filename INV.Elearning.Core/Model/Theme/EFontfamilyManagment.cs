﻿
namespace INV.Elearning.Core.Model.Theme
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Windows.Media;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp quản lý phông chữ cho theme
    /// </summary>
    [XmlRoot("mfont")]
    public class EFontfamilyManagment : RootModel
    {
        private ObservableCollection<EFontfamily> _themeFonts = null;
        /// <summary>
        /// Danh sách phông chữ trong theme
        /// </summary>
        [XmlElement("fonts")]
        public ObservableCollection<EFontfamily> ThemeFonts
        {
            get
            {
                return _themeFonts ?? (_themeFonts = new ObservableCollection<EFontfamily>());
            }

            set
            {
                _themeFonts = value;
            }
        }

        /// <summary>
        /// Danh sách phông chữ chính
        /// </summary>
        public IEnumerable<string> MajorFonts
        {
            get
            {
                return ThemeFonts.Select(x => x.MajorFont);
            }
        }

        /// <summary>
        /// Danh sách phông chữ phụ
        /// </summary>
        public IEnumerable<string> MinorFonts
        {
            get
            {
                return ThemeFonts.Select(x => x.MinorFont);
            }
        }

        /// <summary>
        /// Danh sách các phông chữ 
        /// </summary>
        public IEnumerable<EFontfamily> Fonts
        {
            get
            {
                foreach (var item in ThemeFonts)
                {
                    item.TagName = "Theme Fonts";
                }
                var _result = ThemeFonts.ToList();
                _result.AddRange(AllFonts);
                return _result;
            }
        }

        private static IEnumerable<EFontfamily> _allFonts;
        /// <summary>
        /// Danh sách tất cả các phông chữ hệ thống
        /// </summary>
        public static IEnumerable<EFontfamily> AllFonts
        {
            get
            {
                return _allFonts ?? (_allFonts = GetAllFont());
            }
        }


        /// <summary>
        /// Lấy danh sách tất cả các font của hệ thống
        /// </summary>
        /// <returns></returns>
        private static List<EFontfamily> GetAllFont()
        {
            string fontsfolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Fonts);
            DirectoryInfo _dInfo = new DirectoryInfo(fontsfolder);
            var fonts = _dInfo.GetFiles().Where(x=>x.Name.EndsWith(".otf"));
            List<FontFamily> _otFonts = new List<FontFamily>();
            foreach (var item in fonts)
            {
                ICollection<FontFamily> ffc = System.Windows.Media.Fonts.GetFontFamilies(item.FullName);
                foreach (var ft in ffc)
                {
                    _otFonts.Add(ft);
                }
            }
            

            List<EFontfamily> _allFonts = new List<EFontfamily>();
            foreach (var font in System.Windows.Media.Fonts.SystemFontFamilies.OrderBy(x=>x.ToString()))
            {
                _allFonts.Add(new EFontfamily() { Name = font.ToString(), MajorFont = font.ToString(), TagName = "All Fonts", IsTrueType = CompareFont(font, _otFonts) });
            }
            return _allFonts;
        }


        public static bool CompareFont(FontFamily tName, List<FontFamily> fNames)
        {
            return fNames.Find(x => x.Source.Contains(tName.Source)) == null;
        }

        /// <summary>
        /// Nhân bản dữ liệu
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _result = new EFontfamilyManagment();
            foreach (var fonts in this.ThemeFonts)
            {
                _result.ThemeFonts.Add(fonts.Clone() as EFontfamily);
            }
            return _result;
        }
    }
}

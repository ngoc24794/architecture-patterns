﻿using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp màu có khai báo
    /// </summary>
    [Serializable()]
    [XmlRoot("colrb")]
    public class ColorBinding
    {
        private SolidColor _color;
        /// <summary>
        /// Màu chuẩn 
        /// </summary>
        [XmlElement("color")]
        public SolidColor Color
        {
            get { return _color; }
            set { _color = value; }
        }

        private bool _isBinding;
        /// <summary>
        /// Có binding dữ liệu thumb hay ko
        /// </summary>
        [XmlAttribute("binding")]
        public bool IsBinding
        {
            get { return _isBinding; }
            set { _isBinding = value; }
        }

        private string _targetName;
        /// <summary>
        /// Tên thuộc tính Binding
        /// </summary>
        [XmlAttribute("target")]
        public string TargetName
        {
            get { return _targetName; }
            set { _targetName = value; }
        }

    }
}

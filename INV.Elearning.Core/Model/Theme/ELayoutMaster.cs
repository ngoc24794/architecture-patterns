﻿
namespace INV.Elearning.Core.Model.Theme
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Media;
    using System.Xml.Serialization;

    public enum LayoutType
    {
        None,
        Title,
        TitleContent,
        SectionHeader,
        TwoContent,
        Comparision,
        TitleOnly,
        Blank,
        ContentWithCaption,
        PictureWithCaption,
        Custom
    }

    /// <summary>
    /// Lớp lưu trữ dữ liệu của layout master
    /// </summary>
    [XmlRoot("layoutmaster")]
    [Serializable]
    public class ELayoutMaster : PageElementBase
    {
        private string _layoutName;
        /// <summary>
        /// Tên Layout
        /// </summary>
        [XmlAttribute("ln")]
        public string LayoutName
        {
            get { return _layoutName; }
            set { _layoutName = value; }
        }

        private int _layoutCount;
        /// <summary>
        /// Đếm số Layout được sử dụng
        /// </summary>
        [XmlAttribute("lc")]
        public int LayountCount
        {
            get { return _layoutCount; }
            set { _layoutCount = value; }
        }


        private ESlideMaster _parent;
        /// <summary>
        /// Slide Master Parent
        /// </summary>
        [XmlIgnore]
        public ESlideMaster Parent
        {
            get { return _parent; }
            set { _parent = value; OnPropertyChanged("Parent"); }
        }


        private string _slideParent;
        /// <summary>
        /// Slide Master cha của Layout
        /// </summary>
        [XmlAttribute("slideparent")]
        public string SlideParent
        {
            get { return _slideParent; }
            set { _slideParent = value; OnPropertyChanged("SlideParent"); }
        }

        private string _slideName;
        /// <summary>
        /// Tên của Slide
        /// </summary>
        [XmlAttribute("slideName")]
        public string SlideName
        {
            get { return _slideName; }
            set { _slideName = value; }
        }

        private ObservableCollection<ObjectElementBase> _elements;
        [XmlArray("els")]
        public ObservableCollection<ObjectElementBase> Elements
        {
            get { return _elements ?? (_elements = new ObservableCollection<ObjectElementBase>()); }
            set { _elements = value; }
        }

        private LayoutType _layoutType;
        /// <summary>
        /// Kiểu Layout
        /// </summary>
        [XmlAttribute("lt")]
        public LayoutType LayoutType
        {
            get { return _layoutType; }
            set { _layoutType = value; }
        }

        private ObservableCollection<string> _ListSlideID;
        /// <summary>
        /// Danh sách id của các slide chọn layout master
        /// </summary>
        [XmlArray("lstSlID"),XmlArrayItem("sldIDIt")]
        public ObservableCollection<string> ListSlideID
        {
            get { return _ListSlideID ?? (_ListSlideID = new ObservableCollection<string>()); }
            set { _ListSlideID = value; }
        }

        private bool _isUpdateFooter;
        /// <summary>
        /// Kiểm tra xem có cập nhật chân trang hay không
        /// </summary>
        [XmlAttribute("isUpFot")]
        public bool IsUpdateFooter
        {
            get { return _isUpdateFooter; }
            set { _isUpdateFooter = value; }
        }

        private bool _isUpdateSlideNumber;
        /// <summary>
        /// Kiểm tra xem có cập nhật số trang hay không
        /// </summary>
        [XmlAttribute("isUpSldNu")]
        public bool IsUpdateSlideNumber
        {
            get { return _isUpdateSlideNumber; }
            set { _isUpdateSlideNumber = value; }
        }

        private bool _isUpdateDateTime;
        /// <summary>
        /// Kiểm tra xem có cập nhật ngày giờ hay không
        /// </summary>
        [XmlAttribute("isUpDT")]
        public bool IsUpdateDateTime
        {
            get { return _isUpdateDateTime; }
            set { _isUpdateDateTime = value; }
        }


        public override IElearningElement Clone()
        {
            ELayoutMaster eLayoutMaster = new ELayoutMaster();
            base.Copy(eLayoutMaster);
            eLayoutMaster.LayoutName = LayoutName;
            eLayoutMaster.LayountCount = LayountCount;
            eLayoutMaster.Background = Background;
            eLayoutMaster.ID = ID;
            eLayoutMaster.SlideName = SlideName;
            eLayoutMaster.Name = Name;
            eLayoutMaster.LayoutType = LayoutType;
            eLayoutMaster.IsTitle = IsTitle;
            eLayoutMaster.IsFooters = IsFooters;
            eLayoutMaster.IsHideBackground = IsHideBackground;
            eLayoutMaster.SlideParent = SlideParent;
            eLayoutMaster.MainLayer = MainLayer.Clone() as PageLayer;
            eLayoutMaster.Elements.Clear();
            eLayoutMaster.ListSlideID.Clear();
            eLayoutMaster.IsUpdateFooter = IsUpdateFooter;
            eLayoutMaster.IsUpdateDateTime = IsUpdateDateTime;
            eLayoutMaster.IsUpdateSlideNumber = IsUpdateSlideNumber;
            foreach (ObjectElementBase item in Elements)
            {
                eLayoutMaster.Elements.Add(item.Clone() as ObjectElementBase);
            }
            foreach (string item in ListSlideID)
            {
                eLayoutMaster.ListSlideID.Add(item);
            }
            return eLayoutMaster;
        }
    }
}

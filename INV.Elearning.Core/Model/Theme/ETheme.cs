﻿


namespace INV.Elearning.Core.Model.Theme
{
    using INV.Elearning.Core.Helper;
    using INV.Elearning.Core.View.Theme;
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Media;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp dữ liệu lưu trữ Theme
    /// </summary>
    [XmlRoot("theme")]
    [Serializable]
    public class ETheme : RootModel
    {
        private EFontfamilyManagment _fontFamlies = null;
        private EColorManagment _colors = null;
        private ObservableCollection<ESlideMaster> _eSlideMasters = null;

        private string _rID;

        public string RID
        {
            get { return _rID; }
            set { _rID = value; }
        }

        private bool _isClone;
        /// <summary>
        /// Kiểm tra xem có Clone Theme hay k
        /// </summary>
        [XmlIgnore]
        public bool IsClone
        {
            get { return _isClone; }
            set { _isClone = value; }
        }


        private string _filePath;
        /// <summary>
        /// Đường dẫn file
        /// </summary>
        [XmlIgnore]
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; }
        }


        private string _tagName;
        /// <summary>
        /// TagName
        /// </summary>
        [XmlAttribute("tn")]
        public string TagName
        {
            get { return _tagName; }
            set { _tagName = value; }
        }

        private string _toolTip;
        /// <summary>
        /// Tooltip
        /// </summary>
        [XmlIgnore]
        public string ToolTip
        {
            get { return _toolTip; }
            set { _toolTip = value; }
        }

        private int _countTheme;
        /// <summary>
        /// ThemeCount
        /// </summary>
        [XmlIgnore]
        public int CountTheme
        {
            get { return _countTheme; }
            set
            {
                _countTheme = value;
                Name = string.Empty;
                // Name = ThemeName + " uses by " + value + " slides";
                Name = string.Format("{0} {1} {2} {3}", ThemeName, FileHelper.FindResource("COREELAYOUTMASTER_uses")?.ToString(), value, FileHelper.FindResource("COREELAYOUTMASTER_slides")?.ToString());
                OnPropertyChanged("CountTheme");
            }
        }


        private bool _isLoaded;
        /// <summary>
        /// Kiểm tra xem có load hay ko
        /// </summary>
        [XmlIgnore]
        public bool IsLoaded
        {
            get { return _isLoaded; }
            set { _isLoaded = value; }
        }

        private string _themeName;
        /// <summary>
        /// Tên của Theme
        /// </summary>
        [XmlIgnore]
        public string ThemeName
        {
            get { return _themeName; }
            set
            {
                _themeName = value;
                Name = string.Empty;
                Name = value + " uses by " + CountTheme + " slides";
                OnPropertyChanged("ThemeName");
            }
        }


        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính quản lý màu cho đối tượng
        /// </summary>
        [XmlElement("colors")]
        public EColorManagment Colors
        {
            get
            {
                return _colors ?? (_colors = new EColorManagment());
            }

            set
            {
                _colors = value;
                OnPropertyChanged();
            }
        }





        /// <summary>
        /// Lấy hoặc cài đặt danh sách phông chữ trong theme
        /// </summary>
        [XmlElement("fonts")]
        public EFontfamilyManagment FontFamilies
        {
            get
            {
                return _fontFamlies ?? (_fontFamlies = new EFontfamilyManagment());
            }

            set
            {
                _fontFamlies = value;
                OnPropertyChanged();
            }
        }

        private EFontfamily _selectedFont;
        /// <summary>
        /// Font đang được chọn
        /// </summary>
        [XmlElement("selfont")]
        public EFontfamily SelectedFont
        {
            get { return _selectedFont ?? (_selectedFont = new EFontfamily()); }
            set { _selectedFont = value; OnPropertyChanged("SelectedFont"); }
        }



        /// <summary>
        /// Lấy hoặc cài đặt danh sách các slideMaster trong Theme
        /// </summary>
        [XmlElement("slidemasters")]
        public ObservableCollection<ESlideMaster> SlideMasters
        {
            get
            {
                return _eSlideMasters ?? (_eSlideMasters = new ObservableCollection<ESlideMaster>());
            }
            set
            {
                _eSlideMasters = value;
            }
        }

        private BackgroundItem _backgroundStyle;
        /// <summary>
        /// Màu của background
        /// </summary>
        [XmlIgnore]
        public BackgroundItem BackgroundStyle
        {
            get { return _backgroundStyle; }
            set { _backgroundStyle = value; OnPropertyChanged("BackgroundStyle"); }
        }

        private int _indexTheme;
        [XmlIgnore]
        public int IndexTheme
        {
            get { return _indexTheme; }
            set { _indexTheme = value; }
        }


        public override IElearningElement Clone()
        {
            ETheme eTheme = new ETheme();
            eTheme.Colors = Colors.Clone() as EColorManagment;
            eTheme.FontFamilies = FontFamilies.Clone() as EFontfamilyManagment;
            eTheme.SelectedFont = SelectedFont.Clone() as EFontfamily;
            eTheme.IsLoaded = IsLoaded;
            eTheme.ThemeName = ThemeName;
            eTheme.FilePath = FilePath;
            foreach (ESlideMaster item in SlideMasters)
            {
                eTheme.SlideMasters.Add(item.Clone() as ESlideMaster);
            }


            return eTheme;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Core.Model.Theme
{
    public enum PlaceHolderType
    {
        Content,
        Text,
        Picture,
        Video,
        Chart
    }
}

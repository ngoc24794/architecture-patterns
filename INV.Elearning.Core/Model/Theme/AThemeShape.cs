﻿
using System.Xml.Serialization;
using System;

namespace INV.Elearning.Core.Model.Theme
{
    /// <summary>
    /// Các hình ảnh chứa ở nền đối tượng
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(EThemePath))]
    [XmlInclude(typeof(EThemeRectangle))]
    [XmlInclude(typeof(EThemeEllipse))]
    [XmlInclude(typeof(EThemeImage))]
    [XmlRoot("sht")]
    public abstract class AThemeShape
    {
        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính màu nền cho đối tượng,
        /// Màu này tương ứng với 1 trong các tên màu của theme
        /// </summary>
        [XmlElement("fill")]
        public ColorBinding Fill
        {
            set;
            get;
        }

        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính màu viền
        /// </summary>
        [XmlElement("stroke")]
        public ColorBinding Stroke
        {
            set;
            get;
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính độ dày viền
        /// </summary>
        [XmlAttribute("thickness")]
        public double StrokeThickness
        {
            set;
            get;
        }

        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính độ dài của hình
        /// </summary>
        [XmlAttribute("w")]
        public double Width
        {
            set;
            get;
        }

        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính độ rộng của hình
        /// </summary>
        [XmlAttribute("h")]
        public double Height
        {
            set;
            get;
        }

        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính căn trái của hình
        /// </summary>
        [XmlAttribute("l")]
        public double Left
        {
            set;
            get;
        }

        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính căn trên của hình
        /// </summary>
        [XmlAttribute("t")]
        public double Top
        {
            set;
            get;
        }
    }

    /// <summary>
    /// Lớp dữ liệu cho đối tượng Ellipse
    /// </summary>
    [Serializable]
    [XmlRoot("elp")]
    public class EThemeEllipse : AThemeShape { }

    /// <summary>
    /// Lớp dữ liệu cho đối tượng Rectangle
    /// </summary>
    [Serializable]
    [XmlRoot("rect")]
    public class EThemeRectangle : AThemeShape
    {
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính bo cong các góc
        /// </summary>
        [XmlAttribute("corner")]
        public double CornerRadius { get; set; }
    }

    [Serializable]
    [XmlRoot("img")]
    public class EThemeImage : AThemeShape
    {
        private Image _source;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính hình ảnh cho nền
        /// </summary>
        [XmlElement("source")]
        public Image Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private bool _isRecolor;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính thay đổi màu sắc
        /// </summary>
        [XmlAttribute("recolor")]
        public bool IsRecolor
        {
            get { return _isRecolor; }
            set { _isRecolor = value; }
        }

    }

    /// <summary>
    /// Lớp dữ liệu cho đối tượng đường Path
    /// </summary>
    [Serializable]
    [XmlRoot("path")]
    public class EThemePath : AThemeShape
    {
        /// <summary>
        /// Cài đặt hoặc lấy thuộc tính data của đường Path
        /// </summary>
        [XmlAttribute("d")]
        public string Data
        {
            set;
            get;
        }
    }
}

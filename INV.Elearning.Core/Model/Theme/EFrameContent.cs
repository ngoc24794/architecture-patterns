﻿
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp lưu trữ dữ liệu cho đối tượng khung nội dung
    /// </summary>
    [XmlRoot("frame")]
    public class EFrameContent
    {
        private ESuperPoint _supertPoint;
        private string _fontFamily = string.Empty;
        private bool _isUpperCase;

        /// <summary>
        /// Lấy hoặc cài đặt nội dung in hoa cả đoạn văn bản
        /// </summary>
        [XmlAttribute("upper")]
        public bool IsUppserCase
        {
            get { return _isUpperCase; }
            set { _isUpperCase = value; }
        }


        /// <summary>
        /// Lấy hoặc cài đặt phông chữ cho nội dung
        /// </summary>
        [XmlAttribute("font")]
        public string FontFamily
        {
            get { return _fontFamily; }
            set { _fontFamily = value; }
        }


        /// <summary>
        /// Lấy hoặc cài đặt vị trí, kích thước khung
        /// </summary>
        [XmlElement("spoint")]
        public ESuperPoint SuperPoint
        {
            get { return _supertPoint; }
            set { _supertPoint = value; }
        }

    }
}

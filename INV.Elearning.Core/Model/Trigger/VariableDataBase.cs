﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  VariableDataBase.cs
**
** Description: Lớp cơ sở cho các lớp lưu trữ dữ liệu biến số dùng để lưu xuống xml
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 2/5/2018
===========================================================*/
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp cơ sở cho các lớp lưu trữ dữ liệu biến số dùng để lưu xuống xml
    /// </summary>
    [Serializable, XmlRoot("vari")]
    public abstract class VariableDataBase : RootModel
    {
        private string _rid;

        public string RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public VariableDataBase() { }
        /// <summary>
        /// Lấy dữ liệu từ xml để tạo lại model
        /// </summary>
        /// <returns></returns>
        public abstract VariableModelBase GetModel();
    }
}

﻿using INV.Elearning.Core.ViewModel;
using System.Collections.ObjectModel;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp dành cho xử lí giao diện
    /// </summary>
    public interface ITriggerableObject
    {
        ObservableCollection<TriggerViewModelBase> TriggerData { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class TriggerModelBase : RootModel
    {       
        /// <summary>
        /// Lấy dữ liệu từ Model
        /// </summary>
        /// <returns></returns>
        public abstract TriggerableDataObjectBase GetData();

        public abstract TriggerViewModelBase GenerateViewModel();
    }
}

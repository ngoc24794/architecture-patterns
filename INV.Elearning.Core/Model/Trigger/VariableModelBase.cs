﻿
// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  VariableModelBase.cs
**
** Description: Lớp lưu trữ dữ liệu biến số dùng để xử lí
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 2/5/2018
===========================================================*/
using INV.Elearning.Core.ViewModel;
using System;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp cơ sở cho các lớp lưu trữ dữ liệu biến số dùng để xử lí
    /// </summary>
    [Serializable]
    public abstract class VariableModelBase : RootModel
    {
        public VariableModelBase() { }
        /// <summary>
        /// Lấy dữ liệu từ model để lưu xuống xml
        /// </summary>
        /// <returns></returns>
        public abstract VariableDataBase GetData();
        /// <summary>
        /// Tạo lại ViewModel
        /// </summary>
        /// <returns></returns>
        public abstract VariableViewModelBase GenerateViewModel();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp dành cho lưu trữ
    /// </summary>
    [Serializable]
    public abstract class TriggerableDataObjectBase : RootModel
    {
        #region Constructors
        public TriggerableDataObjectBase() { }
        #endregion

        #region Methods
        /// <summary>
        /// Lấy Model từ Data
        /// </summary>
        /// <returns></returns>
        public abstract TriggerModelBase GetModel(); 
        #endregion
    }

    /// <summary>
    /// Interface cho các đối tượng có triggers
    /// </summary>
    public interface ITriggerDataObject
    {
        ObservableCollection<TriggerableDataObjectBase> TriggerData { set; get; }
    }
}

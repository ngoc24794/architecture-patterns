﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    [Serializable, XmlRoot("varData")]
    public class ListVariables : RootModel
    {

        private string _rid;
        public string RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        private List<VariableDataBase> _variables;

        [XmlArray("varLst"), XmlArrayItem("var")]
        public List<VariableDataBase> Variables { get => _variables ?? (_variables = new List<VariableDataBase>()); set => _variables = value; }

        public override IElearningElement Clone()
        {
            return null;
        }
    }
}

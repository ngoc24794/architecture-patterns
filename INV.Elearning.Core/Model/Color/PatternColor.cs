﻿

using System;
using System.Windows.Media;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp lưu trữ liểu đỗ màu Pattern
    /// </summary>
    [Serializable]
    [XmlRoot("pattern")]
    public class PatternColor : ColorBase
    {
        private SolidColor _foreground;
        /// <summary>
        /// Mầu chữ
        /// </summary>
        [XmlElement("fr")]
        public SolidColor Foreground
        {
            get { return _foreground; }
            set { _foreground = value; }
        }

        private SolidColor _background;
        /// <summary>
        /// Màu nền
        /// </summary>
        [XmlElement("bg")]
        public SolidColor Background
        {
            get { return _background; }
            set { _background = value; }
        }

        private HatchStyle _hatchStyle;
        /// <summary>
        /// Kiểu đỗ
        /// </summary>
        [XmlAttribute("type")]
        public HatchStyle HatchStyle
        {
            get { return _hatchStyle; }
            set { _hatchStyle = value; }
        }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new PatternColor() { Background = (SolidColor)this.Background.Clone(), Foreground = (SolidColor)this.Foreground.Clone(), HatchStyle = this.HatchStyle };
        }

        /// <summary>
        /// Chuyển đổi màu sang dữ liệu
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static PatternColor Converter(ColorPatternBrush pattern)
        {
            PatternColor _result = new PatternColor();
            _result.Background = new SolidColor() { Color = pattern.PatternBackground.ToString() };
            _result.Foreground = new SolidColor() { Color = pattern.PatternForeground.ToString() };
            _result.HatchStyle = pattern.HatchStyle;
            return _result;
        }

        /// <summary>
        /// Chuyển đổi màu sang màu hiển thị
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static ColorPatternBrush Converter(PatternColor pattern)
        {
            ColorPatternBrush _result = new ColorPatternBrush();
            _result.PatternBackground = Converter(pattern.Background);
            _result.PatternForeground = Converter(pattern.Foreground);
            _result.HatchStyle = pattern.HatchStyle;
            return _result;
        }

        private static Controls.SolidColor Converter(SolidColor solid)
        {
            if (solid != null)
            {
                return new Controls.SolidColor() { Name = solid.Name, Color = solid.Color, Brightness = solid.Brightness };
            }
            return null;
        }
    }

    /// <summary>
    /// Defines hatch style
    /// </summary>
    public enum HatchStyle
    {
        HS_HORIZONTAL,
        HS_VERTICAL,
        HS_FORWARDDIAGONAL,
        HS_BACKWARDDIAGONAL,
        HS_LARGEGRID,
        HS_DIAGONALCROSS,
        HS_PERCENT05,
        HS_PERCENT10,
        HS_PERCENT20,
        HS_PERCENT25,
        HS_PERCENT30,
        HS_PERCENT40,
        HS_PERCENT50,
        HS_PERCENT60,
        HS_PERCENT70,
        HS_PERCENT75,
        HS_PERCENT80,
        HS_PERCENT90,
        HS_LIGHTDOWNWARDDIAGONAL,
        HS_LIGHTUPWARDDIAGONAL,
        HS_DARKDOWNWARDDIAGONAL,
        HS_DARKUPWARDDIAGONAL,
        HS_WIDEDOWNWARDDIAGONAL,
        HS_WIDEUPWARDDIAGONAL,
        HS_LIGHTVERTICAL,
        HS_LIGHTHORIZONTAL,
        HS_NARROWVERTICAL,
        HS_NARROWHORIZONTAL,
        HS_DARKVERTICAL,
        HS_DARKHORIZONTAL,
        HS_DASHEDDOWNWARDDIAGONAL,
        HS_DASHEDUPWARDDIAGONAL,
        HS_DASHEDHORIZONTAL,
        HS_DASHEDVERTICAL,
        HS_SMALLCONFETTI,
        HS_LARGECONFETTI,
        HS_ZIGZAG,
        HS_WAVE,
        HS_DIAGONALBRICK,
        HS_HORIZONTALBRICK,
        HS_WEAVE,
        HS_PLAID,
        HS_DIVOT,
        HS_DOTTEDGRID,
        HS_DOTTEDDIAMOND,
        HS_SHINGLE,
        HS_TRELLIS,
        HS_SPHERE,
        HS_SMALLGRID,
        HS_SMALLCHECKERBOARD,
        HS_LARGECHECKERBOARD,
        HS_OUTLINEDDIAMOND,
        HS_SOLIDDIAMOND,
    };

    /// <summary>
    /// Defines hatch type
    /// </summary>
    public enum HatchType
    {
        HT_RECTANGLE,
        HT_LINE,
    };
}

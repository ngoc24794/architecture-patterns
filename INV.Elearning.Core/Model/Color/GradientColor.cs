﻿using INV.Elearning.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Lớp lưu trữ dải màu
    /// </summary>
    [Serializable]
    [XmlRoot("gradient")]
    public class GradientColor : ColorBase
    {
        /// <summary>
        /// Khoảng màu
        /// </summary>
        private List<LinearElement> _linearElements = null;
        private int _angle = 0;
        private GradientColorType _type = GradientColorType.Linear;
        EPoint _centerPoint = new EPoint(0.5, 0.5);

        /// <summary>
        /// Lấy hoặc cài đặt danh sách các khoảng màu
        /// </summary>
        [XmlElement("linears")]
        public List<LinearElement> LinearElements
        {
            get
            {
                return _linearElements ?? (_linearElements = new List<LinearElement>());
            }

            set
            {
                _linearElements = value;
            }
        }

        /// <summary>
        /// Góc quay
        /// </summary>
        [XmlAttribute("a")]
        public int Angle
        {
            set => _angle = value;
            get => _angle;
        }

        /// <summary>
        /// Điểm trung tâm
        /// </summary>
        [XmlElement("c")]
        public EPoint CenterPoint
        {
            get => _centerPoint; set => _centerPoint = value;
        }

        /// <summary>
        /// Kiểu đổ màu
        /// </summary>
        [XmlAttribute("type")]
        public GradientColorType Type { get => _type; set => _type = value; }

        /// <summary>
        /// Nhân bản một dải màu
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _linearGradient = new GradientColor();
            _linearGradient.Opacity = this.Opacity;
            _linearGradient.CenterPoint = new EPoint(this.CenterPoint.X, this.CenterPoint.Y);
            _linearGradient.Angle = this.Angle;
            _linearGradient.Type = this.Type;

            this.LinearElements.ForEach(x =>
            {
                _linearGradient.LinearElements.Add(new LinearElement() { Color = x.Color, Offset = x.Offset, SpecialName = x.SpecialName, Brightness = x.Brightness });
            });

            return _linearGradient;
        }

        /// <summary>
        /// Chuyển đổi dang kiểu màu hệ thống
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static ColorGradientBrush Converter(GradientColor color)
        {
            ColorGradientBrush _result = new ColorGradientBrush();
            _result.GradientType = color.Type;
            foreach (LinearElement item in color.LinearElements)
            {
                _result.GradientStops.Add(LinearElement.Converter(item));
            }
            _result.CenterPoint = EPoint.Converter(color.CenterPoint);
            _result.Angle = color.Angle;
            _result.UpdateBrush();
            return _result;
        }

        /// <summary>
        /// Chuyển từ dải màu hế thống
        /// </summary>
        /// <param name="gradient"></param>
        /// <returns></returns>
        public static GradientColor Converter(ColorGradientBrush gradient)
        {
            GradientColor _result = new GradientColor();
            if (gradient.GradientStops?.Count > 0)
                foreach (CustomGradientStop item in gradient.GradientStops)
                {
                    _result.LinearElements.Add(LinearElement.Converter(item));
                }
            _result.Type = gradient.GradientType;
            _result.Angle = gradient.Angle;
            _result.CenterPoint = EPoint.Converter(gradient.CenterPoint);

            return _result;
        }

        /// <summary>
        /// Chuyển đổi từ dữ liệu màu sắc hệ thống
        /// </summary>
        /// <param name="gradient"></param>
        /// <returns></returns>
        public static ColorGradientBrush Converter(GradientBrush gradient)
        {
            if (gradient == null) return null;
            ColorGradientBrush _result = new ColorGradientBrush();
            _result.GradientType = gradient is RadialGradientBrush ? GradientColorType.Radial : GradientColorType.Linear;

            //_result.Brush = gradient.Clone();
            if (gradient is RadialGradientBrush)
                _result.CenterPoint = (gradient as RadialGradientBrush).Center;
            _result.Angle = 0;

            if (_result.GradientStops == null) _result.GradientStops = new System.Collections.ObjectModel.ObservableCollection<CustomGradientStop>();
            _result.GradientStops.Clear();
            foreach (GradientStop item in gradient.GradientStops)
            {
                _result.GradientStops.Add(LinearElement.Converter(item));
            }
            _result.UpdateBrush();
            return _result;
        }
    }

    /// <summary>
    /// Kiểu đổ màu
    /// </summary>
    public enum GradientColorType
    {
        /// <summary>
        /// Đường thẳng
        /// </summary>
        [XmlEnum("linear")]
        Linear,

        /// <summary>
        /// Hình tròn
        /// </summary>
        [XmlEnum("radial")]
        Radial
    }

    /// <summary>
    /// Màu sắc
    /// </summary>
    [Serializable]
    [XmlRoot("stop")]
    public class LinearElement
    {
        private string color = "#00000000";
        private double _offset = 0.0;

        [XmlAttribute("col")]
        /// <summary>
        /// Cài đặt hoặc lấy mã màu dạng hexa
        /// </summary>
        public string Color
        {
            get
            {
                return color;
            }

            set
            {
                color = value;
            }
        }

        private string _specialName;
        [XmlAttribute("sn")]
        /// <summary>
        /// Tên đặc biệt, các tên thuộc theme
        /// </summary>
        public string SpecialName
        {
            get { return _specialName; }
            set { _specialName = value; }
        }

        private double _brightness;
        [XmlAttribute("br")]
        /// <summary>
        /// Độ sáng của màu
        /// </summary>
        public double Brightness
        {
            get { return _brightness; }
            set { _brightness = value; }
        }


        [XmlAttribute("off")]
        /// <summary>
        /// Cài đặt hoặc lấy vị trí của đơn vị màu
        /// </summary>
        public double Offset
        {
            get
            {
                return _offset;
            }

            set
            {
                _offset = value;
            }
        }

        /// <summary>
        /// Chuyển đổi sang đối tượng của hệ thống
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static CustomGradientStop Converter(LinearElement element)
        {
            return new CustomGradientStop()
            {
                Color = new Controls.SolidColor() { Brightness = element.Brightness, Color = element.Color, Name = element.SpecialName },
                Offset = element.Offset
            };
        }

        /// <summary>
        /// Chuyển đổi từ đối tượng của hệ thống
        /// </summary>
        /// <param name="stop"></param>
        /// <returns></returns>
        public static LinearElement Converter(CustomGradientStop stop)
        {
            return new LinearElement() { Color = stop.Color.Color, Brightness = stop.Color.Brightness, SpecialName = stop.Color.Name, Offset = stop.Offset };
        }

        /// <summary>
        /// Chuyển đổi sang đối tượng của hệ thống
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static CustomGradientStop Converter(GradientStop stop)
        {
            return new CustomGradientStop()
            {
                Color = new Controls.SolidColor() { Color = stop.Color.ToString() },
                Offset = stop.Offset
            };
        }
    }
}

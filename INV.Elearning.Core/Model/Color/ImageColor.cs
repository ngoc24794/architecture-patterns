﻿

namespace INV.Elearning.Core.Model
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp đối tượng màu là hình ảnh
    /// </summary>
    [Serializable]
    [XmlRoot("image")]
    public class ImageColor : ColorBase
    {
        private Image _source;
        /// <summary>
        /// Dữ liệu hình ảnh
        /// </summary>
        [XmlElement("source")]
        public Image Source
        {
            get { return _source; }
            set { _source = value; }
        }


        private bool _isTiled;
        /// <summary>
        /// Lấy hoặc cài đặt giá trị lặp lại của hình
        /// </summary>
        [XmlAttribute("tiled")]
        public bool IsTiled
        {
            get { return _isTiled; }
            set { _isTiled = value; }
        }

        private double _offsetX;
        /// <summary>
        /// Lấy hoặc cài đặt giá trị vị trí X
        /// </summary>
        [XmlAttribute("x")]
        public double OffsetX
        {
            get { return _offsetX; }
            set { _offsetX = value; }
        }

        private double _offsetY;
        /// <summary>
        /// Lấy hoặc cài đặt giá trị vị trí Y
        /// </summary>
        [XmlAttribute("y")]
        public double OffsetY
        {
            get { return _offsetY; }
            set { _offsetY = value; }
        }

        private double _scaleX;
        /// <summary>
        /// Thu phóng kích thước chiều dài
        /// </summary>
        [XmlAttribute("sx")]
        public double ScaleX
        {
            get { return _scaleX; }
            set { _scaleX = value; }
        }

        private double _scaleY;
        /// <summary>
        /// Thu phóng kích thước chiều rộng
        /// </summary>
        [XmlAttribute("sy")]
        public double ScaleY
        {
            get { return _scaleY; }
            set { _scaleY = value; }
        }

        private ImageAlignement _alignment;
        /// <summary>
        /// Vị trí Căn của đối tượng
        /// </summary>
        [XmlAttribute("alg")]
        public ImageAlignement Alignment
        {
            get { return _alignment; }
            set { _alignment = value; }
        }

        private MirrorType _mirrorType;
        /// <summary>
        /// Đối xứng ảnh
        /// </summary>
        [XmlAttribute("mir")]
        public MirrorType MirrorType
        {
            get { return _mirrorType; }
            set { _mirrorType = value; }
        }

        private double _offsetBottom;
        /// <summary>
        /// Lấy hoặc cài đặt giá trị vị trí X
        /// </summary>
        [XmlAttribute("bottom")]
        public double OffsetBottom
        {
            get { return _offsetBottom; }
            set { _offsetBottom = value; }
        }

        private double _offsetLeft;
        /// <summary>
        /// Lấy hoặc cài đặt giá trị vị trí X
        /// </summary>
        [XmlAttribute("left")]
        public double OffsetLeft
        {
            get { return _offsetLeft; }
            set { _offsetLeft = value; }
        }

        private double _offsetRight;
        /// <summary>
        /// Lấy hoặc cài đặt giá trị vị trí X
        /// </summary>
        [XmlAttribute("right")]
        public double OffsetRight
        {
            get { return _offsetRight; }
            set { _offsetRight = value; }
        }

        private double _offsetTop;
        /// <summary>
        /// Lấy hoặc cài đặt giá trị vị trí X
        /// </summary>
        [XmlAttribute("Top")]
        public double OffsetTop
        {
            get { return _offsetTop; }
            set { _offsetTop = value; }
        }

        /// <summary>
        /// Nhân bản một màu sắc dạng hình ảnh
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            var _imageColor = new ImageColor()
            {
                Opacity = this.Opacity,
                Source = this.Source?.Clone() as Image,
                IsTiled = this.IsTiled,
                Alignment = this.Alignment,
                MirrorType = this.MirrorType,
                OffsetX = this.OffsetX,
                OffsetY = this.OffsetY,
                RotateWidthShape = this.RotateWidthShape,
                ScaleX = this.ScaleX,
                ScaleY = this.ScaleY,
                OffsetBottom = this.OffsetBottom,
                OffsetLeft = this.OffsetLeft,
                OffsetRight = this.OffsetRight,
                OffsetTop = this.OffsetTop
            };

            return _imageColor;
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaLoad(IMediaDeserialize mediSerialize)
        {
            this.Source?.IMediaLoad(mediSerialize);
        }

        /// <summary>
        /// Ghi dữ liệu
        /// </summary>
        /// <param name="mediSerialize"></param>
        public override void IMediaSave(IMediaSerialize mediSerialize)
        {
            this.Source?.IMediaSave(mediSerialize);
        }

        /// <summary>
        /// Chuyển sang màu hệ thống
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static ColorTextureBrush Converter(ImageColor image)
        {
            ColorTextureBrush _result = new ColorTextureBrush();
            _result.IsTiled = image.IsTiled;
            _result.Mirror = image.MirrorType;
            _result.OffsetBottom = image.OffsetBottom;
            _result.OffsetLeft = image.OffsetLeft;
            _result.OffsetRight = image.OffsetRight;
            _result.OffsetTop = image.OffsetTop;
            _result.OffsetX = image.OffsetX;
            _result.OffsetY = image.OffsetY;
            _result.ScaleX = image.ScaleX;
            _result.ScaleY = image.ScaleY;
            _result.Transparentcy = 1.0 - image.Opacity;
            _result.Alignement = image.Alignment;
            _result.CanRotate = image.RotateWidthShape;
            _result.ImagePath = image.Source.Path;

            _result.Freeze();
            return _result;
        }

        /// <summary>
        /// Chuyển đổi sang dữ liệu lưu trữ
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static ImageColor Converter(ColorTextureBrush image)
        {
            ImageColor _result = new ImageColor();

            _result.IsTiled = image.IsTiled;
            _result.MirrorType = image.Mirror;
            _result.OffsetBottom = image.OffsetBottom;
            _result.OffsetLeft = image.OffsetLeft;
            _result.OffsetRight = image.OffsetRight;
            _result.OffsetTop = image.OffsetTop;
            _result.OffsetX = image.OffsetX;
            _result.OffsetY = image.OffsetY;
            _result.ScaleX = image.ScaleX;
            _result.ScaleY = image.ScaleY;
            _result.Opacity = 1.0 - image.Transparentcy;
            _result.Alignment = image.Alignement;
            _result.RotateWidthShape = image.CanRotate;
            _result.Source = new Image(image.ImagePath);
            return _result;
        }
    }

    /// <summary>
    /// Vị trí căn lề
    /// </summary>
    public enum ImageAlignement
    {
        TopLeft,
        Top,
        TopRight,
        Left,
        Center,
        Right,
        BottomLeft,
        Bottom,
        BottomRight
    }

    /// <summary>
    /// Kiểu phản chiếu
    /// </summary>
    public enum MirrorType
    {
        None,
        Vertical,
        Horizontal,
        Both
    }
}

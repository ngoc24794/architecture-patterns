﻿

namespace INV.Elearning.Core.Model
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp màu chuẩn
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(SolidColor))]
    [XmlInclude(typeof(GradientColor))]
    [XmlInclude(typeof(ImageColor))]
    [XmlInclude(typeof(PatternColor))]
    public abstract class ColorBase : RootModel
    {
        /// <summary>
        /// Độ trong suốt
        /// </summary>
        private double opacity = 1.0;

        /// <summary>
        /// Lấy hoặc cài đặt độ trong suốt cho màu sắc
        /// </summary>
        [XmlAttribute("opa")]
        public double Opacity
        {
            get
            {
                return opacity;
            }

            set
            {
                opacity = value;
            }
        }

        private string _specialName;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính tên đặc biệt của màu, tương ứng với các màu thuộc theme
        /// </summary>
        [XmlAttribute("sName")]
        public string SpecialName
        {
            get
            {

                if (string.IsNullOrEmpty(_specialName)) _specialName = this.Name;
                return _specialName;
            }
            set { _specialName = value; }
        }


        private bool _rotateWidthShape;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính có quay cùng khung hay không
        /// </summary>
        [XmlAttribute("rota")]
        public bool RotateWidthShape
        {
            get { return _rotateWidthShape; }
            set { _rotateWidthShape = value; }
        }
    }
}

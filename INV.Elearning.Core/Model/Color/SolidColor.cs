﻿

using System;
using System.Windows.Media;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Model
{
    /// <summary>
    /// Dữ liệu mà sắc đơn
    /// </summary>
    [Serializable]
    [XmlRoot("solid")]
    public class SolidColor : ColorBase
    {
        /// <summary>
        /// Mã màu dạng hexa
        /// </summary>
        private string _color = "#00000000";

        /// <summary>
        /// Lấy hoặc cài đặt thông tin mã màu dạng hexa
        /// </summary>
        [XmlAttribute("col")]
        public string Color
        {
            get
            {
                if (string.IsNullOrEmpty(_color))
                    _color = "#00000000";
                return _color;

            }

            set
            {
                _color = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Độ sáng của màu
        /// </summary>
        [XmlAttribute("br")]
        public double Brightness { get; set; }

        /// <summary>
        /// Nhân bản màu sắc
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new SolidColor() { Color = this.Color, Opacity = this.Opacity, SpecialName = this.SpecialName };
        }

        /// <summary>
        /// Chuyển màu
        /// </summary>
        /// <returns></returns>
        public System.Windows.Media.SolidColorBrush GetSolidBrush()
        {
            return new System.Windows.Media.BrushConverter().ConvertFromString(this.Color) as System.Windows.Media.SolidColorBrush;
        }

        /// <summary>
        /// Chuyển sang màu hệ thống
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static ColorSolidBrush Converter(SolidColor color)
        {
            return new ColorSolidBrush() { Color = (Color)ColorConverter.ConvertFromString(color.Color), Opacity = color.Opacity, ColorSpecialName = color.SpecialName };
        }

        /// <summary>
        /// Chuyển đổi từ màu hệ thống
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static SolidColor Converter(ColorSolidBrush color)
        {
            return new SolidColor() { Color = color.Color.ToString(), Opacity = color.Opacity, SpecialName = color.ColorSpecialName };
        }

        /// <summary>
        /// Chuyển đổi từ dữ liệu hệ thống
        /// </summary>
        /// <param name="solid"></param>
        /// <returns></returns>
        public static ColorSolidBrush Converter(SolidColorBrush solid)
        {
            if (solid == null) return new ColorSolidBrush() { Color = Colors.Transparent };
            return new ColorSolidBrush() { Color = solid.Color, Opacity = solid.Opacity };
        }
    }
}

﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Timeline
{
    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: Timing.cs
    // Description: Đối tượng thời gian
    // Develope by : Nguyen Van Ngoc
    // History:
    // 08/03/2018 : Create
    //---------------------------------------------------------------------------
    /// <summary>
    /// Đối tượng thời gian
    /// </summary>
    [Serializable]
    [XmlRoot("tim")]
    public class Timing : RootModel
    {
        private double _widthTrack;
        private double _duration;
        private double _startTime;
        private bool _showUntilEnd;
        private bool _showAlways;
        private double _leftTrack;

        public Timing()
        {
            StartTime = 0;
            Duration = 5;
            LeftTrack = 0;
            WidthTrack = 600;
            ShowAlways = false;
            ShowUntilEnd = true;
            ScaleRuler = 15;
            CuePoints = new ObservableCollection<CuePointData>();
            TotalTime = 5;
        }
        /// <summary>
        /// Thời điểm bắt đầu xuất hiện đối tượng 
        /// </summary>
        [XmlAttribute("st")]
        public double StartTime
        {
            get => Math.Round(_startTime, 2);
            set
            {
                _startTime = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Thời gian tồn tại của đối tượng
        /// </summary>
        [XmlAttribute("dur")]
        public double Duration
        {
            get { return Math.Round(_duration, 2); }
            set
            {
                _duration = value;
                OnPropertyChanged();
            }
        }

        [XmlIgnore]
        public double LeftTrack
        {
            get => _leftTrack; set
            {
                _leftTrack = value;
                OnPropertyChanged();
            }
        }
        [XmlIgnore]
        public double WidthTrack
        {
            get => _widthTrack; set
            {
                _widthTrack = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Hiển thị cho đến cuối slide hiện hành
        /// </summary>
        [XmlAttribute("showUntilEnd")]
        public bool ShowUntilEnd
        {
            get => _showUntilEnd; set
            {
                _showUntilEnd = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Hiển thị trong toàn thời gian trình chiếu của slide
        /// </summary>
        [XmlAttribute("showAlways")]
        public bool ShowAlways
        {
            get => _showAlways; set
            {
                _showAlways = value;
                OnPropertyChanged();
            }
        }

        private double _totalTime;
        /// <summary>
        /// Tổng thời gian trình chiếu của slide hiện hành
        /// </summary>
        [XmlAttribute("totalTime")]
        public double TotalTime
        {
            get { return _totalTime; }
            set
            {
                _totalTime = value;
                OnPropertyChanged();
            }
        }

        private double _scaleRuler;
        /// <summary>
        /// Đơn vị của thước
        /// </summary>
        [XmlAttribute("scale")]
        public double ScaleRuler
        {
            get { return _scaleRuler; }
            set
            {
                _scaleRuler = value;
                OnPropertyChanged("ScaleRuler");
            }
        }

        private double _headTime;
        /// <summary>
        /// Thời điểm hiện tại
        /// </summary>
        [XmlAttribute("headTime")]
        public double HeadTime
        {
            get { return _headTime; }
            set
            {
                _headTime = value;
                OnPropertyChanged("HeadTime");
            }
        }


        private ObservableCollection<CuePointData> _cuePoints;
        /// <summary>
        /// Danh sách điểm bổ sung trên thước của Timeline
        /// </summary>
        [XmlArray("cpts"), XmlArrayItem("cpt")]
        public ObservableCollection<CuePointData> CuePoints
        {
            get { return _cuePoints; }
            set
            {
                _cuePoints = value;
                OnPropertyChanged("CuePoints");
            }
        }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            Timing timing = new Timing()
            {
                Duration = this.Duration,
                LeftTrack = this.LeftTrack,
                ShowAlways = this.ShowAlways,
                ShowUntilEnd = this.ShowUntilEnd,
                StartTime = this.StartTime,
                WidthTrack = this.WidthTrack,
                TotalTime = this.TotalTime,
                ScaleRuler = this.ScaleRuler,
                CuePoints = new ObservableCollection<CuePointData>()
            };

            if (CuePoints != null)
            {
                foreach (CuePointData item in CuePoints)
                {
                    timing.CuePoints.Add(item.Clone() as CuePointData);
                }
            }

            return timing;
        }
    }
}
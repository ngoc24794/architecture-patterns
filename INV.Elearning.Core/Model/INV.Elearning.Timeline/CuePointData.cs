﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  CuePointData.cs
**
** Description: Lớp dữ liệu cho điểm bổ sung (Cue Point) dùng để lưu trữ
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Model;
using System;
using System.Xml.Serialization;

namespace INV.Elearning.Core.Timeline
{
    /// <summary>
    /// Lớp dữ liệu cho điểm bổ sung (Cue Point) dùng để lưu trữ
    /// </summary>
    [Serializable, XmlRoot("cp")]
    public class CuePointData : RootModel
    {
        public CuePointData(int order, double time)
        {
            Order = order;
            Time = time;
        }

        public CuePointData()
        {
        }

        #region Properties
        [XmlAttribute("order")]
        public int Order { get; set; }
        [XmlAttribute("time")]
        public double Time { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Tạo nhân bản
        /// </summary>
        /// <returns></returns>
        public override IElearningElement Clone()
        {
            return new CuePointData(Order, Time) { ID = ID, Name = Name };
        }
        /// <summary>
        /// Lấy model
        /// </summary>
        /// <returns></returns>
        public CuePointModel GetModel()
        {
            return new CuePointModel(Order, Time) { ID = ID, Name = Name };
        }
        #endregion
    }
}

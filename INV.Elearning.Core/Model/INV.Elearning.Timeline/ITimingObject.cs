﻿namespace INV.Elearning.Core.Timeline
{

    //---------------------------------------------------------------------------
    // Copyright (C)Huong Viet Group.  All rights reserved.
    // File: ITimingObject.cs
    // Description: Interface cho các đối tượng có thuộc tính thời gian
    // Develope by : Nguyen Van Ngoc
    // History:
    // 26/03/2018 : Modifier
    //---------------------------------------------------------------------------
    /// <summary>
    /// Interface cho các đối tượng có thuộc tính thời gian
    /// </summary>
    public interface ITimingObject
    {
        /// <summary>
        /// Trạng thái khóa/mở khóa
        /// </summary>
        bool Locked { get; set; }
        /// <summary>
        /// Trạng thái được chọn
        /// </summary>
        bool IsSelected { get; set; }
        /// <summary>
        /// Biểu tượng
        /// </summary>
        string Icon { get; set; }
        /// <summary>
        /// Tên đối tượng trên Slide
        /// </summary>
        string TargetName { get; set; }
        /// <summary>
        /// Thời gian
        /// </summary>
        Timing Timing { get; set; }
    }
}

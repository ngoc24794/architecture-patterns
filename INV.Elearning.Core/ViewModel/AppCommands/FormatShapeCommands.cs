﻿using System.Windows.Media;
using INV.Elearning.Controls.Enums;
using INV.Elearning.Core.View;
using System.Linq;
using INV.Elearning.Core.Helper;
using System.Windows;

namespace INV.Elearning.Core.ViewModel
{
    /// <summary>
    /// Lớp hỗ trợ các lệnh điều khiển định dạng chính
    /// </summary>
    public class FormatShapeCommands
    {
        /// <summary>
        /// Căn lề dọc và ngang
        /// </summary>
        const double SLIDE_MARGIN = 80.0;

        #region Static Commands

        #region ShapeBrushCommand
        private static RelayCommand _shapeBrushCommand;
        /// <summary>
        /// Lệnh cài đặt màu viền
        /// </summary>
        public static RelayCommand ShapeBrushCommand
        {
            get { return _shapeBrushCommand ?? (_shapeBrushCommand = new RelayCommand(p => ShapeBrushExcute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiể cài đặt màu viền
        /// </summary>
        /// <param name="p"></param>
        private static void ShapeBrushExcute(object p)
        {
            Global.StartGrouping();
            if (p is Controls.SolidColor solid)
            {
                var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
                var _containerElements = _elements.Select(x => x.ElementContainer);
                foreach (var item in _elements)
                {
                    if (_containerElements.Contains(item)) continue;
                    (item as IBorderSupport).Stroke = new ColorSolidBrush() { Color = (Color)ColorConverter.ConvertFromString(solid.Color), ColorSpecialName = solid.Name };
                }
            }
            else
            {
                var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
                var _containerElements = _elements.Select(x => x.ElementContainer);
                foreach (var item in _elements)
                {
                    if (_containerElements.Contains(item)) continue;
                    (item as IBorderSupport).Stroke = new ColorSolidBrush() { Color = Colors.Transparent };
                }
            }
            Global.StopGrouping();

        }
        #endregion

        #region ShapeThicknessCommand
        private static RelayCommand _shapeThicknessCommand;
        /// <summary>
        /// Lệnh cài đặt kích thước viền
        /// </summary>
        public static RelayCommand ShapeThicknessCommand
        {
            get { return _shapeThicknessCommand ?? (_shapeThicknessCommand = new RelayCommand(p => ShapeThicknessExcute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiể cài đặt kích thước viền
        /// </summary>
        /// <param name="p"></param>
        private static void ShapeThicknessExcute(object p)
        {
            Global.StartGrouping();
            var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
            var _containerElements = _elements.Select(x => x.ElementContainer);
            foreach (var item in _elements)
            {
                if (_containerElements.Contains(item)) continue;
                (item as IBorderSupport).Thickness = (double)p;
            }
            Global.StopGrouping();
        }

        #endregion

        #region ShapeFillCommand
        private static RelayCommand _shapeFillCommand;
        /// <summary>
        /// Lệnh cài đặt màu nền
        /// </summary>
        public static RelayCommand ShapeFillCommand
        {
            get { return _shapeFillCommand ?? (_shapeFillCommand = new RelayCommand(p => ShapeFillExcute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt màu nền
        /// </summary>
        /// <param name="p"></param>
        private static void ShapeFillExcute(object p)
        {
            Global.StartGrouping();
            if (p is Controls.SolidColor solid)
            {
                var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
                var _containerElements = _elements.Select(x => x.ElementContainer);
                foreach (var item in _elements)
                {
                    if (_containerElements.Contains(item)) continue;
                    (item as IBorderSupport).Fill = new ColorSolidBrush() { Color = (Color)ColorConverter.ConvertFromString(solid.Color), ColorSpecialName = solid.SpecialName };
                }
            }
            else
            {
                var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
                var _containerElements = _elements.Select(x => x.ElementContainer);
                foreach (var item in _elements)
                {
                    if (_containerElements.Contains(item)) continue;
                    (item as IBorderSupport).Fill = new ColorSolidBrush() { Color = Colors.Transparent };
                }
            }
            Global.StopGrouping();
        }

        #endregion

        #region ShapeDashCommand
        private static RelayCommand _shapeDashCommand;
        /// <summary>
        /// Lệnh cài đặt kiểu nét đứt
        /// </summary>
        public static RelayCommand ShapeDashCommand
        {
            get { return _shapeDashCommand ?? (_shapeDashCommand = new RelayCommand(p => ShapeDashExcute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt kiểu nét đứt
        /// </summary>
        /// <param name="p"></param>
        private static void ShapeDashExcute(object p)
        {
            Global.StartGrouping();
            var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
            var _containerElements = _elements.Select(x => x.ElementContainer);
            foreach (var item in _elements)
            {
                if (_containerElements.Contains(item)) continue;
                (item as IBorderSupport).DashType = (DashType)p;
            }
            Global.StopGrouping();
        }

        #endregion

        #region ShapeCapCommand
        private static RelayCommand _shapeCapCommand;
        /// <summary>
        /// Lệnh cài đặt kiểu bo góc
        /// </summary>
        public static RelayCommand ShapeCapCommand
        {
            get { return _shapeCapCommand ?? (_shapeCapCommand = new RelayCommand(p => ShapeCapExcute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt kiểu bo góc
        /// </summary>
        /// <param name="p"></param>
        private static void ShapeCapExcute(object p)
        {
            Global.StartGrouping();
            var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
            var _containerElements = _elements.Select(x => x.ElementContainer);
            foreach (var item in _elements)
            {
                if (_containerElements.Contains(item)) continue;
                (item as IBorderSupport).CapType = (PenLineCap)p;
            }
            Global.StopGrouping();
        }

        #endregion

        #region ShapeJoinCommand
        private static RelayCommand _shapeJoinCommand;
        /// <summary>
        /// Lệnh cài đặt kiểu bo góc
        /// </summary>
        public static RelayCommand ShapeJoinCommand
        {
            get { return _shapeJoinCommand ?? (_shapeJoinCommand = new RelayCommand(p => ShapeJoinExcute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt kiểu bo góc
        /// </summary>
        /// <param name="p"></param>
        private static void ShapeJoinExcute(object p)
        {
            Global.StartGrouping();
            var _elements = (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IBorderSupport);
            var _containerElements = _elements.Select(x => x.ElementContainer);
            foreach (var item in _elements)
            {
                if (_containerElements.Contains(item)) continue;
                (item as IBorderSupport).JoinType = (PenLineJoin)p;
            }
            Global.StopGrouping();
        }

        #endregion

        #endregion
    }
}

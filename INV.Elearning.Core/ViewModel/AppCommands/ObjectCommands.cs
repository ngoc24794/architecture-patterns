﻿using INV.Elearning.Animations;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using INV.Elearning.Resources;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Core.AppCommands
{
    public class ObjectCommands
    {
        #region Order Objects

        #region BringToFront
        private static RelayCommand _bringFrontCommand = null;
        /// <summary>
        /// Lệnh đưa đối tượng lên trên cùng
        /// </summary>
        public static RelayCommand BringFrontCommand
        {
            get
            {
                return _bringFrontCommand ?? (_bringFrontCommand = new RelayCommand(p => BringFrontExcute(), o => (Application.Current as IAppGlobal).SelectedElements.Count > 0));
            }
        }

        /// <summary>
        /// Thực thi lệnh đưa đối tượng lên trên cùng
        /// </summary>
        private static void BringFrontExcute()
        {
            if ((Application.Current as IAppGlobal)?.SelectedElements.Count > 0)
            {
                Panel _panelRoot = (Application.Current as IAppGlobal).SelectedElements[0].Parent as Panel;
                if (_panelRoot != null && _panelRoot.Children.Count > 1)
                {
                    int _maxZIndex = Panel.GetZIndex(_panelRoot.Children[0]);
                    for (int i = 1; i < _panelRoot.Children.Count; i++) //Tìm kiếm chỉ số ZIndex lớn nhất
                    {
                        int _temp = Panel.GetZIndex(_panelRoot.Children[i]);
                        if (_temp > _maxZIndex)
                            _maxZIndex = _temp;
                    }
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.ZIndex = _maxZIndex + 1;
                    }
                }
            }
        }
        #endregion

        #region BringForward
        private static RelayCommand _bringForwardCommand = null;
        /// <summary>
        /// Lệnh tăng ZIndex cho đối tượng
        /// </summary>
        public static RelayCommand BringForwardCommand
        {
            get
            {
                return _bringForwardCommand ?? (_bringForwardCommand = new RelayCommand(p => BringForwardExcute(), o => (Application.Current as IAppGlobal).SelectedElements.Count > 0));
            }
        }

        /// <summary>
        /// Thực thi việc tăng ZIndex cho đối tượng
        /// </summary>
        private static void BringForwardExcute()
        {
            if ((Application.Current as IAppGlobal)?.SelectedElements.Count > 0)
            {
                Panel _panelRoot = (Application.Current as IAppGlobal).SelectedElements[0].Parent as Panel;
                if (_panelRoot != null && _panelRoot.Children.Count > 1)
                {

                    List<UIElement> _children = new List<UIElement>();
                    foreach (UIElement item in _panelRoot.Children) //Clone một danh sách các phần tử
                    {
                        _children.Add(item);
                    }

                    _children.Sort((child1, child2) => Panel.GetZIndex(child1).CompareTo(Panel.GetZIndex(child2))); //Sắp xếp lại danh sách theo thứ tự ZIndex
                    for (int i = 0; i < _children.Count; i++)
                    {
                        Panel.SetZIndex(_children[i], i); //Các phần tử sẽ bắt đầu đánh dấu từ 0 đến số lượng phần tử
                    }

                    int _max = Panel.GetZIndex((Application.Current as IAppGlobal).SelectedElements[0]);
                    UIElement _uIElement = (Application.Current as IAppGlobal).SelectedElements[0];
                    for (int i = 1; i < (Application.Current as IAppGlobal).SelectedElements.Count; i++) //Lấy phần tử có chỉ số ZIndex lớn nhất trong các phần tử được lựa chọn
                    {
                        if ((Application.Current as IAppGlobal).SelectedElements[i].ZIndex > _max)
                        {
                            _max = (Application.Current as IAppGlobal).SelectedElements[i].ZIndex;
                            _uIElement = (Application.Current as IAppGlobal).SelectedElements[i];
                        }
                    }

                    int _currenIndex = _children.IndexOf(_uIElement); //Chỉ số của phần tử có ZIndex nhỏ nhất (trong danh sách được chọn) trong canvas chứa
                    if (_currenIndex < _children.Count - 1)
                    {
                        Panel.SetZIndex(_children[_currenIndex + 1], _max);
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Cài đặt theo phần tử thấp nhất
                        {
                            item.ZIndex = _max + 1;
                        }
                    }
                    else
                    {
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Cài đặt theo phần tử thấp nhất
                        {
                            item.ZIndex = _max;
                        }
                    }
                }
            }
        }
        #endregion

        #region SendToBack
        private static RelayCommand _sendBackCommand = null;
        /// <summary>
        /// Lệnh đưa đối tượng xuống dưới cùng
        /// </summary>
        public static RelayCommand SendBackCommand
        {
            get
            {
                return _sendBackCommand ?? (_sendBackCommand = new RelayCommand(p => SendBackExcute(), o => (Application.Current as IAppGlobal).SelectedElements.Count > 0));
            }
        }

        /// <summary>
        /// Thực thi lệnh đưa đối tượng xuống dưới dùng
        /// </summary>
        private static void SendBackExcute()
        {
            if ((Application.Current as IAppGlobal)?.SelectedElements.Count > 0)
            {
                Panel _panelRoot = (Application.Current as IAppGlobal).SelectedElements[0].Parent as Panel;
                if (_panelRoot != null && _panelRoot.Children.Count > 1)
                {
                    int _minZIndex = Panel.GetZIndex(_panelRoot.Children[0]);
                    for (int i = 1; i < _panelRoot.Children.Count; i++) //Tìm kiếm chỉ số ZIndex nhỏ nhất
                    {
                        int _temp = Panel.GetZIndex(_panelRoot.Children[i]);
                        if (_temp < _minZIndex)
                            _minZIndex = _temp;
                    }
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.ZIndex = _minZIndex - 1;
                    }
                }
            }
        }
        #endregion

        #region SendBackward
        private static RelayCommand _sendBackwardCommand = null;
        /// <summary>
        /// Lệnh giảm ZIndex của đối tượng
        /// </summary>
        public static RelayCommand SendBackwardCommand
        {
            get
            {
                return _sendBackwardCommand ?? (_sendBackwardCommand = new RelayCommand(p => SendBackwardExcute(), o => (Application.Current as IAppGlobal).SelectedElements.Count > 0));
            }
        }

        /// <summary>
        /// Thực thi lệnh giảm ZIndex của đối tượng
        /// </summary>
        private static void SendBackwardExcute()
        {
            if ((Application.Current as IAppGlobal)?.SelectedElements.Count > 0)
            {
                Panel _panelRoot = (Application.Current as IAppGlobal).SelectedElements[0].Parent as Panel;
                if (_panelRoot != null && _panelRoot.Children.Count > 1)
                {

                    List<UIElement> _children = new List<UIElement>();
                    foreach (UIElement item in _panelRoot.Children) //Clone một danh sách các phần tử
                    {
                        _children.Add(item);
                    }

                    _children.Sort((child1, child2) => Panel.GetZIndex(child1).CompareTo(Panel.GetZIndex(child2))); //Sắp xếp lại danh sách theo thứ tự ZIndex
                    for (int i = 0; i < _children.Count; i++)
                    {
                        Panel.SetZIndex(_children[i], i); //Các phần tử sẽ bắt đầu đánh dấu từ 0 đến số lượng phần tử
                    }

                    int _min = Panel.GetZIndex((Application.Current as IAppGlobal).SelectedElements[0]);
                    UIElement _uIElement = (Application.Current as IAppGlobal).SelectedElements[0];
                    for (int i = 1; i < (Application.Current as IAppGlobal).SelectedElements.Count; i++) //Lấy phần tử có chỉ số ZIndex nhỏ nhất trong các phần tử được lựa chọn
                    {
                        if ((Application.Current as IAppGlobal).SelectedElements[i].ZIndex < _min)
                        {
                            _min = (Application.Current as IAppGlobal).SelectedElements[i].ZIndex;
                            _uIElement = (Application.Current as IAppGlobal).SelectedElements[i];
                        }
                    }

                    int _currenIndex = _children.IndexOf(_uIElement); //Chỉ số của phần tử có ZIndex nhỏ nhất (trong danh sách được chọn) trong canvas chứa
                    if (_currenIndex > 0)
                    {
                        Panel.SetZIndex(_children[_currenIndex - 1], _min);
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Cài đặt theo phần tử thấp nhất
                        {
                            item.ZIndex = _min - 1;
                        }
                    }
                    else
                    {
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Cài đặt theo phần tử thấp nhất
                        {
                            item.ZIndex = _min;
                        }
                    }
                }
            }
        }
        #endregion

        #endregion

        #region Position Objects

        #region Align
        private static bool _isAlignToSlide = true;

        #region AlignLeft
        private static RelayCommand _alignLeftCommand = null;
        /// <summary>
        /// Lệnh điều khiển căn lề trái
        /// </summary>
        public static RelayCommand AlignLeftCommand
        {
            get
            {
                return _alignLeftCommand ?? (_alignLeftCommand = new RelayCommand(AlignLeftExcute, AlignCanExcute));
            }
        }

        /// <summary>
        /// Có thể thực thi lệnh
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool AlignCanExcute(object arg)
        {
            return (Application.Current as IAppGlobal)?.SelectedElements?.Count > 0;
        }

        /// <summary>
        /// Thực thi lệnh điều khiển căn lề trái
        /// </summary>
        /// <param name="obj"></param>
        private static void AlignLeftExcute(object obj)
        {
            if (_isAlignToSlide) //Nếu căn theo Slide
            {
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Căn tất cả về 0
                {
                    item.Left = 0; ;
                }
            }
            else //Nếu căn theo các đối tượng đang được chọn
            {
                ObjectElement _group = (Application.Current as IAppGlobal).SelectedElements.FirstOrDefault(x => x.ElementContainer != null);

                IEnumerable<ObjectElement> _elements = _group != null ? (Application.Current as IAppGlobal).SelectedElements.Where(x => !(x is GroupContainer))
                    : (Application.Current as IAppGlobal).SelectedElements;

                double _minLeft = _elements.Min(x => x.RectBound.Left); //Lấy phần tử có căn trái nhỏ nhất
                foreach (var item in _elements)
                {
                    item.Left -= (item.RectBound.Left - _minLeft);
                }
            }
        }

        #endregion

        #region AlignToObjects
        private static RelayCommand _alignToObjectCommand = null;
        /// <summary>
        /// Lệnh điều khiển căn lề giữa
        /// </summary>
        public static RelayCommand AlignToObjectCommand
        {
            get
            {
                return _alignToObjectCommand ?? (_alignToObjectCommand = new RelayCommand(AlignToObjectExecute, AlignToObjectCanExecute));
            }
        }

        /// <summary>
        /// Hàm có thể thao tác căn lề
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool AlignToObjectCanExecute(object arg)
        {
            bool _flag = (Application.Current as IAppGlobal).SelectedElements?.Count > 1;
            if (!_flag && !Proxy.IsAlignToSlide) Proxy.IsAlignToSlide = true;
            return _flag;
        }

        /// <summary>
        /// Do nothing
        /// </summary>
        /// <param name="obj"></param>
        private static void AlignToObjectExecute(object obj)
        {

        }
        #endregion

        #region AlignCenter
        private static RelayCommand _alignCenterCommand = null;
        /// <summary>
        /// Lệnh điều khiển căn lề giữa
        /// </summary>
        public static RelayCommand AlignCenterCommand
        {
            get
            {
                return _alignCenterCommand ?? (_alignCenterCommand = new RelayCommand(AlignCenterExcute, AlignCanExcute));
            }
        }

        /// <summary>
        /// Thực thi lệnh điều khiển căn lề giữa
        /// </summary>
        /// <param name="obj"></param>
        private static void AlignCenterExcute(object obj)
        {
            if (_isAlignToSlide) //Nếu căn theo Slide
            {
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Căn tất cả về 0
                {
                    item.Left = ((Application.Current as IAppGlobal).SlideSize.Width / 2.0 - item.RectBound.Width / 2.0);
                }
            }
            else //Nếu căn theo các đối tượng đang được chọn
            {
                double _minLeft = (Application.Current as IAppGlobal).SelectedElements.Min(x => x.RectBound.Left); //Lấy phần tử có căn trái nhỏ nhất
                double _maxRight = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Right); //Lấy phần tử có căn trái nhỏ nhất
                double _delta = _minLeft + (_maxRight - _minLeft) / 2.0;
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    item.Left -= (item.RectBound.Left - _delta + item.RectBound.Width / 2.0);
                }
            }
        }
        #endregion

        #region AlignRight
        private static RelayCommand _alignRightCommand = null;
        /// <summary>
        /// Lệnh điều khiển căn lề phải
        /// </summary>
        public static RelayCommand AlignRightCommand
        {
            get
            {
                return _alignRightCommand ?? (_alignRightCommand = new RelayCommand(AlignRightExcute, AlignCanExcute));
            }
        }

        /// <summary>
        /// Thực thi lệnh điều khiển căn lề phải
        /// </summary>
        /// <param name="obj"></param>
        private static void AlignRightExcute(object obj)
        {
            if (_isAlignToSlide) //Nếu căn theo Slide
            {
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Căn tất cả về 0
                {
                    item.Left = ((Application.Current as IAppGlobal).SlideSize.Width - item.RectBound.Width);
                }
            }
            else //Nếu căn theo các đối tượng đang được chọn
            {
                double _maxRight = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Right); //Lấy phần tử có căn trái nhỏ nhất
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    item.Left += (_maxRight - item.RectBound.Right);
                }
            }
        }
        #endregion

        #region AlignTop
        private static RelayCommand _alignTopCommand = null;
        /// <summary>
        /// Lệnh điều khiển căn lề trên
        /// </summary>
        public static RelayCommand AlignTopCommand
        {
            get
            {
                return _alignTopCommand ?? (_alignTopCommand = new RelayCommand(AlignTopExcute, AlignCanExcute));
            }
        }

        /// <summary>
        /// Thực thi lệnh điều khiển căn lề trên
        /// </summary>
        /// <param name="obj"></param>
        private static void AlignTopExcute(object obj)
        {
            if (_isAlignToSlide) //Nếu căn theo Slide
            {
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Căn tất cả về 0
                {
                    item.Top = 0;
                }
            }
            else //Nếu căn theo các đối tượng đang được chọn
            {
                double _minTop = (Application.Current as IAppGlobal).SelectedElements.Min(x => x.RectBound.Top); //Lấy phần tử có căn trái nhỏ nhất
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    item.Top -= (item.RectBound.Top - _minTop);
                }
            }
        }
        #endregion

        #region AlignMiddle
        private static RelayCommand _alignMiddleCommand = null;
        /// <summary>
        /// Lệnh điều khiển căn lề giữa
        /// </summary>
        public static RelayCommand AlignMiddleCommand
        {
            get
            {
                return _alignMiddleCommand ?? (_alignMiddleCommand = new RelayCommand(AlignMiddleExcute, AlignCanExcute));
            }
        }

        /// <summary>
        /// Thực thi lệnh điều khiển căn lề giữa
        /// </summary>
        /// <param name="obj"></param>
        private static void AlignMiddleExcute(object obj)
        {
            if (_isAlignToSlide) //Nếu căn theo Slide
            {
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Căn tất cả về 0
                {
                    item.Top = ((Application.Current as IAppGlobal).SlideSize.Height / 2.0 - item.RectBound.Height / 2.0);
                }
            }
            else //Nếu căn theo các đối tượng đang được chọn
            {
                double _minTop = (Application.Current as IAppGlobal).SelectedElements.Min(x => x.RectBound.Top); //Lấy phần tử có căn trên nhỏ nhất
                double _maxBottom = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Bottom); //Lấy phần tử có căn dưới nhỏ nhất
                double _delta = _minTop + (_maxBottom - _minTop) / 2.0;
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    item.Top -= (item.RectBound.Top - _delta + item.RectBound.Height / 2.0);
                }
            }
        }
        #endregion

        #region AlignBottom
        private static RelayCommand _alignBottomCommand = null;
        /// <summary>
        /// Lệnh điều khiển căn lề dưới
        /// </summary>
        public static RelayCommand AlignBottomCommand
        {
            get
            {
                return _alignBottomCommand ?? (_alignBottomCommand = new RelayCommand(AlignBottomExcute, AlignCanExcute));
            }
        }

        /// <summary>
        /// Thực thi lệnh điều khiển căn lề dưới
        /// </summary>
        /// <param name="obj"></param>
        private static void AlignBottomExcute(object obj)
        {
            if (_isAlignToSlide) //Nếu căn theo Slide
            {
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements) //Căn tất cả về 0
                {
                    item.Top = ((Application.Current as IAppGlobal).SlideSize.Height - item.RectBound.Height);
                }
            }
            else //Nếu căn theo các đối tượng đang được chọn
            {
                double _maxBottom = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Bottom); //Lấy phần tử có căn trái nhỏ nhất
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    item.Top += (_maxBottom - item.RectBound.Bottom);
                }
            }
        }
        #endregion

        #region Distributes
        private static RelayCommand _distributeCommand = null;
        /// <summary>
        /// Lệnh điều khiển giãn khoảng cách
        /// </summary>
        public static RelayCommand DistributeCommand
        {
            get
            {
                return _distributeCommand ?? (_distributeCommand = new RelayCommand(DistributeExcute, DistributeCanExcute));
            }
        }

        /// <summary>
        /// Điều kiện thực thi lệnh
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool DistributeCanExcute(object arg)
        {
            if (_isAlignToSlide)
            {
                return (Application.Current as IAppGlobal).SelectedElements?.Count > 0;
            }
            else
            {
                return (Application.Current as IAppGlobal).SelectedElements?.Count > 2;
            }
        }

        /// <summary>
        /// Thực thi lệnh điều khiển căn lề dưới
        /// </summary>
        /// <param name="obj"></param>
        private static void DistributeExcute(object obj)
        {
            if (obj?.ToString() == "Vertical")
            {
                double _extraVertical = 0, _currentTop = 0;
                double _sumHeight = (Application.Current as IAppGlobal).SelectedElements.Sum(x => x.RectBound.Height); //Tổng kích thước của các đối tượng
                var _oderList = (Application.Current as IAppGlobal).SelectedElements.OrderBy(x => x.RectBound.Top);
                if (_isAlignToSlide) //Nếu căn theo Slide
                {
                    _extraVertical = (Application.Current as IAppGlobal).SlideSize.Height;
                    var _firstItem = _oderList.FirstOrDefault(); //Cài đặt giá trị căn lề cho đối tượng đầu tiên.
                    _firstItem.Top = _firstItem.RectBound.Height / 2 - _firstItem.Height / 2;

                    var _lastItem = _oderList.LastOrDefault();
                    _lastItem.Top = (_extraVertical - _lastItem.RectBound.Height / 2 - _lastItem.Height / 2);

                    _currentTop = _firstItem.RectBound.Height;
                }
                else //Nếu căn theo các đối tượng đang được chọn
                {
                    double _minTop = (Application.Current as IAppGlobal).SelectedElements.Min(x => x.RectBound.Top);
                    double _maxBottom = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Bottom);
                    _extraVertical = _maxBottom - _minTop; //Khoảng cách căn lề     
                    _currentTop = _oderList.ElementAt(0).RectBound.Bottom;
                }

                double _deltaVerical = (_extraVertical - _sumHeight) / ((Application.Current as IAppGlobal).SelectedElements.Count - 1);
                for (int i = 1; i < _oderList.Count() - 1; i++)
                {
                    var _item = _oderList.ElementAt(i);
                    _item.Top = (_currentTop + _deltaVerical + _item.RectBound.Height / 2 - _item.Height / 2);
                    _currentTop = _item.RectBound.Height + _currentTop + _deltaVerical;
                }
            }
            else
            {
                double _extraHorizontal = 0, _currentLeft = 0;
                double _sumWidth = (Application.Current as IAppGlobal).SelectedElements.Sum(x => x.RectBound.Width); //Tổng kích thước của các đối tượng
                var _oderList = (Application.Current as IAppGlobal).SelectedElements.OrderBy(x => x.RectBound.Left);
                if (_isAlignToSlide) //Nếu căn theo Slide
                {
                    _extraHorizontal = (Application.Current as IAppGlobal).SlideSize.Width;
                    var _firstItem = _oderList.FirstOrDefault(); //Cài đặt giá trị căn lề cho đối tượng đầu tiên.
                    _firstItem.Left = _firstItem.RectBound.Width / 2 - _firstItem.Width / 2;

                    var _lastItem = _oderList.LastOrDefault();
                    _lastItem.Left = (_extraHorizontal - _lastItem.RectBound.Width / 2 - _lastItem.Width / 2);

                    _currentLeft = _firstItem.RectBound.Width;
                }
                else //Nếu căn theo các đối tượng đang được chọn
                {
                    double _minLeft = (Application.Current as IAppGlobal).SelectedElements.Min(x => x.RectBound.Left);
                    double _maxRight = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Right);
                    _extraHorizontal = _maxRight - _minLeft; //Khoảng cách căn lề     
                    _currentLeft = _oderList.ElementAt(0).RectBound.Right;
                }

                double _deltaHorizontal = (_extraHorizontal - _sumWidth) / ((Application.Current as IAppGlobal).SelectedElements.Count - 1);
                for (int i = 1; i < _oderList.Count() - 1; i++)
                {
                    var _item = _oderList.ElementAt(i);
                    _item.Left = (_currentLeft + _deltaHorizontal + _item.RectBound.Width / 2 - _item.Width / 2);
                    _currentLeft = _item.RectBound.Width + _currentLeft + _deltaHorizontal;
                }
            }
        }
        #endregion

        #endregion

        #region Rotate

        #region Rotate

        private static RelayCommand _rotateCommand = null;
        /// <summary>
        /// Lệnh điều khiển quay ảnh
        /// </summary>
        public static RelayCommand RotateCommand
        {
            get
            {
                return _rotateCommand ?? (_rotateCommand = new RelayCommand(RotateExcute, RotateCanExecute));
            }
        }

        /// <summary>
        /// Quay góc
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool RotateCanExecute(object arg)
        {
            return (Application.Current as IAppGlobal).SelectedElements.Where(x=>x.RuleConfiguration.CanRotate)?.Count() > 0;
        }

        /// <summary>
        /// Điều kiện đảo
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool ScaleCanExecute(object arg)
        {
            return (Application.Current as IAppGlobal).SelectedElements.Count > 0;
        }

        /// <summary>
        /// Thực thi lệnh quay đối tượng
        /// </summary>
        /// <param name="obj"></param>
        private static void RotateExcute(object obj)
        {
            if ((Application.Current as IAppGlobal)?.SelectedElements.Count > 0)
            {
                double _angle = 0.0;
                if (double.TryParse(obj?.ToString(), out _angle))
                {
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.Angle += _angle;
                    }
                }
            }
        }

        #endregion

        #region Flip

        private static RelayCommand _flipCommand = null;
        /// <summary>
        /// Lệnh điều khiển đảo ảnh
        /// </summary>
        public static RelayCommand FlipCommand
        {
            get
            {
                return _flipCommand ?? (_flipCommand = new RelayCommand(FlipExcute, ScaleCanExecute));
            }
        }

        /// <summary>
        /// Thực thi lệnh điều khiển đảo ảnh
        /// </summary>
        /// <param name="obj"></param>
        private static void FlipExcute(object obj)
        {
            if ((Application.Current as IAppGlobal)?.SelectedElements.Count > 0)
            {
                bool _isHori = false; //Quay ngang
                if (bool.TryParse(obj?.ToString(), out _isHori))
                {
                    if (_isHori)
                    {
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                        {
                            item.IsScaleX = !item.IsScaleX;
                        }
                    }
                    else
                    {
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                        {
                            item.IsScaleY = !item.IsScaleY;
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Group Objects

        #region GroupCommand
        private static RelayCommand _groupCommand = null;
        /// <summary>
        /// Lệnh điều khiển nhóm các đối tượng
        /// </summary>
        public static RelayCommand GroupCommand
        {
            get
            {
                return _groupCommand ?? (_groupCommand = new RelayCommand(GroupExcute, GroupCanExcute));
            }
        }

        /// <summary>
        /// Kiểm tra có thể thực thi lệnh
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool GroupCanExcute(object arg)
        {
            return (Application.Current as IAppGlobal)?.SelectedElements.Where(x => x.ElementContainer == null).Count() > 1 && (Application.Current as IAppGlobal)?.SelectedElements.Where(x => !x.RuleConfiguration.CanGroup).Count() == 0;
        }

        private static void GroupExcute(object obj)
        {
            if ((Application.Current as IAppGlobal)?.SelectedElements.Count > 0)
            {
                Global.StartGrouping();
                GroupContainer _group = new GroupContainer();

                double _top = (Application.Current as IAppGlobal).SelectedElements.Min(x => x.RectBound.Top);
                double _bottom = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Bottom);
                double _left = (Application.Current as IAppGlobal).SelectedElements.Min(x => x.RectBound.Left);
                double _right = (Application.Current as IAppGlobal).SelectedElements.Max(x => x.RectBound.Right);

                _group.Top = _top;
                _group.Left = _left;
                _group.Width = _right - _left;
                _group.Height = _bottom - _top;

                Global.BeginInit();
                while ((Application.Current as IAppGlobal).SelectedElements.Count > 0)
                {
                    ObjectElement item = (Application.Current as IAppGlobal).SelectedElements[0];
                    item.IsSelected = false;
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Remove(item);
                    if (item is GroupContainer) //Nếu là một Group
                    {
                        (item as GroupContainer).CanContentFocus = true;
                        (item as GroupContainer).IsContentFocused = true;
                    }

                    Rect _rectPercents = new Rect((item.Left - _left) / _group.Width, (item.Top - _top) / _group.Height, item.Width / _group.Width, item.Height / _group.Height); //Tỷ lệ so với khung chứa
                    GroupContainer.SetOriginPercents(item, _rectPercents);
                    _group.Elements.Add(item);
                }
                Global.EndInit();
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(_group);
                _group.IsSelected = true;
                Global.PushUndo(new GroupElementsStep(_group));
                Global.StopGrouping();
            }
        }
        #endregion

        #region UnGroupCommand
        private static RelayCommand _unGroupCommand = null;
        /// <summary>
        /// Lệnh điều khiển nhóm các đối tượng
        /// </summary>
        public static RelayCommand UnGroupCommand
        {
            get
            {
                return _unGroupCommand ?? (_unGroupCommand = new RelayCommand(UnGroupExcute, UnGroupCanExcute));
            }
        }

        /// <summary>
        /// Kiểm tra có thể thực thi lệnh
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool UnGroupCanExcute(object arg)
        {
            return (Application.Current as IAppGlobal)?.SelectedElements.Where(x => x is GroupContainer).Count() > 0;
        }

        /// <summary>
        /// Thực thi nhóm của đối tượng
        /// </summary>
        /// <param name="obj"></param>
        private static void UnGroupExcute(object obj)
        {
            List<ObjectElement> _elements = new List<ObjectElement>((Application.Current as IAppGlobal)?.SelectedElements);
            for (int i = 0; i < _elements.Count; i++)
            {
                ObjectElement _item = _elements[i];
                if (_item is GroupContainer)
                {
                    (_item as GroupContainer).UnGroup();
                }
            }
        }
        #endregion

        #region ReGroupCommand
        private static RelayCommand _reGroupCommand = null;
        /// <summary>
        /// Lệnh điều khiển nhóm lại các đối tượng
        /// </summary>
        public static RelayCommand ReGroupCommand
        {
            get
            {
                return _reGroupCommand ?? (_reGroupCommand = new RelayCommand(ReGroupExcute, ReGroupCanExcute));
            }
        }

        /// <summary>
        /// Kiểm tra có thể thực thi lệnh
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool ReGroupCanExcute(object arg)
        {
            var _preGroupElements = (Application.Current as IAppGlobal)?.SelectedElements?.Where(x => x.PreviousElementContainer != null)?.GroupBy(x => x.PreviousElementContainer)?.Select(g => new { Group = g.Key, Count = g.Count() });
            foreach (var group in _preGroupElements)
            {
                var _count = (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Elements?.Where(x => x.PreviousElementContainer == group.Group)?.Count();
                if (_count > 1)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Thực thi nhóm lại các đối tượng
        /// </summary>
        /// <param name="obj"></param>
        private static void ReGroupExcute(object obj)
        {
            var _preGroupElements = (Application.Current as IAppGlobal)?.SelectedElements?.Where(x => x.PreviousElementContainer != null).GroupBy(x => x.PreviousElementContainer).Select(g => new { Group = g.Key, Count = g.Count() });

            if (_preGroupElements == null)
                return;

            foreach (var group in _preGroupElements)
            {
                var _elements = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Where(x => x.PreviousElementContainer == group.Group);

                if (_elements.Count() > 1)
                {
                    GroupContainer _group = new GroupContainer();

                    double _top = _elements.Min(x => x.RectBound.Top);
                    double _bottom = _elements.Max(x => x.RectBound.Bottom);
                    double _left = _elements.Min(x => x.RectBound.Left);
                    double _right = _elements.Max(x => x.RectBound.Right);

                    _group.Top = _top;
                    _group.Left = _left;
                    _group.Width = _right - _left;
                    _group.Height = _bottom - _top;

                    for (int i = 0; i < _elements.Count(); i++)
                    {
                        ObjectElement item = _elements.ElementAt(i);
                        item.IsSelected = false;
                        _group.Elements.Add(item);
                        i--;
                    }

                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(_group);
                    _group.IsSelected = true;
                }
            }
        }
        #endregion
        #endregion


        #region PositionCommand
        private static RelayCommand _moveObjectsCommand;
        /// <summary>
        /// Lenh dieu khien di chuyen
        /// </summary>
        public static RelayCommand MoveObjectCommand
        {
            get { return _moveObjectsCommand ?? (_moveObjectsCommand = new RelayCommand(MoveObjectsExecute, MoveObjectsCanExecute)); }
        }

        /// <summary>
        /// Dieu kien thuc thi
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool MoveObjectsCanExecute(object arg)
        {
            return (Application.Current as IAppGlobal).SelectedElements.Count > 0;
        }

        private static void MoveObjectsExecute(object obj)
        {
            Global.StartGrouping();
            switch (obj.ToString())
            {
                case "Left":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.Left -= 1.0;
                    }
                    break;
                case "Right":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.Left += 1.0;
                    }
                    break;
                case "Top":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.Top -= 1.0;
                    }
                    break;
                case "Bottom":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.Top += 1.0;
                    }
                    break;
            }
            Global.StopGrouping();
        }

        #endregion

        #region PositionCommand
        private static RelayCommand _sizeObjectsCommand;
        /// <summary>
        /// Lenh dieu khien thay doi kich thuoc
        /// </summary>
        public static RelayCommand SizeObjectCommand
        {
            get { return _sizeObjectsCommand ?? (_sizeObjectsCommand = new RelayCommand(SizeObjectsExecute, SizeObjectsCanExecute)); }
        }

        /// <summary>
        /// Dieu kien thuc thi
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private static bool SizeObjectsCanExecute(object arg)
        {
            return (Application.Current as IAppGlobal).SelectedElements.Count > 0;
        }

        private static void SizeObjectsExecute(object obj)
        {
            Global.StartGrouping();
            switch (obj.ToString())
            {
                case "IncreaWidth":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.AddWidthDelta(1.0, true, item.ActualWidth);
                        item.AddWidthDelta(1.0, false, item.ActualWidth);
                    }
                    break;
                case "DecreaWidth":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.AddWidthDelta(-1.0, true, item.ActualWidth);
                        item.AddWidthDelta(-1.0, false, item.ActualWidth);
                    }
                    break;
                case "IncreaHeight":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.AddHeightDelta(1.0, true, item.ActualHeight);
                        item.AddHeightDelta(1.0, false, item.ActualHeight);
                    }
                    break;
                case "DecreaHeight":
                    foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                    {
                        item.AddHeightDelta(-1.0, true, item.ActualHeight);
                        item.AddHeightDelta(-1.0, false, item.ActualHeight);
                    }
                    break;
            }
            Global.StopGrouping();
        }

        #endregion

        #region Save as Picture

        private static RelayCommand _saveAsPictureCommand;
        /// <summary>
        /// Lưu dữ liệu ra hình ảnh
        /// </summary>
        public static RelayCommand SaveAsPictureCommand
        {
            get { return _saveAsPictureCommand ?? (_saveAsPictureCommand = new RelayCommand(p => SaveAsPictureExcute(p))); }
        }

        private static void SaveAsPictureExcute(object p)
        {
            if (p is ObjectElement objectElement)
            {
                SaveFileDialog _saveFileDialog = new SaveFileDialog();
                _saveFileDialog.Title = "Avina";
                _saveFileDialog.Filter = "PNG image file (*.png)|*.png|JPEG image file (*.jpg)|*.jpg|Bitmap image file (*.bmp)|*.bmp";
                if (_saveFileDialog.ShowDialog() == true)
                {
                    objectElement.SaveToImage(_saveFileDialog.FileName);
                }
            }
        }


        #endregion

        #region Editor Commands

        #region DeleteCommand
        private static RelayCommand _deleteCommand;
        /// <summary>
        /// Lệnh điều khiển xóa dữ liệu
        /// </summary>
        public static RelayCommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(p => DeleteExcute(p))); }
        }

        /// <summary>
        /// Thực thi xóa dữ liệu
        /// </summary>
        /// <param name="p"></param>
        private static void DeleteExcute(object p)
        {
            Global.StartGrouping();
            // THAIWPF SỬA CÁC ĐỐI tượng không cho phép xóa
            // start
            var _selectedElements = (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements.Where(x => x.IsSelected == true && x.RuleConfiguration.CanDelete == false).ToList();
            if (_selectedElements?.Count > 0)
            {
                var element = _selectedElements.First();
                var result = INV.Elearning.Controls.MessageBox.ShowDialog(element.TargetName + " " + GetResource("Core_NotDelete"), GetResource("Core_WN"), INV.Elearning.Controls.DialogType.Warning, INV.Elearning.Controls.ButtonType.OK);
                return;
            }
            //end
            for (int i = 0; i < (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Elements.Count; i++) //Xóa các đối tượng đang được chọn
            {
                var _element = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i];
                if (_element.IsSelected)
                {
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.RemoveAt(i);
                    _element.IsSelected = false;
                    i--;
                }
            }
            if ((Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Children != null)
            {
                for (int i = 0; i < (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Count; i++)
                {
                    if ((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children[i] is MotionPathObject motionPathObject && motionPathObject.IsSelected)
                    {
                        if (motionPathObject.Owner is ObjectElement owner)
                        {
                            motionPathObject.IsSelected = false;
                            Global.PushUndo(new DeleteMotionPathUndo(motionPathObject, (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout));
                            owner.MotionPaths?.Remove(motionPathObject);
                            (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
            Global.StopGrouping();
        }

        #endregion

        #region DeleteCommand
        private static RelayCommand _selectedAllCommand;
        /// <summary>
        /// Lệnh điều khiển lựa chọn tất cả đối tượng trên slide
        /// </summary>
        public static RelayCommand SelectedAllCommand
        {
            get { return _selectedAllCommand ?? (_selectedAllCommand = new RelayCommand(p => SelectedAllExcute(p))); }
        }

        /// <summary>
        /// Thực thi xóa dữ liệu
        /// </summary>
        /// <param name="p"></param>
        private static void SelectedAllExcute(object p)
        {
            for (int i = 0; i < (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Elements.Count; i++) //Xóa các đối tượng đang được chọn
            {
                (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].IsSelected = true;
            }
            (Application.Current as IAppGlobal).SelectedSlide.UpdateLayout();
        }

        #endregion

        #region CutObjectCommand

        private static RelayCommand _cutObjectCommand;
        /// <summary>
        /// Cắt đối tượng vào Clipboard
        /// </summary>
        public static RelayCommand CutObjectCommand
        {
            get { return _cutObjectCommand ?? (_cutObjectCommand = new RelayCommand(p => CutObjectExcute(p), o => (Application.Current as IAppGlobal)?.SelectedElements.Count > 0)); }
        }

        /// <summary>
        /// Thực thi
        /// </summary>
        /// <param name="p"></param>
        private static void CutObjectExcute(object p)
        {
            List<ObjectElementBase> _objectsData = new List<ObjectElementBase>();
            Global.StartGrouping();
            // THAIWPF SỬA CÁC ĐỐI tượng không cho phép cut
            // start
            var _selectedElements = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Where(x => x.IsSelected == true && x.RuleConfiguration.CanCopy == false).ToList();
            if (_selectedElements?.Count > 0)
            {
                var element = _selectedElements.First();
                var result = INV.Elearning.Controls.MessageBox.ShowDialog(element.TargetName + " " + GetResource("Core_NotCut"), GetResource("Core_WN"), INV.Elearning.Controls.DialogType.Warning, INV.Elearning.Controls.ButtonType.OK);
                return;
            }
            //end

            for (int i = 0; i < (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Elements.Count; i++) //Xóa các đối tượng đang được chọn
            {
                var _element = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i];
                if (_element.IsSelected && _element.RuleConfiguration.CanCopy && _element.RuleConfiguration.CanDelete)
                {
                    _element.RefreshData(); //Cập nhật dữ liệu
                    _objectsData.Add(_element.Data.Clone() as ObjectElementBase); //Đưa vào danh sách

                    _element.IsSelected = false;
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.RemoveAt(i);
                    i--;
                }
            }

            if ((Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Children != null)
            {
                for (int i = 0; i < (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.Count; i++)
                {
                    if ((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children[i] is MotionPathObject motionPathObject && motionPathObject.IsSelected)
                    {
                        if (motionPathObject.Owner is ObjectElement owner)
                        {
                            motionPathObject.RefreshData();
                            _objectsData.Add(motionPathObject.Data.Clone() as ObjectElementBase);

                            motionPathObject.IsSelected = false;
                            owner.MotionPaths?.Remove(motionPathObject);
                            (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }

            Clipboard.Clear();
            Global.StopGrouping();
            Clipboard.SetData(ClipboardHelpers.CLIPBOARD_DATA_KEY, _objectsData); //Đưa vào dữ liệu bộ nhớ tạm
        }

        #endregion

        #region CopyObjectCommand

        private static RelayCommand _copytObjectCommand;
        /// <summary>
        /// Sao chép đối tượng vào Clipboard
        /// </summary>
        public static RelayCommand CopyObjectCommand
        {
            get { return _copytObjectCommand ?? (_copytObjectCommand = new RelayCommand(p => CopyObjectExcute(), o => (Application.Current as IAppGlobal)?.SelectedElements.Count > 0)); }
        }

        /// <summary>
        /// Thực thi
        /// </summary>
        private static void CopyObjectExcute()
        {
            DataObject _dataObject = new DataObject();
            List<ObjectElementBase> _objectsData = new List<ObjectElementBase>();

            // THAIWPF SỬA CÁC ĐỐI tượng không cho phép copy
            // start
            var _selectedElements = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Where(x => x.IsSelected == true && x.RuleConfiguration.CanCopy == false).ToList();
            if (_selectedElements?.Count > 0)
            {
                var element = _selectedElements.First();
                var result = INV.Elearning.Controls.MessageBox.ShowDialog(element.TargetName + " " + GetResource("Core_NotCopy"), GetResource("Core_WN"), INV.Elearning.Controls.DialogType.Warning, INV.Elearning.Controls.ButtonType.OK);
                return;
            }
            //end

            for (int i = 0; i < (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Elements.Count; i++) //Xóa các đối tượng đang được chọn
            {
                if ((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].IsSelected && (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].RuleConfiguration.CanCopy)
                {
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].RefreshData(); //Cập nhật dữ liệu
                    _objectsData.Add((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].Data.Clone() as ObjectElementBase); //Đưa vào danh sách
                }
            }
            if ((Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Children != null)
            {
                foreach (var item in (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Children)
                {
                    if (item is MotionPathObject motionPathObject && motionPathObject.IsSelected)
                    {
                        motionPathObject.RefreshData();
                        _objectsData.Add(motionPathObject.Data.Clone() as ObjectElementBase);
                    }
                }
            }

            _dataObject.SetData(ClipboardHelpers.CLIPBOARD_DATA_KEY, _objectsData, false);
            if (_objectsData.Count == 1) //Nếu chỉ đang lựa chọn 1 phần tử
            {
                MemoryStream memoryStream = new MemoryStream();
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((Application.Current as IAppGlobal).SelectedItem.SaveToBitmap()));
                enc.Save(memoryStream);
                _dataObject.SetData("PNG", memoryStream, false);
            }
            Clipboard.Clear();
            Clipboard.SetDataObject(_dataObject); //Đưa vào dữ liệu bộ nhớ tạm
        }

        #endregion

        #region DuplicateObjectCommand

        private static RelayCommand _duplicateObjectCommand;
        /// <summary>
        /// Sao chép đối tượng vào Clipboard
        /// </summary>
        public static RelayCommand DuplicateObjectCommand
        {
            get { return _duplicateObjectCommand ?? (_duplicateObjectCommand = new RelayCommand(p => DuplicateObjectExcute(), o => (Application.Current as IAppGlobal)?.SelectedElements.Count > 0)); }
        }

        /// <summary>
        /// Thực thi
        /// </summary>
        private static void DuplicateObjectExcute()
        {
            DataObject _dataObject = new DataObject();
            List<ObjectElementBase> _objectsData = new List<ObjectElementBase>();
            for (int i = 0; i < (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Elements.Count; i++) //Xóa các đối tượng đang được chọn
            {
                if ((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].IsSelected && (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].RuleConfiguration.CanCopy)
                {
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].RefreshData(); //Cập nhật dữ liệu
                    _objectsData.Add((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements[i].Data.Clone() as ObjectElementBase); //Đưa vào danh sách
                }
            }
            if ((Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Children != null)
            {
                foreach (var item in (Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Children)
                {
                    if (item is MotionPathObject motionPathObject && motionPathObject.IsSelected)
                    {
                        motionPathObject.RefreshData();
                        _objectsData.Add(motionPathObject.Data.Clone() as ObjectElementBase);
                    }
                }
            }
            PasteData(_objectsData, false);
        }

        #endregion

        #region PasteNormalObjectCommand

        private static RelayCommand _pasteNormalObjectCommand;
        /// <summary>
        /// Dán dữ liệu từ Clipboard, dựng lại các đối tượng dữ liệu
        /// </summary>
        public static RelayCommand PasteNormalObjectCommand
        {
            get { return _pasteNormalObjectCommand ?? (_pasteNormalObjectCommand = new RelayCommand(p => PasteNormalObjectExcute(p), o => Clipboard.GetDataObject().GetFormats().Contains(ClipboardHelpers.CLIPBOARD_DATA_KEY))); }
        }

        private const double DELTA_TOPLEFT = 30.0;
        /// <summary>
        /// Thực thi
        /// </summary>
        private static void PasteNormalObjectExcute(object p)
        {
            var _data = Clipboard.GetDataObject().GetData(ClipboardHelpers.CLIPBOARD_DATA_KEY);
            if (_data is List<ObjectElementBase> objectsData)
            {
                PasteData(objectsData);
            }
            if (p is System.Windows.Controls.ContextMenu menu)
            {
                menu.IsOpen = false;
            }
        }

        /// <summary>
        /// Dán dữ liệu
        /// </summary>
        /// <param name="objectsData"></param>
        private static void PasteData(List<ObjectElementBase> objectsData, bool autoLocation = true)
        {
            if (objectsData.Count > 0)
            {
                ObjectElementsHelper.UnSelectedAll();
                Global.IsPasting = true;
                List<string> pastedIDs = new List<string>();
                foreach (var item in objectsData)
                {
                    var _object = ObjectElementsHelper.LoadData(item);
                    if (_object != null)
                    {
                        if (_object is MotionPathObject motionPathObject)
                        {
                            foreach (ObjectElement owner in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
                            {
                                if (owner == motionPathObject.Owner)
                                {
                                    owner.MotionPaths?.Add(motionPathObject);
                                }
                            }

                            (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Children?.Add(motionPathObject);
                            pastedIDs.Add(motionPathObject.ID);
                            if (autoLocation)
                            {
                                _object.Left += DELTA_TOPLEFT;
                                _object.Top += DELTA_TOPLEFT;
                            }
                            //_object.IsSelected = true;
                            continue;
                        }
                        if (autoLocation && (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.FirstOrDefault(x => x.ID == item.ID) != null) //Nếu có tồn tại đối tượng sao chép trên trang đang thao tác
                        {
                            _object.Left += DELTA_TOPLEFT;
                            _object.Top += DELTA_TOPLEFT;
                            foreach (MotionPathObject path in _object.MotionPaths)
                            {
                                path.Left += DELTA_TOPLEFT;
                                path.Top += DELTA_TOPLEFT;
                            }
                        }
                        (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(_object);
                        pastedIDs.Add(_object.ID);
                        //_object.IsSelected = true;
                    }
                }

                Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    foreach (var id in pastedIDs)
                    {
                        var element = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.FirstOrDefault(x => x.ID == id);
                        //if (element != null) element.IsSelected = true;
                    }
                    Global.IsPasting = false;
                }));
            }
        }
        #endregion

        #region PasteNormalObjectCommand

        private static RelayCommand _pasteUpdateThemeNormaNormalObjectCommand;
        /// <summary>
        /// Dán dữ liệu từ Clipboard, dựng lại các đối tượng dữ liệu
        /// </summary>
        public static RelayCommand PasteUpdateThemeNormalObjectCommand
        {
            get { return _pasteUpdateThemeNormaNormalObjectCommand ?? (_pasteUpdateThemeNormaNormalObjectCommand = new RelayCommand(p => PasteUpdateThemeObjectExcute(p), o => Clipboard.GetDataObject().GetFormats().Contains(ClipboardHelpers.CLIPBOARD_DATA_KEY))); }
        }

        /// <summary>
        /// Thực thi
        /// </summary>
        private static void PasteUpdateThemeObjectExcute(object p)
        {
            var _data = Clipboard.GetDataObject().GetData(ClipboardHelpers.CLIPBOARD_DATA_KEY);
            if (_data is List<ObjectElementBase> objectsData)
            {
                if (objectsData.Count > 0)
                {
                    ObjectElementsHelper.UnSelectedAll();
                    Global.IsPasting = true;
                    List<string> pastedIDs = new List<string>();
                    foreach (var item in objectsData)
                    {
                        var _object = ObjectElementsHelper.LoadData(item);
                        if (_object != null)
                        {
                            if (_object is MotionPathObject motionPathObject)
                            {
                                foreach (ObjectElement owner in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
                                {
                                    if (owner == motionPathObject.Owner)
                                    {
                                        owner.MotionPaths?.Add(motionPathObject);
                                    }
                                }

                                (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Children?.Add(motionPathObject);
                                pastedIDs.Add(motionPathObject.ID);
                                _object.Left += DELTA_TOPLEFT;
                                _object.Top += DELTA_TOPLEFT;
                                //_object.IsSelected = true;
                                continue;
                            }
                            if ((Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.FirstOrDefault(x => x.ID == item.ID) != null) //Nếu có tồn tại đối tượng sao chép trên trang đang thao tác
                            {
                                _object.Left += DELTA_TOPLEFT;
                                _object.Top += DELTA_TOPLEFT;
                                foreach (MotionPathObject path in _object.MotionPaths)
                                {
                                    path.Left += DELTA_TOPLEFT;
                                    path.Top += DELTA_TOPLEFT;
                                }
                            }
                            (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(_object);
                            pastedIDs.Add(_object.ID);
                            _object.UpdateThemeFont();
                            _object.UpdateThemeColor();
                            //_object.IsSelected = true;
                        }
                    }
                    Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                    {
                        foreach (var id in pastedIDs)
                        {
                            var element = (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.FirstOrDefault(x => x.ID == id);
                            if (element != null) element.IsSelected = true;
                        }
                        Global.IsPasting = false;
                    }));
                }
            }
            if (p is System.Windows.Controls.ContextMenu menu)
            {
                menu.IsOpen = false;
            }
        }

        #endregion

        #region PasteNormalObjectCommand

        private static RelayCommand _pasteImageObjectCommand;
        /// <summary>
        /// Dán dữ liệu từ Clipboard, dựng lại các đối tượng dữ liệu
        /// </summary>
        public static RelayCommand PasteImageObjectCommand
        {
            get { return _pasteImageObjectCommand ?? (_pasteImageObjectCommand = new RelayCommand(p => PasteImageObjectExcute(), o => Clipboard.GetDataObject()?.GetData(ClipboardHelpers.CLIPBOARD_DATA_KEY) != null)); }
        }

        /// <summary>
        /// Thực thi
        /// </summary>
        private static void PasteImageObjectExcute()
        {
            var _data = (Clipboard.GetDataObject() as DataObject).GetImage();
            if (_data is BitmapSource bitmap)
            {

            }
        }

        #endregion

        #region FormatPainterCommand

        private static RelayCommand _formatPainterCommandCommand;
        /// <summary>
        /// Sao chép đối tượng vào Clipboard
        /// </summary>
        public static RelayCommand FormatPainterCommand
        {
            get { return _formatPainterCommandCommand ?? (_formatPainterCommandCommand = new RelayCommand(p => FormatPainterExcute(), o => (Application.Current as IAppGlobal)?.SelectedElements.Count == 1)); }
        }

        /// <summary>
        /// Căn theo slide hay theo đối tượng
        /// </summary>
        public static bool IsAlignToSlide { get => _isAlignToSlide; set => _isAlignToSlide = value; }

        /// <summary>
        /// Thực thi
        /// </summary>
        private static void FormatPainterExcute()
        {
            (Application.Current as IAppGlobal).PainterValue = (Application.Current as IAppGlobal).SelectedItem.GetPainter();
        }

        #endregion

        #endregion

        public class ProxyClass : RootViewModel
        {
            public bool IsAlignToSlide
            {
                get => _isAlignToSlide;
                set
                {
                    _isAlignToSlide = value;
                    OnPropertyChanged("IsAlignToSlide");
                }
            }
        }

        private static ProxyClass _proxy;
        /// <summary>
        /// Đối tượng tác động tạm
        /// </summary>
        public static ProxyClass Proxy
        {
            get { return _proxy ?? (_proxy = new ProxyClass()); }
        }

        /// <summary>
        ///Get ressource
        /// </summary>
        /// <param name="_key"></param>
        /// <returns></returns>
        public static string GetResource(string _key)
        {
            string key = (Application.Current.TryFindResource(_key)) as string;
            return key;
        }
    }
}

﻿
using INV.Elearning.Core.Helper;
using System;
using System.Windows;
using System.Windows.Input;

namespace INV.Elearning.Core.AppCommands
{
    public class DocumentCommands
    {
        private RelayCommand _showPlayerConfigCommand;
        /// <summary>
        /// Hiển thị hiệu ứng
        /// </summary>
        public RelayCommand ShowPlayerConfigCommand
        {
            get { return _showPlayerConfigCommand ?? (_showPlayerConfigCommand = new RelayCommand(p => ShowPlayerConfigExcute())); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        private void ShowPlayerConfigExcute()
        {

        }

        #region Document Commands

        #region SaveCommand
        /// <summary>
        /// Lệnh lưu trữ dữ liệu
        /// </summary>
        public static ICommand SaveCommand
        {
            get { return (Application.Current as IAppGlobal)?.SaveDocumentCommand; }
        }

        #endregion

        #region OpenCommand
        /// <summary>
        /// Lệnh lưu trữ dữ liệu
        /// </summary>
        public static ICommand OpenCommand
        {
            get { return (Application.Current as IAppGlobal)?.OpenDocumentCommand; }
        }

        #endregion

        #region ExportCommand
        /// <summary>
        /// Xuất bản dữ liệu
        /// </summary>
        public static ICommand ExportCommand
        {
            get { return (Application.Current as IAppGlobal)?.ExportCommand; }
        }

        #endregion

        #region PreviewCommand
        static ICommand _previewCommand;
        /// <summary>
        /// Kiểm tra xem trước
        /// </summary>
        public static ICommand PreviewCommand
        {
            get { return _previewCommand ?? (_previewCommand = new RelayCommand(p => PreviewExecute(p))); }
        }

        /// <summary>
        /// Thực thi xem trước
        /// </summary>
        /// <param name="p"></param>
        private static void PreviewExecute(object p)
        {
            (Application.Current as IAppGlobal).ExportPreviewExecute(p);
        }

        #endregion

        #region CopyCommand
        static ICommand _copyCommand;
        /// <summary>
        /// Sao chép đối tượng, Có thể Slide, Layout, Đối tượng
        /// </summary>
        public static ICommand CopyCommand
        {
            get { return _copyCommand ?? (_copyCommand = new RelayCommand(p => CopyExecute(p))); }
        }

        /// <summary>
        /// Thực thi xem trước
        /// </summary>
        /// <param name="p"></param>
        private static void CopyExecute(object p)
        {
            
        }
        #endregion

        #endregion
    }
}

﻿using INV.Elearning.Controls.Helpers;
using INV.Elearning.Core.SharedUIs;
using INV.Elearning.Core.View;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.AppCommands
{
    public class LayoutCommands
    {
        #region Editor Layout Commands

        #region CopyCommand
        private static RelayCommand _copyCommand;
        /// <summary>
        /// Lệnh điều khiển sao chép layout
        /// </summary>
        public static RelayCommand CopyCommand
        {
            get { return _copyCommand ?? (_copyCommand = new RelayCommand(p => CopyExecute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void CopyExecute(object p)
        {
            if (p is LayoutBase layout && layout.LayoutRule.CanCopy)
            {
                layout.RefreshData(); //Đưa dữ liệu vào bộ nhớ tạm
                Clipboard.Clear();
                Clipboard.SetData(Helper.ClipboardHelpers.CLIPBOARD_LAYOUT_KEY, layout.Data.Clone());
            }
        }

        #endregion

        #region NewCommand
        private static RelayCommand _newCommand;
        /// <summary>
        /// Lệnh điều khiển tạo mới một layout
        /// </summary>
        public static RelayCommand NewCommand
        {
            get { return _newCommand ?? (_newCommand = new RelayCommand(p => NewExecute(true))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="isNewLayoutSelected">Trỏ dữ liệu vào layout mới</param>
        public static void NewExecute(bool isNewLayoutSelected)
        {
            LayoutBase _layout = new LayoutBase();
            (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Add(_layout);
            _layout.IsSelected = isNewLayoutSelected;
        }
        #endregion

        #region PasteCommand
        private static RelayCommand _pasteCommand;
        /// <summary>
        /// Lệnh điều khiển dán đối tượng từ bộ nhớ đệm
        /// </summary>
        public static RelayCommand PasteCommand
        {
            get { return _pasteCommand ?? (_pasteCommand = new RelayCommand(p => PasteExecute(), o => Clipboard.ContainsData(Helper.ClipboardHelpers.CLIPBOARD_LAYOUT_KEY))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        private static void PasteExecute()
        {
            var _data = Clipboard.GetData(Helper.ClipboardHelpers.CLIPBOARD_LAYOUT_KEY) as Model.PageLayer;
            if (_data != null)
            {
                Helper.Global.IsPasting = true;
                _data.Name = _data.Name + " " + (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Count;
                LayoutBase _layout = Helper.LayoutHelper.LoadDataForSlideLayout(_data) as LayoutBase;
                if (_layout != null)
                {
                    (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Add(_layout);
                }
                Helper.Global.IsPasting = false;
            }
        }
        #endregion

        #region ConfigCommand
        private static RelayCommand _configCommand;
        /// <summary>
        /// Lệnh điều khiển mở cửa sổ cấu hình layout
        /// </summary>
        public static RelayCommand ConfigCommand
        {
            get { return _configCommand ?? (_configCommand = new RelayCommand(p => ConfigExecute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        private static void ConfigExecute(object p)
        {
            if (p is LayoutBase layout)
            {
                if (!layout.IsMainLayout)
                {
                    SlideLayerProperties _window = new SlideLayerProperties();
                    _window.Owner = Application.Current?.MainWindow;
                    _window.ShowDialog();
                }
                else
                    SlideCommands.ConfigCommand?.Execute(null);
            }
        }
        #endregion

        #region DuplicateCommand
        private static RelayCommand _duplicateCommand;
        /// <summary>
        /// Lệnh điều khiển nhân bản đối tượng
        /// </summary>
        public static RelayCommand DuplicateCommand
        {
            get { return _duplicateCommand ?? (_duplicateCommand = new RelayCommand(p => DuplicateExecute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void DuplicateExecute(object p)
        {
            Helper.Global.IsPasting = true;
            if (p is LayoutBase layout && layout.LayoutRule.CanCopy)
            {
                layout.RefreshData();
                var _data = layout.Data.Clone() as Model.PageLayer;
                _data.Name = _data.Name + " " + (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Count;
                LayoutBase _layout = Helper.LayoutHelper.LoadDataForSlideLayout(_data) as LayoutBase;
                if (_layout != null)
                    (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Add(_layout);
            }
            else
            {
                var _selectedLayout = (Application.Current as Helper.IAppGlobal).SelectedSlide?.SelectedLayout;
                if (_selectedLayout != null && _selectedLayout.LayoutRule.CanCopy)
                {
                    _selectedLayout.RefreshData();
                    var _data = _selectedLayout.Data.Clone() as Model.PageLayer;
                    _data.Name = _data.Name + " " + (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Count;
                    LayoutBase _layout = Helper.LayoutHelper.LoadDataForSlideLayout(_data) as LayoutBase;
                    if (_layout != null)
                        (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Add(_layout);
                }
            }
            Helper.Global.IsPasting = false;
        }
        #endregion

        #region DeleteCommand
        private static RelayCommand _deleteCommand;
        /// <summary>
        /// Lệnh điều khiển xóa layout
        /// </summary>
        public static RelayCommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(p => DeleteExecute(p), o => DeleteCanExecute(o))); }
        }

        private static bool DeleteCanExecute(object o)
        {
            if (o is LayoutBase layout)
            {
                return (Application.Current as Helper.IAppGlobal).SelectedSlide?.MainLayout != layout;
            }

            return (Application.Current as Helper.IAppGlobal).SelectedSlide?.MainLayout != null && (Application.Current as Helper.IAppGlobal).SelectedSlide?.MainLayout != (Application.Current as Helper.IAppGlobal).SelectedSlide?.SelectedLayout;
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void DeleteExecute(object p)
        {
            if (p is LayoutBase layout)
            {
                (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Remove(layout);
            }
            else
            {
                (Application.Current as Helper.IAppGlobal).SelectedSlide.Layouts.Remove((Application.Current as Helper.IAppGlobal).SelectedSlide.SelectedLayout);
            }
        }

        #endregion

        #region RenameCommand
        private static RelayCommand _renameCommand;
        /// <summary>
        /// Lệnh điều khiển đổi tên layout
        /// </summary>
        public static RelayCommand RenameCommand
        {
            get { return _renameCommand ?? (_renameCommand = new RelayCommand(p => RenameExecute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void RenameExecute(object p)
        {
            if (p is ListViewItem listViewItem)
            {
                ContentPresenter _content = FindVisualChild<ContentPresenter>(listViewItem);

                var _textBox = _content.ContentTemplate.FindName("PART_TextBox", _content) as TextBox;
                if (_textBox != null)
                {
                    _textBox.IsReadOnly = false;
                    _textBox.SelectAll();
                    _textBox.Focus();
                }
            }
        }

        private static childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        #endregion

        #endregion
    }

    public class MainLayoutConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            ObservableCollection<LayoutBase> _layouts = new ObservableCollection<LayoutBase>();
            _layouts.Add(value as LayoutBase);
            return _layouts;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}

﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.SharedUIs;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace INV.Elearning.Core.AppCommands
{
    public class SlideCommands
    {
        public static string CLIPBOARD_SLIDE_KEY = "AVINA_SLIDE";
        #region Editor Slide Commands

        #region CopyCommand
        private static RelayCommand _copyCommand;
        /// <summary>
        /// Lệnh điều khiển sao chép slide
        /// </summary>
        public static RelayCommand CopyCommand
        {
            get { return _copyCommand ?? (_copyCommand = new RelayCommand(p => CopyExecute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void CopyExecute(object p)
        {
            var _slideData = GetSelectedData();
            if (_slideData != null)
            {
                Clipboard.Clear();
                Clipboard.SetData(CLIPBOARD_SLIDE_KEY, _slideData);
            }
        }

        /// <summary>
        /// Lay du lieu dang duoc lua chon
        /// </summary>
        /// <returns></returns>
        private static List<Model.PageElementBase> GetSelectedData()
        {
            if ((Application.Current as IAppGlobal).SelectedSlides.Count > 0)
            {
                List<Model.PageElementBase> _slideData = new List<Model.PageElementBase>();
                foreach (var slide in (Application.Current as IAppGlobal).SelectedSlides)
                {
                    slide.RefreshData();
                    _slideData.Add(slide.Data.Clone() as Model.PageElementBase);
                }
                return _slideData;
            }
            return null;
        }

        #endregion

        #region CutCommand
        private static RelayCommand _cutCommand;
        /// <summary>
        /// Lệnh điều khiển cắt slide
        /// </summary>
        public static RelayCommand CutCommand
        {
            get { return _cutCommand ?? (_cutCommand = new RelayCommand(p => CutExecute(p), o => DeleteCanExecute(o))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void CutExecute(object p)
        {
            var _slideData = GetSelectedData();
            if (_slideData != null)
            {
                Clipboard.SetData(CLIPBOARD_SLIDE_KEY, _slideData);
                DeleteExecute(null);
            }
        }
        #endregion

        #region PasteCommand
        private static RelayCommand _pasteCommand;
        /// <summary>
        /// Lệnh điều khiển dán đối tượng từ bộ nhớ đệm
        /// </summary>
        public static RelayCommand PasteCommand
        {
            get { return _pasteCommand ?? (_pasteCommand = new RelayCommand(p => PasteExecute(), o => Clipboard.ContainsData(CLIPBOARD_SLIDE_KEY))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        private static void PasteExecute()
        {
            var _data = Clipboard.GetData(CLIPBOARD_SLIDE_KEY) as List<Model.PageElementBase>;
            PasteFromData(_data);
        }

        /// <summary>
        /// Danh tu danh sach du lieu
        /// </summary>
        /// <param name="data"></param>
        private static void PasteFromData(List<Model.PageElementBase> data)
        {
            if (data != null && data.Count > 0)
            {
                Global.StartGrouping();
                SlideHelper.UnSlectedAll();
                Global.IsPasting = true;
                int _index = (Application.Current as Helper.IAppGlobal).DocumentControl.Slides.IndexOf((Application.Current as Helper.IAppGlobal).SelectedSlide);
                foreach (var item in data)
                {
                    SlideBase _slide = Helper.LayoutHelper.LoadDataForSlideLayout(item) as SlideBase;
                    if (_slide != null)
                    {
                        _slide.ID = Helper.ObjectElementsHelper.RandomString(13);
                        (Application.Current as Helper.IAppGlobal).DocumentControl.Slides.Insert(++_index, _slide);
                        _slide.IsSelected = true;

                    }
                }
                Global.IsPasting = false;
                Global.StopGrouping();
            }
        }
        #endregion

        #region DuplicateCommand
        private static RelayCommand _duplicateCommand;
        /// <summary>
        /// Lệnh điều khiển nhân bản đối tượng
        /// </summary>
        public static RelayCommand DuplicateCommand
        {
            get { return _duplicateCommand ?? (_duplicateCommand = new RelayCommand(p => DuplicateExecute(p), o => (Application.Current as IAppGlobal).SlideViewMode == SlideViewMode.Normal)); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void DuplicateExecute(object p)
        {
            PasteFromData(GetSelectedData());
        }
        #endregion

        #region DeleteCommand
        private static RelayCommand _deleteCommand;
        /// <summary>
        /// Lệnh điều khiển xóa layout
        /// </summary>
        public static RelayCommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(p => DeleteExecute(p), o => DeleteCanExecute(o))); }
        }

        private static bool DeleteCanExecute(object o)
        {
            return (Application.Current as Helper.IAppGlobal).SelectedSlides.Count > 0 && (Application.Current as Helper.IAppGlobal).DocumentControl.Slides.Count > (Application.Current as Helper.IAppGlobal).SelectedSlides.Count;
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void DeleteExecute(object p)
        {
            Global.StartGrouping();
            var _selectedSlides = (Application.Current as Helper.IAppGlobal).SelectedSlides;
            int _index = 0;
            ObjectElementsHelper.UnSelectedAll();
            while (_selectedSlides.Count > 0)
            {
                _index = (Application.Current as Helper.IAppGlobal).DocumentControl.Slides.IndexOf(_selectedSlides[0]);
                (Application.Current as Helper.IAppGlobal).DocumentControl.Slides.Remove(_selectedSlides[0]);
            }

            _index = Math.Max(0, _index - 1);
            (Application.Current as IAppGlobal).DocumentControl.Slides[_index].IsSelected = true;
            Global.StopGrouping();
        }

        #endregion

        #region ConfigCommand
        private static RelayCommand _configCommand;
        /// <summary>
        /// Lệnh điều khiển mở cửa sổ cấu hình layout
        /// </summary>
        public static RelayCommand ConfigCommand
        {
            get { return _configCommand ?? (_configCommand = new RelayCommand(p => ConfigExecute())); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        private static void ConfigExecute()
        {
            SlidePropertiesWindow _window = new SlidePropertiesWindow();
            _window.Owner = Application.Current?.MainWindow;
            _window.ShowDialog();
        }
        #endregion

        #region RenameCommand
        private static RelayCommand _renameCommand;
        /// <summary>
        /// Lệnh điều khiển đổi tên layout
        /// </summary>
        public static RelayCommand RenameCommand
        {
            get { return _renameCommand ?? (_renameCommand = new RelayCommand(p => RenameExecute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void RenameExecute(object p)
        {
            if (p is ListViewItem item)
            {
                System.Windows.Controls.TextBox _txtName = FindVisualChild<System.Windows.Controls.TextBox>(item);
                if (_txtName != null)
                {
                    _txtName.Visibility = Visibility.Visible;
                    _txtName.LostFocus += TextName_LostFocus;
                    _txtName.KeyDown += TextName_KeyDown;
                    _txtName.Focus();
                    _txtName.SelectAll();
                }
            }
        }

        /// <summary>
        /// Su kien nha phim
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void TextName_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Windows.Controls.TextBox _owner = sender as System.Windows.Controls.TextBox;
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                _owner.LostFocus -= TextName_LostFocus;
                _owner.KeyDown -= TextName_KeyDown;
                _owner.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Su kien mat truy cap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void TextName_LostFocus(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.TextBox _owner = sender as System.Windows.Controls.TextBox;
            _owner.LostFocus -= TextName_LostFocus;
            _owner.KeyDown -= TextName_KeyDown;
            _owner.Visibility = Visibility.Collapsed;
        }

        private static childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        #endregion

        #endregion

        #region NewCommand
        private static RelayCommand _newCommand;
        /// <summary>
        /// Lệnh điều khiển tạo mới một Slide
        /// </summary>
        public static RelayCommand NewCommand
        {
            get { return _newCommand ?? (_newCommand = new RelayCommand(p => NewExecute())); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        private static void NewExecute()
        {
            NormalSlide _slide = new NormalSlide();
            int _currentIndex = (Application.Current as Helper.IAppGlobal).DocumentControl.Slides.IndexOf((Application.Current as Helper.IAppGlobal).SelectedSlide);
            (Application.Current as Helper.IAppGlobal).DocumentControl.Slides.Insert(_currentIndex + 1, _slide);
            SlideHelper.UnSlectedAll();
            _slide.IsSelected = true;
        }
        #endregion
    }
}

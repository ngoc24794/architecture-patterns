﻿

namespace INV.Elearning.Core
{
    using INV.Elearning.Core.View;

    /// <summary>
    /// Interface chính, cung cấp các thuộc tính, phương thức bắt buộc đối với một module muốn ghép nối vào hệ thống chính
    /// </summary>
    public interface IModule : IObjectDispose
    {
        /// <summary>
        /// Tải lại đối tượng hiển thị từ nội dung.</br>
        /// Các đối tượng sẽ được hiển thị trên tài liệu, hoặc trang.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        ObjectElement Load(object data);      
    }

    /// <summary>
    /// Các loại mô dun
    /// </summary>
    public enum ModuleType
    {
        /// <summary>
        /// Mô đun của công ty phát triển
        /// </summary>
        INVModule,
        /// <summary>
        /// Mô đun của bên thứ 3, đã xác nhận thông tin
        /// </summary>
        ConfirmedModule,
        /// <summary>
        /// Mô đun của bên thứ 3, chưa xác nhận thông tin
        /// </summary>
        UnConfirmedModule
    }

    /// <summary>
    /// Interface hủy dữ liệu
    /// </summary>
    public interface IObjectDispose
    {
        /// <summary>
        /// Khởi tạo các hiển thị
        /// </summary>
        void Initialize();

        /// <summary>
        /// Hủy các tài nguyên chiếm giữ
        /// </summary>
        void Dispose();
    }

    /// <summary>
    /// Cac kieu mo dun
    /// </summary>
    public enum ModuleLevel
    {
        /// <summary>
        /// Danh cho doi tuong
        /// </summary>
        Element,
        /// <summary>
        /// Danh cho layout
        /// </summary>
        Layout,
        /// <summary>
        /// Danh cho bai soan
        /// </summary>
        Slide
    }
}

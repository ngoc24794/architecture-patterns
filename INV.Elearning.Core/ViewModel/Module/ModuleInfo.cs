﻿using System;

namespace INV.Elearning.Core
{
    /// <summary>
    /// Thông tin mô đun kết nối vào hệ thống
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ModuleInfo : Attribute
    {
        #region Private Fields

        /// <summary>
        /// Tên người tạo
        /// </summary>
        private string _publisher = string.Empty;

        /// <summary>
        /// Tên mô đun
        /// </summary>
        private string _name = string.Empty;

        /// <summary>
        /// Mô tả mô đun
        /// </summary>
        private string _description = string.Empty;

        /// <summary>
        /// Phiên bản mô đun
        /// </summary>
        private string _version = string.Empty;

        /// <summary>
        /// Khóa bảo mật, đăng ký để được đưa vào chương trình chính
        /// </summary>
        private string _keyProtective = string.Empty;

        #endregion

        #region Properties

        /// <summary>
        /// Lấy thông tin tên người tạo
        /// </summary>
        public string Publisher
        {
            get
            {
                return _publisher;
            }
        }

        /// <summary>
        /// Lấy thông tin tên mô đun
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
        }

        /// <summary>
        /// Lấy thông tin mô tả mô đun
        /// </summary>
        public string Description
        {
            get
            {
                return _description;
            }
        }

        /// <summary>
        /// Lấy thông tin phiên bản mô đun
        /// </summary>
        public string Version
        {
            get
            {
                return _version;
            }
        }

        /// <summary>
        /// Lấy thông tin khóa bảo mật
        /// </summary>
        public string KeyProtective
        {
            get
            {
                return _keyProtective;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public ModuleInfo()
        {

        }

        /// <summary>
        /// Hàm khởi tạo nhiều tham số
        /// </summary>
        /// <param name="name">Tên mô đun</param>
        /// <param name="publisher">Tên nhà sản xuất</param>
        /// <param name="description">Mô tả</param>
        /// <param name="version">Phiên bản</param>
        /// <param name="key">Khóa bảo mật</param>
        public ModuleInfo(string name, string publisher = "", string description = "", string version = "", string key = "")
        {
            _publisher = publisher;
            _name = name;
            _description = description;
            _keyProtective = key;
        }

        #endregion
    }
}

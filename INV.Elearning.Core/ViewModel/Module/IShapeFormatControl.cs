﻿
namespace INV.Elearning.Core
{
    using System;
    using System.Windows;

    /// <summary>
    /// Giao diện thông tin các đối tượng định dạng ảnh
    /// </summary>
    public interface IShapeFormatControl
    {
        /// <summary>
        /// Kiểu dữ liệu của đối tượng được hỗ trợ
        /// </summary>
        Type TargetType { get; }

        /// <summary>
        /// Đối tượng chứa quản trị nội dung
        /// </summary>
        UIElement Control { get; }

        /// <summary>
        /// Nội dung của tiêu đề
        /// </summary>
        string Header { get; }

        /// <summary>
        /// Biểu tượng của nội dung
        /// </summary>
        object Icon { get; }

        /// <summary>
        /// Thuộc tính lựa chọn
        /// </summary>
        bool IsSelected { set; get; }

        /// <summary>
        /// Id của module
        /// </summary>
        string ID { get; }
    }
}

﻿namespace INV.Elearning.Core.ViewModel.Module
{
    public abstract class AvinaLanguageBase
    {
        /// <summary>
        /// Thông tin đường dẫn hình đại diện cho ngôn ngữ
        /// </summary>
        public string IconUri { get; protected set; }
        /// <summary>
        /// Mô tả nội dung
        /// </summary>
        public string Description { get; protected set; }
        /// <summary>
        /// Đường dẫn ngôn ngữ
        /// </summary>
        public string LanguageUri { get; protected set; }
        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        public string Title { get; protected set; }
        /// <summary>
        /// Mã ngôn ngữ
        /// </summary>
        public string LanguageCode { get; protected set; }
    }
}

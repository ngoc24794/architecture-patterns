﻿

namespace INV.Elearning.Core
{
    using System.Windows;

    /// <summary>
    /// Đối tượng vùng hiển thị
    /// </summary>
    public class PanelItem
    {
        /// <summary>
        /// Đối tượng điều khiển
        /// </summary>
        public FrameworkElement Panel
        {
            set;
            get;
        }

        /// <summary>
        /// Vị trí trên phần mềm
        /// </summary>
        public PanelLocation Location
        {
            set;
            get;
        }
    }

    /// <summary>
    /// Vị trí hiển thị
    /// </summary>
    public enum PanelLocation
    {
        /// <summary>
        /// Vùng trái
        /// </summary>
        Left,
        /// <summary>
        /// Vùng trên
        /// </summary>
        Top,
        /// <summary>
        /// Vùng phải
        /// </summary>
        Right,
        /// <summary>
        /// Vùng dưới
        /// </summary>
        Bottom,
        /// <summary>
        /// Vùng giữa
        /// </summary>
        Center
    }
}

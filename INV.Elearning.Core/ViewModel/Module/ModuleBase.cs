﻿
namespace INV.Elearning.Core.ViewModel
{
    using INV.Elearning.Core.View;
    using INV.Elearning.Controls;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Lớp trừu tượng mô tả thông tin của một Mô đun được phép sử dụng trong hệ thống
    /// </summary>
    public abstract class ModuleBase : IModule
    {
        /// <summary>
        /// Lấy thông tin Group chứa các Tab dữ liệu định dạng cho đối tượng
        /// </summary>
        public abstract RibbonContextualTabGroup ContextualGroup { get; }

        /// <summary>
        /// Lấy thông tin kiểu dữ liệu mô đun quản lý
        /// </summary>
        public abstract Type DataType { get; }

        /// <summary>
        /// Các danh sách của đối tượng con
        /// </summary>
        public List<Type> ChildTypes { get => _childTypes ?? (_childTypes = new List<System.Type>()); set => _childTypes = value; }

        /// <summary>
        /// Lấy thông tin kiểu đối tượng mô đun quản lý
        /// </summary>
        public abstract Type ObjectType { get; }

        /// <summary>
        /// Lấy thông tin mô đun
        /// </summary>
        public abstract ModuleInfo ModuleInfo { get; }

        /// <summary>
        /// Lấy danh sách các vùng điều khiển khác. Ví dụ cho dự án câu hỏi tương tác...
        /// </summary>
        public abstract IEnumerable<PanelItem> Panels { get; }

        /// <summary>
        /// Lấy thông tin thẻ chứa các điều khiển định dạng đối tượng <typeparamref name="DataType"/>
        /// </summary>
        public abstract IEnumerable<RibbonTabItem> RibbonTabItems { get; }

        /// <summary>
        /// Lấy danh sách các menu ngữ cảnh cho đối tượng
        /// </summary>
        public abstract IEnumerable<System.Windows.Controls.MenuItem> MenuItems { get; }

        private List<IShapeFormatControl> _formatElements = null;
        /// <summary>
        /// Danh sách các phần tử
        /// </summary>
        public List<IShapeFormatControl> FormatControls
        {
            get => _formatElements ?? (_formatElements = new List<IShapeFormatControl>());
        }

        /// <summary>
        /// Lấy thông tin Group chứa các Tab dữ liệu định dạng cho đối tượng
        /// </summary>
        public abstract string InsertGroupBoxName { get; }

        /// <summary>
        /// Lấy danh sách các nút điều khiển chèn các đối tượng
        /// </summary>
        public abstract IEnumerable<System.Windows.FrameworkElement> InsertButtons { get; }

        /// <summary>
        /// Hủy các tài nguyên chiếm giữ
        /// </summary>
        public abstract void Dispose();

        /// <summary>
        /// Khởi tạo các hiển thị
        /// </summary>
        public abstract void Initialize();

        /// <summary>
        /// Tải lại đối tượng hiển thị từ nội dung.</br>
        /// Các đối tượng sẽ được hiển thị trên tài liệu, hoặc trang.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract ObjectElement Load(object data);

        /// <summary>
        /// Lấy đường dẫn chứa thư mục ngôn ngữ
        /// </summary>
        public abstract string LanguagePackageUri { get; }

        /// <summary>
        /// Kiểu mô đun
        /// </summary>
        private ModuleType _type = ModuleType.UnConfirmedModule;
        private List<Type> _childTypes;

        /// <summary>
        /// Lấy hoặc cài đặt kiểu mô đun
        /// </summary>
        public ModuleType Type
        {
            get
            {
                return _type;
            }

            internal set
            {
                _type = value;
            }
        }

        /// <summary>
        /// Lay hoac cai dat thuoc tinh cap do cho module
        /// </summary>
        public ModuleLevel Level { get; set; }

        private Type _containerType = typeof(NormalSlide);
        private string[] _extensionFiles;

        /// <summary>
        /// Kiểu dữ liệu có thể chứa đối tượng.
        /// Kiểu dữ liệu bắt buộc phải dẫn xuất từ Normal Slide
        /// </summary>
        public Type ContainerType
        {
            get { return _containerType; }
            set { _containerType = value; }
        }


        /// <summary>
        /// Tai lai du lieu, danh cho Layout, Slide
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual object LoadData(object data) { return null; }

        /// <summary>
        /// Danh sách đuôi tập tin hỗ trợ
        /// </summary>
        public string[] ExtensionFiles { get => _extensionFiles ?? (_extensionFiles = new string[] { }); protected set => _extensionFiles = value; }

        /// <summary>
        /// Dựng dữ liệu từ tập tin
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public virtual ObjectElement ObjectFromFile(string fileName) { return null; }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  TriggerViewModelBase.cs
**
** Description: Lớp cơ sở cho các viewmodel xử lí dữ liệu tạo từ lớp TriggerModel
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Model;

namespace INV.Elearning.Core.ViewModel
{
    /// <summary>
    /// Lớp cơ sở cho các viewmodel xử lí dữ liệu tạo từ lớp TriggerModel
    /// </summary>
    public abstract class TriggerViewModelBase : RootViewModel
    {
        /// <summary>
        /// Dữ liệu của một trigger
        /// </summary>
        public TriggerModelBase Trigger { get; set; }
        /// <summary>
        /// Chỉ số xác định tính duy nhất của trigger
        /// </summary>
        public string ID { get => Trigger?.ID; }
    }
}

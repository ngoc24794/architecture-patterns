﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  VariableViewModelBase.cs
**
** Description: Lớp cơ sở cho các viewmodel có sở hữu một VariableModelBase
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 2/5/2018
===========================================================*/

using INV.Elearning.Core.Model;

namespace INV.Elearning.Core.ViewModel
{
    /// <summary>
    /// Lớp cơ sở cho các viewmodel có sở hữu một VariableModelBase
    /// </summary>
    public abstract class VariableViewModelBase : RootViewModel
    {
        /// <summary>
        /// Dữ liệu của một biến
        /// </summary>
        public VariableModelBase Variable { get; set; }
        /// <summary>
        /// Chỉ số xác định tính duy nhất của biến
        /// </summary>
        public string ID { get => Variable?.ID; }
    }
}

﻿using INV.Elearning.Core.Helper;
using System.Windows;

namespace INV.Elearning.Core.ViewModel.UndoRedo.Steps
{
    public class DependencyPropertyStep : StepBase
    {
        public DependencyProperty Property { get; set; }

        public DependencyPropertyStep(DependencyObject target, DependencyProperty property, object newValue, object oldValue)
        {
            this.Source = target;
            this.Property = property;
            this.NewValue = newValue;
            this.OldValue = oldValue;
        }

        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as DependencyObject).SetValue(Property, OldValue);
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as DependencyObject).SetValue(Property, NewValue);
            Global.EndInit();
        }
    }
}

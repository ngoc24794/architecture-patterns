﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Core.ViewModel.UndoRedo.Steps
{
    public class AddEffectStep : StepBase
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public AddEffectStep(FrameEffectBase value, Collection<FrameEffectBase> collection)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as Collection<FrameEffectBase>).Remove(NewValue as FrameEffectBase);
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<FrameEffectBase>).Add(OldValue as FrameEffectBase);
            Global.EndInit();
        }
    }

    public class RemoveEffectStep : StepBase
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public RemoveEffectStep(FrameEffectBase value, Collection<FrameEffectBase> collection)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as Collection<FrameEffectBase>).Add(NewValue as FrameEffectBase);
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<FrameEffectBase>).Remove(OldValue as FrameEffectBase);
            Global.EndInit();
        }
    }
}

﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System.Collections.Generic;
using System.Linq;

namespace INV.Elearning.Core.ViewModel.UndoRedo.Steps
{
    public class GroupElementsStep : StepBase
    {
        public GroupElementsStep(GroupContainer groupContainer)
        {
            Source = groupContainer;
            NewValue = groupContainer.Elements.ToList();
        }

        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as GroupContainer).UnGroup();
            Global.EndInit();
        }

        public override void RedoExcute()
        {
            Global.BeginInit();
            if (NewValue is List<ObjectElement> elements)
            {
                elements.ForEach(x => (Source as GroupContainer).Elements.Add(x));
            }
            Global.EndInit();
        }
    }
}

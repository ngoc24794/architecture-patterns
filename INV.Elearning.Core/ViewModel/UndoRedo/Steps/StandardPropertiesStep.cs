﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Core.ViewModel.UndoRedo.Steps
{
    public class StandardPropertiesStep : StepBase
    {
        /// <summary>
        /// Hàm khởi tạo
        /// </summary>
        /// <param name="newValue"></param>
        /// <param name="oldValue"></param>
        /// <param name="source"></param>
        public StandardPropertiesStep(StandardProperties newValue, StandardProperties oldValue, ObjectElement source)
        {
            this.NewValue = newValue;
            this.OldValue = oldValue;
            this.Source = source;
        }

        /// <summary>
        /// Thực thi hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();

            (Source as ObjectElement).Angle = (OldValue as StandardProperties).Angle;
            (Source as ObjectElement).Left = (OldValue as StandardProperties).Left;
            (Source as ObjectElement).Top = (OldValue as StandardProperties).Top;
            (Source as ObjectElement).Width = (OldValue as StandardProperties).Width;
            (Source as ObjectElement).Height = (OldValue as StandardProperties).Height;
            (Source as ObjectElement).IsScaleX = (OldValue as StandardProperties).IsScaleX;
            (Source as ObjectElement).IsScaleY = (OldValue as StandardProperties).IsScaleY;

            Global.EndInit();
        }

        /// <summary>
        /// Thực thi hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();

            (Source as ObjectElement).Angle = (NewValue as StandardProperties).Angle;
            (Source as ObjectElement).Left = (NewValue as StandardProperties).Left;
            (Source as ObjectElement).Top = (NewValue as StandardProperties).Top;
            (Source as ObjectElement).Width = (NewValue as StandardProperties).Width;
            (Source as ObjectElement).Height = (NewValue as StandardProperties).Height;
            (Source as ObjectElement).IsScaleX = (NewValue as StandardProperties).IsScaleX;
            (Source as ObjectElement).IsScaleY = (NewValue as StandardProperties).IsScaleY;

            Global.EndInit();
        }
    }

    /// <summary>
    /// Lớp lưu trữ các thuộc tính căn bản của đối tượng
    /// </summary>
    public class StandardProperties
    {
        /// <summary>
        /// Góc quay
        /// </summary>
        public double Angle { get; set; }
        /// <summary>
        /// Căn trái
        /// </summary>
        public double Left { get; set; }
        /// <summary>
        /// Căn trên
        /// </summary>
        public double Top { get; set; }
        /// <summary>
        /// Chiều dài
        /// </summary>
        public double Width { get; set; }
        /// <summary>
        /// Chiều rộng
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// Cờ dảo chiều X
        /// </summary>
        public bool IsScaleX { get; set; }

        /// <summary>
        /// Cờ đảo chiều Y
        /// </summary>
        public bool IsScaleY { get; set; }
    }
}

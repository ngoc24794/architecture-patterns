﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace INV.Elearning.Core.ViewModel.UndoRedo.Steps
{
    public class AddItemToLayoutStep : StepBase
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public AddItemToLayoutStep(ObjectElement value, Collection<ObjectElement> collection)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as Collection<ObjectElement>).Remove(NewValue as ObjectElement);
            (NewValue as ObjectElement).IsSelected = false;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            ObjectElementsHelper.UnSelectedAll();
            (Source as Collection<ObjectElement>).Add(OldValue as ObjectElement);
            (NewValue as ObjectElement).IsSelected = true;
            Global.EndInit();
        }
    }

    public class RemoveItemToLayoutStep : StepBase
    {
        private int _oldIndex;
        /// <summary>
        /// Chỉ số  index trong danh sách
        /// </summary>
        public int OldIndex
        {
            get { return _oldIndex; }
            set { _oldIndex = value; }
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public RemoveItemToLayoutStep(ObjectElement value, Collection<ObjectElement> collection, int index)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
            this.OldIndex = index;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            ObjectElementsHelper.UnSelectedAll();
            (Source as Collection<ObjectElement>).Insert(this.OldIndex, NewValue as ObjectElement);
            (NewValue as ObjectElement).IsSelected = true;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<ObjectElement>).Remove(OldValue as ObjectElement);
            Global.EndInit();
        }
    }

    public class AddLayoutToSlideStep : StepBase
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public AddLayoutToSlideStep(LayoutBase value, Collection<LayoutBase> collection)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as Collection<LayoutBase>).Remove(NewValue as LayoutBase);
            (NewValue as LayoutBase).IsSelected = false;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<LayoutBase>).Add(OldValue as LayoutBase);
            Global.EndInit();
        }
    }

    public class RemoveLayoutFromSlideStep : StepBase
    {

        private int _oldIndex;
        /// <summary>
        /// Chỉ số  index trong danh sách
        /// </summary>
        public int OldIndex
        {
            get { return _oldIndex; }
            set { _oldIndex = value; }
        }


        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public RemoveLayoutFromSlideStep(LayoutBase value, Collection<LayoutBase> collection, int index)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
            this.OldIndex = index;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as Collection<LayoutBase>).Insert(OldIndex, NewValue as LayoutBase);
            (NewValue as LayoutBase).IsSelected = true;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<LayoutBase>).Remove(OldValue as LayoutBase);
            Global.EndInit();
        }
    }

    public class AddSlideToDocumentStep : StepBase
    {

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public AddSlideToDocumentStep(SlideBase value, Collection<SlideBase> collection)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            int _curentIndex = (Source as Collection<SlideBase>).IndexOf(NewValue as SlideBase);
            (Source as Collection<SlideBase>).Remove(NewValue as SlideBase);
            (NewValue as SlideBase).IsSelected = false;
            _curentIndex = Math.Max(0, Math.Min(_curentIndex - 1, (Source as Collection<SlideBase>).Count - 1));
            SlideHelper.UnSlectedAll();
            (Source as Collection<SlideBase>)[_curentIndex].IsSelected = true;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<SlideBase>).Add(OldValue as SlideBase);
            SlideHelper.UnSlectedAll();
            (OldValue as SlideBase).IsSelected = true;
            Global.EndInit();
        }
    }

    public class RemoveSlideFromDocumentStep : StepBase
    {

        private int _oldIndex;
        /// <summary>
        /// Chỉ số  index trong danh sách
        /// </summary>
        public int OldIndex
        {
            get { return _oldIndex; }
            set { _oldIndex = value; }
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public RemoveSlideFromDocumentStep(SlideBase value, Collection<SlideBase> collection, int index)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
            this.OldIndex = index;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as Collection<SlideBase>).Insert(OldIndex, NewValue as SlideBase);
            SlideHelper.UnSlectedAll();
            (NewValue as SlideBase).IsSelected = true;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<SlideBase>).Remove(OldValue as SlideBase);
            Global.EndInit();
        }
    }
}

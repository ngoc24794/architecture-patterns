﻿using INV.Elearning.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace INV.Elearning.Core.ViewModel
{
    public interface IUndoRedo
    {
        object Source { set; get; }

        object OldValue { set; get; }

        object NewValue { set; get; }

        string Description { set; get; }

        bool IsBreak { set; get; }

        long CreateTime { get; }

        void UndoExcute();

        void RedoExcute();
    }
    
    /// <summary>
    /// Cấp độ
    /// </summary>
    public enum StepLevel
    {
        Document,
        Slide,
        Layout,
        Element,
        Object
    }

    public sealed class UndoRedoByProperty : StepBase
    {
        /// <summary>
        /// Tên thuộc tính bị thay đổi giá trị
        /// </summary>
        private string _propertyName = string.Empty;

        /// <summary>
        /// Lấy hoặc cài đặt tên thuộc tính bị thay đổi giá trị
        /// </summary>
        public string PropertyName
        {
            get
            {
                return _propertyName;
            }

            set
            {
                _propertyName = value;
            }
        }

        /// <summary>
        /// Thực thi lệnh Undo
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            Source.GetType().GetProperty(PropertyName).SetValue(Source, NewValue, null);
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi lệnh Redo
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            Source.GetType().GetProperty(PropertyName).SetValue(Source, OldValue, null);
            Global.EndInit();
        }
    }
}

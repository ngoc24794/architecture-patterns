﻿

namespace INV.Elearning.Core.ViewModel
{
    using INV.Elearning.Core.Helper;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Lớp lưu trữ thông tin các bước thay đổi
    /// </summary>
    public abstract class StepBase : IUndoRedo, IDisposable
    {
        /// <summary>
        /// Mô tả nội dung UndoRedo
        /// </summary>
        private string _des = string.Empty;

        /// <summary>
        /// Lấy hoặc cài đặt nội dung mô tả UndoRedo
        /// </summary>
        public string Description
        {
            get
            {
                return _des;
            }

            set
            {
                _des = value;
            }
        }

        /// <summary>
        /// Giá trị mới được mang
        /// </summary>
        private object _newValue;

        /// <summary>
        /// Lấy hoặc cài đặt giá trị mới được mang
        /// </summary>
        public object NewValue
        {
            get
            {
                return _newValue;
            }

            set
            {
                _newValue = value;
            }
        }

        /// <summary>
        /// Giá trị trước đó
        /// </summary>
        private object _oldValue;

        /// <summary>
        /// Lấy hoặc cài đặt giá trị trước đó được mang
        /// </summary>
        public object OldValue
        {
            get
            {
                return _oldValue;
            }

            set
            {
                _oldValue = value;
            }
        }

        /// <summary>
        /// Đối tượng bị thay đổi dữ liệu
        /// </summary>
        object _source;

        /// <summary>
        /// Lấy hoặc cài đặt đối tượng bị thay đổi dữ liệu
        /// </summary>
        public object Source
        {
            get
            {
                return _source;
            }

            set
            {
                _source = value;
            }
        }

        private long _time;
        /// <summary>
        /// Thời điểm ghi nhận Undo
        /// </summary>
        public long CreateTime
        {
            get { return _time; }
            private set { _time = value; }
        }

        /// <summary>
        /// Thuộc tính không gôm nhóm các UndoRedo
        /// </summary>
        public bool IsBreak { get; set; }

        /// <summary>
        /// Khởi tạo
        /// </summary>
        public StepBase()
        {
            CreateTime = DateTime.Now.Ticks / 1000;
        }

        /// <summary>
        /// Thực thi một thao tác Redo
        /// </summary>
        public virtual void RedoExcute()
        {           
        }

        /// <summary>
        /// Thực thi một thao tác Undo
        /// </summary>
        public virtual void UndoExcute()
        {            
        }

        /// <summary>
        /// Hàm hủy đối tượng
        /// </summary>
        public virtual void Dispose() { }
    }

    /// <summary>
    /// Nhiều bước thay đổi
    /// </summary>
    public class MultiStep : StepBase
    {
        internal List<IUndoRedo> _lstStep = null;

        /// <summary>
        /// Ghi đè sự kiện Undo
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            for (int i = _lstStep.Count - 1; i >= 0; i--)
            {
                _lstStep[i].UndoExcute();
            }
            Global.EndInit();
        }

        /// <summary>
        /// Ghi đề phương thức Redo
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            for (int i = _lstStep.Count - 1; i >= 0; i--)
            {
                _lstStep[i].RedoExcute();
            }
            Global.EndInit();
        }

        /// <summary>
        /// Thêm mới một sự kiện
        /// </summary>
        /// <param name="step"></param>
        public void Add(IUndoRedo step)
        {
            _lstStep.Add(step);
        }

        /// <summary>
        /// Khởi tạo mặc định
        /// </summary>
        public MultiStep()
        {
            _lstStep = new List<IUndoRedo>();
            this.Description = "Multi Step";
        }

        /// <summary>
        /// Lấy ra phần tử cuối
        /// </summary>
        /// <returns></returns>
        public IUndoRedo Pick()
        {
            if (_lstStep.Count > 0)
            {
                return _lstStep.Last();
            }
            else
            {
                return null;
            }
        }
    }
}

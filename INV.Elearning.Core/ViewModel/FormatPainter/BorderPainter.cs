﻿using INV.Elearning.Controls.Enums;
using INV.Elearning.Core.View;
using System;
using System.Windows.Media;

namespace INV.Elearning.Core.ViewModel
{
    public class BorderPainter : IFormatPainter
    {
        /// <summary>
        /// Kiểu dữ liệu hỗ trợ
        /// </summary>
        public virtual Type TargetType => typeof(IBorderSupport);

        /// <summary>
        /// Độ dày viền
        /// </summary>
        public double Thickness { get; set; }

        /// <summary>
        /// Kiểu cap
        /// </summary>
        public PenLineCap CapType { get; set; }

        /// <summary>
        /// Kiểu nối
        /// </summary>
        public PenLineJoin JoinType { get; set; }

        /// <summary>
        /// Kiểu nét đứt
        /// </summary>
        public DashType DashType { get; set; }

        /// <summary>
        /// Màu viền
        /// </summary>
        public ColorBrushBase Stroke { get; set; }

        /// <summary>
        /// Màu nền
        /// </summary>
        public ColorBrushBase Fill { get; set; }
    }
}

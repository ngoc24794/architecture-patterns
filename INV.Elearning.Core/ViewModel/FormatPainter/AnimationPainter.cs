﻿using INV.Elearning.Animations;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Core.ViewModel
{
    public class AnimationPainter
    {
        /// <summary>
        /// Kiểu dữ liệu hỗ trợ
        /// </summary>
        public virtual Type TargetType => typeof(IAnimationableObject);

        /// <summary>
        /// Hiệu ứng vào
        /// </summary>
        public IAnimation Entrance { get; set; }

        /// <summary>
        /// Hiệu ứng ra
        /// </summary>
        public IAnimation Exit { get; set; }

        /// <summary>
        /// Danh sách các đường hiệu ứng
        /// </summary>
        public List<IMotionPathObject> MotionPaths { get; set; }
    }
}

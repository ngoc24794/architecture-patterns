﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.Core.ViewModel
{
    public interface IFormatPainter
    {
        Type TargetType { get; }
    }
}

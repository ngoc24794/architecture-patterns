﻿using System.Collections.Generic;

namespace INV.Elearning.Core.ViewModel
{
    public class StandardPainter : BorderPainter
    {
        /// <summary>
        /// Danh sách hiệu ứng
        /// </summary>
        public List<Model.FrameEffectBase> Effects { get; set; }
    }
}

﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace INV.Elearning.Core.ViewModel
{
    /// <summary>
    /// Lớp ViewModel đã thừa kế sẵn hàm PropertyChanged của Interface INotifyPropertyChanged
    /// </summary>
    public class RootViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}

﻿
using INV.Elearning.Controls;
using System.Windows;
using System.Windows.Media;
using System.Linq;
using INV.Elearning.Core.Helper;

namespace INV.Elearning.Core
{
    /// <summary>
    /// Lớp thể hiện màu sắc
    /// </summary>
    public class ColorSolidBrush : ColorBrushBase
    {
        /// <summary>
        /// Hàm tĩnh
        /// </summary>
        static ColorSolidBrush()
        {
            BrushProperty.OverrideMetadata(typeof(ColorSolidBrush), new FrameworkPropertyMetadata(new BrushConverter().ConvertFromString("White")));
        }

        /// <summary>
        /// Hàm cập nhật các thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void PropertyChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ColorSolidBrush).UpdateBrush();
        }

        /// <summary>
        /// Hàm cập nhật lại giá trị Brush
        /// </summary>
        public override void UpdateBrush()
        {
            if (this.Brush.IsFrozen)
                this.Brush = new SolidColorBrush(this.Color) { Opacity = this.Opacity };
            else
            {
                this.Brush.Opacity = this.Opacity;
                (this.Brush as SolidColorBrush).Color = this.Color;
            }
        }

        /// <summary>
        /// Cập nhật dữ liệu màu khi theme thay đổi
        /// </summary>
        public override void UpdateBrushByTheme()
        {
            if (this.ColorSpecialName != null && ColorGallery.ThemeColors.ContainsKey(this.ColorSpecialName))
            {
                var _colors = (Application.Current as IAppGlobal).SelectedTheme.Colors;
                var _color = _colors.SolidColors.FirstOrDefault(x => this.ColorSpecialName.Contains(x.SpecialName));
                if (_color != null)
                    this.Color = (Color)ColorConverter.ConvertFromString(_color.Color);
            }
            base.UpdateBrushByTheme();
        }

        /// <summary>
        /// Ghi đè thể hiện chính
        /// </summary>
        /// <returns></returns>
        protected override Freezable CreateInstanceCore()
        {
            return new ColorSolidBrush();
        }

        #region Color

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính màu sắc của đối tượng
        /// </summary>
        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Color.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(Color), typeof(ColorSolidBrush), new PropertyMetadata(ColorConverter.ConvertFromString("White"), PropertyChangedCallBack));


        #endregion

        #region Opacity

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính đột trong suốt của đối tượng màu
        /// </summary>
        public double Opacity
        {
            get { return (double)GetValue(OpacityProperty); }
            set { SetValue(OpacityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Opacity.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OpacityProperty =
            DependencyProperty.Register("Opacity", typeof(double), typeof(ColorSolidBrush), new PropertyMetadata(1.0, PropertyChangedCallBack));

        #endregion
        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override ColorBrushBase Clone()
        {
            return new ColorSolidBrush() { Opacity = this.Opacity, Color = this.Color, ColorSpecialName = this.ColorSpecialName };
        }

        /// <summary>
        /// Ghi nhận ra chuỗi
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Color.ToString();
        }
    }
}

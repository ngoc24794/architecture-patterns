﻿using INV.Elearning.Controls;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Core
{
    public sealed class ColorGradientBrush : ColorBrushBase
    {
        /// <summary>
        /// Ghi đè thể hiện chính
        /// </summary>
        /// <returns></returns>
        protected override Freezable CreateInstanceCore()
        {
            return new ColorGradientBrush();
        }

        /// <summary>
        /// Hàm tĩnh
        /// </summary>
        static ColorGradientBrush()
        {
            BrushProperty.OverrideMetadata(typeof(ColorGradientBrush), new PropertyMetadata(new LinearGradientBrush()));
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public ColorGradientBrush()
        {
            this.GradientStops = new ObservableCollection<CustomGradientStop>();
        }

        #region Angle

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính góc đỗ màu
        /// </summary>
        public int Angle
        {
            get { return (int)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(int), typeof(ColorGradientBrush), new PropertyMetadata(0, OnPropertyChanged));

        /// <summary>
        /// Thay đổi thuộc tính
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ColorGradientBrush _colorBrush = d as ColorGradientBrush;
            _colorBrush.UpdateBrush(e.Property);
        }

        public void UpdateBrush(DependencyProperty property)
        {
            if (property == GradientTypeProperty)
            {
                if (this.GradientType == GradientColorType.Linear)
                {
                    var _linear = new LinearGradientBrush(this.GradientStopCollection, 0);
                    this.Brush = _linear;
                    this.Angle = 45;
                }
                else if (this.GradientType == GradientColorType.Radial)
                {
                    var _radial = new RadialGradientBrush(this.GradientStopCollection);
                    _radial.Center = new Point(0.5, 0.5);
                    _radial.RadiusX = _radial.RadiusY = 0.5;
                    this.Brush = _radial;
                }
                return;
            }

            if (this.Brush.IsFrozen)
            {
                if (this.GradientType == GradientColorType.Linear)
                {
                    var _linear = new LinearGradientBrush(this.GradientStopCollection, 0);
                    this.Brush = _linear;
                }
                else if (this.GradientType == GradientColorType.Radial)
                {
                    var _radial = new RadialGradientBrush(this.GradientStopCollection);
                    _radial.Center = new Point(0.5, 0.5);
                    _radial.RadiusX = _radial.RadiusY = 0.5;
                    this.Brush = _radial;
                }
            }

            if (property == AngleProperty)
            {
                if (this.Brush is LinearGradientBrush)
                {
                    var _linear = this.Brush as LinearGradientBrush;
                    if (_linear.RelativeTransform is RotateTransform)
                    {
                        (_linear.RelativeTransform as RotateTransform).Angle = this.Angle;
                    }
                    else
                    {
                        _linear.RelativeTransform = new RotateTransform(this.Angle, 0.5, 0.5);
                    }
                    _linear.StartPoint = new Point();
                    _linear.EndPoint = new Point(1, 0);
                }
            }
            else if (property == CenterPointProperty)
            {
                if (this.Brush is RadialGradientBrush)
                    (this.Brush as RadialGradientBrush).Center = (this.Brush as RadialGradientBrush).GradientOrigin = this.CenterPoint;
            }
            else if (property == GradientStopCollectionProperty)
            {
                if (this.Brush is GradientBrush gradient)
                    gradient.GradientStops = this.GradientStopCollection;
            }
            else if (property == GradientStopsProperty)
            {
                if (this.GradientStopCollection.Count != this.GradientStops.Count) //Đồng bộ danh sách điểm
                {
                    this.GradientStopCollection = Converter(this.GradientStops);
                }
            }
            else
            {
                this.GradientStopCollection = Converter(this.GradientStops);
            }
        }

        /// <summary>
        /// Cập nhật Brush
        /// </summary>
        public override void UpdateBrush()
        {
            if (this.GradientStopCollection?.Count != this.GradientStops.Count) //Đồng bộ danh sách điểm
            {
                //this.GradientStopCollection = Converter(this.GradientStops);
            }

            this.GradientStopCollection = Converter(this.GradientStops);
            if (this.GradientType == GradientColorType.Linear)
            {
                var _linear = new LinearGradientBrush(this.GradientStopCollection);
                _linear.StartPoint = new Point();
                _linear.EndPoint = new Point(1, 0);
                _linear.RelativeTransform = new RotateTransform(this.Angle, 0.5, 0.5);
                this.Brush = _linear;
            }
            else if (this.GradientType == GradientColorType.Radial)
            {
                var _radial = new RadialGradientBrush(this.GradientStopCollection);
                _radial.Center = this.CenterPoint;
                _radial.RadiusX = _radial.RadiusY = 1.0;
                this.Brush = _radial;
            }
        }

        /// <summary>
        /// Cập nhật dữ liệu màu khi theme thay đổi
        /// </summary>
        public override void UpdateBrushByTheme()
        {
            foreach (var item in GradientStops)
            {
                if (item.Color?.SpecialName != null && ColorGallery.ThemeColors.ContainsKey(item.Color.SpecialName))
                {
                    item.Color.Color = ColorHelper.Rebright((Color)ColorConverter.ConvertFromString(ColorGallery.ThemeColors[item.Color.SpecialName].Color), item.Color.Brightness).ToString();
                }
            }

            this.UpdateBrush(null);
        }

        /// <summary>
        /// Chuyển đổi tham số màu
        /// </summary>
        /// <param name="gradientStops"></param>
        /// <returns></returns>
        private GradientStopCollection Converter(ObservableCollection<CustomGradientStop> gradientStops)
        {
            GradientStopCollection _result = new GradientStopCollection();

            if (gradientStops != null)
            {
                foreach (var item in gradientStops)
                {
                    _result.Add(new GradientStop() { Offset = item.Offset, Color = (Color)ColorConverter.ConvertFromString(item.Color.Color) });
                }
            }

            return _result;
        }

        #endregion

        #region CenterPoint

        /// <summary>
        /// Lấy hoặc cài đặt điểm trung tâm
        /// </summary>
        public Point CenterPoint
        {
            get { return (Point)GetValue(CenterPointProperty); }
            set { SetValue(CenterPointProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CenterPoint.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CenterPointProperty =
            DependencyProperty.Register("CenterPoint", typeof(Point), typeof(ColorGradientBrush), new PropertyMetadata(new Point(0.5, 0.5), OnPropertyChanged));

        #endregion

        #region GradientType

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính kiểu đỗ màu
        /// </summary>
        public GradientColorType GradientType
        {
            get { return (GradientColorType)GetValue(GradientTypeProperty); }
            set { SetValue(GradientTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GradientType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GradientTypeProperty =
            DependencyProperty.Register("GradientType", typeof(GradientColorType), typeof(ColorGradientBrush), new PropertyMetadata(GradientColorType.Linear, OnPropertyChanged));

        #endregion

        #region GradientStops

        /// <summary>
        /// Thông tin danh sách các đối tượng điểm dừng
        /// </summary>
        public GradientStopCollection GradientStopCollection
        {
            get { return (GradientStopCollection)GetValue(GradientStopCollectionProperty); }
            set { SetValue(GradientStopCollectionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GradientStopCollection.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GradientStopCollectionProperty =
            DependencyProperty.Register("GradientStopCollection", typeof(GradientStopCollection), typeof(ColorGradientBrush), new PropertyMetadata(null, OnPropertyChanged));


        #endregion

        #region GradientStops

        /// <summary>
        /// Danh sách các điểm dừng, theo dữ liệu tự chuẩn
        /// </summary>
        public ObservableCollection<CustomGradientStop> GradientStops
        {
            get
            {
                var _result = (ObservableCollection<CustomGradientStop>)GetValue(GradientStopsProperty);
                return (ObservableCollection<CustomGradientStop>)GetValue(GradientStopsProperty);
            }
            set { SetValue(GradientStopsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GradientStops.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GradientStopsProperty =
            DependencyProperty.Register("GradientStops", typeof(ObservableCollection<CustomGradientStop>), typeof(ColorGradientBrush), new PropertyMetadata(null, new PropertyChangedCallback(GradientStopsCallback)));

        /// <summary>
        /// Thay đổi danh sách
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void GradientStopsCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ColorGradientBrush _owner = d as ColorGradientBrush;
            _owner.GradientStopCollection = StopsConverter(_owner.GradientStops);
        }

        /// <summary>
        /// Chuyển đổi tham số màu
        /// </summary>
        /// <param name="gradientStops"></param>
        /// <returns></returns>
        private static GradientStopCollection StopsConverter(ObservableCollection<CustomGradientStop> gradientStops)
        {
            GradientStopCollection _result = new GradientStopCollection();

            if (gradientStops != null)
            {
                foreach (var item in gradientStops)
                {
                    _result.Add(new GradientStop() { Offset = item.Offset, Color = (Color)ColorConverter.ConvertFromString(item.Color.Color) });
                }
            }

            return _result;
        }
        #endregion

        /// <summary>
        /// Nhân bản
        /// </summary>
        /// <returns></returns>
        public override ColorBrushBase Clone()
        {
            var _result = new ColorGradientBrush()
            {
                Angle = this.Angle,
                CenterPoint = this.CenterPoint,
                GradientType = this.GradientType,
                ColorSpecialName = this.ColorSpecialName,
                GradientStops = CloneStops(GradientStops)
            };
            _result.UpdateBrush();
            return _result;
        }

        /// <summary>
        /// So sánh 2 giá trị
        /// </summary>
        /// <param name="color1"></param>
        /// <param name="color2"></param>
        /// <returns></returns>
        public static bool Compare(ColorGradientBrush color1, ColorGradientBrush color2)
        {
            if (color1 == null || color2 == null) return false;
            if (color1.Angle != color2.Angle) return false;
            if (color1.CenterPoint.X != color2.CenterPoint.X || color1.CenterPoint.Y != color2.CenterPoint.Y) return false;
            foreach (var item in color1.GradientStops)
            {
                foreach (var item2 in color2.GradientStops)
                {
                    if (item.Color.Color != item2.Color.Color || item.Offset != item2.Offset) return false;
                }
            }
            if (color1.GradientType != color2.GradientType) return false;
            return true;
        }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static ObservableCollection<CustomGradientStop> CloneStops(ObservableCollection<CustomGradientStop> source)
        {
            var _result = new ObservableCollection<CustomGradientStop>();
            foreach (var item in source)
            {
                _result.Add(item.Clone());
            }

            return _result;
        }
    }
}

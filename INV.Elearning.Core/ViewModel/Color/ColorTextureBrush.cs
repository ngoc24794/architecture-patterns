﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Core
{
    public class ColorTextureBrush : ColorBrushBase
    {
        /// <summary>
        /// Đường dẫn hình ảnh
        /// </summary>
        public string ImagePath
        {
            get { return (string)GetValue(ImagePathProperty); }
            set { SetValue(ImagePathProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImagePath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImagePathProperty =
            DependencyProperty.Register("ImagePath", typeof(string), typeof(ColorTextureBrush), new PropertyMetadata(string.Empty, OnPropertyChanged));

        /// <summary>
        /// Độ trong suốt
        /// </summary>
        public double Transparentcy
        {
            get { return (double)GetValue(TransparentcyProperty); }
            set { SetValue(TransparentcyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Transparentcy.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransparentcyProperty =
            DependencyProperty.Register("Transparentcy", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(0.0, OnPropertyChanged));


        /// <summary>
        /// Có lặp lại hay không
        /// </summary>
        public bool IsTiled
        {
            get { return (bool)GetValue(IsTiledProperty); }
            set { SetValue(IsTiledProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Tiled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTiledProperty =
            DependencyProperty.Register("IsTiled", typeof(bool), typeof(ColorTextureBrush), new PropertyMetadata(true, OnPropertyChanged));

        /// <summary>
        /// Vị trí X
        /// </summary>
        public double OffsetX
        {
            get { return (double)GetValue(OffsetXProperty); }
            set { SetValue(OffsetXProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OffsetX.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OffsetXProperty =
            DependencyProperty.Register("OffsetX", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(0.0, OnPropertyChanged));

        /// <summary>
        /// Vị trí Y
        /// </summary>
        public double OffsetY
        {
            get { return (double)GetValue(OffsetYProperty); }
            set { SetValue(OffsetYProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OffsetY.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OffsetYProperty =
            DependencyProperty.Register("OffsetY", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(0.0, OnPropertyChanged));

        /// <summary>
        /// Co giãn chiều ngang
        /// </summary>
        public double ScaleX
        {
            get { return (double)GetValue(ScaleXProperty); }
            set { SetValue(ScaleXProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleX.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleXProperty =
            DependencyProperty.Register("ScaleX", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(1.0, OnPropertyChanged));

        /// <summary>
        /// Co giãn chiều dọc
        /// </summary>
        public double ScaleY
        {
            get { return (double)GetValue(ScaleYProperty); }
            set { SetValue(ScaleYProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleY.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleYProperty =
            DependencyProperty.Register("ScaleY", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(1.0, OnPropertyChanged));

        /// <summary>
        /// Vị trí trái
        /// </summary>
        public double OffsetLeft
        {
            get { return (double)GetValue(OffsetLeftProperty); }
            set { SetValue(OffsetLeftProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OffsetLeft.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OffsetLeftProperty =
            DependencyProperty.Register("OffsetLeft", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(0.0, OnPropertyChanged));

        /// <summary>
        /// Vị trí phải
        /// </summary>
        public double OffsetRight
        {
            get { return (double)GetValue(OffsetRightProperty); }
            set { SetValue(OffsetRightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OffsetRight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OffsetRightProperty =
            DependencyProperty.Register("OffsetRight", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(0.0, OnPropertyChanged));


        /// <summary>
        /// Vị trí trên
        /// </summary>
        public double OffsetTop
        {
            get { return (double)GetValue(OffsetTopProperty); }
            set { SetValue(OffsetTopProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OffsetTop.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OffsetTopProperty =
            DependencyProperty.Register("OffsetTop", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(0.0, OnPropertyChanged));


        /// <summary>
        /// Vị trí dưới
        /// </summary>
        public double OffsetBottom
        {
            get { return (double)GetValue(OffsetBottomProperty); }
            set { SetValue(OffsetBottomProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OffsetBottom.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OffsetBottomProperty =
            DependencyProperty.Register("OffsetBottom", typeof(double), typeof(ColorTextureBrush), new PropertyMetadata(0.0, OnPropertyChanged));

        /// <summary>
        /// Căn lề cho hình ảnh
        /// </summary>
        public ImageAlignement Alignement
        {
            get { return (ImageAlignement)GetValue(AlignementProperty); }
            set { SetValue(AlignementProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Alignement.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AlignementProperty =
            DependencyProperty.Register("Alignement", typeof(ImageAlignement), typeof(ColorTextureBrush), new PropertyMetadata(ImageAlignement.Top, OnPropertyChanged));

        /// <summary>
        /// Kiểu phản chiếu
        /// </summary>
        public MirrorType Mirror
        {
            get { return (MirrorType)GetValue(MirrorProperty); }
            set { SetValue(MirrorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Mirror.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MirrorProperty =
            DependencyProperty.Register("Mirror", typeof(MirrorType), typeof(ColorTextureBrush), new PropertyMetadata(MirrorType.None, OnPropertyChanged));

        /// <summary>
        /// Quay cùng với khung bao
        /// </summary>
        public bool CanRotate
        {
            get { return (bool)GetValue(CanRotateProperty); }
            set { SetValue(CanRotateProperty, value); }
        }


        // Using a DependencyProperty as the backing store for CanRotate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanRotateProperty =
            DependencyProperty.Register("CanRotate", typeof(bool), typeof(ColorTextureBrush), new PropertyMetadata(true, OnPropertyChanged));

        /// <summary>
        /// Thay đổi các giá trị
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ColorTextureBrush _color = d as ColorTextureBrush;
            _color.UpdateBrush();
        }

        /// <summary>
        /// Hàm cập nhật lại giá trị Brush
        /// </summary>
        public override void UpdateBrush()
        {
            if (string.IsNullOrEmpty(this.ImagePath) || (!this.ImagePath.Contains("pack://application:,,,/INV.Elearning.Resources;component/Resources/Images/Texture/") && !File.Exists(this.ImagePath)))
            {
                this.Brush = null;
                return;
            }

            if (this.Brush == null || this.Brush.IsFrozen)
                this.Brush = new ImageBrush();
            Size size = new Size();
            if (!this.ImagePath.Contains("pack://application:,,,"))
            {
                using (FileStream fileStream = new FileStream(this.ImagePath, FileMode.Open, FileAccess.Read))
                {
                    BitmapFrame frame = BitmapFrame.Create(fileStream, BitmapCreateOptions.DelayCreation, BitmapCacheOption.None);
                    size = new Size(frame.PixelWidth, frame.PixelHeight);
                }
            }          

            BitmapImage _bitmapImage = new BitmapImage();
            _bitmapImage.BeginInit();
            _bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            _bitmapImage.UriSource = new Uri(this.ImagePath);
            _bitmapImage.DecodePixelWidth = (int)Math.Min((Application.Current as IAppGlobal).SlideSize.Width, size.Width);
            _bitmapImage.DecodePixelHeight = (int)Math.Min((Application.Current as IAppGlobal).SlideSize.Height, size.Height);
            RenderOptions.SetBitmapScalingMode(_bitmapImage, BitmapScalingMode.Linear);
            RenderOptions.SetCachingHint(_bitmapImage, CachingHint.Cache);
            _bitmapImage.EndInit();
            _bitmapImage.Freeze(); // Call this after EndInit and before using the image.
            (this.Brush as ImageBrush).ImageSource = _bitmapImage;

            //(this.Brush as ImageBrush).ImageSource = new BitmapImage(new Uri(this.ImagePath));
            this.Brush.Opacity = 1.0 - this.Transparentcy;
            (this.Brush as ImageBrush).Stretch = Stretch.Fill;
            this.SetAligment((this.Brush as ImageBrush), this.Alignement);
            if (this.IsTiled)//Có lặp lại
            {
                this.SetMirror((this.Brush as ImageBrush), this.Mirror);
                (this.Brush as ImageBrush).Viewport = new Rect(0, 0, (this.Brush as ImageBrush).ImageSource.Width, (this.Brush as ImageBrush).ImageSource.Height);
                (this.Brush as ImageBrush).ViewportUnits = BrushMappingMode.Absolute;

                TransformGroup _trsGroup = new TransformGroup();
                ScaleTransform _scale = new ScaleTransform(this.ScaleX, this.ScaleY);
                _trsGroup.Children.Add(_scale);
                TranslateTransform _translate = new TranslateTransform();
                _translate.X = this.OffsetX;
                _translate.Y = this.OffsetY;
                _trsGroup.Children.Add(_translate);
                (this.Brush as ImageBrush).Transform = _trsGroup;
            }
            else
            {
                double _left, _top, _width, _height;
                _left = this.OffsetLeft;
                _top = this.OffsetTop;
                _width = 1.0 - _left - this.OffsetRight;
                _height = 1.0 - _top - this.OffsetBottom;
                (this.Brush as ImageBrush).Viewport = new Rect(_left, _top, _width, _height);
            }
            (this.Brush as ImageBrush).Freeze();
        }

        /// <summary>
        /// Hàm tĩnh
        /// </summary>
        static ColorTextureBrush()
        {
            BrushProperty.OverrideMetadata(typeof(ColorTextureBrush), new PropertyMetadata(new ImageBrush()));
        }

        /// <summary>
        /// Căn lề
        /// </summary>
        /// <param name="imageBrush"></param>
        /// <param name="align"></param>
        private void SetAligment(ImageBrush imageBrush, ImageAlignement align)
        {
            switch (align)
            {
                case ImageAlignement.TopLeft:
                    imageBrush.AlignmentX = AlignmentX.Left;
                    imageBrush.AlignmentY = AlignmentY.Top;
                    break;
                case ImageAlignement.Top:
                    imageBrush.AlignmentY = AlignmentY.Top;
                    break;
                case ImageAlignement.TopRight:
                    imageBrush.AlignmentX = AlignmentX.Right;
                    imageBrush.AlignmentY = AlignmentY.Top;
                    break;
                case ImageAlignement.Left:
                    imageBrush.AlignmentX = AlignmentX.Left;
                    break;
                case ImageAlignement.Center:
                    imageBrush.AlignmentX = AlignmentX.Center;
                    imageBrush.AlignmentY = AlignmentY.Center;
                    break;
                case ImageAlignement.Right:
                    imageBrush.AlignmentX = AlignmentX.Right;
                    break;
                case ImageAlignement.BottomLeft:
                    imageBrush.AlignmentX = AlignmentX.Left;
                    imageBrush.AlignmentY = AlignmentY.Bottom;
                    break;
                case ImageAlignement.Bottom:
                    imageBrush.AlignmentY = AlignmentY.Bottom;
                    break;
                case ImageAlignement.BottomRight:
                    imageBrush.AlignmentX = AlignmentX.Right;
                    imageBrush.AlignmentY = AlignmentY.Bottom;
                    break;
            }
        }

        /// <summary>
        /// Phản chiếu
        /// </summary>
        /// <param name="imageBrush"></param>
        /// <param name="mirror"></param>
        private void SetMirror(ImageBrush imageBrush, MirrorType mirror)
        {
            switch (mirror)
            {
                case MirrorType.None:
                    imageBrush.TileMode = TileMode.Tile;
                    break;
                case MirrorType.Vertical:
                    imageBrush.TileMode = TileMode.FlipY;
                    break;
                case MirrorType.Horizontal:
                    imageBrush.TileMode = TileMode.FlipX;
                    break;
                case MirrorType.Both:
                    imageBrush.TileMode = TileMode.FlipXY;
                    break;
            }
        }

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override ColorBrushBase Clone()
        {
            return new ColorTextureBrush()
            {
                Alignement = this.Alignement,
                CanRotate = this.CanRotate,
                ImagePath = this.ImagePath,
                IsTiled = this.IsTiled,
                Mirror = this.Mirror,
                OffsetBottom = this.OffsetBottom,
                OffsetLeft = this.OffsetLeft,
                OffsetRight = this.OffsetRight,
                OffsetTop = this.OffsetTop,
                OffsetX = this.OffsetX,
                OffsetY = this.OffsetY,
                ScaleX = this.ScaleX,
                ScaleY = this.ScaleY,
                Transparentcy = this.Transparentcy
            };
        }
    }
}

﻿
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Core
{
    /// <summary>
    /// Lớp đại diện màu sắc.
    /// Có thể cấu hình lưu trữ các thông số ở các kiểu đổ màu khác nhau
    /// </summary>
    public class ColorBrushBase : Freezable, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Cập nhậ thay đổi thuộc tính
        /// </summary>
        /// <param name="propertyName"></param>
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Ghi đè hàm mặc định
        /// </summary>
        /// <returns></returns>
        protected override Freezable CreateInstanceCore()
        {
            return new ColorBrushBase();
        }


        /// <summary>
        /// Màu
        /// </summary>
        public Brush Brush
        {
            get { return (Brush)GetValue(BrushProperty); }
            set { SetValue(BrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Brush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BrushProperty =
            DependencyProperty.Register("Brush", typeof(Brush), typeof(ColorBrushBase), new PropertyMetadata(Brushes.Transparent));


        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính tên của màu sắc
        /// </summary>
        public string ColorSpecialName
        {
            get { return (string)GetValue(ColorSpecialNameProperty); }
            set { SetValue(ColorSpecialNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ColorSpecialName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ColorSpecialNameProperty =
            DependencyProperty.Register("ColorSpecialName", typeof(string), typeof(ColorBrushBase), new PropertyMetadata(string.Empty));



        /// <summary>
        /// Nhân bản một đối tượng
        /// </summary>
        /// <returns></returns>
        public new virtual ColorBrushBase Clone()
        {
            return new ColorBrushBase();
        }

        /// <summary>
        /// Cập nhật lại kiểu màu
        /// </summary>
        public virtual void UpdateBrush() { OnPropertyChanged("Brush"); }

        /// <summary>
        /// Cập nhật lại màu sắc theo theme
        /// </summary>
        public virtual void UpdateBrushByTheme()
        {
            UpdateBrush();
        }

        /// <summary>
        /// Ghi nhận màu
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Brush is SolidColorBrush solid) return solid.Color.ToString();

            return "Transparent";
        }
    }
}

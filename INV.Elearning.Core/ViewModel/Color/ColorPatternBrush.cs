﻿using INV.Elearning.Core.Model;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Core
{
    public sealed class ColorPatternBrush : ColorBrushBase
    {
        /// <summary>
        /// Ghi đè thể hiện chính
        /// </summary>
        /// <returns></returns>
        protected override Freezable CreateInstanceCore()
        {
            return new ColorGradientBrush();
        }

        /// <summary>
        /// Hàm khởi tạo tĩnh
        /// </summary>
        static ColorPatternBrush()
        {
            BrushProperty.OverrideMetadata(typeof(ColorPatternBrush), new PropertyMetadata(new DrawingBrush()));
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public ColorPatternBrush()
        {
            this.PatternForeground = new Controls.SolidColor() { Color = "Black" };
            this.PatternBackground = new Controls.SolidColor() { Color = "White" };
        }

        /// <summary>
        /// Màu chữ
        /// </summary>
        public Controls.SolidColor PatternForeground
        {
            get { return (Controls.SolidColor)GetValue(PatternForegroundProperty); }
            set { SetValue(PatternForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for _patternForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PatternForegroundProperty =
            DependencyProperty.Register("PatternForeground", typeof(Controls.SolidColor), typeof(ColorPatternBrush), new PropertyMetadata(null, CallBack));

        /// <summary>
        /// Màu nền
        /// </summary>
        public Controls.SolidColor PatternBackground
        {
            get { return (Controls.SolidColor)GetValue(PatternBackgroundProperty); }
            set { SetValue(PatternBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for _patternBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PatternBackgroundProperty =
            DependencyProperty.Register("PatternBackground", typeof(Controls.SolidColor), typeof(ColorPatternBrush), new PropertyMetadata(null, CallBack));

        /// <summary>
        /// Kiểu đổ màu
        /// </summary>
        public HatchStyle HatchStyle
        {
            get { return (HatchStyle)GetValue(HatchStyleProperty); }
            set { SetValue(HatchStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Type.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HatchStyleProperty =
            DependencyProperty.Register("HatchStyle", typeof(HatchStyle), typeof(ColorPatternBrush), new PropertyMetadata(HatchStyle.HS_PERCENT05, CallBack));

        /// <summary>
        /// Hàm gọi lại dữ liệu
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void CallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _color = (d as ColorPatternBrush);

            if ((_color.Brush != null || e.NewValue is HatchStyle) && (_color.PatternBackground != null && _color.PatternForeground != null))
                _color.Brush = Helper.Converters.EnumPatternBrushConverter.GetHatchBrush(_color.HatchStyle, _color.PatternForeground.Color, _color.PatternBackground.Color);
        }
        

        /// <summary>
        /// Nhân bản đối tượng
        /// </summary>
        /// <returns></returns>
        public override ColorBrushBase Clone()
        {
            return new ColorPatternBrush()
            {
                HatchStyle = this.HatchStyle,
                PatternBackground = this.PatternBackground,
                PatternForeground = this.PatternForeground
            };
        }

    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IModule.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Mô tả nghiệp vụ của một mô đun
    /// </summary>
    public interface IModule
    {
        /// <summary>
        /// Thực hiện các đăng ký kiểu dữ liệu lên kho và đăng ký view lên các vùng
        /// </summary>
        void RegisterTypes();

        /// <summary>
        /// Khởi tạo các đối tượng trình diễn của mô đun
        /// </summary>
        void Initialize();
    }
}

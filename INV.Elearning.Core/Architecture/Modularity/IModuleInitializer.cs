﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IModuleInitializer.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Khởi tạo một mô đun
    /// </summary>
    public interface IModuleInitializer
    {
        /// <summary>
        /// Khởi tạo một mô đun với <see cref="IModuleInfo"/> được chỉ định
        /// </summary>
        /// <param name="moduleInfo">Thông tin mô đun cần khởi tạo</param>
        void Initialize(IModuleInfo moduleInfo);
    }
}

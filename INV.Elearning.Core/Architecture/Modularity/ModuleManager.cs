﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ModuleManager.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Quản lý mô đun
    /// </summary>
    public class ModuleManager : IModuleManager
    {
        private readonly IModuleCatalog _moduleCatalog;
        private readonly IModuleInitializer _moduleInitializer;
        public ModuleManager(IModuleCatalog moduleCatalog, IModuleInitializer moduleInitializer)
        {
            _moduleCatalog = moduleCatalog;
            _moduleInitializer = moduleInitializer;
        }

        /// <summary>
        /// Chạy tiến trình khởi tạo mô đun
        /// </summary>
        public void Run()
        {
            LoadModules();
        }

        /// <summary>
        /// Nạp các mô đun
        /// </summary>
        public void LoadModules()
        {
            var modules = _moduleCatalog.Modules;
            if (modules != null)
            {
                foreach (var moduleInfo in modules)
                {
                    _moduleInitializer.Initialize(moduleInfo);
                }
            }
        }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IModuleCatalog.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Collections.Generic;

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Danh mục mô đun chứa thông tin của các mô đun được nạp vào ứng dụng. 
    /// </summary>
    public interface IModuleCatalog
    {
        /// <summary>
        /// Danh sách các mô đun hiện có
        /// </summary>
        ICollection<IModuleInfo> Modules { get; }

        /// <summary>
        /// Nạp thêm một mô đun
        /// </summary>
        /// <param name="type">Kiểu dữ liệu của lớp đại diện cho mô đun</param>
        /// <returns><see cref="IModuleCatalog"/> dùng để nạp mô đun liên tục</returns>
        IModuleCatalog AddModule(Type type);

        /// <summary>
        /// Khởi tạo cấu hình cho các mô đun
        /// </summary>
        void Initialize();
    }
}

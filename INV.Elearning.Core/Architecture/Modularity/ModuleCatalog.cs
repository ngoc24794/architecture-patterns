﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ModuleCatalog.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Danh mục mô đun
    /// </summary>
    [Serializable]
    public class ModuleCatalog : IModuleCatalog
    {
        private bool _isLoaded;
        protected ICollection<IModule> Items { get; set; }
        public ModuleCatalog()
        {
            Modules = new Collection<IModuleInfo>();
            Items = new Collection<IModule>();
        }

        public ICollection<IModuleInfo> Modules { get; }

        public IModuleCatalog AddModule(Type type)
        {
            CollectModule(type);
            return this;
        }

        private void CollectModule(Type type)
        {
            IModuleInfo moduleInfo = new ModuleInfo(type.Assembly.GetName().Name, type);
            Modules.Add(moduleInfo);
        }

        public void Load()
        {
            _isLoaded = true;
            InnerLoad();
        }

        public void Initialize()
        {
            if (!_isLoaded)
            {
                Load();
            }
        }

        private void InnerLoad()
        {
            AppDomain childDomain = BuildChildDomain(AppDomain.CurrentDomain);

            try
            {
                Type type = typeof(InnerModuleInfoLoader);
                InnerModuleInfoLoader loader = (InnerModuleInfoLoader)childDomain.CreateInstance(type.Assembly.FullName, type.FullName).Unwrap();
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string[] files = Directory.GetFiles(path, "*.dll");
                foreach (string dll in files)
                {
                    loader.LoadAssembly(dll);
                }
                List<Assembly> validAssemblies = loader.GetAssemblies(Filter);
                validAssemblies?.ForEach(a =>
                {
                    Type[] types = a.GetExportedTypes();
                    foreach (var t in types)
                    {
                        if (t.GetInterface(typeof(IModule).FullName)?.Name == nameof(IModule))
                        {
                            CollectModule(t);
                        }
                    }
                });
            }
            finally
            {
                AppDomain.Unload(childDomain);
            }
        }

        private bool Filter(Assembly a)
        {
            try
            {
                Type[] types;
                try
                {
                    types = a.GetTypes();
                }
                catch (ReflectionTypeLoadException e)
                {
                    types = e.Types;
                }
                foreach (var type in types)
                {
                    if (type.GetInterface(typeof(IModule).FullName).Name == nameof(IModule))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        private AppDomain BuildChildDomain(AppDomain parentDomain)
        {
            Evidence evidence = new Evidence(parentDomain.Evidence);
            AppDomainSetup setup = parentDomain.SetupInformation;
            return AppDomain.CreateDomain("DiscoveryRegion", evidence, setup);
        }

        private class InnerModuleInfoLoader : MarshalByRefObject
        {
            private string _assemblyPath;

            public void LoadAssembly(String assemblyPath)
            {
                try
                {
                    _assemblyPath = assemblyPath;
                    Assembly.ReflectionOnlyLoadFrom(assemblyPath);
                }
                catch (FileNotFoundException)
                {
                    // Tiếp tục nạp các assembly khác
                }
            }

            public TResult Reflect<TResult>(Func<Assembly, TResult> func)
            {
                DirectoryInfo directory = new FileInfo(_assemblyPath).Directory;

                ResolveEventHandler resolveEventHandler = (s, e) => OnReflectionOnlyResolve(e, directory);

                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += resolveEventHandler;

                var assembly = AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies().FirstOrDefault(a => a.Location.CompareTo(_assemblyPath) == 0);

                if (assembly != null)
                {
                    var result = func(assembly);
                    return result;
                }

                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve -= resolveEventHandler;

                return default(TResult);
            }

            private Assembly OnReflectionOnlyResolve(ResolveEventArgs args, DirectoryInfo directory)
            {
                Assembly loadedAssembly =
                    AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies()
                        .FirstOrDefault(
                          asm => string.Equals(asm.FullName, args.Name,
                              StringComparison.OrdinalIgnoreCase));

                if (loadedAssembly != null)
                {
                    return loadedAssembly;
                }

                AssemblyName assemblyName =
                    new AssemblyName(args.Name);
                string dependentAssemblyFilename =
                    Path.Combine(directory.FullName,
                    assemblyName.Name + ".dll");

                if (File.Exists(dependentAssemblyFilename))
                {
                    return Assembly.ReflectionOnlyLoadFrom(
                        dependentAssemblyFilename);
                }
                return Assembly.ReflectionOnlyLoad(args.Name);
            }

            public Assembly GetAssembly(string assemblyName)
            {
                return GetAssembly(a => a.GetName().Name == assemblyName);
            }

            public Assembly GetAssembly(Func<Assembly, bool> selector)
            {
                var assembly = AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies().FirstOrDefault(selector);
                return assembly;
            }

            public List<Assembly> GetAssemblies(Func<Assembly, bool> selector)
            {
                return AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies().Where(selector).ToList();
            }
        }
    }
}

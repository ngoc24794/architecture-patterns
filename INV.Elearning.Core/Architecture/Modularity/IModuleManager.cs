﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IModuleManager.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Quản lý mô đun
    /// </summary>
    public interface IModuleManager
    {
        /// <summary>
        /// Chạy tiến trình khởi tạo mô đun
        /// </summary>
        void Run();

        /// <summary>
        /// Nạp các mô đun
        /// </summary>
        void LoadModules();
    }
}

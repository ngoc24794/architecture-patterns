﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ModuleInitializer.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Architecture.IoC;
using System;

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Khởi tạo một mô đun
    /// </summary>
    public class ModuleInitializer : IModuleInitializer
    {
        private readonly IContainer _container;
        public ModuleInitializer(IContainer container)
        {
            _container = container;
        }

        /// <summary>
        /// Khởi tạo mô đun với <see cref="IModuleInfo"/> được chỉ định.
        /// </summary>
        /// <param name="moduleInfo"><see cref="IModuleInfo"/> dùng để tạo mô đun</param>
        public void Initialize(IModuleInfo moduleInfo)
        {
            var module = CreateModule(Type.GetType(moduleInfo.ModuleType));
            if (module != null)
            {
                module.RegisterTypes();
                module.Initialize();
            }
        }

        /// <summary>
        /// Tạo mới một mô đun bằng cách sử dụng <see cref="IContainer"/>
        /// </summary>
        /// <param name="type">Kiểu dữ liệu của mô đun</param>
        /// <returns>Mô đun đã tạo</returns>
        protected virtual IModule CreateModule(Type type) => (IModule)_container.Resolve(type);
    }
}

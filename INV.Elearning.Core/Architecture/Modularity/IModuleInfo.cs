﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IModuleInfo.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Thông tin của một mô đun
    /// </summary>
    public interface IModuleInfo
    {
        string ModuleName { get; set; }
        string ModuleType { get; set; }
    }
}

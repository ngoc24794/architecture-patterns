﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;

namespace INV.Elearning.Core.Architecture.Modularity
{
    /// <summary>
    /// Thông tin của một mô đun
    /// </summary>
    [Serializable]
    public class ModuleInfo : IModuleInfo
    {
        public ModuleInfo(string moduleName, Type moduleType)
        {
            ModuleName = moduleName;
            ModuleType = moduleType;
        }

        public string ModuleName { get; set; }
        string IModuleInfo.ModuleType
        {
            get => ModuleType.AssemblyQualifiedName;
            set => ModuleType = Type.GetType(value);
        }

        /// <summary>
        /// Lấy hoặc đặt Kiểu dữ liệu đại diện cho mô đun.
        /// </summary>
        /// <value>Kiểu dữ liệu của mô đun</value>
        public Type ModuleType { get; set; }
    }
}

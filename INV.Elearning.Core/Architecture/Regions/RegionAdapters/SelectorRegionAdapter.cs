using INV.Elearning.Core.Architecture.Regions.Behaviors;
using System.Windows.Controls.Primitives;

namespace INV.Elearning.Core.Architecture.Regions
{
    public class SelectorRegionAdapter : RegionAdapterBase<Selector>
    {
        public SelectorRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory)
            : base(regionBehaviorFactory)
        {
        }

        protected override void Adapt(IRegion region, Selector regionTarget)
        {
        }

        protected override void AttachBehaviors(IRegion region, Selector regionTarget)
        {
            region.Behaviors.Add(SelectorItemsSourceSyncBehavior.BehaviorKey, new SelectorItemsSourceSyncBehavior()
                                                                                  {
                                                                                      HostControl = regionTarget
                                                                                  });

            base.AttachBehaviors(region, regionTarget);
        }

        protected override IRegion CreateRegion()
        {
            return new Region();
        }
    }
}
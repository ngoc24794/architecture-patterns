using System.Windows;

namespace INV.Elearning.Core.Architecture.Regions
{
    public abstract class RegionAdapterBase<T> : IRegionAdapter where T : class
    {
        protected RegionAdapterBase(IRegionBehaviorFactory regionBehaviorFactory)
        {
            this.RegionBehaviorFactory = regionBehaviorFactory;
        }

        protected IRegionBehaviorFactory RegionBehaviorFactory { get; set; }

        public IRegion Initialize(T regionTarget, string regionName)
        {
            IRegion region = this.CreateRegion();
            region.Name = regionName;

            SetObservableRegionOnHostingControl(region, regionTarget);

            this.Adapt(region, regionTarget);
            this.AttachBehaviors(region, regionTarget);
            this.AttachDefaultBehaviors(region, regionTarget);
            return region;
        }

        IRegion IRegionAdapter.Initialize(object regionTarget, string regionName)
        {
            return this.Initialize(GetCastedObject(regionTarget), regionName);
        }

        protected virtual void AttachDefaultBehaviors(IRegion region, T regionTarget)
        {
            IRegionBehaviorFactory behaviorFactory = this.RegionBehaviorFactory;
            if (behaviorFactory != null)
            {
                DependencyObject dependencyObjectRegionTarget = regionTarget as DependencyObject;

                foreach (string behaviorKey in behaviorFactory)
                {
                    if (!region.Behaviors.ContainsKey(behaviorKey))
                    {
                        IRegionBehavior behavior = behaviorFactory.CreateFromKey(behaviorKey);

                        if (dependencyObjectRegionTarget != null)
                        {
                            if (behavior is IHostAwareRegionBehavior hostAwareRegionBehavior)
                            {
                                hostAwareRegionBehavior.HostControl = dependencyObjectRegionTarget;
                            }
                        }

                        region.Behaviors.Add(behaviorKey, behavior);
                    }
                }
            }
        }

        protected virtual void AttachBehaviors(IRegion region, T regionTarget)
        {
        }

        protected abstract void Adapt(IRegion region, T regionTarget);

        protected abstract IRegion CreateRegion();

        private static T GetCastedObject(object regionTarget)
        {
            T castedObject = regionTarget as T;
            return castedObject;
        }

        private static void SetObservableRegionOnHostingControl(IRegion region, T regionTarget)
        {
            DependencyObject targetElement = regionTarget as DependencyObject;

            if (targetElement != null)
            {
                RegionManager.GetObservableRegion(targetElement).Value = region;
            }
        }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RibbonRegionAdapter.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Controls;
using System.Collections.Specialized;
using System.Linq;

namespace INV.Elearning.Core.Architecture.Regions
{
    public class RibbonRegionAdapter : RegionAdapterBase<Ribbon>
    {
        public RibbonRegionAdapter(IRegionBehaviorFactory regionBehaviourFactory) : base(regionBehaviourFactory) { }

        private Ribbon _ribbon;

        protected override void Adapt(IRegion region, Ribbon ribbon)
        {
            _ribbon = ribbon;

            ribbon.Tabs.Clear();
            region.ActiveViews.CollectionChanged += new NotifyCollectionChangedEventHandler((s, e) =>
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (RibbonTabItem ribbonTab in e.NewItems)
                            _ribbon.Tabs.Add(ribbonTab);
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (RibbonTabItem ribbonTab in e.NewItems)
                            _ribbon.Tabs.Remove(ribbonTab);
                        break;
                }
            });


            region.ActiveViews.ToList().ForEach(x => ribbon.Tabs.Add(x as RibbonTabItem));
        }

        protected override IRegion CreateRegion()
        {
            return new AllActiveRegion();
        }
    }
}

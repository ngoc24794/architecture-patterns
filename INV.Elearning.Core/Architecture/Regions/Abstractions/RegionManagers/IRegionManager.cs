﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionViewRegistry.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;

namespace INV.Elearning.Core.Architecture.Regions
{
    public interface IRegionManager
    {
        IRegionManager RegisterViewWithRegion(string regionName, Type viewType);
        IRegionManager CreateRegionManager();
        IRegionCollection Regions { get; }
    }
}

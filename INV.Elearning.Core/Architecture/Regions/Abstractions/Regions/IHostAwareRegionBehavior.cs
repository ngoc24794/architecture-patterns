

using System.Windows;

namespace INV.Elearning.Core.Architecture.Regions
{
    public interface IHostAwareRegionBehavior : IRegionBehavior
    {
        DependencyObject HostControl { get; set; }
    }
}
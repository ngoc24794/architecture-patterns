

using System.Collections.Generic;
using System.Collections.Specialized;

namespace INV.Elearning.Core.Architecture.Regions
{
    public interface IViewsCollection : IEnumerable<object>, INotifyCollectionChanged
    {
        bool Contains(object value);
    }
}
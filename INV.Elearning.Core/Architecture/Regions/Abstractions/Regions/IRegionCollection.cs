

using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace INV.Elearning.Core.Architecture.Regions
{
    public interface IRegionCollection : IEnumerable<IRegion>, INotifyCollectionChanged
    {
        IRegion this[string regionName] { get; }

        void Add(IRegion region);

        bool Remove(string regionName);

        bool ContainsRegionWithName(string regionName);

        void Add(string regionName, IRegion region);
    }
}



using System;
using System.Collections.Generic;

namespace INV.Elearning.Core.Architecture.Regions
{
    public interface IRegionBehaviorFactory : IEnumerable<string>
    {
        void AddIfMissing(string behaviorKey, Type behaviorType);

        bool ContainsKey(string behaviorKey);

        IRegionBehavior CreateFromKey(string key);
    }
}
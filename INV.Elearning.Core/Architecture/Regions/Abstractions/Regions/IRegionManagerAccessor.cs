

using System;
using System.Windows;

namespace INV.Elearning.Core.Architecture.Regions
{
    public interface IRegionManagerAccessor
    {
        event EventHandler UpdatingRegions;

        string GetRegionName(DependencyObject element);

        IRegionManager GetRegionManager(DependencyObject element);
    }
}



namespace INV.Elearning.Core.Architecture.Regions
{
    public interface IRegionAdapter
    {
        IRegion Initialize(object regionTarget, string regionName);
    }
}
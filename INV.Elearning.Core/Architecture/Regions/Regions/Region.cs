﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegion.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/


using INV.Elearning.Core.Architecture.Regions.Behaviors;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace INV.Elearning.Core.Architecture.Regions
{
    public class Region : IRegion
    {
        private ObservableCollection<ItemMetadata> itemMetadataCollection;
        private string name;
        private ViewsCollection views;
        private ViewsCollection activeViews;
        private IRegionManager regionManager;

        public Region()
        {
            Behaviors = new RegionBehaviorCollection(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public IRegionBehaviorCollection Behaviors { get; private set; }

        public string Name
        {
            get { return name; }

            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public virtual IViewsCollection Views
        {
            get
            {
                return views ?? (new ViewsCollection(ItemMetadataCollection, x => true));
            }
        }

        public virtual IViewsCollection ActiveViews
        {
            get
            {
                if (views == null)
                {
                    views = new ViewsCollection(ItemMetadataCollection, x => true);
                }

                if (activeViews == null)
                {
                    activeViews = new ViewsCollection(ItemMetadataCollection, x => x.IsActive);
                }

                return activeViews;
            }
        }

        public IRegionManager RegionManager
        {
            get
            {
                return regionManager;
            }

            set
            {
                if (regionManager != value)
                {
                    regionManager = value;
                    OnPropertyChanged("RegionManager");
                }
            }
        }

        protected virtual ObservableCollection<ItemMetadata> ItemMetadataCollection
        {
            get
            {
                return itemMetadataCollection ?? (itemMetadataCollection = new ObservableCollection<ItemMetadata>());
            }
        }

        public IRegionManager Add(object view)
        {
            return Add(view, null, false);
        }

        public IRegionManager Add(object view, string viewName)
        {
            return Add(view, viewName, false);
        }

        public virtual IRegionManager Add(object view, string viewName, bool createRegionManagerScope)
        {
            IRegionManager manager = createRegionManagerScope ? this.RegionManager.CreateRegionManager() : this.RegionManager;
            InnerAdd(view, viewName, manager);
            return manager;
        }

        public virtual void Remove(object view)
        {
            ItemMetadata itemMetadata = this.GetItemMetadata(view);

            ItemMetadataCollection.Remove(itemMetadata);

            DependencyObject dependencyObject = view as DependencyObject;
            if (dependencyObject != null && INV.Elearning.Core.Architecture.Regions.RegionManager.GetRegionManager(dependencyObject) == this.RegionManager)
            {
                dependencyObject.ClearValue(INV.Elearning.Core.Architecture.Regions.RegionManager.RegionManagerProperty);
            }
        }

        public void RemoveAll()
        {
            foreach (var view in Views)
            {
                Remove(view);
            }
        }

        public virtual void Activate(object view)
        {
            ItemMetadata itemMetadata = this.GetItemMetadata(view);

            if (itemMetadata?.IsActive == false)
            {
                itemMetadata.IsActive = true;
            }
        }

        public virtual void Deactivate(object view)
        {
            ItemMetadata itemMetadata = this.GetItemMetadata(view);

            if (itemMetadata?.IsActive == true)
            {
                itemMetadata.IsActive = false;
            }
        }

        public virtual object GetView(string viewName)
        {
            ItemMetadata metadata = this.ItemMetadataCollection.FirstOrDefault(x => x.Name == viewName);

            if (metadata != null)
            {
                return metadata.Item;
            }

            return null;
        }


        private void InnerAdd(object view, string viewName, IRegionManager scopedRegionManager)
        {
            ItemMetadata itemMetadata = new ItemMetadata(view);
            if (!string.IsNullOrEmpty(viewName))
            {
                itemMetadata.Name = viewName;
            }

            if (view is DependencyObject dependencyObject)
            {
                INV.Elearning.Core.Architecture.Regions.RegionManager.SetRegionManager(dependencyObject, scopedRegionManager);
            }

            ItemMetadataCollection.Add(itemMetadata);
        }

        private ItemMetadata GetItemMetadata(object view)
        {
            ItemMetadata itemMetadata = ItemMetadataCollection.FirstOrDefault(x => x.Item == view);

            return itemMetadata;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

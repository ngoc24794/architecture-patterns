﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  AutoPopulateRegionBehavior.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Collections.Generic;

namespace INV.Elearning.Core.Architecture.Regions
{
    public class AutoPopulateRegionBehavior : RegionBehavior
    {
        public const string BehaviorKey = "AutoPopulate";

        private readonly IRegionViewRegistry regionViewRegistry;

        public AutoPopulateRegionBehavior(IRegionViewRegistry regionViewRegistry)
        {
            this.regionViewRegistry = regionViewRegistry;
        }

        protected override void OnAttach()
        {
            if (string.IsNullOrEmpty(this.Region.Name))
            {
                Region.PropertyChanged += Region_PropertyChanged;
            }
            else
            {
                StartPopulatingContent();
            }
        }

        private void StartPopulatingContent()
        {
            foreach (object view in CreateViewsToAutoPopulate())
            {
                AddViewIntoRegion(view);
            }

            regionViewRegistry.ContentRegistered += OnViewRegistered;
        }

        protected virtual IEnumerable<object> CreateViewsToAutoPopulate()
        {
            return regionViewRegistry.GetContents(Region.Name);
        }

        protected virtual void AddViewIntoRegion(object viewToAdd)
        {
            Region.Add(viewToAdd);
        }

        private void Region_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name" && !string.IsNullOrEmpty(Region.Name))
            {
                Region.PropertyChanged -= this.Region_PropertyChanged;
                StartPopulatingContent();
            }
        }

        public virtual void OnViewRegistered(object sender, ViewRegisteredEventArgs e)
        {
            if (e.RegionName == Region.Name)
            {
               AddViewIntoRegion(e.GetView());
            }
        }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace INV.Elearning.Core.Architecture.Regions.Regions.Behaviors
{
    public class RibbonRegionBehavior : RegionBehavior
    {
        protected override void OnAttach()
        {
            Region.Views.CollectionChanged += (s, e) =>
              {
                  switch (e.Action)
                  {
                      case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                          foreach (var item in e.NewItems)
                          {
                              // TODO: 
                          }
                          break;
                      case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                          break;
                  }
              };
        }

        private IEnumerable<T> GetCustomAttributes<T>(Type type)
        {
            return type.GetCustomAttributes(typeof(T), true).OfType<T>();
        }
    }
}

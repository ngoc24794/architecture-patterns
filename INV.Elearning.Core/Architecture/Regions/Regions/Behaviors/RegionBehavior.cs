﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RegionBehavior.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace INV.Elearning.Core.Architecture.Regions
{
    public abstract class RegionBehavior : IRegionBehavior
    {
        private IRegion region;
        public IRegion Region
        {
            get
            {
                return region;
            }
            set
            {
                if (!IsAttached)
                {
                    region = value;
                }
            }
        }

        public bool IsAttached { get; private set; }

        public void Attach()
        {
            IsAttached = true;
            OnAttach();
        }

        protected abstract void OnAttach();
    }
}

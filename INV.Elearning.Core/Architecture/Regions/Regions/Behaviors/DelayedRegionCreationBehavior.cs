using System;
using System.Windows;

namespace INV.Elearning.Core.Architecture.Regions.Behaviors
{
    public class DelayedRegionCreationBehavior
    {
        private readonly RegionAdapterMappings regionAdapterMappings;
        private WeakReference elementWeakReference;
        private bool regionCreated;

        public DelayedRegionCreationBehavior(RegionAdapterMappings regionAdapterMappings)
        {
            this.regionAdapterMappings = regionAdapterMappings;
        }
        public DependencyObject TargetElement
        {
            get { return this.elementWeakReference != null ? this.elementWeakReference.Target as DependencyObject : null; }
            set { this.elementWeakReference = new WeakReference(value); }
        }

        public void Attach()
        {
            RegionManager.UpdatingRegions += this.OnUpdatingRegions;
            this.WireUpTargetElement();
        }

        public void Detach()
        {
            RegionManager.UpdatingRegions -= this.OnUpdatingRegions;
            this.UnWireTargetElement();
        }

        public void OnUpdatingRegions(object sender, EventArgs e)
        {
            this.TryCreateRegion();
        }

        private void TryCreateRegion()
        {
            DependencyObject targetElement = this.TargetElement;
            if (targetElement == null)
            {
                this.Detach();
                return;
            }

            if (targetElement.CheckAccess())
            {
                this.Detach();

                if (!this.regionCreated)
                {
                    string regionName = RegionManager.GetRegionName(targetElement);
                    CreateRegion(targetElement, regionName);
                    this.regionCreated = true;
                }
            }
        }

        protected virtual IRegion CreateRegion(DependencyObject targetElement, string regionName)
        {
            IRegionAdapter regionAdapter = this.regionAdapterMappings.GetMapping(targetElement.GetType());
            IRegion region = regionAdapter.Initialize(targetElement, regionName);

            return region;
        }

        private void ElementLoaded(object sender, RoutedEventArgs e)
        {
            this.UnWireTargetElement();
            this.TryCreateRegion();
        }

        private void WireUpTargetElement()
        {
            if (this.TargetElement is FrameworkElement element)
            {
                element.Loaded += this.ElementLoaded;
                return;
            }

            if (this.TargetElement is FrameworkContentElement fcElement)
            {
                fcElement.Loaded += this.ElementLoaded;
                return;
            }
        }

        private void UnWireTargetElement()
        {
            if (this.TargetElement is FrameworkElement element)
            {
                element.Loaded -= this.ElementLoaded;
                return;
            }

            if (this.TargetElement is FrameworkContentElement fcElement)
            {
                fcElement.Loaded -= this.ElementLoaded;
                return;
            }
        }
    }
}
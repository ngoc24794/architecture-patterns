namespace INV.Elearning.Core.Architecture.Regions
{
    public class AllActiveRegion : Region
    {
        public override IViewsCollection ActiveViews
        {
            get { return Views; }
        }

        public override void Deactivate(object view)
        {
        }
    }
}
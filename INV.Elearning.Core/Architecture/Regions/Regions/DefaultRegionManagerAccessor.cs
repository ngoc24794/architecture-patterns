

using System;
using System.Windows;

namespace INV.Elearning.Core.Architecture.Regions
{
    internal class DefaultRegionManagerAccessor : IRegionManagerAccessor
    {
        public event EventHandler UpdatingRegions
        {
            add { RegionManager.UpdatingRegions += value; }
            remove { RegionManager.UpdatingRegions -= value; }
        }

        public string GetRegionName(DependencyObject element)
        {
            return element.GetValue(RegionManager.RegionNameProperty) as string;
        }

        public IRegionManager GetRegionManager(DependencyObject element)
        {
            return element.GetValue(RegionManager.RegionManagerProperty) as IRegionManager;
        }
    }
}
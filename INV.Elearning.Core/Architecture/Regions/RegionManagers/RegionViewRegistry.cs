

using INV.Elearning.Core.Architecture.IoC;
using System;
using System.Collections.Generic;

namespace INV.Elearning.Core.Architecture.Regions
{
    public class RegionViewRegistry : IRegionViewRegistry
    {
        private readonly IServiceLocator locator;
        private readonly ListDictionary<string, Func<object>> registeredContent = new ListDictionary<string, Func<object>>();
        private readonly WeakDelegatesManager contentRegisteredListeners = new WeakDelegatesManager();

        public RegionViewRegistry(IServiceLocator locator)
        {
            this.locator = locator;
        }

        public event EventHandler<ViewRegisteredEventArgs> ContentRegistered
        {
            add { this.contentRegisteredListeners.AddListener(value); }
            remove { this.contentRegisteredListeners.RemoveListener(value); }
        }
        public IEnumerable<object> GetContents(string regionName)
        {
            List<object> items = new List<object>();
            foreach (Func<object> getContentDelegate in this.registeredContent[regionName])
            {
                items.Add(getContentDelegate());
            }

            return items;
        }

        public void RegisterViewWithRegion(string regionName, Type viewType)
        {
            this.RegisterViewWithRegion(regionName, () => this.CreateInstance(viewType));
        }

        public void RegisterViewWithRegion(string regionName, Func<object> getContentDelegate)
        {
            this.registeredContent.Add(regionName, getContentDelegate);
            this.OnContentRegistered(new ViewRegisteredEventArgs(regionName, getContentDelegate));
        }

        protected virtual object CreateInstance(Type type)
        {
            return this.locator.Resolve(type);
        }

        private void OnContentRegistered(ViewRegisteredEventArgs e)
        {
            this.contentRegisteredListeners.Raise(this, e);
        }
    }
}
﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ViewRegisteredEventArgs.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;

namespace INV.Elearning.Core.Architecture.Regions
{
    public class ViewRegisteredEventArgs : EventArgs
    {
        public ViewRegisteredEventArgs(string regionName, Func<object> getViewDelegate)
        {
            this.GetView = getViewDelegate;
            this.RegionName = regionName;
        }

        public string RegionName { get; private set; }

        public Func<object> GetView { get; private set; }
    }
}

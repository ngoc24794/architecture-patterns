﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  Bootstrapper.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Controls;
using INV.Elearning.Core.Architecture.EventAggregators;
using INV.Elearning.Core.Architecture.IoC;
using INV.Elearning.Core.Architecture.Localization;
using INV.Elearning.Core.Architecture.Modularity;
using INV.Elearning.Core.Architecture.Regions;
using INV.Elearning.Core.Architecture.Regions.Behaviors;
using log4net;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace INV.Elearning.Core.Architecture
{
    /// <summary>
    /// Thực hiện chức năng cấu hình hệ thống cho ứng dụng.
    /// </summary>
    public abstract class Bootstrapper
    {
        protected IContainer Container { get; set; }

        protected ILog Logger { get; set; }

        /// <summary>
        /// Lấy hoặc đặt Danh mục mô đun
        /// </summary>
        protected IModuleCatalog ModuleCatalog { get; set; }

        /// <summary>
        /// Lấy hoặc đặt Cửa sổ chính của ứng dụng
        /// </summary>
        protected DependencyObject Shell { get; set; }

        /// <summary>
        /// Tạo kho chứa <see cref="IAvinaContainer"/> được sử dụng bởi ứng dụng.
        /// </summary>
        /// <returns>Kho chứa <see cref="IAvinaContainer"/></returns>
        protected virtual IContainer CreateContainer()
        {
            IContainer container = new Container();
            container.RegisterInstance(container);
            return container;
        }

        /// <summary>
        /// Tạo đối tượng ghi log
        /// </summary>
        /// <returns>Đối tượng log</returns>
        protected virtual ILog CreateLogger() => LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Chạy tiến trình cấu hình hệ thống.
        /// </summary>
        public void Run()
        {
            Logger = CreateLogger();

            Logger.Info(Properties.Resources.LoggerCreatedSuccessfully);

            ModuleCatalog = CreateModuleCatalog();

            if (ModuleCatalog == null)
            {
                Logger.Error(Properties.Resources.NullModuleCatalogException);
                return;
            }

            Logger.Info(Properties.Resources.ConfiguringModuleCatalog);
            ConfigureModuleCatalog();

            Logger.Info(Properties.Resources.CreatingUnityContainer);
            Container = CreateContainer();

            if (Container == null)
            {
                Logger.Error(Properties.Resources.NullUnityContainerException);
                return;
            }

            Logger.Info(Properties.Resources.ConfiguringUnityContainer);
            ConfigureContainer();

            Logger.Info(Properties.Resources.ConfiguringServiceLocatorSingleton);
            ConfigureServiceLocator();

            Logger.Info(Properties.Resources.ConfiguringRegionAdapters);
            ConfigureRegionAdapterMappings();

            Logger.Info(Properties.Resources.ConfiguringDefaultRegionBehaviors);
            ConfigureDefaultRegionBehaviors();

            Logger.Info(Properties.Resources.CreatingShell);
            Shell = CreateShell();
            if (Shell != null)
            {

                Logger.Info(Properties.Resources.SettingTheRegionManager);
                RegionManager.SetRegionManager(this.Shell, this.Container.Resolve<IRegionManager>());

                Logger.Info(Properties.Resources.UpdatingRegions);
                RegionManager.UpdateRegions();

                Logger.Info(Properties.Resources.InitializingShell);
                InitializeShell();
            }

            if (Container.IsTypeRegistered(typeof(IModuleManager)))
            {
                Logger.Info(Properties.Resources.InitializingModules);
                InitializeModules();
            }

            Logger.Info(Properties.Resources.InitializeLocalization);
            InitializeLocalization();

            Logger.Info(Properties.Resources.BootstrapperSequenceCompleted);
        }

        protected virtual void InitializeLocalization()
        {
            LocalizationScope.SetResourceManager(Shell, new System.Resources.ResourceManager("Elearning_2018.App.Properties.Resources", Assembly.GetEntryAssembly()));
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            LocalizationManager.UpdateValues();
        }

        /// <summary>
        /// Tạo mới một danh mục mô đun <see cref="IModuleCatalog"/>
        /// </summary>
        /// <returns></returns>
        protected virtual IModuleCatalog CreateModuleCatalog() => new ModuleCatalog();

        /// Cấu hình LocatorProvider for <see cref="IServiceLocator"/>
        /// </summary>
        protected virtual void ConfigureServiceLocator()
        {
            Container.RegisterSingleton<IServiceLocator, ServiceLocator>();
            Container.Resolve<IServiceLocator>();
        }

        /// <summary>
        /// Khởi tạo các mô đun
        /// </summary>
        protected void InitializeModules()
        {
            IModuleManager manager;

            manager = this.Container.Resolve<IModuleManager>();

            manager.Run();
        }

        /// <summary>
        /// Cấu hình danh mục mô đun
        /// </summary>
        protected virtual void ConfigureModuleCatalog()
        {
            ModuleCatalog.Initialize();
        }

        /// <summary>
        /// Cấu hình kho chứa
        /// </summary>
        protected virtual void ConfigureContainer()
        {
            Container.RegisterInstance(Logger);
            Container.RegisterInstance(ModuleCatalog);
            
            Container.RegisterSingleton<IServiceLocator,ServiceLocator>();
            Container.RegisterSingleton<IModuleInitializer,ModuleInitializer>();
            Container.RegisterSingleton<IModuleManager,ModuleManager>();
            Container.RegisterSingleton<IModuleManager,ModuleManager>();
            Container.RegisterSingleton<RegionAdapterMappings,RegionAdapterMappings>();
            Container.RegisterSingleton<IRegionManager,RegionManager>();
            Container.RegisterSingleton<IEventAggregator,EventAggregator>();
            Container.RegisterSingleton<IRegionViewRegistry,RegionViewRegistry>();
            Container.RegisterSingleton<IRegionBehaviorFactory,RegionBehaviorFactory>();
        }

        /// <summary>
        /// Cấu hình các ánh xạ vùng mặc định được sử dụng trong ứng dụng 
        /// nhằm kết nối các điều khiển trên giao diện người dùng (XAML) 
        /// với một vùng <see cref="IRegion"/> và hiển thị nó trên giao diện.
        /// Phương thức này có thể được ghi đè bởi các lớp dẫn xuất nhằm chỉ
        /// định các ánh xạ khác.
        /// </summary>
        /// <returns></returns>
        protected virtual RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            RegionAdapterMappings regionAdapterMappings = Container.Resolve<RegionAdapterMappings>();
            if (regionAdapterMappings != null)
            {
                regionAdapterMappings.RegisterMapping(typeof(Selector), Container.Resolve<SelectorRegionAdapter>());
                regionAdapterMappings.RegisterMapping(typeof(ItemsControl), Container.Resolve<ItemsControlRegionAdapter>());
                regionAdapterMappings.RegisterMapping(typeof(ContentControl), Container.Resolve<ContentControlRegionAdapter>());
                regionAdapterMappings.RegisterMapping(typeof(Ribbon), Container.Resolve<RibbonRegionAdapter>());
            }

            return regionAdapterMappings;
        }

        /// <summary>
        /// Cấu hình <see cref="IRegionBehaviorFactory"/>: 
        /// Liệt kê danh sách các hành vi mặc định sẽ được thêm vào một <see cref="IRegion"/>
        /// </summary>
        /// <returns></returns>
        protected virtual IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
        {
            var factory = Container.Resolve<IRegionBehaviorFactory>();

            if (factory != null)
            {
                factory.AddIfMissing(AutoPopulateRegionBehavior.BehaviorKey,
                                                  typeof(AutoPopulateRegionBehavior));
                factory.AddIfMissing(RegionManagerRegistrationBehavior.BehaviorKey,
                                                  typeof(RegionManagerRegistrationBehavior));
            }

            return factory;
        }

        /// <summary>
        /// Tạo cửa sổ chính cho ứng dụng.
        /// </summary>
        /// <returns></returns>
        protected abstract DependencyObject CreateShell();

        /// <summary>
        /// Khởi tạo cửa sổ chính cho ứng dụng.
        /// </summary>
        protected abstract void InitializeShell();
    }
}

﻿using System.Collections.ObjectModel;

/// <summary>
/// Đối tượng chứa các đối tượng khác
/// </summary>
namespace INV.Elearning.Core.View
{
    public interface IGroupContainer
    {
        ObservableCollection<ObjectElement> Elements { get; }
    }
}

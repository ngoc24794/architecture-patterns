﻿

using enums=INV.Elearning.Controls.Enums;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Khung hỗ trợ đường viền
    /// </summary>
    public interface IBorderSupport
    {
        /// <summary>
        /// Độ dày đường viền
        /// </summary>
        double Thickness { get; set; }
        /// <summary>
        /// Biểu vẽ bao đường kẻ
        /// </summary>
        PenLineCap CapType { get; set; }
        /// <summary>
        /// Biểu vẽ bao đường kẻ nét đứt
        /// </summary>
        PenLineJoin JoinType { get; set; }
        /// <summary>
        /// Kiểu nét đứt đường kẻ
        /// </summary>
        enums.DashType DashType { get; set; }
        /// <summary>
        /// Màu đường kẻ
        /// </summary>
        ColorBrushBase Stroke { get; set; }
        /// <summary>
        /// Màu nền khung
        /// </summary>
        ColorBrushBase Fill { set; get; }

        /// <summary>
        /// Cài đặt kiểu cho khung
        /// </summary>
        /// <param name="stroke">Màu viền</param>
        /// <param name="fill">Màu nền</param>
        void SetStyle(ColorBrushBase stroke, ColorBrushBase fill);
    }

    /// <summary>
    /// Lớp hỗ trợ đường viền mở rộng
    /// </summary>
    public interface IBorderSupportExt : IBorderSupport
    {
        void SetStyle(params object[] args);
    }
}

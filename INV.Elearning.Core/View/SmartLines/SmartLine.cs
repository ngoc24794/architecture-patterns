﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  SmartLine.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    internal struct SmartLine
    {
        public SmartLine(Point point1, Point point2, SmartLineType type) : this()
        {
            Point1 = point1;
            Point2 = point2;
            Type = type;
        }
        public Point Point1 { get; set; }
        public Point Point2 { get; set; }
        public Vector Offset { get; set; }
        public SmartLineType Type { get; set; }
        public LineGeometry GetSmartLine()
        {
            LineGeometry lineGeometry = new LineGeometry();
            Point
                point1 = new Point(Math.Min(Point1.X, Point2.X), Math.Min(Point1.Y, Point2.Y)),
                point2 = new Point(Math.Max(Point1.X, Point2.X), Math.Max(Point1.Y, Point2.Y));
            switch (Type)
            {
                case SmartLineType.Top:
                case SmartLineType.Bottom:
                case SmartLineType.Middle:
                    lineGeometry.StartPoint = new Point(point1.X - DISTANCE_EXTENDED, point1.Y);
                    lineGeometry.EndPoint = new Point(point2.X + DISTANCE_EXTENDED, point2.Y);
                    break;
                case SmartLineType.Left:
                case SmartLineType.Right:
                case SmartLineType.Center:
                    lineGeometry.StartPoint = new Point(point1.X, point1.Y - DISTANCE_EXTENDED);
                    lineGeometry.EndPoint = new Point(point2.X, point2.Y + DISTANCE_EXTENDED);
                    break;
            }
            return lineGeometry;
        }

        private const double DISTANCE_EXTENDED = 20;

        public void Snap(ObjectElement source)
        {
            Vector offset = source.Rotate(Offset);

            switch (Type)
            {
                case SmartLineType.Top:
                case SmartLineType.Bottom:
                case SmartLineType.Middle:
                    source.Top += offset.Y;
                    break;
                case SmartLineType.Left:
                case SmartLineType.Right:
                case SmartLineType.Center:
                    source.Left += offset.X;
                    break;
            }
        }
    }

    internal enum SmartLineType
    {
        Top,
        Bottom,
        Left,
        Right,
        Center,
        Middle
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  Distribute.cs
**
** Description: Dữ liệu một phân bố
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    internal struct GuideLine
    {
        public GuideLine(Point point1, Point point2) : this()
        {
            Point1 = point1;
            Point2 = point2;
            if (Point1.X == Point2.X)
            {
                IsVericalLine = true;
            }
            if (Point1.Y == Point2.Y)
            {
                IsHorizontalLine = true;
            }
            if ((Point1.X != Point2.X) && (Point1.Y != Point2.Y))
            {
                IsHorizontalLine = IsVericalLine = false;
            }
        }

        public Point Point1 { get; set; }
        public Point Point2 { get; set; }
        public bool IsHorizontalLine { get; private set; }
        public bool IsVericalLine { get; private set; }
        public LineGeometry GetGeometry()
        {
            return new LineGeometry(Point1, Point2);
        }
    }
    /// <summary>
    /// Phân bố
    /// </summary>
    internal struct Distribute
    {
        private GuideLine _line1;
        private GuideLine _line2;

        public Distribute(GuideLine line1, GuideLine line2) : this()
        {
            Line1 = line1;
            Line2 = line2;
            IsHorizontalDistribute = line1.IsHorizontalLine && line2.IsHorizontalLine;
            IsVerticalDistribute = line1.IsVericalLine && line2.IsVericalLine;
            if (IsHorizontalDistribute)
            {
                if (line2.Point1.Y < line1.Point1.Y)
                {
                    GuideLine guideLine = _line1;
                    _line1 = _line2;
                    _line2 = guideLine;
                }

            }
            if (IsVerticalDistribute)
            {
                if (line1.Point1.X > line2.Point1.X)
                {
                    GuideLine guideLine = _line1;
                    _line1 = _line2;
                    _line2 = guideLine;
                }
            }
            Length = ((Vector)Line1.Point2 - (Vector)Line2.Point2).Length;
        }

        public GuideLine Line1 { get => _line1; set => _line1 = value; }
        public GuideLine Line2 { get => _line2; set => _line2 = value; }
        public double Length { get; private set; }
        public bool IsHorizontalDistribute { get; private set; }
        public bool IsVerticalDistribute { get; private set; }
        /// <summary>
        /// Lấy khoảng cách từ rect đến lề trái của phân bố này
        /// </summary>
        /// <param name="rect"></param>
        /// <returns>
        /// NaN: IsHorizontalDistribute = false
        /// Số dương: rect ở hoàn toàn bên TRÁI của phân bố
        /// Số âm: ngược lại
        /// </returns>
        public double GetLeftDistanceTo(Rect rect)
        {
            if (IsHorizontalDistribute)
            {
                return Line1.Point1.X - rect.Right;
            }
            return double.NaN;
        }
        /// <summary>
        /// Lấy khoảng cách từ rect đến lề phải của phân bố này
        /// </summary>
        /// <param name="rect"></param>
        /// <returns>
        /// NaN: IsHorizontalDistribute = false
        /// Số dương: rect ở hoàn toàn bên PHẢI của phân bố
        /// Số âm: ngược lại
        /// </returns>
        public double GetRightDistanceTo(Rect rect)
        {
            if (IsHorizontalDistribute)
            {
                return rect.Left - Line2.Point1.X;
            }
            return double.NaN;
        }
        /// <summary>
        /// Lấy khoảng cách từ rect đến lề TRÊN của phân bố này
        /// </summary>
        /// <param name="rect"></param>
        /// <returns>
        /// NaN: IsVerticalDistribute = false
        /// Số dương: rect ở hoàn toàn bên TRÊN của phân bố
        /// Số âm: ngược lại
        /// </returns>
        public double GetTopDistanceTo(Rect rect)
        {
            if (IsVerticalDistribute)
            {
                return Line1.Point1.Y - rect.Bottom;
            }
            return double.NaN;
        }
        /// <summary>
        /// Lấy khoảng cách từ rect đến lề DƯỚI của phân bố này
        /// </summary>
        /// <param name="rect"></param>
        /// <returns>
        /// NaN: IsVerticalDistribute = false
        /// Số dương: rect ở hoàn toàn bên DƯỚI của phân bố
        /// Số âm: ngược lại
        /// </returns>
        public double GetBottomDistanceTo(Rect rect)
        {
            if (IsVerticalDistribute)
            {
                return rect.Top - Line2.Point2.Y;
            }
            return double.NaN;
        }
        public GeometryGroup GetGeometry()
        {
            GeometryGroup geometryGroup = new GeometryGroup();
            geometryGroup.Children.Add(Line1.GetGeometry());
            geometryGroup.Children.Add(Line2.GetGeometry());
            GeometryGroup arrows = new GeometryGroup();
            arrows.Children.Add(new LineGeometry(Line1.Point2, Line2.Point2));
            if (IsHorizontalDistribute)
            {
                PathGeometry upArrow = GetArrow(Line1.Point2);
                PathGeometry downArrow = GetArrow(Line2.Point2);
                downArrow.Transform = new RotateTransform(180, Line2.Point2.X, Line2.Point2.Y);
                arrows.Children.Add(upArrow);
                arrows.Children.Add(downArrow);
            }
            if (IsVerticalDistribute)
            {
                PathGeometry leftArrow = GetArrow(Line1.Point2);
                PathGeometry rightArrow = GetArrow(Line2.Point2);
                leftArrow.Transform = new RotateTransform(90, Line1.Point2.X, Line1.Point2.Y);
                rightArrow.Transform = new RotateTransform(-90, Line2.Point2.X, Line2.Point2.Y);
                arrows.Children.Add(leftArrow);
                arrows.Children.Add(rightArrow);
            }
            geometryGroup.Children.Add(arrows);
            return geometryGroup;
        }

        private PathGeometry GetArrow(Point StartPoint)
        {
            return new PathGeometry(new PathFigureCollection()
                    {
                        new PathFigure(StartPoint, new PathSegmentCollection(){
                            new PolyLineSegment(new PointCollection(){
                                (Vector)StartPoint + new Point(3, 5),
                                (Vector)StartPoint + new Point(-3, 5)
                            }, false)
                        }, true)
                    });
        }

        internal bool TrySnap(Rect rect)
        {

            return false;
        }
    }
}

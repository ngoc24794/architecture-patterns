﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  SmartLineAdorner.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using INV.Elearning.Core.Helper;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Media;
using System.Linq;

namespace INV.Elearning.Core.View
{
    public enum ThumbType
    {
        Left,
        Right,
        Top,
        Bottom,
    }
    public class EDragDeltaEventArgs
    {
        public EDragDeltaEventArgs(double horizontalChange, double verticalChange, Thumb thumb)
        {
            HorizontalChange = horizontalChange;
            VerticalChange = verticalChange;
            Thumb = thumb;
        }

        public EDragDeltaEventArgs(double horizontalChange, double verticalChange, Thumb thumb, ThumbType thumbType)
        {
            HorizontalChange = horizontalChange;
            VerticalChange = verticalChange;
            Thumb = thumb;
            ThumbType = thumbType;
        }

        public double HorizontalChange { get; set; }
        public double VerticalChange { get; set; }
        public Thumb Thumb { get; set; }
        public ThumbType ThumbType { get; set; }
    }
    public delegate void DragDeltaEventHanlder(object sender, EDragDeltaEventArgs e);
    public delegate void DragCompletedEventHanlder(object sender, DragCompletedEventArgs e);
    public class SmartLineAdorner : Adorner
    {
        public event DragDeltaEventHanlder Moving;
        public event DragDeltaEventHanlder Resizing;
        public event DragCompletedEventHandler DragCompleted;
        public GeometryGroup Geometry { get; set; }
        public SmartLineAdorner(UIElement adornedElement) : base(adornedElement)
        {
            Moving += SmartLineAdorner_Moving;
            Resizing += SmartLineAdorner_Resizing;
            DragCompleted += SmartLineAdorner_DragCompleted;
            IsHitTestVisible = false;
        }


        private void SmartLineAdorner_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            if (Geometry != null)
            {
                Geometry.Children.Clear();
                InvalidateVisual();
            }
        }

        private void SmartLineAdorner_Resizing(object sender, EDragDeltaEventArgs e)
        {
            if (sender is ObjectElement element)
            {
                Vector offsetVector = new Vector(e.HorizontalChange, e.VerticalChange);
                Rect rect = Rect.Offset(element.RectBound, offsetVector);
                Geometry = new GeometryGroup();

                List<Distribute> distributes = new List<Distribute>();
                List<SmartLine> smartLines = new List<SmartLine>();

                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements != null)
                {
                    //---------------------------------------------------------------------------
                    // Align
                    //---------------------------------------------------------------------------
                    smartLines = Align(offsetVector, element, (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements);
                    //---------------------------------------------------------------------------
                    // Distribute spacing
                    //---------------------------------------------------------------------------
                    //distributes = Distribute(offsetVector, element, (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements);
                }

                foreach (SmartLine smartLine in smartLines)
                {
                    Vector offset = element.Rotate(smartLine.Offset);
                    if (e.Thumb is ResizeThumb resizeThumb)
                    {
                        switch (e.ThumbType)
                        {
                            case ThumbType.Left:
                                if (resizeThumb.LimitDeltaX == 0.0 && (smartLine.Type == SmartLineType.Left || (element.IsScaleX && smartLine.Type == SmartLineType.Right)))
                                {
                                    Geometry.Children.Add(smartLine.GetSmartLine());
                                    element.AddWidthDelta((element.IsScaleX ? -1 : 1) * offset.X, element.IsScaleX, element.Width, true);
                                    resizeThumb.LimitDeltaX = DELTA;
                                }
                                break;
                            case ThumbType.Right:
                                if (resizeThumb.LimitDeltaX == 0.0 && (smartLine.Type == SmartLineType.Right || (element.IsScaleX && smartLine.Type == SmartLineType.Left)))
                                {
                                    Geometry.Children.Add(smartLine.GetSmartLine());
                                    element.AddWidthDelta((element.IsScaleY ? 1 : -1) * offset.X, element.IsScaleX, element.Width, true);
                                    resizeThumb.LimitDeltaX = DELTA;
                                }
                                break;
                            case ThumbType.Top:
                                if (resizeThumb.LimitDeltaY == 0.0 && (smartLine.Type == SmartLineType.Top || (element.IsScaleY && smartLine.Type == SmartLineType.Bottom)))
                                {
                                    Geometry.Children.Add(smartLine.GetSmartLine());
                                    element.AddHeightDelta((element.IsScaleY ? -1 : 1) * offset.Y, element.IsScaleY, element.Height, true);
                                    resizeThumb.LimitDeltaY = DELTA;
                                }
                                break;
                            case ThumbType.Bottom:
                                if (resizeThumb.LimitDeltaY == 0.0 && (smartLine.Type == SmartLineType.Bottom || (element.IsScaleY && smartLine.Type == SmartLineType.Top)))
                                {
                                    Geometry.Children.Add(smartLine.GetSmartLine());
                                    element.AddHeightDelta((element.IsScaleY ? -1 : 1) * offset.Y, element.IsScaleY, element.Height, true);
                                    resizeThumb.LimitDeltaY = DELTA;
                                }
                                break;
                        }
                    }
                }

                //foreach (Distribute item in distributes)
                //{
                //    GeometryGroup geometryGroup = new GeometryGroup();
                //    geometryGroup.Children.Add(item.Line1.GetGeometry());
                //    geometryGroup.Children.Add(item.Line2.GetGeometry());
                //    Geometry.Children.Add(geometryGroup);
                //}
                InvalidateVisual();
            }
        }

        private void SmartLineAdorner_Moving(object sender, EDragDeltaEventArgs e)
        {
            if (sender is ObjectElement element)
            {
                Vector offsetVector = new Vector(e.HorizontalChange, e.VerticalChange);
                Geometry = new GeometryGroup();

                List<Distribute> distributes = new List<Distribute>();
                List<SmartLine> smartLines = new List<SmartLine>();

                if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements != null)
                {
                    //---------------------------------------------------------------------------
                    // Align
                    //---------------------------------------------------------------------------
                    smartLines = Align(offsetVector, element, (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements);
                    //---------------------------------------------------------------------------
                    // Distribute spacing
                    //---------------------------------------------------------------------------
                    distributes = Distribute(offsetVector, element, (Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements);
                }

                foreach (SmartLine smartLine in smartLines)
                {
                    Geometry.Children.Add(smartLine.GetSmartLine());
                    if (e.Thumb is MoveThumb moveThumb)
                    {
                        switch (smartLine.Type)
                        {
                            case SmartLineType.Top:
                            case SmartLineType.Bottom:
                            case SmartLineType.Middle:
                                if (moveThumb.LimitDeltaY == 0.0)
                                {
                                    smartLine.Snap(element);
                                }
                                moveThumb.LimitDeltaY = DELTA; break;
                            case SmartLineType.Left:
                            case SmartLineType.Right:
                            case SmartLineType.Center:
                                if (moveThumb.LimitDeltaX == 0.0)
                                {
                                    smartLine.Snap(element);
                                }
                                moveThumb.LimitDeltaX = DELTA;
                                break;
                        }

                    }
                }

                foreach (Distribute item in distributes)
                {
                    Geometry.Children.Add(item.GetGeometry());
                }
                InvalidateVisual();
            }
        }
        private List<SmartLine> Align(Vector vector, ObjectElement source, IEnumerable<ObjectElement> elements)
        {
            Rect rect = Rect.Offset(source.RectBound, vector);
            double
                left = rect.Left, standardLeft = left,
                top = rect.Top, standardTop = top,
                right = rect.Right, standardRight = right,
                bottom = rect.Bottom, standardBottom = bottom,
                center = (left + right) / 2, standardCenter = center,
                middle = (top + bottom) / 2, standardMiddle = middle,
                horizontalChange = vector.X,
                verticalChange = vector.Y,
                documentWidth = (Application.Current as IAppGlobal).DocumentControl.ActualWidth,
                documentHeight = (Application.Current as IAppGlobal).DocumentControl.ActualHeight,
                leftSlide = 0,
                centerSlide = documentWidth / 2,
                rightSlide = documentWidth,
                topSlide = 0,
                middleSlide = documentHeight / 2,
                bottomSlide = documentHeight;
            bool
                standardLeftFounded = false,
                standardRightFounded = false,
                standardCenterFounded = false,
                standardTopFounded = false,
                standardBottomFounded = false,
                standardMiddleFounded = false;

            List<SmartLine> smartLines = new List<SmartLine>();
            SmartLine
                leftLine = new SmartLine(rect.TopLeft, rect.BottomLeft, SmartLineType.Left),
                rightLine = new SmartLine(rect.TopRight, rect.BottomRight, SmartLineType.Right),
                centerLine = new SmartLine(new Point(center, top), new Point(center, bottom), SmartLineType.Center),
                topLine = new SmartLine(rect.TopLeft, rect.TopRight, SmartLineType.Top),
                bottomLine = new SmartLine(rect.BottomLeft, rect.BottomRight, SmartLineType.Bottom),
                middleLine = new SmartLine(new Point(left, middle), new Point(right, middle), SmartLineType.Middle),
                leftSlideLine = new SmartLine(new Point(0, 0), new Point(0, documentHeight), SmartLineType.Left),
                centerSlideLine = new SmartLine(new Point(documentWidth / 2, 0), new Point(documentWidth / 2, documentHeight), SmartLineType.Center),
                rightSlideLine = new SmartLine(new Point(documentWidth, 0), new Point(documentWidth, documentHeight), SmartLineType.Right),
                topSlideLine = new SmartLine(new Point(0, 0), new Point(documentWidth, 0), SmartLineType.Top),
                middleSlideLine = new SmartLine(new Point(0, documentHeight / 2), new Point(documentWidth, documentHeight / 2), SmartLineType.Middle),
                bottomSlideLine = new SmartLine(new Point(0, documentHeight), new Point(documentWidth, documentHeight), SmartLineType.Bottom);


            //---------------------------------------------------------------------------
            // Gióng theo các đối tượng khác
            //---------------------------------------------------------------------------
            foreach (ObjectElement element in elements)
            {
                if (!element.IsSelected && element != source && MoveThumb.CheckRoot(element, source))
                {
                    Rect irect = element.RectBound;
                    double
                        ileft = irect.Left,
                        itop = irect.Top,
                        iright = irect.Right,
                        ibottom = irect.Bottom,
                        icenter = (ileft + iright) / 2,
                        imiddle = (itop + ibottom) / 2;

                    // left
                    if (horizontalChange * (standardLeft - ileft) >= 0 && Math.Abs(standardLeft - ileft) <= DELTA)
                    {
                        if (!standardLeftFounded)
                        {
                            standardLeft = ileft;
                            standardLeftFounded = true;
                        }
                        leftLine.Point1 = new Point(standardLeft, Math.Min(leftLine.Point1.Y, itop));
                        leftLine.Point2 = new Point(standardLeft, Math.Max(leftLine.Point2.Y, ibottom));
                    }
                    if (horizontalChange * (standardLeft - iright) >= 0 && Math.Abs(standardLeft - iright) <= DELTA)
                    {
                        if (!standardLeftFounded)
                        {
                            standardLeft = iright;
                            standardLeftFounded = true;
                        }
                        leftLine.Point1 = new Point(standardLeft, Math.Min(leftLine.Point1.Y, itop));
                        leftLine.Point2 = new Point(standardLeft, Math.Max(leftLine.Point2.Y, ibottom));
                    }
                    if (horizontalChange * (standardLeft - icenter) >= 0 && Math.Abs(standardLeft - icenter) <= DELTA)
                    {
                        if (!standardLeftFounded)
                        {
                            standardLeft = icenter;
                            standardLeftFounded = true;
                        }
                        leftLine.Point1 = new Point(standardLeft, Math.Min(leftLine.Point1.Y, itop));
                        leftLine.Point2 = new Point(standardLeft, Math.Max(leftLine.Point2.Y, ibottom));
                    }
                    // right
                    if (horizontalChange * (standardRight - ileft) >= 0 && Math.Abs(standardRight - ileft) <= DELTA)
                    {
                        if (!standardRightFounded)
                        {
                            standardRight = ileft;
                            standardRightFounded = true;
                        }
                        rightLine.Point1 = new Point(standardRight, Math.Min(rightLine.Point1.Y, itop));
                        rightLine.Point2 = new Point(standardRight, Math.Max(rightLine.Point2.Y, ibottom));
                    }
                    if (horizontalChange * (standardRight - iright) >= 0 && Math.Abs(standardRight - iright) <= DELTA)
                    {
                        if (!standardRightFounded)
                        {
                            standardRight = iright;
                            standardRightFounded = true;
                        }
                        rightLine.Point1 = new Point(standardRight, Math.Min(rightLine.Point1.Y, itop));
                        rightLine.Point2 = new Point(standardRight, Math.Max(rightLine.Point2.Y, ibottom));
                    }
                    if (horizontalChange * (standardRight - icenter) >= 0 && Math.Abs(standardRight - icenter) <= DELTA)
                    {
                        if (!standardRightFounded)
                        {
                            standardRight = icenter;
                            standardRightFounded = true;
                        }
                        rightLine.Point1 = new Point(standardRight, Math.Min(rightLine.Point1.Y, itop));
                        rightLine.Point2 = new Point(standardRight, Math.Max(rightLine.Point2.Y, ibottom));
                    }
                    // center                                    
                    if (horizontalChange * (standardCenter - icenter) >= 0 && Math.Abs(standardCenter - icenter) <= DELTA)
                    {
                        if (!standardCenterFounded)
                        {
                            standardCenter = icenter;
                            standardCenterFounded = true;
                        }
                        centerLine.Point1 = new Point(standardCenter, Math.Min(centerLine.Point1.Y, itop));
                        centerLine.Point2 = new Point(standardCenter, Math.Max(centerLine.Point2.Y, ibottom));
                    }
                    // top
                    if (verticalChange * (standardTop - itop) >= 0 && Math.Abs(standardTop - itop) <= DELTA)
                    {
                        if (!standardTopFounded)
                        {
                            standardTop = itop;
                            standardTopFounded = true;
                        }
                        topLine.Point1 = new Point(Math.Min(topLine.Point1.X, ileft), standardTop);
                        topLine.Point2 = new Point(Math.Max(topLine.Point2.X, iright), standardTop);
                    }
                    if (verticalChange * (standardTop - imiddle) >= 0 && Math.Abs(standardTop - imiddle) <= DELTA)
                    {
                        if (!standardTopFounded)
                        {
                            standardTop = imiddle;
                            standardTopFounded = true;
                        }
                        topLine.Point1 = new Point(Math.Min(topLine.Point1.X, ileft), standardTop);
                        topLine.Point2 = new Point(Math.Max(topLine.Point2.X, iright), standardTop);
                    }
                    if (verticalChange * (standardTop - ibottom) >= 0 && Math.Abs(standardTop - ibottom) <= DELTA)
                    {
                        if (!standardTopFounded)
                        {
                            standardTop = ibottom;
                            standardTopFounded = true;
                        }
                        topLine.Point1 = new Point(Math.Min(topLine.Point1.X, ileft), standardTop);
                        topLine.Point2 = new Point(Math.Max(topLine.Point2.X, iright), standardTop);
                    }
                    // bottom
                    if (verticalChange * (standardBottom - itop) >= 0 && Math.Abs(standardBottom - itop) <= DELTA)
                    {
                        if (!standardBottomFounded)
                        {
                            standardBottom = itop;
                            standardBottomFounded = true;
                        }
                        bottomLine.Point1 = new Point(Math.Min(bottomLine.Point1.X, ileft), standardBottom);
                        bottomLine.Point2 = new Point(Math.Max(bottomLine.Point2.X, iright), standardBottom);
                    }
                    if (verticalChange * (standardBottom - imiddle) >= 0 && Math.Abs(standardBottom - imiddle) <= DELTA)
                    {
                        if (!standardBottomFounded)
                        {
                            standardBottom = imiddle;
                            standardBottomFounded = true;
                        }
                        bottomLine.Point1 = new Point(Math.Min(bottomLine.Point1.X, ileft), standardBottom);
                        bottomLine.Point2 = new Point(Math.Max(bottomLine.Point2.X, iright), standardBottom);
                    }
                    if (verticalChange * (standardBottom - ibottom) >= 0 && Math.Abs(standardBottom - ibottom) <= DELTA)
                    {
                        if (!standardBottomFounded)
                        {
                            standardBottom = ibottom;
                            standardBottomFounded = true;
                        }
                        bottomLine.Point1 = new Point(Math.Min(bottomLine.Point1.X, ileft), standardBottom);
                        bottomLine.Point2 = new Point(Math.Max(bottomLine.Point2.X, iright), standardBottom);
                    }
                    // middle
                    if (verticalChange * (standardMiddle - imiddle) >= 0 && Math.Abs(standardMiddle - imiddle) <= DELTA)
                    {
                        if (!standardMiddleFounded)
                        {
                            standardMiddle = imiddle;
                            standardMiddleFounded = true;
                        }
                        middleLine.Point1 = new Point(Math.Min(middleLine.Point1.X, ileft), standardMiddle);
                        middleLine.Point2 = new Point(Math.Max(middleLine.Point2.X, iright), standardMiddle);
                    }
                }
            }

            if (standardLeftFounded)
            {
                leftLine.Offset = new Vector(standardLeft - left, 0);
                smartLines.Add(leftLine);
            }
            if (standardRightFounded)
            {
                rightLine.Offset = new Vector(standardRight - right, 0);
                smartLines.Add(rightLine);
            }
            if (standardCenterFounded)
            {
                centerLine.Offset = new Vector(standardCenter - center, 0);
                smartLines.Add(centerLine);
            }
            if (standardTopFounded)
            {
                topLine.Offset = new Vector(0, standardTop - top);
                smartLines.Add(topLine);
            }
            if (standardBottomFounded)
            {
                bottomLine.Offset = new Vector(0, standardBottom - bottom);
                smartLines.Add(bottomLine);
            }
            if (standardMiddleFounded)
            {
                middleLine.Offset = new Vector(0, standardMiddle - middle);
                smartLines.Add(middleLine);
            }


            //---------------------------------------------------------------------------
            // Gióng theo các lề của trang
            //---------------------------------------------------------------------------

            // Lề trái
            if (horizontalChange * (standardLeft - leftSlide) >= 0 && Math.Abs(standardLeft - leftSlide) <= DELTA)
            {
                smartLines.Add(leftSlideLine);
            }
            // Lề giữa
            if (horizontalChange * (standardCenter - centerSlide) >= 0 && Math.Abs(standardCenter - centerSlide) <= DELTA)
            {
                smartLines.Add(centerSlideLine);
            }
            // Lề phải
            if (horizontalChange * (standardRight - rightSlide) >= 0 && Math.Abs(standardRight - rightSlide) <= DELTA)
            {
                smartLines.Add(rightSlideLine);
            }
            // Lề trên
            if (verticalChange * (standardTop - topSlide) >= 0 && Math.Abs(standardTop - topSlide) <= DELTA)
            {
                smartLines.Add(topSlideLine);
            }
            // Lề trung
            if (verticalChange * (standardMiddle - middleSlide) >= 0 && Math.Abs(standardMiddle - middleSlide) <= DELTA)
            {
                smartLines.Add(middleSlideLine);
            }
            // Lề đáy
            if (verticalChange * (standardBottom - bottomSlide) >= 0 && Math.Abs(standardBottom - bottomSlide) <= DELTA)
            {
                smartLines.Add(bottomSlideLine);
            }


            //---------------------------------------------------------------------------
            // Gióng theo GuideLine
            //---------------------------------------------------------------------------
            if ((Application.Current as IAppGlobal).ShowConfig.IsGuideLineVisible && (Application.Current as IAppGlobal).ShowConfig.GuideLines != null)
            {
                foreach (EGuideLine guideLine in (Application.Current as IAppGlobal).ShowConfig.GuideLines)
                {
                    switch (guideLine.Orientation)
                    {
                        case System.Windows.Controls.Orientation.Horizontal:
                            double guideLineY = guideLine.MoveY + documentHeight / 2;
                            Point
                                leftPoint = new Point(0, guideLineY),
                                rightPoint = new Point(documentWidth, guideLineY);
                            // Lề trên
                            if (verticalChange * (standardTop - guideLineY) >= 0 && Math.Abs(standardTop - guideLineY) <= DELTA)
                            {
                                smartLines.Add(new SmartLine(leftPoint, rightPoint, SmartLineType.Top));
                            }
                            // Lề trung
                            if (verticalChange * (standardMiddle - guideLineY) >= 0 && Math.Abs(standardMiddle - guideLineY) <= DELTA)
                            {
                                smartLines.Add(new SmartLine(leftPoint, rightPoint, SmartLineType.Middle));
                            }
                            // Lề đáy
                            if (verticalChange * (standardBottom - guideLineY) >= 0 && Math.Abs(standardBottom - guideLineY) <= DELTA)
                            {
                                smartLines.Add(new SmartLine(leftPoint, rightPoint, SmartLineType.Bottom));
                            }
                            break;
                        case System.Windows.Controls.Orientation.Vertical:
                            double guideLineX = guideLine.MoveX + documentWidth / 2;
                            Point
                                topPoint = new Point(guideLineX, 0),
                                bottomPoint = new Point(guideLineX, documentHeight);
                            // Lề trái
                            if (horizontalChange * (standardLeft - guideLineX) >= 0 && Math.Abs(standardLeft - guideLineX) <= DELTA)
                            {
                                smartLines.Add(new SmartLine(topPoint, bottomPoint, SmartLineType.Left));
                            }
                            // Lề giữa
                            if (horizontalChange * (standardCenter - guideLineX) >= 0 && Math.Abs(standardCenter - guideLineX) <= DELTA)
                            {
                                smartLines.Add(new SmartLine(topPoint, bottomPoint, SmartLineType.Center));
                            }
                            // Lề phải
                            if (horizontalChange * (standardRight - guideLineX) >= 0 && Math.Abs(standardRight - guideLineX) <= DELTA)
                            {
                                smartLines.Add(new SmartLine(topPoint, bottomPoint, SmartLineType.Right));
                            }
                            break;
                    }
                }
            }

            return smartLines;
        }

        private List<Distribute> Distribute(Vector offsetVector, ObjectElement source, IEnumerable<ObjectElement> elements)
        {
            List<Distribute> distributes = new List<Distribute>();
            Rect rect = Rect.Offset(source.RectBound, offsetVector);

            double
                top = rect.Top,
                bottom = rect.Bottom,
                left = rect.Left,
                right = rect.Right,
                height = rect.Height;

            //---------------------------------------------------------------------------
            // Duyệt tìm các đối tượng được gióng theo phương Y (standardElements)
            // Luôn có ít nhất 1 đường thẳng đứng đi qua tất cả các đối tượng này và
            // chúng không chồng lên nhau
            //---------------------------------------------------------------------------
            List<ObjectElement> standardElementsX = new List<ObjectElement>();
            List<ObjectElement> standardElementsY = new List<ObjectElement>();
            foreach (ObjectElement item in elements)
            {
                if (!item.IsSelected && item != source && MoveThumb.CheckRoot(item, source))
                {
                    if (AlignX(rect, item.RectBound))
                    {
                        standardElementsX.Add(item);
                    }
                    if (AlignY(rect, item.RectBound))
                    {
                        standardElementsY.Add(item);
                    }
                }
            }


            //---------------------------------------------------------------------------
            // Thu thập các phân bố
            //---------------------------------------------------------------------------
            List<Distribute> distributesY = new List<Distribute>();
            foreach (ObjectElement element1 in standardElementsY)
            {
                Rect rect1 = element1.RectBound;
                foreach (ObjectElement element2 in standardElementsY)
                {
                    Rect rect2 = element2.RectBound;
                    if (AlignX(rect1, rect2))
                    {
                        distributesY.Add(new Distribute(
                            new GuideLine(rect1.TopLeft, new Point(Math.Max(rect1.Right, rect2.Right) + DISTANCE_EXTENDED, rect1.Top)),
                            new GuideLine(rect2.TopLeft, new Point(Math.Max(rect1.Right, rect2.Right) + DISTANCE_EXTENDED, rect2.Top))
                            ));
                    }
                }
            }


            //---------------------------------------------------------------------------
            // Chọn phân bố để hít dính đối tượng vào
            //---------------------------------------------------------------------------
            foreach (Distribute distribute in distributesY)
            {
                if (distribute.TrySnap(rect))
                {
                    distributes.Add(distribute);
                }
            }

            return distributes;
        }

        private bool AlignY(Rect rect1, Rect rect2)
        {
            return rect1.Left <= rect2.Right && rect1.Right >= rect2.Left;
        }

        private bool AlignX(Rect rect1, Rect rect2)
        {
            return rect1.Top <= rect2.Bottom && rect1.Bottom >= rect2.Top;
        }

        public void RaiseMoving(object sender, EDragDeltaEventArgs e)
        {
            Moving?.Invoke(sender, e);
        }

        public void RaiseResizing(object sender, EDragDeltaEventArgs e)
        {
            Resizing?.Invoke(sender, e);
        }

        public void RaiseDragCompleted(object sender, DragCompletedEventArgs e)
        {
            DragCompleted?.Invoke(sender, e);
        }

        protected override void OnRender(DrawingContext dc)
        {
            if (Geometry != null)
            {
                double scale = (Application.Current as IAppGlobal).DocumentPageScale;
                DashStyle dashStyle = new DashStyle(new List<double>() { 4, 4 }, 2);
                foreach (var item in Geometry.Children)
                {
                    Pen pen1 = new Pen(Brushes.Red, 1 / scale) { DashStyle = dashStyle };
                    double halfPenWidth1 = pen1.Thickness / 2;
                    if (item is LineGeometry lineGeometry)
                    {
                        GuidelineSet guidelineSet = new GuidelineSet();
                        if (lineGeometry.StartPoint.X == lineGeometry.EndPoint.X)
                        {
                            guidelineSet.GuidelinesX.Add(lineGeometry.StartPoint.X + halfPenWidth1);
                            guidelineSet.GuidelinesY.Add(lineGeometry.StartPoint.Y + halfPenWidth1);
                            guidelineSet.GuidelinesY.Add(lineGeometry.EndPoint.Y + halfPenWidth1);
                        }
                        if (lineGeometry.StartPoint.Y == lineGeometry.EndPoint.Y)
                        {
                            guidelineSet.GuidelinesY.Add(lineGeometry.StartPoint.Y + halfPenWidth1);
                            guidelineSet.GuidelinesX.Add(lineGeometry.StartPoint.X + halfPenWidth1);
                            guidelineSet.GuidelinesX.Add(lineGeometry.EndPoint.X + halfPenWidth1);
                        }
                        dc.PushGuidelineSet(guidelineSet);
                        dc.DrawGeometry(null, pen1, lineGeometry);
                        dc.Pop();
                    }
                    if (item is GeometryGroup geometryGroup)
                    {
                        Pen pen2 = new Pen(Brushes.LightGray, 1 / scale) { DashStyle = dashStyle };
                        double halfPenWidth2 = pen2.Thickness / 2;
                        foreach (var geomtry in geometryGroup.Children)
                        {
                            if (geomtry is LineGeometry lineGeometry1)
                            {
                                GuidelineSet guidelineSet = new GuidelineSet();
                                if (lineGeometry1.StartPoint.X == lineGeometry1.EndPoint.X)
                                {
                                    guidelineSet.GuidelinesX.Add(lineGeometry1.StartPoint.X + halfPenWidth2);
                                    guidelineSet.GuidelinesY.Add(lineGeometry1.StartPoint.Y + halfPenWidth2);
                                    guidelineSet.GuidelinesY.Add(lineGeometry1.EndPoint.Y + halfPenWidth2);
                                }
                                if (lineGeometry1.StartPoint.Y == lineGeometry1.EndPoint.Y)
                                {
                                    guidelineSet.GuidelinesY.Add(lineGeometry1.StartPoint.Y + halfPenWidth2);
                                    guidelineSet.GuidelinesX.Add(lineGeometry1.StartPoint.X + halfPenWidth2);
                                    guidelineSet.GuidelinesX.Add(lineGeometry1.EndPoint.X + halfPenWidth2);
                                }
                                dc.PushGuidelineSet(guidelineSet);
                                dc.DrawGeometry(null, pen2, lineGeometry1);
                                dc.Pop();
                            }
                            if (geomtry is GeometryGroup geometryGroup1)
                            {
                                Pen pen3 = new Pen(Brushes.Red, 1 / scale) { DashStyle = dashStyle };
                                double halfPenWidth3 = pen3.Thickness / 2;
                                foreach (var item1 in geometryGroup1.Children)
                                {
                                    if (item1 is LineGeometry line)
                                    {
                                        GuidelineSet guidelineSet = new GuidelineSet();
                                        if (line.StartPoint.X == line.EndPoint.X)
                                        {
                                            guidelineSet.GuidelinesX.Add(line.StartPoint.X + halfPenWidth2);
                                            guidelineSet.GuidelinesY.Add(line.StartPoint.Y + halfPenWidth2);
                                            guidelineSet.GuidelinesY.Add(line.EndPoint.Y + halfPenWidth2);
                                        }
                                        if (line.StartPoint.Y == line.EndPoint.Y)
                                        {
                                            guidelineSet.GuidelinesY.Add(line.StartPoint.Y + halfPenWidth2);
                                            guidelineSet.GuidelinesX.Add(line.StartPoint.X + halfPenWidth2);
                                            guidelineSet.GuidelinesX.Add(line.EndPoint.X + halfPenWidth2);
                                        }
                                        dc.PushGuidelineSet(guidelineSet);
                                        dc.DrawGeometry(null, pen3, line);
                                        dc.Pop();
                                    }
                                    else if (item1 is PathGeometry arrow)
                                    {
                                        dc.DrawGeometry(Brushes.Red, null, arrow);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private const double DELTA = 3;
        private const double DISTANCE_EXTENDED = 20;
    }

    internal static class Extensions
    {
        public static Vector Rotate(this ObjectElement element, Vector vector)
        {
            if ((element.RenderTransform as TransformGroup)?.Children.FirstOrDefault(x => x is RotateTransform) is RotateTransform rotateTransform)
            {
                return (Vector)rotateTransform.Transform((Point)vector);
            }
            return vector;
        }
    }
}

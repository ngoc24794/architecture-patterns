﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.ObjectElement;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using enums = INV.Elearning.Controls.Enums;

namespace INV.Elearning.Core.View
{

    /// <summary>
    /// Interaction logic for GroupContainer.xaml
    /// </summary>
    public partial class GroupContainer : ObjectElement, IGroupContainer, IBorderSupportExt, IEffectSupport
    {
        #region IBorderSupport Implement Properties

        #region Thickness

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính độ dày đường viền
        /// </summary>
        public double Thickness
        {
            get { return (double)GetValue(ThicknessProperty); }
            set { SetValue(ThicknessProperty, value); }
        }

        public static readonly DependencyProperty ThicknessProperty =
            DependencyProperty.Register("Thickness", typeof(double), typeof(GroupContainer), new PropertyMetadata(2.5, ShapePropertyChangedCallBack));

        /// <summary>
        /// Hàm thay đổi các thuộc tính
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ShapePropertyChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == ThicknessProperty)
            {
                foreach (var item in (d as GroupContainer).Elements)
                {
                    if (item is IBorderSupport iBoder) { iBoder.Thickness = (double)e.NewValue; }
                }
            }
            else if (e.Property == CapTypeProperty)
            {
                foreach (var item in (d as GroupContainer).Elements)
                {
                    if (item is IBorderSupport iBoder) { iBoder.CapType = (PenLineCap)e.NewValue; }
                }
            }
            else if (e.Property == JoinTypeProperty)
            {
                foreach (var item in (d as GroupContainer).Elements)
                {
                    if (item is IBorderSupport iBoder) { iBoder.JoinType = (PenLineJoin)e.NewValue; }
                }
            }
            else if (e.Property == StrokeProperty)
            {
                foreach (var item in (d as GroupContainer).Elements)
                {
                    if (item is IBorderSupport iBoder) { iBoder.Stroke = (ColorBrushBase)e.NewValue; }
                }
            }
            else if (e.Property == FillProperty)
            {
                foreach (var item in (d as GroupContainer).Elements)
                {
                    if (item is IBorderSupport iBoder) { iBoder.Fill = (ColorBrushBase)e.NewValue; }
                }
            }
            else if (e.Property == DashTypeProperty)
            {
                foreach (var item in (d as GroupContainer).Elements)
                {
                    if (item is IBorderSupport iBoder) { iBoder.DashType = (enums.DashType)e.NewValue; }
                }
            }
        }

        #endregion

        #region CapType

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính kiểu bo các nét
        /// </summary>
        public System.Windows.Media.PenLineCap CapType
        {
            get { return (System.Windows.Media.PenLineCap)GetValue(CapTypeProperty); }
            set { SetValue(CapTypeProperty, value); }
        }

        public static readonly DependencyProperty CapTypeProperty =
            DependencyProperty.Register("CapType", typeof(System.Windows.Media.PenLineCap), typeof(GroupContainer), new PropertyMetadata(System.Windows.Media.PenLineCap.Square, ShapePropertyChangedCallBack));


        #endregion

        #region JoinType

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính kiểu bo các nét
        /// </summary>
        public System.Windows.Media.PenLineJoin JoinType
        {
            get { return (System.Windows.Media.PenLineJoin)GetValue(JoinTypeProperty); }
            set { SetValue(JoinTypeProperty, value); }
        }

        public static readonly DependencyProperty JoinTypeProperty =
            DependencyProperty.Register("JoinType", typeof(System.Windows.Media.PenLineJoin), typeof(GroupContainer), new PropertyMetadata(System.Windows.Media.PenLineJoin.Miter, ShapePropertyChangedCallBack));


        #endregion

        #region DashType

        /// <summary>
        /// Lấy hoặc cài đặt kiểu nét đứt cho đường kẻ
        /// </summary>
        public enums.DashType DashType
        {
            get { return (enums.DashType)GetValue(DashTypeProperty); }
            set { SetValue(DashTypeProperty, value); }
        }

        public static readonly DependencyProperty DashTypeProperty =
            DependencyProperty.Register("DashType", typeof(enums.DashType), typeof(GroupContainer), new PropertyMetadata(enums.DashType.Solid, ShapePropertyChangedCallBack));

        #endregion

        #region Stroke

        /// <summary>
        /// Lấy hoặc cài đặt màu đường kẻ
        /// </summary>
        public ColorBrushBase Stroke
        {
            get { return (ColorBrushBase)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(ColorBrushBase), typeof(GroupContainer), new PropertyMetadata(null, ShapePropertyChangedCallBack));

        #endregion

        #region Fill

        /// <summary>
        /// Lấy hoặc cài đặt màu nền của khung
        /// </summary>
        public ColorBrushBase Fill
        {
            get { return (ColorBrushBase)GetValue(FillProperty); }
            set
            {
                SetValue(FillProperty, value);
            }
        }

        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(ColorBrushBase), typeof(GroupContainer), new PropertyMetadata(null, ShapePropertyChangedCallBack));

        /// <summary>
        /// Cài đặt kiểu cho khung
        /// </summary>
        /// <param name="stroke"></param>
        /// <param name="fill"></param>
        public virtual void SetStyle(ColorBrushBase stroke, ColorBrushBase fill)
        {
            this.Stroke = stroke;
            this.Fill = Fill;
        }

        /// <summary>
        /// Cài đặt kiểu chung <br/>
        /// Ba tham số đầu tiên lần lượt là <see cref="Stroke"/>, <see cref="Fill"/>, <see cref="Effects"/> và <see cref="Thickness"/>
        /// </summary>
        /// <param name="args"></param>
        public virtual void SetStyle(params object[] args)
        {
            if (args.Length > 0)
            {
                this.Stroke = args[0] as ColorBrushBase;
                if (args.Length > 1)
                {
                    this.Fill = args[1] as ColorBrushBase;
                    if (args.Length > 2)
                    {
                        if (args.Length > 3)
                        {
                            if (args[3] is double thickness)
                                this.Thickness = thickness;
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region IEffectSuport Implement Properties and Methods

        #region Effects
        private ObservableCollection<Model.FrameEffectBase> _effects;

        /// <summary>
        /// Lấy hoặc cài đặt danh sách các hiệu ứng
        /// </summary>
        public ObservableCollection<Model.FrameEffectBase> Effects
        {
            get
            {
                if (_effects == null)
                {
                    _effects = new ObservableCollection<Model.FrameEffectBase>();
                    _effects.CollectionChanged += Effects_Changed;
                }

                return _effects;
            }
        }

        /// <summary>
        /// Xóa các hiệu ứng cùng loại, sao cho 1 thời điểm chỉ có đại diện của 1 loại hiệu ứng xuất hiện trong danh sách hiệu ứng
        /// </summary>
        /// <param name="effect"></param>
        private void RemoveEffect(Model.FrameEffectBase effect)
        {
            for (int i = 0; i < Effects.Count; i++)
            {
                if (Effects[i] != effect && Effects[i].GetType() == effect.GetType())
                {
                    Effects.RemoveAt(i);
                    i--;
                }
            }
        }

        /// <summary>
        /// Thay đổi danh sách các hiệu ứng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Effects_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (FrameEffectBase effect in e.NewItems)
                    {
                        RemoveEffect(effect as Model.FrameEffectBase);
                        foreach (var item in Elements)
                        {
                            if (item is IEffectSupport effectSupport)
                            {
                                effectSupport.SetEffect(effect);
                            }
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (FrameEffectBase effect in e.OldItems)
                    {
                        foreach (var item in Elements)
                        {
                            if (item is IEffectSupport effectSupport)
                            {
                                effectSupport.Effects.Remove(effect);
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Cài đặt hiệu ứng cho khung
        /// </summary>
        /// <param name="effect"></param>
        public void SetEffect(Model.FrameEffectBase effect)
        {
            this.Effects.Add(effect);
        }

        /// <summary>
        /// Xóa tất cả các hiệu ứng hiên thị
        /// </summary>
        public void RemoveAllEffects()
        {
            while (this.Effects.Count > 0)
            {
                this.Effects.RemoveAt(0);
            }
        }

        #endregion

        #endregion

        #region Elements

        private ObservableCollection<ObjectElement> _elements = null;
        /// <summary>
        /// Danh sách các phần tử thuộc Group
        /// </summary>
        public ObservableCollection<ObjectElement> Elements
        {
            get
            {
                if (_elements == null)
                {
                    _elements = new ObservableCollection<ObjectElement>();
                    _elements.CollectionChanged += Elements_Changed;
                }
                return _elements;
            }
        }

        /// <summary>
        /// Thay đổi danh sách
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Elements_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    Panel _panel = this.Parent as Panel;
                    foreach (ObjectElement item in e.NewItems)
                    {
                        item.ElementContainer = this; //Cài đặt giá trị khung chứa
                        item.PreviousElementContainer = null;
                        item.SizeChanged += Item_SizeChanged;
                        item.LocationChanged += Item_LocationChanged;
                        item.AngleChanged += Item_AngleChanged;
                        item.SessionChanged += Item_SessionChanged;
                        if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Elements.Contains(item) == true)
                            (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Remove(item);
                        if (_panel != null && item.Parent == null)
                        {
                            _panel.Children.Add(item);
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (ObjectElement item in e.OldItems)
                    {
                        item.ElementContainer = null; //Reset giá trị khung chứa
                        item.PreviousElementContainer = this;
                        item.SizeChanged -= Item_SizeChanged;
                        item.LocationChanged -= Item_LocationChanged;
                        item.AngleChanged -= Item_AngleChanged;
                        item.SessionChanged -= Item_SessionChanged;
                        (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(item);
                    }
                    break;
            }
        }

        /// <summary>
        /// Thay đổi các trạng thái
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_SessionChanged(object sender, ObjectElementEventArg e)
        {
            if (!(bool)e.NewValue)
            {
                UpdateLocationAndSize();
            }
        }

        /// <summary>
        /// Thay đổi góc quay của đối tượng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_AngleChanged(object sender, ObjectElementEventArg e)
        {
            if (!(sender as ObjectElement).InSession)
                UpdateLocationAndSize();
        }

        /// <summary>
        /// Thay đổi vị trí của đối tượng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_LocationChanged(object sender, ObjectElementEventArg e)
        {
            if (!(sender as ObjectElement).InSession)
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                 {
                     UpdateLocationAndSize();
                 }));
            }
        }

        /// <summary>
        /// Thay đổi kích thước của đối tượng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!(sender as ObjectElement).InSession)
                UpdateLocationAndSize();
        }


        private bool _isSettingFromGroup = false, _isSettingFromChild = false, _isChildForChild = false;
        /// <summary>
        /// Cập nhật vị trí và kích thước
        /// </summary>
        private void UpdateLocationAndSize()
        {
            if (_isChildForChild || _isSettingFromGroup || Global.UndoProcessing || ObjectElementsGlobal.Source == this || CheckRoot()) //Kiểm tra để thoát, tránh gay vòng lặp vô hạn
                return;

            Global.StartGrouping("kzqdsl");
            if (this.Adorner != null)
                this.Adorner.OpacityMask = Brushes.Transparent;
            _isSettingFromChild = true;
            _isChildForChild = true;

            UpdateSize();
            UpdateLocation();
            if (this.Adorner != null)
                this.Adorner.OpacityMask = null;
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                _isChildForChild = false;
                _isSettingFromChild = false;
                Global.StopGrouping("kzqdsl");
            }));

        }

        /// <summary>
        /// Kiểm tra nguồn là cha của các đối tượng
        /// </summary>
        /// <returns></returns>
        private bool CheckRoot()
        {
            ObjectElement _container = this.ElementContainer;
            while (_container?.ElementContainer != null)
            {
                _container = _container.ElementContainer;
            }

            if (_container is GroupContainer _group)
                return _group._isSettingFromGroup;

            return false;
        }

        /// <summary>
        /// Cập nhật kích thước
        /// </summary>
        private void UpdateSize()
        {
            var _elements = GetAllObject(Elements);
            if (_elements.Count == 0) return;
            double _top, _left, _right, _bottom;
            IEnumerable<Rect> _rectsBounds = _elements.Select(x => x.GetBound(this)); //Xác định các điểm biên của các hình so với khung nhóm
            _top = _rectsBounds.Min(x => x.Top);
            _bottom = _rectsBounds.Max(x => x.Bottom);
            _left = _rectsBounds.Min(x => x.Left);
            _right = _rectsBounds.Max(x => x.Right);

            double _deltaR = (_right - this.Width); //Lưu trữ các giá trị bị thay đổi trong tính toán
            double _deltaB = (_bottom - this.Height);
            double _originW = this.Width;
            double _originH = this.Height;

            AddWidthDelta(-_left, true, _originW, true); //Tăng chiều ngang
            AddWidthDelta(_deltaR, false, _originW, true);

            AddHeightDelta(-_top, true, _originH, true); //Tăng chiều dọc
            AddHeightDelta(_deltaB, false, _originH, true);

            _elements.ForEach(x => x.InvalidateMeasure());
        }

        /// <summary>
        /// Cập nhật lại vị trí
        /// </summary>
        private void UpdateLocation()
        {
            IEnumerable<Rect> _rectsBounds = Elements.Select(x => x.RectBound); //Xác định các điểm biên của các hình so với trang tài liệu
            if (_rectsBounds.Count() == 0) return;
            double _top = _rectsBounds.Min(x => x.Top);
            double _bottom = _rectsBounds.Max(x => x.Bottom);
            double _left = _rectsBounds.Min(x => x.Left);
            double _right = _rectsBounds.Max(x => x.Right);

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                foreach (var item in Elements) //Cập nhật lại các tỷ lệ
                {
                    Rect _rectBound = item.GetBound(this);
                    Rect _rectPercents = new Rect(((_rectBound.Left + _rectBound.Width / 2) - item.Width / 2) / this.Width, ((_rectBound.Top + _rectBound.Height / 2) - item.Height / 2) / this.Height, item.Width / this.Width, item.Height / this.Height); //Tỷ lệ so với khung chứa
                    GroupContainer.SetOriginPercents(item, _rectPercents);
                }
            }));
        }

        /// <summary>
        /// Hủy Group các đối tượng
        /// </summary>
        public void UnGroup()
        {
            for (int i = 0; i < Elements.Count; i++)
            {
                Elements[i].IsSelected = true;
                Elements[i].PreviousElementContainer = this;
                Elements.RemoveAt(i);
                i--;
            }
            this.IsSelected = false;
            (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Remove(this);
        }

        /// <summary>
        /// Ghi đè sự kiện PreViewMouseDown
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            if (this.ElementContainer != null) //Nếu nằm trong khung khác thì thoát
                return;
            base.OnPreviewMouseDown(e);
            if (Keyboard.Modifiers != ModifierKeys.Control)
            {
                UnSelected(this);
                IsContentFocused = false;
            }
            if (e.ClickCount > 1)
            {
                IsContentFocused = true;
            }
        }

        /// <summary>
        /// Hủy lựa chọn tất cả các đối tượng con
        /// </summary>
        public void UnSelectChilds()
        {
            foreach (var item in this.Elements)
            {
                item.IsSelected = false;
                if(item is GroupContainer group)
                {
                    group.UnSelectChilds();
                }
            }
            IsContentFocused = false;
        }

        /// <summary>
        /// Hủy tất cả các thành phần con trong group
        /// </summary>
        /// <param name="group"></param>
        private void UnSelected(GroupContainer group)
        {
            foreach (ObjectElement item in group.Elements)
            {
                item.IsSelected = false;
                if (item is GroupContainer)
                    UnSelected(item as GroupContainer);
            }
        }

        /// <summary>
        /// Ghi đề sự kiện thay đổi lựa chọn
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSelectionChanged(ObjectElementEventArg e)
        {
            base.OnSelectionChanged(e);
            if (!(bool)e.NewValue)
            {
                UnSelected(this);
                IsContentFocused = false;
            }
            else
            {
                IsContentFocused = true;
            }
        }

        #endregion

        #region OriginPercents

        /// <summary>
        /// Giá trị lưu trữ các tỷ lệ về kích thước, vị trí so với khung chứa lúc ban đầu
        /// </summary>
        public static readonly DependencyProperty OriginPercentsProperty = DependencyProperty.RegisterAttached("OriginPercents", typeof(Rect), typeof(GroupContainer), new FrameworkPropertyMetadata(new Rect()));

        /// <summary>
        /// Cài đặt giá trị tỷ lệ so với khung chứa
        /// </summary>
        /// <param name="element">Phần tử</param>
        /// <param name="value">Hình chữ nhật có chưa tỷ lệ vị trí, kích thước ban đầu so với khung chứa</param>
        public static void SetOriginPercents(UIElement element, Rect value)
        {
            element.SetValue(OriginPercentsProperty, value);
        }

        /// <summary>
        /// Lấy giá trị tỷ lệ so với khung chứa
        /// </summary>
        /// <param name="element"></param>
        /// <returns>Hình chữ nhật có chưa tỷ lệ vị trí, kích thước ban đầu so với khung chứa</returns>
        public static Rect GetOriginPercents(UIElement element)
        {
            return (Rect)element.GetValue(OriginPercentsProperty);
        }
        #endregion

        #region PreviousScale      
        /// <summary>
        /// Giá trị scale x trước đây
        /// </summary>
        public new bool PreviousScaleX { get; private set; }

        /// <summary>
        /// Giá trị scale y trước đây
        /// </summary>
        public bool PreviousScaleY { set; private get; }
        #endregion

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public GroupContainer()
        {
            InitializeComponent();
            this.SizeChanged += GroupContainer_SizeChanged;
            this.AngleChanged += GroupContainer_AngleChanged;
            this.LocationChanged += GroupContainer_LocationChanged;
            this.IsVisibleChanged += GroupContainer_IsVisibleChanged;

            Binding _binding = new Binding("ElementContainer");
            _binding.Source = this;
            _binding.Mode = BindingMode.OneWay;
            _binding.Converter = new NullToBooleanConverter();
            this.SetBinding(IsHitTestVisibleProperty, _binding);
            this.Data = new GroupElement();
        }

        private void GroupContainer_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Visibility != Visibility.Visible)
            {
                foreach (var item in Elements)
                {
                    item.Visibility = Visibility.Hidden;
                }
            }
            else
            {
                foreach (var item in Elements)
                {
                    item.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Hàm static
        /// </summary>
        static GroupContainer()
        {
            //Ghi đè giá trị mặc định cho việc có chứa nội dung có thể thay đổi
            HasEditableContentProperty.OverrideMetadata(typeof(GroupContainer), new FrameworkPropertyMetadata(true));
            LockedProperty.OverrideMetadata(typeof(GroupContainer), new PropertyMetadata(false, LockCallBack));
        }

        /// <summary>
        /// Hàm gọi lại
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void LockCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as GroupContainer;
            if (_source.Locked)
            {
                _source.IsHitTestVisible = false;
                _source.IsSelected = false;                
            }
            else
            {
                _source.IsHitTestVisible = _source.IsShow;
            }
            _source.SetIsSelected(_source.IsSelected && !(bool)e.NewValue && _source.IsShow);
            foreach (var item in _source.Elements)
            {
                item.Locked = _source.Locked;
            }
        }

        private void GroupContainer_LocationChanged(object sender, ObjectElementEventArg e)
        {
            if (Global.UndoProcessing) return;

            if (!_isSettingFromChild || this.IsLoaded && (ObjectElementsGlobal.Source == sender))
            {
                _isSettingFromGroup = true;
                double _verticalChange = (e.NewValue as Point?).Value.X - (e.PreviousValue as Point?).Value.X;
                double _horizontalChange = (e.NewValue as Point?).Value.Y - (e.PreviousValue as Point?).Value.Y;
                foreach (ObjectElement item in Elements)
                {
                    item.Left += _verticalChange;
                    item.Top += _horizontalChange;
                }
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    _isSettingFromGroup = false;
                }));
            }
        }

        /// <summary>
        /// Sự kiện thay đổi góc quay của khung
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GroupContainer_AngleChanged(object sender, ObjectElementEventArg e)
        {
            if (Global.UndoProcessing) return;

            Global.StartGrouping("asdwfd");
            if (!_isSettingFromChild || this.IsLoaded && (ObjectElementsGlobal.Source == sender))
            {
                _isSettingFromGroup = true;
                Point _centerGroup = new Point(this.RectBound.Left + this.RectBound.Width / 2, this.RectBound.Top + this.RectBound.Height / 2);
                for (int i = 0; i < Elements.Count; i++)
                {
                    _elements[i].Angle += (double)e.NewValue - (double)e.PreviousValue;
                    Point _center = new Point(_elements[i].Left + _elements[i].Width / 2, _elements[i].Top + _elements[i].Height / 2);
                    Point _newPoint = RotationPoint(_centerGroup, _center, (double)e.NewValue - (double)e.PreviousValue);
                    _elements[i].Left = _newPoint.X - _elements[i].Width / 2;
                    _elements[i].Top = _newPoint.Y - _elements[i].Height / 2;
                }
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    _isSettingFromGroup = false;
                }));
            }
        }

        /// <summary>
        /// Lấy tất cả các phần tử có trong group
        /// </summary>
        /// <returns></returns>
        private List<ObjectElement> GetAllObject(IEnumerable<ObjectElement> input)
        {
            List<ObjectElement> _result = new List<ObjectElement>();

            foreach (var item in input)
            {
                if (item is GroupContainer)
                {
                    _result.AddRange(GetAllObject((item as GroupContainer).Elements));
                }
                else
                {
                    _result.Add(item);
                }
            }

            return _result;
        }

        /// <summary>
        /// Thay đổi điểm sau mỗi góc quay
        /// </summary>
        /// <param name="center"></param>
        /// <param name="oldPoint"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        private Point RotationPoint(Point center, Point oldPoint, double angle)
        {
            double angleRad = angle * Math.PI / 180;
            double x = center.X + (oldPoint.X - center.X) * Math.Cos(angleRad) - (oldPoint.Y - center.Y) * Math.Sin(angleRad);
            double y = center.Y + (oldPoint.X - center.X) * Math.Sin(angleRad) + (oldPoint.Y - center.Y) * Math.Cos(angleRad);
            return new Point(x, y);

        }

        /// <summary>
        /// Sự kiện thay đổi khung kích thước
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GroupContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Global.UndoProcessing) return;

            if (!_isSettingFromChild || this.IsLoaded && (ObjectElementsGlobal.Source == sender))
            {
                _isSettingFromGroup = true;
                double _heigh = e.NewSize.Height;
                double _width = e.NewSize.Width;

                double _deltaX = IsScaleX ? -_width : 0;
                double _deltaY = IsScaleY ? -_heigh : 0;

                foreach (ObjectElement item in Elements)
                {
                    if (this.IsScaleX != this.PreviousScaleX) //Đảo ngược
                    {
                        item.IsScaleX = !item.IsScaleX;
                    }

                    if (this.IsScaleY != this.PreviousScaleY)
                    {
                        item.IsScaleY = !item.IsScaleY;
                    }

                    Rect _rect = GroupContainer.GetOriginPercents(item);

                    double _scaleLeft = _rect.Left + (_rect.Right - _rect.Left) / 2;
                    double _scaleTop = _rect.Top + (_rect.Bottom - _rect.Top) / 2;

                    //Chuyển điểm sang vị trí tương đối so với trang soạn thảo
                    Point _center = this.TranslatePoint(new Point(Math.Abs(_scaleLeft * _width + _deltaX), Math.Abs(_scaleTop * _heigh + _deltaY)), this.Parent as Panel);

                    if (_heigh * _rect.Height >= item.MinHeight)
                    {
                        item.Height = _heigh * _rect.Height;
                        item.Top = _center.Y - item.Height / 2;
                    }

                    if (_width * _rect.Width >= item.MinWidth)
                    {
                        item.Width = _width * _rect.Width;
                        item.Left = _center.X - item.Width / 2;
                    }
                }

                if (this.IsScaleX != this.PreviousScaleX) //Cập nhật lại giá trị
                {
                    this.PreviousScaleX = this.IsScaleX;
                }

                if (this.IsScaleY != this.PreviousScaleY)
                {
                    this.PreviousScaleY = this.IsScaleY;
                }

                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    _isSettingFromGroup = false; ;
                }));
            }
        }

        /// <summary>
        /// Nhấn xuống chuột
        /// </summary>
        /// <param name="sender"></param>
        public override void MouseDownExcute(object sender)
        {
            Global.StartGrouping("group-mouseD");
            base.MouseDownExcute(sender);
            foreach (var item in this.Elements)
            {
                if (!(sender is MoveThumb))
                    item.IsSelected = false;
                item.InSession = true;
                item.MouseDownExcute(sender);
            }
            if (!(sender is MoveThumb))
                IsContentFocused = false;
        }

        /// <summary>
        /// Thả chuột
        /// </summary>
        /// <param name="sender"></param>
        public override void MouseUpExcute(object sender)
        {
            base.MouseUpExcute(sender);
            _isSettingFromGroup = true;
            foreach (var item in this.Elements)
            {
                item.InSession = false;
                item.MouseUpExcute(sender);
            }
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                _isSettingFromGroup = false;
                Global.StopGrouping("group-mouseD");
            }));
        }

        #region Override Method

        /// <summary>
        /// Cập nhật lại dữ liệu
        /// </summary>
        public override void RefreshData()
        {
            if (this.Data == null) this.Data = new GroupElement();
            base.RefreshData();
            (this.Data as GroupElement).Members.Clear();
            foreach (var item in this.Elements)
            {
                item.RefreshData();
                (this.Data as GroupElement).Members.Add(item.Data);
            }
        }

        /// <summary>
        /// Tải lại dữ liệu lưu trữ
        /// </summary>
        /// <param name="data"></param>
        public override void UpdateUI(ObjectElementBase data)
        {
            base.UpdateUI(data);
            if (data is GroupElement group)
            {
                this.Elements.Clear();
                foreach (var item in group.Members)
                {
                    var _element = ObjectElementsHelper.LoadData(item);
                    if (_element != null)
                    {
                        this.Elements.Add(_element);
                    }
                }
            }
            UpdateLocation();
        }


        public override bool SaveToImage(string imagePath, Size size = default(Size))
        {
            return FileHelper.SaveToImage(imagePath, this);
        }

        /// <summary>
        /// Overide hàm cập nhật màu cho các phần tử bên trong group
        /// </summary>
        public override void UpdateThemeColor()
        {
            base.UpdateThemeColor();
            foreach (var child in Elements)
            {
                child.UpdateThemeColor();
            }
        }

        /// <summary>
        /// Overide hàm cập nhật font cho các phần tử bên trong group
        /// </summary>
        public override void UpdateThemeFont()
        {
            base.UpdateThemeFont();
            foreach (var child in Elements)
            {
                child.UpdateThemeFont();
            }
        }
        #endregion
    }

    /// <summary>
    /// Chuyển từ null sang boolean
    /// </summary>
    public class NullToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

}

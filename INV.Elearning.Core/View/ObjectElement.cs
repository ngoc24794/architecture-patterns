﻿using INV.Elearning.Animations;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Các kiểu place holder
    /// </summary>
    public enum PlaceHolderEnum
    {
        None,
        CenterTitle,
        Title,
        Subtitle,
        Body,
        DateTime,
        SlideNumber,
        Footer,
        Normal
    }
    /// <summary>
    /// Lớp đối tượng chung
    /// </summary>
    public class ObjectElement : ContentControl, IObjectElement, ITimingObject, IAnimationableObject, ITriggerableObject, IUpdatePropertyByUndo, IThemeSupport, IPainterSupport
    {
        private TriggerFocusAdorner _triggerAdorner = null;
        private List<FrameworkElement> _supportElements = null;
        /// <summary>
        /// Danh sách các đối tượng tạm, hỗ trợ thể hiệu hiệu ứng ...
        /// </summary>
        public List<FrameworkElement> SupportElements { get => _supportElements ?? (_supportElements = new List<FrameworkElement>()); }

        #region Constructor
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public ObjectElement()
        {
            Loaded += ObjectElement_Loaded;
            this.RenderTransformOrigin = new Point(0.5, 0.5);
            this.MinWidth = this.MinHeight = 10;

            this.Style = TryFindResource("ObjectElementStyleKey") as Style;
            if (Style == null && Application.Current != null)
            {
                var _resource = new ResourceDictionary();
                _resource.Source = new Uri("pack://application:,,,/INV.Elearning.Core;Component/Resources/Generic.xaml");
                Application.Current.Resources.MergedDictionaries.Add(_resource);
                this.Style = TryFindResource("ObjectElementStyleKey") as Style;
            }
            Timing = new Timing();
            if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout?.Timing is Timing timing)
            {
                if (Timing != null)
                {
                    if (timing.HeadTime >= timing.TotalTime)
                    {
                        Timing.StartTime = 0;
                    }
                    else
                    {
                        Timing.StartTime = timing.HeadTime;
                    }
                }
            }
            IsShow = true;
            Locked = false;
            TriggerDataSource = new CollectionViewSource();
            BindingOperations.SetBinding(TriggerDataSource, CollectionViewSource.SourceProperty, new Binding("TriggerData")
            {
                Source = this
            });
            LocationChanged += OnLocationChanged;
            this.TargetName = "Element";
            ID = ObjectElementsHelper.RandomString(13);
            EntranceAnimation = new NoneAnimation(TimeSpan.FromSeconds(0), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", INV.Elearning.Animations.Enums.eAnimationType.Entrance, false);
            ExitAnimation = new NoneAnimation(TimeSpan.FromSeconds(0), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", INV.Elearning.Animations.Enums.eAnimationType.Exit, false);
        }

        /// <summary>
        /// Tải xong dữ liệu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectElement_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ObjectElement_Loaded;
            UpdateScale();
        }

        /// <summary>
        /// Khởi tạo hàm tĩnh
        /// </summary>
        static ObjectElement()
        {
            WidthProperty.OverrideMetadata(typeof(ObjectElement), new FrameworkPropertyMetadata(double.NaN, new PropertyChangedCallback(WidthChangedCallback)));
            HeightProperty.OverrideMetadata(typeof(ObjectElement), new FrameworkPropertyMetadata(double.NaN, new PropertyChangedCallback(HeightChangedCallback)));
        }



        public bool IsSizeRounded
        {
            get { return (bool)GetValue(IsSizeRoundedProperty); }
            set { SetValue(IsSizeRoundedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSizeRounded.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSizeRoundedProperty =
            DependencyProperty.Register("IsSizeRounded", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true));



        /// <summary>
        /// Thay đổi giá trị chiều rộng
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void HeightChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObjectElement _element = d as ObjectElement;
            if (_element.Height == 0 || !_element.IsSizeRounded) return;
            Global.BeginInit();
            _element.Height = Math.Round(_element.Height);
            Global.EndInit();
            if ((d as ObjectElement).CanPushUndo && Global.CanPushUndo && !(d as ObjectElement).InSession && (d as ObjectElement).IsLoaded) //Ghi nhận giá trị Undo
            {
                Global.PushUndo(new UndoRedoByProperty() { Source = d, NewValue = e.NewValue, OldValue = e.OldValue, PropertyName = "Height" });
            }
        }

        /// <summary>
        /// Giá trị chiều dài thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void WidthChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObjectElement _element = d as ObjectElement;
            if (_element.Width == 0 || !_element.IsSizeRounded) return;
            Global.BeginInit();
            _element.Width = Math.Round(_element.Width);
            Global.EndInit();
            if ((d as ObjectElement).CanPushUndo && Global.CanPushUndo && !(d as ObjectElement).InSession && (d as ObjectElement).IsLoaded) //Ghi nhận giá trị Undo
            {
                Global.PushUndo(new UndoRedoByProperty() { Source = d, NewValue = e.NewValue, OldValue = e.OldValue, PropertyName = "Width" });
            }
        }

        #endregion

        #region Routed Events
        public delegate void PropertyEventHanler(object sender, ObjectElementEventArg e);

        #region SelectionChanged
        /// <summary>
        /// Sự kiện thay đổi vị trí
        /// </summary>
        public static readonly RoutedEvent SelectionChangedEvent = EventManager.RegisterRoutedEvent(
            "SelectionChanged",
            RoutingStrategy.Bubble,
            typeof(PropertyEventHanler),
            typeof(ObjectElement));

        /// <summary>
        /// Sự kiện thay đổi vị trí
        /// </summary>
        public event PropertyEventHanler SelectionChanged
        {
            add
            {
                base.AddHandler(ObjectElement.SelectionChangedEvent, value);
            }
            remove
            {
                base.RemoveHandler(ObjectElement.SelectionChangedEvent, value);
            }
        }

        /// <summary>
        /// Thực thi một lệnh điều khiển
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSelectionChanged(ObjectElementEventArg e)
        {
            this.RaiseEvent(e);
        }
        #endregion

        #region AngleChanged
        /// <summary>
        /// Sự kiện thay đổi vị trí
        /// </summary>
        public static readonly RoutedEvent AngleChangedEvent = EventManager.RegisterRoutedEvent(
            "AngleChanged",
            RoutingStrategy.Bubble,
            typeof(PropertyEventHanler),
            typeof(ObjectElement));

        /// <summary>
        /// Sự kiện thay đổi vị trí
        /// </summary>
        public event PropertyEventHanler AngleChanged
        {
            add
            {
                base.AddHandler(ObjectElement.AngleChangedEvent, value);
            }
            remove
            {
                base.RemoveHandler(ObjectElement.AngleChangedEvent, value);
            }
        }
        #endregion

        #region LocationChanged
        /// <summary>
        /// Sự kiện thay đổi vị trí
        /// </summary>
        public static readonly RoutedEvent LocationChangedEvent = EventManager.RegisterRoutedEvent(
            "LocationChanged",
            RoutingStrategy.Bubble,
            typeof(PropertyEventHanler),
            typeof(ObjectElement));

        /// <summary>
        /// Sự kiện thay đổi vị trí
        /// </summary>
        public event PropertyEventHanler LocationChanged
        {
            add
            {
                base.AddHandler(ObjectElement.LocationChangedEvent, value);
            }
            remove
            {
                base.RemoveHandler(ObjectElement.LocationChangedEvent, value);
            }
        }

        #endregion

        #region SessionChanged
        /// <summary>
        /// Sự kiện thay trạng thái thao tác người dùng với các Thumb: Thay đổi vị trí, thay đổi kích thước, thay đổi góc quay
        /// </summary>
        public static readonly RoutedEvent SessionChangedEvent = EventManager.RegisterRoutedEvent(
            "SessionChanged",
            RoutingStrategy.Bubble,
            typeof(PropertyEventHanler),
            typeof(ObjectElement));

        /// <summary>
        /// Sự kiện thay trạng thái thao tác người dùng với các Thumb: Thay đổi vị trí, thay đổi kích thước, thay đổi góc quay
        /// </summary>
        public event PropertyEventHanler SessionChanged
        {
            add
            {
                base.AddHandler(ObjectElement.SessionChangedEvent, value);
            }
            remove
            {
                base.RemoveHandler(ObjectElement.SessionChangedEvent, value);
            }
        }

        #endregion

        #endregion

        #region Fields
        internal Size OriginSize = new Size();
        internal bool PreviousScaleX = false;
        long UpdateTimerTick = 0;
        private Adorner _adorner;
        public Adorner Adorner
        {
            get
            {
                return _adorner;
            }
            set
            {
                _adorner = value;
                if (value != null)
                {
                    UpdateScale();
                }
            }
        }

        /// <summary>
        /// Grid chứa các nội dung
        /// </summary>
        public Grid Container { get; private set; }
        #endregion

        #region Ruleconfiguration

        /// <summary>
        /// Cấu hình quy định của đối tượng
        /// </summary>
        public ObjectElementRule RuleConfiguration
        {
            get { return (ObjectElementRule)GetValue(RuleConfigurationProperty); }
            private set { SetValue(RuleConfigurationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RuleConfigurationProperty =
            DependencyProperty.Register("RuleConfiguration", typeof(ObjectElementRule), typeof(ObjectElement), new PropertyMetadata(new ObjectElementRule()));


        #endregion

        #region InSession

        /// <summary>
        /// Thay đổi trạng thái
        /// </summary>
        public bool InSession
        {
            get { return (bool)GetValue(InSessionProperty); }
            set { SetValue(InSessionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for InSession.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InSessionProperty =
            DependencyProperty.Register("InSession", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false, InSessionCallBack));

        private static void InSessionCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObjectElement _element = d as ObjectElement;
            ObjectElementEventArg _arg = new ObjectElementEventArg(ObjectElement.SessionChangedEvent);
            _arg.NewValue = e.NewValue;
            _arg.PreviousValue = e.OldValue;
            _element.RaiseEvent(_arg);

            //Ghi nhận các thay đổi giá trị
            if ((bool)e.NewValue) //Nếu bắt đầu trạng thái thì ghi nhận các giá trị kích thước, vị trí, góc quay
            {
                _element._wBackup = _element.Width;
                _element._hBackup = _element.Height;
                _element._tBackup = _element.Top;
                _element._lBackup = _element.Left;
                _element._aBackup = _element.Angle;
                _element._isXBackup = _element.IsScaleX;
                _element._isYBackup = _element.IsScaleY;
                if (_element.Adorner != null)
                {
                    _element.Adorner.Opacity = 0;
                    if (_element.Adorner is ContainerAdorner _containerAdorner)
                    {
                        _containerAdorner.Chrome.UpdateCursorByAngle(_element.Angle);
                    }
                }
            }
            else
            {
                if (Global.CanPushUndo && (d as ObjectElement).IsLoaded) //Kiểm tra điều kiện đẩy vào ngăn xếp
                {
                    if (_element._wBackup != _element.Width ||
                        _element._hBackup != _element.Height ||
                        _element._tBackup != _element.Top ||
                        _element._lBackup != _element.Left ||
                        _element._aBackup != _element.Angle ||
                        _element._isXBackup != _element.IsScaleX ||
                        _element._isYBackup != _element.IsScaleY)
                        Global.PushUndo(new StandardPropertiesStep(new StandardProperties() { Angle = _element.Angle, Left = _element.Left, Top = _element.Top, Height = _element.Height, Width = _element.Width, IsScaleX = _element.IsScaleX, IsScaleY = _element.IsScaleY },
                            new StandardProperties() { Angle = _element._aBackup, Left = _element._lBackup, Top = _element._tBackup, Height = _element._hBackup, Width = _element._wBackup, IsScaleX = _element._isXBackup, IsScaleY = _element._isYBackup }, _element));
                }
                if (_element.Adorner != null) _element.Adorner.Opacity = 1;
            }
        }

        private double _wBackup, _hBackup, _tBackup, _lBackup, _aBackup; //Các biến lưu trữ giá trị trước lúc thay đổi
        private bool _isXBackup, _isYBackup;

        #endregion

        #region IsGraphicBackground

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính cho Graphic
        /// </summary>
        public bool IsGraphicBackground
        {
            get { return (bool)GetValue(IsGraphicBackgroundProperty); }
            set { SetValue(IsGraphicBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsGraphicBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsGraphicBackgroundProperty =
            DependencyProperty.Register("IsGraphicBackground", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true));


        #endregion

        #region Scale

        /// <summary>
        /// Có thu Phóng theo chiều X
        /// </summary>
        public bool IsScaleX
        {
            get { return (bool)GetValue(IsScaleXProperty); }
            set { SetValue(IsScaleXProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsScaleX.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsScaleXProperty =
            DependencyProperty.Register("IsScaleX", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false, IsScaleXCallBack));

        /// <summary>
        /// Thay đổi giá trị ScaleX
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsScaleXCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObjectElement _element = d as ObjectElement;
            if (_element.IsTemplateSecond) //Nếu cài đặt giao diện adorner thứ 2, thì không cho phép giá trị bằng true
            {
                if (_element.IsScaleX)
                {
                    _element.IsScaleX = false;
                }
                return;
            }
            _element.UpdateScale();

            if (_element.IsLoaded && !_element.InSession)
                Global.PushUndo(new DependencyPropertyStep(d, e.Property, e.NewValue, e.OldValue));
        }

        /// <summary>
        /// Cập nhật lại tỷ lệ
        /// </summary>
        protected virtual void UpdateScale()
        {
            if (!this.IsLoaded) return;
            ScaleTransform _scale = null;

            if ((this.Adorner as ContainerAdorner)?.Chrome != null)
            {
                if (_scale == null)
                {
                    _scale = new ScaleTransform(this.IsScaleX ? -1 : 1, this.IsScaleY ? -1 : 1);
                }
                (this.Adorner as ContainerAdorner).Chrome.LayoutTransform = _scale;
            }

            if (this.Container?.Children.Count > 0 && this.Container?.Children[0] is MoveThumb _moveThumb)
            {
                if (_scale == null)
                {
                    _scale = new ScaleTransform(this.IsScaleX ? -1 : 1, this.IsScaleY ? -1 : 1);
                }
                _moveThumb.LayoutTransform = _scale;
            }

            if (this.Adorner is ContainerAdorner _containerAdorner)
            {
                _containerAdorner.Chrome.UpdateCursorByAngle(this.Angle);
            }
        }

        /// <summary>
        /// Có thu phóng theo chiều Y
        /// </summary>
        public bool IsScaleY
        {
            get { return (bool)GetValue(IsScaleYProperty); }
            set { SetValue(IsScaleYProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsScaleY.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsScaleYProperty =
            DependencyProperty.Register("IsScaleY", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false, IsScaleYCallBack));

        /// <summary>
        /// Thay đổi giá trị IsScaleY
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsScaleYCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObjectElement _element = d as ObjectElement;
            if (_element.IsTemplateSecond) //Nếu cài đặt giao diện adorner thứ 2, thì không cho phép giá trị bằng true
            {
                if (_element.IsScaleY)
                {
                    _element.IsScaleY = false;
                }
                return;
            }

            _element.UpdateScale();

            if (_element.IsLoaded && !_element.InSession)
                Global.PushUndo(new DependencyPropertyStep(d, e.Property, e.NewValue, e.OldValue));
        }
        #endregion

        #region Data
        /// <summary>
        /// Lấy hoặc cài đặt dữ liệu data cho đối tượng khung thuộc trang
        /// </summary>
        public ObjectElementBase Data
        {
            get { return (ObjectElementBase)GetValue(DataProperty); }
            protected set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(ObjectElementBase), typeof(ObjectElement), new PropertyMetadata(null));
        #endregion

        #region IsSelected

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính lựa chọn cho đối tượng
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false, SelectedCallBack));

        private static void SelectedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as ObjectElement;
            if (_source.IsSelected)
            {
                if (!_source.IsShow || _source.Locked)
                {
                    _source.IsSelected = false;
                    return;
                }
            }
            _source.SetIsSelected((bool)e.NewValue && _source.CanUserResizeAndRotate && _source.IsShow && !_source.Locked);
            if (Application.Current is IAppGlobal)
            {
                if ((bool)e.NewValue) //Nếu đang được lựa chọn
                {
                    (Application.Current as IAppGlobal).SelectedItem = _source;
                }
                else
                {
                    if ((Application.Current as IAppGlobal).SelectedElements.Contains(_source)) //Nếu đã tồn tại trong danh sách
                        (Application.Current as IAppGlobal).SelectedElements.Remove(_source);
                }
            }

            if (!(bool)e.NewValue) //Nếu giá trị là false
            {
                _source.CanContentFocus = false;
                _source.IsContentFocused = false;
            }
            else
            {
                if (_source.ElementContainer != null)
                {
                    ObjectElement _container = _source.ElementContainer;
                    while (_container.ElementContainer != null)
                    {
                        _container = _container.ElementContainer;
                    }
                    _container.IsSelected = true;
                    _container.IsContentFocused = true;
                }
            }

            ObjectElementEventArg _arg = new ObjectElementEventArg(ObjectElement.SelectionChangedEvent);
            _arg.NewValue = e.NewValue;
            _arg.PreviousValue = e.OldValue;
            (d as ObjectElement).OnSelectionChanged(_arg);
        }

        #endregion

        #region CanUserResizeAndRotate

        /// <summary>
        /// Người dùng có được phép thay đổi góc quay và kích thước hay không
        /// </summary>
        public bool CanUserResizeAndRotate
        {
            get { return (bool)GetValue(CanUserResizeAndRotateProperty); }
            set { SetValue(CanUserResizeAndRotateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanUserResizeAndRotate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanUserResizeAndRotateProperty =
            DependencyProperty.Register("CanUserResizeAndRotate", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true, CanUserResizeAndRotateCallBack));

        /// <summary>
        /// Thay đổi giá trị của thuộc tính
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void CanUserResizeAndRotateCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ObjectElement).SetCanuserResizeAndRotate((bool)e.NewValue);
        }


        #endregion

        #region LayoutOwner

        /// <summary>
        /// Layout chứa đối tượng
        /// </summary>
        public LayoutBase LayoutOwner
        {
            get { return (LayoutBase)GetValue(LayoutOwnerProperty); }
            internal set { SetValue(LayoutOwnerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutOwner.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutOwnerProperty =
            DependencyProperty.Register("LayoutOwner", typeof(LayoutBase), typeof(ObjectElement), new PropertyMetadata(null));


        #endregion

        #region Top

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính top cho đối tượng
        /// </summary>
        public double Top
        {
            get { return (double)GetValue(TopProperty); }
            set { SetValue(TopProperty, value); }
        }

        public static readonly DependencyProperty TopProperty =
            DependencyProperty.Register("Top", typeof(double), typeof(ObjectElement), new FrameworkPropertyMetadata(0.0, TopCallBack)
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

        private static void TopCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Canvas.SetTop((d as ObjectElement), (double)e.NewValue);



            ObjectElementEventArg _arg = new ObjectElementEventArg(ObjectElement.LocationChangedEvent);
            _arg.NewValue = new Point((d as ObjectElement).Left, (double)e.NewValue);
            _arg.PreviousValue = new Point((d as ObjectElement).Left, (double)e.OldValue);

            (d as ObjectElement).RaiseEvent(_arg);
            if ((d as ObjectElement).CanPushUndo && Global.CanPushUndo && !(d as ObjectElement).InSession && (d as ObjectElement).IsLoaded)
            {
                Global.PushUndo(new UndoRedoByProperty() { Source = d, NewValue = e.NewValue, OldValue = e.OldValue, PropertyName = "Top" });
            }
        }


        #endregion

        #region Left

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính căn lề trái cho đối tượng
        /// </summary>
        public double Left
        {
            get { return (double)GetValue(LeftProperty); }
            set { SetValue(LeftProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Left.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftProperty =
            DependencyProperty.Register("Left", typeof(double), typeof(ObjectElement), new FrameworkPropertyMetadata(0.0, LeftCallBack)
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

        private static void LeftCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Canvas.SetLeft((d as ObjectElement), (double)e.NewValue);

            ObjectElementEventArg _arg = new ObjectElementEventArg(ObjectElement.LocationChangedEvent);
            _arg.NewValue = new Point((double)e.NewValue, (d as ObjectElement).Top);
            _arg.PreviousValue = new Point((double)e.OldValue, (d as ObjectElement).Top);
            (d as ObjectElement).RaiseEvent(_arg);

            if ((d as ObjectElement).CanPushUndo && Global.CanPushUndo && !(d as ObjectElement).InSession && (d as ObjectElement).IsLoaded)
            {
                Global.PushUndo(new UndoRedoByProperty() { Source = d, NewValue = e.NewValue, OldValue = e.OldValue, PropertyName = "Left" });
            }
        }

        #endregion

        #region Angle

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính góc quay cho đối tượng
        /// </summary>
        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(ObjectElement), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsMeasure, AngleCallBack)
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

        /// <summary>
        /// Hàm được gọi khi giá trị Angle thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void AngleCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as ObjectElement;
            if ((double)e.NewValue != 0.0 && !_source.RuleConfiguration.CanRotate) //Nếu cấu hình không được phép quay
            {
                _source.Angle = 0;
                return;
            }

            if (_source.IsTemplateSecond) //Nếu giao diện adoner thứ 2 được bật (không có thumb quay)
                                          //Thuộc tính Angle luôn mang giá trị là 0
            {
                if ((double)e.NewValue != 0.0)
                {
                    _source.Angle = 0.0;
                }
                return;
            }

            if (!(_source.RenderTransform is TransformGroup)) //Cần bỏ vào một transform group để có thể chứa nhiều loại Transform
            {
                TransformGroup _transGroup = new TransformGroup();
                _source.RenderTransform = _transGroup;
            }

            if ((_source.RenderTransform as TransformGroup).Children.Count == 0 || (_source.RenderTransform as TransformGroup).Children.First(x => x is RotateTransform) == null)
            {
                (_source.RenderTransform as TransformGroup).Children.Add(new RotateTransform());
            }

            ((_source.RenderTransform as TransformGroup).Children.First(x => x is RotateTransform) as RotateTransform).Angle = (double)e.NewValue;

            //Gọi sự kiện thay đổi góc quay
            ObjectElementEventArg _arg = new ObjectElementEventArg(ObjectElement.AngleChangedEvent);
            _arg.NewValue = (double)e.NewValue;
            _arg.PreviousValue = (double)e.OldValue;
            (d as ObjectElement).RaiseEvent(_arg);

            if ((d as ObjectElement).CanPushUndo && Global.CanPushUndo && !(d as ObjectElement).InSession && (d as ObjectElement).IsLoaded) //Ghi nhận giá trị Undo
            {
                Global.PushUndo(new UndoRedoByProperty() { Source = d, NewValue = e.NewValue, OldValue = e.OldValue, PropertyName = "Angle" });
            }
        }


        #endregion

        #region ZIndex

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính ZIndex của đối tượng
        /// </summary>
        public int ZIndex
        {
            get { return Panel.GetZIndex(this); }
            set { SetValue(ZIndexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ZIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ZIndexProperty =
            DependencyProperty.Register("ZIndex", typeof(int), typeof(ObjectElement), new PropertyMetadata(0, ZIndexCallBack));

        /// <summary>
        /// Hàm gọi lại sau mỗi lần cài đặt ZIndex
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ZIndexCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Panel.SetZIndex(d as ObjectElement, (int)e.NewValue);

            if ((d as ObjectElement).CanPushUndo && Global.CanPushUndo && !(d as ObjectElement).InSession && (d as ObjectElement).IsLoaded) //Ghi nhận giá trị Undo
            {
                Global.PushUndo(new UndoRedoByProperty() { Source = d, NewValue = e.NewValue, OldValue = e.OldValue, PropertyName = "ZIndex" });
            }
        }


        #endregion

        #region Locked

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính khóa đối tượng
        /// </summary>
        public bool Locked
        {
            get { return (bool)GetValue(LockedProperty); }
            set { SetValue(LockedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Locked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LockedProperty =
            DependencyProperty.Register("Locked", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false, LockCallback));

        /// <summary>
        /// Gọi lại sự kiện khóa đối tượng
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void LockCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as ObjectElement;
            if (_source.Locked)
            {
                _source.IsHitTestVisible = false;
                _source.IsSelected = false;
            }
            else
            {
                _source.IsHitTestVisible = _source.IsShow;
            }
            _source.SetIsSelected(_source.IsSelected && !(bool)e.NewValue && _source.IsShow);
        }

        #endregion

        #region IsShow

        /// <summary>
        /// Hiển thị trên khung
        /// </summary>
        public bool IsShow
        {
            get { return (bool)GetValue(IsShowProperty); }
            set { SetValue(IsShowProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsShow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsShowProperty =
            DependencyProperty.Register("IsShow", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true, IsShowCallback));

        private static void IsShowCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as ObjectElement;
            if (_source.IsShow)
            {
                _source.IsHitTestVisible = !_source.Locked && true;
                _source.Visibility = Visibility.Visible;
            }
            else
            {
                _source.IsHitTestVisible = false;
                _source.Visibility = Visibility.Hidden;
                _source.IsSelected = false;
            }
            _source.SetIsSelected(_source.IsSelected && (bool)e.NewValue && !_source.Locked);
            (Application.Current as IAppGlobal).CheckIsShowTextBox(_source);
        }


        #endregion

        #region Icon

        /// <summary>
        /// Lấy hoặc cài đặt biểu tượng cho đối tượng
        /// </summary>
        public string Icon
        {
            get { return (string)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(string), typeof(ObjectElement), new PropertyMetadata("pack://application:,,,/INV.Elearning.Core;Component/Images/RectangleShape.png"));

        #endregion

        #region DescriptionContent

        /// <summary>
        /// Lấy hoặc cài đặt nội dung mô tả cho đối tượng
        /// </summary>
        public object DescriptionContent
        {
            get { return (object)GetValue(DescriptionContentProperty); }
            set { SetValue(DescriptionContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DescriptionContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DescriptionContentProperty =
            DependencyProperty.Register("DescriptionContent", typeof(object), typeof(ObjectElement), new PropertyMetadata(null));


        #endregion

        #region TargetName

        public string TargetName
        {
            get { return (string)GetValue(TargetNameProperty); }
            set { SetValue(TargetNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TargetName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetNameProperty =
            DependencyProperty.Register("TargetName", typeof(string), typeof(ObjectElement), new PropertyMetadata(string.Empty));


        #endregion

        #region Timing

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính thời gian trình chiếu cho đối tượng
        /// </summary>
        public Timing Timing
        {
            get { return (Timing)GetValue(TimingProperty); }
            set { SetValue(TimingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Timing.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimingProperty =
            DependencyProperty.Register("Timing", typeof(Timing), typeof(ObjectElement), new PropertyMetadata(null, TimingCallback));

        /// <summary>
        /// Gọi lại
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void TimingCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as ObjectElement;
            if (e.NewValue != null && _source.RuleConfiguration.LockTiming)
            {
                _source.Timing = null;
            }
        }

        #endregion

        #region ID

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính định danh cho đối tượng
        /// </summary>
        public string ID
        {
            get { return (string)GetValue(IDProperty); }
            set { SetValue(IDProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IDProperty =
            DependencyProperty.Register("ID", typeof(string), typeof(ObjectElement), new PropertyMetadata(string.Empty));


        #endregion

        #region RectBound

        /// <summary>
        /// Lấy thuộc tính khung chữ nhật bao quát đối tượng
        /// </summary>
        public Rect RectBound
        {
            get
            {
                if (this.Parent is Panel)
                    return this.TransformToVisual(this.Parent as Panel).TransformBounds(new Rect(this.RenderSize));
                else
                    return new Rect();
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Cài đặt lựa chọn
        /// </summary>
        /// <param name="value"></param>
        public void SetIsSelected(bool value)
        {
            var _adornerLayer = AdornerLayer.GetAdornerLayer(this);

            if (_adornerLayer != null)
            {
                if (value && RuleConfiguration.IsAdornerVisible)
                {
                    if (Adorner == null)
                    {
                        Adorner = new ContainerAdorner(this);
                        _adornerLayer.Add(Adorner);
                        this.UpdateLayout();
                    }
                    else if (Adorner.Parent == null)
                    {
                        _adornerLayer.Add(Adorner);
                        this.InvalidateArrange();
                    }
                }
                else if (this.Adorner != null)
                {
                    _adornerLayer.Remove(Adorner);
                }
            }
        }

        /// <summary>
        /// Cài đặt người dùng được phép thay đổi
        /// </summary>
        /// <param name="value"></param>
        public void SetCanuserResizeAndRotate(bool value)
        {
            if (Adorner != null)
                Adorner.Visibility = value ? (IsSelected ? Visibility.Visible : Visibility.Hidden) : Visibility.Hidden;
        }

        /// <summary>
        /// Lấy kích thước khung bao xung quay đối tượng theo vị trí với Panel chứa
        /// </summary>
        /// <param name="panel"></param>
        /// <returns></returns>
        public Rect GetBound(FrameworkElement panel)
        {
            try
            {
                if (Window.GetWindow(this) == null) return new Rect(0, 0, this.ActualWidth, this.ActualHeight);
                return this.TransformToVisual(panel).TransformBounds(new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            }
            catch
            {
                return new Rect();
            }

        }

        /// <summary>
        /// Thêm một khoảng delta kích thước vào chiều dài
        /// </summary>
        /// <param name="delta">Khoảng tăng thêm</param>
        /// <param name="isLeft">Bên trái hoặc bên phải</param>
        internal void AddWidthDelta(double delta, bool isLeft, double sourceWidth, bool isSelf = false)
        {
            if (delta == 0) return;

            double _originWidth = isSelf ? sourceWidth : this.OriginSize.Width > 0 ? this.OriginSize.Width : this.ActualWidth;
            delta *= (_originWidth / sourceWidth);
            var angle = Angle / 180.0 * Math.PI;
            if (isLeft)
            {
                if (!this.IsRealSize)
                {
                    if (this.Adorner is ContainerAdorner)
                        (this.Adorner as ContainerAdorner).RectStartPoint = new Point((this.Adorner as ContainerAdorner).RectStartPoint.X - delta, (this.Adorner as ContainerAdorner).RectStartPoint.Y);
                    return;
                }

                if (-delta > this.Width) //Trong trường hợp di chuyển có ảnh hưởng đến Top
                {
                    IsScaleX = !IsScaleX;

                    Left += (Width * Math.Cos(angle) + (0.5 * Width * (1 - Math.Cos(angle))));
                    Width = -delta - Width;
                    Top -= (0.5 * delta * Math.Sin(angle));
                    Left -= (Width * 0.5 * (1 - Math.Cos(angle)));
                }
                else
                {
                    Width += delta; //Tính toán lại vị trí Left
                    Top -= (0.5 * delta * Math.Sin(angle));
                    Left -= (delta * Math.Cos(angle) + (0.5 * delta * (1 - Math.Cos(angle))));
                }
            }
            else
            {
                if (!this.IsRealSize)
                {
                    if (this.Adorner is ContainerAdorner)
                        (this.Adorner as ContainerAdorner).RectEndPoint = new Point((this.Adorner as ContainerAdorner).RectEndPoint.X + delta, (this.Adorner as ContainerAdorner).RectEndPoint.Y);
                    return;
                }

                if (-delta > this.Width) //Trong trường hợp di chuyển có ảnh hưởng đến Top
                {
                    IsScaleX = !IsScaleX;

                    Top += (0.5 * -Width * Math.Sin(angle));
                    Left -= (-Width * 0.5 * (1 - Math.Cos(angle)));

                    Width = -delta - Width;

                    Top -= (0.5 * Width * Math.Sin(angle));
                    Left -= (Width * Math.Cos(angle) + (0.5 * Width * (1 - Math.Cos(angle))));
                }
                else
                {
                    Width += delta;
                    Top += (0.5 * delta * Math.Sin(angle));
                    Left -= (delta * 0.5 * (1 - Math.Cos(angle)));
                }
            }
        }

        /// <summary>
        /// Thêm một khoảng delta kích thước vào chiều rộng
        /// </summary>
        /// <param name="delta">Khoảng tăng thêm</param>
        /// <param name="isTop">Bên trên hoặc bên dưới</param>
        internal void AddHeightDelta(double delta, bool isTop, double sourceHeight, bool isSelf = false)
        {
            if (delta == 0) return;

            double _originHeight = isSelf ? sourceHeight : this.OriginSize.Height > 0 ? this.OriginSize.Height : this.ActualHeight;
            delta *= (_originHeight / sourceHeight);
            var angle = Angle / 180.0 * Math.PI;
            if (isTop)
            {
                if (!this.IsRealSize)
                {
                    if (this.Adorner is ContainerAdorner)
                        (this.Adorner as ContainerAdorner).RectStartPoint = new Point((this.Adorner as ContainerAdorner).RectStartPoint.X, (this.Adorner as ContainerAdorner).RectStartPoint.Y - delta);
                    return;
                }

                if (-delta > this.Height)
                {
                    IsScaleY = !IsScaleY;

                    Top -= (-Height * Math.Cos(-angle) + (0.5 * -Height * (1 - Math.Cos(-angle))));
                    Left -= (-Height * Math.Sin(-angle) - (0.5 * -Height * Math.Sin(-angle)));

                    Height = -delta - Height;

                    Top -= (0.5 * Height * (1 - Math.Cos(-angle)));
                    Left += (Height * 0.5 * Math.Sin(-angle));
                }
                else
                {
                    Height += delta;
                    Top -= (delta * Math.Cos(-angle) + (0.5 * delta * (1 - Math.Cos(-angle))));
                    Left -= (delta * Math.Sin(-angle) - (0.5 * delta * Math.Sin(-angle)));
                }
            }
            else
            {
                if (!this.IsRealSize)
                {
                    if (this.Adorner is ContainerAdorner)
                        (this.Adorner as ContainerAdorner).RectEndPoint = new Point((this.Adorner as ContainerAdorner).RectEndPoint.X, (this.Adorner as ContainerAdorner).RectEndPoint.Y + delta);
                    return;
                }

                if (-delta > this.Height)
                {
                    IsScaleY = !IsScaleY;

                    Top -= (0.5 * -Height * (1 - Math.Cos(-angle)));
                    Left += (-Height * 0.5 * Math.Sin(-angle));

                    Height = -delta - Height;

                    Top -= (Height * Math.Cos(-angle) + (0.5 * Height * (1 - Math.Cos(-angle))));
                    Left -= (Height * Math.Sin(-angle) - (0.5 * Height * Math.Sin(-angle)));
                }
                else
                {
                    Height += delta;
                    Top -= (0.5 * delta * (1 - Math.Cos(-angle)));
                    Left += (delta * 0.5 * Math.Sin(-angle));
                }
            }
        }

        /// <summary>
        /// Sự kiện áp dụng Template
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Container = GetTemplateChild("PART_Container") as Grid;
        }

        /// <summary>
        /// Sự kiện nhấn chuột
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            if (this.IsContentFocused)
            {
                if (Global.FindParent<MoveThumb>(e.OriginalSource as DependencyObject) != null)
                {
                    return;
                }
            }

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (!this.IsContentFocused)
                    this.IsSelected = !this.IsSelected;

                if (this.ElementContainer == null) //Nếu tồn tại trong 1 group
                {
                    for (int i = 0; i < (Application.Current as IAppGlobal).SelectedElements.Count; i++)
                    {
                        if ((Application.Current as IAppGlobal).SelectedElements[i].ElementContainer != null && (Application.Current as IAppGlobal).SelectedElements[i].ElementContainer != this)
                        {
                            (Application.Current as IAppGlobal).SelectedElements[i].ElementContainer.CanContentFocus = false;
                            (Application.Current as IAppGlobal).SelectedElements[i].ElementContainer.IsContentFocused = false;
                            (Application.Current as IAppGlobal).SelectedElements[i].IsSelected = false;
                            i--;
                        }
                    }
                }

                return;
            }

            if (this.IsSelected && (Application.Current as IAppGlobal).SelectedElements?.Count > 1)
                return;

            //if (this.IsSelected && this.HasEditableContent)
            //{
            //    this.CanContentFocus = true;
            //}

            ObjectElement _container = this.ElementContainer;
            while (_container?.ElementContainer != null)
            {
                _container = _container.ElementContainer;
            }

            ObjectElementsHelper.UnSelectedAll(this, _container);
            this.IsSelected = true;
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            var _menu = MenuContextHelper.GetContextMenu(this);
            if (_menu != null)
            {
                _menu.IsOpen = true;
                e.Handled = true;
            }
        }

        /// <summary>
        /// Sự kiện thay đổi thuộc tính
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == WidthProperty || e.Property == HeightProperty || e.Property == AngleProperty || e.Property == LeftProperty || e.Property == TopProperty)
            {
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    (Application.Current as IAppGlobal)?.SelectedSlide?.MainLayout?.UpdateDescendantBounds();
                }));
            }
        }
        #endregion

        #region CanContentFocus

        /// <summary>
        /// Nội dung bên trong có được quyền Focus
        /// </summary>
        public bool CanContentFocus
        {
            get { return (bool)GetValue(CanContentFocusProperty); }
            protected internal set { SetValue(CanContentFocusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanContentFocus.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanContentFocusProperty =
            DependencyProperty.Register("CanContentFocus", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false));


        #endregion

        #region HasEditableContent

        /// <summary>
        /// Thuộc tính phần nội dung thuộc khung có thể chỉnh sửa hay không
        /// </summary>
        public bool HasEditableContent
        {
            get { return (bool)GetValue(HasEditableContentProperty); }
            //protected set { SetValue(HasEditableContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasEditableContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasEditableContentProperty =
            DependencyProperty.Register("HasEditableContent", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false));


        #endregion

        #region IsContentFocused

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Focused vào nội dung
        /// </summary>
        public bool IsContentFocused
        {
            get { return (bool)GetValue(IsContentFocusedProperty); }
            set { SetValue(IsContentFocusedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsContentFocused.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsContentFocusedProperty =
            DependencyProperty.Register("IsContentFocused", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false, IsContentFocusedCallBack));

        private static void IsContentFocusedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue && !(d as ObjectElement).HasEditableContent)
            {
                (d as ObjectElement).IsContentFocused = false;
            }
        }


        #endregion

        #region ElementContainer

        /// <summary>
        /// Lấy hoặc cài đặt giá trị khung chứa cho đối tượng<br/>
        /// Các giá trị góc quay, vị trí sẽ có ảnh hưởng của thuộc tính này
        /// </summary>
        public ObjectElement ElementContainer
        {
            get { return (ObjectElement)GetValue(ElementContainerProperty); }
            set { SetValue(ElementContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ElementContainer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ElementContainerProperty =
            DependencyProperty.Register("ElementContainer", typeof(ObjectElement), typeof(ObjectElement), new PropertyMetadata(null));


        #endregion

        #region PreviousElementContainer

        /// <summary>
        /// Lấy hoặc cài đặt giá trị khung chứa trước đó của đối tượng<br/>
        /// Mục đích hỗ trợ cho việc ReGroup các nhóm đối tượng
        /// </summary>
        public ObjectElement PreviousElementContainer
        {
            get { return (ObjectElement)GetValue(PreviousElementContainerProperty); }
            internal set { SetValue(PreviousElementContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ElementContainer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreviousElementContainerProperty =
            DependencyProperty.Register("PreviousElementContainer", typeof(ObjectElement), typeof(ObjectElement), new PropertyMetadata(null));


        #endregion

        #region IsTemplateSecond

        /// <summary>
        /// Cờ trạng thái cho giao diện thứ 2, không bao gồm góc quay, scale
        /// Các thuộc tính <see cref="Angle"/> sẽ luôn mang giá trị 0, thuộc tính <see cref="IsScaleX"/>, thuộc tính <see cref="IsScaleY"/> sẽ luôn mang giá trị false
        /// </summary>
        public bool IsTemplateSecond
        {
            get { return (bool)GetValue(IsTemplateSecondProperty); }
            protected set { SetValue(IsTemplateSecondProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsTemplateSecond.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTemplateSecondProperty =
            DependencyProperty.Register("IsTemplateSecond", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false));

        #endregion

        #region IsRealSize

        /// <summary>
        /// Cập nhật tức thời kích thước sau mỗi thao tác resize trên khung.
        /// Thuộc tính chỉ nhận giá trị false khi thuộc tính <see cref="IsTemplateSecond"/> mang giá trị true
        /// </summary>
        public bool IsRealSize
        {
            get { return (bool)GetValue(IsRealSizeProperty); }
            set { SetValue(IsRealSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRealSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRealSizeProperty =
            DependencyProperty.Register("IsRealSize", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true, RealSizeCallBack));

        private static void RealSizeCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue)
            {
                if (!(d as ObjectElement).IsTemplateSecond)
                {
                    (d as ObjectElement).IsRealSize = true;
                }
            }
        }


        #endregion

        #region IsAspectedRatio

        /// <summary>
        /// Khóa tỷ lệ kích thước chiều dài và chiều rộng
        /// </summary>
        public bool IsAspectedRatio
        {
            get { return (bool)GetValue(IsAspectedRatioProperty); }
            set { SetValue(IsAspectedRatioProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsAspectedRatio.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsAspectedRatioProperty =
            DependencyProperty.Register("IsAspectedRatio", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true));


        #endregion

        #region TriggerData
        public CollectionViewSource TriggerDataSource { get; set; }
        /// <summary>
        /// Dữ liệu trigger
        /// </summary>
        public ObservableCollection<TriggerViewModelBase> TriggerData { get => _triggerData ?? (_triggerData = new ObservableCollection<TriggerViewModelBase>()); set { _triggerData = value; } }
        #endregion

        #region IAnimationableObject Impliments

        /// <summary>
        /// Thay đổi vị trí
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLocationChanged(object sender, ObjectElementEventArg e)
        {
            //if (e.PreviousValue is Point position)
            //{
            //    foreach (ObjectElement path in MotionPaths)
            //    {
            //        if (!path.IsSelected)
            //        {
            //            double
            //                deltaX = path.Left - position.X,
            //                deltaY = path.Top - position.Y;

            //            path.Left = Left + deltaX;
            //            path.Top = Top + deltaY;
            //        }
            //    }
            //}
        }



        public IAnimation EntranceAnimation
        {
            get { return (IAnimation)GetValue(EntranceAnimationProperty); }
            set { SetValue(EntranceAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EntranceAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EntranceAnimationProperty =
            DependencyProperty.Register("EntranceAnimation", typeof(IAnimation), typeof(ObjectElement), new PropertyMetadata(null));


        //private IAnimation _animationEntrance;
        //public IAnimation EntranceAnimation
        //{
        //    get { return _animationEntrance ?? new NoneAnimation(TimeSpan.FromSeconds(0), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", INV.Elearning.Animations.Enums.eAnimationType.Entrance, false); }
        //    set { _animationEntrance = value; }
        //}



        public IAnimation ExitAnimation
        {
            get { return (IAnimation)GetValue(ExitAnimationProperty); }
            set { SetValue(ExitAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExitAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExitAnimationProperty =
            DependencyProperty.Register("ExitAnimation", typeof(IAnimation), typeof(ObjectElement), new PropertyMetadata(null));


        //private IAnimation _animationExit;

        //public IAnimation ExitAnimation
        //{
        //    get { return _animationExit ?? new NoneAnimation(TimeSpan.FromSeconds(0), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png", INV.Elearning.Animations.Enums.eAnimationType.Exit, false); }
        //    set { _animationExit = value; }
        //}
        private ObservableCollection<IMotionPathObject> _animationPath;
        private ObservableCollection<TriggerViewModelBase> _triggerData;

        public ObservableCollection<IMotionPathObject> MotionPaths
        {
            get { return _animationPath ?? (_animationPath = new ObservableCollection<IMotionPathObject>()); }
            set { _animationPath = value; }
        }
        public bool IsHasAnimation { get; set; }

        public string CopiedId { get; set; }
        #endregion

        #region Virtual Methods

        /// <summary>
        /// Lưu trữ đối tượng ra dạng hình ảnh
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="size">Kích thước xuất</param>
        /// <returns></returns>
        public virtual bool SaveToImage(string imagePath, Size size = new Size())
        {
            return FileHelper.SaveToImage(imagePath, this.Container, size);
        }

        /// <summary>
        /// Lưu trữ đối tượng ra dạng hình ảnh
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="size">Kích thước xuất</param>
        /// <returns></returns>
        public virtual BitmapSource SaveToBitmap(Size size = new Size())
        {
            return FileHelper.SaveToBitmap(this.Container, size);
        }

        /// <summary>
        /// Cập nhật lại dữ liệu
        /// </summary>
        public virtual void RefreshData()
        {
            if (this.Data != null)
            {
                this.Data.ID = this.ID;
                this.Data.Width = this.Width;
                this.Data.Height = this.Height;
                this.Data.Angle = this.Angle;
                this.Data.Top = this.Top;
                this.Data.Left = this.Left;
                this.Data.ZOder = this.ZIndex;
                this.Data.Name = this.TargetName;
                this.Data.ID = this.ID;
                this.Data.IsScaleX = this.IsScaleX;
                this.Data.IsScaleY = this.IsScaleY;
                this.Data.Timing = this.Timing;
                this.Data.IsShow = IsShow;
                this.Data.IsLocked = Locked;
                this.Data.IsGraphicBackground = this.IsGraphicBackground;
                this.Data.PlaceHolderType = PlaceHolderType;
                this.Data.Visibility = Visibility;
                this.Data.ElementCount = ElementCount;
                this.Data.IsCustomGraphics = IsCustomGraphics;
                this.Data.KeyLanguage = KeyLanguage;
                CloneAnimation.ChangeDataAnimation(this.EntranceAnimation as Animation);
                this.Data.Animations = new EAnimation() { Entrance = this.EntranceAnimation as Animation, Exit = this.ExitAnimation as Animation };


                foreach (var item in MotionPaths)
                {
                    this.Data.Animations.MotionPaths.Add(item.GetData());
                }

                this.Data.TriggerData = new ObservableCollection<TriggerableDataObjectBase>();
                foreach (var trigger in this.TriggerData)
                {
                    if (trigger.Trigger != null)
                    {
                        this.Data.TriggerData.Add(trigger.Trigger.GetData());
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật lại dữ liệu cho đối tượng
        /// </summary>
        /// <param name="data"></param>
        public virtual void UpdateUI(ObjectElementBase data)
        {
            this.Width = data.Width;
            this.Height = data.Height;
            this.Top = data.Top;
            this.Left = data.Left;
            this.Angle = data.Angle;
            this.ZIndex = data.ZOder;
            this.ID = data.ID;
            this.CopiedId = data.ID;
            if (Global.IsPasting) //Nếu đang dán thì tạo mới id
                this.ID = Helper.ObjectElementsHelper.RandomString(13);
            this.TargetName = data.Name;
            this.IsScaleX = data.IsScaleX;
            this.IsScaleY = data.IsScaleY;
            this.Timing = data.Timing;
            this.IsShow = data.IsShow;
            this.Locked = data.IsLocked;
            this.PlaceHolderType = data.PlaceHolderType;
            this.IsGraphicBackground = data.IsGraphicBackground;
            this.Visibility = data.Visibility;
            this.EntranceAnimation = data.Animations?.Entrance;
            this.ExitAnimation = data.Animations?.Exit;
            this.ElementCount = data.ElementCount;
            this.IsCustomGraphics = data.IsCustomGraphics;
            this.KeyLanguage = data.KeyLanguage;
            if (data.Animations?.MotionPaths != null)
                foreach (var item in data.Animations.MotionPaths)
                {
                    var _motionPath = DataMotionPath.GetObject(item, this);
                    if (_motionPath != null)
                    {
                        _motionPath.CopiedId = _motionPath.ID;
                        if (Global.IsPasting) //Nếu đang dán thì tạo mới id
                            _motionPath.ID = Helper.ObjectElementsHelper.RandomString(13);
                        this.MotionPaths.Add(_motionPath);
                        this.SupportElements.Add(_motionPath);
                        if (this.Parent is Panel panel)
                        {
                            panel.Children.Add(_motionPath);
                        }
                    }
                }

            this.TriggerData?.Clear();

            if (data.TriggerData != null)
                foreach (var trigger in data.TriggerData)
                {
                    TriggerData.Add(trigger.GetModel().GenerateViewModel());
                }
        }

        /// <summary>
        /// Sự kiện nhấn chuột trên form, chế độ bị động, thông qua đối tượng khác
        /// </summary>
        public virtual void MouseDownExcute(object sender) { }

        /// <summary>
        /// Các sự kiện xảy ra xung quanh
        /// </summary>
        /// <param name="e"></param>
        public virtual void OtherEvents(RoutedEventArgs e) { }

        /// <summary>
        /// Sự kiện thả chuột trên form, chế độ bị động, thông qua đối tượng khác
        /// </summary>
        public virtual void MouseUpExcute(object sender) { }

        /// <summary>
        /// Cập nhật thuộc tính, gọi bởi các xử lý Undo
        /// </summary>
        public virtual void OnPropertyChangeByUndo()
        {
            if (DateTime.Now.Ticks - UpdateTimerTick > 2500000) //Thời gian giữ các lần lấy dữ liệu phải lớn hơn .25 giây
            {
                UpdateThumbnail();
                this.LayoutOwner?.OnPropertyChangeByUndo();
            }

            UpdateTimerTick = DateTime.Now.Ticks;
        }

        /// <summary>
        /// Hủy dữ liệu
        /// </summary>
        public virtual void Dispose()
        {
            this.IsSelected = false;
            this.Adorner = null;
            if (this.Container != null)
                for (int i = 0; i < this.Container.Children.Count; i++)
                {
                    this.Container.Children.RemoveAt(i--);
                }
        }
        #endregion

        #region CanPushUndo

        /// <summary>
        /// Có thể đẩy dữ liệu vào UNdo
        /// </summary>
        public bool CanPushUndo
        {
            get { return (bool)GetValue(CanPushUndoProperty); }
            set { SetValue(CanPushUndoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanPushUndo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanPushUndoProperty =
            DependencyProperty.Register("CanPushUndo", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true));


        #endregion

        #region ThumbnailBitmap

        /// <summary>
        /// Bitmap chứa thông tin ảnh thumbnail của layout
        /// </summary>
        public ImageSource ThumbnailBitmap
        {
            get { return (ImageSource)GetValue(ThumbnailBitmapProperty); }
            private set { SetValue(ThumbnailBitmapProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThumbnailBitmapProperty =
            DependencyProperty.Register("ThumbnailBitmap", typeof(ImageSource), typeof(ObjectElement), new PropertyMetadata(null));

        /// <summary>
        /// Cập nhật bitmapThumbnail
        /// </summary>
        public void UpdateThumbnail()
        {
            this.ThumbnailBitmap = FileHelper.SaveToBitmap(this, new Size(150, 67));
        }

        /// <summary>
        /// Cập nhật lại màu sắc theo màu theme
        /// </summary>
        public virtual void UpdateThemeColor()
        {

        }

        /// <summary>
        /// Cập nhật lại phông chữ theo theme
        /// </summary>
        public virtual void UpdateThemeFont()
        {

        }
        #endregion

        #region IsTriggerFocus

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính truy cập bởi trigger
        /// </summary>
        public bool IsTrigerFocused
        {
            get { return (bool)GetValue(IsTrigerFocusedProperty); }
            set { SetValue(IsTrigerFocusedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsTrigerFocused.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTrigerFocusedProperty =
            DependencyProperty.Register("IsTrigerFocused", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false, IsTriggerFucusedCallback));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsTriggerFucusedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObjectElement _owner = d as ObjectElement;
            var _adornerLayer = AdornerLayer.GetAdornerLayer(_owner);
            if (_adornerLayer != null)
            {
                if (_owner.IsTrigerFocused)
                {
                    _owner._triggerAdorner = new TriggerFocusAdorner(_owner);
                    _adornerLayer.Add(_owner._triggerAdorner);
                }
                else
                {
                    if (_owner._triggerAdorner != null)
                    {
                        _adornerLayer.Remove(_owner._triggerAdorner);
                    }
                }
            }
        }


        #endregion

        #region ElementCount

        /// <summary>
        /// Đếm số lượng Element có trong Slide
        /// </summary>
        public int ElementCount
        {
            get { return (int)GetValue(ElementCountProperty); }
            set { SetValue(ElementCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ElementCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ElementCountProperty =
            DependencyProperty.Register("ElementCount", typeof(int), typeof(ObjectElement), new PropertyMetadata(0, ElementCountPropertyCallback));

        private static void ElementCountPropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ObjectElement _owner = d as ObjectElement;
            _owner.TargetName = _owner.ElementType + " " + _owner.ElementCount;
            if (string.IsNullOrEmpty(_owner.ElementType) && _owner is StandardElement standardElement && standardElement.ShapePresent != null)
            {
                _owner.TargetName = standardElement.ShapePresent.ShapeName + " " + _owner.ElementCount;
            }
        }

        /// <summary>
        /// Lấy dữ liệu định dạng
        /// </summary>
        /// <returns></returns>
        public AnimationPainter GetAnimationPainter(AnimationPainter painter = null)
        {
            if (painter is AnimationPainter _animationPainter)
            {
                _animationPainter.Entrance = this.EntranceAnimation;
                _animationPainter.Exit = this.ExitAnimation;
                _animationPainter.MotionPaths = this.MotionPaths.ToList();
                return _animationPainter;
            }
            else
            {
                _animationPainter = new AnimationPainter();
                _animationPainter.Entrance = this.EntranceAnimation;
                _animationPainter.Exit = this.ExitAnimation;
                _animationPainter.MotionPaths = this.MotionPaths.ToList();
                return _animationPainter;
            }
        }

        /// <summary>
        /// Cài đặt dữ liệu định dạng
        /// </summary>
        /// <param name="painter"></param>
        public void SetAnimationPainter(AnimationPainter painter)
        {
            if (painter is AnimationPainter _animationPainter)
            {
                this.EntranceAnimation = _animationPainter.Entrance;
                this.ExitAnimation = _animationPainter.Exit;
                if (_animationPainter.MotionPaths != null)
                    this.MotionPaths = new ObservableCollection<IMotionPathObject>(_animationPainter.MotionPaths);
            }
        }

        /// <summary>
        /// Lấy dữ liệu định dạng
        /// </summary>
        /// <returns></returns>
        public virtual IFormatPainter GetPainter(IFormatPainter painter = null)
        {
            return painter;
        }

        /// <summary>
        /// Cài đặt dữ liệu định dạng
        /// </summary>
        /// <param name="painter"></param>
        public virtual void SetPainter(IFormatPainter painter)
        {

        }

        #endregion

        #region ElementType


        public string ElementType
        {
            get { return (string)GetValue(ElementTypeProperty); }
            set { SetValue(ElementTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ElementType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ElementTypeProperty =
            DependencyProperty.Register("ElementType", typeof(string), typeof(ObjectElement), new PropertyMetadata(string.Empty));


        #endregion

        #region PlaceHolderType

        /// <summary>
        /// Kiểu Place Holder
        /// </summary>
        public PlaceHolderEnum PlaceHolderType
        {
            get { return (PlaceHolderEnum)GetValue(PlaceHolderTypeProperty); }
            set { SetValue(PlaceHolderTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PlaceHolderType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceHolderTypeProperty =
            DependencyProperty.Register("PlaceHolderType", typeof(PlaceHolderEnum), typeof(ObjectElement), new PropertyMetadata(PlaceHolderEnum.None));


        #endregion

        #region IsEdited

        /// <summary>
        /// Kiểm tra xem có edit hay ko
        /// </summary>
        public bool IsEdited
        {
            get { return (bool)GetValue(IsEditedProperty); }
            set { SetValue(IsEditedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsEdited.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsEditedProperty =
            DependencyProperty.Register("IsEdited", typeof(bool), typeof(ObjectElement), new PropertyMetadata(false));


        #endregion

        #region IsCustomGraphics

        /// <summary>
        /// Kiểm tra xem object bên slide master có phải tự thêm vào hay không
        /// </summary>
        public bool IsCustomGraphics
        {
            get { return (bool)GetValue(IsCustomGraphicsProperty); }
            set { SetValue(IsCustomGraphicsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsCustomGraphics.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCustomGraphicsProperty =
            DependencyProperty.Register("IsCustomGraphics", typeof(bool), typeof(ObjectElement), new PropertyMetadata(true));


        #endregion

        #region KeyLanguage

        /// <summary>
        /// Key ngôn ngữ cho text của chủ đề
        /// </summary>
        public string KeyLanguage
        {
            get { return (string)GetValue(KeyLanguageProperty); }
            set { SetValue(KeyLanguageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for KeyLanguage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty KeyLanguageProperty =
            DependencyProperty.Register("KeyLanguage", typeof(string), typeof(ObjectElement), new PropertyMetadata(null));


        #endregion

        #region Methods
        /// <summary>
        /// Trả về tọa độ (Left,Top) của khung khi chưa áp dụng phép biến hình
        /// </summary>
        /// <returns></returns>
        public Point GetOrigin()
        {
            ObjectElement objectElement = new ObjectElement()
            {
                Left = Left,
                Top = Top,
                IsScaleX = IsScaleX,
                IsScaleY = IsScaleY,
                Angle = Angle
            };
            objectElement.IsScaleX = objectElement.IsScaleY = false;
            objectElement.Angle = 0;
            return new Point(objectElement.Left, objectElement.Top);
        }
        #endregion
    }

    /// <summary>
    /// Lớp chứa thông tin sự kiện thay đổi
    /// </summary>
    public class ObjectElementEventArg : RoutedEventArgs
    {
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public ObjectElementEventArg()
        {
        }

        /// <summary>
        /// Hàm khởi tạo 1 tham số
        /// </summary>
        /// <param name="routedEvent"></param>
        public ObjectElementEventArg(RoutedEvent routedEvent)
        {
            this.RoutedEvent = routedEvent;
        }

        /// <summary>
        /// Giá trị vị trí trước đó
        /// </summary>
        public object PreviousValue { get; set; }

        /// <summary>
        /// Giá trị vị trí mới
        /// </summary>
        public object NewValue { get; set; }
    }
}

﻿
using System.Windows.Media;
using System.Windows;
using System.Collections.ObjectModel;
using INV.Elearning.Core.Helper;
using System.Windows.Controls;
using System.Windows.Data;
using INV.Elearing.Controls.Shapes;
using System;
using enums = INV.Elearning.Controls.Enums;
using INV.Elearing.Controls.Shapes.Rectangles;
using INV.Elearning.Core.Helper.Effect;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using INV.Elearning.Core.Model;
using System.Windows.Media.Imaging;
using INV.Elearning.Core.ViewModel;
using System.Linq;
using System.ComponentModel;

namespace INV.Elearning.Core.View
{
    public class StandardElement : ObjectElement, IEffectSupport, IBorderSupportExt, INotifyPropertyChanged
    {
        #region RaisePropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }
        #endregion
        #region Properties
     

        #region Thickness

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính độ dày đường viền
        /// </summary>
        public double Thickness
        {
            get { return (double)GetValue(ThicknessProperty); }
            set { SetValue(ThicknessProperty, value); }
        }

        public static readonly DependencyProperty ThicknessProperty =
            DependencyProperty.Register("Thickness", typeof(double), typeof(StandardElement), new PropertyMetadata(0.0, ShapePropertyChangedCallBack));

        #endregion

        #region CapType

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính kiểu bo các nét
        /// </summary>
        public System.Windows.Media.PenLineCap CapType
        {
            get { return (System.Windows.Media.PenLineCap)GetValue(CapTypeProperty); }
            set { SetValue(CapTypeProperty, value); }
        }

        public static readonly DependencyProperty CapTypeProperty =
            DependencyProperty.Register("CapType", typeof(System.Windows.Media.PenLineCap), typeof(StandardElement), new PropertyMetadata(System.Windows.Media.PenLineCap.Square, ShapePropertyChangedCallBack));


        #endregion

        #region JoinType

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính kiểu bo các nét
        /// </summary>
        public System.Windows.Media.PenLineJoin JoinType
        {
            get { return (System.Windows.Media.PenLineJoin)GetValue(JoinTypeProperty); }
            set { SetValue(JoinTypeProperty, value); }
        }

        public static readonly DependencyProperty JoinTypeProperty =
            DependencyProperty.Register("JoinType", typeof(System.Windows.Media.PenLineJoin), typeof(StandardElement), new PropertyMetadata(System.Windows.Media.PenLineJoin.Miter, ShapePropertyChangedCallBack));


        #endregion

        #region DashType

        /// <summary>
        /// Lấy hoặc cài đặt kiểu nét đứt cho đường kẻ
        /// </summary>
        public enums.DashType DashType
        {
            get { return (enums.DashType)GetValue(DashTypeProperty); }
            set { SetValue(DashTypeProperty, value); }
        }

        public static readonly DependencyProperty DashTypeProperty =
            DependencyProperty.Register("DashType", typeof(enums.DashType), typeof(StandardElement), new PropertyMetadata(enums.DashType.Solid, ShapePropertyChangedCallBack));

        #endregion

        #region Stroke

        /// <summary>
        /// Lấy hoặc cài đặt màu đường kẻ
        /// </summary>
        public ColorBrushBase Stroke
        {
            get { return (ColorBrushBase)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(ColorBrushBase), typeof(StandardElement), new PropertyMetadata((new ColorSolidBrush() { Color = Colors.Gray }), ShapePropertyChangedCallBack));

        #endregion

        #region Fill

        /// <summary>
        /// Lấy hoặc cài đặt màu nền của khung
        /// </summary>
        public ColorBrushBase Fill
        {
            get { return (ColorBrushBase)GetValue(FillProperty); }
            set
            {
                this.IsLikeSlideFill = false;
                SetValue(FillProperty, value);
                RaisePropertyChanged("Fill");
            }
        }

        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(ColorBrushBase), typeof(StandardElement), new PropertyMetadata((new ColorSolidBrush() { Color = Colors.Transparent }), ShapePropertyChangedCallBack));

        /// <summary>
        /// Cài đặt kiểu cho khung
        /// </summary>
        /// <param name="stroke"></param>
        /// <param name="fill"></param>
        public virtual void SetStyle(ColorBrushBase stroke, ColorBrushBase fill)
        {
            this.Stroke = stroke;
            this.Fill = Fill;
        }

        /// <summary>
        /// Cài đặt kiểu chung <br/>
        /// Ba tham số đầu tiên lần lượt là <see cref="Stroke"/>, <see cref="Fill"/>, <see cref="Effects"/> và <see cref="Thickness"/>
        /// </summary>
        /// <param name="args"></param>
        public virtual void SetStyle(params object[] args)
        {
            if (args.Length > 0)
            {
                this.RemoveAllEffects();
                this.Stroke = args[0] as ColorBrushBase;
                if (args.Length > 1)
                {
                    this.Fill = args[1] as ColorBrushBase;
                    if (args.Length > 2)
                    {
                        if (args[2] is FrameEffectBase effect)
                            this.Effects.Add(effect);
                        if (args.Length > 3)
                        {
                            if (args[3] is double thickness)
                                this.Thickness = thickness;
                        }
                    }
                }
            }
        }

        #endregion

        #region IsLikeSlideFill

        /// <summary>
        /// Giống màu nền của slide
        /// </summary>
        public bool IsLikeSlideFill
        {
            get { return (bool)GetValue(IsLikeSlideFillProperty); }
            set { SetValue(IsLikeSlideFillProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsLikeSlideFill.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsLikeSlideFillProperty =
            DependencyProperty.Register("IsLikeSlideFill", typeof(bool), typeof(StandardElement), new PropertyMetadata(false, IsLikeSlideFillCallBack));

        /// <summary>
        /// Gọi lại sự kiện
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsLikeSlideFillCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StandardElement _element = d as StandardElement; //Cài đặt theo màu layout chứa
            BindingOperations.ClearBinding(_element, FillProperty);
            if ((bool)e.NewValue)
            {
                Binding _binding = new Binding("LayoutOwner.Fill");
                _binding.Source = _element;
                _binding.Mode = BindingMode.OneWay;
                _element.SetBinding(FillProperty, _binding);
            }
        }


        #endregion

        #region Effects
        private ObservableCollection<Model.FrameEffectBase> _effects;

        /// <summary>
        /// Lấy hoặc cài đặt danh sách các hiệu ứng
        /// </summary>
        public ObservableCollection<Model.FrameEffectBase> Effects
        {
            get
            {
                if (_effects == null)
                {
                    _effects = new ObservableCollection<Model.FrameEffectBase>();
                    _effects.CollectionChanged += Effects_Changed;
                }

                return _effects;
            }
        }

        /// <summary>
        /// Xóa các hiệu ứng cùng loại, sao cho 1 thời điểm chỉ có đại diện của 1 loại hiệu ứng xuất hiện trong danh sách hiệu ứng
        /// </summary>
        /// <param name="effect"></param>
        private void RemoveEffect(Model.FrameEffectBase effect)
        {
            for (int i = 0; i < Effects.Count; i++)
            {
                if (Effects[i] != effect && Effects[i].GetType() == effect.GetType())
                {
                    Effects.RemoveAt(i);
                    i--;
                }
            }
        }

        /// <summary>
        /// Làm mới các hiệu ứng
        /// </summary>
        private void RefreshEffects()
        {
            if (this.Effects?.Count > 0)
            {
                var _backup = this.Effects.ToList();
                RemoveAllEffects();
                foreach (var item in _backup)
                {
                    this.Effects.Add(item);
                }
                // var _reflects = this.Effects.Where(x => x is Model.ReflectionEffect reflec);
                //if (_reflects.Count() > 0)
                //{
                //    for (int i = 0; i < _reflects.Count(); i++)
                //    {
                //        var _flect = _reflects.ElementAt(i);
                //        this.Effects.Remove(_flect);
                //        this.Effects.Add(_flect);
                //    }
                //}
            }
        }

        /// <summary>
        /// Thay đổi danh sách các hiệu ứng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Effects_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (var effect in e.NewItems)
                    {
                        RemoveEffect(effect as Model.FrameEffectBase);
                        if (this.CanPushUndo && this.IsLoaded)
                            Global.PushUndo(new AddEffectStep(effect as Model.FrameEffectBase, Effects));
                        if (effect is Model.ShadowEffect)
                        {
                            EffectHelper.SetShadowEffect(this, effect as Model.ShadowEffect);
                        }
                        else if (effect is Model.GlowEffect)
                        {
                            EffectHelper.SetGlowEffect(this, effect as Model.GlowEffect);
                        }
                        else if (effect is Model.ReflectionEffect)
                        {
                            EffectHelper.SetReflectionEffect(this, effect as Model.ReflectionEffect);
                        }
                        else if (effect is Model.SoftEdgeEffect)
                        {
                            EffectHelper.SetSoftEdgeEffect(this, effect as Model.SoftEdgeEffect);
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (var effect in e.OldItems)
                    {
                        if (this.CanPushUndo && this.IsLoaded)
                            Global.PushUndo(new RemoveEffectStep(effect as Model.FrameEffectBase, Effects));
                        if (effect is Model.ShadowEffect)
                        {
                            EffectHelper.RemoveShadowEffect(this);
                        }
                        else if (effect is Model.GlowEffect)
                        {
                            EffectHelper.RemoveGlowEffect(this);
                        }
                        else if (effect is Model.ReflectionEffect)
                        {
                            EffectHelper.RemoveReflectionEffect(this);
                        }
                        else if (effect is Model.SoftEdgeEffect)
                        {
                            EffectHelper.RemoveSoftEdgeEffect(this);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Cài đặt hiệu ứng cho khung
        /// </summary>
        /// <param name="effect"></param>
        public void SetEffect(Model.FrameEffectBase effect)
        {
            this.Effects.Add(effect);
        }

        /// <summary>
        /// Xóa tất cả các hiệu ứng hiên thị
        /// </summary>
        public void RemoveAllEffects()
        {
            while (this.Effects.Count > 0)
            {
                this.Effects.RemoveAt(0);
            }
        }

        #endregion

        #region ShapePresent

        /// <summary>
        /// Hình vẽ thể hiện nền
        /// </summary>
        public ShapeBase ShapePresent
        {
            get
            {
                if ((ShapeBase)GetValue(ShapePresentProperty) == null)
                {
                    SetValue(ShapePresentProperty, new RectangleShape());
                }
                return (ShapeBase)GetValue(ShapePresentProperty);
            }
            set { SetValue(ShapePresentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShapePresent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShapePresentProperty =
            DependencyProperty.Register("ShapePresent", typeof(ShapeBase), typeof(StandardElement), new PropertyMetadata(null, ShapePresentCallBack));

        /// <summary>
        /// Hàm cập nhật khi cài đặt ShapePresent
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ShapePresentCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StandardElement _owner = d as StandardElement;
            if (_owner.CanPushUndo && _owner.IsLoaded)
            {
                DependencyPropertyStep _step = new DependencyPropertyStep(d, e.Property, e.NewValue, e.OldValue);
                Global.PushUndo(_step);
            }

            if (e.OldValue != null) //Nếu trước đây đã có Shape, thì phải xóa khỏi khung chứa và hủy bỏ các sự kiện kèm theo
            {
                ShapeBase _shape = e.OldValue as ShapeBase;
                BindingOperations.ClearAllBindings(_shape);
                if (_owner.Container != null)
                    _owner.Container.Children.Remove(_shape);
            }

            if (e.NewValue != null) //Nếu cài đặt một khung mới
            {
                ShapeBase _shape = e.NewValue as ShapeBase;
                _owner.SetBindingValue(_shape);
                if (_owner.Content != null && !_owner.HasEditableContent)
                {
                    _shape.Fill = new VisualBrush() { Visual = _owner.Content as Visual };
                    if (_owner.Container != null)
                        foreach (UIElement item in _owner.Container.Children)
                        {
                            if (item is ContentPresenter)
                            {
                                item.Visibility = Visibility.Hidden;
                                break;
                            }
                        }
                }
                _owner.ShapePresent.SnapsToDevicePixels = true;
                _owner.ShapePresent.LayoutTransform = new ScaleTransform(_owner.IsScaleX ? -1 : 1, _owner.IsScaleY ? -1 : 1);
            }
            else
            {
                if (_owner.Container != null)
                    foreach (UIElement item in _owner.Container.Children)
                    {
                        if (item is ContentPresenter)
                        {
                            item.Visibility = Visibility.Visible;
                            break;
                        }
                    }
            }


            _owner.RefreshEffects();
        }

        #endregion

        #region IsContentFirst

        /// <summary>
        /// Phần nội dung đặt trên các nội dung khác: hình vẽ.
        /// </summary>
        public bool IsContentFirst
        {
            get { return (bool)GetValue(IsContentFirstProperty); }
        }

        // Using a DependencyProperty as the backing store for IsContentFirst.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsContentFirstProperty =
            DependencyProperty.Register("IsContentFirst", typeof(bool), typeof(StandardElement), new PropertyMetadata(true));


        #endregion

        #endregion

        #region Constructors
        public StandardElement()
        {
            this.Style = TryFindResource("StandardElementStyleKey") as Style;
            if (Style == null && Application.Current != null)
            {
                var _resource = new ResourceDictionary();
                _resource.Source = new Uri("pack://application:,,,/INV.Elearning.Core;Component/Resources/Generic.xaml");
                Application.Current.Resources.MergedDictionaries.Add(_resource);
                this.Style = TryFindResource("StandardElementStyleKey") as Style;
            }
            Loaded += StandardElement_Loaded;
            Unloaded += StandardElement_Unloaded;
        }

        private void StandardElement_Unloaded(object sender, RoutedEventArgs e)
        {
            SupportElements.ForEach(x =>
            {
                if (x.Parent is Panel panel)
                {
                    panel.Children.Remove(x);
                }
            });
        }

        private void StandardElement_Loaded(object sender, RoutedEventArgs e)
        {
            SupportElements.ForEach(x =>
            {
                if (x.Parent == null)
                {
                    (this.Parent as Panel).Children.Add(x);
                }
            });
            UpdateScale();
        }


        /// <summary>
        /// Hàm tĩnh
        /// </summary>
        static StandardElement()
        {
            //Ghi đè các giá trị đảo chiều để xử lý thêm cho hình vẽ
            //IsScaleXProperty.OverrideMetadata(typeof(StandardElement), new PropertyMetadata(false, IsScaleXCallBack));
            //IsScaleYProperty.OverrideMetadata(typeof(StandardElement), new PropertyMetadata(false, IsScaleYCallBack));
        }

        private static void IsScaleYCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StandardElement _element = d as StandardElement;
            if (_element.IsTemplateSecond) //Nếu cài đặt giao diện adorner thứ 2, thì không cho phép giá trị bằng true
            {
                if (_element.IsScaleY)
                {
                    _element.IsScaleY = false;
                }
                return;
            }

            _element.UpdateScale();
        }

        /// <summary>
        /// Cập nhật tỷ lệ
        /// </summary>
        protected override void UpdateScale()
        {
            base.UpdateScale();
            ScaleTransform _scale = null;

            if (this.ShapePresent != null)
            {
                if (_scale == null)
                {
                    _scale = new ScaleTransform(this.IsScaleX ? -1 : 1, this.IsScaleY ? -1 : 1);
                }
                this.ShapePresent.LayoutTransform = _scale;
            }

            if (this.Container != null)
            {
                foreach (FrameworkElement item in this.Container.Children)
                {
                    if (item != this.Content && !(item is MoveThumb) && !(item is OuterShadowEffectRectangle))
                    {
                        item.LayoutTransform = _scale;
                    }
                }
            }
        }

        /// <summary>
        /// Thay đổi đảo chiều ngang
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsScaleXCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StandardElement _element = d as StandardElement;
            if (_element.IsTemplateSecond) //Nếu cài đặt giao diện adorner thứ 2, thì không cho phép giá trị bằng true
            {
                if (_element.IsScaleX)
                {
                    _element.IsScaleX = false;
                }
                return;
            }

            _element.UpdateScale();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Cập nhật các thay đổi khi các thuộc tính shape thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ShapePropertyChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StandardElement _owner = d as StandardElement;
            if (_owner.ShapePresent == null)
                _owner.ShapePresent = new RectangleShape();
            RenderOptions.SetBitmapScalingMode(_owner.ShapePresent, BitmapScalingMode.NearestNeighbor);
            //if (_owner.Thickness == 1)
            //{
            //    RenderOptions.SetEdgeMode(_owner.ShapePresent, EdgeMode.Aliased);
            //}
            //else
            //{
            //    RenderOptions.SetEdgeMode(_owner.ShapePresent, EdgeMode.Unspecified);
            //}
            if (_owner.CanPushUndo && Global.CanPushUndo && _owner.IsLoaded)
            {
                Global.PushUndo(new DependencyPropertyStep(d, e.Property, e.NewValue, e.OldValue));
            }
        }

        /// <summary>
        /// Cập nhật các giá trị
        /// </summary>
        /// <param name="shape"></param>
        private void SetBindingValue(ShapeBase shape)
        {
            shape.IsHitTestVisible = false;
            Panel.SetZIndex(shape, -1);
            if (this.Container != null)
                this.Container.Children.Add(shape);

            //if (this.Content != null && this.IsContentFirst) //Nếu nội dung trước thì phải đặt shape ở dưới
            //{
            //    Panel.SetZIndex(shape, Panel.GetZIndex(this.Content as UIElement) + 1);
            //}

            //Cài đặt các giá trị theo Binding
            Binding _binding = new Binding("Stroke.Brush"); //Đỗ màu viền
            _binding.Source = this;
            shape.SetBinding(ShapeBase.StrokeProperty, _binding);

            _binding = new Binding("Fill.Brush"); //Đổ màu
            _binding.Source = this;
            shape.SetBinding(ShapeBase.FillProperty, _binding);

            _binding = new Binding("Thickness"); //Kích thước viền
            _binding.Source = this;
            shape.SetBinding(ShapeBase.StrokeThicknessProperty, _binding);

            _binding = new Binding("DashType"); //Kiểu nét đứt
            _binding.Source = this;
            shape.SetBinding(ShapeBase.DashTypeProperty, _binding);

            _binding = new Binding("CapType"); //Kiểu Cap
            _binding.Source = this;
            shape.SetBinding(ShapeBase.CapTypeProperty, _binding);

            _binding = new Binding("JoinType"); //Kiểu Cap
            _binding.Source = this;
            shape.SetBinding(ShapeBase.JoinTypeProperty, _binding);

            _binding = new Binding("Width"); //Chiều dài
            _binding.Source = this;
            shape.SetBinding(ShapeBase.WidthProperty, _binding);

            _binding = new Binding("Height"); //Chiều rộng
            _binding.Source = this;
            shape.SetBinding(ShapeBase.HeightProperty, _binding);

            _binding = new Binding("IsSelected"); //Lựa chọn
            _binding.Source = this;
            shape.SetBinding(ShapeBase.IsSelectedProperty, _binding);

            shape.RenderTransformOrigin = new Point(0.5, 0.5);
        }
        #endregion       

        #region Override Methods

        /// <summary>
        /// Cập nhật lại dữ liệu
        /// </summary>
        public override void RefreshData()
        {
            if (this.Data == null) this.Data = new EStandardElement();
            base.RefreshData();
            var _data = this.Data as EStandardElement;
            _data.FrameEffects = this.Effects;
            _data.Border = new EBorder();
            _data.Border.BorderBrush = ColorHelper.ConverterFromColor(this.Stroke);
            _data.Border.BorderThickness = this.Thickness;
            _data.Border.CapType = this.CapType;
            _data.Border.JoinType = this.JoinType;
            _data.Border.DashType = ShapeHelper.DashConverter(this.DashType);
            _data.Border.Fill = ColorHelper.ConverterFromColor(this.Fill);
            _data.ShapePresent = this.ShapePresent?.GetInfo();
        }

        /// <summary>
        /// Cập nhật lại dữ liệu
        /// </summary>
        /// <param name="data"></param>
        public override void UpdateUI(ObjectElementBase data)
        {
            base.UpdateUI(data);
            if (data is EStandardElement standardData)
            {
                this.ShapePresent = standardData.ShapePresent?.GetShape();
                if (standardData.Border != null)
                {
                    this.Thickness = standardData.Border.BorderThickness;
                    this.Fill = ColorHelper.ConverFromColorData(standardData.Border.Fill);
                    this.Stroke = ColorHelper.ConverFromColorData(standardData.Border.BorderBrush);
                    this.DashType = ShapeHelper.DashConverter(standardData.Border.DashType);
                    this.CapType = standardData.Border.CapType;
                    this.JoinType = standardData.Border.JoinType;
                }
                if (standardData.FrameEffects != null)
                    foreach (var item in standardData.FrameEffects)
                    {
                        this.Effects.Add(item);
                    }
            }
            this.IsScaleX = data.IsScaleX;
            this.IsScaleY = data.IsScaleY;
        }

        /// <summary>
        /// Cập nhật lại màu sắc theo theme
        /// </summary>
        public override void UpdateThemeColor()
        {
            base.UpdateThemeColor();

            if (this.Fill != null)
            {
                if (this.Fill.IsFrozen)
                {
                    var _clone = this.Fill.Clone();
                    _clone.UpdateBrushByTheme();
                    this.Fill = _clone;
                }
                else
                {
                    this.Fill.UpdateBrushByTheme();
                }
            }

            if (this.Stroke != null)
            {
                if (this.Stroke.IsFrozen)
                {
                    var _clone = this.Fill.Clone();
                    _clone.UpdateBrushByTheme();
                    this.Stroke = _clone;
                }
                else
                {
                    this.Stroke.UpdateBrushByTheme();
                }
            }
        }

        /// <summary>
        /// Sự kiện áp dụng Template
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (Container != null)
            {
                if (ShapePresent != null && ShapePresent.Parent == null)
                {
                    Container.Children.Add(ShapePresent);
                    Panel.SetZIndex(ShapePresent, -1);
                    if (Content != null && !CanContentFocus && !HasEditableContent) //Ẩn các đối tượng khác
                    {
                        foreach (UIElement item in Container.Children)
                        {
                            if (item is ContentPresenter)
                            {
                                item.Visibility = Visibility.Hidden;
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Lưu trữ đối tượng ra dạng hình ảnh
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="size">Kích thước xuất</param>
        /// <returns></returns>
        public override bool SaveToImage(string imagePath, Size size = new Size())
        {
            return FileHelper.SaveToImageOriginSize(imagePath, this.Container, new Size(this.ActualWidth + this.Thickness, this.ActualHeight + this.Thickness));
        }

        /// <summary>
        /// Lưu trữ đối tượng ra dạng hình ảnh
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="size">Kích thước xuất</param>
        /// <returns></returns>
        public override BitmapSource SaveToBitmap(Size size = new Size())
        {
            return FileHelper.SaveToBitmap(this.Container, new Size(this.ActualWidth + this.Thickness, this.ActualHeight + this.Thickness));
        }

        /// <summary>
        /// Lấy dữ liệu định dạng
        /// </summary>
        /// <returns></returns>
        public override IFormatPainter GetPainter(IFormatPainter painter = null)
        {
            if (painter is StandardPainter _standardPainter)
            {
                base.GetPainter(_standardPainter);
                _standardPainter.CapType = this.CapType;
                _standardPainter.DashType = this.DashType;
                _standardPainter.Effects = this.Effects.ToList();
                _standardPainter.Fill = this.Fill;
                _standardPainter.JoinType = this.JoinType;
                _standardPainter.Stroke = this.Stroke;
                _standardPainter.Thickness = this.Thickness;
                return _standardPainter;
            }
            else
            {
                _standardPainter = new StandardPainter();
                base.GetPainter(_standardPainter);
                _standardPainter.CapType = this.CapType;
                _standardPainter.DashType = this.DashType;
                _standardPainter.Effects = this.Effects.ToList();
                _standardPainter.Fill = this.Fill;
                _standardPainter.JoinType = this.JoinType;
                _standardPainter.Stroke = this.Stroke;
                _standardPainter.Thickness = this.Thickness;
                return _standardPainter;
            }
        }

        /// <summary>
        /// Cài đặt dữ liệu định dạng
        /// </summary>
        /// <param name="painter"></param>
        public override void SetPainter(IFormatPainter painter)
        {
            if (painter is StandardPainter _standardPainter)
            {
                base.SetPainter(_standardPainter);
                this.CapType = _standardPainter.CapType;
                this.DashType = _standardPainter.DashType;
                this.RemoveAllEffects();
                if (_standardPainter.Effects != null)
                    foreach (var item in _standardPainter.Effects)
                    {
                        this.Effects.Add(item.Clone() as FrameEffectBase);
                    }
                this.Fill = _standardPainter.Fill?.Clone();
                this.JoinType = _standardPainter.JoinType;
                this.Stroke = _standardPainter.Stroke?.Clone();
                this.Thickness = _standardPainter.Thickness;
            }
        }

        /// <summary>
        /// Hàm hủy dữ liệu
        /// </summary>
        public override void Dispose()
        {
            this.ShapePresent = null;
            this.RemoveAllEffects();
            this.Fill = null;
            this.Stroke = null;
            base.Dispose();
        }
        #endregion
    }
}

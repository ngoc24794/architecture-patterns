﻿using INV.Elearning.Core.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Interface hỗ trợ hiệu ứng khung
    /// </summary>
    public interface IEffectSupport
    {
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính danh sách các hiệu ứng khung của đối tượng
        /// </summary>
        ObservableCollection<FrameEffectBase> Effects { get; }

        /// <summary>
        /// Cài đặt giá trị hiệu ứng cho khung
        /// </summary>
        /// <param name="effect"></param>
        void SetEffect(FrameEffectBase effect);
        /// <summary>
        /// Xóa tất cả hiệu ứng
        /// </summary>
        void RemoveAllEffects();
    }
}

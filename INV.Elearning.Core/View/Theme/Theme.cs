﻿using INV.Elearning.Core.Model;
using theme = INV.Elearning.Core.Model.Theme;
using System.Collections.ObjectModel;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.ViewModel;
using System.Windows;
using INV.Elearning.Core.Helper;
using System;
using System.Linq;
using System.Windows.Media;

namespace INV.Elearning.Core.View.Theme
{
    /// <summary>
    /// Theme của SlideMaster
    /// </summary>
    public class Theme : RootViewModel
    {
        public ETheme TempTheme { get; set; }
        private EColorManagment _color;
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính quản lý màu cho đối tượng
        /// </summary>
        public EColorManagment Colors
        {
            get { return _color ?? (_color = new EColorManagment()); }
            set
            {
              //  Global.StartGrouping("colors");
                //if (value == _color) return;
                //SlideMaster slide = null;
                //if ((Application.Current as IAppGlobal).SelectedSlide is SlideMaster slidemaster)
                //{
                //    slide = slidemaster;
                //}
                //else if ((Application.Current as IAppGlobal).SelectedSlide is LayoutMaster layoutMaster)
                //{
                //    slide = layoutMaster.SlideParent;
                //}
                //if (slide != null)
                //{
                //    slide.UpdateColor(value);
                //}
                //if (Global.CanPushUndo && (Application.Current.MainWindow.IsLoaded))
                //{
                //    UndoRedoByProperty step = new UndoRedoByProperty();
                //    step.Source = this;
                //    step.OldValue = _color;
                //    step.NewValue = value;
                //    step.PropertyName = "Colors";
                //    Global.PushUndo(step);
                //}
                _color = value;
                OnPropertyChanged("Colors");
               // Global.StopGrouping("colors");
            }
        }


        private theme.EFontfamilyManagment _fontFamlies;
        /// <summary>
        /// Lấy hoặc cài đặt danh sách phông chữ trong theme
        /// </summary>
        public theme.EFontfamilyManagment FontFamilies
        {
            get { return _fontFamlies ?? (_fontFamlies = new theme.EFontfamilyManagment()); }
            set { _fontFamlies = value; OnPropertyChanged("Colors"); }
        }

        private EFontfamily _selectedFont;
        /// <summary>
        /// Danh sach cac kieu phong chu
        /// </summary>
        public EFontfamily SelectedFont
        {
            get { return _selectedFont ?? (_selectedFont = new EFontfamily()); }
            set
            {
                //if (value == _selectedFont) return;
                //SlideMaster slide = null;
                //if ((Application.Current as IAppGlobal).SelectedSlide is SlideMaster slidemaster)
                //{
                //    slide = slidemaster;
                //}
                //else if ((Application.Current as IAppGlobal).SelectedSlide is LayoutMaster layoutMaster)
                //{
                //    slide = layoutMaster.SlideParent;
                //}
                //if (slide != null)
                //{
                //    slide.UpdateFont(value);
                //}
                //if (Global.CanPushUndo && (Application.Current.MainWindow.IsLoaded))
                //{
                //    UndoRedoByProperty step = new UndoRedoByProperty();
                //    step.Source = this;
                //    step.OldValue = _selectedFont;
                //    step.NewValue = value;
                //    step.PropertyName = "SelectedFont";
                //    Global.PushUndo(step);
                //}
                _selectedFont = value;
                OnPropertyChanged("SelectedFont");
            }
        }


        private BackgroundItem _background;
        /// <summary>
        /// Thay đổi background
        /// </summary>
        public BackgroundItem Background
        {
            get { return _background ?? (_background = new BackgroundItem()); }
            set
            {
                if (value == _background) return;
                Global.StartGrouping("background");
                if (_background == null) _background = new BackgroundItem();
                SlideBase slide = (Application.Current as IAppGlobal).SelectedSlide;

                if (slide != null)
                {
                    slide.UpdateBackground(value);
                }
                (Application.Current as IAppGlobal).SelectedTheme.BackgroundStyle = value;

                if (Global.CanPushUndo && (Application.Current.MainWindow.IsLoaded))
                {
                    UndoRedoByProperty step = new UndoRedoByProperty();
                    step.Source = this;
                    step.OldValue = _background;
                    step.NewValue = value;
                    step.PropertyName = "Background";
                    Global.PushUndo(step);
                }
                _background = value;
                OnPropertyChanged("Background");
                Global.StopGrouping("background");
            }
        }




        private ObservableCollection<SlideMaster> slideMasters = null;

        /// <summary>
        /// Danh sách Slide Master có trong Theme
        /// </summary>
        public ObservableCollection<SlideMaster> SlideMasters
        {
            get
            {
                if (slideMasters == null)
                {
                    slideMasters = new ObservableCollection<SlideMaster>();
                    slideMasters.CollectionChanged += SlideMasters_CollectionChanged;
                }
                return slideMasters;
            }
            set
            {
                slideMasters = value;
            }
        }

        /// <summary>
        /// Sự kiện thay đổi danh sách slide master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SlideMasters_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (SlideMaster slide in e.NewItems)
                    {
                        Global.BeginInit();
                        slide.IsRemove = true;
                        slide.UpdateThemeColor();
                        slide.UpdateThemeFont();
                        (Application.Current as IAppGlobal).DocumentControl.Slides.Add(slide);
                        foreach (LayoutMaster layout in slide.LayoutMasters)
                        {
                            layout.UpdateThemeColor();
                            layout.UpdateThemeFont();
                            (Application.Current as IAppGlobal).DocumentControl.Slides.Add(layout);
                        }
                        Global.EndInit();

                        if (Global.CanPushUndo && (Application.Current as IAppGlobal).DocumentControl.IsLoaded) //Ghi nhận Undo
                        {
                            Global.PushUndo(new AddSlideMasterToDocumentStep(slide, SlideMasters, TempTheme, (Application.Current as IAppGlobal).SelectedTheme));
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (SlideMaster slide in e.OldItems)
                    {
                        if (Global.CanPushUndo && (Application.Current as IAppGlobal).DocumentControl.IsLoaded) //Ghi nhận Undo
                        {
                            Global.PushUndo(new RemoveSlideMasterFromDocumentStep(slide, SlideMasters, e.OldStartingIndex));
                        }
                        Global.BeginInit();
                        foreach (LayoutMaster layout in slide.LayoutMasters)
                        {
                            (Application.Current as IAppGlobal).DocumentControl.Slides.Remove(layout);
                        }
                        (Application.Current as IAppGlobal).DocumentControl.Slides.Remove(slide);

                        Global.EndInit();

                    }
                    break;
            }
        }

        private Image _thumbnail;
        /// <summary>
        /// Image Thumbnail
        /// </summary>
        public Image Thumbnail
        {
            get { return _thumbnail; }
            set { _thumbnail = value; }
        }

        private Image _thumbnailIcon;
        /// <summary>
        /// Image Thumbnail Icon
        /// </summary>
        public Image ThumbnailIcon
        {
            get { return _thumbnailIcon; }
            set { _thumbnailIcon = value; }
        }

        private ETheme _data;
        /// <summary>
        /// Lấy data của theme
        /// </summary>
        public ETheme Data
        {
            get { return _data ?? (_data = new ETheme()); }
        }

        private ETheme _selectedTheme;

        public ETheme SelectedTheme
        {
            get { return _selectedTheme; }
            set
            {
                //if (value == _selectedTheme) return;
                //SlideMaster slide = null;
                //if ((Application.Current as IAppGlobal).SelectedSlide is SlideMaster slidemaster)
                //{
                //    slide = slidemaster;
                //}
                //else if ((Application.Current as IAppGlobal).SelectedSlide is LayoutMaster layoutMaster)
                //{
                //    slide = layoutMaster.SlideParent;
                //}
                //if (slide != null)
                //{
                //    slide.UpdateThemeSlideMaster(value);
                //}
                //if (Global.CanPushUndo && (Application.Current.MainWindow.IsLoaded))
                //{
                //    UndoRedoByProperty step = new UndoRedoByProperty();
                //    step.Source = this;
                //    step.PropertyName = "SelectedTheme";
                //    step.OldValue = _selectedTheme;
                //    step.NewValue = value;
                //    Global.PushUndo(step);
                //}
                _selectedTheme = value;
                OnPropertyChanged("SelectedTheme");
            }
        }


        /// <summary>
        /// Update giao diện từ Data
        /// </summary>
        /// <param name="data"></param>
        public void UpdateUI(ETheme data)
        {
            if (data is ETheme eTheme)
            {
                Colors = eTheme.Colors;
                FontFamilies = eTheme.FontFamilies;
                SelectedFont = eTheme.SelectedFont;
                SlideMasters.Clear();
                foreach (ESlideMaster item in eTheme.SlideMasters)
                {
                    SlideMaster slideMaster = new SlideMaster();
                    slideMaster.LoadData(item);
                    slideMaster.ThemeValueChanged += ThemeValueChanged;
                    SlideMasters.Add(slideMaster);
                }
            }
        }

        /// <summary>
        /// Thay doi noi dung theme
        /// </summary>
        /// <param name="source"></param>
        /// <param name="property"></param>
        private void ThemeValueChanged(SlideMaster source, DependencyProperty property)
        {
            if (source != null)
            {
                if (property == SlideMaster.IsDateProperty)
                {

                }
                else if (property == SlideMaster.IsFooterProperty)
                {

                }
            }
        }

        /// <summary>
        /// Lấy data của theme
        /// </summary>
        public void RefreshData()
        {
            if (this.Data is ETheme eTheme)
            {
                eTheme.Colors = Colors;
                eTheme.FontFamilies = FontFamilies;
                eTheme.SelectedFont = SelectedFont;
                eTheme.SlideMasters.Clear();
                foreach (SlideMaster item in SlideMasters)
                {
                    ESlideMaster eSlideMaster = new ESlideMaster() { };
                    item.RefreshData();
                    eSlideMaster = item.Data as ESlideMaster;
                    eTheme.SlideMasters.Add(eSlideMaster);
                }
            }
        }

    }
    /// <summary>
    /// Model cho màu background
    /// </summary>
    public class BackgroundItem : RootModel
    {

        private Brush _colorStyle;

        public Brush ColorStyle
        {
            get { return _colorStyle; }
            set { _colorStyle = value; OnPropertyChanged("ColorStyle"); }
        }

        private ColorGradientBrush _colorDataStyle;

        public ColorGradientBrush ColorDataStyle
        {
            get { return _colorDataStyle; }
            set { _colorDataStyle = value; }
        }


        public override IElearningElement Clone()
        {
            return null;
        }
    }

}

﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Core.View.Theme
{
    /// <summary>
    /// Layout Master
    /// </summary>
    public class LayoutMaster : SlideBase
    {

      


        public bool IsCustomLayout
        {
            get { return (bool)GetValue(IsCustomLayoutProperty); }
            set { SetValue(IsCustomLayoutProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsCustomLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCustomLayoutProperty =
            DependencyProperty.Register("IsCustomLayout", typeof(bool), typeof(LayoutMaster), new PropertyMetadata(false));



     



        /// <summary>
        /// SlideMaster cha
        /// </summary>
        public SlideMaster SlideParent
        {
            get { return (SlideMaster)GetValue(SlideParentProperty); }
            set { SetValue(SlideParentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SlideParent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SlideParentProperty =
            DependencyProperty.Register("SlideParent", typeof(SlideMaster), typeof(LayoutMaster), new PropertyMetadata(null));


        /// <summary>
        /// Kiểu Layout
        /// </summary>
        public LayoutType LayoutType
        {
            get { return (LayoutType)GetValue(LayoutTypeProperty); }
            set { SetValue(LayoutTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutTypeProperty =
            DependencyProperty.Register("LayoutType", typeof(LayoutType), typeof(LayoutMaster), new PropertyMetadata(LayoutType.None));


        /// <summary>
        /// Tên layout
        /// </summary>
        public string LayoutName
        {
            get { return (string)GetValue(LayoutNameProperty); }
            set { SetValue(LayoutNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutNameProperty =
            DependencyProperty.Register("LayoutName", typeof(string), typeof(LayoutMaster), new PropertyMetadata(string.Empty, LayoutNameCallback));

        private static void LayoutNameCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LayoutMaster _owner = d as LayoutMaster;
            //_owner.SlideName = string.Format("{0}:  uses by {1} slides", _owner.LayoutName, _owner.LayoutCount);
            _owner.SlideName = string.Format("{0} {1} {2} {3}", _owner.LayoutName, FileHelper.FindResource("COREELAYOUTMASTER_uses")?.ToString(), _owner.LayoutCount, FileHelper.FindResource("COREELAYOUTMASTER_slides")?.ToString());
        }


        /// <summary>
        /// Đếm số layout được sử dụng
        /// </summary>
        public int LayoutCount
        {
            get { return (int)GetValue(LayoutCountProperty); }
            set { SetValue(LayoutCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutCountProperty =
            DependencyProperty.Register("LayoutCount", typeof(int), typeof(LayoutMaster), new PropertyMetadata(0, LayoutCountPropertyChanged));

        private static void LayoutCountPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LayoutMaster layoutMaster = d as LayoutMaster;
            //layoutMaster.SlideName = string.Format("{0}: uses by {1} slides", layoutMaster.LayoutName, e.NewValue);
            layoutMaster.SlideName = string.Format("{0} {1} {2} {3}", layoutMaster.LayoutName, FileHelper.FindResource("COREELAYOUTMASTER_uses")?.ToString(), e.NewValue, FileHelper.FindResource("COREELAYOUTMASTER_slides")?.ToString());
        }

        public bool IsMainLayoutMaster
        {
            get { return (bool)GetValue(IsMainLayoutMasterProperty); }
            set { SetValue(IsMainLayoutMasterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMainLayoutMaster.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMainLayoutMasterProperty =
            DependencyProperty.Register("IsMainLayoutMaster", typeof(bool), typeof(LayoutMaster), new PropertyMetadata(false));


        /// <summary>
        /// Theme hiện tại của Layout Master
        /// </summary>
        public Theme Theme
        {
            get { return (Theme)GetValue(ThemeProperty); }
            set { SetValue(ThemeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Theme.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeProperty =
            DependencyProperty.Register("Theme", typeof(Theme), typeof(LayoutMaster), new PropertyMetadata(null));


        private ObservableCollection<string> _listSlideID;

        public ObservableCollection<string> ListSlideID
        {
            get
            {
                if (_listSlideID == null)
                {
                    _listSlideID = new ObservableCollection<string>();
                    _listSlideID.CollectionChanged += _listSlideID_CollectionChanged; ;
                }
                return _listSlideID;
            }
            set { _listSlideID = value; }
        }

        private void _listSlideID_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LayoutCount = ListSlideID.Count;
        }

        public ELayoutMaster RenderData
        {
            get { return (ELayoutMaster)GetValue(RenderDataProperty); }
            set { SetValue(RenderDataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RenderData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RenderDataProperty =
            DependencyProperty.Register("RenderData", typeof(ELayoutMaster), typeof(LayoutMaster), new PropertyMetadata(null, new PropertyChangedCallback(RenderDataPropertyChanged)));

        private static void RenderDataPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ELayoutMaster eLayoutMaster = e.NewValue as ELayoutMaster;
            LayoutMaster layoutMaster = d as LayoutMaster;
            layoutMaster.MainLayout.Background = Brushes.Red;
            layoutMaster.LoadData(eLayoutMaster);
        }

        public LayoutMaster()
        {
            Data = new ELayoutMaster();
        }

        #region IsUpdateFooter

        /// <summary>
        /// Kiểm tra xem có cập nhật chân trang hay không
        /// </summary>
        public bool IsUpdateFooter
        {
            get { return (bool)GetValue(IsUpdateFooterProperty); }
            set { SetValue(IsUpdateFooterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsUpdateFooter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsUpdateFooterProperty =
            DependencyProperty.Register("IsUpdateFooter", typeof(bool), typeof(LayoutMaster), new PropertyMetadata(false));


        #endregion

        #region IsUpdateDateTime

        /// <summary>
        /// Kiểm tra xem có cập nhật ngày giờ hay không
        /// </summary>
        public bool IsUpdateDateTime
        {
            get { return (bool)GetValue(IsUpdateDateTimeProperty); }
            set { SetValue(IsUpdateDateTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsUpdateDateTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsUpdateDateTimeProperty =
            DependencyProperty.Register("IsUpdateDateTime", typeof(bool), typeof(LayoutMaster), new PropertyMetadata(false));


        #endregion

        #region IsUpdateSlideNumber

        /// <summary>
        /// Kiểm tra xem có cập nhật số trang hay không
        /// </summary>
        public bool IsUpdateSlideNumber
        {
            get { return (bool)GetValue(IsUpdateSlideNumberProperty); }
            set { SetValue(IsUpdateSlideNumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsUpdateSlideNumber.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsUpdateSlideNumberProperty =
            DependencyProperty.Register("IsUpdateSlideNumber", typeof(bool), typeof(LayoutMaster), new PropertyMetadata(false));


        #endregion


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
        }




        /// <summary>
        /// Lấy data từ view
        /// </summary>
        public override void RefreshData()
        {
            base.RefreshData();
            if (this.Data is ELayoutMaster eLayoutMaster)
            {
                eLayoutMaster.LayoutName = LayoutName;
                eLayoutMaster.LayountCount = LayoutCount;
                eLayoutMaster.LayoutType = LayoutType;
                eLayoutMaster.SlideName = SlideName;
                eLayoutMaster.Background = ColorHelper.ConverterFromColor(GradienToSolid(Background));
                eLayoutMaster.ID = ID;
                MainLayout.RefreshData();
                eLayoutMaster.TextFooter = TextFooter;
                eLayoutMaster.MainLayer = MainLayout.Data as PageLayer;
                eLayoutMaster.ListSlideID.Clear();
                eLayoutMaster.IsUpdateFooter = IsUpdateFooter;
                eLayoutMaster.IsUpdateDateTime = IsUpdateDateTime;
                eLayoutMaster.IsUpdateSlideNumber = IsUpdateSlideNumber;
                foreach (var item in ListSlideID)
                {
                    eLayoutMaster.ListSlideID.Add(item);
                }
            }
        }

        /// <summary>
        /// Xuất view từ data
        /// </summary>
        /// <param name="data"></param>
        public override void LoadData(PageElementBase data)
        {
            if (data is ELayoutMaster eLayoutMaster)
            {
                SlideName = eLayoutMaster.SlideName;
                LayoutName = eLayoutMaster.LayoutName;
                LayoutCount = eLayoutMaster.LayountCount;
                LayoutType = eLayoutMaster.LayoutType;
                IsUpdateFooter = eLayoutMaster.IsUpdateFooter;
                IsUpdateSlideNumber = eLayoutMaster.IsUpdateSlideNumber;
                IsUpdateDateTime = eLayoutMaster.IsUpdateDateTime;
                if (ThemeLayout == null)
                {
                    ThemeLayout = new ThemeLayout();
                }
                this.IsTitle = data.IsTitle;
                this.IsFooter = data.IsFooters;
                this.IsText = data.IsText;
                this.IsDate = data.IsDate;
                this.IsSlideNumber = data.IsSlideNumber;
                this.IsHideBackground = data.IsHideBackground;
                this.IsFollowMasterBackground = data.IsFollowBackground;
                this.TextFooter = data.TextFooter;
                if (Global.IsPasting) //Nếu đang dán thì tạo mới id
                    this.ID = Helper.ObjectElementsHelper.RandomString(13);
                this.CanShowInMenu = data.CanShowInMenu;
                this.Transition = data.Transition;
                this.Note = data.Note;
                // Background = ColorHelper.ConverFromColorData(eLayoutMaster.Background).Brush;
                ID = eLayoutMaster.ID;
                MainLayout.UpdateUI(eLayoutMaster.MainLayer);

                ThemeLayout.UpdateUI(data.MainLayer);
                this.IsHideBackground = data.IsHideBackground;

                if (IsHideBackground)
                {
                    ThemeLayout.Visibility = Visibility.Hidden;
                }
                else
                {
                    ThemeLayout.Visibility = Visibility.Visible;
                }
                ListSlideID.Clear();
                foreach (var item in eLayoutMaster.ListSlideID)
                {
                    ListSlideID.Add(item);
                }
                MainLayout.UpdateThumbnail();

            }
        }


        private ColorBrushBase GradienToSolid(Brush brush)
        {
            if (brush is GradientBrush gradientBrush)
            {
                ColorGradientBrush colorGradientBrush = new ColorGradientBrush();
                colorGradientBrush.Brush = gradientBrush;
                colorGradientBrush.GradientType = Core.Model.GradientColorType.Linear;
                colorGradientBrush.Angle = 0;
                return colorGradientBrush;
            }
            else if (brush is SolidColorBrush solid)
            {
                return new ColorSolidBrush() { Color = solid.Color };
            }
            return null;
        }

        public void UpdateLayoutTheme()
        {
            if (this.SlideParent != null)
            {
                if (this.ThemeLayout == null) this.ThemeLayout = new ThemeLayout();
                if (this.SlideParent.MainLayout != null)
                {
                    this.SlideParent.MainLayout.RefreshData();
                    this.ThemeLayout.UpdateUIB(this.SlideParent.MainLayout.Data);
                }
            }
        }

        /// <summary>
        /// Giải phóng bộ nhớ LayoutMaster
        /// </summary>
        public void Dispose()
        {
            for (int i = 0; i < MainLayout.Elements.Count; i++)
            {
                ObjectElement objectElement = MainLayout.Elements[i];
                objectElement.Dispose();
                MainLayout.Elements.RemoveAt(i);
                objectElement = null;
                i--;
            }
        }
    }
}

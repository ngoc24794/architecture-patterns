﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace INV.Elearning.Core.View.Theme
{
    public class SlideMaster : SlideBase
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }


        public bool IsCustomSlide
        {
            get { return (bool)GetValue(IsCustomSlideProperty); }
            set { SetValue(IsCustomSlideProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsCustomSlide.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCustomSlideProperty =
            DependencyProperty.Register("IsCustomSlide", typeof(bool), typeof(SlideMaster), new PropertyMetadata(false, OnPropertyCallBack));



        private static void OnPropertyCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideMaster _owner = d as SlideMaster;
            _owner.ThemeValueChanged?.Invoke(_owner, e.Property);
        }


        /// <summary>
        /// Tên của theme Slide
        /// </summary>
        public string ThemesName
        {
            get { return (string)GetValue(ThemesNameProperty); }
            set { SetValue(ThemesNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemesName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemesNameProperty =
            DependencyProperty.Register("ThemesName", typeof(string), typeof(SlideMaster), new PropertyMetadata(null, ThemesNamePropertyCallback));

        private static void ThemesNamePropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideMaster _owner = d as SlideMaster;
         //   _owner.SlideName = string.Format("{0}: uses by {1} slides", _owner.ThemesName, _owner.ThemeCount);
            _owner.SlideName = string.Format("{0} {1} {2} {3}", _owner.ThemesName, FileHelper.FindResource("COREELAYOUTMASTER_uses")?.ToString(), _owner.ThemeCount, FileHelper.FindResource("COREELAYOUTMASTER_slides")?.ToString());

        }


        /// <summary>
        /// Đếm số Slide sử dụng theme
        /// </summary>
        public int ThemeCount
        {
            get { return (int)GetValue(ThemeCountProperty); }
            set { SetValue(ThemeCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeCountProperty =
            DependencyProperty.Register("ThemeCount", typeof(int), typeof(SlideMaster), new PropertyMetadata(0, ThemeCountPropertyCallback));

        private static void ThemeCountPropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideMaster _owner = d as SlideMaster;
           // _owner.SlideName = string.Format("{0}: uses by {1} slides", _owner.ThemesName, _owner.ThemeCount);
            _owner.SlideName = string.Format("{0} {1} {2} {3}", _owner.ThemesName, FileHelper.FindResource("COREELAYOUTMASTER_uses")?.ToString(), _owner.ThemeCount, FileHelper.FindResource("COREELAYOUTMASTER_slides")?.ToString());
        }




        /// <summary>
        /// Màu được chọn
        /// </summary>
        public EColorManagment SelectedColor
        {
            get { return (EColorManagment)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedColorProperty =
            DependencyProperty.Register("SelectedColor", typeof(EColorManagment), typeof(SlideMaster), new PropertyMetadata(new EColorManagment(), SelectedColorPropertyChanged));

        private static void SelectedColorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Global.StartGrouping("colors");
            SlideMaster _owner = d as SlideMaster;
            _owner.UpdateColor(e.NewValue as EColorManagment);
            if (Global.CanPushUndo && (_owner.IsLoaded))
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.Source = _owner;
                step.OldValue = e.OldValue;
                step.NewValue = e.NewValue;
                step.PropertyName = "SelectedColor";
                Global.PushUndo(step);
            }
            Global.StopGrouping("colors");
        }



        public bool IsChangeBackground
        {
            get { return (bool)GetValue(IsChangeBackgroundProperty); }
            set { SetValue(IsChangeBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsChangeBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsChangeBackgroundProperty =
            DependencyProperty.Register("IsChangeBackground", typeof(bool), typeof(SlideMaster), new PropertyMetadata(true));

        #region IsUpdateFooter

        /// <summary>
        /// Kiểm tra xem có cập nhật chân trang hay không
        /// </summary>
        public bool IsUpdateFooter
        {
            get { return (bool)GetValue(IsUpdateFooterProperty); }
            set { SetValue(IsUpdateFooterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsUpdateFooter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsUpdateFooterProperty =
            DependencyProperty.Register("IsUpdateFooter", typeof(bool), typeof(SlideMaster), new PropertyMetadata(false));


        #endregion

        #region IsUpdateDateTime

        /// <summary>
        /// Kiểm tra xem có cập nhật ngày giờ hay không
        /// </summary>
        public bool IsUpdateDateTime
        {
            get { return (bool)GetValue(IsUpdateDateTimeProperty); }
            set { SetValue(IsUpdateDateTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsUpdateDateTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsUpdateDateTimeProperty =
            DependencyProperty.Register("IsUpdateDateTime", typeof(bool), typeof(SlideMaster), new PropertyMetadata(false));


        #endregion

        #region IsUpdateSlideNumber

        /// <summary>
        /// Kiểm tra xem có cập nhật số trang hay không
        /// </summary>
        public bool IsUpdateSlideNumber
        {
            get { return (bool)GetValue(IsUpdateSlideNumberProperty); }
            set { SetValue(IsUpdateSlideNumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsUpdateSlideNumber.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsUpdateSlideNumberProperty =
            DependencyProperty.Register("IsUpdateSlideNumber", typeof(bool), typeof(SlideMaster), new PropertyMetadata(false));


        #endregion

        /// <summary>
        /// Theme đang được chọn
        /// </summary>
        public ETheme SelectedTheme
        {
            get { return (ETheme)GetValue(SelectedThemeProperty); }
            set { SetValue(SelectedThemeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedTheme.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedThemeProperty =
            DependencyProperty.Register("SelectedTheme", typeof(ETheme), typeof(SlideMaster), new PropertyMetadata(null, SelectedThemePropertyChanged));

        private static void SelectedThemePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideMaster _owner = d as SlideMaster;
            if (e.NewValue != null)
            {
                if (Global.CanPushUndo && (_owner.IsLoaded))
                {
                    UndoRedoByProperty step = new UndoRedoByProperty();
                    step.Source = _owner;
                    step.OldValue = e.OldValue;
                    step.NewValue = e.NewValue;
                    step.PropertyName = "SelectedTheme";
                    Global.PushUndo(step);
                }
                _owner.SelectedThemeId = (e.NewValue as ETheme).ID;
                Global.BeginInit("theme");
                if (e.NewValue != null && e.OldValue != null)
                {
                    GetListIDSlide(e.NewValue as ETheme, e.OldValue as ETheme);
                }
                if (_owner.IsChangeBackground)
                    _owner.UpdateThemeSlideMaster(e.NewValue as ETheme);

                Global.EndInit("theme");
            }
        }

        /// <summary>
        /// Lấy ListSlideID
        /// </summary>
        /// <param name="oldTheme"></param>
        /// <param name="newTheme"></param>
        private static void GetListIDSlide(ETheme oldTheme, ETheme newTheme)
        {
            foreach (var oldT in oldTheme.SlideMasters[0].LayoutMasters)
            {
                foreach (var newT in newTheme.SlideMasters[0].LayoutMasters)
                {
                    if (oldT.LayoutType == newT.LayoutType)
                    {
                        newT.ListSlideID = oldT.ListSlideID;
                    }
                }
            }

            foreach (var oldLayout in oldTheme.SlideMasters[0].LayoutMasters)
            {
                if (oldLayout.LayoutType == LayoutType.Custom || oldLayout.LayoutType == LayoutType.None)
                {
                    ELayoutMaster eLayoutMaster = CheckLayoutBlank(newTheme.SlideMasters[0]);
                    if (eLayoutMaster != null)
                    {
                        foreach (var item in oldLayout.ListSlideID)
                        {
                            eLayoutMaster.ListSlideID.Add(item);
                        }
                    }
                    else
                    {
                        foreach (var item in oldLayout.ListSlideID)
                        {
                            newTheme.SlideMasters[0]?.LayoutMasters[0]?.ListSlideID.Add(item);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Kiểm tra xem có Layout Blank hay không
        /// </summary>
        /// <param name="eSlideMaster"></param>
        /// <returns></returns>
        private static ELayoutMaster CheckLayoutBlank(ESlideMaster eSlideMaster)
        {
            foreach (var item in eSlideMaster.LayoutMasters)
            {
                if (item.LayoutType == LayoutType.Blank)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        /// Theme ID đang được lựa chọn
        /// </summary>
        public string SelectedThemeId
        {
            get { return (string)GetValue(SelectedThemeIdProperty); }
            set { SetValue(SelectedThemeIdProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedThemeId.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedThemeIdProperty =
            DependencyProperty.Register("SelectedThemeId", typeof(string), typeof(SlideMaster), new PropertyMetadata(string.Empty, SelectedThemeIdPropertyChanged));

        private static void SelectedThemeIdPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //SlideMaster _owner = d as SlideMaster;
            //if (e.NewValue != null)
            //{
            //    if (Global.CanPushUndo && (_owner.IsLoaded))
            //    {
            //        UndoRedoByProperty step = new UndoRedoByProperty();
            //        step.Source = _owner;
            //        step.OldValue = e.OldValue;
            //        step.NewValue = e.NewValue;
            //        step.PropertyName = "SelectedThemeId";
            //        Global.PushUndo(step);
            //    }
            //    Global.BeginInit("theme");
            //    foreach (ETheme item in (Application.Current as IAppGlobal).ThemesCollection)
            //    {
            //        if (e.NewValue.ToString() == item.ID)
            //        {
            //            _owner.UpdateThemeSlideMaster(item);
            //            break;
            //        }
            //    }

            //    Global.EndInit("theme");
            //}

        }





        /// <summary>
        /// Font được chọn
        /// </summary>
        public EFontfamily SelectedFont
        {
            get { return (EFontfamily)GetValue(SelectedFontProperty); }
            set { SetValue(SelectedFontProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedFont.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedFontProperty =
            DependencyProperty.Register("SelectedFont", typeof(EFontfamily), typeof(SlideMaster), new PropertyMetadata(new EFontfamily(), SelectedFontPropertyChanged));

        private static void SelectedFontPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Global.StartGrouping("fonts");
            SlideMaster _owner = d as SlideMaster;
            _owner.UpdateFont(e.NewValue as EFontfamily);
            if (Global.CanPushUndo && (_owner.IsLoaded))
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.Source = _owner;
                step.OldValue = e.OldValue;
                step.NewValue = e.NewValue;
                step.PropertyName = "SelectedFont";
                Global.PushUndo(step);
            }
            Global.StopGrouping("fonts");

        }

        private ObservableCollection<LayoutMaster> _layoutMasters;

        public ObservableCollection<LayoutMaster> LayoutMasters
        {
            get
            {
                if (_layoutMasters == null)
                {
                    _layoutMasters = new ObservableCollection<LayoutMaster>();
                    _layoutMasters.CollectionChanged += LayoutCollectionChanged;
                }
                return _layoutMasters;
            }
            set { _layoutMasters = value; }
        }

        private void LayoutCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (LayoutMaster item in e.NewItems)
                    {
                        item.SlideParent = this;
                        if (this.Parent != null && item.Parent == null)
                        {
                            // (this.Parent as Panel).Children.Add(item);
                            item.IsRemove = true;
                            (Application.Current as IAppGlobal).DocumentControl.Slides.Add(item);
                            if (this.MainLayout != null)
                            {
                                this.MainLayout.RefreshData();
                                item.ThemeLayout = new ThemeLayout();
                                item.ThemeLayout.UpdateUI(this.MainLayout.Data);
                            }
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    if (this.Parent == null) break;
                    foreach (LayoutMaster item in e.OldItems)
                    {
                        if (item.Parent != null)
                        {
                            (Application.Current as IAppGlobal).DocumentControl.Slides.Remove(item);
                            //(this.Parent as Panel).Children.Remove(item);
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    if (this.Parent == null) break;
                    (this.Parent as Panel).Children.Clear();
                    break;
                default:
                    break;
            }
        }

        public delegate void ThemeValueChange(SlideMaster source, DependencyProperty property);
        public ThemeValueChange ThemeValueChanged { set; get; }


        public SlideMaster()
        {
            Data = new ESlideMaster();
            Loaded += SlideMaster_Loaded;

        }

        private void SlideMaster_Loaded(object sender, RoutedEventArgs e)
        {
            // MainLayout.Elements.CollectionChanged += Elements_CollectionChanged;
        }


        /// <summary>
        /// Xử lý sự kiện khi thêm, xóa phần tử trong SlideMaster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Elements_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    ObjectElement addObject = e.NewItems[0] as ObjectElement;
                    Type type = addObject.GetType();
                    if (addObject.TargetName == string.Empty)
                    {
                        addObject.TargetName = "AddObject" + CountObjectAddToSlide();
                    }
                    addObject.RefreshData();
                    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                    {
                        MainLayout.UpdateThumbnail();
                    }));
                    Global.BeginInit();
                    foreach (LayoutMaster layout in LayoutMasters)
                    {
                        foreach (ObjectElement item1 in layout.MainLayout.Elements)
                        {
                            if (item1.TargetName == addObject.TargetName)
                            {

                            }
                        }
                        ObjectElementBase objectElementBase = addObject.Data;
                        var _element = ObjectElementsHelper.LoadData(objectElementBase);
                        _element.IsHitTestVisible = false;
                        _element.ZIndex = 1;
                        layout.MainLayout.Elements.Add(_element);
                        layout.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                        {
                            layout.MainLayout.UpdateThumbnail();
                        }));
                    }
                    Global.EndInit();
                    break;
                case NotifyCollectionChangedAction.Remove:
                    ObjectElement removeItem = e.OldItems[0] as ObjectElement;
                    string removeItemName = removeItem.TargetName;
                    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                    {
                        MainLayout.UpdateThumbnail();
                    }));
                    if (removeItemName != string.Empty)
                    {
                        Global.BeginInit();
                        foreach (LayoutMaster layout in LayoutMasters)
                        {
                            for (int i = 0; i < layout.MainLayout.Elements.Count; i++)
                            {
                                ObjectElement element = layout.MainLayout.Elements[i] as ObjectElement;
                                if (element.TargetName == removeItemName)
                                {
                                    layout.MainLayout.Elements.Remove(element);
                                }
                            }
                            layout.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                            {
                                layout.MainLayout.UpdateThumbnail();
                            }));
                        }
                        Global.EndInit();
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    break;
                default:
                    break;
            }
        }

        private int CountObjectAddToSlide()
        {
            int count = 0;
            foreach (ObjectElement item in MainLayout.Elements)
            {
                if (item.TargetName.Contains("AddObject"))
                {
                    string subString = item.TargetName.Substring(9);
                    int convert = int.Parse(subString);
                    if (convert >= count)
                    {
                        count = convert + 1;
                    }
                }
            }
            return count;
        }

        public override void LoadData(PageElementBase data)
        {
            if (data is ESlideMaster eSlideMaster)
            {
                SlideName = eSlideMaster.SlideName;
                ID = eSlideMaster.ID;
                ThemesName = eSlideMaster.ThemesName;
                ThemeCount = eSlideMaster.ThemeCount;
                this.IsTitle = data.IsTitle;
                this.IsFooter = data.IsFooters;
                this.IsText = data.IsText;
                this.IsDate = data.IsDate;
                this.IsSlideNumber = data.IsSlideNumber;
                this.IsUpdateSlideNumber = eSlideMaster.IsUpdateSlideNumber;
                this.IsUpdateDateTime = eSlideMaster.IsUpdateDateTime;
                this.IsUpdateFooter = eSlideMaster.IsUpdateFooter;
                this.SelectedThemeId = eSlideMaster.SelectedThemeID;
                this.TextFooter = data.TextFooter;
                if (Global.IsPasting) //Nếu đang dán thì tạo mới id
                    this.ID = Helper.ObjectElementsHelper.RandomString(13);
                this.CanShowInMenu = data.CanShowInMenu;
                this.Transition = data.Transition;
                this.Note = data.Note;
                //Color = eSlideMaster.Color;
                MainLayout.UpdateUI(eSlideMaster.MainLayer);
                LayoutMasters.Clear();
                Global.BeginInit();
                foreach (ELayoutMaster item in eSlideMaster.LayoutMasters)
                {
                    LayoutMaster _layout = new LayoutMaster();
                    _layout.LoadData(item);
                    LayoutMasters.Add(_layout);
                }
                Global.EndInit();
            }
        }

        public override void RefreshData()
        {
            base.RefreshData();
            if (this.Data is ESlideMaster eSlideMaster)
            {
                eSlideMaster.SlideName = SlideName;
                eSlideMaster.ThemesName = ThemesName;
                eSlideMaster.ThemeCount = ThemeCount;
                eSlideMaster.SelectedThemeID = SelectedThemeId;
                eSlideMaster.IsUpdateDateTime = IsUpdateDateTime;
                eSlideMaster.IsUpdateFooter = IsUpdateFooter;
                eSlideMaster.IsUpdateSlideNumber = IsUpdateSlideNumber;
                //eSlideMaster.Color = Color;
                eSlideMaster.ID = ID;
                MainLayout.RefreshData();
                eSlideMaster.LayoutMasters.Clear();
                foreach (LayoutMaster item in LayoutMasters)
                {
                    ELayoutMaster eLayoutMaster = new ELayoutMaster();
                    item.RefreshData();
                    eLayoutMaster = item.Data as ELayoutMaster;
                    eSlideMaster.LayoutMasters.Add(eLayoutMaster);
                }
            }
        }




        /// <summary>
        /// Xử lý sự kiện khi thay đổi thuộc tính element trong SlideMaster
        /// </summary>
        public override void OnPropertyChangeByUndo()
        {
            base.OnPropertyChangeByUndo();

            MainLayout.UpdateThumbnail();
            RefreshData();
            //(Application.Current as IAppGlobal).SelectedTheme.SlideMasters[0] = Data as ESlideMaster;
            //(Application.Current as IAppGlobal).LocalThemesCollection[0].SlideMasters[0] = Data as ESlideMaster;
            Global.BeginInit();
            foreach (LayoutMaster item in LayoutMasters)
            {
                item.UpdateLayoutTheme();
                item.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                {
                    item.MainLayout.UpdateThumbnail();
                }));
            }
            Global.EndInit();
        }

        /// <summary>
        /// Cập nhật Slide Master theo theme
        /// </summary>
        /// <param name="eTheme"></param>
        public void UpdateThemeSlideMaster(ETheme eTheme)
        {
            ChangeThemeBackground(eTheme);

        }

        /// <summary>
        /// Cập nhật màu
        /// </summary>
        /// <param name="eColor"></param>
        public void UpdateColor(EColorManagment eColor)
        {
            (Application.Current as IAppGlobal).UpdateColor(eColor, this);
        }

        /// <summary>
        /// Cập nhật fonts
        /// </summary>
        /// <param name="eFontfamily"></param>
        public void UpdateFont(EFontfamily eFontfamily)
        {
            (Application.Current as IAppGlobal).UpdateFont(eFontfamily, this);
        }

        /// <summary>
        /// Thay đổi background khi lựa chọn theme
        /// </summary>
        /// <param name="eTheme"></param>
        public void ChangeThemeBackground(ETheme eTheme)
        {
            Global.BeginInit();
            string id = (Application.Current as IAppGlobal).SelectedTheme.ID;
            ETheme cloneTheme = eTheme.Clone() as ETheme;
            cloneTheme.Name = eTheme.Name;
            cloneTheme.TagName = "This Presenter";
            cloneTheme.ID = id;
            var index = (Application.Current as IAppGlobal).LocalThemesCollection.IndexOf((Application.Current as IAppGlobal).SelectedTheme);
            if (index != -1)
            {
                (Application.Current as IAppGlobal).LocalThemesCollection.RemoveAt(index);
                (Application.Current as IAppGlobal).LocalThemesCollection.Insert(index, cloneTheme);
            }
            (Application.Current as IAppGlobal).SelectedTheme = cloneTheme;
            SelectedThemeId = cloneTheme.ID;

            if ((Application.Current as IAppGlobal).SelectedTheme.TagName == "This Presenter")
            {
                (Application.Current as IAppGlobal).SelectedThemeView.SelectedFont = eTheme.SelectedFont;
                (Application.Current as IAppGlobal).SelectedThemeView.Colors = eTheme.Colors;
                SelectedColor = eTheme.Colors;
            }
            RemoveBackgroundSlideMasterShapes(this);

            ObservableCollection<StandardElement> StandardElements = new ObservableCollection<StandardElement>();
            foreach (var item in eTheme.SlideMasters[0].MainLayer.Children)
            {
                if (item is EStandardElement eThemeShape && eThemeShape.IsGraphicBackground)
                {
                    StandardElement StandardElement = new StandardElement();
                    StandardElement.UpdateUI(eThemeShape);
                    StandardElements.Add(StandardElement);
                }
            }

            this.MainLayout.Fill = new ColorSolidBrush() { Color = Colors.Transparent, ColorSpecialName = "Transparent" };



            int maxZindex = GetMaxZIndexShapeBackground(StandardElements);
            foreach (ObjectElement objectelement in MainLayout.Elements)
            {
                objectelement.ZIndex += (maxZindex + 1);
            }
            foreach (StandardElement item in StandardElements)
            {

                MainLayout.Elements.Add(item as StandardElement);
            }

            foreach (var layout in LayoutMasters)
            {
                foreach (var elayout in cloneTheme.SlideMasters[0].LayoutMasters)
                {
                    if (layout.LayoutType == elayout.LayoutType)
                    {
                        elayout.ListSlideID.Clear();
                        elayout.ListSlideID = layout.ListSlideID;
                    }
                }
            }

            //Xoá các phần LayoutMaster trong SlideMaster

            Dictionary<List<ObjectElementBase>, LayoutType> objectLayoutDict = new Dictionary<List<ObjectElementBase>, LayoutType>();

            for (int i = 0; i < LayoutMasters.Count; i++)
            {
                LayoutMaster layoutMaster = LayoutMasters[i];
                List<ObjectElementBase> customObjects = new List<ObjectElementBase>();
                foreach (var item in layoutMaster.MainLayout.Elements)
                {
                    if (item != null && item.IsCustomGraphics && !(item.ToString() == "INV.Elearning.Text.Views.TextEditor" || item.ToString() == "INV.Elearning.DesignControl.Views.PlaceHolder"))
                    {
                        item.RefreshData();
                        ObjectElementBase objectElementBase = item.Data.Clone() as ObjectElementBase;
                        customObjects.Add(objectElementBase);
                    }
                    else if (item != null && item.ToString() == "INV.Elearning.DesignControl.Views.PlaceHolder" && !item.IsCustomGraphics)
                    {
                        item.RefreshData();
                        ObjectElementBase objectElementBase = item.Data.Clone() as ObjectElementBase;
                        customObjects.Add(objectElementBase);
                    }
                }
                if (layoutMaster.LayoutType == LayoutType.Custom || layoutMaster.LayoutType == LayoutType.None)
                {
                    if (layoutMaster.ListSlideID.Count > 0)
                    {
                        ELayoutMaster eLayoutMaster = GetELayoutMasterByType(cloneTheme.SlideMasters[0]);
                        foreach (var item in layoutMaster.ListSlideID)
                        {
                            if (eLayoutMaster != null)
                            {
                                eLayoutMaster.ListSlideID.Add(item);
                            }
                            else
                            {
                                cloneTheme.SlideMasters[0].LayoutMasters[0].ListSlideID.Add(item);
                            }
                        }
                    }
                }
                objectLayoutDict.Add(customObjects, layoutMaster.LayoutType);
                LayoutMasters.RemoveAt(i);
                layoutMaster.Dispose();
                layoutMaster = null;
                i--;
            }
            foreach (ELayoutMaster item in cloneTheme.SlideMasters[0].LayoutMasters)
            {
                LayoutMaster layoutMasters = new LayoutMaster();
                layoutMasters.LoadData(item);
                foreach (var child in objectLayoutDict)
                {
                    if (child.Value == layoutMasters.LayoutType)
                    {
                        foreach (var objectElementBase in child.Key)
                        {
                            ObjectElement objectElement = ObjectElementsHelper.LoadData(objectElementBase);
                            if (objectElement != null)
                            {
                                objectElement.ZIndex = layoutMasters.GetMaxZIndex() + 1;
                                layoutMasters.MainLayout.Elements.Add(objectElement);
                            }
                        }
                    }
                }
                LayoutMasters.Add(layoutMasters);
            }
            //(Application.Current as IAppGlobal).UpdateLayoutsByETheme(this, eTheme);

            foreach (LayoutMaster layout in LayoutMasters)
            {
                layout.MainLayout.Fill = new ColorSolidBrush() { Color = Colors.Transparent, ColorSpecialName = "Transparent" };
                layout.UpdateLayoutTheme();
                if (layout.IsHideBackground)
                {
                    layout.ThemeLayout.Visibility = Visibility.Hidden;
                }
                else
                {
                    layout.ThemeLayout.Visibility = Visibility.Visible;
                }
            }
            SlideHelper.UnSlectedAll();
            IsSelected = true;
            Global.EndInit();
        }


        private ELayoutMaster GetELayoutMasterByType(ESlideMaster eSlideMaster)
        {
            foreach (var item in eSlideMaster.LayoutMasters)
            {
                if (item.LayoutType == LayoutType.Blank)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        /// Xóa toàn bộ hình nền của Slide Master
        /// </summary>
        /// <param name="slide"></param>
        private static void RemoveBackgroundSlideMasterShapes(SlideMaster slide)
        {
            Global.BeginInit();
            for (int i = 0; i < slide.MainLayout.Elements.Count; i++)
            {
                if (slide.MainLayout.Elements[i] is StandardElement && (slide.MainLayout.Elements[i] as StandardElement).IsGraphicBackground && !(slide.MainLayout.Elements[i] as StandardElement).IsCustomGraphics)
                {
                    slide.MainLayout.Elements.Remove(slide.MainLayout.Elements[i] as StandardElement);
                    i--;
                }
            }
            Global.EndInit();
        }

        /// <summary>
        /// Get max Zindex của hình nền
        /// </summary>
        /// <param name="shapeBackgrounds"></param>
        /// <returns></returns>
        private static int GetMaxZIndexShapeBackground(ObservableCollection<StandardElement> shapeBackgrounds)
        {
            int max = 0;
            foreach (StandardElement item in shapeBackgrounds)
            {
                if (item.ZIndex > max)
                {
                    max = item.ZIndex;
                }
            }
            return max;
        }


    }

    public class AddSlideMasterToDocumentStep : StepBase
    {
        public ETheme OldTheme { get; set; }
        public ETheme NewTheme { get; set; }
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public AddSlideMasterToDocumentStep(SlideMaster value, Collection<SlideMaster> collection, ETheme oldTheme, ETheme newTheme)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
            OldTheme = oldTheme;
            NewTheme = newTheme;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            int _curentIndex = (Source as Collection<SlideMaster>).IndexOf(NewValue as SlideMaster);
            (Source as Collection<SlideMaster>).Remove(NewValue as SlideMaster);
            (NewValue as SlideMaster).IsSelected = false;
            _curentIndex = Math.Max(0, Math.Min(_curentIndex - 1, (Source as Collection<SlideMaster>).Count - 1));
            SlideHelper.UnSlectedAll();
            if ((Source as Collection<SlideMaster>).Count > 0)
            {
                (Source as Collection<SlideMaster>)[_curentIndex].IsSelected = true;
            }
            (Application.Current as IAppGlobal).SelectedTheme = OldTheme;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<SlideMaster>).Add(OldValue as SlideMaster);
            (OldValue as SlideMaster).IsSelected = true;
            (Application.Current as IAppGlobal).SelectedTheme = NewTheme;
            Global.EndInit();
        }
    }

    public class RemoveSlideMasterFromDocumentStep : StepBase
    {

        private int _oldIndex;
        /// <summary>
        /// Chỉ số  index trong danh sách
        /// </summary>
        public int OldIndex
        {
            get { return _oldIndex; }
            set { _oldIndex = value; }
        }

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="value"></param>
        /// <param name="collection"></param>
        public RemoveSlideMasterFromDocumentStep(SlideMaster value, Collection<SlideMaster> collection, int index)
        {
            this.NewValue = this.OldValue = value;
            Source = collection;
            this.OldIndex = index;
        }

        /// <summary>
        /// Thực thi điều khiển hoàn tác
        /// </summary>
        public override void UndoExcute()
        {
            Global.BeginInit();
            (Source as Collection<SlideMaster>).Insert(OldIndex, NewValue as SlideMaster);
            SlideHelper.UnSlectedAll();
            (NewValue as SlideMaster).IsSelected = true;
            Global.EndInit();
        }

        /// <summary>
        /// Thực thi điều khiển hủy hoàn tác
        /// </summary>
        public override void RedoExcute()
        {
            Global.BeginInit();
            (Source as Collection<SlideMaster>).Remove(OldValue as SlideMaster);
            Global.EndInit();
        }
    }
}

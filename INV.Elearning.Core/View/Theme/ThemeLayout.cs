﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;

namespace INV.Elearning.Core.View.Theme
{
    /// <summary>
    /// Canvas chứa hình nền của MasterSlide
    /// </summary>
    public class ThemeLayout : LayoutBase
    {
        /// <summary>
        /// ID của theme áp dụng lên Slide
        /// </summary>
        public string ThemeID { get; set; }
        /// <summary>
        /// Update Giao diện
        /// </summary>
        /// <param name="data"></param>
        public override void UpdateUI(PageLayer data)
        {
            if (data != null)
            {
                this.IsHitTestVisible = false;
                this.Elements.Clear();
                //this.Children.Clear();
                for (int i = 0; i < Children.Count; i++)
                {
                    var element = Children[i];
                    Children.Remove(element);
                    element = null;
                    i--;
                }
                //this.Fill = ColorHelper.ConverFromColorData(data.Background);
                this.ID = data.ID;
                this.LayoutConfig = data.Setting;
                this.Timing = data.Timing;
                this.TargetName = data.Name;
                foreach (var item in data.Children)
                {
                    if (!(item is IEPlaceHolder) && item.IsGraphicBackground)
                    {
                        var _element = ObjectElementsHelper.LoadData(item);
                        if (_element != null)
                            this.Elements.Add(_element);
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật Theme layout vs graphic của slide cha
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataParent"></param>
        public void UpdateWithParent(PageLayer data, PageLayer dataParent)
        {
            if (data != null)
            {
                this.IsHitTestVisible = false;
                this.Elements.Clear();
                this.Children.Clear();
              //  this.Fill = ColorHelper.ConverFromColorData(data.Background);
                this.ID = data.ID;
                this.LayoutConfig = data.Setting;
                this.Timing = data.Timing;
                this.TargetName = data.Name;
                foreach (var item in dataParent.Children)
                {
                    if (!(item is IEPlaceHolder) && item.IsGraphicBackground)
                    {
                        var _element = ObjectElementsHelper.LoadData(item);
                        if (_element != null)
                            this.Elements.Add(_element);
                    }
                }
                foreach (var item in data.Children)
                {
                    if (!(item is IEPlaceHolder) && item.IsGraphicBackground)
                    {
                        var _element = ObjectElementsHelper.LoadData(item);
                        if (_element != null)
                            this.Elements.Add(_element);
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật lại khung nhìn
        /// </summary>
        /// <param name="data"></param>
        public void UpdateUIB(PageLayer data)
        {
            if (data != null)
            {
                this.IsHitTestVisible = false;
                this.Fill = ColorHelper.ConverFromColorData(data.Background);
                this.ID = data.ID;
                this.LayoutConfig = data.Setting;
                this.Timing = data.Timing;
                this.TargetName = data.Name;
                foreach (var item in data.Children)
                {
                    if (!(item is IEPlaceHolder) && item.IsGraphicBackground)
                    {
                        ObjectElement _element = this.Elements.FirstOrDefault(x => x.ID == item.ID);
                        if (_element != null)
                        {
                            _element.UpdateUI(item);
                        }
                        else
                        {
                            _element = ObjectElementsHelper.LoadData(item);
                            if (_element != null)
                                this.Elements.Add(_element);
                        }
                    }
                }

                for (int i = 0; i < this.Elements.Count; i++)
                {
                    if (data.Children.FirstOrDefault(x => x.ID == this.Elements[i].ID) == null)
                    {
                        this.Elements.RemoveAt(i);
                        i--;
                    }
                }
            }
            else
            {
                this.Elements.Clear();
            }
        }

        /// <summary>
        /// Không thực hiện gì cả
        /// </summary>
        public override void UpdateThumbnail()
        {
            
        }
    }
}

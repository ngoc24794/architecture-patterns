﻿using INV.Elearing.Controls.Shapes;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.View.Theme
{
    public class ThemeShapeBackground : StandardElement
    {


        public bool IsShapeBackground
        {
            get { return (bool)GetValue(IsShapeBackgroundProperty); }
            set { SetValue(IsShapeBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsShapeBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsShapeBackgroundProperty =
            DependencyProperty.Register("IsShapeBackground", typeof(bool), typeof(ThemeShapeBackground), new PropertyMetadata(true));


        public ThemeShapeBackground()
        {
            Data = new EThemeShapeBackground();
        }

        public override void UpdateUI(ObjectElementBase data)
        {
            base.UpdateUI(data);
            if (data is EThemeShapeBackground eThemeShape)
            {
                IsShapeBackground = eThemeShape.IsShapeBackground;
            }
        }

        public override void RefreshData()
        {
            base.RefreshData();
            if (this.Data is EThemeShapeBackground eThemeShape)
            {
                eThemeShape.IsShapeBackground = IsShapeBackground;
            }
        }
    }
}

﻿
using INV.Elearning.Core.AppCommands;
using INV.Elearning.Core.Helper;
using INV.Elearning.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    public class MoveThumb : Thumb
    {
        private RotateTransform rotateTransform;//Xoay theo góc quay
        private Adorner adorner;//Các Adorer để vẽ các đường SmartLine và các Thumb
        private static List<ObjectElement> _objects = new List<ObjectElement>();//Danh sách các Object có trong Canvas
        private static List<double> _listMoveX = new List<double>();//List các moveX của các đường GuideLine theo chiều dọc
        private static List<double> _listMoveY = new List<double>();//List các moveX của các đường GuideLine theo chiều ngang
        const double DELTA = 4.0;//Hằng số độ lệch khi di căn chỉnh các đường SmartLine
        private bool isExcuteDuplicate = false; //Cờ trạng thái đã nhân bản

        #region DesignerItem

        public ObjectElement DesignerItem
        {
            get { return (ObjectElement)GetValue(DesignerItemProperty); }
            set { SetValue(DesignerItemProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DesignerItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DesignerItemProperty =
            DependencyProperty.Register("DesignerItem", typeof(ObjectElement), typeof(MoveThumb), new PropertyMetadata(null));

        #endregion

        public Adorner SmartLineAdorner { get; set; }

        #region Contructor
        public MoveThumb()
        {
            DragStarted += new DragStartedEventHandler(this.MoveThumb_DragStarted);
            DragDelta += new DragDeltaEventHandler(this.MoveThumb_DragDelta);
            DragCompleted += MoveThumb_DragCompleted;
        }
        #endregion

        public void SetSmartLineAdorner(Adorner adorner)
        {
            SmartLineAdorner = adorner;
        }

        /// <summary>
        /// Hàm xử lý khi thả chuột, không di chuyển nữa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            this.Cursor = CursorsHelper.Move;
            if (adorner != null)
                adorner.Opacity = 1;
            this.DesignerItem.InSession = false;
            //this.DesignerItem.Opacity = 1.0;
            if (ObjectElementsGlobal.Source == this.DesignerItem)
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    if (item != this.DesignerItem && item.Container != null)
                        (item.Container.FindName(this.Name) as MoveThumb)?.MoveThumb_DragCompleted(null, e);
                }
            ObjectElementsGlobal.Source = null;
            ObjectElementsGlobal.ObjectElements = null;
            DesignerItem?.MouseUpExcute(this);
            if (SmartLineAdorner is SmartLineAdorner smartLineAdorner)
            {
                smartLineAdorner.RaiseDragCompleted(this, e);
            }
            Global.StopGrouping("move-ks02");
        }

        private void MoveThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            Global.StartGrouping("move-ks02");
            DesignerItem?.MouseDownExcute(this);
            // DesignerItem.Opacity = 0.5;
            isExcuteDuplicate = false;
            this.Cursor = CursorsHelper.Moving;
            if (AdornerLayer.GetAdornerLayer((Application.Current as IAppGlobal).DocumentControl) is AdornerLayer adornerLayer)
            {
                if (adornerLayer.GetAdorners((Application.Current as IAppGlobal).DocumentControl) != null)
                {
                    foreach (Adorner item in adornerLayer.GetAdorners((Application.Current as IAppGlobal).DocumentControl))
                    {
                        if (item is SmartLineAdorner)
                        {
                            SmartLineAdorner = item;
                            break;
                        }
                    }
                }
            }
            if (this.DesignerItem != null)
            {
                if (adorner == null)
                {
                    var _adornerLayer = AdornerLayer.GetAdornerLayer(this.DesignerItem);
                    adorner = _adornerLayer?.GetAdorners(this.DesignerItem)?.FirstOrDefault();
                }
                else
                {
                    adorner.Opacity = 0;
                }

                this.DesignerItem.InSession = true;
                this.rotateTransform = (this.DesignerItem.RenderTransform as TransformGroup)?.Children.FirstOrDefault(x => x is RotateTransform) as RotateTransform;


                if (ObjectElementsGlobal.ObjectElements == null && ObjectElementsGlobal.Source == null) //Tác động lên những đối tượng khác
                {
                    ObjectElementsGlobal.Source = DesignerItem;
                    ObjectElementsGlobal.ObjectElements = ObjectElementsGlobal.GetObjectElements(this.DesignerItem.Parent as Canvas);

                    for (int i = 0; i < (Application.Current as IAppGlobal).SelectedElements.Count; i++)
                    {
                        var item = (Application.Current as IAppGlobal).SelectedElements[i];
                        if (item != this.DesignerItem && item.Container != null)
                            (item.Container.FindName(this.Name) as MoveThumb)?.MoveThumb_DragStarted(null, e);
                    }
                }
            }
        }

        public double LimitDeltaX { get; set; }
        public double LimitDeltaY { get; set; }
        private Point _ctrlMouseDownPoint = new Point();
        private void MoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            double _backupL = DesignerItem.Left;
            double _backupT = DesignerItem.Top;
            if (this.DesignerItem != null)
            {
                if (!isExcuteDuplicate && Keyboard.Modifiers == ModifierKeys.Control)
                {
                    if (_ctrlMouseDownPoint.X != 0 || _ctrlMouseDownPoint.Y != 0)
                    {
                        var _currentPoint = Mouse.GetPosition(null);
                        if (_ctrlMouseDownPoint.X - _currentPoint.X > 5 || _ctrlMouseDownPoint.X - _currentPoint.X > 5) //Bắt buộc di chuyển hơn 5 pixel mới cho phép nhân bản
                        {
                            ObjectCommands.DuplicateObjectCommand.Execute(null);
                            isExcuteDuplicate = true;
                        }
                    }
                    else
                    {
                        _ctrlMouseDownPoint = Mouse.GetPosition(null);
                    }                   
                }

                double _scale = (Application.Current as IAppGlobal).DocumentPageScale; //Tỷ lệ thu phóng của trang hiện tại
                if (_scale > 1) _scale = 1 / _scale;
                     
                Point dragDelta = new Point(e.HorizontalChange * _scale * (this.DesignerItem.IsScaleX ? -1 : 1), e.VerticalChange * _scale * (this.DesignerItem.IsScaleY ? -1 : 1));

                if (this.rotateTransform != null)
                {
                    dragDelta = this.rotateTransform.Transform(dragDelta);
                }

                bool
                    isShouldChangeLeft = Math.Abs(dragDelta.X) > LimitDeltaX,
                    isShouldChangeTop = Math.Abs(dragDelta.Y) > LimitDeltaY;
                if (ObjectElementsGlobal.Source == this.DesignerItem)
                {
                    if (isShouldChangeLeft)
                    {
                        this.DesignerItem.Left += dragDelta.X;
                        LimitDeltaX = 0;
                    }
                    if (isShouldChangeTop)
                    {
                        this.DesignerItem.Top += dragDelta.Y;
                        LimitDeltaY = 0;
                    }
                    if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                    {
                        if (SmartLineAdorner is SmartLineAdorner adorner)
                        {
                            adorner.RaiseMoving(DesignerItem, new EDragDeltaEventArgs(dragDelta.X, dragDelta.Y, this));
                        }
                    }
                }


                if (ObjectElementsGlobal.Source == this.DesignerItem)
                {
                    dragDelta.X = DesignerItem.Left - _backupL;
                    dragDelta.Y = DesignerItem.Top - _backupT;
                    foreach (var item in ObjectElementsGlobal.ObjectElements) //Cho các đối tượng khác nữa
                    {
                        if (item != this.DesignerItem && CheckRoot(item, this.DesignerItem))
                        {
                            if (isShouldChangeLeft)
                            {
                                item.Left += dragDelta.X;
                            }
                            if (isShouldChangeTop)
                            {
                                item.Top += dragDelta.Y;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        internal static bool CheckRoot(ObjectElement target, ObjectElement source)
        {
            ObjectElement _container = source.ElementContainer;
            while (_container?.ElementContainer != null)
            {
                _container = _container.ElementContainer;
            }

            if (_container is ObjectElement)
                return _container != target;

            return true;
        }


        /// <summary>
        /// Sự kiện nhả phím trên đối tượng
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            this.Cursor = CursorsHelper.Move;
        }

        /// <summary>
        /// Sự kiện nhấn phím lên đối tượng
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                this.Cursor = CursorsHelper.Select;
            }
        }

        /// <summary>
        /// Sự kiện rê chuột lên đối tượng
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                this.Cursor = CursorsHelper.Select;
            }
            else
            {
                this.Cursor = CursorsHelper.Move;
            }

            //this.Focusable = true;
            // this.Focus();

        }
    }
}

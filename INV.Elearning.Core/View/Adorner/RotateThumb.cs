﻿using INV.Elearning.Core.Helper;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Thumb quay
    /// </summary>
    public class RotateThumb : Thumb
    {
        private double initialAngle;
        private Vector startVector;
        private Point centerPoint;
        private ObjectElement designerItem;
        private Canvas canvas;
        public ContainerChrome ChromeParent { get; set; }
        public ObjectElement DesignerItem { get => designerItem; set => designerItem = value; }
        private bool heighNaN;

        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public RotateThumb()
        {
            DragDelta += new DragDeltaEventHandler(this.RotateThumb_DragDelta);
            DragStarted += new DragStartedEventHandler(this.RotateThumb_DragStarted);
            DragCompleted += RotateThumb_DragCompleted;
        }

        /// <summary>
        /// Hoàn thành quay
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RotateThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            this.Cursor = INV.Elearning.Resources.CursorsHelper.Rotate;
            if (ChromeParent != null)
            {
                this.ChromeParent.Opacity = 1;
                ChromeParent.UpdateCursorByAngle(DesignerItem.Angle);
            }

            this.DesignerItem.InSession = false;

            if (heighNaN && this.DesignerItem != null)
            {
                this.DesignerItem.Height = double.NaN;
            }

            if (ObjectElementsGlobal.Source == this.DesignerItem)
                //foreach (var item in ObjectElementsGlobal.ObjectElements)
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    if (item != this.DesignerItem && item.Adorner is ContainerAdorner)
                        ((item.Adorner as ContainerAdorner).Chrome.FindName(this.Name) as RotateThumb).RotateThumb_DragCompleted(null, e);
                }
            ObjectElementsGlobal.Source = null;
            ObjectElementsGlobal.ObjectElements = null;
            Global.StopGrouping("rotate-029s");
        }

        /// <summary>
        /// Bắt đầu quay
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RotateThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            Global.StartGrouping("rotate-029s");
            heighNaN = false;
            if (this.DesignerItem != null)
            {
                this.canvas = VisualTreeHelper.GetParent(this.DesignerItem) as Canvas;
                if (double.IsNaN(this.DesignerItem.Height))
                {
                    heighNaN = true;
                    this.DesignerItem.Height = this.DesignerItem.ActualHeight;
                }

                if (this.canvas != null)
                {
                    if (ObjectElementsGlobal.ObjectElements == null && ObjectElementsGlobal.Source == null) //Tác động lên những đối tượng khác
                    {
                        ObjectElementsGlobal.Source = DesignerItem;
                        ObjectElementsGlobal.ObjectElements = ObjectElementsGlobal.GetObjectElements(canvas);
                        
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                        {
                            if (item != this.DesignerItem && item.Adorner is ContainerAdorner)
                                ((item.Adorner as ContainerAdorner).Chrome.FindName(this.Name) as RotateThumb).RotateThumb_DragStarted(null, e);
                        }
                    }

                    if (this.ChromeParent != null)
                        this.ChromeParent.Opacity = 0;

                    this.DesignerItem.InSession = true;
                    this.Cursor = INV.Elearning.Resources.CursorsHelper.Rotating;
                    this.centerPoint = this.DesignerItem.TranslatePoint(
                        new Point(this.DesignerItem.Width * this.DesignerItem.RenderTransformOrigin.X,
                                  this.DesignerItem.Height * this.DesignerItem.RenderTransformOrigin.Y),
                                  this.canvas);

                    Point startPoint = Mouse.GetPosition(this.canvas);
                    this.startVector = Point.Subtract(startPoint, this.centerPoint);

                    this.initialAngle = this.DesignerItem.Angle;
                }
            }
        }

        /// <summary>
        /// Đang quay
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RotateThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var _deltaAngle = this.DesignerItem.Angle;
            if (this.DesignerItem != null && this.canvas != null)
            {
                Point currentPoint = Mouse.GetPosition(this.canvas);
                Vector deltaVector = Point.Subtract(currentPoint, this.centerPoint);

                double angle = Vector.AngleBetween(this.startVector, deltaVector);

                this.DesignerItem.Angle = RotationRounding(this.initialAngle + Math.Round(angle, 0));
                this.DesignerItem.InvalidateMeasure();

                _deltaAngle = this.DesignerItem.Angle - _deltaAngle;
            }

            if (ObjectElementsGlobal.Source == this.DesignerItem)
                foreach (var item in ObjectElementsGlobal.ObjectElements) //Cho các đối tượng khác nữa
                {
                    if (item != this.DesignerItem && //Đối tượng khác so với khung lựa chọn, 
                        MoveThumb.CheckRoot(item, this.DesignerItem)//không phải là cha chứa khung lựa chọn
                        ) 
                    {
                        item.Angle += _deltaAngle;
                        item.InvalidateMeasure();
                    }
                }
        }

        const double DELTA = 3.0; //Góc lệch

        /// <summary>
        /// Làm tròn các góc
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        private double RotationRounding(double angle)
        {
            while(angle < 0)
            {
                angle += 360.0;
            }

            while(angle > 360)
            {
                angle -= 360;
            }

            double _surplus = angle % 90; //Góc dư với các góc đặc biệt 0,90,180,270,360

            if (_surplus < DELTA)
            {
                angle -= _surplus;
            }
            else if (90 - _surplus < DELTA)
            {
                angle -= (_surplus - 90);
            }

            return angle;
        }
    }
}

﻿using INV.Elearning.Animations;
using INV.Elearning.Core.Helper;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    public class ResizeThumb : Thumb
    {
        private double angle, _aspectRatio;
        private Point transformOrigin;
        private ObjectElement designerItem;
        private Cursor previousCursor = null;
        private Canvas canvas;
        private bool _previousScaleX = false, _previousScaleY = false;
        public ObjectElement DesignerItem { get => designerItem; set => designerItem = value; }
        public ContainerChrome ChromeParent { get; set; }
        public Adorner SmartLineAdorner { get; set; }
        private int _count = 0;
        public ResizeThumb()
        {
            DragStarted += new DragStartedEventHandler(this.ResizeThumb_DragStarted);
            DragDelta += new DragDeltaEventHandler(this.ResizeThumb_DragDelta);
            DragCompleted += ResizeThumb_DragCompleted;
        }

        /// <summary>
        /// Ghi đè
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
        {
            
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {

        }

        Point _startPoint, _endPoint = new Point();

        private void ResizeThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            if (this.ChromeParent != null)
            {
                ChromeParent.UpdateCursorByAngle(this.DesignerItem.Angle);
                this.ChromeParent.Opacity = 1;
            }
            this.Cursor = this.previousCursor;
            this.DesignerItem.Opacity = 1;

            if (!this.DesignerItem.IsRealSize)
            {
                if (this.DesignerItem.Adorner is ContainerAdorner _adorner)
                {
                    this.DesignerItem.Left += Math.Min(_adorner.RectStartPoint.X, _adorner.RectEndPoint.X);
                    this.DesignerItem.Top += Math.Min(_adorner.RectStartPoint.Y, _adorner.RectEndPoint.Y);
                    this.DesignerItem.Width = Math.Abs(_adorner.RectEndPoint.X - _adorner.RectStartPoint.X);
                    this.DesignerItem.Height = Math.Abs(_adorner.RectEndPoint.Y - _adorner.RectStartPoint.Y);
                    _adorner.RemoveRectVisual();
                }
            }

            this.DesignerItem.InSession = false;
            _aspectRatio = 0;

            if (ObjectElementsGlobal.Source == this.DesignerItem)
                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    if (item != this.DesignerItem && item.Adorner is ContainerAdorner)
                        ((item.Adorner as ContainerAdorner).Chrome.FindName(this.Name) as ResizeThumb).ResizeThumb_DragCompleted(null, e);
                }
            ObjectElementsGlobal.Source = null;
            ObjectElementsGlobal.ObjectElements = null;
            if (SmartLineAdorner is SmartLineAdorner smartLineAdorner)
            {
                smartLineAdorner.RaiseDragCompleted(DesignerItem, e);
            }
        }

        private void ResizeThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (this.DesignerItem != null)
            {
                this.DesignerItem.OriginSize = new Size(this.DesignerItem.Width, this.DesignerItem.Height);//Lấy kích thước lúc mới bắt đầu tùy chỉnh
                this.DesignerItem.InSession = true;
                this.DesignerItem.PreviousScaleX = this.DesignerItem.IsScaleX;
                _previousScaleX = this.DesignerItem.IsScaleX;
                _previousScaleY = this.DesignerItem.IsScaleY;

                this.canvas = VisualTreeHelper.GetParent(this.DesignerItem) as Canvas;

                if (this.DesignerItem.IsRealSize)
                {
                    this.ChromeParent.Opacity = 0;
                    this.DesignerItem.Opacity = 0.5;
                }

                if (AdornerLayer.GetAdornerLayer((Application.Current as IAppGlobal).DocumentControl) is AdornerLayer adornerLayer)
                {
                    if (adornerLayer.GetAdorners((Application.Current as IAppGlobal).DocumentControl) != null)
                    {
                        foreach (Adorner item in adornerLayer.GetAdorners((Application.Current as IAppGlobal).DocumentControl))
                        {
                            if (item is SmartLineAdorner)
                            {
                                SmartLineAdorner = item;
                                break;
                            }
                        }
                    }
                }

                this.previousCursor = this.Cursor;
                this.Cursor = Cursors.Cross;
                if (this.canvas != null)
                {
                    if (ObjectElementsGlobal.ObjectElements == null && ObjectElementsGlobal.Source == null) //Tác động lên những đối tượng khác
                    {
                        ObjectElementsGlobal.Source = DesignerItem;
                        ObjectElementsGlobal.ObjectElements = ObjectElementsGlobal.GetObjectElements(canvas);
                        foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                        {
                            if (item != this.DesignerItem && item.Adorner is ContainerAdorner)
                                ((item.Adorner as ContainerAdorner).Chrome.FindName(this.Name) as ResizeThumb).ResizeThumb_DragStarted(null, e);
                        }
                    }

                    this.transformOrigin = this.DesignerItem.RenderTransformOrigin;
                    this.angle = this.DesignerItem.Angle * Math.PI / 180.0;
                }

                _aspectRatio = this.DesignerItem.Width / this.DesignerItem.Height;
            }

            if (!this.DesignerItem.IsRealSize && this.DesignerItem.Adorner is ContainerAdorner)
                (this.DesignerItem.Adorner as ContainerAdorner).AddRectVisual();
            _startPoint = Mouse.GetPosition(Application.Current.MainWindow);
        }

        public double LimitDeltaX { get; set; }
        public double LimitDeltaY { get; set; }
        /// <summary>
        /// Sau mỗi thay đổi kéo thumb
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            _endPoint = Mouse.GetPosition(Application.Current.MainWindow);
            if (this.DesignerItem != null)
            {
                double _scale = (Application.Current as IAppGlobal).DocumentPageScale; //Tỷ lệ thu phóng của trang hiện tại
                bool
                    isLeft = false,
                    isTop = false, isLinePath = (DesignerItem.Adorner as ContainerAdorner).Chrome.IsLineContainerChrome == Visibility.Collapsed,

                    //---------------------------------------------------------------------------
                    // Dùng để hít dính các cạnh của đối tượng với SmartLine
                    //---------------------------------------------------------------------------
                    isShouldChangeHorizontal = true,
                    isShouldChangeVertical = true;

                double deltaVertical = 0.0, deltaHorizontal = 0.0, deltaVerticalAdd = 0.0, deltaHorizontalAdd = 0.0, horizontalChange = 0, verticalChange = 0;
                if (isLinePath)
                {
                    DesignerItem.MinHeight = DesignerItem.MinWidth = 1;
                }
                switch (VerticalAlignment)
                {
                    case VerticalAlignment.Bottom:
                        isTop = this.DesignerItem.IsScaleY;

                        if (!this.DesignerItem.IsRealSize) //Không cập nhật Size theo thao tác
                        {
                            deltaVertical = _startPoint.Y - _endPoint.Y;
                            deltaVertical = Math.Round(deltaVertical, 0);
                            if (this.DesignerItem.Adorner is ContainerAdorner _adorner)
                                _adorner.RectEndPoint = new Point(_adorner.RectEndPoint.X, _adorner.RectEndPoint.Y - deltaVertical);
                            break;
                        }

                        deltaVertical = Math.Min(-e.VerticalChange / _scale, this.DesignerItem.ActualHeight - this.DesignerItem.MinHeight);
                        deltaVertical = Math.Round(deltaVertical, 0);
                        if (Math.Round(DesignerItem.Height, 0) == this.DesignerItem.MinHeight)
                        {
                            if (e.VerticalChange <= -this.DesignerItem.MinHeight) //Kiểm tra để đảo ngược X
                            {
                                if (this.DesignerItem.IsScaleY)
                                {
                                    deltaVerticalAdd = this.DesignerItem.Height;
                                    deltaVerticalAdd = Math.Round(deltaVerticalAdd, 0);
                                    deltaVertical = this.DesignerItem.Height;
                                    deltaVertical = Math.Round(deltaVertical, 0);
                                    verticalChange = (deltaVertical * Math.Cos(-this.angle) + (this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle))));
                                    horizontalChange = (deltaVertical * Math.Sin(-this.angle) - (this.transformOrigin.Y * deltaVertical * Math.Sin(-this.angle)));
                                    this.DesignerItem.Top += verticalChange;
                                    this.DesignerItem.Left += horizontalChange;
                                    this.DesignerItem.IsScaleY = false;
                                }
                                else
                                {
                                    deltaVerticalAdd = 3 * this.DesignerItem.Height;
                                    deltaVerticalAdd = Math.Round(deltaVerticalAdd, 0);
                                    deltaVertical = -this.DesignerItem.Height;
                                    deltaVertical = Math.Round(deltaVertical, 0);
                                    horizontalChange = (deltaVertical * Math.Sin(-this.angle) - (this.transformOrigin.Y * deltaVertical * Math.Sin(-this.angle)));
                                    verticalChange = (deltaVertical * Math.Cos(-this.angle) + (this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle))));
                                    this.DesignerItem.Top += verticalChange;
                                    this.DesignerItem.Left += horizontalChange;
                                    this.DesignerItem.IsScaleY = true;
                                }

                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(horizontalChange, verticalChange, this, ThumbType.Bottom));
                                    }
                                }

                                goto SizeAll;
                            }
                        }

                        //deltaVertical /= _scale;
                        isShouldChangeVertical = Math.Abs(deltaVertical) > LimitDeltaY;
                        if (isShouldChangeVertical)
                        {
                            if (!this.DesignerItem.IsScaleY) //Nếu chưa thay đổi thì tính theo các giá trị thay đổi thực
                            {
                                horizontalChange = (deltaVertical * this.transformOrigin.Y * Math.Sin(-this.angle));
                                verticalChange = ((this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle))));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left -= horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(-deltaHorizontal - deltaHorizontalAdd, -deltaVertical - deltaVerticalAdd, this, ThumbType.Bottom));
                                    }
                                }
                            }
                            else
                            {
                                horizontalChange = (deltaVertical * Math.Sin(-this.angle) - (this.transformOrigin.Y * deltaVertical * Math.Sin(-this.angle)));
                                verticalChange = (deltaVertical * Math.Cos(-this.angle) + (this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle))));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(horizontalChange, verticalChange, this, ThumbType.Bottom));
                                    }
                                }
                            }
                            this.DesignerItem.Height -= deltaVertical;
                            LimitDeltaY = 0;

                        }
                        break;
                    case VerticalAlignment.Top:
                        isTop = !this.DesignerItem.IsScaleY;

                        if (!this.DesignerItem.IsRealSize) //Không cập nhật Size theo thao tác
                        {
                            deltaVertical = _endPoint.Y - _startPoint.Y;
                            deltaVertical = Math.Round(deltaVertical, 0);
                            if (this.DesignerItem.Adorner is ContainerAdorner _adorner)
                                _adorner.RectStartPoint = new Point(_adorner.RectStartPoint.X, _adorner.RectStartPoint.Y + deltaVertical);
                            break;
                        }

                        deltaVertical = Math.Min(e.VerticalChange / _scale, this.DesignerItem.ActualHeight - this.DesignerItem.MinHeight);
                        deltaVertical = Math.Round(deltaVertical, 0);
                        if (Math.Round(DesignerItem.Height, 0) == this.DesignerItem.MinHeight)
                        {
                            if (e.VerticalChange >= this.DesignerItem.MinHeight) //Kiểm tra để đảo ngược X
                            {
                                if (this.DesignerItem.IsScaleY) //Nếu đã bị đảo ngược trước đây, thì phải đảo ngược trở lại
                                {
                                    deltaVerticalAdd = 3 * this.DesignerItem.Height;
                                    deltaVerticalAdd = Math.Round(deltaVerticalAdd, 0);
                                    deltaVertical = -this.DesignerItem.Height;
                                    deltaVertical = Math.Round(deltaVertical, 0);
                                    horizontalChange = (deltaVertical * Math.Sin(-this.angle) - (this.transformOrigin.Y * deltaVertical * Math.Sin(-this.angle)));
                                    verticalChange = (deltaVertical * Math.Cos(-this.angle) + (this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle))));
                                    this.DesignerItem.Top += verticalChange;
                                    this.DesignerItem.Left += horizontalChange;
                                    this.DesignerItem.IsScaleY = false;
                                }
                                else
                                {
                                    deltaVerticalAdd = this.DesignerItem.Height;
                                    deltaVerticalAdd = Math.Round(deltaVerticalAdd, 0);
                                    deltaVertical = this.DesignerItem.Height;
                                    deltaVertical = Math.Round(deltaVertical, 0);
                                    horizontalChange = (deltaVertical * Math.Sin(-this.angle) - (this.transformOrigin.Y * deltaVertical * Math.Sin(-this.angle)));
                                    verticalChange = (deltaVertical * Math.Cos(-this.angle) + (this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle))));
                                    this.DesignerItem.Top += verticalChange;
                                    this.DesignerItem.Left += horizontalChange;
                                    this.DesignerItem.IsScaleY = true;
                                }
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(-deltaHorizontal - deltaHorizontalAdd, -deltaVertical - deltaVerticalAdd, this, ThumbType.Top));
                                    }
                                }
                                goto SizeAll;
                            }
                        }

                        //deltaVertical /= _scale;
                        isShouldChangeVertical = Math.Abs(deltaVertical) > LimitDeltaY;
                        if (isShouldChangeVertical)
                        {
                            this.DesignerItem.Height -= deltaVertical;
                            LimitDeltaY = 0;
                            if (!this.DesignerItem.IsScaleY) //Nếu chưa thay đổi thì tính theo các giá trị thay đổi thực
                            {
                                horizontalChange = (deltaVertical * Math.Sin(-this.angle) - (this.transformOrigin.Y * deltaVertical * Math.Sin(-this.angle)));
                                verticalChange = (deltaVertical * Math.Cos(-this.angle) + (this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle))));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(horizontalChange, verticalChange, this, ThumbType.Top));
                                    }
                                }
                            }
                            else
                            {
                                horizontalChange = (deltaVertical * this.transformOrigin.Y * Math.Sin(-this.angle));
                                verticalChange = (this.transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this.angle)));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left -= horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(-deltaHorizontal - deltaHorizontalAdd, -deltaVertical - deltaVerticalAdd, this, ThumbType.Top));
                                    }
                                }
                            }

                        }
                        break;
                    default:
                        break;
                }

                switch (HorizontalAlignment)
                {
                    case System.Windows.HorizontalAlignment.Left:
                        isLeft = !this.DesignerItem.IsScaleX;

                        if (!this.DesignerItem.IsRealSize) //Không cập nhật Size theo thao tác
                        {
                            deltaHorizontal = -_startPoint.X + _endPoint.X;
                            deltaHorizontal = Math.Round(deltaHorizontal, 0);
                            if (this.DesignerItem.Adorner is ContainerAdorner _adorner)
                                _adorner.RectStartPoint = new Point(_adorner.RectStartPoint.X + deltaHorizontal, _adorner.RectStartPoint.Y);
                            break;
                        }

                        deltaHorizontal = Math.Min(e.HorizontalChange / _scale, this.DesignerItem.ActualWidth - this.DesignerItem.MinWidth);
                        deltaHorizontal = Math.Round(deltaHorizontal, 0);
                        if (Math.Round(DesignerItem.Width, 0) == this.DesignerItem.MinWidth)
                        {
                            if (e.HorizontalChange >= this.DesignerItem.MinWidth) //Kiểm tra để đảo ngược X
                            {
                                if (this.DesignerItem.IsScaleX)
                                {
                                    deltaHorizontalAdd = 3 * this.DesignerItem.Width;
                                    deltaHorizontalAdd = Math.Round(deltaHorizontalAdd, 0);
                                    deltaHorizontal = -this.DesignerItem.Width;
                                    deltaHorizontal = Math.Round(deltaHorizontal, 0);
                                    this.DesignerItem.IsScaleX = false;
                                }
                                else
                                {
                                    deltaHorizontalAdd = this.DesignerItem.Width;
                                    deltaHorizontalAdd = Math.Round(deltaHorizontalAdd, 0);
                                    deltaHorizontal = this.DesignerItem.Width;
                                    deltaHorizontal = Math.Round(deltaHorizontal, 0);
                                    this.DesignerItem.IsScaleX = true;
                                }
                                horizontalChange = (deltaHorizontal * Math.Cos(this.angle) + (this.transformOrigin.X * deltaHorizontal * (1 - Math.Cos(this.angle))));
                                verticalChange = (deltaHorizontal * Math.Sin(this.angle) - this.transformOrigin.X * deltaHorizontal * Math.Sin(this.angle));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(-deltaHorizontal - deltaHorizontalAdd, -deltaVertical - deltaVerticalAdd, this, ThumbType.Left));
                                    }
                                }
                                goto SizeAll;
                            }
                        }

                        //deltaHorizontal /= _scale;
                        isShouldChangeHorizontal = Math.Abs(deltaHorizontal) > LimitDeltaX;
                        if (isShouldChangeHorizontal)
                        {
                            LimitDeltaX = 0;
                            this.DesignerItem.Width -= deltaHorizontal;
                            if (!this.DesignerItem.IsScaleX) //Nếu chưa thay đổi thì tính theo các giá trị thay đổi thực
                            {
                                horizontalChange = (deltaHorizontal * Math.Cos(this.angle) + (this.transformOrigin.X * deltaHorizontal * (1 - Math.Cos(this.angle))));
                                verticalChange = (deltaHorizontal * Math.Sin(this.angle) - this.transformOrigin.X * deltaHorizontal * Math.Sin(this.angle));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(horizontalChange, verticalChange, this, ThumbType.Left));
                                    }
                                }
                            }
                            else
                            {
                                horizontalChange = (deltaHorizontal * this.transformOrigin.X * (1 - Math.Cos(this.angle)));
                                verticalChange = (this.transformOrigin.X * deltaHorizontal * Math.Sin(this.angle));
                                this.DesignerItem.Top -= verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(-deltaHorizontal - deltaHorizontalAdd, -deltaVertical - deltaVerticalAdd, this, ThumbType.Left));
                                    }
                                }
                            }

                            //if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                            //{
                            //    if (SmartLineAdorner is SmartLineAdorner adorner)
                            //    {
                            //        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(horizontalChange, verticalChange, this, ThumbType.Left));
                            //    }
                            //}
                        }
                        break;
                    case HorizontalAlignment.Right:
                        isLeft = this.DesignerItem.IsScaleX;

                        if (!this.DesignerItem.IsRealSize) //Không cập nhật Size theo thao tác
                        {
                            deltaHorizontal = _startPoint.X - _endPoint.X;
                            deltaHorizontal = Math.Round(deltaHorizontal, 0);
                            if (this.DesignerItem.Adorner is ContainerAdorner _adorner)
                                _adorner.RectEndPoint = new Point(_adorner.RectEndPoint.X - deltaHorizontal, _adorner.RectEndPoint.Y);
                            break;
                        }

                        deltaHorizontal = Math.Min(-e.HorizontalChange / _scale, this.DesignerItem.ActualWidth - this.DesignerItem.MinWidth);
                        deltaHorizontal = Math.Round(deltaHorizontal, 0);
                        if (Math.Round(DesignerItem.Width, 0) == this.DesignerItem.MinWidth)
                        {
                            if (e.HorizontalChange <= -this.DesignerItem.MinWidth) //Kiểm tra để đảo ngược X
                            {
                                if (this.DesignerItem.IsScaleX)
                                {
                                    deltaHorizontalAdd = this.DesignerItem.Width;
                                    deltaHorizontalAdd = Math.Round(deltaHorizontalAdd, 0);
                                    deltaHorizontal = this.DesignerItem.Width;
                                    deltaHorizontal = Math.Round(deltaHorizontal, 0);
                                    this.DesignerItem.IsScaleX = false;
                                }
                                else
                                {
                                    deltaHorizontalAdd = 3 * this.DesignerItem.Width;
                                    deltaHorizontalAdd = Math.Round(deltaHorizontalAdd, 0);
                                    deltaHorizontal = -this.DesignerItem.Width;
                                    deltaHorizontal = Math.Round(deltaHorizontal, 0);
                                    this.DesignerItem.IsScaleX = true;
                                }
                                horizontalChange = (deltaHorizontal * Math.Cos(this.angle) + (this.transformOrigin.X * deltaHorizontal * (1 - Math.Cos(this.angle))));
                                verticalChange = (deltaHorizontal * Math.Sin(this.angle) - this.transformOrigin.X * deltaHorizontal * Math.Sin(this.angle));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(horizontalChange, verticalChange, this, ThumbType.Right));
                                    }
                                }
                                goto SizeAll;
                            }
                        }

                        //deltaHorizontal /= _scale;
                        isShouldChangeHorizontal = Math.Abs(deltaHorizontal) > LimitDeltaX;
                        if (isShouldChangeHorizontal)
                        {
                            this.DesignerItem.Width -= deltaHorizontal;
                            LimitDeltaX = 0;
                            if (this.DesignerItem.IsScaleX) //Nếu chưa thay đổi thì tính theo các giá trị thay đổi thực
                            {
                                horizontalChange = (deltaHorizontal * Math.Cos(this.angle) + (this.transformOrigin.X * deltaHorizontal * (1 - Math.Cos(this.angle))));
                                verticalChange = (deltaHorizontal * Math.Sin(this.angle) - this.transformOrigin.X * deltaHorizontal * Math.Sin(this.angle));
                                this.DesignerItem.Top += verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(horizontalChange, verticalChange, this, ThumbType.Right));
                                    }
                                }
                            }
                            else
                            {
                                horizontalChange = (deltaHorizontal * this.transformOrigin.X * (1 - Math.Cos(this.angle)));
                                verticalChange = (this.transformOrigin.X * deltaHorizontal * Math.Sin(this.angle));
                                this.DesignerItem.Top -= verticalChange;
                                this.DesignerItem.Left += horizontalChange;
                                if ((Application.Current as IAppGlobal).ShowConfig.IsSmartLineVisible)
                                {
                                    if (SmartLineAdorner is SmartLineAdorner adorner)
                                    {
                                        adorner.RaiseResizing(DesignerItem, new EDragDeltaEventArgs(-deltaHorizontal - deltaHorizontalAdd, -deltaVertical - deltaVerticalAdd, this, ThumbType.Right));
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }



                SizeAll:
                if (ObjectElementsGlobal.Source == this.DesignerItem)
                {
                    if (this.DesignerItem is MotionPathObject)
                    {
                        return;
                    }
                    var _deltaX = -deltaHorizontal - deltaHorizontalAdd; //Giá trị các thay đổi;
                    var _deltaY = -deltaVertical - deltaVerticalAdd;

                    foreach (var item in ObjectElementsGlobal.ObjectElements) //Cho các đối tượng khác nữa
                    {
                        if (item != this.DesignerItem.ElementContainer)
                        {
                            if (isShouldChangeHorizontal)
                            {
                                item.AddWidthDelta(_deltaX, isLeft, this.DesignerItem.OriginSize.Width);
                            }
                            if (isShouldChangeVertical)
                            {
                                item.AddHeightDelta(_deltaY, isTop, this.DesignerItem.OriginSize.Height);
                            }
                        }
                    }
                }
                _startPoint = Mouse.GetPosition(Application.Current.MainWindow);
            }
            e.Handled = true;
        }
    }
}

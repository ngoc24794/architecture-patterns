﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    public class TriggerFocusAdorner : Adorner
    {
        public TriggerFocusAdorner(UIElement adornedElement) : base(adornedElement)
        {
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            drawingContext.DrawRectangle(Brushes.Transparent, new Pen(Brushes.Red, 3), new Rect(-5, -5, this.ActualWidth + 10, this.ActualHeight + 10));
        }
    }
}

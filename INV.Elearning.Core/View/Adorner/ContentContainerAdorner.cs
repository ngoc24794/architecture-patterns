﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Adorner cho khung nội dung
    /// </summary>
    public class ContentContainerAdorner : Adorner
    {
        #region Properties

        #region ThumbFill        
        /// <summary>
        /// Đổ màu cho các vị trí thumb
        /// </summary>
        public Brush ThumbFill
        {
            get { return (Brush)GetValue(ThumbFillProperty); }
            set { SetValue(ThumbFillProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThumbFill.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThumbFillProperty =
            DependencyProperty.Register("ThumbFill", typeof(Brush), typeof(ContentContainerAdorner), new FrameworkPropertyMetadata(Brushes.LightGray, FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion

        #region Stroke

        /// <summary>
        /// Kiểu màu cho đường kẻ
        /// </summary>
        public Brush Stroke
        {
            get { return (Brush)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Stroke.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(Brush), typeof(ContentContainerAdorner), new FrameworkPropertyMetadata(Brushes.Gray, FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion

        #region Scale

        /// <summary>
        /// Tỷ lệ thu phóng trang tài liệu hiện tại
        /// </summary>
        public double Scale
        {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Scale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register("Scale", typeof(double), typeof(ContentContainerAdorner), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender));


        #endregion

        #endregion
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        /// <param name="adornedElement"></param>
        public ContentContainerAdorner(ObjectContentElement adornedElement) : base(adornedElement)
        {

        }

        /// <summary>
        /// Vẽ giao diện
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            Pen _pen = new Pen(this.Stroke, 1.0 / Scale);
            Rect _rect = new Rect(0, 0, 10, 10);
            double _halfPenWidth = _pen.Thickness / 2;

            // Create a guidelines set
            GuidelineSet guidelines = new GuidelineSet();
            guidelines.GuidelinesX.Add(_rect.Left + _halfPenWidth);
            guidelines.GuidelinesX.Add(_rect.Right + _halfPenWidth);
            guidelines.GuidelinesY.Add(_rect.Top + _halfPenWidth);
            guidelines.GuidelinesY.Add(_rect.Bottom + _halfPenWidth);

            drawingContext.PushGuidelineSet(guidelines);
            drawingContext.DrawRectangle(null, _pen, _rect); //Vẽ đường bao
            drawingContext.Pop();

            drawingContext.DrawEllipse(this.ThumbFill, _pen, new Point(_rect.Left, _rect.Top), 4, 4); //Vẽ các Elip ở 4 góc
            drawingContext.DrawEllipse(this.ThumbFill, _pen, new Point(_rect.Right, _rect.Top), 4, 4);
            drawingContext.DrawEllipse(this.ThumbFill, _pen, new Point(_rect.Right, _rect.Bottom), 4, 4);
            drawingContext.DrawEllipse(this.ThumbFill, _pen, new Point(_rect.Left, _rect.Bottom), 4, 4);
        }
    }
}

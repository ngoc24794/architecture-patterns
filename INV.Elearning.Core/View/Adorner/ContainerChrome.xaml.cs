﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Interaction logic for ContainerChrome.xaml
    /// </summary>
    public partial class ContainerChrome : UserControl
    {
        public Visibility IsLineContainerChrome
        {
            get { return (Visibility)GetValue(IsLineContainerChromeProperty); }
            set { SetValue(IsLineContainerChromeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsLineContainerChrome.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsLineContainerChromeProperty =
            DependencyProperty.Register("IsLineContainerChrome", typeof(Visibility), typeof(ContainerChrome), new PropertyMetadata(Visibility.Visible));


        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public ContainerChrome()
        {
            InitializeComponent();
        }

        #region IsContentFocused

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Focus vào nội dung trong khung
        /// </summary>
        public bool IsContentFocused
        {
            get { return (bool)GetValue(IsContentFocusedProperty); }
            set { SetValue(IsContentFocusedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsContentFocused.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsContentFocusedProperty =
            DependencyProperty.Register("IsContentFocused", typeof(bool), typeof(ContainerChrome), new PropertyMetadata(false, IsContentCallBack));

        /// <summary>
        /// Hàm được gọi sau mỗi thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsContentCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ContainerChrome).RectRounder.StrokeDashArray = ((bool)e.NewValue ? new DoubleCollection() { 1.5, 1.5 } : null);
        }


        #endregion

        #region CanRotate

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính có thể quay cho khung.
        /// </summary>
        public bool CanRotate
        {
            get { return (bool)GetValue(CanRotateProperty); }
            set { SetValue(CanRotateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanRotate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanRotateProperty =
            DependencyProperty.Register("CanRotate", typeof(bool), typeof(ContainerChrome), new PropertyMetadata(true));


        #endregion

        #region Scale

        /// <summary>
        /// Tỉ lệ thu phóng
        /// </summary>
        public double Scale
        {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Scale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register("Scale", typeof(double), typeof(ContainerChrome), new PropertyMetadata(1.0, ScaleCallBack));

        private static void ScaleCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ContainerChrome).SetScale((double)e.NewValue);
        }


        #endregion

        #region ElementParent
        private ObjectElement _elementParent;
        /// <summary>
        /// Đối tượng chứa
        /// </summary>
        public ObjectElement ElementParent
        {
            get { return _elementParent; }
            set
            {
                _elementParent = value;
                foreach (var item in this.gridRoot.Children)
                {
                    if (item is ResizeThumb)
                    {
                        (item as ResizeThumb).DesignerItem = value;
                        (item as ResizeThumb).ChromeParent = this;
                    }
                    else if (item is RotateThumb)
                    {
                        (item as RotateThumb).DesignerItem = value;
                        (item as RotateThumb).ChromeParent = this;
                    }
                }
            }
        }

        #endregion


        /// <summary>
        /// Cập nhật lại các con trỏ sau khi quay
        /// </summary>
        /// <param name="angle"></param>
        public void UpdateCursorByAngle(double angle)
        {
            angle = angle % 180;
            if (angle < 0)
            {
                if (ElementParent.IsScaleX | ElementParent.IsScaleY && !(ElementParent.IsScaleX & ElementParent.IsScaleY))
                {
                    angle = -angle;
                }
                else
                {
                    angle = 180 + angle;
                }
            }
            else if (ElementParent.IsScaleX | ElementParent.IsScaleY && !(ElementParent.IsScaleX & ElementParent.IsScaleY))
            {
                angle = 180 - angle;
            }

            if (angle > 156 || angle <= 22)
            {
                if (ElementParent.IsScaleX | ElementParent.IsScaleY && !(ElementParent.IsScaleX & ElementParent.IsScaleY))
                {
                    TopLeft.Cursor = BottomRight.Cursor = Cursors.SizeNESW;
                    TopRight.Cursor = BottomLeft.Cursor = Cursors.SizeNWSE;
                }
                else
                {
                    TopLeft.Cursor = BottomRight.Cursor = Cursors.SizeNWSE;
                    TopRight.Cursor = BottomLeft.Cursor = Cursors.SizeNESW;
                }
                TopCenter.Cursor = BottomCenter.Cursor = Cursors.SizeNS;
                CenterRight.Cursor = CenterLeft.Cursor = Cursors.SizeWE;
            }
            else if (angle > 112)
            {
                if (ElementParent.IsScaleX | ElementParent.IsScaleY && !(ElementParent.IsScaleX & ElementParent.IsScaleY))
                {
                    TopCenter.Cursor = BottomCenter.Cursor = Cursors.SizeNESW;
                    CenterRight.Cursor = CenterLeft.Cursor = Cursors.SizeNWSE;
                }
                else
                {
                    TopCenter.Cursor = BottomCenter.Cursor = Cursors.SizeNWSE;
                    CenterRight.Cursor = CenterLeft.Cursor = Cursors.SizeNESW;
                }
                TopLeft.Cursor = BottomRight.Cursor = Cursors.SizeWE;
                TopRight.Cursor = BottomLeft.Cursor = Cursors.SizeNS;
            }
            else if (angle > 67)
            {
                if (ElementParent.IsScaleX | ElementParent.IsScaleY && !(ElementParent.IsScaleX & ElementParent.IsScaleY))
                {
                    TopLeft.Cursor = BottomRight.Cursor = Cursors.SizeNWSE;
                    TopRight.Cursor = BottomLeft.Cursor = Cursors.SizeNESW;
                }
                else
                {
                    TopLeft.Cursor = BottomRight.Cursor = Cursors.SizeNESW;
                    TopRight.Cursor = BottomLeft.Cursor = Cursors.SizeNWSE;
                }

                TopCenter.Cursor = BottomCenter.Cursor = Cursors.SizeWE;
                CenterRight.Cursor = CenterLeft.Cursor = Cursors.SizeNS;
            }
            else if (angle > 22)
            {
                if (ElementParent.IsScaleX | ElementParent.IsScaleY && !(ElementParent.IsScaleX & ElementParent.IsScaleY))
                {
                    TopCenter.Cursor = BottomCenter.Cursor = Cursors.SizeNWSE;
                    CenterRight.Cursor = CenterLeft.Cursor = Cursors.SizeNESW;
                }
                else
                {
                    TopCenter.Cursor = BottomCenter.Cursor = Cursors.SizeNESW;
                    CenterRight.Cursor = CenterLeft.Cursor = Cursors.SizeNWSE;
                }
                TopLeft.Cursor = BottomRight.Cursor = Cursors.SizeNS;
                TopRight.Cursor = BottomLeft.Cursor = Cursors.SizeWE;
            }
        }

        /// <summary>
        /// Chỉnh lại kích thước các phần tử khi có sự thay đổi phóng to/thu nhỏ trang soạn thảo
        /// </summary>
        /// <param name="scale"></param>
        public void SetScale(double scale)
        {
            scale = 1.0 / scale;
            RectRounder.StrokeThickness = 1.2 * scale;

            Rounder1px.StrokeThickness = 1.0 * scale;
            Rounder3px.StrokeThickness = 3.0 * scale;
            Rounder1pxmg3.StrokeThickness = 1.0 * scale;
            Rounder1pxmg3.Margin = new Thickness(3 * scale);

            Lineter.StrokeThickness = 1 * scale;
            Lineter.Height = 22 * scale;
            Lineter.Y2 = Lineter.Height;
            Lineter.Margin = new Thickness(0, -Lineter.Height, 0, 0);

            ScaleTransform _scale = new ScaleTransform(scale, scale);

            Rotate.LayoutTransform = _scale;
            Rotate.Margin = new Thickness(0, -40 * scale, 0, 0);

            TopLeft.LayoutTransform = _scale;
            TopLeft.Margin = new Thickness(-5 * scale);

            TopCenter.LayoutTransform = _scale;
            TopCenter.Margin = new Thickness(0, -5 * scale, 0, 0);

            TopRight.LayoutTransform = _scale;
            TopRight.Margin = new Thickness(-5 * scale);

            CenterRight.LayoutTransform = _scale;
            CenterRight.Margin = new Thickness(0, 0, -5 * scale, 0);

            BottomRight.LayoutTransform = _scale;
            BottomRight.Margin = new Thickness(-5 * scale);

            BottomCenter.LayoutTransform = _scale;
            BottomCenter.Margin = new Thickness(0, 0, 0, -5 * scale);

            BottomLeft.LayoutTransform = _scale;
            BottomLeft.Margin = new Thickness(-5 * scale);

            CenterLeft.LayoutTransform = _scale;
            CenterLeft.Margin = new Thickness(-5 * scale, 0, 0, 0);
        }

        #region IsTemplateSecond

        /// <summary>
        /// Cờ trạng thái cho giao diện thứ 2
        /// </summary>
        public bool IsTemplateSecond
        {
            get { return (bool)GetValue(IsTemplateSecondProperty); }
            set { SetValue(IsTemplateSecondProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsTemplateSecond.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTemplateSecondProperty =
            DependencyProperty.Register("IsTemplateSecond", typeof(bool), typeof(ContainerChrome), new PropertyMetadata(false, IsTemplateSecondCallBack));

        private static void IsTemplateSecondCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                (d as ContainerChrome).CanRotate = false;
                bool _added = false;
                foreach (var item in (d as ContainerChrome).gridRoot.Children)
                {
                    if (item is ResizeThumb)
                    {
                        ControlTemplate _template = Application.Current.TryFindResource($"{(item as ResizeThumb).Name}_RSThumb") as ControlTemplate;
                        if (_template != null)
                        {
                            (item as ResizeThumb).Template = _template;
                        }
                        else if (!_added)
                        {
                            var _resource = new ResourceDictionary();
                            _resource.Source = new Uri("pack://application:,,,/INV.Elearning.Core;Component/Resources/Generic.xaml");
                            Application.Current.Resources.MergedDictionaries.Add(_resource);
                            _added = true;
                        }
                    }
                }
            }
        }


        #endregion        
    }
}

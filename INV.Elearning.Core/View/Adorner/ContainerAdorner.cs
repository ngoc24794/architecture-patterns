﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    public class ContainerAdorner : Adorner
    {
        private VisualCollection _visuals;
        private ContainerChrome _chrome;

        public ContainerAdorner(ObjectElement adornedElement) : base(adornedElement)
        {
            SnapsToDevicePixels = true;
            this._chrome = new ContainerChrome();
            this._chrome.CanRotate = adornedElement.RuleConfiguration.CanRotate;
            this._chrome.IsHitTestVisible = adornedElement.RuleConfiguration.CanUserResize;

            Binding _binding = new Binding("DocumentPageScale");
            _binding.Source = Application.Current;
            _binding.Mode = BindingMode.OneWay;
            _chrome.SetBinding(ContainerChrome.ScaleProperty, _binding);

            this.Chrome.ElementParent = adornedElement;

            _binding = new Binding("IsContentFocused");
            _binding.Source = adornedElement;
            _binding.Mode = BindingMode.OneWay;
            _chrome.SetBinding(ContainerChrome.IsContentFocusedProperty, _binding);

            _binding = new Binding("IsTemplateSecond");
            _binding.Source = adornedElement;
            _binding.Mode = BindingMode.OneWay;
            _chrome.SetBinding(ContainerChrome.IsTemplateSecondProperty, _binding);

            _binding = new Binding("IsLinePath")
            {
                Source = adornedElement.Content,
                Mode = BindingMode.OneWay,
                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            };
            _chrome.SetBinding(ContainerChrome.IsLineContainerChromeProperty, _binding);

            if (adornedElement is StandardElement)
            {
                _binding = new Binding("IsLinePath")
                {
                    Source = (adornedElement as StandardElement)?.ShapePresent,
                    Mode = BindingMode.OneWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                };
                _chrome.SetBinding(ContainerChrome.IsLineContainerChromeProperty, _binding);
            }

            // this._chrome.Scale = 0.5;
            this._visuals = new VisualCollection(this);
            this._visuals.Add(Chrome);
        }

        /// <summary>
        /// Hàm khởi tạo cho kế thừa
        /// </summary>
        /// <param name="adornedElement"></param>
        /// <param name="isExtended"></param>
        public ContainerAdorner(ObjectElement adornedElement, bool isExtended) : base(adornedElement)
        {

        }

        /// <summary>
        /// Lấy đối tượng hiển thị chính
        /// </summary>
        public ContainerChrome Chrome { get => _chrome; }

        protected override int VisualChildrenCount
        {
            get
            {
                return this._visuals.Count;
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            return this._visuals[index];
        }


        //protected override Size MeasureOverride(Size constraint)
        //{
            //Chrome.Measure(constraint);
            //return constraint;
        //}

        protected override Size ArrangeOverride(Size finalSize)
        {
            Chrome.Arrange(new Rect(new Point(0, 0), finalSize));
            return finalSize;
        }

        #region Rectangle Visual

        DrawingVisual _rectVisual = null;

        #region RectStartPoint

        /// <summary>
        /// Điểm bắt đầu vẽ
        /// </summary>
        public Point RectStartPoint
        {
            get { return (Point)GetValue(RectStartPointProperty); }
            set { SetValue(RectStartPointProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RectStartPoint.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RectStartPointProperty =
            DependencyProperty.Register("RectStartPoint", typeof(Point), typeof(ContainerAdorner), new PropertyMetadata(new Point(), RectCallBack));


        #endregion

        #region RectEndPoint

        /// <summary>
        /// Điểm kết thúc
        /// </summary>
        public Point RectEndPoint
        {
            get { return (Point)GetValue(RectEndPointProperty); }
            set { SetValue(RectEndPointProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RectEndPoint.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RectEndPointProperty =
            DependencyProperty.Register("RectEndPoint", typeof(Point), typeof(ContainerAdorner), new PropertyMetadata(new Point(), RectCallBack));

        private static void RectCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ContainerAdorner).OnRectRender();
        }

        #endregion

        /// <summary>
        /// Cập nhật các Visual
        /// </summary>
        private void OnRectRender()
        {
            if (_rectVisual == null)
            {
                return;
            }

            using (DrawingContext _dc = _rectVisual.RenderOpen())
            {
                _dc.PushOpacity(0.5);

                //VisualBrush visualBrush = new VisualBrush();
                //visualBrush.Stretch = Stretch.Fill;
                //visualBrush.Visual = this._chrome.ElementParent;

                _dc.DrawRectangle(Brushes.White, new Pen(Brushes.Gray, 1.0), new Rect(RectStartPoint, RectEndPoint));
                _dc.Pop();
            }
        }

        /// <summary>
        /// Thêm một giao diện
        /// </summary>
        protected internal void AddRectVisual()
        {
            if (_rectVisual == null)
            {
                _rectVisual = new DrawingVisual();
                RectStartPoint = new Point();
                RectEndPoint = new Point(this.ActualWidth, this.ActualHeight);
                this._visuals.Add(_rectVisual);
            }
        }

        /// <summary>
        /// Xóa nội dung bề mặt
        /// </summary>
        protected internal void RemoveRectVisual()
        {
            if (_rectVisual != null)
            {
                this._visuals.Remove(_rectVisual);
                _rectVisual = null;
                this.RectStartPoint = this.RectEndPoint = new Point();
            }
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);
            this.Chrome?.ElementParent?.MouseDownExcute(this);
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseUp(e);
            this.Chrome?.ElementParent?.MouseUpExcute(this);
        }
        #endregion
    }
}

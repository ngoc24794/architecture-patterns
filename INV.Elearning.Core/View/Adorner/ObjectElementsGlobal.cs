﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace INV.Elearning.Core.View
{
    public static class ObjectElementsGlobal
    {
        #region ObjectElements
        public static List<ObjectElement> ObjectElements { get; set; }

        public static List<ObjectElement> GetObjectElements(Canvas canvas)
        {
            var _result = new List<ObjectElement>();
            if (canvas == null)
                return _result;

            foreach (var child in canvas.Children)
            {
                if ((child is ObjectElement) && (child as ObjectElement).IsSelected && child != Source)
                {
                    _result.Add(child as ObjectElement);
                }
            }

            return _result;
        }

        public static object Source = null;
        #endregion
    }
}

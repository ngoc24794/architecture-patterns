﻿using INV.Elearning.Core.Helper;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Hỗ trợ tạo vùng chọn cho DesignerCanvas
    /// </summary>
    public class AreaSelectionAdorner : Adorner
    {
        #region Fields
        // Điểm bắt đầu và điểm kết thúc vùng chọn
        Point? startPoint, endPoint;
        // Bút vẽ vùng chọn
        Pen pen;
        UIElement panel = null;
        Brush fill = null;
        #endregion

        #region Contructions
        public AreaSelectionAdorner(UIElement panel, Point? dragStartPoint) : base(panel)
        {
            this.panel = panel;
            startPoint = dragStartPoint;
            pen = new Pen(Brushes.LightSlateGray, 1);
            fill = new BrushConverter().ConvertFromString("#99CCCCCC") as Brush;
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Vẽ vùng chọn hình chữ nhật trong khi rê chuột
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));

            if (startPoint.HasValue && endPoint.HasValue)
            {
                Rect rect = new Rect(startPoint.Value, endPoint.Value);
                GuidelineSet guidelines = new GuidelineSet();
                guidelines.GuidelinesX.Add(rect.Left + pen.Thickness / 2.0);
                guidelines.GuidelinesX.Add(rect.Right + pen.Thickness / 2.0);
                guidelines.GuidelinesY.Add(rect.Top + pen.Thickness / 2.0);
                guidelines.GuidelinesY.Add(rect.Bottom + pen.Thickness / 2.0);
                drawingContext.PushGuidelineSet(guidelines);
                drawingContext.DrawRectangle(fill, pen, rect);
                drawingContext.Pop();
            }
        }

        /// <summary>
        /// Xử lí sự kiện di chuyển chuột
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            // Nếu nút trái chuột được nhấn
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                // Nếu phần tử này chưa bắt chuột 
                if (!IsMouseCaptured)
                    CaptureMouse(); // thì bắt chuột

                endPoint = e.GetPosition(this);
                InvalidateVisual();
            }
            else if (IsMouseCaptured) ReleaseMouseCapture();
        }

        /// <summary>
        /// Xử lí sự kiện thả chuột
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            UpdateSelection();
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(panel);
            if (adornerLayer != null)
                adornerLayer.Remove(this);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Cập nhật lại danh sách các đối tượng được chọn trong canvas cha
        /// </summary>
        void UpdateSelection()
        {
            if (startPoint == null || endPoint == null || (Application.Current as IAppGlobal).SelectedSlide?.IsSelected != true) return;
            // Tạo vùng chọn hình chữ nhật
            Rect areaSelection = new Rect(startPoint.Value, endPoint.Value);

            foreach (var item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements)
            {
                // Đường bao đối tượng hiện tại
                Rect itemRect = VisualTreeHelper.GetDescendantBounds(item);
                Rect itemBounds = item.TransformToAncestor(panel).TransformBounds(itemRect);

                // Nếu vùng chọn chứa đối tượng hiện tại 
                if (areaSelection.Contains(itemBounds))
                {
                    item.IsSelected = true;
                }
            }
        }
        #endregion
    }
}

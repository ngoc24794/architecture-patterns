﻿
namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Interface cho một khung cơ bản
    /// </summary>
    public interface IObjectElement
    {
        /// <summary>
        /// Giá trị lựa chọn
        /// </summary>
        bool IsSelected { set; get; }
        /// <summary>
        /// Giá trị căn trái
        /// </summary>
        double Left { set; get; }
        /// <summary>
        /// Giá trị căn trên
        /// </summary>
        double Top { set; get; }

        /// <summary>
        /// Giá trị góc quay
        /// </summary>
        double Angle { set; get; }
    }
}

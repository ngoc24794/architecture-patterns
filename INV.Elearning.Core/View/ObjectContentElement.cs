﻿using INV.Elearning.Core.Helper;
using System.Windows;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Lớp thể hiện các đối tượng thuộc về đối tượng trên Layout
    /// </summary>
    public class ObjectContentElement : FrameworkElement, IObjectElement
    {
        #region IsSelected
        
        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính lựa chọn cho đối tượng
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        
        /// <summary>
        /// Lấy hoặc cài đặt căn lề trái
        /// </summary>
        public double Left
        {
            get { return (double)GetValue(LeftProperty); }
            set { SetValue(LeftProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Left.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftProperty =
            DependencyProperty.Register("Left", typeof(double), typeof(ObjectContentElement), new PropertyMetadata(0.0));

        /// <summary>
        /// Lấy hoặc cài đặt giá trị căn lề trên
        /// </summary>
        public double Top
        {
            get { return (double)GetValue(TopProperty); }
            set { SetValue(TopProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Top.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TopProperty =
            DependencyProperty.Register("Top", typeof(double), typeof(ObjectContentElement), new PropertyMetadata(0.0));


        /// <summary>
        /// Lây hoặc cài đặt giá trị góc quay
        /// </summary>
        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(ObjectContentElement), new PropertyMetadata(0.0));



        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(ObjectContentElement), new PropertyMetadata(false, SelectedCallBack));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void SelectedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (Application.Current is IAppGlobal)
            {
                ObjectContentElement _contentElement = d as ObjectContentElement;
                if ((bool)e.NewValue) //Nếu đang được lựa chọn
                {
                    if (!(Application.Current as IAppGlobal).SelectedContentElements.Contains(_contentElement)) //Nếu chưa tồn tại trong danh sách
                        (Application.Current as IAppGlobal).SelectedContentElements.Add(_contentElement);
                }
                else
                {
                    if ((Application.Current as IAppGlobal).SelectedContentElements.Contains(_contentElement)) //Nếu đã tồn tại trong danh sách
                        (Application.Current as IAppGlobal).SelectedContentElements.Remove(_contentElement);
                }
            }
        }

        #endregion        
    }
}

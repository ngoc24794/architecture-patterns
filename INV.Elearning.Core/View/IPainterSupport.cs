﻿using INV.Elearning.Core.ViewModel;

namespace INV.Elearning.Core.View
{
    public interface IPainterSupport
    {
        /// <summary>
        /// Lấy nội dung Painter
        /// </summary>
        /// <returns></returns>
        IFormatPainter GetPainter(IFormatPainter painter);

        /// <summary>
        /// Cài đặt nội dung Painter
        /// </summary>
        /// <param name="painter"></param>
        void SetPainter(IFormatPainter painter);
    }
}

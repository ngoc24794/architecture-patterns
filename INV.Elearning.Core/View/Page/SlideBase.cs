﻿using INV.Elearning.Core.Helper;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using INV.Elearning.Core.Model;
using System.Windows.Input;
using System.Windows.Media;
using INV.Elearning.Animations;
using System.Collections.Generic;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using INV.Elearning.Core.View.Theme;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.Model.Theme;
using System.ComponentModel;
using System.Windows.Data;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Khung mẫu
    /// </summary>
    public abstract class SlideBase : Grid, ITriggerableObject, IUpdatePropertyByUndo, IThemeSupport
    {
        #region Contructors
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public SlideBase()
        {
            this.MainLayout = new LayoutBase();
            this.Focusable = true;
            this.Visibility = Visibility.Hidden;
            this.Background = Brushes.White;
            TriggerData = new ObservableCollection<TriggerViewModelBase>();
            SlideName = Application.Current.TryFindResource("App_SlideBaseName")?.ToString();
            ID = ObjectElementsHelper.RandomString(10);
            this.PageConfig = new PageConfig();
            if (this is NormalSlide)
            {
                IsDate = false;
                IsSlideNumber = false;
                IsFooter = false;
                PageConfig.NextButtonEnable = true;
                PageConfig.PreviousButtonEnable = true;
                PageConfig.SubmitButtonEnable = false;
            }
            else if (this is IResultSlide)
            {
                PageConfig.NextButtonEnable = true;
                PageConfig.PreviousButtonEnable = true;
                PageConfig.SubmitButtonEnable = false;
            }
            else
            {
                PageConfig.NextButtonEnable = false;
                PageConfig.PreviousButtonEnable = false;
                PageConfig.SubmitButtonEnable = true;
            }

            RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.LowQuality);
        }
        #endregion

        #region Transition

        /// <summary>
        /// Hiệu ứng chuyển trang
        /// </summary>
        public Transiton Transition
        {
            get { return (Transiton)GetValue(TransitionProperty); }
            set { SetValue(TransitionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Transition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransitionProperty =
            DependencyProperty.Register("Transition", typeof(Transiton), typeof(SlideBase), new PropertyMetadata(null, TransitionCallback));

        private static void TransitionCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as SlideBase).HasTransition = e.NewValue != null && !(e.NewValue is NoneTransition);
        }

        #endregion

        #region LayoutID

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính id của Layout cho slide
        /// </summary>
        public string LayoutID
        {
            get { return (string)GetValue(LayoutIDProperty); }
            set { SetValue(LayoutIDProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutIDProperty =
            DependencyProperty.Register("LayoutID", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty, LayoutIDCallback));

        private static void LayoutIDCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase owner = d as SlideBase;
            (Application.Current as IThemesControl).UpdateLayout(owner);
            if (Global.CanPushUndo && owner.IsLoaded)
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.PropertyName = "LayoutID";
                step.Source = owner;
                step.NewValue = e.NewValue;
                step.OldValue = e.OldValue;
                Global.PushUndo(step);
            }
        }


        #endregion

        #region MainLayout

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Layout chính của Silde<br/>
        /// Layout chính có nhiệm vụ chứa có đối tượng vào trang chính<br/>
        /// Cần thực hiện cài đặt giá trị lại với mỗi loại Slide tương ứng
        /// </summary>
        public LayoutBase MainLayout
        {
            get { return (LayoutBase)GetValue(MainLayoutProperty); }
            set
            {
                if (value == null) return; //Không cho cấu hình Null
                SetValue(MainLayoutProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for MainLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MainLayoutProperty =
            DependencyProperty.Register("MainLayout", typeof(LayoutBase), typeof(SlideBase), new PropertyMetadata(null, MainLayoutCallBack));

        /// <summary>
        /// Thay đổi khi MainLayout thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void MainLayoutCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is LayoutBase) //Xóa Layout cũ nếu có
            {
                (e.OldValue as LayoutBase).IsMainLayout = false;
                (d as SlideBase).Children.Remove(e.OldValue as LayoutBase);
            }

            if (e.NewValue is LayoutBase layoutBase)
            {
                (e.NewValue as LayoutBase).IsMainLayout = true;
                (d as SlideBase).Children.Add(layoutBase); //Thêm Layout mới
                (d as SlideBase).HasVideo = layoutBase.Elements.FirstOrDefault(y => y is IVideoElement) != null;
                (d as SlideBase).HasAudio = layoutBase.Elements.FirstOrDefault(y => y is IAudioElement) != null;
            }
        }

        #endregion

        #region Layouts

        private ObservableCollection<LayoutBase> _layouts;
        /// <summary>
        /// Lấy danh sách các layout có trong Slide
        /// </summary>
        public ObservableCollection<LayoutBase> Layouts
        {
            get
            {
                if (_layouts == null)
                {
                    _layouts = new ObservableCollection<LayoutBase>();
                    _layouts.CollectionChanged += LayoutsChanged;
                }
                return _layouts;
            }
        }

        /// <summary>
        /// Thay đổi trong danh sách các layout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (LayoutBase layout in e.NewItems) //Thêm mới các phần tử vào trong Canvas
                    {
                        if (layout.Parent == null) //Nếu đã được chứa trong một Panel khác thì không thêm mới. Sẽ phát sinh lỗi nếu cố tình thêm mới
                        {
                            this.Children.Add(layout);
                            if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                            {
                                Global.PushUndo(new AddLayoutToSlideStep(layout, _layouts));
                            }
                        }
                        else if (layout == MainLayout)
                        {
                            Layouts.Remove(layout);
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    if (e.OldItems != null)
                        foreach (LayoutBase layout in e.OldItems) //Xóa các phần tử khỏi Canvas
                        {
                            if (layout != MainLayout) //Không xóa form chính
                            {
                                this.Children.Remove(layout);
                                if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                                {
                                    Global.PushUndo(new RemoveLayoutFromSlideStep(layout, _layouts, e.OldStartingIndex));
                                }
                            }
                        }
                    MainLayout.IsSelected = true;
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    for (int i = 0; i < this.Children.Count; i++)
                    {
                        if (this.Children[i] != MainLayout)
                        {
                            this.Children.RemoveAt(i);
                            i--;
                        }
                    }
                    MainLayout.IsSelected = true;
                    break;
            }
        }
        #endregion

        #region DocumentParent

        /// <summary>
        /// Lay hoc cai dat thuoc tinh tai lieu chua slide
        /// </summary>
        public DocumentControl DocumentParent
        {
            get { return (DocumentControl)GetValue(DocumentParentProperty); }
            set { SetValue(DocumentParentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DocumentParent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DocumentParentProperty =
            DependencyProperty.Register("DocumentParent", typeof(DocumentControl), typeof(SlideBase), new PropertyMetadata(null));


        #endregion

        #region ThemeLayout

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Layout theo theme<br/>
        /// </summary>
        public ThemeLayout ThemeLayout
        {
            get { return (ThemeLayout)GetValue(ThemeLayoutProperty); }
            set { SetValue(ThemeLayoutProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeLayoutProperty =
            DependencyProperty.Register("ThemeLayout", typeof(ThemeLayout), typeof(SlideBase), new PropertyMetadata(null, ThemeLayoutCallBack));

        /// <summary>
        /// Sự kiện khi thêm mới một theme layout
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ThemeLayoutCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is ThemeLayout) //Xóa Layout cũ nếu có
            {
                (d as SlideBase).MainLayout.Children.Remove(e.OldValue as ThemeLayout);
            }

            //if (e.NewValue is ThemeLayout themeLayout)
            //{
            //    SetZIndex(themeLayout, -100); //Cài đặt vị trí thấp nhất
            //    (d as SlideBase).MainLayout.Children.Add(themeLayout); //Thêm Layout mới
            //}


            if ((d as SlideBase).IsSelected || Global.IsDataLoading) (d as SlideBase).AddThemeLayout();
        }

        /// <summary>
        /// Thêm hiệu ứng vào
        /// </summary>
        private void AddThemeLayout()
        {
            if (ThemeLayout != null)
            {
                if (ThemeLayout.Parent is Panel panel)
                {
                    panel.Children.Remove(ThemeLayout);
                }

                SetZIndex(ThemeLayout, -100); //Cài đặt vị trí thấp nhất
                this.MainLayout.Children.Add(ThemeLayout);
            }
        }

        #endregion

        #region IsHideBackground
        /// <summary>
        /// Có hiện hình ảnh background hay không
        /// </summary>
        public bool IsHideBackground
        {
            get { return (bool)GetValue(IsHideBackgroundProperty); }
            set { SetValue(IsHideBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsHideBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsHideBackgroundProperty =
            DependencyProperty.Register("IsHideBackground", typeof(bool), typeof(SlideBase), new PropertyMetadata(false, new PropertyChangedCallback(IsHideBackgroundPropertyChanged)));

        private static void IsHideBackgroundPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                if ((d as SlideBase).ThemeLayout != null)
                    (d as SlideBase).ThemeLayout.Visibility = Visibility.Hidden;
            }
            else
            {
                if ((d as SlideBase).ThemeLayout != null)
                    (d as SlideBase).ThemeLayout.Visibility = Visibility.Visible;
            }
           (d as SlideBase).MainLayout.UpdateThumbnail();
        }
        #endregion

        #region Data

        /// <summary>
        /// Lấy hoặc cài đặt dữ liệu
        /// </summary>
        public PageElementBase Data
        {
            get { return (PageElementBase)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(PageElementBase), typeof(SlideBase), new PropertyMetadata(null));



        #endregion

        #region ID

        /// <summary>
        /// Lấy hoặc cài đặt mã số cho đối tượng
        /// </summary>
        public string ID
        {
            get { return (string)GetValue(IDProperty); }
            set { SetValue(IDProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IDProperty =
            DependencyProperty.Register("ID", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty));


        #endregion

        #region Note

        /// <summary>
        /// Ghi chú cho Slide
        /// </summary>
        public string Note
        {
            get { return (string)GetValue(NoteProperty); }
            set { SetValue(NoteProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Note.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NoteProperty =
            DependencyProperty.Register("Note", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty));


        #endregion

        #region SlideName

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính
        /// </summary>
        public string SlideName
        {
            get { return (string)GetValue(SlideNameProperty); }
            set { SetValue(SlideNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SlideName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SlideNameProperty =
            DependencyProperty.Register("SlideName", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty));

        #endregion

        #region IsSelected

        /// <summary>
        /// Lấy hoặc cài đặt giá trị lựa chọn cho đối tượng
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(SlideBase), new PropertyMetadata(false, IsSelectedCallBack));

        /// <summary>
        /// Hàm được gọi khi giá trị IsSelected thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsSelectedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            if ((bool)e.NewValue)
            {
                if (_owner.DocumentParent == null || _owner.Parent == null)
                {
                    _owner.IsSelected = false;
                    return;
                }
                if (_owner.IsSelected && _owner.Parent == null && _owner.DocumentParent != null)
                {
                    _owner.DocumentParent.GRID_ROOT.Children.Add(_owner);
                }
                _owner.Visibility = Visibility.Visible;
                if ((Application.Current as IAppGlobal).SelectedSlide != null && (Application.Current as IAppGlobal).SelectedSlide != _owner)
                    (Application.Current as IAppGlobal).SelectedSlide.Visibility = Visibility.Hidden;

                if (!(Application.Current as IAppGlobal).SelectedSlides.Contains(_owner))
                    (Application.Current as IAppGlobal).SelectedSlides.Add(_owner);
                (Application.Current as IAppGlobal).SelectedSlide = _owner;

                SlideMaster slide = null;
                if (_owner is SlideMaster slideMaster)
                {
                    slide = slideMaster;
                }
                else if (_owner is LayoutMaster layoutMaster)
                {
                    slide = layoutMaster.SlideParent;
                }

                if (slide != null && !_owner.IsRemove)
                {
                    ETheme eTheme = (Application.Current as IAppGlobal).LocalThemesCollection.FirstOrDefault(x => x.ID == slide.ID);
                    if (eTheme != null)
                    {
                        (Application.Current as IAppGlobal).SelectedTheme = eTheme;
                    }

                    ICollectionView view = CollectionViewSource.GetDefaultView((Application.Current as IAppGlobal).LocalThemesCollection);
                    view.Refresh();
                }

                _owner.AddThemeLayout();//Mới thêm
            }
            else
            {
                _owner.Visibility = Visibility.Collapsed;
                ObjectElementsHelper.UnSelectedAll(); //Huy tat ca lau chon doi tuong
                if ((Application.Current as IAppGlobal).SelectedSlides.Contains(_owner))
                    (Application.Current as IAppGlobal).SelectedSlides.Remove(_owner);

                if ((Application.Current as IAppGlobal).SelectedSlides.Count > 0)
                {
                    var _lastSeletedSlide = (Application.Current as IAppGlobal).SelectedSlides.LastOrDefault();
                    _lastSeletedSlide.Visibility = Visibility.Visible;
                    (Application.Current as IAppGlobal).SelectedSlide = _lastSeletedSlide;
                }
                else
                {
                    if (Keyboard.Modifiers == ModifierKeys.Control && _owner.Parent != null)
                        _owner.IsSelected = true;
                }
            }
        }

        #endregion

        #region LayoutMaster

        /// <summary>
        /// Layout Master dang duoc lua chon
        /// </summary>
        public ELayoutMaster LayoutMaster
        {
            get { return (ELayoutMaster)GetValue(LayoutMasterProperty); }
            set { SetValue(LayoutMasterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutMaster.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutMasterProperty =
            DependencyProperty.Register("LayoutMaster", typeof(ELayoutMaster), typeof(SlideBase), new PropertyMetadata(null, LayoutMasterCallback));

        private static void LayoutMasterCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            _owner.ThemeLayout.UpdateUIB(_owner.LayoutMaster.MainLayer);
        }


        #endregion

        #region SelectedLayout
        /// <summary>
        /// Sự kiện layer hiện hành bị thay đổi
        /// </summary>
        public event SelectedItemChangedEventHanlder SelectedLayoutChanged;

        /// <summary>
        /// Lấy Layout đang được lựa chọn
        /// </summary>
        public LayoutBase SelectedLayout
        {
            get
            {
                var _result = (LayoutBase)GetValue(SelectedLayoutProperty);
                if (_result == null)
                {
                    if (MainLayout != null)
                    {
                        _result = MainLayout;
                    }
                    else
                        _result = this.Layouts.FirstOrDefault();
                }
                if (_result?.IsSelected == false)
                {
                    _result.IsSelected = true;
                }
                return _result;
            }
            internal set
            {
                if (value == null || value != MainLayout && !Layouts.Contains(value))
                    return;
                //SelectedLayoutChanged?.Invoke(this, new SelectedItemEventArgs(value, (LayoutBase)GetValue(SelectedLayoutProperty)));
                SetValue(SelectedLayoutProperty, value);
                SelectedLayoutChanged?.Invoke(this, new SelectedItemEventArgs(value));
            }
        }

        // Using a DependencyProperty as the backing store for SelectedLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedLayoutProperty =
            DependencyProperty.Register("SelectedLayout", typeof(LayoutBase), typeof(SlideBase), new PropertyMetadata(null));

        #endregion

        #region TriggerData
        public ObservableCollection<TriggerViewModelBase> TriggerData { get; set; }
        #endregion

        #region HasTransition

        /// <summary>
        /// Tồn tại hiệu ứng chuyển trang
        /// </summary>
        public bool HasTransition
        {
            get { return (bool)GetValue(HasTransitionProperty); }
            private set { SetValue(HasTransitionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasTransition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasTransitionProperty =
            DependencyProperty.Register("HasTransition", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));

        #endregion

        #region HasVideo
        /// <summary>
        /// Tồn tại video
        /// </summary>
        public bool HasVideo
        {
            get { return (bool)GetValue(HasVideoProperty); }
            internal set { SetValue(HasVideoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasVideo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasVideoProperty =
            DependencyProperty.Register("HasVideo", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));

        #endregion

        #region HasAudio
        /// <summary>
        /// Tồn tại video
        /// </summary>
        public bool HasAudio
        {
            get { return (bool)GetValue(HasAudioProperty); }
            internal set { SetValue(HasAudioProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasAudio.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasAudioProperty =
            DependencyProperty.Register("HasAudio", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));

        #endregion

        #region Timing
        /// <summary>
        /// Thời gian diễn ra trang
        /// </summary>
        public Timing Timing
        {
            get
            {
                return this.MainLayout?.Timing;
            }
        }
        #endregion

        #region CanShowInMenu

        /// <summary>
        /// Có thể hiện thị trên thanh menu
        /// </summary>
        public bool CanShowInMenu
        {
            get { return (bool)GetValue(CanShowInMenuProperty); }
            set { SetValue(CanShowInMenuProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanShowInMenu.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanShowInMenuProperty =
            DependencyProperty.Register("CanShowInMenu", typeof(bool), typeof(SlideBase), new PropertyMetadata(true));


        #endregion

        #region SlideConfig

        /// <summary>
        /// Cài đặt thuộc tính của Slide
        /// </summary>
        public PageConfig PageConfig
        {
            get { return (PageConfig)GetValue(PageConfigProperty); }
            set { SetValue(PageConfigProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PageConfig.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PageConfigProperty =
            DependencyProperty.Register("PageConfig", typeof(PageConfig), typeof(SlideBase), new PropertyMetadata(null));


        #endregion
        #region SelectLayoutType

        /// <summary>
        /// Kiểu Layout đang được chọn
        /// </summary>
        public LayoutType SelectLayoutType
        {
            get { return (LayoutType)GetValue(SelectLayoutTypeProperty); }
            set { SetValue(SelectLayoutTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectLayoutType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectLayoutTypeProperty =
            DependencyProperty.Register("SelectLayoutType", typeof(LayoutType), typeof(SlideBase), new PropertyMetadata(LayoutType.None));


        #endregion

        #region Methods

        /// <summary>
        /// Lưu trữ đối tượng ra dạng hình ảnh
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="size">Kích thước xuất</param>
        /// <returns></returns>
        public bool SaveToImage(string imagePath, Size size = new Size())
        {
            return this.MainLayout?.SaveToImage(imagePath, size) == true;
        }

        /// <summary>
        /// Tải lại các giao diện từ dữ liệu
        /// </summary>
        /// <param name="data"></param>
        public virtual void LoadData(PageElementBase data)
        {
            this.MainLayout.Elements.Clear(); //Xóa tất cả phần tử trong Layout chính
            this.SlideName = data.Name;
            this.ID = data.ID;
            this.IsTitle = data.IsTitle;
            this.IsFooter = data.IsFooters;
            this.IsText = data.IsText;
            this.IsDate = data.IsDate;
            this.IsSlideNumber = data.IsSlideNumber;
            if (Global.IsPasting) //Nếu đang dán thì tạo mới id
                this.ID = Helper.ObjectElementsHelper.RandomString(13);
            this.CanShowInMenu = data.CanShowInMenu;
            this.Transition = data.Transition;
            this.PageConfig = data.PageConfig;
            this.SelectLayoutType = data.SelectLayoutType;
            this.Note = data.Note;
            this.IsFollowMasterBackground = data.IsFollowBackground;
            Dictionary<Type, IModule> _dictionary = new Dictionary<Type, IModule>();
            if (data.MainLayer != null)
            {
                var _mainLayout = LayoutHelper.LoadDataForSlideLayout(data.MainLayer) as LayoutBase;
                if (_mainLayout != null)
                {
                    MainLayout = _mainLayout;
                }
            }

            this.Layouts.Clear();
            foreach (var layout in data.PageLayers)
            {
                var _layout = LayoutHelper.LoadDataForSlideLayout(layout) as LayoutBase;
                if (_layout != null)
                {
                    this.Layouts.Add(_layout);
                }
            }
            this.LayoutID = data.IDLayout;
            this.IsHideBackground = data.IsHideBackground;

        }

        /// <summary>
        /// Hàm tải nhanh
        /// </summary>
        /// <param name="page"></param>
        internal void QuickUpdateUI(NormalPage page)
        {
            this.Data = page;
        }

        /// <summary>
        /// Ghi đè lại hàm cập nhật dữ liệu
        /// </summary>
        public virtual void RefreshData()
        {
            if (this.Data == null)
            {
                this.Data = new NormalPage();
            }

            this.Data.CanShowInMenu = this.CanShowInMenu;
            this.Data.ID = this.ID;
            this.Data.IsTitle = this.IsTitle;
            this.Data.IsFooters = this.IsFooter;
            this.Data.IsDate = this.IsDate;
            this.Data.IsSlideNumber = this.IsSlideNumber;
            this.Data.IsText = this.IsText;
            this.Data.Name = this.SlideName;
            this.Data.IsHideBackground = this.IsHideBackground;
            this.MainLayout?.RefreshData();
            this.Data.MainLayer = this.MainLayout.Data;
            this.Data.Transition = Transition;
            this.Data.PageConfig = this.PageConfig;
            this.Data.Note = this.Note;
            this.Data.IDLayout = LayoutID;
            this.Data.SelectLayoutType = SelectLayoutType;
            this.Data.IsFollowBackground = IsFollowMasterBackground;
            this.Data.PageLayers.Clear();
            foreach (var layout in this.Layouts)
            {
                layout.RefreshData();
                this.Data.PageLayers.Add(layout.Data);
            }
        }


        /// <summary>
        /// Cập nhật dữ liệu từ Undo
        /// </summary>
        public virtual void OnPropertyChangeByUndo()
        {
            if (Global.UndoProcessing)
            {
                //IsSelected = true;
                //SlideHelper.UnSlectedAll();
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Sự kiện thả chuột trên khung slide
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            (Application.Current as IAppGlobal)?.SelectedSlide?.MainLayout?.UpdateDescendantBounds();
            (Application.Current as IAppGlobal)?.SelectedSlide?.MainLayout?.UpdateRectBounds();
        }

        #endregion

        #region IsTitle
        /// <summary>
        /// Có hiện Khung Title hay không
        /// </summary>
        public bool IsTitle
        {
            get { return (bool)GetValue(IsTitleProperty); }
            set { SetValue(IsTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTitleProperty =
            DependencyProperty.Register("IsTitle", typeof(bool), typeof(SlideBase), new PropertyMetadata(true, IsTitlePropertyCallback));

        private static void IsTitlePropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            Global.StartGrouping("isTitleLayout");
            (Application.Current as IAppGlobal).IsTitleSlideChanged((bool)e.NewValue, _owner);
            if (Global.CanPushUndo && _owner.IsLoaded)
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.Source = _owner;
                step.NewValue = e.NewValue;
                step.OldValue = e.OldValue;
                step.PropertyName = "IsTitle";
                Global.PushUndo(step);
            }
            Global.StopGrouping("isTitleLayout");
        }

        #endregion

        #region IsFooters
        /// <summary>
        /// Có hiện khung Footer hay không
        /// </summary>
        public bool IsFooter
        {
            get { return (bool)GetValue(IsFooterProperty); }
            set { SetValue(IsFooterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsFooter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsFooterProperty =
            DependencyProperty.Register("IsFooter", typeof(bool), typeof(SlideBase), new PropertyMetadata(true, IsFooterPropertyCallback));

        private static void IsFooterPropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            Global.StartGrouping("isFooterLayout");
            (Application.Current as IAppGlobal).IsFootersSlideChanged((bool)e.NewValue, _owner);
            if (Global.CanPushUndo && _owner.IsLoaded)
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.Source = _owner;
                step.NewValue = e.NewValue;
                step.OldValue = e.OldValue;
                step.PropertyName = "IsFooter";
                Global.PushUndo(step);
            }
            Global.StopGrouping("isFooterLayout");


        }
        #endregion

        #region IsText
        /// <summary>
        /// Có hiện khung text hay không
        /// </summary>
        public bool IsText
        {
            get { return (bool)GetValue(IsTextProperty); }
            set { SetValue(IsTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTextProperty =
            DependencyProperty.Register("IsText", typeof(bool), typeof(SlideBase), new PropertyMetadata(true, IsTextPropertyCallback));

        private static void IsTextPropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            Global.StartGrouping("isTextLayout");
            (Application.Current as IAppGlobal).IsTextSlideChanged((bool)e.NewValue, _owner);
            if (Global.CanPushUndo && _owner.IsLoaded)
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.Source = _owner;
                step.NewValue = e.NewValue;
                step.OldValue = e.OldValue;
                step.PropertyName = "IsText";
                Global.PushUndo(step);
            }
            Global.StopGrouping("isTextLayout");
        }
        #endregion

        #region IsDate
        /// <summary>
        /// Có hiện khung ngày tháng hay không
        /// </summary>
        public bool IsDate
        {
            get { return (bool)GetValue(IsDateProperty); }
            set { SetValue(IsDateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDateProperty =
            DependencyProperty.Register("IsDate", typeof(bool), typeof(SlideBase), new PropertyMetadata(true, IsDatePropertyCallback));

        private static void IsDatePropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            Global.StartGrouping("IsDateLayout");
            (Application.Current as IAppGlobal).IsDateSlideChanged((bool)e.NewValue, _owner);
            if (Global.CanPushUndo && _owner.IsLoaded)
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.Source = _owner;
                step.NewValue = e.NewValue;
                step.OldValue = e.OldValue;
                step.PropertyName = "IsDate";
                Global.PushUndo(step);
            }
            Global.StopGrouping("IsDateLayout");
        }
        #endregion

        #region IsSlideNumber
        /// <summary>
        /// Có hiện khung đánh số slide hay không
        /// </summary>
        public bool IsSlideNumber
        {
            get { return (bool)GetValue(IsSlideNumberProperty); }
            set { SetValue(IsSlideNumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSlideNumber.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSlideNumberProperty =
            DependencyProperty.Register("IsSlideNumber", typeof(bool), typeof(SlideBase), new PropertyMetadata(true, IsSlideNumberPropertyCallback));

        private static void IsSlideNumberPropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            SlideBase _owner = d as SlideBase;
            Global.StartGrouping("IsSlideNumberLayout");
            (Application.Current as IAppGlobal).IsSlideNumberSlideChanged((bool)e.NewValue, _owner);
            if (Global.CanPushUndo && _owner.IsLoaded)
            {
                UndoRedoByProperty step = new UndoRedoByProperty();
                step.Source = _owner;
                step.NewValue = e.NewValue;
                step.OldValue = e.OldValue;
                step.PropertyName = "IsSlideNumber";
                Global.PushUndo(step);
            }
            Global.StopGrouping("IsSlideNumberLayout");
        }

        #endregion

        #region LayoutIndex


        public int LayoutIndex
        {
            get { return (int)GetValue(LayoutIndexProperty); }
            set { SetValue(LayoutIndexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutIndexProperty =
            DependencyProperty.Register("LayoutIndex", typeof(int), typeof(SlideBase), new PropertyMetadata(0));


        #endregion

        #region SelectedLayoutMaster


        public LayoutMaster SelectedLayoutMaster
        {
            get { return (LayoutMaster)GetValue(SelectedLayoutMasterProperty); }
            set { SetValue(SelectedLayoutMasterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedLayoutMaster.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedLayoutMasterProperty =
            DependencyProperty.Register("SelectedLayoutMaster", typeof(LayoutMaster), typeof(SlideBase), new PropertyMetadata(null, new PropertyChangedCallback(SelectedLayoutMasterPropertyChanged)));

        private static void SelectedLayoutMasterPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// Cập nhật lại phông chữ theo theme
        /// </summary>
        public virtual void UpdateThemeFont()
        {
            if (this.IsLoaded)
            {
                this.MainLayout?.UpdateThemeFont();
                this.MainLayout?.UpdateThumbnail();
                foreach (LayoutBase layout in Layouts)
                {
                    layout.UpdateThemeFont();
                    layout.UpdateThumbnail();
                }
            }
        }

        /// <summary>
        /// Cập nhật lại laout
        /// </summary>
        /// <param name="eLayoutMaster"></param>
        public virtual void UpdateLayoutMaster(ETheme eTheme, ETheme oldTheme)
        {
            if (this.ThemeLayout == null) this.ThemeLayout = new ThemeLayout();
            if (this.DocumentParent?.SelectedTheme?.SlideMasters.Count > 0)
            {
                this.ThemeLayout.UpdateUIB(DocumentParent.SelectedTheme.SlideMasters[0].MainLayer);

            }
            else
            {
                this.ThemeLayout.Elements.Clear();
            }
            if (DocumentParent?.SelectedTheme.SlideMasters.Count > 0)
            {
                foreach (ELayoutMaster item in DocumentParent?.SelectedTheme.SlideMasters[0].LayoutMasters)
                {
                    if (item.LayoutType == SelectLayoutType)
                    {
                        this.LayoutID = item.ID;
                    }
                }
            }
            UpdateThemeFont();
            UpdateThemeColor();
            (Application.Current as IAppGlobal).UpdateThemeSlide(eTheme, oldTheme);
        }

        public void UpdateLayoutTheme(ETheme eTheme, ETheme oldTheme)
        {
            if (string.IsNullOrEmpty(LayoutID))
            {
                LayoutID = string.Empty;
                LayoutID = eTheme.SlideMasters[0].LayoutMasters[0].ID;
                eTheme.SlideMasters[0].LayoutMasters[0].ListSlideID.Add(ID);
            }
            else
            {
                //Trường hợp load
                if (Global.IsPasting || Global.IsDataLoading)
                {
                    (Application.Current as IThemesControl).UpdateLayout(this);
                }
                else
                {
                    //Trưòng hợp ko load
                    if (DocumentParent?.SelectedTheme.SlideMasters.Count > 0)
                    {
                        LayoutID = null;
                        GetLayoutID();
                    }
                }

            }


            UpdateThemeFont();
            UpdateThemeColor();
            (Application.Current as IAppGlobal).UpdateThemeSlide(eTheme, oldTheme);
        }

        /// <summary>
        /// Lấy layout ID của Slide
        /// </summary>
        private void GetLayoutID()
        {
            foreach (ELayoutMaster item in DocumentParent?.SelectedTheme.SlideMasters[0].LayoutMasters)
            {
                if (item.ListSlideID.Contains(ID))
                {
                    LayoutID = item.ID;
                }
            }
        }

        /// <summary>
        /// Lấy ListSlideID
        /// </summary>
        /// <param name="oldTheme"></param>
        /// <param name="newTheme"></param>
        private void GetListIDSlide(ETheme oldTheme, ETheme newTheme)
        {
            foreach (var oldT in oldTheme.SlideMasters[0].LayoutMasters)
            {
                foreach (var newT in newTheme.SlideMasters[0].LayoutMasters)
                {
                    if (oldT.LayoutType == newT.LayoutType)
                    {
                        newT.ListSlideID.Clear();
                        newT.ListSlideID = oldT.ListSlideID;
                    }
                }
            }
        }

        /// <summary>
        /// Cập lại màu sắc theo màu sắc của theme
        /// </summary>
        public virtual void UpdateThemeColor()
        {
            if (this.IsLoaded)
            {
                this.ThemeLayout?.UpdateThemeColor();
                this.MainLayout?.UpdateThemeColor();
                this.MainLayout?.UpdateThumbnail();
                foreach (LayoutBase layout in Layouts)
                {
                    layout.UpdateThemeColor();
                    layout.UpdateThumbnail();
                }
            }
        }

        #endregion

        #region SlideNumber

        /// <summary>
        /// Đánh số thứ tự của Slide
        /// </summary>
        public int SlideNumber
        {
            get { return (int)GetValue(SlideNumberProperty); }
            set { SetValue(SlideNumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SlideNumber.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SlideNumberProperty =
            DependencyProperty.Register("SlideNumber", typeof(int), typeof(SlideBase), new PropertyMetadata(1, SlideNumberPropertyCallback));

        private static void SlideNumberPropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            (Application.Current as IAppGlobal).UpdateSlideNumber(_owner);
        }


        #endregion

        #region TextFooter

        /// <summary>
        /// Text của footer
        /// </summary>
        public string TextFooter
        {
            get { return (string)GetValue(TextFooterProperty); }
            set { SetValue(TextFooterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextFooter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextFooterProperty =
            DependencyProperty.Register("TextFooter", typeof(string), typeof(SlideBase), new PropertyMetadata(null));


        #endregion

        #region UpdateBackground
        /// <summary>
        /// Cập nhật màu background
        /// </summary>
        /// <param name="background"></param>
        public void UpdateBackground(BackgroundItem background)
        {
            if ((Application.Current as IAppGlobal).SlideViewMode == SlideViewMode.Normal)
            {
                (Application.Current as IAppGlobal).UpdateThumbnailBackground(background);
            }
            (Application.Current as IAppGlobal).UpdateBackground(background, this);
        }
        #endregion

        #region IsFolowMasterBackground

        /// <summary>
        /// Slide có gán màu nền giống slideMaster hay không
        /// </summary>
        public bool IsFollowMasterBackground
        {
            get { return (bool)GetValue(IsFollowMasterBackgroundProperty); }
            set { SetValue(IsFollowMasterBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsFollowMasterBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsFollowMasterBackgroundProperty =
            DependencyProperty.Register("IsFollowMasterBackground", typeof(bool), typeof(SlideBase), new PropertyMetadata(true));


        #endregion

        #region IsRemove


        public bool IsRemove
        {
            get { return (bool)GetValue(IsRemoveProperty); }
            set { SetValue(IsRemoveProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRemove.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRemoveProperty =
            DependencyProperty.Register("IsRemove", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));


        #endregion

        #region GetMaxZIndex
        public int GetMaxZIndex()
        {
            int maxZindex = 0;
            foreach (var item in MainLayout.Elements)
            {
                if (item.ZIndex > maxZindex)
                    maxZindex = item.ZIndex;
            }
            return maxZindex;
        }
        #endregion

    }

    /// <summary>
    /// Giao diện cho các đối tượng MEdia
    /// </summary>
    public interface IMediaElement { }

    /// <summary>
    /// Giao diện cho đối tượng Video
    /// </summary>
    public interface IVideoElement : IMediaElement
    {
    }
    /// <summary>
    /// Giao diện cho đối tượng âm thanh
    /// </summary>
    public interface IAudioElement : IMediaElement
    {
    }

    /// <summary>
    /// Giao diện cho đối tượng PlacHolder
    /// </summary>
    public interface IPlaceHolder
    {
        bool IsMasterSlide { set; get; }
    }

    /// <summary>
    /// Giao diện cho đối tượng văn bản
    /// </summary>
    public interface ITextElement
    {
        void SetTextStyle(ColorBrushBase foreground, ColorBrushBase stroke, ColorBrushBase shadow, double thickness);
    }

    /// <summary>
    /// Giao diện cho đối tượng Quizz
    /// </summary>
    public interface IQuizSlide { }

    /// <summary>
    /// Giao diện cho đối tượng trang kết quar
    /// </summary>
    public interface IResultSlide { }
}

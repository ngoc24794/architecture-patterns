﻿using INV.Elearning.Core.Helper;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using INV.Elearning.Core.Model;
using System.Windows.Input;
using System.Windows.Media;
using INV.Elearning.Animations;
using System.Collections.Generic;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using INV.Elearning.Core.View.Theme;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.Model.Theme;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Khung mẫu
    /// </summary>
    public abstract class SlideBase : Grid, ITriggerableObject, IUpdatePropertyByUndo, IThemeSupport
    {
        #region Contructors
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public SlideBase()
        {
            this.MainLayout = new LayoutBase();
            this.Focusable = true;
            this.Visibility = Visibility.Hidden;
            this.Background = Brushes.White;
            TriggerData = new ObservableCollection<TriggerViewModelBase>();
            SlideName = "Slide";
            ID = ObjectElementsHelper.RandomString(10);
            Transition = new NoneTransition(TimeSpan.FromSeconds(0.75), "/INV.Elearning.Animations.UI;component/Images/Animations/EntranceAnimations/Spin.png");
        }
        #endregion

        #region Transition

        /// <summary>
        /// Hiệu ứng chuyển trang
        /// </summary>
        public Transiton Transition
        {
            get { return (Transiton)GetValue(TransitionProperty); }
            set { SetValue(TransitionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Transition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransitionProperty =
            DependencyProperty.Register("Transition", typeof(Transiton), typeof(SlideBase), new PropertyMetadata(null, TransitionCallback));

        private static void TransitionCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as SlideBase).HasTransition = e.NewValue != null && !(e.NewValue is NoneTransition);
        }

        #endregion

        #region MainLayout

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Layout chính của Silde<br/>
        /// Layout chính có nhiệm vụ chứa có đối tượng vào trang chính<br/>
        /// Cần thực hiện cài đặt giá trị lại với mỗi loại Slide tương ứng
        /// </summary>
        public LayoutBase MainLayout
        {
            get { return (LayoutBase)GetValue(MainLayoutProperty); }
            set
            {
                if (value == null) return; //Không cho cấu hình Null
                SetValue(MainLayoutProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for MainLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MainLayoutProperty =
            DependencyProperty.Register("MainLayout", typeof(LayoutBase), typeof(SlideBase), new PropertyMetadata(null, MainLayoutCallBack));

        /// <summary>
        /// Thay đổi khi MainLayout thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void MainLayoutCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is LayoutBase) //Xóa Layout cũ nếu có
            {
                (e.OldValue as LayoutBase).IsMainLayout = false;
                (d as SlideBase).Children.Remove(e.OldValue as LayoutBase);
            }

            if (e.NewValue is LayoutBase)
            {
                (e.NewValue as LayoutBase).IsMainLayout = true;
                (d as SlideBase).Children.Add(e.NewValue as LayoutBase); //Thêm Layout mới
            }
        }

        #endregion

        #region Layouts

        private ObservableCollection<LayoutBase> _layouts;
        /// <summary>
        /// Lấy danh sách các layout có trong Slide
        /// </summary>
        public ObservableCollection<LayoutBase> Layouts
        {
            get
            {
                if (_layouts == null)
                {
                    _layouts = new ObservableCollection<LayoutBase>();
                    _layouts.CollectionChanged += LayoutsChanged;
                }
                return _layouts;
            }
        }

        /// <summary>
        /// Thay đổi trong danh sách các layout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (LayoutBase layout in e.NewItems) //Thêm mới các phần tử vào trong Canvas
                    {
                        if (layout.Parent == null) //Nếu đã được chứa trong một Panel khác thì không thêm mới. Sẽ phát sinh lỗi nếu cố tình thêm mới
                        {
                            this.Children.Add(layout);
                            if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                            {
                                Global.PushUndo(new AddLayoutToSlideStep(layout, _layouts));
                            }
                        }
                        else if (layout == MainLayout)
                        {
                            Layouts.Remove(layout);
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    if (e.OldItems != null)
                        foreach (LayoutBase layout in e.OldItems) //Xóa các phần tử khỏi Canvas
                        {
                            if (layout != MainLayout) //Không xóa form chính
                            {
                                this.Children.Remove(layout);
                                if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                                {
                                    Global.PushUndo(new RemoveLayoutFromSlideStep(layout, _layouts, e.OldStartingIndex));
                                }
                            }
                        }
                    MainLayout.IsSelected = true;
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    for (int i = 0; i < this.Children.Count; i++)
                    {
                        if (this.Children[i] != MainLayout)
                        {
                            this.Children.RemoveAt(i);
                            i--;
                        }
                    }
                    MainLayout.IsSelected = true;
                    break;
            }
        }
        #endregion

        #region DocumentParent

        /// <summary>
        /// Lay hoc cai dat thuoc tinh tai lieu chua slide
        /// </summary>
        public DocumentControl DocumentParent
        {
            get { return (DocumentControl)GetValue(DocumentParentProperty); }
            set { SetValue(DocumentParentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DocumentParent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DocumentParentProperty =
            DependencyProperty.Register("DocumentParent", typeof(DocumentControl), typeof(SlideBase), new PropertyMetadata(null));


        #endregion

        #region ThemeLayout

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính Layout theo theme<br/>
        /// </summary>
        public ThemeLayout ThemeLayout
        {
            get { return (ThemeLayout)GetValue(ThemeLayoutProperty); }
            set { SetValue(ThemeLayoutProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeLayoutProperty =
            DependencyProperty.Register("ThemeLayout", typeof(ThemeLayout), typeof(SlideBase), new PropertyMetadata(null, ThemeLayoutCallBack));

        /// <summary>
        /// Sự kiện khi thêm mới một theme layout
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ThemeLayoutCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is ThemeLayout) //Xóa Layout cũ nếu có
            {
                (d as SlideBase).MainLayout.Children.Remove(e.OldValue as ThemeLayout);
            }

            if (e.NewValue is ThemeLayout themeLayout)
            {
                SetZIndex(themeLayout, -100); //Cài đặt vị trí thấp nhất
                (d as SlideBase).MainLayout.Children.Add(themeLayout); //Thêm Layout mới
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// Lấy hoặc cài đặt dữ liệu
        /// </summary>
        public PageElementBase Data
        {
            get { return (PageElementBase)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(PageElementBase), typeof(SlideBase), new PropertyMetadata(null));



        #endregion

        #region ID

        /// <summary>
        /// Lấy hoặc cài đặt mã số cho đối tượng
        /// </summary>
        public string ID
        {
            get { return (string)GetValue(IDProperty); }
            set { SetValue(IDProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IDProperty =
            DependencyProperty.Register("ID", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty));


        #endregion



        #region SlideName

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính
        /// </summary>
        public string SlideName
        {
            get { return (string)GetValue(SlideNameProperty); }
            set { SetValue(SlideNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SlideName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SlideNameProperty =
            DependencyProperty.Register("SlideName", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty));

        #endregion

        #region IsSelected

        /// <summary>
        /// Lấy hoặc cài đặt giá trị lựa chọn cho đối tượng
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(SlideBase), new PropertyMetadata(false, IsSelectedCallBack));

        /// <summary>
        /// Hàm được gọi khi giá trị IsSelected thay đổi
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void IsSelectedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            if ((bool)e.NewValue)
            {
                _owner.Visibility = Visibility.Visible;
                if ((Application.Current as IAppGlobal).SelectedSlide != null && (Application.Current as IAppGlobal).SelectedSlide != _owner)
                    (Application.Current as IAppGlobal).SelectedSlide.Visibility = Visibility.Collapsed;

                if (!(Application.Current as IAppGlobal).SelectedSlides.Contains(_owner))
                    (Application.Current as IAppGlobal).SelectedSlides.Add(_owner);
                (Application.Current as IAppGlobal).SelectedSlide = _owner;

                if (!_owner.IsContentLoaded) //Nếu chưa tải nội dung lên trang
                {
                    _owner.LoadData(_owner.Data);
                }
            }
            else
            {
                _owner.Visibility = Visibility.Collapsed;
                ObjectElementsHelper.UnSelectedAll(); //Huy tat ca lau chon doi tuong
                if ((Application.Current as IAppGlobal).SelectedSlides.Contains(_owner))
                    (Application.Current as IAppGlobal).SelectedSlides.Remove(_owner);

                if ((Application.Current as IAppGlobal).SelectedSlides.Count > 0)
                {
                    var _lastSeletedSlide = (Application.Current as IAppGlobal).SelectedSlides.LastOrDefault();
                    _lastSeletedSlide.Visibility = Visibility.Visible;
                    (Application.Current as IAppGlobal).SelectedSlide = _lastSeletedSlide;
                }
                else
                {
                    if (Keyboard.Modifiers == ModifierKeys.Control)
                        _owner.IsSelected = true;
                }
            }
        }
        #endregion

        #region IsContentLoaded

        /// <summary>
        /// Thuộc tính cài đặt nội dung đã được load
        /// </summary>
        public bool IsContentLoaded
        {
            get { return (bool)GetValue(IsContentLoadedProperty); }
            private set { SetValue(IsContentLoadedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsContentLoaded.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsContentLoadedProperty =
            DependencyProperty.Register("IsContentLoaded", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));


        #endregion

        #region LayoutMaster

        /// <summary>
        /// Layout Master dang duoc lua chon
        /// </summary>
        public ELayoutMaster LayoutMaster
        {
            get { return (ELayoutMaster)GetValue(LayoutMasterProperty); }
            set { SetValue(LayoutMasterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutMaster.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutMasterProperty =
            DependencyProperty.Register("LayoutMaster", typeof(ELayoutMaster), typeof(SlideBase), new PropertyMetadata(null, LayoutMasterCallback));

        private static void LayoutMasterCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SlideBase _owner = d as SlideBase;
            _owner.ThemeLayout.UpdateUIB(_owner.LayoutMaster.MainLayer);
        }


        #endregion

        #region SelectedLayout
        /// <summary>
        /// Sự kiện layer hiện hành bị thay đổi
        /// </summary>
        public event SelectedItemChangedEventHanlder SelectedLayoutChanged;

        /// <summary>
        /// Lấy Layout đang được lựa chọn
        /// </summary>
        public LayoutBase SelectedLayout
        {
            get
            {
                var _result = (LayoutBase)GetValue(SelectedLayoutProperty);
                if (_result == null)
                {
                    if (MainLayout != null)
                    {
                        _result = MainLayout;
                    }
                    else
                        _result = this.Layouts.FirstOrDefault();
                }
                if (_result?.IsSelected == false)
                {
                    _result.IsSelected = true;
                }
                return _result;
            }
            internal set
            {
                if (value == null || value != MainLayout && !Layouts.Contains(value))
                    return;
                //SelectedLayoutChanged?.Invoke(this, new SelectedItemEventArgs(value, (LayoutBase)GetValue(SelectedLayoutProperty)));
                SetValue(SelectedLayoutProperty, value);
                SelectedLayoutChanged?.Invoke(this, new SelectedItemEventArgs(value));
            }
        }

        // Using a DependencyProperty as the backing store for SelectedLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedLayoutProperty =
            DependencyProperty.Register("SelectedLayout", typeof(LayoutBase), typeof(SlideBase), new PropertyMetadata(null));

        #endregion

        #region TriggerData
        public ObservableCollection<TriggerViewModelBase> TriggerData { get; set; }
        #endregion

        #region HasTransition

        /// <summary>
        /// Tồn tại hiệu ứng chuyển trang
        /// </summary>
        public bool HasTransition
        {
            get { return (bool)GetValue(HasTransitionProperty); }
            private set { SetValue(HasTransitionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasTransition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasTransitionProperty =
            DependencyProperty.Register("HasTransition", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));

        #endregion

        #region HasVideo
        /// <summary>
        /// Tồn tại video
        /// </summary>
        public bool HasVideo
        {
            get { return (bool)GetValue(HasVideoProperty); }
            internal set { SetValue(HasVideoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasVideo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasVideoProperty =
            DependencyProperty.Register("HasVideo", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));

        #endregion

        #region HasAudio
        /// <summary>
        /// Tồn tại video
        /// </summary>
        public bool HasAudio
        {
            get { return (bool)GetValue(HasAudioProperty); }
            internal set { SetValue(HasAudioProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasAudio.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasAudioProperty =
            DependencyProperty.Register("HasAudio", typeof(bool), typeof(SlideBase), new PropertyMetadata(false));

        #endregion

        #region Timing
        /// <summary>
        /// Thời gian diễn ra trang
        /// </summary>
        public Timing Timing
        {
            get
            {
                return this.MainLayout?.Timing;
            }
        }
        #endregion

        #region CanShowInMenu

        /// <summary>
        /// Có thể hiện thị trên thanh menu
        /// </summary>
        public bool CanShowInMenu
        {
            get { return (bool)GetValue(CanShowInMenuProperty); }
            set { SetValue(CanShowInMenuProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanShowInMenu.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanShowInMenuProperty =
            DependencyProperty.Register("CanShowInMenu", typeof(bool), typeof(SlideBase), new PropertyMetadata(true));


        #endregion

        #region SlideConfig

        /// <summary>
        /// Cài đặt thuộc tính của Slide
        /// </summary>
        public PageConfig PageConfig
        {
            get { return (PageConfig)GetValue(PageConfigProperty); }
            set { SetValue(PageConfigProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PageConfig.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PageConfigProperty =
            DependencyProperty.Register("PageConfig", typeof(PageConfig), typeof(SlideBase), new PropertyMetadata(null));


        #endregion

        #region Methods

        /// <summary>
        /// Lưu trữ đối tượng ra dạng hình ảnh
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="size">Kích thước xuất</param>
        /// <returns></returns>
        public bool SaveToImage(string imagePath, Size size = new Size())
        {
            return FileHelper.SaveToImage(imagePath, this, size);
        }

        /// <summary>
        /// Tải lại các giao diện từ dữ liệu
        /// </summary>
        /// <param name="data"></param>
        public virtual void LoadData(PageElementBase data)
        {
            this.IsContentLoaded = true;

            this.Data = data;
            this.MainLayout.Elements.Clear(); //Xóa tất cả phần tử trong Layout chính
            this.SlideName = data.Name;
            this.ID = data.ID;
            if (Global.IsPasting) //Nếu đang dán thì tạo mới id
                this.ID = Helper.ObjectElementsHelper.RandomString(13);
            this.CanShowInMenu = data.CanShowInMenu;
            this.Transition = data.Transition;
            this.PageConfig = data.PageConfig;
            Dictionary<Type, IModule> _dictionary = new Dictionary<Type, IModule>();
            if (data.MainLayer != null)
            {
                var _mainLayout = LayoutHelper.LoadDataForSlideLayout(data.MainLayer) as LayoutBase;
                if (_mainLayout != null)
                {
                    MainLayout = _mainLayout;
                }
            }

            this.Layouts.Clear();
            foreach (var layout in data.PageLayers)
            {
                var _layout = LayoutHelper.LoadDataForSlideLayout(layout) as LayoutBase;
                if (_layout != null)
                {
                    this.Layouts.Add(_layout);
                }
            }
        }

        /// <summary>
        /// Tải nhanh dữ liệu lúc mở bài giảng
        /// </summary>
        /// <param name="data"></param>
        public virtual void QuickUpdateUI(PageElementBase data)
        {
            this.Data = data;
        }

        /// <summary>
        /// Ghi đè lại hàm cập nhật dữ liệu
        /// </summary>
        public virtual void RefreshData()
        {
            if (this.Data == null)
            {
                this.Data = new NormalPage();
            }

            this.Data.CanShowInMenu = this.CanShowInMenu;
            this.Data.ID = this.ID;
            this.Data.Name = this.SlideName;
            this.MainLayout?.RefreshData();
            this.Data.MainLayer = this.MainLayout.Data;
            this.Data.Transition = Transition;
            this.Data.PageConfig = this.PageConfig;

            this.Data.PageLayers.Clear();
            foreach (var layout in this.Layouts)
            {
                layout.RefreshData();
                this.Data.PageLayers.Add(layout.Data);
            }
        }


        /// <summary>
        /// Cập nhật dữ liệu từ Undo
        /// </summary>
        public virtual void OnPropertyChangeByUndo()
        {

        }
        #endregion

        #region Overrides
        /// <summary>
        /// Sự kiện thả chuột trên khung slide
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            (Application.Current as IAppGlobal)?.SelectedSlide?.MainLayout?.UpdateDescendantBounds();
            (Application.Current as IAppGlobal)?.SelectedSlide?.MainLayout?.UpdateRectBounds();
        }

        #endregion

        #region SelectedLayoutMaster


        public LayoutMaster SelectedLayoutMaster
        {
            get { return (LayoutMaster)GetValue(SelectedLayoutMasterProperty); }
            set { SetValue(SelectedLayoutMasterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedLayoutMaster.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedLayoutMasterProperty =
            DependencyProperty.Register("SelectedLayoutMaster", typeof(LayoutMaster), typeof(SlideBase), new PropertyMetadata(null, new PropertyChangedCallback(SelectedLayoutMasterPropertyChanged)));

        private static void SelectedLayoutMasterPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// Cập nhật lại phông chữ theo theme
        /// </summary>
        public virtual void UpdateThemeFont()
        {
            this.MainLayout?.UpdateThemeFont();
            foreach (IThemeSupport layout in Layouts)
            {
                layout.UpdateThemeFont();
            }
        }

        /// <summary>
        /// Cập nhật lại laout
        /// </summary>
        /// <param name="eLayoutMaster"></param>
        public virtual void UpdateLayoutMaster()
        {
            if (this.ThemeLayout == null) this.ThemeLayout = new ThemeLayout();
            if (this.DocumentParent?.SelectedTheme?.SlideMasters.Count > 0)
                this.ThemeLayout.UpdateUIB(DocumentParent.SelectedTheme.SlideMasters[0].MainLayer);
            else
            {
                this.ThemeLayout.Elements.Clear();
            }
            UpdateThemeFont();
            UpdateThemeColor();
        }

        /// <summary>
        /// Cập lại màu sắc theo màu sắc của theme
        /// </summary>
        public virtual void UpdateThemeColor()
        {
            this.ThemeLayout?.UpdateThemeColor();
            this.MainLayout?.UpdateThemeColor();
            this.MainLayout?.UpdateThumbnail();
            foreach (IThemeSupport layout in Layouts)
            {
                layout.UpdateThemeColor();
            }
        }

        #endregion
    }

    /// <summary>
    /// Giao diện cho các đối tượng MEdia
    /// </summary>
    public interface IMediaElement { }

    /// <summary>
    /// Giao diện cho đối tượng Video
    /// </summary>
    public interface IVideoElement : IMediaElement
    {
    }
    /// <summary>
    /// Giao diện cho đối tượng âm thanh
    /// </summary>
    public interface IAudioElement : IMediaElement
    {
    }

    /// <summary>
    /// Giao diện cho đối tượng PlacHolder
    /// </summary>
    public interface IPlaceHolder { }

    /// <summary>
    /// Giao diện cho đối tượng văn bản
    /// </summary>
    public interface ITextElement
    {
        void SetTextStyle(ColorBrushBase foreground, ColorBrushBase stroke, ColorBrushBase shadow, double thickness);
    }

    /// <summary>
    /// Giao diện cho đối tượng Quizz
    /// </summary>
    public interface IQuizSlide { }

    /// <summary>
    /// Giao diện cho đối tượng trang kết quar
    /// </summary>
    public interface IResultSlide { }
}

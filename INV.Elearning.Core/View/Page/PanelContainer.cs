﻿using INV.Elearning.Core.Helper;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    public class PanelContainer : Grid
    {
        Point? selectionStartPoint = null;

        public PanelContainer()
        {
            this.Background = Brushes.Transparent;
        }

        #region Methods
        /// <summary>
        /// Xử lí sự kiện nhấn chuột lên canvas này
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if ((Application.Current as IAppGlobal).SelectedSlide == null)
            {
                if ((Application.Current as IAppGlobal).DocumentControl?.Slides?.Count > 0)
                    (Application.Current as IAppGlobal).DocumentControl.Slides.First().IsSelected = true;
            }
            // Nếu nhấn chuột trực tiếp lên Canvas này
            selectionStartPoint = new Point?(e.GetPosition(this));

            if (Keyboard.Modifiers == ModifierKeys.Control || Keyboard.Modifiers == ModifierKeys.Shift)
            {
                return;
            }

            ObjectElementsHelper.UnSelectedAll();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            // nếu nút trái chuột không được nhấn thì không vẽ vùng chọn
            if (e.LeftButton != MouseButtonState.Pressed)
                selectionStartPoint = null;

            // nếu nút trái chuột được nhấn và ta đã đặt giá trị cho selectionStartPoint
            if (selectionStartPoint.HasValue)
            {
                // vẽ vùng chọn
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    AreaSelectionAdorner adorner = new AreaSelectionAdorner(this, selectionStartPoint);
                    if (adorner != null)
                    {
                        adornerLayer.Add(adorner);
                    }
                }
            }
            e.Handled = true;
        }
        #endregion
    }
}

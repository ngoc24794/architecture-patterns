﻿using System.Windows;

namespace INV.Elearning.Core.View.Page
{
    public interface IMultiViewModel
    {
        FrameworkElement FormViewContent { set; get; }
    }
}

﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INV.Elearning.Core.View
{
    public sealed class NormalSlide : SlideBase
    {
        public NormalSlide()
        {
            this.Data = new NormalPage();
        }
    }
}

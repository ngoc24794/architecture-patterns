﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.Timeline;
using INV.Elearning.Core.View.Theme;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Core.View
{
    /// <summary>
    /// Layout chuẩn trong tài liệu
    /// </summary>
    public class LayoutBase : Canvas, ITriggerableObject, ITimingObject, IUpdatePropertyByUndo, IThemeSupportExt
    {
        #region ID
        public string ID { get; set; }
        #endregion

        #region Elements

        private ObservableCollection<ObjectElement> _elements;
        /// <summary>
        /// Danh sách các phần tử
        /// </summary>
        public ObservableCollection<ObjectElement> Elements
        {
            get
            {
                if (_elements == null)
                {
                    _elements = new ObservableCollection<ObjectElement>();
                    _elements.CollectionChanged += ElementsChanged;
                }
                return _elements;
            }
        }

        /// <summary>
        /// Thêm đối tượng Group vào canvas
        /// </summary>
        /// <param name="group"></param>
        private void AddIGroupContainer(IGroupContainer group)
        {
            foreach (ObjectElement item in group.Elements)
            {
                if (item is IGroupContainer groupSub)
                    AddIGroupContainer(groupSub);
                if (item.Parent == null)
                    this.Children.Add(item);
                item.LayoutOwner = this;
            }
        }

        /// <summary>
        /// Xóa đối tượng Group ra khỏi canvas
        /// </summary>
        /// <param name="group"></param>
        private void RemoveIGroupContainer(IGroupContainer group)
        {
            foreach (ObjectElement item in group.Elements)
            {
                if (item is IGroupContainer groupSub)
                    RemoveIGroupContainer(groupSub);
                this.Children.Remove(item);
                item.LayoutOwner = null;
            }
        }

        /// <summary>
        /// Thay đổi của danh sách các phần tử
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ElementsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (ObjectElement element in e.NewItems) //Thêm các phần tử vào Layout
                    {
                        if (element.Parent == null)
                        {
                            if (element is IGroupContainer group) //Nếu là một cấu trúc chứa các phần tử thì thực hiện thêm các phần tử con trong danh sách
                            {
                                AddIGroupContainer(group);
                            }
                            //Lấy số phần tử có cùng kiểu trong 1 slide
                            this.Children.Add(element);
                            element.LayoutOwner = this;

                            if (!Global.UndoProcessing && Global.IsPasting && Global.IsDataLoading) //Chỉ trong trường hợp thêm mới
                            {
                                if (element.ZIndex == 0 && this.Elements.Count > 0)
                                {
                                    element.ZIndex = this.Elements.Max(x => x.ZIndex) + 1;
                                }
                            }

                            if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                            {
                                element.ElementCount = GetCountElement(element.GetType());
                                Global.PushUndo(new AddItemToLayoutStep(element, _elements));
                            }
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (ObjectElement element in e.OldItems) //Xóa các phần tử khỏi Layout
                    {
                        if (!element.RuleConfiguration.CanDelete) //Nếu cấu hình không được phép xóa
                        {
                            this.Elements.Add(element);
                            return;
                        }

                        if (element is IGroupContainer group) //Nếu là một cấu trúc chứa các phần tử thì thực hiện xóa các phần tử con trong danh sách
                        {
                            RemoveIGroupContainer(group);
                        }
                        this.Children.Remove(element);
                        element.LayoutOwner = null;
                        //Thay đổi thuộc tính của SlideMaster
                        if (Parent is SlideMaster slideMaster)
                        {
                            if (element.PlaceHolderType == PlaceHolderEnum.Title)
                            {
                                slideMaster.IsTitle = false;
                            }
                            if (element.PlaceHolderType == PlaceHolderEnum.Footer)
                            {
                                slideMaster.IsFooter = false;
                            }
                            if (element.PlaceHolderType == PlaceHolderEnum.DateTime)
                            {
                                slideMaster.IsDate = false;
                            }
                            if (element.PlaceHolderType == PlaceHolderEnum.SlideNumber)
                            {
                                slideMaster.IsSlideNumber = false;
                            }
                            if (element.PlaceHolderType == PlaceHolderEnum.Body)
                            {
                                slideMaster.IsText = false;
                            }
                        }
                        //Thay đổi thuộc tính của LayoutMaster
                        if (Parent is LayoutMaster layoutMaster)
                        {
                            if (element.PlaceHolderType == PlaceHolderEnum.Title || element.PlaceHolderType == PlaceHolderEnum.CenterTitle)
                            {
                                layoutMaster.IsTitle = false;
                            }
                            if (!CheckFooter())
                            {
                                layoutMaster.IsFooter = false;
                            }
                        }
                        OnPropertyChangeByUndo();
                        if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                        {
                            Global.PushUndo(new RemoveItemToLayoutStep(element, _elements, e.OldStartingIndex));
                        }
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    while (this.Elements.Count > 0)
                    {
                        this.Elements.RemoveAt(0);
                    }
                    break;
            }
            if (this.Parent is SlideBase slideBase && this == slideBase.MainLayout)
            {
                slideBase.HasVideo = this.Elements.FirstOrDefault(y => y is IVideoElement) != null;
                slideBase.HasAudio = this.Elements.FirstOrDefault(y => y is IAudioElement) != null;
            }

            if (Elements.Count == 0)
            {
                OnPropertyChangeByUndo();
            }

            UpdateThumbnail();
        }

        /// <summary>
        /// Check footer trong slide master hoặc Layout Master
        /// </summary>
        /// <returns></returns>
        private bool CheckFooter()
        {
            foreach (var element in Elements)
            {
                if (element.PlaceHolderType == PlaceHolderEnum.SlideNumber || element.PlaceHolderType == PlaceHolderEnum.Footer || element.PlaceHolderType == PlaceHolderEnum.DateTime)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Đếm số lượng Object Element có cùng kiểu có trong Slide
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private int GetCountElement(Type type)
        {
            int count = 0;
            foreach (var item in Elements)
            {
                if (item is ObjectElement objectElement && objectElement.GetType() == type)
                {
                    count = Math.Max(count, objectElement.ElementCount);
                }
            }
            return count + 1;
        }
        #endregion

        #region LayoutRule

        /// <summary>
        /// Quy định của Layout
        /// </summary>
        public LayoutRule LayoutRule
        {
            get { return (LayoutRule)GetValue(LayoutRuleProperty); }
            private set { SetValue(LayoutRuleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutRule.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutRuleProperty =
            DependencyProperty.Register("LayoutRule", typeof(LayoutRule), typeof(LayoutBase), new PropertyMetadata(new LayoutRule()));


        #endregion

        #region Data

        /// <summary>
        /// Lấy thuộc tính lưu trữ dữ liệu <br/>
        /// Chú ý: gọi phương thức <see cref="RefreshData"/> trước khi lấy dữ liệu
        /// </summary>
        public PageLayer Data
        {
            get { return (PageLayer)GetValue(DataProperty); }
            protected set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(PageLayer), typeof(LayoutBase), new PropertyMetadata(null));


        #endregion

        #region LayoutConfig

        /// <summary>
        /// Cấu hình của layout
        /// </summary>
        public PageLayerConfig LayoutConfig
        {
            get { return (PageLayerConfig)GetValue(LayoutConfigProperty); }
            set { SetValue(LayoutConfigProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LayoutConfig.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LayoutConfigProperty =
            DependencyProperty.Register("LayoutConfig", typeof(PageLayerConfig), typeof(LayoutBase), new PropertyMetadata(new PageLayerConfig()));


        #endregion

        #region IsLayoutVisibble

        /// <summary>
        /// Cài đặt ẩn hiện đối tượng
        /// </summary>
        public bool IsLayoutVisible
        {
            get { return (bool)GetValue(IsLayoutVisibleProperty); }
            set { SetValue(IsLayoutVisibleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsLayoutVisible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsLayoutVisibleProperty =
            DependencyProperty.Register("IsLayoutVisible", typeof(bool), typeof(LayoutBase), new PropertyMetadata(true, IsLayoutVisibileCallback));

        private static void IsLayoutVisibileCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LayoutBase _owner = d as LayoutBase;
            if (!(bool)e.NewValue)
            {
                if (_owner.IsSelected)
                    _owner.IsLayoutVisible = true;
            }
        }


        #endregion

        #region CuePoints
        private ObservableCollection<CuePointModel> _cuePoints;
        /// <summary>
        /// Danh sách điểm bổ sung trên thước của Timeline
        /// </summary>
        public ObservableCollection<CuePointModel> CuePoints
        {
            get { return _cuePoints ?? (_cuePoints = new ObservableCollection<CuePointModel>()); }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Lưu trữ đối tượng ra dạng hình ảnh
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="size">Kích thước xuất</param>
        /// <returns></returns>
        public bool SaveToImage(string imagePath, Size size = new Size())
        {
            if ((this.Parent as SlideBase)?.ThemeLayout != null)
            {
                return FileHelper.SaveToImage(imagePath, this, (this.Parent as SlideBase).ThemeLayout);
            }
            else
            {
                return FileHelper.SaveToImage(imagePath, this, size);
            }
        }

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseLeftButtonDown(e);
            if (e.ClickCount == 2)
            {
                this.UpdateThumbnail();
            }
        }


        #region Vitural Methods
        /// <summary>
        /// Cập nhật lại dữ liệu lưu trữ.
        /// Phương thức sẽ được gọi trong mỗi lần lấy giá trị Data
        /// </summary>
        public virtual void RefreshData()
        {
            if (Data == null) Data = new PageLayer();
            Data.Children.Clear();
            Data.Background = ColorHelper.ConverterFromColor(this.Fill);
            Data.ID = this.ID;
            Data.ThemeLayoutOwnerID = ThemeLayoutOwnerID;
            Data.Name = this.TargetName;
            Data.Setting = this.LayoutConfig;
            Data.Timing = this.Timing;
            Data.CanShowInMenu = this.CanShowInMenu;
            if (Data.Timing != null && CuePoints != null)
            {
                Data.Timing.CuePoints = new ObservableCollection<CuePointData>();
                foreach (CuePointModel cuePoint in CuePoints)
                {
                    Data.Timing.CuePoints.Add(cuePoint.GetData());
                }
            }
            if (TriggerData != null)
            {
                Data.TriggerData = new List<TriggerableDataObjectBase>();
                foreach (var item in TriggerData)
                {
                    if (item is TriggerViewModelBase trigger && trigger.Trigger?.GetData() is TriggerableDataObjectBase data)
                    {
                        Data.TriggerData.Add(data);
                    }
                }
            }
            foreach (var item in Elements)
            {
                item.RefreshData();
                Data.Children.Add(item.Data);
            }
        }

        /// <summary>
        /// Cập nhật lại dữ liệu cho đối tượng
        /// </summary>
        /// <param name="data"></param>
        public virtual void UpdateUI(PageLayer data)
        {
            if (data != null)
            {
                this.Fill = ColorHelper.ConverFromColorData(data.Background);

                this.ID = data.ID;
                if (Global.IsPasting) //Nếu đang dán thì tạo mới id
                    this.ID = Helper.ObjectElementsHelper.RandomString(13);
                this.ThemeLayoutOwnerID = ThemeLayoutOwnerID;
                this.LayoutConfig = data.Setting;
                this.CanShowInMenu = data.CanShowInMenu;
                this.Timing = data.Timing;
                this.TargetName = data.Name;
                this.Elements.Clear();
                foreach (var item in data.Children)
                {
                    var _element = ObjectElementsHelper.LoadData(item);
                    if (_element != null)
                        this.Elements.Add(_element);
                }
                CuePoints.Clear();
                if (data.Timing?.CuePoints != null)
                    foreach (CuePointData item in data.Timing.CuePoints)
                    {
                        CuePoints.Add(item.GetModel() as CuePointModel);
                    }
                if (data.TriggerData != null)
                {
                    foreach (var item in data.TriggerData)
                    {
                        if (item is TriggerableDataObjectBase triggerData && triggerData.GetModel()?.GenerateViewModel() is TriggerViewModelBase trigger)
                        {
                            TriggerData?.Add(trigger);
                        }
                    }
                }
                data = null;
            }
        }

        /// <summary>
        /// Hàm tải nhanh
        /// </summary>
        /// <param name="layer"></param>
        internal void QuickUpdateUI(PageLayer layer)
        {
            this.Data = layer;
        }

        #endregion
        #endregion

        #region Constructor
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public LayoutBase()
        {
            this.TargetName = Application.Current.TryFindResource("App_LayoutBaseName")?.ToString();
            ID = ObjectElementsHelper.RandomString(11);
            Loaded += LayoutBase_Loaded;
            Timing = new Timing()
            {
                StartTime = 0,
                Duration = 5,
                TotalTime = 5,
                ScaleRuler = 15
            };
            RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.NearestNeighbor);
        }

        /// <summary>
        /// Tải xong dữ liệu lên trang
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.UpdateThumbnail();
        }
        #endregion

        #region Fill

        /// <summary>
        /// Giá trị màu nền theo kiểu dữ liệu lưu trữ
        /// </summary>
        public ColorBrushBase Fill
        {
            get
            {
                return (ColorBrushBase)GetValue(FillProperty);
            }
            set { SetValue(FillProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Background.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(ColorBrushBase), typeof(LayoutBase), new PropertyMetadata(new ColorSolidBrush() { Color = Colors.Transparent, ColorSpecialName = "Background Light 1" }, PropertyCallBack));

        #endregion

        #region IsSelected

        /// <summary>
        /// Lấy hoặc cài đặt giá trị được lựa chọn cho đối tượng layout
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(LayoutBase), new PropertyMetadata(false, IsSelectedCallBack));

        private bool? _preIsLayoutVisible = null;
        private static void IsSelectedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LayoutBase _owner = d as LayoutBase;
            if ((bool)e.NewValue)
            {
                if (_owner.Parent is SlideBase slide)
                {
                    slide.SelectedLayout = _owner;
                    var _maxZIndex = slide.Layouts.Count > 0 ? slide.Layouts.Max(x => Panel.GetZIndex(x)) : 0;
                    SetZIndex(_owner, _maxZIndex + 1);
                }
                _owner.IsHitTestVisible = true;
                _owner._preIsLayoutVisible = _owner.IsLayoutVisible;
                _owner.IsLayoutVisible = true;
            }
            else
            {
                foreach (var item in _owner.Elements)
                {
                    item.IsSelected = false;
                }
                _owner.IsHitTestVisible = false;
                if (_owner._preIsLayoutVisible != null)
                    _owner.IsLayoutVisible = (bool)_owner._preIsLayoutVisible;
            }
        }

        #endregion

        #region IsMainLayout

        /// <summary>
        /// La layout chinh
        /// </summary>
        public bool IsMainLayout
        {
            get { return (bool)GetValue(IsMainLayoutProperty); }
            internal set { SetValue(IsMainLayoutProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMainLayout.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMainLayoutProperty =
            DependencyProperty.Register("IsMainLayout", typeof(bool), typeof(LayoutBase), new PropertyMetadata(false));


        #endregion
        #region ThemeLayoutOwnerID


        public string ThemeLayoutOwnerID
        {
            get { return (string)GetValue(ThemeLayoutOwnerIDProperty); }
            set { SetValue(ThemeLayoutOwnerIDProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeLayoutOwnerID.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeLayoutOwnerIDProperty =
            DependencyProperty.Register("ThemeLayoutOwnerID", typeof(string), typeof(LayoutBase), new PropertyMetadata(string.Empty));


        #endregion
        #region DescendantBounds

        /// <summary>
        /// Đường bao hiện tại của trang tài liệu
        /// </summary>
        public Rect DescendantBounds
        {
            get { return (Rect)GetValue(DescendantBoundsProperty); }
            set { SetValue(DescendantBoundsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BiggerMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DescendantBoundsProperty =
            DependencyProperty.Register("DescendantBounds", typeof(Rect), typeof(LayoutBase), new PropertyMetadata(new Rect()));

        /// <summary>
        /// Cập nhật khung ảnh
        /// </summary>
        public void UpdateDescendantBounds()
        {
            DescendantBounds = VisualTreeHelper.GetDescendantBounds(this);
        }

        /// <summary>
        /// Lấy danh sách các đối tượng
        /// </summary>
        /// <param name="elements"></param>
        /// <returns></returns>
        List<ObjectElement> GetElements(ObservableCollection<ObjectElement> elements)
        {
            var _result = new List<ObjectElement>();

            foreach (var item in elements)
            {
                if (item is GroupContainer)
                {
                    _result.AddRange(GetElements((item as GroupContainer).Elements));
                }
                else
                {
                    _result.Add(item);
                }
            }

            return _result;
        }

        #endregion

        #region IsShowInMenu

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính hiển thị trên Menu
        /// </summary>
        public bool CanShowInMenu
        {
            get { return (bool)GetValue(CanShowInMenuProperty); }
            set { SetValue(CanShowInMenuProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanShowInMenu.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanShowInMenuProperty =
            DependencyProperty.Register("CanShowInMenu", typeof(bool), typeof(LayoutBase), new PropertyMetadata(true));


        #endregion

        #region DescendantBounds

        /// <summary>
        /// Đường bao hiện tại của trang tài liệu
        /// </summary>
        public Rect RectBounds
        {
            get { return (Rect)GetValue(RectBoundsProperty); }
            set { SetValue(RectBoundsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BiggerMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RectBoundsProperty =
            DependencyProperty.Register("RectBounds", typeof(Rect), typeof(LayoutBase), new PropertyMetadata(new Rect()));

        /// <summary>
        /// Cập nhật khung ảnh
        /// </summary>
        public void UpdateRectBounds()
        {
            RectBounds = this.DescendantBounds;
        }

        #endregion

        #region ThumbnailBitmap

        /// <summary>
        /// Bitmap chứa thông tin ảnh thumbnail của layout
        /// </summary>
        public ImageSource ThumbnailBitmap
        {
            get { return (ImageSource)GetValue(ThumbnailBitmapProperty); }
            set { SetValue(ThumbnailBitmapProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThumbnailBitmapProperty =
            DependencyProperty.Register("ThumbnailBitmap", typeof(ImageSource), typeof(LayoutBase), new PropertyMetadata(null));
        /// <summary>
        /// Cập nhật bitmapThumbnail
        /// </summary>
        public virtual void UpdateThumbnail()
        {
            //if (!this.IsLoaded || Global.IsDataLoading)
            //    return;

            //if (DateTime.Now.Ticks - UpdateTimerTick > 2000000) //Thời gian giữ các lần lấy dữ liệu phải lớn hơn 100 mili giây
            //{
            //    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            //    {
            //        if ((this.Parent as SlideBase)?.ThemeLayout != null)
            //        {
            //            this.ThumbnailBitmap = FileHelper.SaveToBitmap(this, (this.Parent as SlideBase).ThemeLayout, new Size(150, 67));
            //        }
            //        UpdateTimerTick = DateTime.Now.Ticks;
            //    }));
            //}
            //UpdateTimerTick = DateTime.Now.Ticks;

            if (!this.IsLoaded || Global.IsDataLoading)
                return;
            if ((Application.Current as IAppGlobal)?.CurrentDocument != null)
            {
                if (DateTime.Now.Ticks - UpdateTimerTick > 2000000) //Thời gian giữ các lần lấy dữ liệu phải lớn hơn 100 mili giây
                {
                    (Application.Current as IAppGlobal).CurrentDocument.AddRequest(this);
                }
                UpdateTimerTick = DateTime.Now.Ticks;
            }
        }

        /// <summary>
        /// Cập nhật ảnh lại
        /// </summary>
        public void UpdateThumbnail2()
        {
            if ((this.Parent as SlideBase)?.ThemeLayout != null)
            {
                this.ThumbnailBitmap = FileHelper.SaveToBitmap(this, (this.Parent as SlideBase).ThemeLayout, new Size(150, 67));
            }
            else
            {
                this.ThumbnailBitmap = FileHelper.SaveToBitmap(this, new Size(150, 67));
            }
        }

        /// <summary>
        /// Lấy thông tin đường bao
        /// </summary>
        /// <param name="panel"></param>
        /// <returns></returns>
        public Rect GetBound(Panel panel)
        {
            return VisualTreeHelper.GetDescendantBounds(this);
        }
        #endregion

        #region TriggerData
        public ObservableCollection<TriggerViewModelBase> TriggerData { get => _triggerData ?? (_triggerData = new ObservableCollection<TriggerViewModelBase>()); set => _triggerData = value; }
        #endregion

        #region Locked

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính khóa đối tượng
        /// </summary>
        public bool Locked
        {
            get { return (bool)GetValue(LockedProperty); }
            set { SetValue(LockedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Locked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LockedProperty =
            DependencyProperty.Register("Locked", typeof(bool), typeof(LayoutBase), new PropertyMetadata(false, LockCallback));

        /// <summary>
        /// Gọi lại sự kiện khóa đối tượng
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void LockCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as LayoutBase;
            if (_source.Locked)
            {
                _source.IsSelected = false;
                _source.IsHitTestVisible = false;
            }
            else
            {
                _source.IsHitTestVisible = true;
            }
        }

        #endregion

        #region Icon

        public string Icon
        {
            get { return (string)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty));

        #endregion

        #region TargetName

        public string TargetName
        {
            get { return (string)GetValue(TargetNameProperty); }
            set { SetValue(TargetNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TargetName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetNameProperty =
            DependencyProperty.Register("TargetName", typeof(string), typeof(SlideBase), new PropertyMetadata(string.Empty, PropertyCallBack));

        /// <summary>
        /// Thay đổi thuộc tính
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void PropertyCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == FillProperty)
            {
                if ((d as LayoutBase).Fill is ColorBrushBase _colorBase)
                {
                    (d as LayoutBase).Background = _colorBase.Brush;
                }
                else
                {
                    (d as LayoutBase).Background = Brushes.Transparent;
                }
            }

            if (Global.CanPushUndo && (d as LayoutBase).IsLoaded) //Ghi nhận giá trị Undo
            {
                Global.PushUndo(new DependencyPropertyStep(d, e.Property, e.NewValue, e.OldValue));
            }
            if ((d as LayoutBase).Parent is SlideMaster slideMaster && e.Property == FillProperty)
            {
                Global.BeginInit();
                (Application.Current as IAppGlobal).SelectedTheme.SlideMasters[0].MainLayer.Background = ColorHelper.ConverterFromColor(e.NewValue as ColorBrushBase);
                slideMaster.MainLayout.UpdateThumbnail();
                foreach (LayoutMaster layoutMaster in slideMaster.LayoutMasters)
                {
                    if (layoutMaster.IsFollowMasterBackground)
                        layoutMaster.MainLayout.Fill = e.NewValue as ColorBrushBase;
                    layoutMaster.MainLayout.UpdateThumbnail();
                }
                Global.EndInit();
            }
        }


        #endregion

        #region Timing

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính thời gian trình chiếu cho đối tượng
        /// </summary>
        public Timing Timing
        {
            get { return (Timing)GetValue(TimingProperty); }
            set { SetValue(TimingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Timing.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimingProperty =
            DependencyProperty.Register("Timing", typeof(Timing), typeof(SlideBase), new PropertyMetadata(null, TimingCallback));

        /// <summary>
        /// Gọi lại
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void TimingCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var _source = d as ObjectElement;
            if (e.NewValue != null && _source.RuleConfiguration.LockTiming)
            {
                _source.Timing = null;
            }
        }

        private long UpdateTimerTick = 0;
        private ObservableCollection<TriggerViewModelBase> _triggerData;

        /// <summary>
        /// Cập nhật dữ liệu từ Undo
        /// </summary>
        public virtual void OnPropertyChangeByUndo()
        {
            UpdateThumbnail();
            if (this.Parent is SlideBase slide)
            {
                slide.OnPropertyChangeByUndo();
            }
        }

        /// <summary>
        /// Cập nhật lại phông chữ theo theme
        /// </summary>
        public virtual void UpdateThemeFont()
        {
            if (this.IsLoaded)
                foreach (IThemeSupport element in Elements)
                {
                    element.UpdateThemeFont();
                }
        }

        /// <summary>
        /// Cập nhật lại laout
        /// </summary>
        /// <param name="eLayoutMaster"></param>
        public virtual void UpdateLayout(ELayoutMaster eLayoutMaster)
        {

        }

        /// <summary>
        /// Cập lại màu sắc theo màu sắc của theme
        /// </summary>
        public virtual void UpdateThemeColor()
        {
            if (this.IsLoaded && this.Fill != null)
            {
                if (this.Fill.IsFrozen)
                {
                    var _clone = this.Fill.Clone();
                    _clone.UpdateBrushByTheme();
                    this.Fill = _clone;
                }
                else
                {
                    this.Fill.UpdateBrushByTheme();
                }
            }
            foreach (var element in Elements)
            {
                element.UpdateThemeColor();
            }
        }

        #endregion

        #region ClearElememn
        /// <summary>
        /// Xóa tất cả đối tượng có trong Layout
        /// </summary>
        public void ClearElement()
        {
            for (int i = 0; i < Elements.Count; i++)
            {
                Elements.RemoveAt(i--);
            }
        }
        #endregion

    }

    /// <summary>
    /// Quy định cho LayoutRule
    /// </summary>
    public class LayoutRule
    {
        private bool _canCopy = true;
        /// <summary>
        /// Có thể sao chép hay không
        /// </summary>
        public bool CanCopy { get => _canCopy; set => _canCopy = value; }
    }
}

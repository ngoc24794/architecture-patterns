﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.View.Theme;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.Core.ViewModel.UndoRedo.Steps;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;

namespace INV.Elearning.Core.View
{
    [ContentProperty("Slides")]
    public class DocumentControl : ContentControl, INotifyPropertyChanged
    {
        #region SelectedThemeViewData
        #endregion
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #region Fields
        internal Grid GRID_ROOT = null; //Grid chứa các slide
        /// <summary>
        /// Gia tri thoi gian buoc nhay luc luu tru
        /// </summary>
        public long LastStepSave = 0;
        #endregion

        #region FileUri

        /// <summary>
        /// Duong dan luu bai giang
        /// </summary>
        public string FileUri
        {
            get { return (string)GetValue(FileUriProperty); }
            set { SetValue(FileUriProperty, value); RaisePropertyChanged("Title"); }
        }

        // Using a DependencyProperty as the backing store for FileUri.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FileUriProperty =
            DependencyProperty.Register("FileUri", typeof(string), typeof(DocumentControl), new PropertyMetadata(string.Empty));

        /// <summary>
        /// Kiem tra co the luu tru
        /// </summary>
        public bool CanSave
        {
            get => LastStepSave == 0 || (Global.GetLastStepTimeID() != -1 && LastStepSave != Global.GetLastStepTimeID());
            set { RaisePropertyChanged("CanSave"); }
        }

        /// <summary>
        /// Bat dau luu tru
        /// </summary>
        public void StartSave()
        {
            LastStepSave = Global.GetLastStepTimeID();
            RaisePropertyChanged("CanSave");
        }
        #endregion

        #region Title

        /// <summary>
        /// Lấy hoặc cài đặt nội dung tiêu đề
        /// </summary>
        public string Title
        {
            get
            {
                string _title = System.IO.Path.GetFileNameWithoutExtension(this.FileUri);
                if (string.IsNullOrEmpty(_title))
                {
                    return Application.Current.TryFindResource("Main_DefaultDocumentName")?.ToString();
                }
                return _title;
            }
        }


        #endregion

        #region Data

        /// <summary>
        /// Dữ liệu lưu trữ của đối tượng
        /// </summary>
        public ElearningDocument Data
        {
            get { return (ElearningDocument)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(ElearningDocument), typeof(DocumentControl), new PropertyMetadata(new ElearningDocument()));


        #endregion

        #region Contructors

        /// <summary>
        /// Khởi tạo mặc định
        /// </summary>
        public DocumentControl()
        {
            Style = TryFindResource("DocumentStyle") as Style;
            if (Style == null && Application.Current != null)
            {
                var _resource = new ResourceDictionary();
                _resource.Source = new Uri("pack://application:,,,/INV.Elearning.Core;Component/Resources/Generic.xaml");
                Application.Current.Resources.MergedDictionaries.Add(_resource);
                this.Style = TryFindResource("Style") as Style;
            }
            Adorner = new SmartLineAdorner(this);
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                if (AdornerLayer.GetAdornerLayer(this) is AdornerLayer adornerLayer)
                {
                    if (Adorner != null && adornerLayer.GetAdorners(this) == null)
                    {
                        adornerLayer.Add(Adorner);
                    }
                }
            }));
        }
        #endregion

        #region Slides

        private ObservableCollection<SlideBase> _slides;
        /// <summary>
        /// Danh sách các đối tượng Slide thuộc tài liệu
        /// </summary>
        public ObservableCollection<SlideBase> Slides
        {
            get
            {
                if (_slides == null)
                {
                    _slides = new ObservableCollection<SlideBase>();
                    _slides.CollectionChanged += SlidesCollectionChanged;
                }
                return _slides;
            }
        }

        /// <summary>
        /// Thay đổi danh sách
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SlidesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (GRID_ROOT != null)
            {
                switch (e.Action)
                {
                    case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                        foreach (SlideBase slide in e.NewItems)
                        {
                            if (slide.Parent == null)
                            {
                                GRID_ROOT.Children.Add(slide);
                                foreach (SlideBase item in Slides)
                                {
                                    item.SlideNumber = Slides.IndexOf(item) + 1;
                                    // (Application.Current as IAppGlobal).UpdateSlideNumber(item);
                                }
                                slide.DocumentParent = this;
                                if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                                {
                                    Global.PushUndo(new AddSlideToDocumentStep(slide, _slides));
                                }
                            }
                        }
                        break;
                    case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                        foreach (SlideBase slide in e.OldItems)
                        {
                            slide.IsSelected = false;
                            GRID_ROOT.Children.Remove(slide);
                            foreach (SlideBase item in Slides)
                            {
                                item.SlideNumber = Slides.IndexOf(item) + 1;
                                //(Application.Current as IAppGlobal).UpdateSlideNumber(item);
                            }
                            slide.DocumentParent = null;
                            RemoveSlideIdToListSlideIdTheme(slide.ID);
                            if (Global.CanPushUndo && this.IsLoaded) //Ghi nhận Undo
                            {
                                Global.PushUndo(new RemoveSlideFromDocumentStep(slide, _slides, e.OldStartingIndex));
                            }
                        }
                        break;
                    case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                        GRID_ROOT.Children.Clear();
                        (Application.Current as IAppGlobal).SelectedSlides.Clear();
                        (Application.Current as IAppGlobal).SelectedSlide = null;
                        break;
                }
            }
        }

        /// <summary>
        /// Xóa bỏ id của Slide đang được xóa ra khỏi list Slide ID
        /// </summary>
        /// <param name="id"></param>
        private void RemoveSlideIdToListSlideIdTheme(string id)
        {
            foreach (var item in (Application.Current as IAppGlobal).LocalThemesCollection)
            {
                foreach (ELayoutMaster elayout in item.SlideMasters[0].LayoutMasters)
                {
                    if (elayout.ListSlideID.Contains(id))
                    {
                        elayout.ListSlideID.Remove(id);
                    }
                }
            }
        }
        #endregion

        #region SelectedTheme

        /// <summary>
        /// Theme dang duoc lua chon
        /// </summary>
        public ETheme SelectedTheme
        {
            get { return (ETheme)GetValue(SelectedThemeProperty); }
            set { SetValue(SelectedThemeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedTheme.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedThemeProperty =
            DependencyProperty.Register("SelectedTheme", typeof(ETheme), typeof(DocumentControl), new PropertyMetadata(null, SelectedThemeCallBack));

        private static void SelectedThemeCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DocumentControl _owner = d as DocumentControl;
            if (e.NewValue != null)
            {
                GetListIDSlide((Application.Current as IAppGlobal).SelectedTheme, e.NewValue as ETheme);
                (Application.Current as IAppGlobal).SelectedTheme = _owner.SelectedTheme;
            }

            if (e.NewValue != null && (Application.Current as IAppGlobal).SlideViewMode == SlideViewMode.Normal)
            {
                if (Global.CanPushUndo && _owner.IsLoaded)
                {
                    UndoRedoByProperty step = new UndoRedoByProperty();
                    step.PropertyName = "SelectedTheme";
                    step.Source = _owner;
                    step.NewValue = e.NewValue;
                    step.OldValue = e.OldValue;
                    Global.PushUndo(step);
                }
                Global.BeginInit();
               // Global.StartGrouping("selectTheme");
                if ((e.NewValue as ETheme).IsClone || !Global.CanPushUndo)
                {
                    //var index = (Application.Current as IAppGlobal).LocalThemesCollection.IndexOf(e.OldValue as ETheme);
                    var index = (e.NewValue as ETheme).IndexTheme;
                    if (index != -1)
                    {
                        if (index < (Application.Current as IAppGlobal).LocalThemesCollection.Count)
                            (Application.Current as IAppGlobal).LocalThemesCollection.RemoveAt(index);
                        (e.NewValue as ETheme).IsClone = false;
                        (Application.Current as IAppGlobal).LocalThemesCollection.Insert(index, e.NewValue as ETheme);
                    }
                }

                foreach (SlideBase slide in _owner.Slides)
                {
                    //if (slide.ThemeLayout == null)
                    //    slide.ThemeLayout = new ThemeLayout();
                    //slide.ThemeLayout.ThemeID = (e.NewValue as ETheme).ID;
                    //slide.UpdateLayoutTheme(e.NewValue as ETheme, e.OldValue as ETheme);

                    if (slide.ThemeLayout == null)
                    {
                        ThemeLayout themeLayout = _owner.ThemeLayouts.Find(x => x.ThemeID == (e.NewValue as ETheme).ID);
                        if(themeLayout != null)
                        {
                            slide.ThemeLayout = themeLayout;
                        }
                    }
                    if (slide.ThemeLayout == null)
                    {
                        slide.ThemeLayout = new ThemeLayout();
                        slide.ThemeLayout.ThemeID = (e.NewValue as ETheme).ID;
                        _owner.ThemeLayouts.Add(slide.ThemeLayout);
                    }
                    slide.UpdateLayoutTheme(e.NewValue as ETheme, e.OldValue as ETheme);

                }

                (Application.Current as IAppGlobal).SetIndexTheme((e.NewValue as ETheme).IndexTheme);

                //if (Global.CanPushUndo && _owner.IsLoaded)
                //{
                //    UndoRedoByProperty step = new UndoRedoByProperty();
                //    step.PropertyName = "SelectedTheme";
                //    step.Source = _owner;
                //    step.NewValue = e.NewValue;
                //    step.OldValue = e.OldValue;
                //    Global.PushUndo(step);
                //}
                //Global.StopGrouping("selectTheme");
                Global.EndInit();
            }
        }

        /// <summary>
        /// Danh sách ThemeLayout
        /// </summary>
        public List<ThemeLayout> ThemeLayouts = new List<ThemeLayout>();

        /// <summary>
        /// Lấy ListSlideID
        /// </summary>
        /// <param name="oldTheme"></param>
        /// <param name="newTheme"></param>
        private static void GetListIDSlide(ETheme oldTheme, ETheme newTheme)
        {
            if (oldTheme?.SlideMasters?.Count > 0 && newTheme?.SlideMasters?.Count > 0)
            {
                foreach (var oldT in oldTheme.SlideMasters[0].LayoutMasters)
                {
                    foreach (var newT in newTheme.SlideMasters[0].LayoutMasters)
                    {
                        if (!(oldT.LayoutType == LayoutType.Custom && oldT.LayoutType == LayoutType.None))
                        {
                            if (oldT.LayoutType == newT.LayoutType)
                            {
                                newT.ListSlideID = oldT.ListSlideID;
                            }
                        }
                    }
                }

                foreach (var oldLayout in oldTheme.SlideMasters[0].LayoutMasters)
                {
                    if (oldLayout.LayoutType == LayoutType.Custom || oldLayout.LayoutType == LayoutType.None)
                    {
                        ELayoutMaster eLayoutMaster = CheckLayoutBlank(newTheme.SlideMasters[0]);
                        if (eLayoutMaster != null)
                        {
                            foreach (var item in oldLayout.ListSlideID)
                            {
                                eLayoutMaster.ListSlideID.Add(item);
                            }
                        }
                        else
                        {
                            foreach (var item in oldLayout.ListSlideID)
                            {
                                //newTheme.SlideMasters[0]?.LayoutMasters[0]?.ListSlideID.Add(item);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Kiểm tra xem có Layout Blank hay không
        /// </summary>
        /// <param name="eSlideMaster"></param>
        /// <returns></returns>
        private static ELayoutMaster CheckLayoutBlank(ESlideMaster eSlideMaster)
        {
            foreach (var item in eSlideMaster.LayoutMasters)
            {
                if (item.LayoutType == LayoutType.Blank)
                {
                    return item;
                }
            }
            return null;
        }

        #endregion

        #region SelectedBackground
        private BackgroundItem _selectedBackground;
        /// <summary>
        /// Background Style đang được chọn
        /// </summary>
        public BackgroundItem SelectedBackground
        {
            get { return _selectedBackground; }
            set
            {
                if (value == _selectedBackground) return;
                if (_selectedBackground == null) _selectedBackground = new BackgroundItem() { ColorStyle = Brushes.White };
                Global.StartGrouping("backgroundSlide");
                foreach (SlideBase item in Slides)
                {
                    item.UpdateBackground(value);
                }
                 (Application.Current as IAppGlobal).DocumentControl.SelectedTheme.BackgroundStyle = value;
                (Application.Current as IAppGlobal).DocumentControl.SelectedTheme.SlideMasters[0].MainLayer.Background = ColorHelper.ConverterFromColor(value.ColorDataStyle);
                foreach (ELayoutMaster eLayoutMaster in (Application.Current as IAppGlobal).DocumentControl.SelectedTheme.SlideMasters[0].LayoutMasters)
                {
                    eLayoutMaster.MainLayer.Background = ColorHelper.ConverterFromColor(value.ColorDataStyle);
                }
                //(Application.Current as IAppGlobal).SelectedTheme.SlideMasters[0].MainLayer.Background = ColorHelper.ConverterFromColor(value.ColorDataStyle);
                if (Global.CanPushUndo && IsLoaded)
                {
                    UndoRedoByProperty step = new UndoRedoByProperty();
                    step.OldValue = _selectedBackground;
                    step.NewValue = value;
                    step.PropertyName = "SelectedBackground";
                    step.Source = this;
                    Global.PushUndo(step);
                }
                _selectedBackground = value;
                RaisePropertyChanged("SelectedBackground");
                Global.StopGrouping("backgroundSlide");
            }
        }

        #endregion

        #region SelectedColors


        //public EColorManagment SelectedColors
        //{
        //    get { return (EColorManagment)GetValue(SelectedColorsProperty); }
        //    set { SetValue(SelectedColorsProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for SelectedColors.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty SelectedColorsProperty =
        //    DependencyProperty.Register("SelectedColors", typeof(EColorManagment), typeof(DocumentControl), new PropertyMetadata(new EColorManagment(), SelectedColorsPropertyChanged));

        //private static void SelectedColorsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    Global.StartGrouping("clor");
        //    DocumentControl _owner = d as DocumentControl;
        //    (Application.Current as IAppGlobal).UpdateColorSlide(e.NewValue as EColorManagment);

        //    if (Global.CanPushUndo && _owner.IsLoaded)
        //    {
        //        UndoRedoByProperty step = new UndoRedoByProperty();
        //        step.Source = _owner;
        //        step.NewValue = e.NewValue;
        //        step.OldValue = e.OldValue;
        //        step.PropertyName = "SelectedColors";
        //        Global.PushUndo(step);
        //    }
        //    Global.StopGrouping("clor");
        //}

        private EColorManagment _selectedColor;
        /// <summary>
        /// Color đang được chọn
        /// </summary>
        public EColorManagment SelectedColors
        {
            get { return _selectedColor ?? (_selectedColor = new EColorManagment()); }
            set
            {
                if (_selectedColor == value) return;
                Global.StartGrouping("clor");
                SelectedTheme.Colors = _selectedColor;
                UpdateColor(value);
                if (Global.CanPushUndo && IsLoaded)
                {
                    UndoRedoByProperty step = new UndoRedoByProperty();
                    step.Source = this;
                    step.NewValue = value;
                    step.OldValue = _selectedColor;
                    step.PropertyName = "SelectedColors";
                    Global.PushUndo(step);
                }
                Global.StopGrouping("clor");
                _selectedColor = value;
                RaisePropertyChanged("SelectedColors");
            }
        }

        /// <summary>
        /// Cập nhật Color
        /// </summary>
        /// <param name="eColor"></param>
        private void UpdateColor(EColorManagment eColor)
        {
            (Application.Current as IAppGlobal).UpdateColorSlide(eColor);
        }
        #endregion

        #region SelectFont
        private EFontfamily _selectedFont;
        /// <summary>
        /// Font đang được chọn
        /// </summary>
        public EFontfamily SelectedFont
        {
            get { return _selectedFont ?? (_selectedFont = new EFontfamily()); }
            set
            {
                if (_selectedFont == value) return;
                Global.StartGrouping("fnt");
                UpdateFont(value);
                if (Global.CanPushUndo && IsLoaded)
                {
                    UndoRedoByProperty step = new UndoRedoByProperty();
                    step.Source = this;
                    step.NewValue = value;
                    step.OldValue = _selectedFont;
                    step.PropertyName = "SelectedFont";
                    Global.PushUndo(step);
                }
                Global.StopGrouping("fnt");
                _selectedFont = value;
                RaisePropertyChanged("SelectedFont");
            }
        }

        /// <summary>
        /// Cập nhật font
        /// </summary>
        /// <param name="eFontfamily"></param>
        private void UpdateFont(EFontfamily eFontfamily)
        {
            (Application.Current as IAppGlobal).UpdateFontSlide(eFontfamily);
        }
        #endregion

        #region Variables
        private ObservableCollection<VariableViewModelBase> _variables;
        /// <summary>
        /// Danh sách các phần tử biến trong chương trình
        /// </summary>
        public ObservableCollection<VariableViewModelBase> Variables
        {
            get { return _variables ?? (_variables = new ObservableCollection<VariableViewModelBase>()); }
            private set { _variables = value; }
        }

        public object Where { get; internal set; }

        #endregion

        /// <summary>
        /// Adorner chứa
        /// </summary>
        public Adorner Adorner { get; set; }

        #region Overrides

        /// <summary>
        /// Áp dụng template cho đối tượng
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            GRID_ROOT = GetTemplateChild("PART_GridRoot") as Grid;

            foreach (SlideBase item in this.Slides)
            {
                item.DocumentParent = this;
                GRID_ROOT.Children.Add(item);
            }
        }
        #endregion

        /// <summary>
        /// Làm mới dữ liệu
        /// </summary>
        public void RefreshData()
        {
            this.Data.Pages.Clear();
            foreach (var slide in this.Slides)
            {
                slide.RefreshData();
                this.Data.Pages.Add(slide.Data);
            }

            this.Data.VariableData.Variables.Clear();
            foreach (var variable in this.Variables)
            {
                if (variable.Variable?.GetData() is VariableDataBase data)
                {
                    this.Data.VariableData.Variables.Add(data);
                }
            }

            this.Data.SelectedTheme = this.SelectedTheme;
            (Application.Current as IAppGlobal).PlayerDataSetting.RefreshData();
            this.Data.PlayerSetting = (Application.Current as IAppGlobal).PlayerDataSetting.Data;
        }

        /// <summary>
        /// Tải lại dữ liệu
        /// </summary>
        /// <param name="data"></param>
        public void UpdateUI(ElearningDocument data)
        {
            Global.IsDataLoading = true;
            //Cập nhật biến
            this.Variables?.Clear();
            foreach (var variableData in data.VariableData.Variables)
            {
                if (variableData.GetModel()?.GenerateViewModel() is VariableViewModelBase variable)
                {
                    this.Variables?.Add(variable);
                }
            }

            this.Slides.Clear();
            (Application.Current as IAppGlobal).LocalThemesCollection = data.ListETheme;
            this.SelectedTheme = data.SelectedTheme; //Cập nhật chủ đề
            foreach (var slideData in data.Pages)
            {
                var slide = LayoutHelper.LoadDataForSlideLayout(slideData);
                if (slide is SlideBase)
                {
                    this.Slides.Add(slide as SlideBase);
                }
            }
            //Cập nhật trigger
            (Application.Current as IAppGlobal).UpdateTrigger();

            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                Global.IsDataLoading = false;
                foreach (var slide in this.Slides) //Cập nhật lại các slide
                {
                    slide.MainLayout.UpdateThumbnail();
                    foreach (var layout in slide.Layouts)
                    {
                        layout.UpdateThumbnail();
                    }
                }
            }));
        }

        /// <summary>
        /// Chèn vào Slide
        /// </summary>
        /// <param name="pages"></param>
        public void InsertSlide(System.Collections.Generic.IEnumerable<PageElementBase> pages)
        {
            SlideBase _lastSelectedSlide = this.Slides.LastOrDefault(x => x.IsSelected);
            int _currentIndex = -1;
            if(_lastSelectedSlide != null)
            {
                _currentIndex = this.Slides.IndexOf(_lastSelectedSlide);
            }
            foreach (var slideData in pages)
            {
                var slide = LayoutHelper.LoadDataForSlideLayout(slideData);
                if (slide is SlideBase)
                {
                    this.Slides.Insert(++_currentIndex, slide as SlideBase);
                }
            }
        }

        #region UpdateThumbnail
        /// <summary>
        /// Lớp lưu trữ các đối tượng cần cập nhật thumbnail
        /// </summary>
        public class UpdateThumnailRequest
        {
            public LayoutBase LayoutBase { get; set; }
            public long Time { get; set; }
        }

        private List<UpdateThumnailRequest> _updateThumbRequests;
        /// <summary>
        /// Danh sách hàng đợi cần cập nhật
        /// </summary>
        public List<UpdateThumnailRequest> UpdateThumbRequests
        {
            get { return _updateThumbRequests ?? (_updateThumbRequests = new List<UpdateThumnailRequest>()); }
        }

        /// <summary>
        /// Thêm mới một request
        /// </summary>
        /// <param name="layoutBase"></param>
        public void AddRequest(LayoutBase layoutBase)
        {
            UpdateThumnailRequest request = UpdateThumbRequests.FirstOrDefault(x => x.LayoutBase == layoutBase);
            if (request == null)
            {
                request = new UpdateThumnailRequest() { LayoutBase = layoutBase };
                UpdateThumbRequests.Add(request);
            }
            request.Time = DateTime.Now.Ticks;
        }

        /// <summary>
        /// Cập nhật lại nội dung
        /// </summary>
        public void UpdateThumbnail()
        {
            for (int i = 0; i < UpdateThumbRequests.Count; i++)
            {
                if(UpdateThumbRequests[i].Time < DateTime.Now.Ticks) //Nếu vào hàng đợi thì xử lý
                {
                    UpdateThumbRequests[i].LayoutBase.UpdateThumbnail2();
                    UpdateThumbRequests.RemoveAt(i--);
                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INV.Elearning.Core.ViewModel
{
    public class GlobalResources
    {
        private static double[] _fontSizes;
        /// <summary>
        /// Danh sách ngôn ngữ
        /// </summary>
        public static double[] FontSizes
        {
            get
            {
                return _fontSizes ?? (_fontSizes = new double[] { 8, 9, 10, 10.5, 11, 12, 14, 16, 18, 20, 24, 28, 32, 36, 40, 44, 48, 54, 60, 66, 72, 80, 88, 96 });
            }
        }
    }
}

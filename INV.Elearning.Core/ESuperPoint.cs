﻿namespace INV.Elearning.Core
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Lớp lưu trữ đối tượng điểm siêu cấp, có thể chưa kích thước, vị trí
    /// </summary>
    [Serializable]
    [XmlRoot("sp")]
    public class ESuperPoint
    {
        private double _width;
        private double _height;
        private double _left;
        private double _top;

        /// <summary>
        /// Lấy hoặc cài thuộc tính căn
        /// </summary>
        [XmlAttribute("t")]
        public double Top
        {
            get
            {
                return _top;
            }
            set
            {
                _top = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính căn trái
        /// </summary>
        [XmlAttribute("l")]
        public double Left
        {
            get
            {
                return _left;
            }
            set
            {
                _left = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính chiều dài
        /// </summary>
        [XmlAttribute("w")]
        public double Width
        {
            get
            {
                return _width;
            }

            set
            {
                _width = value;
            }
        }

        /// <summary>
        /// Lấy hoặc cài đặt thuộc tính chiều rộng
        /// </summary>
        [XmlAttribute("h")]
        public double Height
        {
            get
            {
                return _height;
            }

            set
            {
                _height = value;
            }
        }

        public ESuperPoint Clone()
        {
            return new ESuperPoint() { Left = this.Left, Top = this.Top, Height = this.Height, Width = this.Width };
        }
    }
}

﻿using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;

namespace INV.Elearning.Core.Helper
{
    public class LayoutHelper
    {
        /// <summary>
        /// Tai lai du lieu danh cho Slide hoac layout
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static object LoadDataForSlideLayout(object data)
        {
            if (data == null) return null;

            var _module = Global.GetModuleByData(data.GetType()); //Tìm mô đun quản lý dữ liệu
            if (_module != null)
            {
                return _module.LoadData(data);
            }
            else
            {
                if (data is NormalPage page)
                {
                    NormalSlide _normalSlide = new NormalSlide();
                    _normalSlide.LoadData(page);
                    return _normalSlide;
                }
                else if (data is PageLayer layer)
                {
                    LayoutBase _layout = new LayoutBase();
                    _layout.UpdateUI(layer);
                    return _layout;
                }
            }

            return null;
        }

        /// <summary>
        /// Tải nhanh dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static object QuickLoadDataForSlideLayout(object data)
        {
            if (data == null) return null;

            var _module = Global.GetModuleByData(data.GetType()); //Tìm mô đun quản lý dữ liệu
            if (_module != null)
            {
                return _module.LoadData(data);
            }
            else
            {
                if (data is NormalPage page)
                {
                    NormalSlide _normalSlide = new NormalSlide();
                    _normalSlide.QuickUpdateUI(page);
                    return _normalSlide;
                }
                else if (data is PageLayer layer)
                {
                    LayoutBase _layout = new LayoutBase();
                    _layout.QuickUpdateUI(layer);
                    return _layout;
                }
            }

            return null;
        }
    }
}

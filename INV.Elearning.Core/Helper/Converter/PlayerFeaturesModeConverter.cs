﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Converter
{
    public class PlayerFeaturesModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            PlayerFeaturesMode featuresMode = (PlayerFeaturesMode)value;
            if (featuresMode == PlayerFeaturesMode.Default)
            {
                return 0;
            }
            else return 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int)value == 0)
            {
                return PlayerFeaturesMode.Default;
            }
            else return PlayerFeaturesMode.Custom;
        }
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Lớp giả chuyển đổi. Sẽ không có thao tác thay đổi dữ liệu nào trong chuyển đổi này
    /// </summary>
    public class MultiValueConverterFake : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[0];
        }
    }
}

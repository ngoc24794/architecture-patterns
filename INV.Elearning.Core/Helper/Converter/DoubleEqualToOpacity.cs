﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Chuyển đổi so sánh sang Opacity
    /// </summary>
    public class DoubleEqualToOpacity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value * (Application.Current as IAppGlobal).DocumentPageScale <= double.Parse(parameter.ToString()) ? 0.0 : 1.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    /// <summary>
    /// Chuyển đổi so sánh sang Opacity
    /// </summary>
    public class MultiDoubleEqualToOpacity : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)values[0] * (double)values[1] <= double.Parse(parameter.ToString()) ? 0.0 : 1.0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, Binding.DoNothing };
        }
    }
}

﻿using INV.Elearning.Core.Model;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Chuyển đổi sang kiểu Pattern Fill
    /// </summary>
    public class EnumPatternBrushConverter : IMultiValueConverter
    {
        #region Override

        /// <summary>
        /// Lấy Brush từ dữ liệu truyền vào
        /// </summary>
        /// <param name="values">Phần tử đầu tiên là kiểu enum HatchStyle<br/>
        /// Phần tử thứ hai Foreground<br/>
        /// Phần tử thứ ba Background</param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[1] == DependencyProperty.UnsetValue || values[2] == DependencyProperty.UnsetValue)
                return null;

            return GetHatchBrush((HatchStyle)values[0], (string)values[1], (string)values[2]);
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
        #endregion

        #region HatchFill

        /// <summary>
        /// Lấy màu dựa vào enum
        /// </summary>
        /// <param name="yHatchStyle"></param>
        /// <param name="clrForeColor"></param>
        /// <param name="clrBackColor"></param>
        /// <returns></returns>
        public DrawingBrush GetHatchBrush(HatchStyle yHatchStyle, string clrForeColor, string clrBackColor)
        {
            DrawingBrush oReturnBrush = new DrawingBrush();
            GeometryGroup oHatchGroup = new GeometryGroup();
            GeometryGroup oHatchCtrlGroup = new GeometryGroup();
            int lWidth = 0;
            int lHeight = 0;
            HatchType yType = HatchType.HT_LINE;
            bool bAliased = true;
            switch (yHatchStyle)
            {
                case HatchStyle.HS_HORIZONTAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 7, 0));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_VERTICAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 0, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_FORWARDDIAGONAL:
                    lWidth = 16;
                    lHeight = 16;
                    oHatchGroup.Children.Add(mp_GLine(0, 12, 3, 15));
                    oHatchGroup.Children.Add(mp_GLine(0, 4, 11, 15));
                    oHatchGroup.Children.Add(mp_GLine(4, 0, 15, 11));
                    oHatchGroup.Children.Add(mp_GLine(12, 0, 15, 3));
                    System.Diagnostics.Debug.Write(oHatchGroup);
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_BACKWARDDIAGONAL:
                    lWidth = 16;
                    lHeight = 16;
                    oHatchGroup.Children.Add(mp_GLine(0, 12, 3, 15));
                    oHatchGroup.Children.Add(mp_GLine(0, 4, 11, 15));
                    oHatchGroup.Children.Add(mp_GLine(4, 0, 15, 11));
                    oHatchGroup.Children.Add(mp_GLine(12, 0, 15, 3));
                    oHatchGroup.Transform = new RotateTransform(90, 8, 8);
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_LARGEGRID:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 7, 0));
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 0, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DIAGONALCROSS:
                    lWidth = 7;
                    lHeight = 7;
                    oHatchGroup.Children.Add(mp_GRect(1, 1, 5, 5));
                    oHatchGroup.Transform = new RotateTransform(45, 3, 3);
                    yType = HatchType.HT_LINE; break;
                    bAliased = false;
                case HatchStyle.HS_PERCENT05:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 4));
                    oHatchGroup.Children.Add(mp_GPoint(4, 0));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT10:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    oHatchGroup.Children.Add(mp_GPoint(4, 0));
                    oHatchGroup.Children.Add(mp_GPoint(0, 6));
                    oHatchGroup.Children.Add(mp_GPoint(4, 4));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT20:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT25:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    oHatchGroup.Children.Add(mp_GPoint(2, 1));
                    oHatchGroup.Children.Add(mp_GPoint(2, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT30:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    oHatchGroup.Children.Add(mp_GPoint(2, 0));
                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT40:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(2, 0));
                    oHatchGroup.Children.Add(mp_GPoint(4, 0));
                    oHatchGroup.Children.Add(mp_GPoint(6, 0));

                    oHatchGroup.Children.Add(mp_GPoint(3, 1));
                    oHatchGroup.Children.Add(mp_GPoint(5, 1));
                    oHatchGroup.Children.Add(mp_GPoint(7, 1));

                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(4, 2));
                    oHatchGroup.Children.Add(mp_GPoint(6, 2));

                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    oHatchGroup.Children.Add(mp_GPoint(5, 3));
                    oHatchGroup.Children.Add(mp_GPoint(7, 3));

                    oHatchGroup.Children.Add(mp_GPoint(0, 4));
                    oHatchGroup.Children.Add(mp_GPoint(2, 4));
                    oHatchGroup.Children.Add(mp_GPoint(4, 4));
                    oHatchGroup.Children.Add(mp_GPoint(6, 4));
                    oHatchGroup.Children.Add(mp_GPoint(1, 5));
                    oHatchGroup.Children.Add(mp_GPoint(3, 5));
                    oHatchGroup.Children.Add(mp_GPoint(7, 5));
                    oHatchGroup.Children.Add(mp_GPoint(0, 6));
                    oHatchGroup.Children.Add(mp_GPoint(2, 6));
                    oHatchGroup.Children.Add(mp_GPoint(4, 6));
                    oHatchGroup.Children.Add(mp_GPoint(6, 6));
                    oHatchGroup.Children.Add(mp_GPoint(1, 7));
                    oHatchGroup.Children.Add(mp_GPoint(3, 7));
                    oHatchGroup.Children.Add(mp_GPoint(5, 7));
                    oHatchGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT50:
                    lWidth = 2;
                    lHeight = 2;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT60:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(2, 0));
                    oHatchGroup.Children.Add(mp_GPoint(3, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    oHatchGroup.Children.Add(mp_GPoint(3, 1));
                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    oHatchGroup.Children.Add(mp_GPoint(1, 2));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT70:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(1, 0));
                    oHatchGroup.Children.Add(mp_GPoint(2, 0));
                    oHatchGroup.Children.Add(mp_GPoint(3, 0));
                    oHatchGroup.Children.Add(mp_GPoint(0, 1));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    oHatchGroup.Children.Add(mp_GPoint(3, 1));
                    oHatchGroup.Children.Add(mp_GPoint(1, 2));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(3, 2));
                    oHatchGroup.Children.Add(mp_GPoint(0, 3));
                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT75:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(2, 0));
                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    InvertColors(ref clrForeColor, ref clrBackColor);
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT80:
                    lWidth = 8;
                    lHeight = 7;
                    oHatchGroup.Children.Add(mp_GPoint(3, 0));
                    oHatchGroup.Children.Add(mp_GPoint(3, 4));
                    oHatchGroup.Children.Add(mp_GPoint(7, 2));
                    oHatchGroup.Children.Add(mp_GPoint(7, 6));
                    InvertColors(ref clrForeColor, ref clrBackColor);
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PERCENT90:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 5));
                    oHatchGroup.Children.Add(mp_GPoint(4, 1));
                    InvertColors(ref clrForeColor, ref clrBackColor);
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_LIGHTDOWNWARDDIAGONAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_LIGHTUPWARDDIAGONAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 3));
                    oHatchGroup.Children.Add(mp_GPoint(1, 2));
                    oHatchGroup.Children.Add(mp_GPoint(2, 1));
                    oHatchGroup.Children.Add(mp_GPoint(3, 0));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DARKDOWNWARDDIAGONAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 3));
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    oHatchGroup.Children.Add(mp_GPoint(1, 0));
                    oHatchGroup.Children.Add(mp_GPoint(2, 1));
                    oHatchGroup.Children.Add(mp_GPoint(3, 2));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DARKUPWARDDIAGONAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(0, 1));
                    oHatchGroup.Children.Add(mp_GPoint(1, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(3, 1));
                    oHatchGroup.Children.Add(mp_GPoint(2, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 2));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_WIDEDOWNWARDDIAGONAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(3, 0, 5, 0));
                    oHatchGroup.Children.Add(mp_GLine(4, 1, 6, 1));
                    oHatchGroup.Children.Add(mp_GLine(5, 2, 7, 2));
                    oHatchGroup.Children.Add(mp_GPoint(0, 3));
                    oHatchGroup.Children.Add(mp_GLine(6, 3, 7, 3));
                    oHatchGroup.Children.Add(mp_GLine(0, 4, 1, 4));
                    oHatchGroup.Children.Add(mp_GPoint(7, 4));
                    oHatchGroup.Children.Add(mp_GLine(0, 5, 2, 5));
                    oHatchGroup.Children.Add(mp_GLine(1, 6, 3, 6));
                    oHatchGroup.Children.Add(mp_GLine(2, 7, 4, 7));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_WIDEUPWARDDIAGONAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(4, 0, 6, 0));
                    oHatchGroup.Children.Add(mp_GLine(3, 1, 5, 1));
                    oHatchGroup.Children.Add(mp_GLine(2, 2, 4, 2));
                    oHatchGroup.Children.Add(mp_GLine(1, 3, 3, 3));
                    oHatchGroup.Children.Add(mp_GLine(0, 4, 2, 4));
                    oHatchGroup.Children.Add(mp_GLine(0, 5, 1, 5));
                    oHatchGroup.Children.Add(mp_GPoint(7, 5));
                    oHatchGroup.Children.Add(mp_GPoint(0, 6));
                    oHatchGroup.Children.Add(mp_GLine(6, 6, 7, 6));
                    oHatchGroup.Children.Add(mp_GLine(5, 7, 7, 7));
                    oHatchCtrlGroup.Children.Add(mp_GLine(0, 0, 7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_LIGHTVERTICAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 0, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_LIGHTHORIZONTAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 3, 0));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_NARROWVERTICAL:
                    lWidth = 2;
                    lHeight = 2;
                    oHatchGroup.Children.Add(mp_GLine(1, 0, 1, 1));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_NARROWHORIZONTAL:
                    lWidth = 2;
                    lHeight = 2;
                    oHatchGroup.Children.Add(mp_GLine(0, 1, 1, 1));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DARKVERTICAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 0, 3));
                    oHatchGroup.Children.Add(mp_GLine(1, 0, 1, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DARKHORIZONTAL:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 3, 0));
                    oHatchGroup.Children.Add(mp_GLine(0, 1, 3, 1));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DASHEDDOWNWARDDIAGONAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(2, 4));
                    oHatchGroup.Children.Add(mp_GPoint(3, 5));
                    oHatchGroup.Children.Add(mp_GPoint(4, 2));
                    oHatchGroup.Children.Add(mp_GPoint(5, 3));
                    oHatchGroup.Children.Add(mp_GPoint(6, 4));
                    oHatchGroup.Children.Add(mp_GPoint(7, 5));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DASHEDUPWARDDIAGONAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 7));
                    oHatchGroup.Children.Add(mp_GPoint(1, 6));
                    oHatchGroup.Children.Add(mp_GPoint(2, 5));
                    oHatchGroup.Children.Add(mp_GPoint(3, 4));
                    oHatchGroup.Children.Add(mp_GPoint(4, 7));
                    oHatchGroup.Children.Add(mp_GPoint(5, 6));
                    oHatchGroup.Children.Add(mp_GPoint(6, 5));
                    oHatchGroup.Children.Add(mp_GPoint(7, 4));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DASHEDHORIZONTAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(4, 0, 7, 0));
                    oHatchGroup.Children.Add(mp_GLine(0, 4, 3, 4));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DASHEDVERTICAL:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 0, 1));
                    oHatchGroup.Children.Add(mp_GLine(0, 6, 0, 7));
                    oHatchGroup.Children.Add(mp_GLine(4, 2, 4, 5));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_SMALLCONFETTI:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(4, 1));
                    oHatchGroup.Children.Add(mp_GPoint(1, 2));
                    oHatchGroup.Children.Add(mp_GPoint(6, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 4));
                    oHatchGroup.Children.Add(mp_GPoint(7, 5));
                    oHatchGroup.Children.Add(mp_GPoint(2, 6));
                    oHatchGroup.Children.Add(mp_GPoint(5, 7));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_LARGECONFETTI:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(0, 1, 0, 2));
                    oHatchGroup.Children.Add(mp_GLine(0, 6, 0, 7));
                    oHatchGroup.Children.Add(mp_GLine(1, 6, 1, 7));
                    oHatchGroup.Children.Add(mp_GLine(2, 2, 2, 3));
                    oHatchGroup.Children.Add(mp_GLine(3, 2, 3, 3));
                    oHatchGroup.Children.Add(mp_GLine(3, 5, 3, 6));
                    oHatchGroup.Children.Add(mp_GLine(4, 0, 4, 1));
                    oHatchGroup.Children.Add(mp_GLine(4, 5, 4, 6));
                    oHatchGroup.Children.Add(mp_GLine(5, 0, 5, 1));
                    oHatchGroup.Children.Add(mp_GLine(6, 4, 6, 5));
                    oHatchGroup.Children.Add(mp_GLine(7, 1, 7, 2));
                    oHatchGroup.Children.Add(mp_GLine(7, 4, 7, 5));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_ZIGZAG:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GLine(3, 3, 4, 3));
                    oHatchGroup.Children.Add(mp_GPoint(5, 2));
                    oHatchGroup.Children.Add(mp_GPoint(6, 1));
                    oHatchGroup.Children.Add(mp_GPoint(7, 0));

                    oHatchGroup.Children.Add(mp_GPoint(0, 4));
                    oHatchGroup.Children.Add(mp_GPoint(1, 5));
                    oHatchGroup.Children.Add(mp_GPoint(2, 6));
                    oHatchGroup.Children.Add(mp_GLine(3, 7, 4, 7));
                    oHatchGroup.Children.Add(mp_GPoint(5, 6));
                    oHatchGroup.Children.Add(mp_GPoint(6, 5));
                    oHatchGroup.Children.Add(mp_GPoint(7, 4));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_WAVE:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(2, 0));
                    oHatchGroup.Children.Add(mp_GPoint(5, 0));
                    oHatchGroup.Children.Add(mp_GPoint(7, 0));
                    oHatchGroup.Children.Add(mp_GLine(0, 2, 1, 2));

                    oHatchGroup.Children.Add(mp_GLine(3, 4, 4, 4));
                    oHatchGroup.Children.Add(mp_GPoint(2, 4));
                    oHatchGroup.Children.Add(mp_GPoint(5, 4));
                    oHatchGroup.Children.Add(mp_GPoint(7, 4));

                    oHatchGroup.Children.Add(mp_GLine(0, 6, 1, 6));
                    oHatchGroup.Children.Add(mp_GLine(3, 8, 4, 8));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DIAGONALBRICK:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 7));
                    oHatchGroup.Children.Add(mp_GPoint(1, 6));
                    oHatchGroup.Children.Add(mp_GPoint(2, 5));
                    oHatchGroup.Children.Add(mp_GPoint(3, 4));
                    oHatchGroup.Children.Add(mp_GPoint(4, 3));
                    oHatchGroup.Children.Add(mp_GPoint(5, 2));
                    oHatchGroup.Children.Add(mp_GPoint(6, 1));
                    oHatchGroup.Children.Add(mp_GPoint(7, 0));
                    oHatchGroup.Children.Add(mp_GPoint(4, 4));
                    oHatchGroup.Children.Add(mp_GPoint(5, 5));
                    oHatchGroup.Children.Add(mp_GPoint(6, 6));
                    oHatchGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_HORIZONTALBRICK:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(4, 0));
                    oHatchGroup.Children.Add(mp_GPoint(4, 1));
                    oHatchGroup.Children.Add(mp_GLine(0, 1, 0, 5));
                    oHatchGroup.Children.Add(mp_GPoint(4, 6));
                    oHatchGroup.Children.Add(mp_GPoint(4, 7));

                    oHatchGroup.Children.Add(mp_GLine(1, 2, 7, 2));
                    oHatchGroup.Children.Add(mp_GLine(1, 6, 7, 6));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_WEAVE:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(1, 1));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(0, 4));
                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 1));
                    oHatchGroup.Children.Add(mp_GPoint(4, 0));
                    oHatchGroup.Children.Add(mp_GPoint(5, 1));
                    oHatchGroup.Children.Add(mp_GPoint(6, 2));
                    oHatchGroup.Children.Add(mp_GPoint(7, 3));
                    oHatchGroup.Children.Add(mp_GPoint(5, 3));
                    oHatchGroup.Children.Add(mp_GPoint(4, 4));
                    oHatchGroup.Children.Add(mp_GPoint(3, 5));
                    oHatchGroup.Children.Add(mp_GPoint(2, 6));
                    oHatchGroup.Children.Add(mp_GPoint(1, 7));
                    oHatchGroup.Children.Add(mp_GPoint(3, 7));
                    oHatchGroup.Children.Add(mp_GPoint(5, 5));
                    oHatchGroup.Children.Add(mp_GPoint(6, 6));
                    oHatchGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_PLAID:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 3, 0));
                    oHatchGroup.Children.Add(mp_GLine(0, 1, 3, 1));

                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(4, 2));
                    oHatchGroup.Children.Add(mp_GPoint(6, 2));

                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    oHatchGroup.Children.Add(mp_GPoint(5, 3));
                    oHatchGroup.Children.Add(mp_GPoint(7, 3));


                    oHatchGroup.Children.Add(mp_GPoint(0, 4));
                    oHatchGroup.Children.Add(mp_GPoint(2, 4));
                    oHatchGroup.Children.Add(mp_GPoint(4, 4));
                    oHatchGroup.Children.Add(mp_GPoint(6, 4));

                    oHatchGroup.Children.Add(mp_GPoint(1, 5));
                    oHatchGroup.Children.Add(mp_GPoint(3, 5));
                    oHatchGroup.Children.Add(mp_GPoint(5, 5));
                    oHatchGroup.Children.Add(mp_GPoint(7, 5));

                    oHatchGroup.Children.Add(mp_GLine(0, 6, 3, 6));
                    oHatchGroup.Children.Add(mp_GLine(0, 7, 3, 7));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DIVOT:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 1));
                    oHatchGroup.Children.Add(mp_GPoint(0, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 5));
                    oHatchGroup.Children.Add(mp_GPoint(3, 7));
                    oHatchGroup.Children.Add(mp_GPoint(4, 6));
                    oHatchGroup.Children.Add(mp_GPoint(7, 2));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DOTTEDGRID:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(1, 6));
                    oHatchGroup.Children.Add(mp_GPoint(3, 6));
                    oHatchGroup.Children.Add(mp_GPoint(5, 6));
                    oHatchGroup.Children.Add(mp_GPoint(7, 6));
                    oHatchGroup.Children.Add(mp_GPoint(7, 4));
                    oHatchGroup.Children.Add(mp_GPoint(7, 2));
                    oHatchGroup.Children.Add(mp_GPoint(7, 0));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_DOTTEDDIAMOND:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 0));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(4, 4));
                    oHatchGroup.Children.Add(mp_GPoint(6, 6));
                    oHatchGroup.Children.Add(mp_GPoint(2, 6));
                    oHatchGroup.Children.Add(mp_GPoint(6, 2));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_SHINGLE:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GPoint(0, 3));
                    oHatchGroup.Children.Add(mp_GPoint(1, 4));
                    oHatchGroup.Children.Add(mp_GPoint(2, 5));
                    oHatchGroup.Children.Add(mp_GPoint(3, 5));
                    oHatchGroup.Children.Add(mp_GPoint(4, 6));
                    oHatchGroup.Children.Add(mp_GPoint(5, 6));
                    oHatchGroup.Children.Add(mp_GPoint(6, 7));
                    oHatchGroup.Children.Add(mp_GPoint(4, 4));
                    oHatchGroup.Children.Add(mp_GPoint(5, 3));
                    oHatchGroup.Children.Add(mp_GPoint(6, 2));
                    oHatchGroup.Children.Add(mp_GPoint(7, 2));
                    oHatchGroup.Children.Add(mp_GPoint(7, 0));
                    oHatchGroup.Children.Add(mp_GPoint(7, 1));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_TRELLIS:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 3, 0));
                    oHatchGroup.Children.Add(mp_GLine(1, 1, 2, 1));
                    oHatchGroup.Children.Add(mp_GLine(0, 2, 3, 2));
                    oHatchGroup.Children.Add(mp_GPoint(0, 3));
                    oHatchGroup.Children.Add(mp_GPoint(3, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_SPHERE:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GLine(1, 0, 3, 0));
                    oHatchGroup.Children.Add(mp_GLine(1, 1, 3, 1));
                    oHatchGroup.Children.Add(mp_GPoint(0, 2));
                    oHatchGroup.Children.Add(mp_GPoint(4, 2));
                    oHatchGroup.Children.Add(mp_GLine(1, 3, 2, 3));
                    oHatchGroup.Children.Add(mp_GPoint(0, 6));
                    oHatchGroup.Children.Add(mp_GPoint(4, 6));
                    oHatchGroup.Children.Add(mp_GLine(1, 7, 3, 7));
                    oHatchGroup.Children.Add(mp_GLine(5, 7, 6, 7));
                    oHatchGroup.Children.Add(mp_GLine(5, 3, 7, 3));
                    oHatchGroup.Children.Add(mp_GLine(5, 4, 7, 4));
                    oHatchGroup.Children.Add(mp_GLine(5, 5, 7, 5));
                    oHatchCtrlGroup.Children.Add(mp_GPoint(7, 7));
                    InvertColors(ref clrForeColor, ref clrBackColor);
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_SMALLGRID:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 3, 0));
                    oHatchGroup.Children.Add(mp_GLine(0, 0, 0, 3));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_SMALLCHECKERBOARD:
                    lWidth = 4;
                    lHeight = 4;
                    oHatchGroup.Children.Add(mp_GRect(0, 0, 2, 2));
                    oHatchGroup.Children.Add(mp_GRect(2, 2, 2, 2));
                    yType = HatchType.HT_RECTANGLE; break;
                case HatchStyle.HS_LARGECHECKERBOARD:
                    lWidth = 8;
                    lHeight = 8;
                    oHatchGroup.Children.Add(mp_GRect(0, 0, 4, 4));
                    oHatchGroup.Children.Add(mp_GRect(4, 4, 4, 4));
                    yType = HatchType.HT_RECTANGLE; break;
                case HatchStyle.HS_OUTLINEDDIAMOND:
                    lWidth = 8;
                    lHeight = 8;

                    oHatchGroup.Children.Add(mp_GPoint(0, 4));
                    oHatchGroup.Children.Add(mp_GPoint(1, 3));
                    oHatchGroup.Children.Add(mp_GPoint(2, 2));
                    oHatchGroup.Children.Add(mp_GPoint(3, 1));
                    oHatchGroup.Children.Add(mp_GPoint(4, 0));

                    oHatchGroup.Children.Add(mp_GPoint(5, 1));
                    oHatchGroup.Children.Add(mp_GPoint(6, 2));
                    oHatchGroup.Children.Add(mp_GPoint(7, 3));

                    oHatchGroup.Children.Add(mp_GPoint(7, 5));
                    oHatchGroup.Children.Add(mp_GPoint(6, 6));
                    oHatchGroup.Children.Add(mp_GPoint(5, 7));

                    oHatchGroup.Children.Add(mp_GPoint(3, 7));
                    oHatchGroup.Children.Add(mp_GPoint(2, 6));
                    oHatchGroup.Children.Add(mp_GPoint(1, 5));
                    yType = HatchType.HT_LINE; break;
                case HatchStyle.HS_SOLIDDIAMOND:
                    lWidth = 7;
                    lHeight = 7;
                    oHatchGroup.Children.Add(mp_GRect(1, 1, 5, 5));
                    oHatchGroup.Transform = new RotateTransform(45, 3, 3);
                    yType = HatchType.HT_RECTANGLE; break;
            }
            var _brushCvt = new BrushConverter();

            GeometryDrawing oBackgroundSquare = new GeometryDrawing(_brushCvt.ConvertFromString(clrBackColor) as Brush, null, new RectangleGeometry(new Rect(0, 0, lWidth, lHeight)));
            SolidColorBrush oHatchBrush = _brushCvt.ConvertFromString(clrForeColor) as SolidColorBrush;
            Pen oHatchPen = new Pen(oHatchBrush, 1);
            oHatchBrush.Freeze();
            oHatchPen.Freeze();
            SolidColorBrush oHatchCtrlBrush = new SolidColorBrush(Colors.Red);
            Pen oHatchCtrlPen = new Pen(oHatchCtrlBrush, 1);
            oHatchCtrlBrush.Freeze();
            oHatchCtrlPen.Freeze();
            GeometryDrawing oHatch = null;
            GeometryDrawing oHatchCtrl = null;
            switch (yType)
            {
                case HatchType.HT_RECTANGLE:
                    oHatch = new GeometryDrawing(oHatchBrush, null, oHatchGroup);
                    if (oHatchCtrlGroup.Children.Count > 0)
                    {
                        oHatchCtrl = new GeometryDrawing(oHatchCtrlBrush, null, oHatchCtrlGroup);
                    }
                    break;
                case HatchType.HT_LINE:
                    oHatch = new GeometryDrawing(null, oHatchPen, oHatchGroup);
                    if (oHatchCtrlGroup.Children.Count > 0)
                    {
                        oHatchCtrl = new GeometryDrawing(null, oHatchCtrlPen, oHatchCtrlGroup);
                    }
                    break;

            }
            DrawingGroup oDrawingGroup = new DrawingGroup();
            if (bAliased)
            {
                oDrawingGroup.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            }
            if (oHatchCtrl != null)
            {
                oDrawingGroup.Children.Add(oHatchCtrl);
            }
            oDrawingGroup.Children.Add(oBackgroundSquare);
            oDrawingGroup.Children.Add(oHatch);
            oReturnBrush.Drawing = oDrawingGroup;
            oReturnBrush.Stretch = Stretch.None;
            oReturnBrush.ViewportUnits = BrushMappingMode.Absolute;
            oReturnBrush.Viewport = new Rect(0, 0, lWidth, lHeight);
            oReturnBrush.TileMode = TileMode.Tile;
            oReturnBrush.Freeze();
            return oReturnBrush;
        }

        /// <summary>
        /// Đường thẳng
        /// </summary>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <returns></returns>
        private LineGeometry mp_GLine(int X1, int Y1, int X2, int Y2)
        {
            if (X1 != X2)
            {
                X2 = X2 + 1;
            }
            if (Y1 != Y2)
            {
                Y2 = Y2 + 1;
            }
            LineGeometry oReturn = new LineGeometry(new Point(X1, Y1), new Point(X2, Y2)); ;
            return oReturn;
        }

        /// <summary>
        /// Điểm
        /// </summary>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <returns></returns>
        private LineGeometry mp_GPoint(int X1, int Y1)
        {
            LineGeometry oReturn = new LineGeometry(new Point(X1, Y1), new Point(X1 + 1, Y1 + 1)); ;
            return oReturn;
        }

        /// <summary>
        /// Hình chữ nhật
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <returns></returns>
        private RectangleGeometry mp_GRect(int X, int Y, int Width, int Height)
        {
            RectangleGeometry oReturn = new RectangleGeometry(new Rect(X, Y, Width, Height)); ;
            return oReturn;
        }

        /// <summary>
        /// Đảo màu
        /// </summary>
        /// <param name="clrForeColor"></param>
        /// <param name="clrBackColor"></param>
        private void InvertColors(ref string clrForeColor, ref string clrBackColor)
        {
            string clrBuff;
            clrBuff = clrBackColor;
            clrBackColor = clrForeColor;
            clrForeColor = clrBuff;
        }

        #endregion
    }
}

﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Converter
{
    public class WhenRevisitingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            RevisitMode revisitMode = (RevisitMode)value;
            if (revisitMode == RevisitMode.Auto)
            {
                return 0;
            }
            else if (revisitMode == RevisitMode.Initial)
            {
                return 1;
            }
            else return 2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}

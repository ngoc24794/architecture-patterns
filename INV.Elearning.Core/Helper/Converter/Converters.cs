﻿

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Lớp cung cấp các hàm chuyển đối chính
    /// </summary>
    public class Converters
    {
        /// <summary>
        /// Chuyển đổi giữa 2 danh sách màu
        /// </summary>
        public static SolidsToSolids SolidsToSolids = new SolidsToSolids();

        /// <summary>
        /// Chuyển đổi giữa màu core và màu control
        /// </summary>
        public static ESolidToCSolid ESolidToCSolid = new ESolidToCSolid();

        /// <summary>
        /// Sắp xếp danh sách các Font chữ
        /// </summary>
        public static FontFamiliesConverter FontFamiliesConverter = new FontFamiliesConverter();

        /// <summary>
        /// Chuyển đổi với màu hệ thống
        /// </summary>
        public static CSolidToColor CSolidToColor = new CSolidToColor();        

        /// <summary>
        /// Chuyển đổi từ false sang hiển thị, true sang ẩn
        /// </summary>
        public static InBooleanToVisibilityConverter InBooleanToVisibilityConverter = new InBooleanToVisibilityConverter();

        /// <summary>
        /// So sanh value va parameter
        /// </summary>

        public static CompareValueToVisibilityConverter CompareValueToVisibilityConverter = new CompareValueToVisibilityConverter();

        /// <summary>
        /// So sánh nhiều phàn tử
        /// </summary>

        public static CompareValueToVisibilityMultiConverter CompareValueToVisibilityMultiConverter = new CompareValueToVisibilityMultiConverter();

        public static InCompareValueToVisibilityConverter InCompareValueToVisibilityConverter = new InCompareValueToVisibilityConverter();

        public static CompareValueToBooleanConverter CompareValueToBooleanConverter = new CompareValueToBooleanConverter();
        public static CompareValueToInBooleanConverter CompareValueToInBooleanConverter = new CompareValueToInBooleanConverter();

        /// <summary>
        /// Chuyển đổi từ true sang hiển thị, false sang ẩn
        /// </summary>
        public static BooleanToVisibility BooleanToVisibility = new BooleanToVisibility();

        /// <summary>
        /// Chuyển đổi giả
        /// </summary>
        public static MultiValueConverterFake MultiValueConverterFake = new MultiValueConverterFake();

        /// <summary>
        /// Chuyển đổi thuộc tính
        /// </summary>
        public static PropertyConverter PropertyConverter = new PropertyConverter();

        /// <summary>
        /// Chuyển đổi sang kiểu Pattern Fill
        /// </summary>
        public static EnumPatternBrushConverter EnumPatternBrushConverter = new EnumPatternBrushConverter();

        /// <summary>
        /// Chuyển đổi các giá trị vào việc hiển thị các thumb theo kích thước
        /// </summary>
        public static MultiDoubleEqualToOpacity MultiDoubleEqualToOpacity = new MultiDoubleEqualToOpacity();

        /// <summary>
        /// Chuyển đổi các giá trị
        /// </summary>
        public static MultiValueToOpacity MultiValueToOpacity = new MultiValueToOpacity();

        /// <summary>
        /// Chuyển đổi ngược của kiểu bool
        /// </summary>
        public static InBooleanToBoolean InBooleanToBoolean = new InBooleanToBoolean();
    }
}

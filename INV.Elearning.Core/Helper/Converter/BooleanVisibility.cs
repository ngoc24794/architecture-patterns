﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Chuyển đổi từ kiểu bool sang kiểu visibility</br>
    /// Với true là hiển thị, false là không hiển thị
    /// </summary>
    public class BooleanToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value == Visibility.Visible;
        }
    }

    /// <summary>
    /// Chuyển đổi từ kiểu bool sang kiểu visibility</br>
    /// Với false là hiển thị, true là không hiển thị
    /// </summary>
    public class InBooleanToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value != Visibility.Visible;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class MultiValueToOpacity : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)values[0] && (double)values[1] > double.Parse(parameter.ToString()) ? 1.0 : 0.0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

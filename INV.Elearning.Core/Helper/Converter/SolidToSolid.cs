﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper
{
    public class SolidsToSolids : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _listColor = value as List<SolidColor>;
            if(_listColor?.Count > 0)
            {
                var _result = new List<INV.Elearning.Controls.SolidColor>();
                _listColor.ForEach(x =>
                {
                    _result.Add(new INV.Elearning.Controls.SolidColor() { Color = x.Color, Name = x.Name, SpecialName = x.SpecialName });
                });
                return _result;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    /// <summary>
    /// Chuyền từ dữ liệu của thư viện Controls và thư viện Core
    /// </summary>
    public class ESolidToCSolid : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is SolidColor)
            {
                return new INV.Elearning.Controls.SolidColor() { Color = (value as SolidColor).Color, Name = (value as SolidColor).Name, SpecialName = (value as SolidColor).SpecialName };
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is INV.Elearning.Controls.SolidColor)
            {
                return new SolidColor() { Color = (value as INV.Elearning.Controls.SolidColor).Color, Name = (value as INV.Elearning.Controls.SolidColor).Name, SpecialName = (value as INV.Elearning.Controls.SolidColor).SpecialName };
            }

            return null;
        }
    }

    /// <summary>
    /// Chuyển từ dữ liệu mà Core sang màu Media.Color
    /// </summary>
    public class CSolidToColor: IValueConverter
    {
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is INV.Elearning.Controls.SolidColor)
            {
                return (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString((value as INV.Elearning.Controls.SolidColor).Color);
            }

            return null;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is System.Windows.Media.Color)
            {
                return new INV.Elearning.Controls.SolidColor() { Color = ((System.Windows.Media.Color)value).ToString(), Name = ((System.Windows.Media.Color)value).ToString() };
            }

            return null;
        }
    }
}

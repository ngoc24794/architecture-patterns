﻿using INV.Elearning.Core.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Converter
{
    public class SlideAdvancesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SlideAdvanceMode advanceMode = (SlideAdvanceMode)value;
            if (advanceMode == SlideAdvanceMode.Auto)
            {
                return 0;
            }
            else return 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}

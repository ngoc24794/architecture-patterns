﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Lớp tính toán thông số căn lề trên cho đối tượng dựa vào góc quay, kích thước
    /// </summary>
    class TopConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _angle = (double)values[0];
            var _distance = (double)values[1];

            double _top = LocationHelper.GetMinY(parameter as FrameworkElement, _angle); //Cài đặt vị trí

            return _top + _distance;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

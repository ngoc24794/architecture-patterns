﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Sắp xếp lại danh sách font chữ
    /// </summary>
    public class FontFamiliesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _fontFamilies = value as ICollection<FontFamily>;
            if(_fontFamilies != null)
            {
                return _fontFamilies.OrderBy(x => x.Source);
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}

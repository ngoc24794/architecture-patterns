﻿using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper
{
    public class PropertyConverter : IMultiValueConverter
    {

        /// <summary>
        /// Hàm chuyển đổi
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _elements = (values[0] as ObservableCollection<ObjectElement>);
            if (_elements?.Count > 0)
            {
                if (_elements.Count == 1)
                {
                    var _value = _elements[0].GetValue(parameter as DependencyProperty);
                    return _value;
                }
                else
                {
                    object _value = _elements[0].GetValue(parameter as DependencyProperty);
                    for (int i = 1; i < _elements.Count; i++)
                    {
                        if (!_elements[i].GetValue(parameter as DependencyProperty).Equals(_value))
                        {
                            return (parameter as DependencyProperty).DefaultMetadata.DefaultValue;
                        }
                    }
                    return _value;
                }
            }
            return (parameter as DependencyProperty).DefaultMetadata.DefaultValue;
        }

        /// <summary>
        /// Hàm chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[2] { new BindingValue() { DependencyProperty = parameter as DependencyProperty, Value = value }, Binding.DoNothing };
        }
    }
}

﻿using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Lớp cung cấp các xử lý cho thao tác với tập tin
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Lấy mã MD5 cho file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetMd5(string fileName, out long fileSize)
        {

            if (fileName.StartsWith("pack://application:,,,")) { fileSize = 0; return string.Empty; }
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    fileSize = stream.Length;
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        /// <summary>
        /// Lấy MD5 của tập tin dựa vào Stream
        /// </summary>
        /// <param name="stream">Mảng giá trị truyền vào</param>
        /// <returns></returns>
        public static string GetMd5(Stream stream)
        {
            using (var md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
            }
        }

        /// <summary>
        /// Lấy thông tin tập tin mở rộng
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetExtension(string fileName)
        {
            if (fileName?.Contains(".") == true)
            {
                return fileName.Substring(fileName.LastIndexOf('.') + 1);
            }

            return string.Empty;
        }

        /// <summary>
        /// Lấy thông tin tên tập tin có bao gồm phần mở rộng
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetFileFullName(string filePath)
        {
            if (filePath.Contains("\\"))
            {
                return filePath.Substring(filePath.LastIndexOf('\\') + 1);
            }

            return string.Empty;
        }

        /// <summary>
        /// Lấy tên tập tin
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetFileName(string filePath)
        {
            var _fullName = GetFileFullName(filePath);
            if (_fullName != string.Empty && _fullName.Contains("."))
            {
                return _fullName.Substring(0, _fullName.LastIndexOf('.'));
            }

            return _fullName;
        }

        /// <summary>
        /// Lấy ảnh từ đối tượng
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="element">UI cần xuất</param>
        /// <param name="size">Kích thước</param>
        /// <returns></returns>
        public static bool SaveToImage(string imagePath, FrameworkElement element, Size imgSize = new Size())
        {
            if (string.IsNullOrEmpty(imagePath))
                return false;


            FileInfo fileInfo = new FileInfo(imagePath);
            
            try
            {
                Size size = new Size(element.ActualWidth, element.ActualHeight);
                // Get the size of canvas
                if (imgSize.Width == 0 || imgSize.Height == 0)
                    imgSize = size;

                // Measure and arrange the this
                // VERY IMPORTANT
                element.Measure(size);
                element.Arrange(new Rect(size));

                // Create a render bitmap and push the this to it
                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d,
                    96d,
                    PixelFormats.Pbgra32);
                renderBitmap.Render(element);
                TransformedBitmap transformedBitmap = new TransformedBitmap(renderBitmap, new ScaleTransform(imgSize.Width / size.Width, imgSize.Height / size.Height));

                // Create a file stream for saving image
                using (FileStream outStream = new FileStream(imagePath, FileMode.Create))
                {
                    BitmapEncoder encoder = null;
                    // Use png encoder for our data
                    switch (fileInfo.Extension.ToLower())
                    {
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                        case ".jpg":
                            encoder = new JpegBitmapEncoder();
                            break;
                        case ".bmp":
                            encoder = new BmpBitmapEncoder();
                            break;
                        default:
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                    // push the rendered bitmap to it
                    encoder.Frames.Add(BitmapFrame.Create(transformedBitmap));
                    // save the data to the stream
                    encoder.Save(outStream);
                    transformedBitmap = null;
                    renderBitmap = null;
                    encoder = null;
                }

                // Restore previously saved layout
                //        element.LayoutTransform = transform;

                return true;
            }
            catch (Exception e)
            {
                //    element.LayoutTransform = transform;
                return false;
            }
        }

        /// <summary>
        /// Lấy ảnh từ đối tượng bao gồm các các viền
        /// </summary>
        /// <param name="imagePath">Đường dẫn lưu trữ</param>
        /// <param name="element">UI cần xuất</param>
        /// <param name="size">Kích thước</param>
        /// <returns></returns>
        public static bool SaveToImageOriginSize(string imagePath, FrameworkElement element, Size size)
        {
            if (string.IsNullOrEmpty(imagePath))
                return false;


            FileInfo fileInfo = new FileInfo(imagePath);
            try
            {
                // Get the size of canvas
                if (size.Width == 0 || size.Height == 0)
                    size = new Size(element.ActualWidth, element.ActualHeight);

                // Measure and arrange the this
                // VERY IMPORTANT
                element.Measure(size);
                element.Arrange(new Rect(size));

                // Create a render bitmap and push the this to it
                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d,
                    96d,
                    PixelFormats.Pbgra32);
                renderBitmap.Render(element);

                // Create a file stream for saving image
                using (FileStream outStream = new FileStream(imagePath, FileMode.Create))
                {
                    BitmapEncoder encoder = null;
                    // Use png encoder for our data
                    switch (fileInfo.Extension.ToLower())
                    {
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                        case ".jpg":
                            encoder = new JpegBitmapEncoder();
                            break;
                        case ".bmp":
                            encoder = new BmpBitmapEncoder();
                            break;
                        default:
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                    // push the rendered bitmap to it
                    encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                    // save the data to the stream
                    encoder.Save(outStream);
                    renderBitmap = null;
                    encoder = null;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Lưu trữ hình ảnh đối tượng ra bitmap
        /// </summary>
        /// <param name="element"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static BitmapSource SaveToBitmap(FrameworkElement element, Size size = new Size())
        {
            try
            {
                double _width = element.ActualWidth > 0 ? element.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element.ActualHeight > 0 ? element.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                try
                {
                    renderBitmap.Render(element);
                }
                catch
                {

                }

                renderBitmap.Freeze();
                return renderBitmap;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Capture Layout
        /// </summary>
        /// <param name="element"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static BitmapSource CaptureLayout(LayoutBase element, Size size = new Size())
        {
            try
            {
                double _width = element.ActualWidth > 0 ? element.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element.ActualHeight > 0 ? element.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                element.Measure(size);
                element.Arrange(new Rect(size));
                element.UpdateLayout();

                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(element);
                    ctx.DrawRectangle(vb, null, new Rect(new Point(0, 0), new Size(_width, _height)));
                }
                renderBitmap.Render(dv);

                return renderBitmap;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Lấy bitmap có cả theme
        /// </summary>
        /// <param name="element1"></param>
        /// <param name="element2"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static BitmapSource SaveToBitmap(FrameworkElement element1, FrameworkElement element2, Size size = new Size())
        {
            try
            {
                double _width = element1.ActualWidth > 0 ? element1.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element1.ActualHeight > 0 ? element1.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                Rect _rect = new Rect(0, 0, _width, _height);

                RenderTargetBitmap renderBitmap1 =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                renderBitmap1.Render(element1);

                RenderTargetBitmap renderBitmap2 =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                renderBitmap2.Render(element2);

                DrawingVisual _visual = new DrawingVisual();
                using (var dc = _visual.RenderOpen())
                {
                    dc.DrawImage(renderBitmap2, _rect);
                    dc.DrawImage(renderBitmap1, _rect);
                }

                RenderTargetBitmap combien =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                combien.Render(_visual);
                renderBitmap1 = null;
                renderBitmap2 = null;


                var bitmapImage = new BitmapImage();
                var bitmapEncoder = new PngBitmapEncoder();
                bitmapEncoder.Frames.Add(BitmapFrame.Create(combien));

                using (var stream = new MemoryStream())
                {
                    bitmapEncoder.Save(stream);
                    stream.Seek(0, SeekOrigin.Begin);

                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = stream;
                    bitmapImage.DecodePixelWidth = (int)size.Width;
                    bitmapImage.DecodePixelHeight = (int)size.Height;
                    bitmapImage.EndInit();
                }
                combien = null;
                bitmapImage.Freeze();
                return bitmapImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Lưu ra ảnh, bao gồm cả theme
        /// </summary>
        /// <param name="fileUri"></param>
        /// <param name="element1"></param>
        /// <param name="element2"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static bool SaveToImage(string fileUri, FrameworkElement element1, FrameworkElement element2, Size size = new Size())
        {
            try
            {
                double _width = element1.ActualWidth > 0 ? element1.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element1.ActualHeight > 0 ? element1.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                Rect _rect = new Rect(0, 0, _width, _height);

                RenderTargetBitmap renderBitmap1 =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                renderBitmap1.Render(element1);

                RenderTargetBitmap renderBitmap2 =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                renderBitmap2.Render(element2);

                DrawingVisual _visual = new DrawingVisual();
                using (var dc = _visual.RenderOpen())
                {
                    dc.DrawImage(renderBitmap2, _rect);
                    dc.DrawImage(renderBitmap1, _rect);
                }

                RenderTargetBitmap combien =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                combien.Render(_visual);
                renderBitmap1 = null;
                renderBitmap2 = null;

                using (FileStream outStream = new FileStream(fileUri, FileMode.Create))
                {
                    FileInfo fileInfo = new FileInfo(fileUri);
                    BitmapEncoder encoder = null;
                    // Use png encoder for our data
                    switch (fileInfo.Extension.ToLower())
                    {
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                        case ".jpg":
                            encoder = new JpegBitmapEncoder();
                            break;
                        case ".bmp":
                            encoder = new BmpBitmapEncoder();
                            break;
                        default:
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                    // push the rendered bitmap to it
                    encoder.Frames.Add(BitmapFrame.Create(combien));
                    // save the data to the stream
                    encoder.Save(outStream);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Lưu group ra ảnh
        /// </summary>
        /// <param name="fileUri"></param>
        /// <param name="groupContainer"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static bool SaveToImage(string fileUri, GroupContainer groupContainer, Size size = new Size())
        {
            try
            {
                double _width = groupContainer.ActualWidth > 0 ? groupContainer.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = groupContainer.ActualHeight > 0 ? groupContainer.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);


                DrawingVisual _visual = new DrawingVisual();
                RenderTargetBitmap elementBitmap = null;
                elementBitmap = new RenderTargetBitmap((int)groupContainer.Elements[0].Width, (int)groupContainer.Elements[0].Height, 96d, 96d, PixelFormats.Pbgra32);
                elementBitmap.Render(groupContainer.Elements[0]);
                using (var dc = _visual.RenderOpen())
                {
                    Rect _rect2 = new Rect(0, 0, _width, _height);
                    dc.DrawRectangle(Brushes.Aqua, new Pen(), _rect2);
                    foreach (var item in groupContainer.Elements)
                    {
                        var _rect = item.GetBound(groupContainer);
                        item.Measure(new Size(_rect.Width, _rect.Height));
                        item.Arrange(new Rect(0, 0, _rect.Width, _rect.Height));
                        elementBitmap = new RenderTargetBitmap((int)item.Width, (int)item.Height, 96d, 96d, PixelFormats.Pbgra32);
                        elementBitmap.Render(item);
                        dc.DrawImage(elementBitmap, _rect);
                        item.Measure(new Size());
                        item.Arrange(new Rect());
                    }
                }
                RenderTargetBitmap combien =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                combien.Render(_visual);

                using (FileStream outStream = new FileStream(fileUri, FileMode.Create))
                {
                    FileInfo fileInfo = new FileInfo(fileUri);
                    BitmapEncoder encoder = null;
                    // Use png encoder for our data
                    switch (fileInfo.Extension.ToLower())
                    {
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                        case ".jpg":
                            encoder = new JpegBitmapEncoder();
                            break;
                        case ".bmp":
                            encoder = new BmpBitmapEncoder();
                            break;
                        default:
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                    // push the rendered bitmap to it
                    encoder.Frames.Add(BitmapFrame.Create(combien));
                    // save the data to the stream
                    encoder.Save(outStream);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static BitmapSource SaveToBitmapUsingVisual(FrameworkElement element, Size size = new Size())
        {
            try
            {
                double _width = element.Width > 0 ? element.Width : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element.Height > 0 ? element.Height : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                element.Measure(size);
                element.Arrange(new Rect(size));
                element.UpdateLayout();

                Rect _rect = new Rect(0, 0, _width, _height);

                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                renderBitmap.Render(element);

                DrawingVisual _visual = new DrawingVisual();
                using (var dc = _visual.RenderOpen())
                {
                    dc.DrawImage(renderBitmap, _rect);
                }

                renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                renderBitmap.Render(_visual);

                return renderBitmap;
            }
            catch
            {
                return null;
            }
        }


        public static BitmapSource SaveToThumbnailIconSmall(LayoutBase element, EColorManagment eColor, EFontfamily fontfamily, Size size = new Size())
        {
            try
            {
                double _width = element.ActualWidth > 0 ? element.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element.ActualHeight > 0 ? element.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                double _slideWidth = (Application.Current as IAppGlobal).SlideSize.Width;
                double _slideHeight = (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)_slideWidth,
                    (int)_slideWidth,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    ctx.DrawRectangle(Brushes.White, null, new Rect(new Point(10, 10), new Size(_slideWidth - 20, _slideWidth - 20)));

                    foreach (ObjectElement item in element.Elements)
                    {
                        if (!item.IsGraphicBackground && item.Visibility != Visibility.Hidden)
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                    }

                    ScaleTransform scale = new ScaleTransform();
                    scale.ScaleY = (_slideWidth / _slideHeight);
                    RenderTargetBitmap renderBitmap1 = new RenderTargetBitmap(
                   (int)_slideWidth - 20,
                   (int)_slideWidth - 20,
                   96d * size.Width / _width,
                   96d * size.Height / _height,
                   PixelFormats.Pbgra32);
                    renderBitmap1.Render(element);

                    ctx.PushTransform(scale);
                    ctx.DrawImage(renderBitmap1, new Rect(new Point(10, 10), new Size(_slideWidth - 20, _slideWidth - 20)));
                    ctx.Pop();

                    ctx.DrawLine(new Pen(Brushes.Black, 20), new Point(0, 10), new Point(_slideWidth, 10));
                    ctx.DrawLine(new Pen(Brushes.Black, 20), new Point(0, _slideWidth - 10), new Point(_slideWidth, _slideWidth - 10));
                    ctx.DrawLine(new Pen(Brushes.Black, 20), new Point(10, 0), new Point(10, _slideWidth));
                    ctx.DrawLine(new Pen(Brushes.Black, 20), new Point(_slideWidth - 10, 0), new Point(_slideWidth - 10, _slideWidth));



                    FormattedText formatted_text = new FormattedText("A", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(fontfamily.MajorFont), 450, Brushes.Black);
                    formatted_text.TextAlignment = TextAlignment.Center;
                    ctx.DrawText(formatted_text, new Point(300, 250));

                    formatted_text = new FormattedText("a", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(fontfamily.MinorFont), 450, Brushes.Black);
                    formatted_text.TextAlignment = TextAlignment.Center;
                    ctx.DrawText(formatted_text, new Point(310 + formatted_text.WidthIncludingTrailingWhitespace, 250));

                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent1.Color), null, new Rect(130, 720, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent2.Color), null, new Rect(260, 720, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent3.Color), null, new Rect(390, 720, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent4.Color), null, new Rect(520, 720, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent5.Color), null, new Rect(650, 720, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent6.Color), null, new Rect(780, 720, 120, 50));
                }
                renderBitmap.Render(dv);
                foreach (ObjectElement item in element.Elements)
                {
                    if (!item.IsGraphicBackground && item.Visibility == Visibility.Collapsed)
                    {
                        item.Visibility = Visibility.Visible;
                    }
                }
                return renderBitmap;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BitmapSource SaveToThumbnailIcon(LayoutBase element, EColorManagment eColor, EFontfamily fontfamily, Size size = new Size())
        {
            try
            {
                double _width = element.ActualWidth > 0 ? element.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element.ActualHeight > 0 ? element.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    ctx.DrawRectangle(Brushes.White, null, new Rect(new Point(0, 0), new Size(_width, _height)));
                    var _sType = typeof(StandardElement);
                    foreach (ObjectElement item in element.Elements)
                    {
                        if (!item.IsGraphicBackground && item.Visibility != Visibility.Hidden)
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                    }


                    RenderTargetBitmap renderBitmap1 = new RenderTargetBitmap(
                   (int)size.Width,
                   (int)size.Height,
                   96d * size.Width / _width,
                   96d * size.Height / _height,
                   PixelFormats.Pbgra32);
                    renderBitmap1.Render(element);

                    ctx.DrawImage(renderBitmap1, new Rect(new Point(0, 0), new Size(_width, _height)));

                    FormattedText formatted_text = new FormattedText("A", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(fontfamily.MajorFont), 150, Brushes.Black);
                    formatted_text.TextAlignment = TextAlignment.Center;
                    ctx.DrawText(formatted_text, new Point(180, 250));

                    formatted_text = new FormattedText("a", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(fontfamily.MinorFont), 150, Brushes.Black);
                    formatted_text.TextAlignment = TextAlignment.Center;
                    ctx.DrawText(formatted_text, new Point(185 + formatted_text.WidthIncludingTrailingWhitespace, 250));

                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent1.Color), null, new Rect(130, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent2.Color), null, new Rect(260, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent3.Color), null, new Rect(390, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent4.Color), null, new Rect(520, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent5.Color), null, new Rect(650, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent6.Color), null, new Rect(780, 420, 120, 50));
                }
                renderBitmap.Render(dv);
                foreach (ObjectElement item in element.Elements)
                {
                    if (!item.IsGraphicBackground && item.Visibility == Visibility.Collapsed)
                    {
                        item.Visibility = Visibility.Visible;
                    }
                }
                return renderBitmap;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BitmapSource SaveToThumbnailIcon(LayoutBase element, Size size = new Size())
        {
            try
            {
                double _width = element.ActualWidth > 0 ? element.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element.ActualHeight > 0 ? element.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    ctx.DrawRectangle(Brushes.White, null, new Rect(new Point(0, 0), new Size(_width, _height)));
                    var _sType = typeof(StandardElement);
                    foreach (ObjectElement item in element.Elements)
                    {
                        if (!item.IsGraphicBackground && item.Visibility != Visibility.Hidden)
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                    }


                    RenderTargetBitmap renderBitmap1 = new RenderTargetBitmap(
                   (int)size.Width,
                   (int)size.Height,
                   96d * size.Width / _width,
                   96d * size.Height / _height,
                   PixelFormats.Pbgra32);
                    renderBitmap1.Render(element);

                    ctx.DrawImage(renderBitmap1, new Rect(new Point(0, 0), new Size(_width, _height)));

                }
                renderBitmap.Render(dv);
                foreach (ObjectElement item in element.Elements)
                {
                    if (!item.IsGraphicBackground && item.Visibility == Visibility.Collapsed)
                    {
                        item.Visibility = Visibility.Visible;
                    }
                }
                return renderBitmap;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static BitmapSource SaveToThumbnailIcon(LayoutBase element, EColorManagment eColor, ColorBase colorBase, Size size = new Size())
        {
            try
            {
                double _width = element.ActualWidth > 0 ? element.ActualWidth : (Application.Current as IAppGlobal).SlideSize.Width;
                double _height = element.ActualHeight > 0 ? element.ActualHeight : (Application.Current as IAppGlobal).SlideSize.Height;

                if (size.Width == 0 || size.Height == 0)
                    size = new Size(_width, _height);

                element.Measure(size);
                element.Arrange(new Rect(size));
                element.UpdateLayout();

                RenderTargetBitmap renderBitmap =
                  new RenderTargetBitmap(
                    (int)size.Width,
                    (int)size.Height,
                    96d * size.Width / _width,
                    96d * size.Height / _height,
                    PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    if (colorBase != null)
                    {
                        ColorBrushBase colorBrushBase = ColorHelper.ConverFromColorData(colorBase);
                        ctx.DrawRectangle(colorBrushBase.Brush, null, new Rect(new Point(0, 0), new Size(_width, _height)));
                    }

                    var _sType = typeof(StandardElement);
                    foreach (ObjectElement item in element.Elements)
                    {
                        if (!item.IsGraphicBackground && item.Visibility != Visibility.Hidden)
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                    }


                    RenderTargetBitmap renderBitmap1 = new RenderTargetBitmap(
                   (int)size.Width,
                   (int)size.Height,
                   96d * size.Width / _width,
                   96d * size.Height / _height,
                   PixelFormats.Pbgra32);
                    renderBitmap1.Render(element);

                    ctx.DrawImage(renderBitmap1, new Rect(new Point(0, 0), new Size(_width, _height)));

                    FormattedText formatted_text = new FormattedText("A", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(""), 150, Brushes.Black);
                    formatted_text.TextAlignment = TextAlignment.Center;
                    ctx.DrawText(formatted_text, new Point(180, 250));

                    formatted_text = new FormattedText("a", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface(""), 150, Brushes.Black);
                    formatted_text.TextAlignment = TextAlignment.Center;
                    ctx.DrawText(formatted_text, new Point(185 + formatted_text.WidthIncludingTrailingWhitespace, 250));

                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent1.Color), null, new Rect(130, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent2.Color), null, new Rect(260, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent3.Color), null, new Rect(390, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent4.Color), null, new Rect(520, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent5.Color), null, new Rect(650, 420, 120, 50));
                    ctx.DrawRectangle((Brush)new BrushConverter().ConvertFromString(eColor.Accent6.Color), null, new Rect(780, 420, 120, 50));
                }
                renderBitmap.Render(dv);
                foreach (ObjectElement item in element.Elements)
                {
                    if (!item.IsGraphicBackground && item.Visibility == Visibility.Collapsed)
                    {
                        item.Visibility = Visibility.Visible;
                    }
                }
                return renderBitmap;

            }
            catch (Exception e)
            {
                return null;
            }
        }


        /// <summary>
        /// Lấy bitmap từ file ảnh
        /// </summary>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        public static BitmapSource GetBitmapSource(string imageFile)
        {
            if (imageFile == null || !File.Exists(imageFile)) return null;
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            Uri imageSource = new Uri(imageFile);
            image.UriSource = imageSource;
            image.EndInit();
            return image;
        }

        /// <summary>
        /// Tìm kiếm resource key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object FindResource(string key)
        {
            return Application.Current?.TryFindResource(key);
        }
    }
}

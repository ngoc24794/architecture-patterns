﻿using System;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// EventHanlder của sự kiện
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SelectedItemChangedEventHanlder(object sender, SelectedItemEventArgs e);

    /// <summary>
    /// Tham số cho sự kiện 
    /// </summary>
    public class SelectedItemEventArgs : EventArgs
    {
        public SelectedItemEventArgs(object newValue)
        {
            NewValue = newValue;
        }

        public object NewValue { get; set; }
    }
}

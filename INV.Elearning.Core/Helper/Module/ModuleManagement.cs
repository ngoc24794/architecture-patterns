﻿

namespace INV.Elearning.Core.Helper
{
    using INV.Elearning.Core.Model;
    using INV.Elearning.Core.ViewModel;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Quản lý các mô đun
    /// </summary>
    public class ModuleManagement
    {
        [ImportMany]
        public Lazy<ModuleBase, IDictionary<string, object>>[] AvailableModules { get; set; }

        /// <summary>
        /// Danh sách các module trong hệ thống
        /// </summary>
        public IEnumerable<ModuleBase> Modules
        {
            get
            {
                return this.AvailableModules.Select(x => x.Value);
            }
        }

        /// <summary>
        /// Danh sách các loại đối tượng được hỗ trợ
        /// </summary>
        public IEnumerable<Type> Types
        {
            get
            {
                List<Type> _result = new List<Type>();
                AvailableModules.Select(x => x.Value).Where(z => z.DataType != null).ToList()?.ForEach(x => { _result.Add(x.DataType); _result.AddRange(x.ChildTypes); });
                //var _result = AvailableModules.Select(x => x.Value).Where(z => z.DataType != null).Select(y => y.DataType).ToList();
                _result.Add(typeof(NormalPage));
                return _result;
            }
        }

        /// <summary>
        /// Lấy danh sách dữ liệu cho mô đun
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public IEnumerable<Type> GetTypes(ModuleLevel level)
        {
            List<Type> _result = new List<Type>();
            AvailableModules.Select(x => x.Value).Where(z => z.Level == level).ToList()?.ForEach(x => { if (x.DataType != null) _result.Add(x.DataType); });
            return _result;
        }
        
        /// <summary>
        /// Lấy các kiểu dữ liệu bé hơn dữ liệu level nhập vào
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public IEnumerable<Type> GetTypesLessThan(ModuleLevel level)
        {
            List<Type> _result = new List<Type>();
            AvailableModules.Select(x => x.Value).Where(z => z.Level < level).ToList()?.ForEach(x => { if (x.DataType != null) _result.Add(x.DataType); });
            return _result;
        }

        /// <summary>
        /// Lấy kiểu dữ liệu của từng trang
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<Type> GetTypesSupport(Type type)
        {
            List<Type> _result = new List<Type>();
            AvailableModules.Select(x => x.Value).Where(z => z.Level < ModuleLevel.Layout && z.ContainerType == type).ToList()?.ForEach(x => { if (x.DataType != null) _result.Add(x.DataType); });
            return _result;
        }

        /// <summary>
        /// Lấy kiểu dữ liệu chung dựa vào Level
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private Type GetDataTypeByLevel(ModuleLevel level)
        {
            switch (level)
            {
                case ModuleLevel.Element:
                    return typeof(ObjectElementBase);
                case ModuleLevel.Layout:
                    return typeof(PageLayer);
                case ModuleLevel.Slide:
                    return typeof(PageElementBase);
                default:
                    return typeof(ObjectElementBase);
            }
        }

        /// <summary>
        /// Thêm các thành phần-PlugIn vào chương trình
        /// </summary>
        public void AssembleComponents()
        {
            try
            {
                //Tạo danh sách chứa các thực thể của PlugIn
                var aggregateCatalog = new AggregateCatalog();

                //Tải các thành phần từ các thư viện đã đóng gói
                var directoryCatalog = new DirectoryCatalog(Global.PLUGIN_FOLDER, "*.dll");

                //Tải các thành phần được reference trực tiếp vào Project
                var asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.ImageProcess"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.Text"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.Charts"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.VideoElementControl"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.Controls.Audio"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.Controls.Flash"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.Quizz"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.DesignControl"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.ELearning.Trigger"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.ELearning.Player"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.Animations.UI"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                asmCatalog = new AssemblyCatalog(Assembly.Load("INV.Elearning.YoutubeVideo"));
                //Thêm các thành phần vào danh sách chứa
                aggregateCatalog.Catalogs.Add(asmCatalog);

                //Tạo một đối tượng chứa các danh sách thực thể
                var container = new CompositionContainer(aggregateCatalog);

                //Gán vào cho lớp quản lý, sau khi chạy dòng lệnh này, các Plugin được đưa trực tiếp vào Thuộc tính AvailableModules
                container.ComposeParts(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Lấy mô đun dựa vào kiểu dữ liệu cung cấp bởi mô đun
        /// </summary>
        /// <param name="type">Kiểu dữ liệu cung cấp bởi mô đun</param>
        /// <returns></returns>
        internal ModuleBase GetModule(Type type)
        {
            foreach (var item in AvailableModules)
            {
                if ((item.Metadata["ModuleMetadata"] as Type)?.IsAssignableFrom(type) == true)
                {
                    return item.Value;
                }
            }

            return null;
        }

        /// <summary>
        /// Lấy mô đun dựa vào kiểu dữ liệu lưu trữ cung cấp bởi mô đun
        /// </summary>
        /// <param name="type">Kiểu dữ liệu lưu trữ cung cấp bởi mô đun</param>
        /// <returns></returns>
        internal ModuleBase GetModuleByData(Type type)
        {
            foreach (var item in AvailableModules)
            {
                if (item.Value.DataType?.IsAssignableFrom(type) == true)
                {
                    return item.Value;
                }
            }

            return null;
        }
    }
}

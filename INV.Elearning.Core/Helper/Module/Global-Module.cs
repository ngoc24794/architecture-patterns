﻿

namespace INV.Elearning.Core.Helper
{
    using INV.Elearning.Core.View;
    using INV.Elearning.Core.ViewModel;
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// Lớp chứa các giá trị dùng chung cho cả chương trình phần quản lý Mô đun
    /// </summary>
    public static partial class Global
    {
        public const string PLUGIN_FOLDER = @""; //Đường dẫn thư mục chứa các tập tin PlugIn

        /// <summary>
        /// Quản lý mô đun
        /// </summary>
        private static ModuleManagement _moduleManagement = null;

        /// <summary>
        /// Lấy quản lý mô đun hiện tại
        /// </summary>
        public static ModuleManagement ModuleManagement
        {
            get
            {
                if (_moduleManagement == null)
                {
                    _moduleManagement = new ModuleManagement();
                    _moduleManagement.AssembleComponents();
                }

                return _moduleManagement;
            }
        }

        /// <summary>
        /// Lấy mô đun dựa vào kiểu dữ liệu mô đun cung cấp
        /// </summary>
        /// <param name="type">Kiểu dữ liệu</param>
        /// <returns></returns>
        public static ModuleBase GetModule(Type type)
        {
            return ModuleManagement.GetModule(type);
        }

        /// <summary>
        /// Lấy mô đun dựa vào kiểu dữ liệu lưu trữ mô đun cung cấp
        /// </summary>
        /// <param name="type">Kiểu dữ liệu lưu trữ</param>
        /// <returns></returns>
        public static ModuleBase GetModuleByData(Type type)
        {
            return ModuleManagement.GetModuleByData(type);
        }

        private static ObservableCollection<IShapeFormatControl> _moduleFormats;
        /// <summary>
        /// Danh sách các control định dạng khung
        /// </summary>
        public static ObservableCollection<IShapeFormatControl> ModuleFormats
        {
            get { return _moduleFormats ?? (_moduleFormats = new ObservableCollection<IShapeFormatControl>()); }
        }

        /// <summary>
        /// Lấy dữ liệu từ tập tin
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static void DropExecute(string fileName)
        {
            if ((Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout == null) return; //Nếu không có dữ liệu layout đang được chọn thì thoát
            FileInfo fileInfo = new FileInfo(fileName);
            ModuleBase module = ModuleManagement.AvailableModules.FirstOrDefault(x => x.Value.ExtensionFiles.Contains(fileInfo.Extension.ToLower()))?.Value;
            if (module != null)
            {
                ObjectElement element = module.ObjectFromFile(fileName);
                if (element != null)
                {
                    (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Add(element);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper
{
    public static class AppHelper
    {
        public static IEnumerable<EFontFamily> SystemFonts
        {
            get
            {
                string fontsfolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Fonts);
                var _fonts = Directory.GetFiles(fontsfolder).Select(x => new FileInfo(x)).Where(x => x.Extension.ToLower() == ".ttf" || x.Extension.ToLower() == ".otf");
                List<EFontFamily> _result = new List<EFontFamily>();
                foreach (var item in _fonts)
                {
                    _result.Add(new EFontFamily() { FontFamily = new FontFamily(item.FullName), IsTrueTypeface = item.Extension == ".ttf" });
                }
                return _result;
            }
        }
    }

    public class EFontFamily
    {
        public FontFamily FontFamily { get; set; }
        public bool IsTrueTypeface { get; set; }

    }
}

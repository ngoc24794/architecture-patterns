﻿using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearning.Controls.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper
{
    public class ShapeHelper
    {
        private static ObservableCollection<ShapeItemInfo> _shapes;
        /// <summary>
        /// Danh sách các hình vẽ
        /// </summary>
        public static ObservableCollection<ShapeItemInfo> Shapes
        {
            get { return _shapes ?? (_shapes = GetShapes()); }
        }

        private static ObservableCollection<ShapeItemInfo> GetShapes()
        {
            ObservableCollection<ShapeItemInfo> _result = new ObservableCollection<ShapeItemInfo>();
            #region Lines
            string[] lines = { "Line", "Arrow", "Double Arow", "Elbow Connector", "Elbow Arrow Connector", "Elbow Double Arrow Connector",
                "Curve Connector", "Curved Arrow Connector", "Curved Double Arrow Connector",
                "Curve", "Freeform", "Scribble" };
            string[] lineKeys = {
                ShapeNames.Line, ShapeNames.Arrow, ShapeNames.DoubleArrow, ShapeNames.ElbowConnector, ShapeNames.ElbowArrowConnnector, ShapeNames.ElbowDoubleArrowConnector,
                ShapeNames.CurvedConnector, ShapeNames.CurvedArrowConnector, ShapeNames.CurvedDoubleArrowConnector,
                ShapeNames.Curve, ShapeNames.Freeform, ShapeNames.Scribble
            };
            for (int i = 0; i < lines.Length; i++)
            {
                var item = lines[i];
                string
                    key = lineKeys[i],
                    value = "Line";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    Name = AddResource(key, value)?.ToString(),
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Lines/{item}.png",
                    GroupName = AddResource("ShapeGroup_Lines")?.ToString(),
                    Type = (ShapeType)Enum.Parse(typeof(ShapeType), item.Replace(" ", ""))
                };
                _result.Add(_info);
            }
            #endregion



            #region Rectangles
            string[] rectangles = {"Rectangle", "Rounded Rectangle", "Snip Single Corner Rectangle", "Snip Same Side Corner Rectangle", "Snip Diagonal Corner Rectangle",
                "Snip And Round Single Corner Rectangle", "Round Single Corner Rectangle", "Round Same Side Corner Rectangle",  "Round Diagonal Corner Rectangle"};

            string[] rectangleKeys = {
                ShapeNames.Rectangle, ShapeNames.RoundedRectangle, ShapeNames.SnipSingleCornerRectangle, ShapeNames.SnipSameSideCornerRectangle, ShapeNames.SnipDiagonalCornerRectangle,
                ShapeNames.SnipAndRoundSingleCornerRectangle, ShapeNames.RoundSingleCornerRectangle, ShapeNames.RoundSameSideCornerRectangle,  ShapeNames.RoundDiagonalCornerRectangle
            };
            for (int i = 0; i < rectangles.Length; i++)
            {
                var item = rectangles[i];
                string
     key = rectangleKeys[i],
     value = "Rectangle";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Rectangle/{item}.png",
                    Name = AddResource(key, value)?.ToString(),
                    GroupName = AddResource("ShapeGroup_Rectangles")?.ToString(),
                    Type = (ShapeType)Enum.Parse(typeof(ShapeType), item.Replace(" ", ""))
                };
                _result.Add(_info);
            }
            #endregion

            #region Basic Shapes
            string[] basics = {"Text", "Oval", "Triangle", "Right Triangle", "Parallelogram", "Trapezoid", "Diamond", "Regular Pentagon", "Hexagon",
              "Heptagon", "Octagon", "Decagon", "Dodecagon", "Pie", "Chord", "Frame", "Half Frame", "L-Shape", "Diagonal Stripe",
              "Cross", "Plaque", "Can", "Cube", "Bevel", "Donut", "\"No\" Symbol", "Block Arc", "Folded Corner", "Smiley Face", "Heart", "Lightning Bolt", "Sun", "Moon","Cloud", "Arc", "Checkmark",
              "Double Bracket", "Double Brace", "Left Brace", "Right Brace", "Left Bracket", "Right Bracket"  };
            string[] basicKeys = {
                ShapeNames.Text, ShapeNames.Oval, ShapeNames.Triangle, ShapeNames.RightTriangle, ShapeNames.Parallelogram, ShapeNames.Trapezoid, ShapeNames.Diamond,
                ShapeNames.RegularPentagon, ShapeNames.Hexagon,   ShapeNames.Heptagon, ShapeNames.Octagon, ShapeNames.Decagon, ShapeNames.Dodecagon, ShapeNames.Pie,
                ShapeNames.Chord, ShapeNames.Frame, ShapeNames.HalfFrame, ShapeNames.LShape, ShapeNames.DiagonalStripe, ShapeNames.Cross, ShapeNames.Plaque, ShapeNames.Can,
                ShapeNames.Cube, ShapeNames.Bevel, ShapeNames.Donut, ShapeNames.NoSymbol, ShapeNames.BlockArc, ShapeNames.FoldedCorner, ShapeNames.Smile, ShapeNames.Heart,
                ShapeNames.LightnightBolt, ShapeNames.Sun, ShapeNames.Moon, ShapeNames.Cloud, ShapeNames.BlockArc, ShapeNames.Checkmark,
                ShapeNames.DoubleBracket, ShapeNames.DoubleBrace, ShapeNames.LeftBrace,ShapeNames.RightBrace, ShapeNames.LeftBracket, ShapeNames.RightBracket
            };
            for (int i = 0; i < basics.Length; i++)
            {
                var item = basics[i];
                string key = basicKeys[i], value = "Shape";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Basic/{item.Replace("\"", "")}.png",
                    Name = AddResource(key, value)?.ToString(),
                    GroupName = AddResource("ShapeGroup_BasicShapes")?.ToString(),
                    Type = (ShapeType)Enum.Parse(typeof(ShapeType), item.Replace(" ", "").Replace("\"", "").Replace("-", ""))
                };
                _result.Add(_info);
            }
            #endregion

            #region Arrows

            string[] arows = { "Right Arrow", "Left Arrow", "Down Arrow", "Up Arrow", "Left-Right Arrow", "Up-Down Arrow", "Striped Right Arrow", "Notched Right Arrow", "Pentagon", "Chevron",
                                "Quad Arrow", "Left-Right-Up Arrow", "Left-Up Arrow", "Bent-Up Arrow", "U-Turn Arrow", "Bent Arrow", "Down Arrow Callout", "Left Arrow Callout", "Right Arrow Callout", "Up Arrow Callout","Left-Right Arrow Callout", "Quad Arrow Callout"};

            string[] arrowKeys = {
                ShapeNames.RightArrow, ShapeNames.LeftArrow, ShapeNames.DownArrow, ShapeNames.UpArrow, ShapeNames.LeftRightArrow, ShapeNames.UpDownArrow,
                ShapeNames.StripedRightArrow, ShapeNames.NotchedRightArrowArrow, ShapeNames.Pentagon, ShapeNames.Chevron,
                ShapeNames.QuadArrow, ShapeNames.LeftRightUpArrow, ShapeNames.LeftUpArrow, ShapeNames.BentUpArrow, ShapeNames.UTurnArrow, ShapeNames.BentArrow,
                ShapeNames.DownArrowCallout, ShapeNames.LeftArrowCallout, ShapeNames.RightArrowCallout, ShapeNames.UpArrowCallout, ShapeNames.LeftRightArrowCallout, ShapeNames.QuadArrowCallout};

            for (int i = 0; i < arows.Length; i++)
            {
                string item = arows[i];
                string
     key = arrowKeys[i],
     value = "Arrow";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Arrow/{item}.png",
                    Name = AddResource(key, value)?.ToString(),
                    GroupName = AddResource("ShapeGroup_BlockArrows")?.ToString(),
                    Type = (ShapeType)Enum.Parse(typeof(ShapeType), item.Replace(" ", "").Replace("-", ""))
                };
                _result.Add(_info);
            }
            #endregion

            #region Equation Shapes

            string[] equations = { "Plus", "Minus", "Multifly", "Division", "Equal", "Not Equal", };
            string[] equationKeys = { ShapeNames.Plus, ShapeNames.Minus, ShapeNames.Multiply, ShapeNames.Division, ShapeNames.Equal, ShapeNames.NotEqual };

            for (int i = 0; i < equations.Length; i++)
            {
                string item = equations[i];
                string
     key = equationKeys[i],
     value = "Operator";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Equation/{item}.png",
                    Name = AddResource(key, value)?.ToString(),
                    GroupName = AddResource("ShapeGroup_EquationShapes")?.ToString(),
                    Type = (ShapeType)Enum.Parse(typeof(ShapeType), item.Replace(" ", ""))
                };
                _result.Add(_info);
            }
            #endregion

            #region Flowcharts
            string[] flowcharts = {"Process", "Alternate Process", "Decision", "Data", "Predefined Process", "Internal Storage", "Document", "Multidocument", "Terminator", "Preparation",
                                    "Manual Input","Manual Operation","Connector","Off-page Connector","Card","Punched Tape","Summing Junction","Or","Collate","Sort",
                                    "Extract","Merge","Stored Data","Delay","Sequencial Access Storage","Magnetic Disk","Direct Access Storage","Display"};
            string[] flowchartKeys = {
                ShapeNames.Process, ShapeNames.AlternateProcess, ShapeNames.Decision, ShapeNames.Data, ShapeNames.PredefinedProcess, ShapeNames.InternalStorage,
                ShapeNames.Document, ShapeNames.Multidocument, ShapeNames.Terminator, ShapeNames.Preparation, ShapeNames.ManualInput, ShapeNames.ManualOperation,
                ShapeNames.Connector, ShapeNames.OffpageConnector, ShapeNames.Card, ShapeNames.PunchedTape, ShapeNames.SummingJunction, ShapeNames.Or, ShapeNames.Collate, ShapeNames.Sort,
                ShapeNames.Extract, ShapeNames.Merge, ShapeNames.StoredData, ShapeNames.Delay, ShapeNames.SequencialAccessStorage, ShapeNames.MagneticDisk, ShapeNames.DirectAccessStorage, ShapeNames.Display};
            for (int i = 0; i < flowcharts.Length; i++)
            {
                string item = flowcharts[i];
                string
     key = flowchartKeys[i],
     value = "Flow chart";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Flowchart/{item}.png",
                    Name = AddResource(key, value)?.ToString(),
                    GroupName = AddResource("ShapeGroup_Flowchart")?.ToString(),
                    Type = (ShapeType)Enum.Parse(typeof(ShapeType), item.Replace(" ", "").Replace("-", ""))
                };
                _result.Add(_info);
            }
            #endregion

            #region Stars

            string[] stars = { "Explosion 1", "Explosion 2", "4-Point Star", "5-Point Star", "6-Point Star", "7-Point Star",
                "8-Point Star", "10-Point Star", "12-Point Star", "16-Point Star", "24-Point Star", "32-Point Star" };
            ShapeType[] starType = {ShapeType.Explosion1, ShapeType.Explosion2, ShapeType.PointStar4, ShapeType.PointStar5, ShapeType.PointStar6, ShapeType.PointStar7,
                    ShapeType.PointStar8, ShapeType.PointStar10, ShapeType.PointStar12, ShapeType.PointStar16, ShapeType.PointStar24, ShapeType.PointStar32};
            string[] starKeys = {
                ShapeNames.Explosion1, ShapeNames.Explosion2, ShapeNames.Star, ShapeNames.Star, ShapeNames.Star, ShapeNames.Star,
                ShapeNames.Star, ShapeNames.Star, ShapeNames.Star, ShapeNames.Star, ShapeNames.Star, ShapeNames.Star };
            ShapeType[] starTypes = {ShapeType.Explosion1, ShapeType.Explosion2, ShapeType.PointStar4, ShapeType.PointStar5, ShapeType.PointStar6, ShapeType.PointStar7,
                    ShapeType.PointStar8, ShapeType.PointStar10, ShapeType.PointStar12, ShapeType.PointStar16, ShapeType.PointStar24, ShapeType.PointStar32};

            for (int i = 0; i < stars.Length; i++)
            {
                var item = stars[i];
                string key = starKeys[i], value = "Star";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Star/{item}.png",
                    Name = AddResource(key, value)?.ToString(),
                    GroupName = AddResource("ShapeGroup_Stars")?.ToString(),
                    Type = starTypes[i]
                };
                _result.Add(_info);
            }
            #endregion

            #region Callouts

            string[] callouts = {"Rectangular Callout","Rounded Rectangular Callout","Oval Callout","Cloud Callout","Line Callout 1","Line Callout 2","Line Callout 3","Line Callout 1 (Accent Bar)",
                "Line Callout 2 (Accent Bar)","Line Callout 3 (Accent Bar)","Line Callout 1 (No Border)","Line Callout 2 (No Border)","Line Callout 3 (No Border)",
                "Line Callout 1 (Border And Accent Bar)","Line Callout 2 (Border And Accent Bar)","Line Callout 3 (Border And Accent Bar)"};
            string[] calloutKeys = {
                ShapeNames.RectangularCallout, ShapeNames.RoundedRectangularCallout, ShapeNames.OvalCallout, ShapeNames.CloudCallout,
                ShapeNames.LineCallout1, ShapeNames.LineCallout2, ShapeNames.LineCallout3, ShapeNames.LineCallout1AccentBar,
                ShapeNames.LineCallout2AccentBar, ShapeNames.LineCallout3AccentBar, ShapeNames.LineCallout1NoBorder, ShapeNames.LineCallout2NoBorder, ShapeNames.LineCallout3NoBorder,
                ShapeNames.LineCallout1BorderAccentBar, ShapeNames.LineCallout2BorderAccentBar, ShapeNames.LineCallout3BorderAccentBar};

            for (int i = 0; i < callouts.Length; i++)
            {
                string item = callouts[i];
                string
    key = calloutKeys[i],
    value = "Callout";
                ShapeItemInfo _info = new ShapeItemInfo
                {
                    ImageUri = $"/INV.Elearning.Resources;component/Resources/Images/Shapes/Callout/{item}.png",
                    Name = AddResource(key, value)?.ToString(),
                    GroupName = AddResource("ShapeGroup_Callouts")?.ToString(),
                    Type = (ShapeType)Enum.Parse(typeof(ShapeType), item.Replace(" ", "").Replace("(", "").Replace(")", ""))
                };
                _result.Add(_info);
            }
            #endregion
            return _result;

        }

        /// <summary>
        /// Thêm resource
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static object AddResource(string key, object value = null)
        {
            return Application.Current.TryFindResource(key);
        }

        private static List<DashModel> _dashTypes;
        /// <summary>
        /// Danh sách các loại Dash
        /// </summary>
        public static List<DashModel> DashTypes
        {
            get { return _dashTypes ?? (_dashTypes = GetDashes()); }
        }

        /// <summary>
        /// Lấy danh sách các loại dash
        /// </summary>
        /// <returns></returns>
        private static List<DashModel> GetDashes()
        {
            List<DashModel> _result = new List<DashModel>();

            _result.Add(new DashModel() { Name = "Solid", DashType = DashType.Solid, StrokeDashCap = PenLineCap.Flat });
            _result.Add(new DashModel() { Name = "Round Dot", DashType = DashType.RoundDot, StrokeDashCap = PenLineCap.Triangle, Dashes = new DoubleCollection() { 0, 2 } });
            _result.Add(new DashModel() { Name = "Square Dot", DashType = DashType.SquareDot, StrokeDashCap = PenLineCap.Flat, Dashes = new DoubleCollection() { 1, 1 } });
            _result.Add(new DashModel() { Name = "Dash", DashType = DashType.Dash, StrokeDashCap = PenLineCap.Square, Dashes = new DoubleCollection() { 4, 4 } });
            _result.Add(new DashModel() { Name = "Dash Dot", DashType = DashType.DashDot, StrokeDashCap = PenLineCap.Square, Dashes = new DoubleCollection() { 4, 4, 1, 4 } });
            _result.Add(new DashModel() { Name = "Long Dash", DashType = DashType.LongDash, StrokeDashCap = PenLineCap.Square, Dashes = new DoubleCollection() { 6, 4 } });
            _result.Add(new DashModel() { Name = "Long Dash Dot", DashType = DashType.LongDashDot, StrokeDashCap = PenLineCap.Square, Dashes = new DoubleCollection() { 6, 4, 1, 4 } });
            _result.Add(new DashModel() { Name = "Long Dash Dot Dot", DashType = DashType.LongDashDotDot, StrokeDashCap = PenLineCap.Square, Dashes = new DoubleCollection() { 6, 4, 1, 4, 1, 4 } });

            return _result;
        }

        /// <summary>
        /// Chuyển đổi kiểu nét dứt
        /// </summary>
        /// <param name="dash"></param>
        /// <returns></returns>
        public static DashType DashConverter(Model.DashType dash)
        {
            switch (dash)
            {
                case Model.DashType.Solid:
                    return DashType.Solid;
                case Model.DashType.RoundDot:
                    return DashType.RoundDot;
                case Model.DashType.SquareDot:
                    return DashType.SquareDot;
                case Model.DashType.Dash:
                    return DashType.Dash;
                case Model.DashType.DashDot:
                    return DashType.DashDot;
                case Model.DashType.LongDash:
                    return DashType.LongDash;
                case Model.DashType.LongDashDot:
                    return DashType.LongDashDot;
                case Model.DashType.LongDashDotDot:
                    return DashType.LongDashDotDot;
                default:
                    return DashType.Solid;
            }
        }

        /// <summary>
        /// Chuyển đổi kiểu nét đứt
        /// </summary>
        /// <param name="dash"></param>
        /// <returns></returns>
        public static Model.DashType DashConverter(DashType dash)
        {
            switch (dash)
            {
                case DashType.Solid:
                    return Model.DashType.Solid;
                case DashType.RoundDot:
                    return Model.DashType.RoundDot;
                case DashType.SquareDot:
                    return Model.DashType.SquareDot;
                case DashType.Dash:
                    return Model.DashType.Dash;
                case DashType.DashDot:
                    return Model.DashType.DashDot;
                case DashType.LongDash:
                    return Model.DashType.LongDash;
                case DashType.LongDashDot:
                    return Model.DashType.LongDashDot;
                case DashType.LongDashDotDot:
                    return Model.DashType.LongDashDotDot;
                default:
                    return Model.DashType.Solid;
            }
        }
    }

    /// <summary>
    /// Lớp lưu trữ thông tin của kiểu nét
    /// </summary>
    public class DashModel
    {
        public DoubleCollection Dashes { get; set; }
        public PenLineCap StrokeDashCap { get; set; }
        public DashType DashType { get; set; }
        public string Name { set; get; }
    }

    /// <summary>
    /// Lớp lưu trữ thông tin hình ảnh
    /// </summary>
    public class ShapeItemInfo
    {
        /// <summary>
        /// Đường dẫn chứa hình ảnh
        /// </summary>
        public string ImageUri { get; set; }
        /// <summary>
        /// Tên hình
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Loại hình
        /// </summary>
        public ShapeType Type { get; set; }
        /// <summary>
        /// Tên nhóm
        /// </summary>
        public string GroupName { get; set; }
    }
}

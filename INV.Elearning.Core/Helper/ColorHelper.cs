﻿using INV.Elearning.Core.Model;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper
{
    public class ColorHelper
    {
        /// <summary>
        /// Chuyển đổi màu sắc
        /// </summary>
        /// <param name="colorData"></param>
        /// <returns></returns>
        public static ColorBrushBase ConverFromColorData(ColorBase colorData)
        {
            if(colorData is SolidColor)
            {
                return SolidColor.Converter(colorData as SolidColor);
            }
            else if (colorData is GradientColor)
            {
                return GradientColor.Converter(colorData as GradientColor);
            }
            else if (colorData is ImageColor)
            {
                return ImageColor.Converter(colorData as ImageColor);
            }
            else if (colorData is PatternColor)
            {
                return PatternColor.Converter(colorData as PatternColor);
            }

            return null;
        }

        /// <summary>
        /// Lấy màu sắc từ màu hiển thị
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static ColorBase ConverterFromColor(ColorBrushBase color)
        {
            if(color is ColorSolidBrush)
            {
                return SolidColor.Converter(color as ColorSolidBrush);
            }
            else if (color is ColorGradientBrush)
            {
                return GradientColor.Converter(color as ColorGradientBrush);
            }
            else if (color is ColorTextureBrush)
            {
                return ImageColor.Converter(color as ColorTextureBrush);
            }
            else if (color is ColorPatternBrush)
            {
                return PatternColor.Converter(color as ColorPatternBrush);
            }

            return null;
        }

        /// <summary>
        /// Returns brightness of the given color from 0..1
        /// </summary>
        /// <param name="color">Color</param>
        /// <returns>Brightness of the given color from 0..1</returns>
        static double GetBrightness(Color color)
        {
            double summ = ((double)color.R) + ((double)color.G) + ((double)color.B);
            return summ / (255.0 * 3.0);
        }

        /// <summary>
        /// Làm sáng màu
        /// </summary>
        /// <param name="color"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static Color Lighter(Color color, double power)
        {
            double totalAvailability = 255.0 * 3.0 - (double)color.R + (double)color.G + (double)color.B;
            double redAvailability;
            double greenAvailability;
            double blueAvailability;
            double needToBeAdded;

            if (color.R + color.G + color.B == 0)
            {
                redAvailability = 1.0 / 3.0;
                greenAvailability = 1.0 / 3.0;
                blueAvailability = 1.0 / 3.0;
                needToBeAdded = power * 255.0 * 3.0;
            }
            else
            {
                redAvailability = (255.0 - (double)color.R) / totalAvailability;
                greenAvailability = (255.0 - (double)color.G) / totalAvailability;
                blueAvailability = (255.0 - (double)color.B) / totalAvailability;
                needToBeAdded = ((double)color.R + (double)color.G + (double)color.B) * (power - 1);
            }


            Color result = Color.FromRgb(
                (byte)(color.R + ((byte)(redAvailability * needToBeAdded))),
                (byte)(color.G + ((byte)(greenAvailability * needToBeAdded))),
                (byte)(color.B + ((byte)(blueAvailability * needToBeAdded))));

            return result;
        }

        /// <summary>
        /// Làm tối màu
        /// </summary>
        /// <param name="color"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static Color Darker(Color color, double power)
        {
            double totalAvailability = (double)color.R + (double)color.G + (double)color.B;
            double redAvailability = ((double)color.R) / totalAvailability;
            double greenAvailability = ((double)color.G) / totalAvailability;
            double blueAvailability = ((double)color.B) / totalAvailability;

            double needToBeAdded = ((double)color.R + (double)color.G + (double)color.B);
            needToBeAdded = needToBeAdded - needToBeAdded * power;

            Color result = Color.FromRgb(
                (byte)(color.R - ((byte)(redAvailability * needToBeAdded))),
                (byte)(color.G - ((byte)(greenAvailability * needToBeAdded))),
                (byte)(color.B - ((byte)(blueAvailability * needToBeAdded))));

            return result;
        }

        /// <summary>
        /// Tạo màu mới với độ sáng thay đổi
        /// </summary>
        /// <param name="color"></param>
        /// <param name="newBrightness"></param>
        /// <returns></returns>
        public static Color Rebright(Color color, double correctionFactor)
        {
            //if (power > 1.0)
            //    return Lighter(color, power);
            //else
            //    return Darker(color, power);
            double red = (double)color.R;
            double green = (double)color.G;
            double blue = (double)color.B;

            if (correctionFactor < 0)
            {
                correctionFactor = 1 + correctionFactor;
                red *= correctionFactor;
                green *= correctionFactor;
                blue *= correctionFactor;
            }
            else
            {
                red = (255 - red) * correctionFactor + red;
                green = (255 - green) * correctionFactor + green;
                blue = (255 - blue) * correctionFactor + blue;
            }

            return Color.FromArgb(color.A, (byte)red, (byte)green, (byte)blue);
        }

        public static string GetImagePattern()
        {
            return string.Empty;
        }
    }
}

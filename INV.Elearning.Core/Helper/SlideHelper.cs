﻿using INV.Elearning.Core.View;
using System.Linq;
using System.Windows;

namespace INV.Elearning.Core.Helper
{
    public class SlideHelper
    {
        /// <summary>
        /// Huy bo cac slide dang duoc chon
        /// </summary>
        public static void UnSlectedAll()
        {
            SlideBase _lastValue = null;
            while ((Application.Current as IAppGlobal).SelectedSlides.Count > 0)
            {
                if (_lastValue != null && _lastValue == (Application.Current as IAppGlobal).SelectedSlides[0]) return;
                _lastValue = (Application.Current as IAppGlobal).SelectedSlides[0];
                (Application.Current as IAppGlobal).SelectedSlides[0].IsSelected = false;
            }
        }

        /// <summary>
        /// hủy bỏ lựa chọn tất cả slide
        /// </summary>
        public static void UnSelectedAllSlide()
        {
            foreach (var item in (Application.Current as IAppGlobal).DocumentControl.Slides)
            {
                item.IsSelected = false;
            }
        }
    }
}

﻿using INV.Elearning.Animations;
using INV.Elearning.Controls;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.ObjectElement;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper
{
    public class ObjectElementsHelper
    {
        #region Editor Commands

        /// <summary>
        /// Hủy lựa chọn tất cả các đối tượng đang được chọn
        /// </summary>
        public static void UnSelectedAll()
        {
            int count = (Application.Current as IAppGlobal).SelectedElements.Count;
            while ((Application.Current as IAppGlobal)?.SelectedElements.Count != 0) //Hủy tất cả các lựa chọn khác
            {
                (Application.Current as IAppGlobal).SelectedElements[0].IsSelected = false;
                if (count == (Application.Current as IAppGlobal).SelectedElements.Count) break;
                count = (Application.Current as IAppGlobal).SelectedElements.Count;
            }

            (Application.Current as IAppGlobal).SelectedElements.Clear();

            if ((Application.Current as IAppGlobal).SelectedSlide?.SelectedLayout != null)
                foreach (var item in (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Children)
                {
                    if (item is ObjectElement element) element.IsSelected = false;
                }
        }

        /// <summary>
        /// Lấy chỉ số ZIndex lớn nhất hiện tại
        /// </summary>
        /// <returns></returns>
        public static int GetMaxZIndex()
        {
            if ((Application.Current as IAppGlobal)?.SelectedSlide?.SelectedLayout?.Elements.Count > 0)
            {
                return (Application.Current as IAppGlobal).SelectedSlide.SelectedLayout.Elements.Max(x => x.ZIndex);
            }

            return 0;
        }

        /// <summary>
        /// Hủy lựa chọn tất cả các đối tượng đang được chọn. Trừ những đối tượng nhập vào
        /// </summary>
        /// <param name="elements"></param>
        public static void UnSelectedAll(params ObjectElement[] elements)
        {
            for (int i = 0; i < (Application.Current as IAppGlobal).SelectedElements.Count; i++)
            {
                if (!elements.Contains((Application.Current as IAppGlobal).SelectedElements[i]))
                {
                    (Application.Current as IAppGlobal).SelectedElements[i].IsSelected = false;
                    i--;
                }
            }
        }

        /// <summary>
        /// Lựa chọn tất cả các đối tượng thuộc Layout hiện tại
        /// </summary>
        public static void SellectedAll()
        {
            foreach (ObjectElement item in (Application.Current as IAppGlobal)?.SelectedSlide.SelectedLayout.Elements)
            {
                item.IsSelected = true;
            }
        }

        /// <summary>
        /// Dựng lại đối tượng từ dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ObjectElement LoadData(object data)
        {
            if (data == null) return null;

            var _module = Global.GetModuleByData(data.GetType()); //Tìm mô đun quản lý dữ liệu
            if (_module != null)
            {
                return _module.Load(data);
            }
            else
            {
                if (data is EStandardElement standData)
                {
                    StandardElement _standardElement = new StandardElement();
                    _standardElement.UpdateUI(standData);
                    return _standardElement;
                }
                else if (data is GroupElement group)
                {
                    GroupContainer _groupContainer = new GroupContainer();
                    _groupContainer.UpdateUI(group);
                    return _groupContainer;
                }
                else if (data is DataMotionPath dataMotionPath)
                {
                    MotionPathObject motionPathObject = new MotionPathObject();
                    motionPathObject.UpdateUI(dataMotionPath);
                    return motionPathObject;
                }
            }

            return null;
        }

        #endregion

        #region DependencyProperties

        /// <summary>
        /// Thuộc tính Width
        /// </summary>
        public static DependencyProperty WidthProperty
        {
            get => FrameworkElement.WidthProperty;
        }

        /// <summary>
        /// Thuộc tính Height
        /// </summary>
        public static DependencyProperty HeightProperty
        {
            get => FrameworkElement.HeightProperty;
        }

        /// <summary>
        /// Thuộc tính Angle
        /// </summary>
        public static DependencyProperty AngleProperty
        {
            get => ObjectElement.AngleProperty;
        }

        /// <summary>
        /// Thuộc tính Left
        /// </summary>
        public static DependencyProperty LeftProperty
        {
            get => ObjectElement.LeftProperty;
        }

        /// <summary>
        /// Thuộc tính Top
        /// </summary>
        public static DependencyProperty TopProperty
        {
            get => ObjectElement.TopProperty;
        }

        /// <summary>
        /// Thuộc tính Top
        /// </summary>
        public static DependencyProperty ShapeFillProperty
        {
            get => StandardElement.FillProperty;
        }

        #endregion

        #region Styles
        private static ObservableCollection<FrameStyle> _FrameStyles;
        /// <summary>
        /// Danh sách các kiểu khung văn bản
        /// </summary>
        public static ObservableCollection<FrameStyle> FrameStyles
        {
            get { return _FrameStyles ?? (_FrameStyles = GetFrameStyles()); }
            set { _FrameStyles = value; }
        }

        private static string[] _colorsTheme = { "BackgroundDark1", "Accent1", "Accent2", "Accent3", "Accent4", "Accent5", "Accent6" };
        /// <summary>
        /// Lấy danh sách các kiểu khung
        /// </summary>
        /// <returns></returns>
        private static ObservableCollection<FrameStyle> GetFrameStyles()
        {
            var _result = new ObservableCollection<FrameStyle>();
            var _souce = (Application.Current as IAppGlobal);

            foreach (string item in _colorsTheme) //Lấy theo kiểu 1: Màu chữ: đen, nền: trắng, viền: màu theme
            {
                FrameStyle _style = new FrameStyle();
                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.BorderBrush = _solid;
                _style.Background = new SolidColorBrush(Colors.White);
                _style.Foreground = new SolidColorBrush(Colors.Black);
                _result.Add(_style);
            }

            ColorBrighnessConverter _converter = new ColorBrighnessConverter();
            foreach (string item in _colorsTheme) //Lấy theo kiểu 2: Màu chữ: trắng, nền: màu theme, viền: màu theme đậm 50%
            {
                FrameStyle _style = new FrameStyle();
                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.Background = _solid;

                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                _binding.Converter = _converter;
                _binding.ConverterParameter = 0.5;
                _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.BorderBrush = _solid;

                _style.Foreground = new SolidColorBrush(Colors.White);
                _result.Add(_style);
            }

            foreach (string item in _colorsTheme) //Lấy theo kiểu 3: Màu chữ: trắng, nền: màu theme, viền: trắng
            {
                FrameStyle _style = new FrameStyle();
                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.Background = _solid;

                _style.BorderBrush = new SolidColorBrush(Colors.White);
                _style.Foreground = new SolidColorBrush(Colors.White);
                _result.Add(_style);
            }

            foreach (string item in _colorsTheme) //Lấy theo kiểu 4: Màu chữ: đen, nền: màu theme nhạt 50%, viền: màu theme
            {
                FrameStyle _style = new FrameStyle();
                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.BorderBrush = _solid;

                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                _binding.Converter = _converter;
                _binding.ConverterParameter = 1.7;
                _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.Background = _solid;

                _style.Foreground = new SolidColorBrush(Colors.Black);
                _result.Add(_style);
            }

            foreach (string item in _colorsTheme) //Lấy theo kiểu 5: Màu chữ: trắng, nền: dải màu theme nhạt 30% -> màu theme, viền: màu theme
            {
                FrameStyle _style = new FrameStyle();
                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.BorderBrush = _solid;

                LinearGradientBrush _linear = new LinearGradientBrush();
                _linear.StartPoint = new Point(0, 0);
                _linear.EndPoint = new Point(1, 0);

                GradientStop _stop = new GradientStop(); //Điểm dừng đầu tiên: màu nhạt đi 30%, vị trí 0
                _stop.Offset = 0;
                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                _binding.Converter = _converter;
                _binding.ConverterParameter = 1.3;
                BindingOperations.SetBinding(_stop, GradientStop.ColorProperty, _binding);
                _linear.GradientStops.Add(_stop);

                _stop = new GradientStop(); //Điểm dừng đầu tiên: màu theme, vị trí 0.5
                _stop.Offset = 0.5;
                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                BindingOperations.SetBinding(_stop, GradientStop.ColorProperty, _binding);
                _linear.GradientStops.Add(_stop);

                _stop = new GradientStop(); //Điểm dừng đầu tiên: màu theme, vị trí 1
                _stop.Offset = 1.0;
                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                BindingOperations.SetBinding(_stop, GradientStop.ColorProperty, _binding);
                _linear.GradientStops.Add(_stop);

                _style.Background = _linear;

                _style.Foreground = new SolidColorBrush(Colors.White);
                _result.Add(_style);
            }

            foreach (string item in _colorsTheme) //Lấy theo kiểu 5: Màu chữ: trắng, nền: dải màu theme nhạt 30% -> màu theme, viền: trong suốt
            {
                FrameStyle _style = new FrameStyle();
                _style.IsDropShadow = true;

                LinearGradientBrush _linear = new LinearGradientBrush();
                _linear.StartPoint = new Point(0, 0);
                _linear.EndPoint = new Point(1, 0);

                GradientStop _stop = new GradientStop(); //Điểm dừng đầu tiên: màu nhạt đi 30%, vị trí 0
                _stop.Offset = 0;
                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                _binding.Converter = _converter;
                _binding.ConverterParameter = 1.3;
                BindingOperations.SetBinding(_stop, GradientStop.ColorProperty, _binding);
                _linear.GradientStops.Add(_stop);

                _stop = new GradientStop(); //Điểm dừng đầu tiên: màu theme, vị trí 0.5
                _stop.Offset = 0.5;
                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                BindingOperations.SetBinding(_stop, GradientStop.ColorProperty, _binding);
                _linear.GradientStops.Add(_stop);

                _stop = new GradientStop(); //Điểm dừng đầu tiên: màu theme, vị trí 1
                _stop.Offset = 1;
                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                BindingOperations.SetBinding(_stop, GradientStop.ColorProperty, _binding);
                _linear.GradientStops.Add(_stop);

                _style.Background = _linear;

                _style.Foreground = new SolidColorBrush(Colors.White);
                _result.Add(_style);
            }

            return _result;
        }

        #region Thực hiện các Command ShapeStyle

        private static ICommand _shapeStyleCommand;
        /// <summary>
        /// Lệnh thực thi thay đổi hiêu ứng đối tượng
        /// </summary>
        public static ICommand ShapeStyleCommand
        {
            get
            {
                return _shapeStyleCommand ?? (_shapeStyleCommand = new RelayCommand(param => SetShapeStyleCommand(param)));
            }

        }
        /// <summary>
        /// Thực thi thay đổi hiêu ứng đối tượng
        /// </summary>
        /// <param name="para"></param>
        public static void SetShapeStyleCommand(Object param)
        {
            if (param is FrameStyle style)
            {
                Global.StartGrouping();
                ColorBrushBase _foreground = Model.SolidColor.Converter(style.Foreground as SolidColorBrush);
                ColorBrushBase _background = null, _stroke = null;
                if (style.Background is SolidColorBrush)
                {
                    _background = Model.SolidColor.Converter(style.Background as SolidColorBrush);
                }
                else if (style.Background is GradientBrush)
                {
                    _background = Model.GradientColor.Converter(style.Background as GradientBrush);
                }
                else
                {
                    _background = new ColorSolidBrush() { Color = Colors.Transparent };
                }

                if (style.BorderBrush is SolidColorBrush)
                {
                    _stroke = Model.SolidColor.Converter(style.BorderBrush as SolidColorBrush);
                }
                else if (style.BorderBrush is GradientBrush)
                {
                    _stroke = Model.GradientColor.Converter(style.BorderBrush as GradientBrush);
                }
                else
                {
                    _stroke = new ColorSolidBrush() { Color = Colors.Transparent };
                }

                ShadowEffect _effect = null;
                if (style.IsDropShadow) { _effect = new ShadowEffect() { Type = ShadowType.Outer, Color = "Gray", Blur = 10, Distance = 10, FakeSize = 1.0, IsSize = false, Angle = 45, Transparency = 0.5 }; }

                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    if (item is IBorderSupportExt iBoderExt)
                    {
                        iBoderExt.DashType = Controls.Enums.DashType.Solid;
                        iBoderExt.SetStyle(_stroke, _background, _effect, style.Thickness, _foreground);
                    }
                    else if (item is IBorderSupport iBoder)
                    {
                        iBoder.DashType = Controls.Enums.DashType.Solid;
                        iBoder.SetStyle(_stroke, _background);
                    }
                }
                Global.StopGrouping();
            }
        }

        #endregion

        #endregion

        #region WordStyles

        private static ObservableCollection<WordArtStyle> _wordArtStyles;
        /// <summary>
        /// Danh sách các kiểu chữ nghệ thuật
        /// </summary>
        public static ObservableCollection<WordArtStyle> WordArtStyles
        {
            get { return _wordArtStyles ?? (_wordArtStyles = GetWordArtStyles()); }
            set { _wordArtStyles = value; }
        }

        /// <summary>
        /// Lấy danh sách WordArt
        /// </summary>
        /// <returns></returns>
        private static ObservableCollection<WordArtStyle> GetWordArtStyles()
        {
            ObservableCollection<WordArtStyle> _result = new ObservableCollection<WordArtStyle>();
            var _souce = (Application.Current as IAppGlobal);

            ColorBrighnessConverter _converter = new ColorBrighnessConverter();
            foreach (var item in _colorsTheme)
            {
                WordArtStyle _style = new WordArtStyle();

                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.OutLine = _solid;

                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                _binding.Converter = _converter;
                _binding.ConverterParameter = 1.7;
                _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.Fill = _solid;

                _result.Add(_style);
            }

            foreach (var item in _colorsTheme)
            {
                WordArtStyle _style = new WordArtStyle();

                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.OutLine = _solid;

                _style.Fill = new SolidColorBrush(Colors.White);

                _result.Add(_style);
            }

            foreach (var item in _colorsTheme)
            {
                WordArtStyle _style = new WordArtStyle();
                _style.IsDropShadow = true;

                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.Fill = _solid;

                _style.OutLine = new SolidColorBrush(Colors.White);

                _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                _binding.Converter = _converter;
                _binding.ConverterParameter = 1.7;
                _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.ShadowColor = _solid;

                _result.Add(_style);
            }

            foreach (var item in _colorsTheme)
            {
                WordArtStyle _style = new WordArtStyle();
                _style.IsDropShadow = true;

                Binding _binding = new Binding($"SelectedTheme.Colors.{item}.Color");
                _binding.Source = _souce;
                SolidColorBrush _solid = new SolidColorBrush();
                BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                _style.ShadowColor = _style.OutLine = _solid;

                _style.Fill = new SolidColorBrush(Colors.White);

                _result.Add(_style);
            }

            return _result;
        }

        #region WordStyleCommand
        private static ICommand _wordStyleCommand;
        /// <summary>
        /// Lệnh điều khiển áp dụng Style cho chữ
        /// </summary>
        public static ICommand WordStyleCommand
        {
            get { return _wordStyleCommand ?? (_wordStyleCommand = new RelayCommand(p => WordStyleExecute(p))); }
        }

        /// <summary>
        /// Thực thi điều khiển
        /// </summary>
        /// <param name="p"></param>
        private static void WordStyleExecute(object p)
        {
            if (p is WordArtStyle style)
            {
                ColorBrushBase _fill = Model.SolidColor.Converter(style.Fill as SolidColorBrush);
                ColorBrushBase _outline = Model.SolidColor.Converter(style.OutLine as SolidColorBrush);
                ColorBrushBase _shadow = Model.SolidColor.Converter(style.ShadowColor as SolidColorBrush);

                foreach (var item in (Application.Current as IAppGlobal).SelectedElements)
                {
                    if (item is ITextElement standardElement)
                    {
                        standardElement.SetTextStyle(_fill, _outline, _shadow, style.Thickness);
                    }
                }
            }
        }

        #endregion

        #endregion



        private static Random random = new Random();
        /// <summary>
        /// Lấy ngẫu nhiên một chuỗi với độ dài truyền vào
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

    /// <summary>
    /// Lớp lưu trữ thông tin của kiểu khung văn bản
    /// </summary>
    public class FrameStyle : RootViewModel
    {
        private Brush _foregound;
        /// <summary>
        /// Màu chữ
        /// </summary>
        public Brush Foreground
        {
            get { return _foregound; }
            set { _foregound = value; OnPropertyChanged("Foreground"); }
        }

        /// <summary>
        /// Tên đặc biệt của màu chữ
        /// </summary>
        public object ForegroundSupport { get; set; }

        private Brush _background;
        /// <summary>
        /// Màu nền
        /// </summary>
        public Brush Background
        {
            get { return _background; }
            set { _background = value; OnPropertyChanged("Background"); }
        }

        /// <summary>
        /// Hỗ trợ cho màu nền
        /// </summary>
        public object BackgroundSupport { get; set; }

        private Brush _borderBrush;
        /// <summary>
        /// Màu viền
        /// </summary>
        public Brush BorderBrush
        {
            get { return _borderBrush; }
            set { _borderBrush = value; OnPropertyChanged("BorderBrush"); }
        }

        private bool _isDropShadow;
        /// <summary>
        /// Có đổ bóng hay không
        /// </summary>
        public bool IsDropShadow
        {
            get { return _isDropShadow; }
            set { _isDropShadow = value; }
        }

        private string _name;
        private double _thickness = 2.0;

        /// <summary>
        /// Tên của kiểu khung
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Độ dày viền
        /// </summary>
        public double Thickness { get => _thickness; set => _thickness = value; }

        /// <summary>
        /// Nhân bản dữ liệu
        /// </summary>
        /// <param name="FrameStyle"></param>
        /// <returns></returns>
        public static FrameStyle CloneFrameStyle(FrameStyle FrameStyle)
        {
            FrameStyle result = new FrameStyle();
            result.Foreground = FrameStyle.Foreground;
            result.Background = FrameStyle.Background;
            result.BorderBrush = FrameStyle.BorderBrush;
            result.IsDropShadow = FrameStyle.IsDropShadow;
            result.Name = FrameStyle.Name;
            return result;
        }
    }

    /// <summary>
    /// Lớp lưu trữ kiểu chữ nghệ thuật
    /// </summary>
    public class WordArtStyle : RootViewModel
    {
        private Brush _fill;
        /// <summary>
        /// Màu chữ
        /// </summary>
        public Brush Fill
        {
            get { return _fill; }
            set { _fill = value; OnPropertyChanged("Fill"); }
        }

        private Brush _outLine;
        /// <summary>
        /// Màu đường kẻ
        /// </summary>
        public Brush OutLine
        {
            get { return _outLine; }
            set { _outLine = value; OnPropertyChanged("OutLine"); }
        }

        private Brush _shadowColor;
        /// <summary>
        /// Màu bóng đỗ
        /// </summary>
        public Brush ShadowColor
        {
            get { return _shadowColor; }
            set { _shadowColor = value; OnPropertyChanged("ShadowColor"); }
        }

        /// <summary>
        /// Độ dày viền
        /// </summary>
        public double Thickness { get; set; }

        private bool _isDropShadow;
        /// <summary>
        /// Có đổ bóng hay không
        /// </summary>
        public bool IsDropShadow
        {
            get { return _isDropShadow; }
            set { _isDropShadow = value; }
        }

        private string _name;
        /// <summary>
        /// Tên của kiểu khung
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }

    /// <summary>
    /// Chuyển đổi màu theo độ sáng truyền vào
    /// </summary>

    public class ColorBrighnessConverter : IValueConverter
    {
        /// <summary>
        /// Chuyển đổi thuận
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double _percent = 1.0;
            double.TryParse(parameter?.ToString(), out _percent);
            string _color = value.ToString();
            return ColorHelper.Rebright((Color)ColorConverter.ConvertFromString(_color), _percent);
        }

        /// <summary>
        /// Chuyển đổi ngược
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}

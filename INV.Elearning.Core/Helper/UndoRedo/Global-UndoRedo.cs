﻿

namespace INV.Elearning.Core.Helper
{
    using INV.Elearning.Core.Model;
    using INV.Elearning.Core.View;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;
    using ViewModel;

    /// <summary>
    /// Các khai báo dùng cho toàn bộ chương trình
    /// </summary>
    public static partial class Global
    {
        #region Fields

        /// <summary>
        /// Đang thực hiện dán
        /// </summary>
        public static bool IsPasting { get; set; }
        /// <summary>
        /// Đang đọc lại tài liệu
        /// </summary>
        public static bool IsDataLoading { get; set; }
        /// <summary>
        /// Cờ khai báo có thể đẩy dữ liệu vào stack Undo hay không
        /// </summary>
        private static bool _canPushUndo = true;

        /// <summary>
        /// Cờ khai báo đẩy dữ liệu vào một nhóm
        /// </summary>
        private static bool _pushGrouping = false;

        /// <summary>
        /// Quản lý danh sách các đối tượng Undo Redo
        /// </summary>
        private static UndoRedoManagement _undoRedoManagement = null;

        /// <summary>
        /// Lệnh hoàn tác - Undo
        /// </summary>
        private static RelayCommand _undoCommand = null;

        /// <summary>
        /// Lệnh hủy hoàn tác - Redo
        /// </summary>
        private static RelayCommand _redoCommand = null;

        /// <summary>
        /// Lệnh hoàn tác đến một thời điểm
        /// </summary>
        private static RelayCommand _undoToObjectCommand = null;

        /// <summary>
        /// Lệnh hủy hoàn tác đến một thời điểm
        /// </summary>
        private static RelayCommand _redoToObjectCommand = null;
        #endregion

        #region Properties
        /// <summary>
        /// Lấy quản lý danh sách các đối tượng Undo Redo
        /// </summary>
        public static UndoRedoManagement UndoRedoManagement
        {
            get
            {
                return _undoRedoManagement ?? (_undoRedoManagement = new UndoRedoManagement());
            }
        }

        /// <summary>
        /// Lấy lệnh Undo
        /// </summary>
        public static RelayCommand UndoCommand
        {
            get
            {
                return _undoCommand ?? (_undoCommand = new RelayCommand((o) => UndoExcute(), p => UndoRedoManagement.UndoCollections.Count > 0));
            }
        }

        /// <summary>
        /// Lấy lệnh Redo
        /// </summary>
        public static RelayCommand RedoCommand
        {
            get
            {
                return _redoCommand ?? (_redoCommand = new RelayCommand((o) => RedoExcute(), p => UndoRedoManagement.RedoCollections.Count > 0));
            }
        }

        /// <summary>
        /// Lấy lệnh Undo đến một thời điểm truyền vào
        /// </summary>
        public static RelayCommand UndoToObjectCommand
        {
            get
            {
                return _undoToObjectCommand ?? (_undoToObjectCommand = new RelayCommand((o) => UndoToObjectExcute(o)));
            }
        }

        /// <summary>
        /// Lấy lệnh Redo đến một thời điểm truyền vào
        /// </summary>
        public static RelayCommand RedoToObjectCommand
        {
            get
            {
                return _redoToObjectCommand ?? (_redoToObjectCommand = new RelayCommand((o) => RedoToStepExcute(o)));
            }
        }

        /// <summary>
        /// Lấy trạng thái của cờ đẩy dữ liệu vào stack Undo
        /// </summary>
        public static bool CanPushUndo
        {
            get
            {
                return _canPushUndo;
            }
        }

        /// <summary>
        /// Lấy trạng thái thông báo đang Undo
        /// </summary>
        public static bool UndoProcessing { get; private set; }

        /// <summary>
        /// Lấy trạng thái đang gôm nhóm.
        /// Nếu là true thì dữ liệu sẽ được gôm lại trong 1 multistep
        /// </summary>
        public static bool PushGrouping { get => _pushGrouping; }

        #endregion

        #region Methods

        /// <summary>
        ///Thực thi một lệnh Redo đến thời điểm có đối tượng thay đổi cuối cùng là o
        /// </summary>
        /// <param name="o">Đối tượng thay đổi cuối cùng</param>
        private static void RedoToStepExcute(object o)
        {
            UndoRedoManagement.RedoExcute(o as IUndoRedo);
        }

        /// <summary>
        /// Thực thi một lệnh Undo đến thời điểm có đối tượng thay đổi cuối cùng là o
        /// </summary>
        /// <param name="o">Đối tượng thay đổi cuối cùng</param>
        private static void UndoToObjectExcute(object o)
        {
            UndoRedoManagement.UndoExcute(o as IUndoRedo);
        }

        /// <summary>
        /// Thực thi một lệnh Redo
        /// </summary>
        private static void RedoExcute()
        {
            UndoProcessing = true;
            UndoRedoManagement.RedoExcute();
            Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                UndoProcessing = false;
                _timeCount = 0;

                if (_undoTimer == null) { TimerInitial(); }
                _undoTimer.Start();
            }));

            if ((Application.Current as IAppGlobal).DocumentControl != null)
                (Application.Current as IAppGlobal).DocumentControl.CanSave = true;
        }

        /// <summary>
        /// Thực thi một lệnh Undo
        /// </summary>
        private static void UndoExcute()
        {
            UndoProcessing = true;
            UndoRedoManagement.UndoExcute();
            Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                UndoProcessing = false;
                _timeCount = 0;

                if (_undoTimer == null) { TimerInitial(); }
                _undoTimer.Start();
            }));

            if ((Application.Current as IAppGlobal).DocumentControl != null)
                (Application.Current as IAppGlobal).DocumentControl.CanSave = true;
        }

        /// <summary>
        /// Lay thoi diem cua buoc moi nhat
        /// </summary>
        /// <returns></returns>
        public static long GetLastStepTimeID()
        {
            if (UndoRedoManagement.UndoCollections.Pick() is IUndoRedo step1)
                return step1.CreateTime;
            else if (UndoRedoManagement.RedoCollections.Pick() is IUndoRedo step2)
                return step2.CreateTime;
            return -1;
        }

        /// <summary>
        /// Thêm một đối tượng IUndoRedo vào danh sách quản lý Undo Redo.
        /// </summary>
        /// <param name="value">Đối tượng cần thêm</param>
        //[Obsolete("Lập trình viên cần kiểm tra thuộc tính Global.CanPushUndo trước khi gọi phương thức này")]
        public static void PushUndo(IUndoRedo value)
        {
            if (_canPushUndo)
            {
                if ((Application.Current as IAppGlobal).DocumentControl != null)
                    (Application.Current as IAppGlobal).DocumentControl.CanSave = true;

                if (UndoRedoManagement.MaxStep > 0 && UndoRedoManagement.UndoCollections.Count == UndoRedoManagement.MaxStep)
                    UndoRedoManagement.UndoCollections.RemoveFirstItem(); //Xóa đối tượng nếu danh sách bị đầy

                if (!_isGroupingStarted) //Nếu bắt đầu chế độ group
                {
                    MultiStep _multiStep = new MultiStep();
                    _multiStep.Add(value);
                    UndoRedoManagement.UndoCollections.Push(_multiStep);
                    _isGroupingStarted = true;
                    goto CLEAR_REDOs;
                }

                if (_pushGrouping) //Nếu đang trong chế độ group
                {
                    IUndoRedo _topStep = UndoRedoManagement.UndoCollections.Pick();
                    if (_topStep is MultiStep)
                    {
                        MultiStep _multiStep = _topStep as MultiStep;
                        _multiStep.Add(value);
                        goto CLEAR_REDOs;
                    }
                }

                if (!value.IsBreak && UndoRedoManagement.UndoCollections.Count > 0) //Kiểm tra để gộp nhóm nhiều thay đổi trong 1 bước
                {
                    IUndoRedo _topStep = UndoRedoManagement.UndoCollections.Pick();
                    if (_topStep is MultiStep)
                    {
                        MultiStep _multiStep = _topStep as MultiStep;
                        _topStep = _multiStep.Pick();
                        if (value.CreateTime - _topStep.CreateTime <= 25)
                        {
                            _multiStep.Add(value);
                            goto CLEAR_REDOs;
                        }
                    }
                    else if (value.CreateTime - _topStep.CreateTime <= 25)
                    {
                        MultiStep _multiStep = new MultiStep();
                        _multiStep.Add(UndoRedoManagement.UndoCollections.Pop());
                        _multiStep.Add(value);
                        UndoRedoManagement.UndoCollections.Push(_multiStep);

                        goto CLEAR_REDOs;
                    }
                }

                UndoRedoManagement.UndoCollections.Push(value);

                CLEAR_REDOs:
                //Khi thưc hiện đẩy một undoObj vào trong danh sách Undo, thì sẽ vô hiếu hóa toàn bộ các redoObj trong danh sách redo
                UndoRedoManagement.RedoCollections.Clear();
                _timeCount = 0;

                if (_undoTimer == null) { TimerInitial(); }
                _undoTimer.Start();
            }
        }

        /// <summary>
        /// Tên phương thức gọi hàm
        /// </summary>
        private static string _callName = string.Empty;
        /// <summary>
        /// Bắt đầu quá trình không ghi nhận dữ liệu thay đổi của đối tượng
        /// </summary>
        public static void BeginInit([CallerMemberName] string callName = "")
        {
            if (_canPushUndo && string.IsNullOrEmpty(_callName) && !string.IsNullOrEmpty(callName))
            {
                _canPushUndo = false;
                _callName = callName;
            }
        }


        /// <summary>
        /// Kết thúc quá trình không ghi nhận dữ liệu thay đổi của đối tượng
        /// </summary>
        public static void EndInit([CallerMemberName] string callName = "")
        {
            if (_callName == callName)
            {
                _canPushUndo = true;
                _callName = string.Empty;
            }
        }

        /// <summary>
        /// Tên phương thức gọi hàm
        /// </summary>
        private static string _groupCallName = string.Empty;
        /// <summary>
        /// Cờ ghi nhận đã bắt đầu gôm nhóm, sẽ được bật lên bằng true khi có bất kỳ đối tượng thay đổi nào được thêm vào sau khi gọi hàm StartGrouping
        /// </summary>
        private static bool _isGroupingStarted = true;
        /// <summary>
        /// Bắt đầu quá trình gôm nhóm dữ liệu thay đổi của đối tượng
        /// </summary>
        public static void StartGrouping([CallerMemberName] string callName = "")
        {
            if (!_pushGrouping && !string.IsNullOrEmpty(callName))
            {
                _isGroupingStarted = false;
                _pushGrouping = true;
                _groupCallName = callName;
            }
        }


        /// <summary>
        /// Kết thúc quá trình gôm nhóm dữ liệu thay đổi của đối tượng
        /// </summary>
        public static void StopGrouping([CallerMemberName] string callName = "")
        {
            if (_groupCallName == callName)
                _pushGrouping = false;
        }

        #region Timer
        private static int _timeCount = 0; //Biến lưu trữ giá trị, sẽ được cài đặt = 0 mỗi lúc danh sách Undo, Redo thay đổi
        private static DispatcherTimer _undoTimer = null;
        private static void TimerInitial()
        {
            _undoTimer = new DispatcherTimer();
            _undoTimer.Interval = TimeSpan.FromMilliseconds(1);
            _undoTimer.Tick += UndoTimer_Tick;
        }

        /// <summary>
        /// Đếm chính
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void UndoTimer_Tick(object sender, EventArgs e)
        {
            if (_timeCount > 1)
            {
                var _topStep = UndoRedoManagement.UndoCollections.Pick();
                if (_topStep == null) _topStep = UndoRedoManagement.RedoCollections.Pick();
                if (_topStep != null)
                {
                    List<object> _objects = new List<object>();
                    UpdatePropertyByUndo(_topStep, _objects);
                    _objects.Clear();
                    _objects = null;
                }
                _undoTimer.Stop();
            }
            else
            {
                _timeCount++;
            }
        }

        /// <summary>
        /// Cập nhật các thông số
        /// </summary>
        /// <param name="step"></param>
        private static void UpdatePropertyByUndo(IUndoRedo step, List<object> objects)
        {
            if (step is MultiStep multi)
            {
                if (multi._lstStep != null)
                {
                    foreach (var item in multi._lstStep)
                    {
                        UpdatePropertyByUndo(item, objects);
                    }
                }
            }
            else
            {
                bool _checkFlag = false;
                if (step.Source is IUpdatePropertyByUndo iUndo)
                {
                    if (!objects.Contains(iUndo))
                    {
                        iUndo.OnPropertyChangeByUndo();
                        objects.Add(iUndo);
                        _checkFlag = true;
                    }
                }
                else
                {
                    iUndo = FindParent<LayoutBase>(step.Source as DependencyObject);
                    if (iUndo != null && !objects.Contains(iUndo))
                    {
                        iUndo.OnPropertyChangeByUndo();
                        objects.Add(iUndo);
                        _checkFlag = true;
                    }
                }

                if (step.NewValue is IUpdatePropertyByUndo iUndo1)
                {
                    if (!objects.Contains(iUndo1))
                    {
                        iUndo1.OnPropertyChangeByUndo();
                        objects.Add(iUndo1);
                        _checkFlag = true;
                    }
                }

                if (step.OldValue is IUpdatePropertyByUndo iUndo2)
                {
                    if (!objects.Contains(iUndo2))
                    {
                        iUndo2.OnPropertyChangeByUndo();
                        objects.Add(iUndo2);
                        _checkFlag = true;
                    }
                }

                if (!_checkFlag)
                {
                    foreach (var slide in (Application.Current as IAppGlobal).SelectedSlides)
                    {
                        if (slide.MainLayout?.IsSelected == true) slide.MainLayout.UpdateThumbnail();
                        foreach (var layout in slide.Layouts)
                        {
                            if (layout.IsSelected) layout.UpdateThumbnail();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tìm cha đổi đối tượng
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="child"></param>
        /// <returns></returns>
        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            if (child == null) return null;
            try
            {
                //get parent item
                DependencyObject parentObject = VisualTreeHelper.GetParent(child);

                //we've reached the end of the tree
                if (parentObject == null) return null;

                //check if the parent matches the type we're looking for
                T parent = parentObject as T;
                if (parent != null)
                    return parent;
                else
                    return FindParent<T>(parentObject);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Lấy tài nguyên
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetResource(object key)
        {
            return Application.Current?.TryFindResource(key);
        }
        #endregion
        #endregion
    }
}

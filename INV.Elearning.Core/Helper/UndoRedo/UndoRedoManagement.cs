﻿

namespace INV.Elearning.Core.Helper
{
    using INV.Elearning.Core.View;
    using System.Windows;
    using ViewModel;
    public class UndoRedoManagement
    {
        #region Fields

        /// <summary>
        /// Danh sách các nội dung hoàn tác
        /// </summary>
        private UndoRedoCollections<IUndoRedo> _undoCollections = null;
        /// <summary>
        /// Danh sách các nội dung hủy hoàn tác
        /// </summary>
        private UndoRedoCollections<IUndoRedo> _redoCollections = null;

        #endregion

        #region Properties

        /// <summary>
        /// Lấy danh sách nội dung hoàn tác
        /// </summary>
        internal UndoRedoCollections<IUndoRedo> UndoCollections
        {
            get
            {
                return _undoCollections ?? (_undoCollections = new UndoRedoCollections<IUndoRedo>());
            }
        }

        /// <summary>
        /// Lấy danh sách nội dung hủy hoàn tác
        /// </summary>
        internal UndoRedoCollections<IUndoRedo> RedoCollections
        {
            get
            {
                return _redoCollections ?? (_redoCollections = new UndoRedoCollections<IUndoRedo>());
            }
        }

        public int UndoCount { get => this.UndoCollections.Count; }
        public int RedoCount { get => this.RedoCollections.Count; }
        public int MaxStep  { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Thực thi một lệnh Undo
        /// </summary>
        internal void UndoExcute()
        {
            var _undoObj = UndoCollections.Pop();

            if(_undoObj != null)
            {                
                _undoObj.UndoExcute();
                RedoCollections.Push(_undoObj);
            }

        }

        /// <summary>
        /// Thực thi một lệnh Undo với số bước truyền vào
        /// </summary>
        /// <param name="numOfStep">Số bước cần Undo</param>
        internal void UndoExcute(int numOfStep)
        {
            for (int i = 0; i < numOfStep; i++)
            {
                var _undoObj = UndoCollections.Pop();

                if (_undoObj != null)
                {
                    _undoObj.UndoExcute();
                    RedoCollections.Push(_undoObj);
                }
                else
                    break;
            }
        }


        /// <summary>
        /// Thực thi một lệnh Undo đến thời điểm
        /// </summary>
        /// <param name="undoObj">Đối tượng Undo</param>
        internal void UndoExcute(IUndoRedo undoObj)
        {
            if (UndoCollections.Contains(undoObj))
            {
                while (true)
                {
                    var _undoObj = UndoCollections.Pop();

                    if (_undoObj != null)
                    {
                        _undoObj.UndoExcute();
                        RedoCollections.Push(_undoObj);

                        if (_undoObj.Equals(undoObj))
                            break;
                    }
                    else
                        break;
                }
            }
        }

        /// <summary>
        /// Thực thi một lệnh Redo
        /// </summary>
        internal void RedoExcute()
        {
            var _redoObj = RedoCollections.Pop();

            if (_redoObj != null)
            {
                _redoObj.RedoExcute();
                UndoCollections.Push(_redoObj);
            }
        }

        /// <summary>
        /// Thực thi một lệnh Redo với số bước truyền vào
        /// </summary>
        /// <param name="numOfStep">Số bước cần Redo</param>
        internal void RedoExcute(int numOfStep)
        {
            for (int i = 0; i < numOfStep; i++)
            {
                var _redoObj = RedoCollections.Pop();

                if (_redoObj != null)
                {
                    _redoObj.RedoExcute();
                    UndoCollections.Push(_redoObj);
                }
                else
                    break;
            }
        }

        /// <summary>
        /// Thực thi một lệnh Redo đến thời điểm
        /// </summary>
        /// <param name="redoObj">Đối tượng Redo</param>
        internal void RedoExcute(IUndoRedo redoObj)
        {
            if (UndoCollections.Contains(redoObj))
            {
                while (true)
                {
                    var _redoObj = RedoCollections.Pop();

                    if (_redoObj != null)
                    {
                        _redoObj.UndoExcute();
                        UndoCollections.Push(_redoObj);

                        if (_redoObj.Equals(redoObj))
                            break;
                    }
                    else
                        break;
                }
            }
        }

        #endregion
    }
}

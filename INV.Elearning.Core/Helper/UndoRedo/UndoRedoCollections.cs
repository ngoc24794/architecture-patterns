﻿
using System;
using System.Collections;
using System.Collections.Specialized;

namespace INV.Elearning.Core.Helper
{
    internal class UndoRedoCollections<T>
    {
        private ArrayList _innerList = null;

        protected ArrayList InnerList
        {
            get
            {
                return _innerList ?? (_innerList = new ArrayList());
            }
        }

        /// <summary>
        /// Thêm một đối tượng vào danh sách
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu đối tượng đang mang</typeparam>
        /// <param name="value">Đối tượng</param>
        internal void Push(T value)
        {
            this.InnerList.Add(value);
        }

        /// <summary>
        /// Lấy ra một đối tượng khỏi danh sách
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu đối tượng</typeparam>
        /// <returns>Đối tượng cuối cùng trong danh sách</returns>
        internal T Pop()
        {
            if (this.InnerList.Count > 0)
            {
                var _lastObj = this.InnerList[this.InnerList.Count - 1];
                this.InnerList.RemoveAt(this.InnerList.Count - 1);
                return (T)_lastObj;
            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// Lấy đối tượng mới thêm cuối cùng trong danh sách
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu đối tư</typeparam>
        /// <returns></returns>
        internal T Pick()
        {
            if (this.InnerList.Count > 0)
            {
                return (T)this.InnerList[this.InnerList.Count - 1];
            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// Kiểm tra tồn tại một đối tượng
        /// </summary>
        /// <param name="value">Đối tượng cần kiểm tra</param>
        /// <returns></returns>
        internal bool Contains(T value)
        {
            return this.InnerList.Contains(value);
        }

        /// <summary>
        /// Xóa mọi phần tử có trong danh sách
        /// </summary>
        public void Clear()
        {
            foreach (var item in this.InnerList)
            {
                if (item is IDisposable iDispose)
                    iDispose.Dispose();
            }
            this.InnerList.Clear();
        }

        /// <summary>
        /// Xóa đối tượng dư thừa
        /// </summary>
        public void RemoveFirstItem()
        {
            if (this.InnerList.Count > 0)
                this.InnerList.RemoveAt(0);
        }

        /// <summary>
        /// Đếm số lượng các phần tử trong danh sách
        /// </summary>
        public int Count
        {
            get
            {
                return this.InnerList.Count;
            }
        }
    }
}

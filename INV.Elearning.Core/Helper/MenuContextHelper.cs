﻿

using INV.Elearning.Controls;
using INV.Elearning.Core.AppCommands;
using INV.Elearning.Core.View;
using System.Windows;

namespace INV.Elearning.Core.Helper
{
    public class MenuContextHelper
    {
        private static MenuItem CUT_ITEM, COPY_ITEM, PASTE_ITEM, GROUP_ITEM,
            BRINGFRONT_ITEM, SENDBACK_ITEM, RENAME_ITEM, SAVEIMAGE_ITEM, SIZE_ITEM, FORMATBG_ITEM;

        /// <summary>
        /// Khởi tạo các giá trị menu
        /// </summary>
        private static void InitMenuItems()
        {
            //Định nghĩa menu cắt
            CUT_ITEM = new MenuItem();
            CUT_ITEM.Header = GetResource("COREMENUCONTEXT_Cut")?.ToString();
            CUT_ITEM.Icon = GetResource("CutIcon");
            CUT_ITEM.Command = ObjectCommands.CutObjectCommand;
            //Định nghĩa menu sao chép
            COPY_ITEM = new MenuItem();
            COPY_ITEM.Header = GetResource("COREMENUCONTEXT_Copy")?.ToString();
            COPY_ITEM.Icon = GetResource("CopyIcon");
            COPY_ITEM.Command = ObjectCommands.CopyObjectCommand;
            //Định nghĩa menu dán
            PASTE_ITEM = new MenuItem();
            PASTE_ITEM.Header = GetResource("COREMENUCONTEXT_Paste")?.ToString();
            PASTE_ITEM.Icon = GetResource("PasteIcon");
            PASTE_ITEM.Command = ObjectCommands.PasteNormalObjectCommand;

            //Định nghĩa menu gôm nhóm
            GROUP_ITEM = new MenuItem();
            GROUP_ITEM.Header = GetResource("COREMENUCONTEXT_Group")?.ToString();
            GROUP_ITEM.Icon = GetResource("GroupIcon");
            //Định nghĩa menu gôm nhóm Level 2
            var subItem = new MenuItem();
            subItem.Header = GetResource("COREMENUCONTEXT_Group")?.ToString();
            subItem.Icon = GetResource("GroupIcon");
            subItem.Command = ObjectCommands.GroupCommand;
            GROUP_ITEM.Items.Add(subItem);

            //Định nghĩa menu hủy nhóm Level 2
            subItem = new MenuItem();
            subItem.Header = GetResource("COREMENUCONTEXT_UnGroup")?.ToString();
            subItem.Icon = GetResource("UngroupIcon");
            subItem.Command = ObjectCommands.UnGroupCommand;
            GROUP_ITEM.Items.Add(subItem);

            //Định nghĩa menu nhóm lại Level 2
            subItem = new MenuItem();
            subItem.Header = GetResource("COREMENUCONTEXT_ReGroup")?.ToString();
            subItem.Icon = GetResource("RegroupIcon");
            subItem.Command = ObjectCommands.ReGroupCommand;
            GROUP_ITEM.Items.Add(subItem);

            //Định nghĩa menu chuyển lên trên
            BRINGFRONT_ITEM = new MenuItem();
            BRINGFRONT_ITEM.Header = GetResource("COREMENUCONTEXT_BringToFront")?.ToString();
            BRINGFRONT_ITEM.Icon = GetResource("BringToFrontIcon");
            BRINGFRONT_ITEM.Command = ObjectCommands.BringFrontCommand;

            //Định nghĩa menu chuyển lên trên Level 2
            subItem = new MenuItem();
            subItem.Header = GetResource("COREMENUCONTEXT_BringToFront")?.ToString();
            subItem.Icon = GetResource("BringToFrontIcon");
            BRINGFRONT_ITEM.Items.Add(subItem);

            //Định nghĩa menu chuyển lên trên 1 bước Level 2
            subItem = new MenuItem();
            subItem.Header = GetResource("COREMENUCONTEXT_BringForward")?.ToString();
            subItem.Icon = GetResource("BringForwardIcon");
            subItem.Command = ObjectCommands.BringFrontCommand;
            BRINGFRONT_ITEM.Items.Add(subItem);

            //Định nghĩa menu chuyển xuống dưới
            SENDBACK_ITEM = new MenuItem();
            SENDBACK_ITEM.Header = GetResource("COREMENUCONTEXT_SendToBack")?.ToString();
            SENDBACK_ITEM.Icon = GetResource("SendToBackIcon");

            //Định nghĩa menu chuyển xuống dưới Level 2
            subItem = new MenuItem();
            subItem.Header = GetResource("COREMENUCONTEXT_SendToBack")?.ToString();
            subItem.Icon = GetResource("SendToBackIcon");
            subItem.Command = ObjectCommands.SendBackCommand;
            SENDBACK_ITEM.Items.Add(subItem);

            //Định nghĩa menu chuyển xuống dưới 1 bước Level 2
            subItem = new MenuItem();
            subItem.Header = GetResource("COREMENUCONTEXT_SendBackward")?.ToString();
            subItem.Icon = GetResource("SendBackwardIcon");
            subItem.Command = ObjectCommands.SendBackwardCommand;
            SENDBACK_ITEM.Items.Add(subItem);

            //Định nghĩa menu thay đổi tên
            RENAME_ITEM = new MenuItem();
            RENAME_ITEM.Header = GetResource("COREMENUCONTEXT_Rename")?.ToString();

            //Định nghĩa menu lưu ra hình ảnh
            SAVEIMAGE_ITEM = new MenuItem();
            SAVEIMAGE_ITEM.Header = GetResource("COREMENUCONTEXT_SaveAsPicture")?.ToString();
            SAVEIMAGE_ITEM.Command = ObjectCommands.SaveAsPictureCommand;

            //Định nghĩa menu chuyển lên trên
            SIZE_ITEM = new MenuItem();
            SIZE_ITEM.Header = GetResource("COREMENUCONTEXT_SizeAndPosition")?.ToString();
            SIZE_ITEM.Icon = GetResource("HeightIcon");
            SIZE_ITEM.Command = (Application.Current as IAppGlobal).OpenShapeFormatWindowCommand;
            SIZE_ITEM.CommandParameter = StandardElement.WidthProperty;

            //Định nghĩa menu chuyển lên trên
            FORMATBG_ITEM = new MenuItem();
            FORMATBG_ITEM.Header = GetResource("COREMENUCONTEXT_ShapeFormat")?.ToString();
            FORMATBG_ITEM.Icon = GetResource("ShapeFormat");
            FORMATBG_ITEM.Command = (Application.Current as IAppGlobal).OpenShapeFormatWindowCommand;
            FORMATBG_ITEM.CommandParameter = StandardElement.FillProperty;
        }

        /// <summary>
        /// Lấy menu
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ContextMenu GetContextMenu2(ObjectElement element)
        {
            InitMenuItems();
            ContextMenu _result = new ContextMenu();
            _result.Items.Add(CUT_ITEM);
            _result.Items.Add(COPY_ITEM);
            _result.Items.Add(PASTE_ITEM);

            System.Windows.Controls.Separator _separator = new System.Windows.Controls.Separator();
            _result.Items.Add(_separator);

            var _module = Global.GetModule(element.GetType());
            if (_module != null) //Nạp menu của riêng đối tượng
            {
                foreach (var item in _module.MenuItems)
                {
                    _result.Items.Add(item);
                }
            }
            _result.Items.Add(GROUP_ITEM);
            _result.Items.Add(BRINGFRONT_ITEM);
            _result.Items.Add(SENDBACK_ITEM);
            _result.Items.Add(RENAME_ITEM);
            _separator = new System.Windows.Controls.Separator();
            _result.Items.Add(_separator);
            SAVEIMAGE_ITEM.CommandParameter = element;
            _result.Items.Add(SAVEIMAGE_ITEM);
            _separator = new System.Windows.Controls.Separator();
            _result.Items.Add(_separator);
            _result.Items.Add(SIZE_ITEM);
            _result.Items.Add(FORMATBG_ITEM);

            return _result;
        }

        /// <summary>
        /// Lấy menu
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ContextMenu GetContextMenu(ObjectElement element)
        {
            //InitMenuItems();
            ContextMenu _result = new ContextMenu();
            var _menuItem = GetResource("Obj_CutItem") as MenuItem;
            if (_menuItem != null)
            {
                if (_menuItem.Parent is ContextMenu contextMenu)
                {
                    contextMenu.Items.Clear();
                }
                _result.Items.Add(_menuItem);
            }
            _menuItem = GetResource("Obj_CopyItem") as MenuItem;
            if (_menuItem != null) _result.Items.Add(_menuItem);
            _menuItem = GetResource("Obj_PasteNormalItem") as MenuItem;
            if (_menuItem != null) _result.Items.Add(_menuItem);

            System.Windows.Controls.Separator _separator = new System.Windows.Controls.Separator();
            _result.Items.Add(_separator);

            var _module = Global.GetModule(element.GetType());
            if (_module != null) //Nạp menu của riêng đối tượng
            {
                if (_module.MenuItems != null)
                {
                    foreach (var item in _module.MenuItems)
                    {
                        _result.Items.Add(item);
                    }
                }
            }

            if (element.GetType() == typeof(StandardElement))
            {
                _menuItem = new MenuItem();
                _menuItem.Header = GetResource("COREMENUCONTEXT_EditText")?.ToString();
                _menuItem.Click += (s, e) => { (Application.Current as IAppGlobal).ConvertToTextElement((Application.Current as IAppGlobal).SelectedItem as StandardElement); };
                _result.Items.Add(_menuItem);
            }

            _menuItem = GetResource("Obj_GroupItem") as MenuItem;
            if (_menuItem != null) _result.Items.Add(_menuItem);
            _menuItem = GetResource("Obj_BringFrontItem") as MenuItem;
            if (_menuItem != null) _result.Items.Add(_menuItem);
            _menuItem = GetResource("Obj_SendBackItem") as MenuItem;
            if (_menuItem != null) _result.Items.Add(_menuItem);
            //_menuItem = GetResource("Obj_ReNameItem") as MenuItem;
            //if (_menuItem != null) _result.Items.Add(_menuItem);
            _separator = new System.Windows.Controls.Separator();
            _result.Items.Add(_separator);
            _menuItem = GetResource("Obj_SaveAsPictureItem") as MenuItem;
            if (_menuItem != null) { _result.Items.Add(_menuItem); (_menuItem as MenuItem).CommandParameter = element; }
            _separator = new System.Windows.Controls.Separator();
            _result.Items.Add(_separator);
            _menuItem = GetResource("Obj_SizeItem") as MenuItem;
            if (_menuItem != null) _result.Items.Add(_menuItem);
            _menuItem = GetResource("Obj_FormatItem") as MenuItem;
            if (_menuItem != null) _result.Items.Add(_menuItem);

            return _result;
        }

        /// <summary>
        /// Lấy tài nguyên
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static object GetResource(string key)
        {
            return Application.Current?.TryFindResource(key);
        }
    }
}

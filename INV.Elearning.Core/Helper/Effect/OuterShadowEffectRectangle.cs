﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: OuterShadowEffectRectangle.cs
// Description: Cài đặt hiệu ứng OuterShadow
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------
using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace INV.Elearning.Core.Helper.Effect
{
    public class OuterShadowEffectRectangle : ContentControl
    {
        public OuterShadowEffectRectangle(StandardElement element, ShadowEffect shadowEffect)
        {
            #region OutterShadowEffect
            Rectangle _rectShadow = new Rectangle();

            Binding _binding = new Binding();//Cài đặt binding cho các thuộc tính
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualWidth");
            BindingOperations.SetBinding(_rectShadow, WidthProperty, _binding);

            _binding = new Binding();//Cài đặt binding cho các thuộc tính
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualHeight");
            BindingOperations.SetBinding(_rectShadow, HeightProperty, _binding);

            //_binding = new Binding("ShapePresent.Data");
            //_binding.Source = element;
            //_rectShadow.SetBinding(Rectangle.ClipProperty, _binding);

            VisualBrush _visual = new VisualBrush();//Cài đặt hình ảnh mặt nạ
            _binding = new Binding("ShapePresent");
            _binding.Source = element;
            BindingOperations.SetBinding(_visual, VisualBrush.VisualProperty, _binding);
            // _visual.Visual = element.ShapePresent as FrameworkElement;
            _visual.Stretch = Stretch.None;
            _visual.TileMode = TileMode.None;
            _rectShadow.OpacityMask = _visual;

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Color");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rectShadow, Shape.FillProperty, _binding);

            _binding = new Binding();//Cài đặt binding cho các thuộc tính
            _binding.Converter = new TransparencyConverter();
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Transparency");
            BindingOperations.SetBinding(_rectShadow, OpacityProperty, _binding);

            MultiBinding _multiBinding = new MultiBinding();
            _multiBinding.Converter = new OutterShadowMarginConverter();
            _multiBinding.ConverterParameter = _rectShadow;
            _multiBinding.Bindings.Add(new Binding("Left") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Distance") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = shadowEffect });
            //_multiBinding.Bindings.Add(new Binding("OuterShadowType") { Source = shadowEffect });
            BindingOperations.SetBinding(_rectShadow, MarginProperty, _multiBinding);


            //_binding = new Binding();//Cài đặt binding cho các thuộc tính
            //_binding.Converter = new OutterShadowMarginConverter();
            //_binding.Source = shadowEffect;
            //_binding.Path = new PropertyPath("Distance");
            //BindingOperations.SetBinding(_rectShadow, MarginProperty, _binding);

            // Panel.SetZIndex((element.Container.Children[1] as ContentPresenter), 2);
            Panel.SetZIndex(this, -2);
            BlurEffect _blur = new BlurEffect();

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Blur");
            BindingOperations.SetBinding(_blur, BlurEffect.RadiusProperty, _binding);
            _rectShadow.Effect = _blur;

            TransformGroup tgShadow = new TransformGroup();
            ScaleTransform stShadow = new ScaleTransform();

            _binding = new Binding();//Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("FakeSize");
            BindingOperations.SetBinding(stShadow, ScaleTransform.ScaleXProperty, _binding);

            _binding = new Binding();//Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("FakeSize");
            BindingOperations.SetBinding(stShadow, ScaleTransform.ScaleYProperty, _binding);

            tgShadow.Children.Add(stShadow);

            _rectShadow.RenderTransform = tgShadow;

            _binding = new Binding();
            _binding.Converter = new OuterShadowRenderTransformOriginConverter();
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("OuterShadowType");
            BindingOperations.SetBinding(_rectShadow, RenderTransformOriginProperty, _binding);
            Content = _rectShadow;

            //Cài đặt Binding Ẩn hiện
            _binding = new Binding();
            _binding.Source = element;
            _binding.Path = new PropertyPath("InSession");
            _binding.Converter = Converters.InBooleanToVisibilityConverter;
            BindingOperations.SetBinding(this, VisibilityProperty, _binding);
            #endregion
        }
    }
}

﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: GlowEffectRectangle.cs
// Description: Cài đặt hiệu ứng GlowEffect
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------


using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Effects;

namespace INV.Elearning.Core.Helper.Effect
{
    public class GlowEffectRectangle
    {

        /// <summary>
        /// Cài đặt hiệu ứng GlowEffectShadow
        /// </summary>
        /// <param name="element"></param>
        /// <param name="glowEffect"></param>
        public static void SetGlowEffect(StandardElement element, GlowEffect glowEffect)
        {
            if(glowEffect.Color == null)
            {
                RemoveGlowEffect(element);
                return;
            }

            DropShadowEffect _glowEffect = new DropShadowEffect() { ShadowDepth = 0};
            Binding _binding = new Binding("Color");
            _binding.Source = glowEffect;
            BindingOperations.SetBinding(_glowEffect, DropShadowEffect.ColorProperty, _binding);
            
            _binding = new Binding("Size");
            _binding.Source = glowEffect;
            BindingOperations.SetBinding(_glowEffect, DropShadowEffect.BlurRadiusProperty, _binding);

            MultiBinding _mBinding = new MultiBinding();
            _mBinding.Converter = new ShadowEffectConverter();
            _binding = new Binding("InSession");
            _binding.Source = element;
            _mBinding.Bindings.Add(_binding);

            _binding = new Binding();
            _binding.Source = _glowEffect;
            _mBinding.Bindings.Add(_binding);

            BindingOperations.SetBinding(element.ShapePresent, FrameworkElement.EffectProperty, _mBinding);
        }

        /// <summary>
        /// Xóa hiệu ứng
        /// </summary>
        /// <param name="element"></param>
        public static void RemoveGlowEffect(StandardElement element)
        {
            if(element.ShapePresent?.Effect is DropShadowEffect glowEffect)
            {
                BindingOperations.ClearAllBindings(glowEffect);
                element.ShapePresent.Effect = null;
            }
        }

        /// <summary>
        /// Chuyển đổi
        /// </summary>
        public class ShadowEffectConverter : IMultiValueConverter
        {
            public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
            {
                if ((bool)values[0])
                    return null;

                return values[1];
            }

            public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            {
                return new object[] { Binding.DoNothing, Binding.DoNothing };
            }
        }
    }
}

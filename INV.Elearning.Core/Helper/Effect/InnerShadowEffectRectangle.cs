﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: InnerShadowEffectRectangle.cs
// Description: Cài đặt hiệu ứng InnerShadowEffect
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------

using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace INV.Elearning.Core.Helper.Effect
{
    public class InnerShadowEffectRectangle : ContentControl
    {

        /// <summary>
        /// Cài đặt Hiệu ứng InnerShadow
        /// </summary>
        /// <param name="element"></param>
        /// <param name="shadowEffect"></param>
        public InnerShadowEffectRectangle(StandardElement element, ShadowEffect shadowEffect)
        {
            Path _intersetPath = new Path();//Tạo một đường Path là hiệu của hình và phần bóng

            MultiBinding _multiBinding = new MultiBinding();//Binding lấy đường Path là hiệu của hình và phần bóng
            _multiBinding.Converter = new InnerShadowPathGeometryConverter();
            _multiBinding.ConverterParameter = element;
            _multiBinding.Bindings.Add(new Binding("Distance") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Blur") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("ActualWidth") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ActualHeight") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ShapePresent.Data") { Source = element });
            BindingOperations.SetBinding(_intersetPath, Path.DataProperty, _multiBinding);

            _intersetPath.Fill = Brushes.Transparent;

            BlurEffect _blur = new BlurEffect();//Tạo hiệu ứng Blur
            Binding _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Blur");
            BindingOperations.SetBinding(_blur, BlurEffect.RadiusProperty, _binding);
            _intersetPath.Effect = _blur;

            _binding = new Binding();//Binding thuộc tính Transparency
            _binding.Converter = new TransparencyConverter();
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Transparency");
            BindingOperations.SetBinding(_intersetPath, OpacityProperty, _binding);

            //Nếu Content của ObjectElement là một đường Path thì cắt hình mới thêm vào
            var p = (element.Content);
            if (p is Path)
            {
                PathGeometry _pathGeo = new PathGeometry();
                _pathGeo.AddGeometry((p as Path).Data);
                this.Clip = _pathGeo;
            }

            IsHitTestVisible = false;

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Color");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_intersetPath, Shape.StrokeProperty, _binding);


            //_binding = new Binding();//Cài đặt binding cho các thuộc tính
            //_binding.Source = shadowEffect;
            //_binding.Path = new PropertyPath("Distance");
            //BindingOperations.SetBinding(_intersetPath, Shape.StrokeThicknessProperty, _binding);

            _multiBinding = new MultiBinding();//Binding thuộc tính Stroke thickness của path
            _multiBinding.Converter = new StrokeThicknessInnerShadowConverter();
            _multiBinding.Bindings.Add(new Binding("Distance") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Blur") { Source = shadowEffect });
            BindingOperations.SetBinding(_intersetPath, Shape.StrokeThicknessProperty, _multiBinding);

            Grid grid = new Grid();//Tạo một Grid mới   

            //Thêm 2 đường Path trên vào Grid
            // grid.Children.Add(newPath);
            grid.Children.Add(_intersetPath);
            //Gán content là Grid
            Content = grid;

            _binding = new Binding("ShapePresent.Data");
            _binding.Source = element;
            grid.SetBinding(Grid.ClipProperty, _binding);

            MultiBinding _multibinding = new MultiBinding();
            _multibinding.Bindings.Add(new Binding("Fill") { Source = element });
            _multibinding.Bindings.Add(new Binding("InSession") { Source = element });
            _multibinding.Converter = new ColorInnerShadowEffectConverter();
            BindingOperations.SetBinding(this, VisibilityProperty, _multibinding);

            //Điều chỉnh margin cho hiệu ứng
            Rect bound = _intersetPath.Data.Bounds;
            _intersetPath.Margin = new Thickness(0, 0, -double.MaxValue, -double.MaxValue);

            //_binding = new Binding();
            //_binding.Source = element;
            //_binding.Path = new PropertyPath("InSession");
            //_binding.Converter = Converters.InBooleanToVisibilityConverter;
            //BindingOperations.SetBinding(this, VisibilityProperty, _binding);
        }

        class MarginConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return new Thickness(-(double)value / 2.0);
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return Binding.DoNothing;
            }
        }
    }
}

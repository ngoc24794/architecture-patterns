﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: PerspectiveShadowRectangleEffect.cs
// Description: Cài đặt hiệu ứng PerspectiveShadowRectangleEffect
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------

using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace INV.Elearning.Core.Helper.Effect
{
    public class PerspectiveShadowRectangleEffect : ContentControl
    {
        public Rectangle _rectShadow;
        /// <summary>
        /// Đối tượng chủ
        /// </summary>
        public FrameworkElement Owner { get; private set; }

        public PerspectiveShadowRectangleEffect(ObjectElement element, ShadowEffect shadowEffect)
        {
            Owner = element;
            #region PerspectiveShadow
            _rectShadow = new Rectangle();//Tạo một rectTangle chứa hình ảnh phản chiếu
            _rectShadow.IsHitTestVisible = false;
            _rectShadow.Stretch = Stretch.Fill;

            VisualBrush _visual = new VisualBrush();//Lấy VisualBrush của ObjectElement gán vào hình phản chiếu
            _visual.Visual = element.Container as FrameworkElement;
            _visual.Stretch = Stretch.None;
            _visual.TileMode = TileMode.None;
            _rectShadow.OpacityMask = _visual;

            Binding _binding = new Binding();//Binding Width
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualWidth");
            BindingOperations.SetBinding(this, WidthProperty, _binding);

            _binding = new Binding();//Binding Height
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualHeight");
            BindingOperations.SetBinding(this, HeightProperty, _binding);

            MultiBinding _multiBinding = new MultiBinding();//Binding Left
            _multiBinding.Converter = new PerspectiveShadowLeftConverter();
            _multiBinding.ConverterParameter = element;
            _multiBinding.Bindings.Add(new Binding("Left") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Distance") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("PerspectiveShadowType") { Source = shadowEffect });
            _multiBinding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(this, Canvas.LeftProperty, _multiBinding);


            _multiBinding = new MultiBinding();//Binding Top
            _multiBinding.Converter = new PerspectiveShadowTopConverter();
            _multiBinding.ConverterParameter = element;
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Top") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Distance") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("PerspectiveShadowType") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("ActualWidth") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ActualHeight") { Source = element });
            _multiBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            _multiBinding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(this, Canvas.TopProperty, _multiBinding);

            TransformGroup tgShadow = new TransformGroup();

            RotateTransform _rotateTranformShadow = new RotateTransform();//Thay đổi góc quay
            _rectShadow.RenderTransformOrigin = new Point(0.5, 0.5);

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = element;
            _binding.Path = new PropertyPath("Angle");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rotateTranformShadow, RotateTransform.AngleProperty, _binding);
            tgShadow.Children.Add(_rotateTranformShadow);

            SkewTransform skShadow = new SkewTransform();
            _binding = new Binding();
            _binding.Converter = new PerspectiveShadowSkewAngleConverter();
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("PerspectiveShadowType");
            BindingOperations.SetBinding(skShadow, SkewTransform.AngleXProperty, _binding);
            tgShadow.Children.Add(skShadow);

            BlurEffect _blur = new BlurEffect();
            _binding = new Binding(); //Binding Blur
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Blur");
            BindingOperations.SetBinding(_blur, BlurEffect.RadiusProperty, _binding);
            _rectShadow.Effect = _blur;

            ScaleTransform stShadow = new ScaleTransform();

            _multiBinding = new MultiBinding();
            _multiBinding.Converter = new PerspectiveShadowCenterXConverter();
            _multiBinding.ConverterParameter = element;
            _multiBinding.Bindings.Add(new Binding("FakeSize") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Top") { Source = element });
            _multiBinding.Bindings.Add(new Binding("PerspectiveShadowType") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("ActualWidth") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ActualHeight") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = element });
            BindingOperations.SetBinding(stShadow, ScaleTransform.CenterXProperty, _multiBinding);

            _multiBinding = new MultiBinding();
            _multiBinding.Converter = new PerspectiveShadowCenterYConverter();
            _multiBinding.ConverterParameter = element;
            _multiBinding.Bindings.Add(new Binding("FakeSize") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("Top") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ActualWidth") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ActualHeight") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = element });
            BindingOperations.SetBinding(stShadow, ScaleTransform.CenterYProperty, _multiBinding);


            _binding = new Binding();//Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("FakeSize");
            BindingOperations.SetBinding(stShadow, ScaleTransform.ScaleXProperty, _binding);

            _multiBinding = new MultiBinding();//Cài đặt binding cho các thuộc tính
            _multiBinding.Converter = new PerspectiveShadowScaleYConverter();
            _multiBinding.ConverterParameter = element;
            _multiBinding.Bindings.Add(new Binding("FakeSize") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("PerspectiveShadowType") { Source = shadowEffect });
            _multiBinding.Bindings.Add(new Binding("IsSize") { Source = shadowEffect });
            BindingOperations.SetBinding(stShadow, ScaleTransform.ScaleYProperty, _multiBinding);

            tgShadow.Children.Add(stShadow);

            _rectShadow.RenderTransform = tgShadow;

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Color");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rectShadow, Shape.FillProperty, _binding);

            _binding = new Binding();//Cài đặt binding cho các thuộc tính
            _binding.Converter = new TransparencyConverter();
            _binding.Source = shadowEffect;
            _binding.Path = new PropertyPath("Transparency");
            BindingOperations.SetBinding(_rectShadow, OpacityProperty, _binding);

            _binding = new Binding("ZIndex");//Cài đặt binding cho các thuộc tính
            _binding.Converter = new ZIndexConverter();
            _binding.Source = element;
            BindingOperations.SetBinding(this, Panel.ZIndexProperty, _binding);

            IsHitTestVisible = false;
            Content = _rectShadow;

            //Cài đặt Binding Ẩn hiện
            _binding = new Binding();
            _binding.Source = element;
            _binding.Path = new PropertyPath("InSession");
            _binding.Converter = Converters.InBooleanToVisibilityConverter;
            BindingOperations.SetBinding(this, VisibilityProperty, _binding);
            #endregion
        }

        class ZIndexConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return (int)value - 1;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return Binding.DoNothing;
            }
        }
    }
}

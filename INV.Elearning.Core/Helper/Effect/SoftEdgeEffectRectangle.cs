﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: SoftEdgeEffectRectangle.cs
// Description: Cài đặt hiệu ứng SoftEdgeEffectRectangle
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------

using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace INV.Elearning.Core.Helper.Effect
{
    public class SoftEdgeEffectRectangle
    {
        public static void SetSoftEdgeEffect(StandardElement element, SoftEdgeEffect softEdgeEffect)
        {
            #region SoftEdge

            Path _rectangle = new Path();
            _rectangle.Fill = Brushes.Black;

            Binding _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = element;
            _binding.Path = new PropertyPath("ShapePresent.Data");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rectangle, Path.DataProperty, _binding);

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualWidth");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rectangle, Path.WidthProperty, _binding);

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualHeight");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rectangle, Path.HeightProperty, _binding);

            BlurEffect _blur = new BlurEffect();
            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Source = softEdgeEffect;
            _binding.Path = new PropertyPath("Size");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_blur, BlurEffect.RadiusProperty, _binding);

            _blur.KernelType = KernelType.Gaussian;
            _rectangle.Effect = _blur; //Cài đặt Blur effect

            VisualBrush _visual = new VisualBrush(); //Cài đặt hình ảnh mặt nạ
            _visual.Visual = _rectangle;
            _visual.TileMode = TileMode.None;

            //Binding thuộc tính ẩn hiện
            _binding = new Binding();
            _binding.Converter = new SoftEdgeVisibilityConverter();
            _binding.ConverterParameter = _visual;
            _binding.Source = element;
            _binding.Path = new PropertyPath("InSession");
            if (element.Container != null)
            {
                BindingOperations.SetBinding(element.Container, FrameworkElement.OpacityMaskProperty, _binding);
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                    new Action(() =>
                    {
                        BindingOperations.SetBinding(element.Container, FrameworkElement.OpacityMaskProperty, _binding);
                    }));
            }
            #endregion
        }

        /// <summary>
        /// Xóa bỏ hiệu ứng
        /// </summary>
        /// <param name="element"></param>
        public static void RemoveSoftEdgeEffect(StandardElement element)
        {
            if (element.Container?.OpacityMask is VisualBrush visual)
            {
                if (visual.Visual is Path path)
                {
                    BindingOperations.ClearAllBindings(path);
                    visual.Visual = null;
                }
                BindingOperations.ClearBinding(element.Container, FrameworkElement.OpacityMaskProperty);
                element.Container.OpacityMask = null;
            }
        }
    }
}

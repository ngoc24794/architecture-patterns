﻿using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using INV.Elearning.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper.Effect
{
    public enum ReflectionEfflectEnum
    {
        None,
        TightReflection,
        HalfReflection,
        FullReflection
    }

    public enum OuterShadowEnum
    {
        BottomRight,
        Bottom,
        BottomLeft,
        Right,
        Center,
        Left,
        TopRight,
        Top,
        TopLeft
    }

    public enum PerspectiveShadowEnum
    {
        UpperLeft,
        UpperRight,
        Below,
        LowerLeft,
        LowerRight
    }

    public enum InnerShadowEnum
    {
        TopLeft,
        Top,
        TopRight,
        Left,
        Center,
        Right,
        BottomLeft,
        Bottom,
        BottomRight
    }

    /// <summary>
    /// Lớp hỗ trợ cung cấp các xử lý cài đặt hiệu ứng đối tượng
    /// </summary>
    public class EffectHelper
    {
        /// <summary>
        /// Set hiệu ứng Reflection cho Shape
        /// </summary>
        /// <param name="element"></param>
        /// <param name="effect"></param>
        public static void SetReflectionEffect(StandardElement element, ReflectionEffect effect)
        {
            if (effect.Blur == 0 && effect.Distance == 0 && effect.Size == 0) return;
            ReflectionEffectRectangle _reflection = new ReflectionEffectRectangle(element, effect);
            (element.Parent as Panel)?.Children.Add(_reflection);
            element.SupportElements.Add(_reflection);
        }

        /// <summary>
        /// Set hiệu ứng Glow
        /// </summary>
        /// <param name="element"></param>
        /// <param name="glowEffect"></param>
        public static void SetGlowEffect(StandardElement element, GlowEffect glowEffect)
        {
            GlowEffectRectangle.SetGlowEffect(element, glowEffect);
        }

        /// <summary>
        /// Set Hiệu ứng SoftEdge
        /// </summary>
        /// <param name="element"></param>
        /// <param name="softEdgeEffect"></param>
        public static void SetSoftEdgeEffect(StandardElement element, SoftEdgeEffect softEdgeEffect)
        {
            SoftEdgeEffectRectangle.SetSoftEdgeEffect(element, softEdgeEffect);
        }

        /// <summary>
        /// Set Hiệu ứng InnerShadow
        /// </summary>
        /// <param name="element"></param>
        /// <param name="shadowEffect"></param>
        private static void SetInnerShadow(StandardElement element, ShadowEffect shadowEffect)
        {
            InnerShadowEffectRectangle _innerShadow = new InnerShadowEffectRectangle(element, shadowEffect);
            element.Container.Children.Add(_innerShadow);
        }

        /// <summary>
        /// Cài đặt hiệu ứng đỗ bóng cho đối tượng
        /// </summary>
        /// <param name="element"></param>
        /// <param name="shadowEffect"></param>
        public static void SetShadowEffect(StandardElement element, ShadowEffect shadowEffect)
        {
            RemoveShadowEffect(element);
            if (shadowEffect != null)
            {
                switch (shadowEffect.Type)
                {
                    case ShadowType.Outer:
                        if (element.Container == null)
                        {
                            element.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                            {
                                OuterShadowEffectRectangle _outerShadow = new OuterShadowEffectRectangle(element, shadowEffect);
                                element.Container?.Children.Add(_outerShadow);
                            }));
                        }
                        else
                        {
                            OuterShadowEffectRectangle _outerShadow = new OuterShadowEffectRectangle(element, shadowEffect);
                            element.Container.Children.Add(_outerShadow);
                        }
                        break;
                    case ShadowType.Inner:

                        if (element.Container == null)
                        {
                            element.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                            {
                                InnerShadowEffectRectangle _innerShadow = new InnerShadowEffectRectangle(element, shadowEffect);
                                Panel.SetZIndex(_innerShadow, 5);
                                (element.Container)?.Children.Add(_innerShadow);
                            }));
                        }
                        else
                        {
                            InnerShadowEffectRectangle _innerShadow = new InnerShadowEffectRectangle(element, shadowEffect);
                            Panel.SetZIndex(_innerShadow, 5);
                            (element.Container)?.Children.Add(_innerShadow);
                        }

                        break;
                    case ShadowType.Perspective:
                        if (!element.IsLoaded || element.Parent == null)
                        {
                            element.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                            {
                                if (element.IsLoaded || element.Parent != null)
                                {
                                    PerspectiveShadowRectangleEffect _perspective = new PerspectiveShadowRectangleEffect(element, shadowEffect);
                                    (element.Parent as Panel)?.Children.Add(_perspective);
                                    element.SupportElements.Add(_perspective);
                                }
                            }));
                        }
                        else
                        {
                            PerspectiveShadowRectangleEffect _perspective = new PerspectiveShadowRectangleEffect(element, shadowEffect);
                            (element.Parent as Panel)?.Children.Add(_perspective);
                            element.SupportElements.Add(_perspective);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Xóa các hiệu ứng đổ bóng
        /// </summary>
        /// <param name="element"></param>
        public static void RemoveShadowEffect(StandardElement element)
        {
            for (int i = 0; i < element.Container?.Children.Count; i++) //Kiểm tra xóa hiệu ứng đổ bóng trong và ngoài
            {
                if (element.Container.Children[i] is OuterShadowEffectRectangle || element.Container.Children[i] is InnerShadowEffectRectangle)
                {
                    element.Container.Children.RemoveAt(i);
                    i--;
                }
            }

            Panel _panel = (element.Parent as Panel);
            if (_panel != null)
            {
                foreach (UIElement child in _panel.Children) //Kiểm tra xóa hiệu ứng đổ bóng xéo
                {
                    if (child is PerspectiveShadowRectangleEffect && (child as PerspectiveShadowRectangleEffect).Owner == element)
                    {
                        _panel.Children.Remove(child);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Xóa hiệu ứng tỏa sáng
        /// </summary>
        /// <param name="element"></param>
        public static void RemoveGlowEffect(StandardElement element)
        {
            GlowEffectRectangle.RemoveGlowEffect(element);
        }

        /// <summary>
        /// Xóa hiệu ứng phản chiếu
        /// </summary>
        /// <param name="element"></param>
        public static void RemoveReflectionEffect(StandardElement element)
        {
            if (element.Parent is Panel _panel)
            {
                foreach (UIElement child in _panel.Children) //Kiểm tra xóa hiệu ứng đổ bóng xéo
                {
                    if (child is ReflectionEffectRectangle && (child as ReflectionEffectRectangle).Owner == element)
                    {
                        _panel.Children.Remove(child);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Xóa hiệu ứng làm mềm
        /// </summary>
        /// <param name="element"></param>
        public static void RemoveSoftEdgeEffect(StandardElement element)
        {
            SoftEdgeEffectRectangle.RemoveSoftEdgeEffect(element);
        }

        #region Danh sách các icon cho từng hiệu ứng

        #region Shadows
        private static List<ShadowIconModel> _shadowIcons;
        /// <summary>
        /// Danh sách các biểu tượng của hiệu ứng đổ bóng
        /// </summary>
        public static List<ShadowIconModel> ShadowIcons
        {
            get { return _shadowIcons ?? (_shadowIcons = GetShadowIcons()); }
        }

        /// <summary>
        /// Lấy danh sách các hình ảnh cho giao diện hiệu ứng đỗ bóng
        /// </summary>
        /// <returns></returns>
        private static List<ShadowIconModel> GetShadowIcons()
        {
            List<ShadowIconModel> _shadowIcons = new List<ShadowIconModel>();
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_NoneShadow")?.ToString(), IconResource = Application.Current.TryFindResource("NoneShadowIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_NoneShadow")?.ToString() });

            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalRightBottom")?.ToString(), IconResource = Application.Current.TryFindResource("OuterRightBottomIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.BottomRight });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalBottom")?.ToString(), IconResource = Application.Current.TryFindResource("OuterBottomIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.Bottom });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalLeftBottom")?.ToString(), IconResource = Application.Current.TryFindResource("OuterLeftBottomIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.BottomLeft });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalRight")?.ToString(), IconResource = Application.Current.TryFindResource("OuterRightIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.Right });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalCenter")?.ToString(), IconResource = Application.Current.TryFindResource("OuterCenterIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.Center });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalLeft")?.ToString(), IconResource = Application.Current.TryFindResource("OuterLeftIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.Left });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalRightTop")?.ToString(), IconResource = Application.Current.TryFindResource("OuterRightTopIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.TopRight });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalTop")?.ToString(), IconResource = Application.Current.TryFindResource("OuterTopIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.Top });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_OffsetDiagonalLeftTop")?.ToString(), IconResource = Application.Current.TryFindResource("OuterLeftTopIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Outer")?.ToString(), EnumType = OuterShadowEnum.TopLeft });

            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalLeftTop")?.ToString(), IconResource = Application.Current.TryFindResource("InnerLeftTopIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.TopLeft });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalTop")?.ToString(), IconResource = Application.Current.TryFindResource("InnerTopIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.Top });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalRightTop")?.ToString(), IconResource = Application.Current.TryFindResource("InnerRightTopIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.TopRight });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalLeft")?.ToString(), IconResource = Application.Current.TryFindResource("InnerLeftIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.Left });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalCenter")?.ToString(), IconResource = Application.Current.TryFindResource("InnerCenterIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.Center });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalRight")?.ToString(), IconResource = Application.Current.TryFindResource("InnerRightIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.Right });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalLeftBottom")?.ToString(), IconResource = Application.Current.TryFindResource("InnerLeftBottomIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.BottomLeft });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalBottom")?.ToString(), IconResource = Application.Current.TryFindResource("InnerBottomIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.Bottom });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_InsideDiagonalRightBottom")?.ToString(), IconResource = Application.Current.TryFindResource("InnerRightBottomIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Inner")?.ToString(), EnumType = InnerShadowEnum.BottomRight });

            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_PerspectiveDiagonalUpperLeft")?.ToString(), IconResource = Application.Current.TryFindResource("PreUpperLeftIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Perspective")?.ToString(), EnumType = PerspectiveShadowEnum.UpperLeft });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_PerspectiveDiagonalUpperRight")?.ToString(), IconResource = Application.Current.TryFindResource("PreUpperRightIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Perspective")?.ToString(), EnumType = PerspectiveShadowEnum.UpperRight });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_PerspectiveDiagonalBelow")?.ToString(), IconResource = Application.Current.TryFindResource("PreBelowIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Perspective")?.ToString(), EnumType = PerspectiveShadowEnum.Below });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_PerspectiveDiagonalLowerLeft")?.ToString(), IconResource = Application.Current.TryFindResource("PreLowerLeftIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Perspective")?.ToString(), EnumType = PerspectiveShadowEnum.LowerLeft });
            _shadowIcons.Add(new ShadowIconModel() { Name = FileHelper.FindResource("CORESHADOWEFFECT_PerspectiveDiagonalLowerRight")?.ToString(), IconResource = Application.Current.TryFindResource("PreLowerRightIcon"), GroupName = FileHelper.FindResource("CORESHADOWEFFECT_Perspective")?.ToString(), EnumType = PerspectiveShadowEnum.LowerRight });
            return _shadowIcons;
        }

        private static RelayCommand _shadowCommand;
        /// <summary>
        /// Lệnh điều khiển cho cài đặt hiệu ứng đỗ bóng
        /// </summary>
        public static RelayCommand ShadowCommand
        {
            get { return _shadowCommand ?? (_shadowCommand = new RelayCommand(ShadowExcute)); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt hiệu ứng đỗ bóng
        /// </summary>
        /// <param name="obj"></param>
        private static void ShadowExcute(object obj)
        {
            ShadowIconModel _shadow = obj as ShadowIconModel;
            if (_shadow != null)
            {
                Global.StartGrouping();
                if (_shadow.EnumType == null) //Nếu không đỗ bóng
                {
                    var effect = new ShadowEffect()
                    {
                        Type = ShadowType.None
                    };
                    foreach (var stElement in (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IEffectSupport))
                    {
                        (stElement as IEffectSupport).Effects.Add(effect);
                    }
                }
                else if (_shadow.EnumType is OuterShadowEnum outerType) //Nếu là hiệu ứng đổ bóng ngoài
                {
                    var effect = new ShadowEffect()
                    {
                        Blur = outerType == OuterShadowEnum.Center ? 15 : 10,
                        Distance = outerType == OuterShadowEnum.Center ? 0 : 10,
                        FakeSize = 1.0,
                        Transparency = 0.5,
                        Angle = GetOuterShadowAngle(outerType),
                        Color = "Gray",
                        Type = ShadowType.Outer
                    };

                    foreach (var stElement in (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IEffectSupport))
                    {
                        (stElement as IEffectSupport).Effects.Add(effect);
                    }
                }
                else if (_shadow.EnumType is InnerShadowEnum innerType) //Nếu là hiệu ứng đổ bóng Trong
                {
                    var effect = new ShadowEffect() { Angle = GetInnserShadowAngle(innerType), Transparency = 0.5, Color = "Black", Type = ShadowType.Inner };
                    if (innerType == InnerShadowEnum.Center)
                    {
                        effect.Blur = 9;
                        effect.Distance = 0;
                    }
                    else
                    {
                        effect.Blur = 5;
                        effect.Distance = 10;
                    }
                    foreach (var stElement in (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IEffectSupport))
                    {
                        (stElement as IEffectSupport).Effects.Add(effect);
                    }
                }
                else if (_shadow.EnumType is PerspectiveShadowEnum perType) //Nếu là hiệu ứng đổ bóng xéo
                {
                    var effect = new ShadowEffect()
                    {
                        Blur = 20,
                        Distance = 0,
                        FakeSize = 1,
                        Angle = 90,
                        Transparency = 0.5,
                        Color = "Gray",
                        IsSize = false,
                        Type = ShadowType.Perspective,
                        PerspectiveShadowType = perType
                    };
                    foreach (var stElement in (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IEffectSupport))
                    {
                        (stElement as IEffectSupport).Effects.Add(effect);
                    }
                }
                Global.StopGrouping();
            }
        }

        /// <summary>
        /// Lấy góc quay cho hiệu ứng đổ bóng ngoài
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static double GetOuterShadowAngle(OuterShadowEnum type)
        {
            switch (type)
            {
                case OuterShadowEnum.BottomRight:
                    return 45;
                case OuterShadowEnum.Bottom:
                    return 90;
                case OuterShadowEnum.BottomLeft:
                    return 135;
                case OuterShadowEnum.Right:
                    return 0;
                case OuterShadowEnum.Center:
                    return 0;
                case OuterShadowEnum.Left:
                    return 180;
                case OuterShadowEnum.TopRight:
                    return -45;
                case OuterShadowEnum.Top:
                    return -90;
                case OuterShadowEnum.TopLeft:
                    return 225;
                default:
                    return 0.0;
            }
        }

        /// <summary>
        /// Lấy góc quay cho hiệu ứng đổ bóng trong
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static double GetInnserShadowAngle(InnerShadowEnum type)
        {
            switch (type)
            {
                case InnerShadowEnum.BottomRight:
                    return 45;
                case InnerShadowEnum.Bottom:
                    return 90;
                case InnerShadowEnum.BottomLeft:
                    return 135;
                case InnerShadowEnum.Right:
                    return 0;
                case InnerShadowEnum.Center:
                    return 0;
                case InnerShadowEnum.Left:
                    return 180;
                case InnerShadowEnum.TopRight:
                    return -45;
                case InnerShadowEnum.Top:
                    return -90;
                case InnerShadowEnum.TopLeft:
                    return 225;
                default:
                    return 0.0;
            }
        }
        #endregion

        #region Reflections

        private static List<ReflectionIconModel> _reflectionIcons;
        /// <summary>
        /// Danh sách các biểu tượng của hiệu ứng phản chiếu
        /// </summary>
        public static List<ReflectionIconModel> ReflectionIcons
        {
            get { return _reflectionIcons ?? (_reflectionIcons = GetReflectionIcons()); }
        }

        private static List<ReflectionIconModel> GetReflectionIcons()
        {
            List<ReflectionIconModel> _result = new List<ReflectionIconModel>();

            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_NoneReflection")?.ToString(), RefHeight = 0.9, Distance = new Thickness(0, 22, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_NoneReflection")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.65, Distance = new Thickness(0, 22, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_TightTouching")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.5, Distance = new Thickness(0, 22, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_HalfTouching")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.35, Distance = new Thickness(0, 22, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_FullTouching")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.65, Distance = new Thickness(0, 25, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_Tight4")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.5, Distance = new Thickness(0, 25, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_Half4")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.35, Distance = new Thickness(0, 25, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_Full4")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.65, Distance = new Thickness(0, 28, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_Tight8")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.5, Distance = new Thickness(0, 28, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_Half8")?.ToString() });
            _result.Add(new ReflectionIconModel() { GroupName = FileHelper.FindResource("COREREFLECTIONEFFECT_ReflectionVariations")?.ToString(), RefHeight = 0.35, Distance = new Thickness(0, 28, 0, 0), Name = FileHelper.FindResource("COREREFLECTIONEFFECT_Full8")?.ToString() });

            return _result;
        }

        private static RelayCommand _reflectionCommand;
        /// <summary>
        /// Lệnh điều khiển cho cài đặt hiệu ứng đỗ bóng
        /// </summary>
        public static RelayCommand ReflectionCommand
        {
            get { return _reflectionCommand ?? (_reflectionCommand = new RelayCommand(ReflectionExcute)); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt hiệu ứng phẩn chiếu
        /// </summary>
        /// <param name="obj"></param>
        private static void ReflectionExcute(object obj)
        {
            if (obj is ReflectionIconModel refl)
            {
                Global.StartGrouping();
                var effect = new ReflectionEffect()
                {
                    Distance = (refl.Distance.Top - 21.9) * 3,
                    Size = (1 - refl.RefHeight) * 100,
                    Transparency = 0.5
                };
                if (refl.Name == "No Reflection") effect = new ReflectionEffect();
                foreach (var stElement in (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IEffectSupport))
                {
                    (stElement as IEffectSupport).Effects.Add(effect);
                }
                Global.StopGrouping();
            }
        }
        #endregion

        #region Glows
        private static List<GlowIconModel> _glowIcons;
        /// <summary>
        /// Danh sách các biểu tượng của hiệu ứng phản chiếu
        /// </summary>
        public static List<GlowIconModel> GlowIcons
        {
            get { return _glowIcons ?? (_glowIcons = GetGlowIcons()); }
        }

        private static List<GlowIconModel> GetGlowIcons()
        {
            string[] _colorsTheme = { "Accent1", "Accent2", "Accent3", "Accent4", "Accent5", "Accent6" };
            string[] _colorsThemeName = { FileHelper.FindResource("COREGLOWEFFECT_Accent1")?.ToString(), FileHelper.FindResource("COREGLOWEFFECT_Accent2")?.ToString(), FileHelper.FindResource("COREGLOWEFFECT_Accent3")?.ToString(), FileHelper.FindResource("COREGLOWEFFECT_Accent4")?.ToString(), FileHelper.FindResource("COREGLOWEFFECT_Accent5")?.ToString(), FileHelper.FindResource("COREGLOWEFFECT_Accent6")?.ToString() };
            double[] _blurRadius = { 5, 8, 11, 15 };
            var _souce = (Application.Current);
            List<GlowIconModel> _result = new List<GlowIconModel>();

            _result.Add(new GlowIconModel() { GroupName = FileHelper.FindResource("COREGLOWEFFECT_NoGlow")?.ToString(), Name = FileHelper.FindResource("COREGLOWEFFECT_NoGlow")?.ToString() });
            for (int i = 0; i < _blurRadius.Length; i++)
            {
                for (int j = 0; j < _colorsTheme.Length; j++)
                {
                    GlowIconModel _style = new GlowIconModel();
                    Binding _binding = new Binding($"SelectedTheme.Colors.{_colorsTheme[j]}.Color");
                    _binding.Source = _souce;
                    SolidColorBrush _solid = new SolidColorBrush();
                    BindingOperations.SetBinding(_solid, SolidColorBrush.ColorProperty, _binding);
                    _style.Color = _solid;
                    _style.GroupName = FileHelper.FindResource("COREGLOWEFFECT_GlowVariations")?.ToString();
                    // _style.Name = $"{_blurRadius[i]}px glow, Color {_colorsThemeName[j]}";
                    _style.Name = string.Format("{0}px {1}, {2} {3}", _blurRadius[i], FileHelper.FindResource("COREGLOWEFFECT_glow")?.ToString(), FileHelper.FindResource("COREGLOWEFFECT_Color")?.ToString(), _colorsThemeName[j]);
                    _style.BlurRadius = _blurRadius[i];
                    _result.Add(_style);
                }
            }
            return _result;
        }

        private static RelayCommand _glowCommand;
        /// <summary>
        /// Lệnh điều khiển cho cài đặt hiệu ứng tỏa sáng
        /// </summary>
        public static RelayCommand GlowCommand
        {
            get { return _glowCommand ?? (_glowCommand = new RelayCommand(GlowExcute)); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt hiệu ứng đổ bóng
        /// </summary>
        /// <param name="obj"></param>
        private static void GlowExcute(object obj)
        {
            if (obj is GlowIconModel glow)
            {
                Global.StartGrouping();
                var effect = new GlowEffect()
                {
                    Color = glow.Color?.ToString(),
                    Size = 10 + glow.BlurRadius,
                    Transparency = 1.0
                };
                foreach (var stElement in (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IEffectSupport))
                {
                    (stElement as IEffectSupport).Effects.Add(effect);
                }
                Global.StopGrouping();
            }
        }
        #endregion

        #region SoftEdges

        private static List<SoftEdgeIconModel> _softEdgeIcons;
        /// <summary>
        /// Danh sách các biểu tượng của hiệu ứng làm mờ
        /// </summary>
        public static List<SoftEdgeIconModel> SoftEdgeIcons
        {
            get { return _softEdgeIcons ?? (_softEdgeIcons = GetSoftEdgeIcons()); }
        }

        private static List<SoftEdgeIconModel> GetSoftEdgeIcons()
        {
            List<SoftEdgeIconModel> _result = new List<SoftEdgeIconModel>();

            _result.Add(new SoftEdgeIconModel() { GroupName = FileHelper.FindResource("CORESOFTEDGEEFFECT_NoSoftEdge")?.ToString(), Name = FileHelper.FindResource("CORESOFTEDGEEFFECT_NoSoftEdge")?.ToString() });
            _result.Add(new SoftEdgeIconModel() { GroupName = FileHelper.FindResource("CORESOFTEDGEEFFECT_SoftEdgesVariations")?.ToString(), Size = new Thickness(9), Name = "1 Pixel", DSize = 2.5 });
            _result.Add(new SoftEdgeIconModel() { GroupName = FileHelper.FindResource("CORESOFTEDGEEFFECT_SoftEdgesVariations")?.ToString(), Size = new Thickness(10), Name = "2.5 Pixel", DSize = 5.0 });
            _result.Add(new SoftEdgeIconModel() { GroupName = FileHelper.FindResource("CORESOFTEDGEEFFECT_SoftEdgesVariations")?.ToString(), Size = new Thickness(10.5), Name = "5 Pixel", DSize = 7.5 });
            _result.Add(new SoftEdgeIconModel() { GroupName = FileHelper.FindResource("CORESOFTEDGEEFFECT_SoftEdgesVariations")?.ToString(), Size = new Thickness(12), Name = "10 Pixel", DSize = 10 });
            _result.Add(new SoftEdgeIconModel() { GroupName = FileHelper.FindResource("CORESOFTEDGEEFFECT_SoftEdgesVariations")?.ToString(), Size = new Thickness(13.5), Name = "25 Pixel", DSize = 25 });
            _result.Add(new SoftEdgeIconModel() { GroupName = FileHelper.FindResource("CORESOFTEDGEEFFECT_SoftEdgesVariations")?.ToString(), Size = new Thickness(15), Name = "50 Pixel", DSize = 50 });

            return _result;
        }

        private static RelayCommand _softEdgeCommand;
        /// <summary>
        /// Lệnh điều khiển cho cài đặt hiệu ứng làm mềm
        /// </summary>
        public static RelayCommand SoftEdgeCommand
        {
            get { return _softEdgeCommand ?? (_softEdgeCommand = new RelayCommand(SoftEdgeExcute)); }
        }

        /// <summary>
        /// Thực thi điều khiển cài đặt hiệu ứng làm mềm
        /// </summary>
        /// <param name="obj"></param>
        private static void SoftEdgeExcute(object obj)
        {
            if (obj is SoftEdgeIconModel softEdge)
            {
                Global.StartGrouping();
                var effect = new SoftEdgeEffect()
                {
                    Size = softEdge.DSize,
                    Transparency = 1.0
                };
                foreach (var stElement in (Application.Current as IAppGlobal).SelectedElements.Where(x => x is IEffectSupport))
                {
                    (stElement as IEffectSupport).Effects.Add(effect);
                }
                Global.StopGrouping();
            }
        }
        #endregion

        #endregion
    }

    public class SoftEdgeIconModel
    {
        /// <summary>
        /// Tên hiển thị
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Nhóm
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// Kích thước
        /// </summary>
        public Thickness Size { get; set; }

        /// <summary>
        /// Kích thước làm mờ
        /// </summary>
        public double DSize { get; set; }
    }

    public class GlowIconModel : RootViewModel
    {
        /// <summary>
        /// Tên hiển thị
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Nhóm
        /// </summary>
        public string GroupName { get; set; }
        private SolidColorBrush _color;
        /// <summary>
        /// Màu sắc
        /// </summary>
        public SolidColorBrush Color
        {
            get { return _color; }
            set { _color = value; OnPropertyChanged("Color"); }
        }

        /// <summary>
        /// Độ mờ
        /// </summary>
        public double BlurRadius { get; set; }
    }

    public class ReflectionIconModel
    {
        /// <summary>
        /// Tên hiển thị
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Khoảng cách bóng
        /// </summary>
        public Thickness Distance { get; set; }
        /// <summary>
        /// Độ rộng bóng
        /// </summary>
        public double RefHeight { get; set; }
        /// <summary>
        /// Tên nhóm
        /// </summary>
        public string GroupName { get; set; }
    }

    public class ShadowIconModel
    {
        /// <summary>
        /// Tên hiển thị
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Kiểu hiệu ứng
        /// </summary>
        public object EnumType { get; set; }

        /// <summary>
        /// Icon đại diện
        /// </summary>
        public object IconResource { get; set; }

        /// <summary>
        /// Tên nhóm
        /// </summary>
        public string GroupName { get; set; }
    }
}

﻿using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class PerspectiveShadowLeftConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _left = (double)values[0];
            var _angle = (double)values[3];
            var _anglerad = Math.PI * _angle / 180;
            var _distance = (double)values[2];
            var _perspectiveType = (PerspectiveShadowEnum)values[4];
            double _leftAngle = _distance * Math.Cos(_anglerad);
            ObjectElement _object = parameter as ObjectElement;
            var newWidth = _object.RectBound.Height * Math.Tan(15 * Math.PI / 180);
            if (_perspectiveType == PerspectiveShadowEnum.UpperLeft || _perspectiveType == PerspectiveShadowEnum.LowerLeft)
            {
                return _left - newWidth / 2 + _leftAngle;//AngeSkew = 15

            }
            else if (_perspectiveType == PerspectiveShadowEnum.UpperRight || _perspectiveType == PerspectiveShadowEnum.LowerRight)
            {
                return _left + newWidth / 2 + _leftAngle;//AngeSkew = -15
            }
            else return _left + _leftAngle;

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper.Effect
{
    public class ReflectionGradienStopColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string _colorString = value.ToString();
            return (Color)ColorConverter.ConvertFromString(_colorString);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

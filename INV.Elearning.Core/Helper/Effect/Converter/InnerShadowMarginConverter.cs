﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class InnerShadowMarginConverter : IMultiValueConverter
    {        
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _angle = (double)values[0];
            var _anglerad = Math.PI * (double)values[0] / 180;
            var _distance = (double)values[1];
            double _marginRight = _distance * Math.Cos(_anglerad);
            double _marginTop = _distance * Math.Sin(_anglerad);

            if (_angle >= 0 && _angle < 90)
            {
                return new Thickness(0, 0, _marginRight * 2, _marginTop * 2);
            }
            else if (_angle >= 90 && _angle < 180)
            {
                return new Thickness(-_marginRight * 2, 0, 0, _marginTop * 2);
            }
            else if (_angle >= 180 && _angle < 270)
            {
                return new Thickness(-_marginRight * 2, -_marginTop * 2, 0, 0);
            }
            else
            {
                return new Thickness(0, -_marginTop * 2, _marginRight * 2, 0);
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

﻿using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class PerspectiveShadowTopConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _angle = (double)values[0];
            var _anglerad = Math.PI * (double)values[2] / 180;
            var _distance = (double)values[3];
            //var _perspectiveType = (PerspectiveShadowEnum)values[4];
            double _topAngle = _distance * Math.Sin(_anglerad);
            ObjectElement _object = parameter as ObjectElement;
            double top = LocationHelper.GetMinY(parameter as FrameworkElement, _angle);
            return top - _object.RectBound.Height + _topAngle;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;


namespace INV.Elearning.Core.Helper.Effect
{
    public class OuterShadowRenderTransformOriginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _outerShadowType = (OuterShadowEnum)value;
            if (_outerShadowType == OuterShadowEnum.BottomRight)
            {
                return new Point(0, 0);
            }
            else if (_outerShadowType == OuterShadowEnum.Bottom)
            {
                return new Point(0.5, 0);
            }
            else  if (_outerShadowType == OuterShadowEnum.BottomLeft)
            {
                return new Point(1, 0);
            }
            else if(_outerShadowType == OuterShadowEnum.Right)
            {
                return new Point(0, 0.5);
            }
            else if(_outerShadowType == OuterShadowEnum.Center)
            {
                return new Point(0.5, 0.5);
            }
            else if (_outerShadowType == OuterShadowEnum.Left)
            {
                return new Point(1, 0.5);
            }
            else if (_outerShadowType == OuterShadowEnum.TopRight)
            {
                return new Point(0, 1);
            }
            else if (_outerShadowType == OuterShadowEnum.Top)
            {
                return new Point(0.5, 1);
            }
            else return new Point(1, 1);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

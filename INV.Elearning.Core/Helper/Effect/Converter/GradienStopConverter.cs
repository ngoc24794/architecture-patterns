﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class GradienStopConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _gradienStop = (double)value;
            if (_gradienStop == 0) _gradienStop = 1;
            return (1 - _gradienStop / 100);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

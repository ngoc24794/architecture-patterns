﻿using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class PerspectiveShadowCenterXConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _perspectiveType = (PerspectiveShadowEnum)values[2];
            ObjectElement _object = (ObjectElement)parameter;
            if (_perspectiveType == PerspectiveShadowEnum.UpperLeft || _perspectiveType == PerspectiveShadowEnum.LowerLeft)
            {
                return (_object.RectBound.Width + _object.RectBound.Height * Math.Tan(15 * Math.PI / 180)) / 2;//SkewAngle = 15

            }
            if (_perspectiveType == PerspectiveShadowEnum.UpperRight || _perspectiveType == PerspectiveShadowEnum.LowerRight)
            {
                return -(_object.RectBound.Width + _object.RectBound.Height * Math.Tan(15 * Math.PI / 180)) / 2;//SkewAngle = -15
            }
            else return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

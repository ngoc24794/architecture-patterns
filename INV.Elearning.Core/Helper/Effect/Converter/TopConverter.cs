﻿using INV.Elearning.Core.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class TopConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _angle = (double)values[0];
            var _distance = (double)values[1];
            double top  = LocationHelper.GetMinY(parameter as FrameworkElement, _angle);
            if (double.IsNaN(top)) top = (double)values[4];
            return top +_distance;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}


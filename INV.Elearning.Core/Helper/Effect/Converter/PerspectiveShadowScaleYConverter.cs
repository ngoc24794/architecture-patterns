﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class PerspectiveShadowScaleYConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _size = (double)values[0];
            var _perspectiveType = (PerspectiveShadowEnum)values[1];
            var _isSize = (bool)values[2];
            if (_perspectiveType == PerspectiveShadowEnum.UpperLeft || _perspectiveType == PerspectiveShadowEnum.UpperRight)
            {
                if (_isSize)
                {
                    return _size;
                }
                else return 0.3;
            }
            else
            {
                if (_isSize)
                {
                    return -_size;
                }
                else return -0.3;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

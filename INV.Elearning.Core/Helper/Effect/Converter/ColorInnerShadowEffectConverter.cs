﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper.Effect
{
    public class ColorInnerShadowEffectConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            ColorBrushBase colorBrushBase = values[0] as ColorBrushBase;
            bool isSession = (bool)values[1];

            if (colorBrushBase == null || colorBrushBase.Brush.ToString() == Brushes.Transparent.ToString() || isSession)
            {
                return Visibility.Hidden;
            }
            return Visibility.Visible;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: SoftEdgeVisibilityConverter.cs
// Description: Cài đặt SoftEdgeVisibilityConverter
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace INV.Elearning.Core.Helper.Effect
{
    public class SoftEdgeVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _visual = (VisualBrush)parameter;
            if (!(bool)value)
            {
                return _visual;
            }
            else return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

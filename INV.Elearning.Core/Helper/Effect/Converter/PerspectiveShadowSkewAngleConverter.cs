﻿
using System;
using System.Globalization;
using System.Windows.Data;

namespace INV.Elearning.Core.Helper.Effect
{
    public class PerspectiveShadowSkewAngleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var _perspectiveType = (PerspectiveShadowEnum)value;
            switch (_perspectiveType)
            {
                case PerspectiveShadowEnum.UpperLeft:
                    return 15;
                case PerspectiveShadowEnum.UpperRight:
                    return -15;
                case PerspectiveShadowEnum.Below:
                    return 0;
                case PerspectiveShadowEnum.LowerLeft:
                    return 15;
                case PerspectiveShadowEnum.LowerRight:
                    return -15;
            }
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

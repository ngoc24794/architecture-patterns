﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: InnerShadowPathGeometryConverter.cs
// Description: Cài đặt InnerShadowPathGeometryConverter
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------

using INV.Elearing.Controls.Shapes.Helpers;
using INV.Elearing.Controls.Shapes.Stars;
using INV.Elearning.Core.View;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace INV.Elearning.Core.Helper.Effect
{
    public class InnerShadowPathGeometryConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _distance = (double)values[0];
            var _angle = (double)values[1];
            var _anglerad = Math.PI * _angle / 180;
            var _blur = (double)values[2];
            double _marginRight = _distance * Math.Cos(_anglerad);
            double _marginTop = _distance * Math.Sin(_anglerad);


            StandardElement _object = parameter as StandardElement;
            if (_object.ShapePresent != null)
            {
                //_pathGeo.AddGeometry(_object.ShapePresent.PathGeometry);
                //PathGeometry _pathGeo2 = new PathGeometry();
                //PathGeometry transformGeometry = GeometryHelpers.AddPadding(_pathGeo, _blur / 2);
                //PathGeometry translateGeometry = CalculateData(transformGeometry, -_marginRight / 2, -_marginTop / 2);
                //_pathGeo2.AddGeometry(translateGeometry);

                //CombinedGeometry _combined = new CombinedGeometry();
                //_combined.Geometry1 = _pathGeo;
                //_combined.Geometry2 = _pathGeo2;
                //_combined.GeometryCombineMode = GeometryCombineMode.Exclude;
                if(_object.ShapePresent is StarShapeBase)
                {
                    PathGeometry _pathGeo = new PathGeometry();
                    _pathGeo.AddGeometry(_object.ShapePresent.PathGeometry);
                    PathGeometry _combined = new PathGeometry();
                    _combined = CalculateData(_pathGeo, -_marginRight / 2, -_marginTop / 2);
                    return _combined;
                }
                else
                {
                    PathGeometry _pathGeo = new PathGeometry();
                    _pathGeo = GeometryHelpers.SingleFigureGeometry(_object.ShapePresent.PathGeometry);
                    PathGeometry _combined = new PathGeometry();
                    _combined = CalculateData(_pathGeo, -_marginRight / 2, -_marginTop / 2);
                    return _combined;
                }
               
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }

        /// <summary>
        /// Tính toán kích thước của đường Path khi dịch chuyển 1 đơn vị
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="deltaX"></param>
        /// <param name="deltaY"></param>
        /// <returns></returns>
        public PathGeometry CalculateData(PathGeometry shape, double deltaX, double deltaY)
        {

            PathGeometry pathGeometry = new PathGeometry();
            PathFigureCollection figureCollection = new PathFigureCollection();


            foreach (PathFigure figure in shape.Figures)
            {
                PathSegmentCollection segmentCollection = new PathSegmentCollection();
                foreach (PathSegment segment in figure.Segments)
                {
                    if (segment is LineSegment)
                    {
                        LineSegment line = segment as LineSegment;
                        LineSegment newLine = line.Clone();
                        newLine.Point = TranslatePoint(line.Point, deltaX, deltaY);
                        segmentCollection.Add(newLine);
                    }
                    else if (segment is PolyLineSegment)
                    {
                        PolyLineSegment polyLine = segment as PolyLineSegment;
                        PolyLineSegment newPolyLine = polyLine.Clone();
                        newPolyLine.Points = TranslatePointCollection(polyLine.Points, deltaX, deltaY);
                        segmentCollection.Add(newPolyLine);
                    }
                    else if (segment is ArcSegment)
                    {
                        ArcSegment arc = segment as ArcSegment;
                        ArcSegment newArc = arc.Clone();
                        newArc.Point = TranslatePoint(arc.Point, deltaX, deltaY);
                        segmentCollection.Add(newArc);
                    }
                    else if (segment is BezierSegment)
                    {
                        BezierSegment curve = segment as BezierSegment;
                        BezierSegment bezierSegment = curve.Clone();
                        bezierSegment.Point1 = TranslatePoint(curve.Point1, deltaX, deltaY);
                        bezierSegment.Point2 = TranslatePoint(curve.Point2, deltaX, deltaY);
                        bezierSegment.Point3 = TranslatePoint(curve.Point3, deltaX, deltaY);
                        segmentCollection.Add(bezierSegment);
                    }
                    else if (segment is PolyBezierSegment)
                    {
                        PolyBezierSegment polyCurve = segment as PolyBezierSegment;
                        PolyBezierSegment polyBezierSegment = polyCurve.Clone();
                        polyBezierSegment.Points = TranslatePointCollection(polyCurve.Points, deltaX, deltaY);
                        segmentCollection.Add(polyBezierSegment);
                    }
                }

                PathFigure newFigure = new PathFigure
                {
                    StartPoint = TranslatePoint(figure.StartPoint, deltaX, deltaY),
                    IsClosed = figure.IsClosed,
                    IsFilled = figure.IsFilled,
                    Segments = segmentCollection
                };

                figureCollection.Add(newFigure);
            }

            pathGeometry.Figures = figureCollection;
            return pathGeometry;
        }

      
        /// <summary>
        /// Tịnh tiến điểm theo vector 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="deltaX"></param>
        /// <param name="deltaY"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        private Point TranslatePoint(Point point, double deltaX, double deltaY, int decimals = 2)
        {
            return new Point()
            {
                X = Math.Round(point.X + deltaX, decimals),
                Y = Math.Round(point.Y + deltaY, decimals)
            };
        }

        /// <summary>
        /// Tịnh tiến tập điểm theo vector
        /// </summary>
        /// <param name="points"></param>
        /// <param name="deltaX"></param>
        /// <param name="deltaY"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        private PointCollection TranslatePointCollection(PointCollection points, double deltaX, double deltaY, int decimals = 2)
        {
            PointCollection result = new PointCollection();
            foreach (Point point in points)
            {
                Point pResult = new Point()
                {
                    X = Math.Round(point.X + deltaX, decimals),
                    Y = Math.Round(point.Y + deltaY, decimals)
                };
                result.Add(pResult);
            }

            return result;
        }       
    }
}

﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: InnerShadowPathGeometryShadow2Converter.cs
// Description: Cài đặt hiệu ứng InnerShadowEffect
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------

using INV.Elearning.Core.View;
using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;

namespace INV.Elearning.Core.Helper.Effect
{
    public class InnerShadowPathGeometryShadow2Converter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var _distance = (double)values[0];
            var _angle = (double)values[1];
            var _anglerad = Math.PI * _angle / 180;

            double _marginRight = _distance * Math.Cos(_anglerad);
            double _marginTop = _distance * Math.Sin(_anglerad);


            ObjectElement _object = parameter as ObjectElement;
            //var p = (_object.Content as Grid).Children[0];
            var p = _object.Content;
            if (p is Path)
            {
                PathGeometry _pathGeo = new PathGeometry();
                _pathGeo.AddGeometry((p as Path).Data);
                PathGeometry _pathGeo2 = new PathGeometry();

                _pathGeo2.AddGeometry(CalculateData(_pathGeo, -_marginRight, -_marginTop));

                CombinedGeometry _combined = new CombinedGeometry();
                _combined.Geometry1 = _pathGeo;
                _combined.Geometry2 = _pathGeo2;
                _combined.GeometryCombineMode = GeometryCombineMode.Intersect;
                return _combined;
            }
            else
            {
                FrameworkElement _framework = p as FrameworkElement;

                RectangleGeometry _rect1 = new RectangleGeometry(new Rect(0, 0, _framework.ActualWidth, _framework.ActualHeight));
                RectangleGeometry _rect2 = new RectangleGeometry(new Rect(-_marginRight, -_marginTop, _framework.ActualWidth, _framework.ActualHeight));
                CombinedGeometry _combined = new CombinedGeometry();
                _combined.Geometry1 = _rect1;
                _combined.Geometry2 = _rect2;
                _combined.GeometryCombineMode = GeometryCombineMode.Intersect;
                return _combined;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }

        public PathGeometry CalculateData(PathGeometry shape, double deltaX, double deltaY)
        {

            PathGeometry pathGeometry = new PathGeometry();
            PathFigureCollection figureCollection = new PathFigureCollection();


            foreach (PathFigure figure in shape.Figures)
            {
                PathSegmentCollection segmentCollection = new PathSegmentCollection();
                foreach (PathSegment segment in figure.Segments)
                {
                    if (segment is LineSegment)
                    {
                        LineSegment line = segment as LineSegment;
                        LineSegment newLine = line.Clone();
                        newLine.Point = TranslatePoint(line.Point, deltaX, deltaY);
                        segmentCollection.Add(newLine);
                    }
                    else if (segment is PolyLineSegment)
                    {
                        PolyLineSegment polyLine = segment as PolyLineSegment;
                        PolyLineSegment newPolyLine = polyLine.Clone();
                        newPolyLine.Points = TranslatePointCollection(polyLine.Points, deltaX, deltaY);
                        segmentCollection.Add(newPolyLine);
                    }
                    else if (segment is ArcSegment)
                    {
                        ArcSegment arc = segment as ArcSegment;
                        ArcSegment newArc = arc.Clone();
                        newArc.Point = TranslatePoint(arc.Point, deltaX, deltaY);
                        segmentCollection.Add(newArc);
                    }
                    else if (segment is BezierSegment)
                    {
                        BezierSegment curve = segment as BezierSegment;
                        BezierSegment bezierSegment = curve.Clone();
                        bezierSegment.Point1 = TranslatePoint(curve.Point1, deltaX, deltaY);
                        bezierSegment.Point2 = TranslatePoint(curve.Point2, deltaX, deltaY);
                        bezierSegment.Point3 = TranslatePoint(curve.Point3, deltaX, deltaY);
                        segmentCollection.Add(bezierSegment);
                    }
                    else if (segment is PolyBezierSegment)
                    {
                        PolyBezierSegment polyCurve = segment as PolyBezierSegment;
                        PolyBezierSegment polyBezierSegment = polyCurve.Clone();
                        polyBezierSegment.Points = TranslatePointCollection(polyCurve.Points, deltaX, deltaY);
                        segmentCollection.Add(polyBezierSegment);
                    }
                }

                PathFigure newFigure = new PathFigure
                {
                    StartPoint = TranslatePoint(figure.StartPoint, deltaX, deltaY),
                    IsClosed = figure.IsClosed,
                    IsFilled = figure.IsFilled,
                    Segments = segmentCollection
                };

                figureCollection.Add(newFigure);
            }

            pathGeometry.Figures = figureCollection;


            return pathGeometry;
        }

        /// <summary>
        /// Tịnh tiến điểm theo vector 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="deltaX"></param>
        /// <param name="deltaY"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        private Point TranslatePoint(Point point, double deltaX, double deltaY, int decimals = 2)
        {
            return new Point()
            {
                X = Math.Round(point.X + deltaX, decimals),
                Y = Math.Round(point.Y + deltaY, decimals)
            };
        }


        /// <summary>
        /// Tịnh tiến tập điểm theo vector
        /// </summary>
        /// <param name="points"></param>
        /// <param name="deltaX"></param>
        /// <param name="deltaY"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        private PointCollection TranslatePointCollection(PointCollection points, double deltaX, double deltaY, int decimals = 2)
        {
            PointCollection result = new PointCollection();
            foreach (Point point in points)
            {
                Point pResult = new Point()
                {
                    X = Math.Round(point.X + deltaX, decimals),
                    Y = Math.Round(point.Y + deltaY, decimals)
                };
                result.Add(pResult);
            }

            return result;
        }
    }
}

﻿//---------------------------------------------------------------------------
// Copyright (C)Huong Viet Group.  All rights reserved.
// File: ReflectionEffectRectangle.cs
// Description: Cài đặt hiệu ứng ReflectionEffect
// Develope by : Nguyen Quang Trieu
// History:
// 03/02/2018 : Create
//---------------------------------------------------------------------------

using INV.Elearning.Core.Model;
using INV.Elearning.Core.View;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace INV.Elearning.Core.Helper.Effect
{
    public class ReflectionEffectRectangle : ContentControl
    {
        /// <summary>
        /// Đối tượng chủ
        /// </summary>
        public FrameworkElement Owner { get; private set; }

        public ReflectionEffectRectangle(StandardElement element, ReflectionEffect reflectionEffect)
        {
            Owner = element;
            #region Reflection
            Rectangle _reflectionRect = new Rectangle();//Tạo một rectTangle chứa hình ảnh phản chiếu
            _reflectionRect.Stretch = Stretch.Fill;
            _reflectionRect.IsHitTestVisible = false;

            Binding _binding = new Binding();//Binding Width
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualWidth");
            BindingOperations.SetBinding(this, WidthProperty, _binding);

            _binding = new Binding();//Binding Height
            _binding.Source = element;
            _binding.Path = new PropertyPath("ActualHeight");
            BindingOperations.SetBinding(this, HeightProperty, _binding);

            _binding = new Binding();//Binding Left
            _binding.Source = element;
            _binding.Path = new PropertyPath("Left");
            BindingOperations.SetBinding(this, Canvas.LeftProperty, _binding);

            MultiBinding _multiBinding = new MultiBinding();//Binding Top
            _multiBinding.Converter = new TopConverter();
            _multiBinding.ConverterParameter = element;
            _multiBinding.Bindings.Add(new Binding("Angle") { Source = element });
            _multiBinding.Bindings.Add(new Binding("Distance") { Source = reflectionEffect });
            _multiBinding.Bindings.Add(new Binding("Top") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ActualWidth") { Source = element });
            _multiBinding.Bindings.Add(new Binding("ActualHeight") { Source = element });
            _multiBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            BindingOperations.SetBinding(this, Canvas.TopProperty, _multiBinding);

            RotateTransform _rotateTranformShadow = new RotateTransform();//Thay đổi góc quay
            _reflectionRect.RenderTransform = _rotateTranformShadow;
            _reflectionRect.RenderTransformOrigin = new Point(0.5, 0.5);

            VisualBrush _visual = new VisualBrush();//Lấy VisualBrush của ObjectElement gán vào hình phản chiếu
            _visual.Visual = element.ShapePresent;
            _visual.Stretch = Stretch.None;
            _visual.TileMode = TileMode.None;
            _reflectionRect.Fill = _visual;

            _binding = new Binding();//Binding Transparency
            _binding.Converter = new TransparencyConverter();
            _binding.Source = reflectionEffect;
            _binding.Path = new PropertyPath("Transparency");
            BindingOperations.SetBinding(_reflectionRect, OpacityProperty, _binding);

            ScaleTransform _scale = new ScaleTransform(); //Quay ngược hình ảnh để tạo phản chiếu
            _scale.ScaleX = 1;
            _scale.ScaleY = -1;
            _reflectionRect.LayoutTransform = _scale;

            _rotateTranformShadow = new RotateTransform();
            _reflectionRect.RenderTransform = _rotateTranformShadow;
            _reflectionRect.RenderTransformOrigin = new Point(0.5, 0.5);

            _binding = new Binding(); //Cài đặt binding cho các thuộc tính
            _binding.Converter = new AngleReflectionConverter();
            _binding.Source = element;
            _binding.Path = new PropertyPath("Angle");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rotateTranformShadow, RotateTransform.AngleProperty, _binding);

            LinearGradientBrush _linear = new LinearGradientBrush(); //Tạo vùng nhìn thấy cho hình phản chiếu
            var _stopPoint = new GradientStop();
            _stopPoint.Color = Colors.Black;

            _stopPoint.Offset = 1;
            _linear.GradientStops.Add(_stopPoint);

            _stopPoint = new GradientStop();
            _stopPoint.Color = Colors.Transparent;

            _binding = new Binding("Size");//Binding Size
            _binding.Converter = new GradienStopConverter();
            _binding.Source = reflectionEffect;
            BindingOperations.SetBinding(_stopPoint, GradientStop.OffsetProperty, _binding);
            _linear.GradientStops.Add(_stopPoint);

            _rotateTranformShadow = new RotateTransform();//Thay đổi góc quay của màu bên trong hình ảnh phản chiếu
            _rotateTranformShadow.CenterX = 0.5;
            _rotateTranformShadow.CenterY = 0.5;
            _binding = new Binding(); //Binding Angle
            _binding.Source = element;
            _binding.Converter = new AngleConverter2();
            _binding.Path = new PropertyPath("Angle");
            _binding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(_rotateTranformShadow, RotateTransform.AngleProperty, _binding);

            _linear.RelativeTransform = _rotateTranformShadow;
            _linear.StartPoint = new Point(0, 0);
            _linear.EndPoint = new Point(0, 1);
            _reflectionRect.OpacityMask = _linear;

            BlurEffect _blur = new BlurEffect();
            _binding = new Binding(); //Binding Blur
            _binding.Source = reflectionEffect;
            _binding.Path = new PropertyPath("Blur");
            BindingOperations.SetBinding(_blur, BlurEffect.RadiusProperty, _binding);
            _reflectionRect.Effect = _blur;


            IsHitTestVisible = false;
            Content = _reflectionRect;

            //Cài đặt Binding Ẩn hiện
            //_binding = new Binding();
            //_binding.Source = element;
            //_binding.Path = new PropertyPath("InSession");
            //_binding.Converter = Converters.InBooleanToVisibilityConverter;
            //BindingOperations.SetBinding(this, VisibilityProperty, _binding);

            _multiBinding = new MultiBinding();
            _multiBinding.Bindings.Add(new Binding("InSession") { Source = element });
            _multiBinding.Bindings.Add(new Binding("IsShow") { Source = element });
            _multiBinding.Converter = new ReflectionShadowVisibilityConverter();
            BindingOperations.SetBinding(this, VisibilityProperty, _multiBinding);

            #endregion
        }
    }
}

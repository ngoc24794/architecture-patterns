﻿using System.Windows;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Lớp ủy nhiệm kết nối
    /// </summary>
    public class BindingProxy : Freezable
    {
        /// <summary>
        /// Ghi đè các phương thức
        /// </summary>
        /// <returns></returns>
        protected override Freezable CreateInstanceCore()
        {
            return new BindingProxy();
        }

        /// <summary>
        /// Dữ liệu mang
        /// </summary>
        public object Data
        {
            get { return (object)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object),
            typeof(BindingProxy), new UIPropertyMetadata(null));
    }
}

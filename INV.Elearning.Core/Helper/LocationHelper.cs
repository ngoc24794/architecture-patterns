﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace INV.Elearning.Core.Helper
{
    public class LocationHelper
    {
        #region ---------------Hàm phụ tính khoảng cách và góc của ảnh phản chiếu-------------

        
        /// <summary>
        /// Lấy điểm có tọa độ Y nhỏ nhất
        /// </summary>
        /// <param name="element"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static double GetMinY(FrameworkElement element, double angle)
        {
            if (angle > 360) //Làm trồn các góc quay để nhỏ hơn 360
            {
                angle = Math.Round(angle % 360, 2, MidpointRounding.AwayFromZero);
            }

            Point _centerPoint = new Point(element.Width / 2.0 + Canvas.GetLeft(element), element.Height / 2.0 + Canvas.GetTop(element));
            Point _oldHighLeftConner = GetHighLeftConner(element);
            Point _oldLowLeftConner = GetLowLeftConner(element);
            Point _oldLowRightConner = GetLowRightConner(element);
            Point _oldHighRightConner = GetHeightRightConner(element);

            List<Point> _lstOldPoint = new List<Point>();
            _lstOldPoint.Add(_oldHighLeftConner);
            _lstOldPoint.Add(_oldLowLeftConner);
            _lstOldPoint.Add(_oldLowRightConner);
            _lstOldPoint.Add(_oldHighRightConner);

            Point _newHighLeftConner = GetNewPoint(_oldHighLeftConner, _centerPoint, angle * Math.PI / 180);
            Point _newLowLeftConner = GetNewPoint(_oldLowLeftConner, _centerPoint, angle * Math.PI / 180);
            Point _newLowRightConner = GetNewPoint(_oldLowRightConner, _centerPoint, angle * Math.PI / 180);
            Point _newHighRightConner = GetNewPoint(_oldHighRightConner, _centerPoint, angle * Math.PI / 180);

            List<Point> _lstNewPoint = new List<Point>();
            _lstNewPoint.Add(_newHighLeftConner);
            _lstNewPoint.Add(_newLowLeftConner);
            _lstNewPoint.Add(_newLowRightConner);
            _lstNewPoint.Add(_newHighRightConner);

            double _oldtop = GetMaxTop(_lstOldPoint).Y;
            double _top = GetMaxTop(_lstNewPoint).Y;

            return _top + _top - _oldtop;
        }

        /// <summary>
        /// Lấy điểm có giá tị Y lớn nhất
        /// </summary>
        /// <param name="_lstPoint"></param>
        /// <returns></returns>
        private static Point GetMaxTop(List<Point> _lstPoint)
        {
            Point _temp = new Point(0, 0);
            for (int i = 0; i < _lstPoint.Count - 1; i++)
            {
                for (int j = i + 1; j < _lstPoint.Count; j++)
                {
                    if (_lstPoint[j].Y < _lstPoint[i].Y)
                    {
                        _temp = _lstPoint[i];
                        _lstPoint[i] = _lstPoint[j];
                        _lstPoint[j] = _temp;
                    }
                }

            }

            _temp = _lstPoint[_lstPoint.Count - 1];
            return _temp;
        }

        /// <summary>
        /// Lấy điểm cao bên góc trái
        /// </summary>
        /// <param name="rec"></param>
        /// <returns></returns>
        private static Point GetHighLeftConner(FrameworkElement rec)
        {
            return new Point(Canvas.GetLeft(rec), Canvas.GetTop(rec));
        }
        /// <summary>
        /// Xác định tọa độ đỉnh trái thấp
        /// </summary>
        /// <param name="rec"></param>
        /// <returns></returns>
        private static Point GetLowLeftConner(FrameworkElement rec)
        {
            return new Point(Canvas.GetLeft(rec), Canvas.GetTop(rec) + rec.Height);
        }

        /// <summary>
        /// Xác định tọa độ đỉnh phải cao
        /// </summary>
        /// <param name="rec"></param>
        /// <returns></returns>
        private static Point GetLowRightConner(FrameworkElement rec)
        {
            return new Point(Canvas.GetLeft(rec) + rec.Width, Canvas.GetTop(rec) + rec.Height);
        }
        /// <summary>
        /// Xác định tọa độ đỉnh phải thấp
        /// </summary>
        /// <param name="rec"></param>
        /// <returns></returns>
        private static Point GetHeightRightConner(FrameworkElement rec)
        {
            return new Point(Canvas.GetLeft(rec) + rec.Width, Canvas.GetTop(rec));
        }

        /// <summary>
        /// Xác định điểm mới khi xoay một góc
        /// </summary>
        /// <param name="oldPoint">Tọa độ điểm củ</param>
        /// <param name="centerPoint">Tọa độ tâm xoay</param>
        /// <param name="angle">Góc quay</param>
        /// <returns></returns>
        private static Point GetNewPoint(Point oldPoint, Point centerPoint, double angle)
        {
            double _xNew = centerPoint.X + (oldPoint.X - centerPoint.X) * Math.Cos(angle) - (oldPoint.Y - centerPoint.Y) * Math.Sin(angle);
            double _yNew = centerPoint.Y + (oldPoint.X - centerPoint.X) * Math.Sin(angle) + (oldPoint.Y - centerPoint.Y) * Math.Cos(angle);
            return new Point(_xNew, _yNew);
        }
        #endregion
    }

    public class ABC
    {
        private static Point GetNewPoint(double x, double y, double centerX, double centerY, double angle)
        {
            double _xNew = centerX + (x - centerX) * Math.Cos(angle) - (y - centerY) * Math.Sin(angle);
            double _yNew = centerY + (x - centerX) * Math.Sin(angle) + (y - centerY) * Math.Cos(angle);
            return new Point(_xNew, _yNew);
        }
    }
}

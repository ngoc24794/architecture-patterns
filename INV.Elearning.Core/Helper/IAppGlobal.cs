﻿using INV.Elearning.Animations;
using INV.Elearning.Core.Model;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.Core.View;
using INV.Elearning.Core.View.Theme;
using INV.Elearning.Core.ViewModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Interface cho lớp mang giá trị toàn ứng dụng
    /// </summary>
    public interface IAppGlobal : IThemesControl
    {
        /// <summary>
        /// Đường dẫn thư mục dữ liệu
        /// </summary>
        string ElearningAppDataFolder { get; }

        /// <summary>
        /// Đường dẫn thư mục tạm
        /// </summary>
        string DocumentTempFolder { get; }

        /// <summary>
        /// Thư mục chứa nội dung
        /// </summary>
        string Texture { get; }

        /// <summary>
        /// Tỷ lệ thu phóng của trang soạn thảo hiện tại
        /// </summary>
        double DocumentPageScale { set; get; }

        /// <summary>
        /// Thông tin tiến trình đang chạy
        /// </summary>
        string ProcessMessage { set; get; }

        /// <summary>
        /// Màu sắc hình nền tiến trình đang chạy
        /// </summary>
        System.Windows.Media.Brush StatusBarBackground { set; get; }

        /// <summary>
        /// Mức hoàn thành tiến trình đang chạy
        /// </summary>
        int ProcessPercents { set; get; }

        /// <summary>
        /// Đang trong tiến trình
        /// </summary>
        bool OnProcessing { set; get; }

        /// <summary>
        /// Danh sách các phần tử thuộc layout đang được lựa chọn
        /// </summary>
        ObservableCollection<ObjectElement> SelectedElements { get; }

        /// <summary>
        /// Danh sách các Slide đang được lựa chọn
        /// </summary>
        ObservableCollection<SlideBase> SelectedSlides { get; }

        /// <summary>
        /// Danh sách các phần tử nội dung đang được lựa chọn
        /// </summary>
        ObservableCollection<ObjectContentElement> SelectedContentElements { get; }

        /// <summary>
        /// Đối tượng đang được chọn
        /// </summary>
        ObjectElement SelectedItem { set; get; }

        /// <summary>
        /// Giá trị định dạng
        /// </summary>
        IFormatPainter PainterValue { set; get; }

        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        string Language { get; }

        /// <summary>
        /// Sự kiện trang hiện hành bị thay đổi
        /// </summary>
        event SelectedItemChangedEventHanlder SelectedSlideChanged;
        /// <summary>
        /// Slide đang được chọn để thao tác
        /// </summary>
        SlideBase SelectedSlide { get; set; }

        /// <summary>
        /// Kích thước trang tài liệu
        /// </summary>
        Size SlideSize { set; get; }

        /// <summary>
        /// Đối tượng tài liệu chính
        /// </summary>
        DocumentControl DocumentControl { get; }

        /// <summary>
        /// Đối tượng tài liệu hiện tại
        /// </summary>
        DocumentControl CurrentDocument { get; }

        /// <summary>
        /// Đơn vị đo chiều dài
        /// </summary>
        LenghtUnit LenghtUnit { get; }

        /// <summary>
        /// Chế độ xem
        /// </summary>
        DocumentMode ViewMode { get; set; }

        /// <summary>
        /// Chế độ xem Slide
        /// </summary>
        SlideViewMode SlideViewMode { set; get; }

        /// <summary>
        /// Cấu hình hiển thị các lưới
        /// </summary>
        ShowConfig ShowConfig { get; }

        /// <summary>
        /// Cấu hình lưu trữ của Player
        /// </summary>
        IPlayerDataSetting PlayerDataSetting { set; get; }

        /// <summary>
        /// Giá trị dữ liệu trung gian
        /// </summary>
        object BindingSupportValue { set; get; }

        /// <summary>
        /// Hàm cập nhật dữ liệu
        /// </summary>
        void RefreshValue();

        /// <summary>
        /// Lệnh điều khiển hiển thị cửa sổ 
        /// </summary>
        ICommand ShowPlayerSettingCommand { get; }

        ICommand SaveDocumentCommand { get; }

        ICommand OpenDocumentCommand { get; }

        ICommand ExportCommand { get; }

        ICommand ImportPowerPointCommand { get; }

        ICommand SlideBackgroundFormatCommand { get; }

        /// <summary>
        /// Lệnh mở cửa sổ định dạng đối tượng
        /// </summary>
        ICommand OpenShapeFormatWindowCommand { get; }
        /// <summary>
        /// Đang ở chế độ xem trước HTML5
        /// </summary>
        bool IsPreviewing { get; }
        /// <summary>
        /// Đã đăng ký bản quyền
        /// </summary>
        bool IsLicensed { get; }

        /// <summary>
        /// Xem trước bài giảng HTML5
        /// </summary>
        void ExportPreviewExecute(object param);

        /// <summary>
        /// Hủy chế độ xem trước
        /// </summary>
        /// <param name="param"></param>
        void ClosePreviewExecute(object param);

        void UpdateTrigger();
        void ConverterDataPathToPath(DataMotionPath dataMotionPath, out MotionPathObject motionPathObject, ObjectElement owner);
        void ConverterDataPathToPath(DataMotionPath dataMotionPath, MotionPathObject motionPathObject, ObjectElement owner);

        /// <summary>
        /// Chuyển đổi sang khung văn bản
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        ITextElement ConvertToTextElement(StandardElement standardElement);
    }

    /// <summary>
    /// Giao diện hỗ trợ thuộc tính theme
    /// </summary>
    public interface IThemesControl
    {
        /// <summary>
        /// Danh sách các đối tượng theme mặc định
        /// </summary>
        ObservableCollection<ETheme> ThemesCollection { get; set; }

        /// <summary>
        /// Danh sach các theme trong bài giảng
        /// </summary>
        ObservableCollection<ETheme> LocalThemesCollection { get; set; }



        /// <summary>
        /// Theme đang được lựa chọn, dạng dữ liệu
        /// </summary>
        ETheme SelectedTheme { set; get; }

        /// <summary>
        /// Theme đang được lựa chọn, dạng view
        /// </summary>
        Theme SelectedThemeView { set; get; }

        /// <summary>
        /// Danh sách nhóm màu mặc định
        /// </summary>
        List<EColorManagment> OfficeColors { get; }

        /// <summary>
        /// Danh sách nhóm phông mặc định
        /// </summary>
        List<EFontfamily> OfficeFonts { get; }

        /// <summary>
        /// Cập nhật Layout
        /// </summary>
        void UpdateLayout(SlideBase slide);

        /// <summary>
        /// Cập nhật chủ đề
        /// </summary>
        void UpdateTheme();

        void UpdateLayoutsByETheme(SlideMaster slideMaster, ETheme eTheme);

        void UpdateColor(EColorManagment eColor, SlideMaster slide);
        void UpdateFont(EFontfamily eFontfamily, SlideMaster slide);

        void UpdateBackground(BackgroundItem background, SlideBase slide);

        void UpdateThemeSlide(ETheme eTheme, ETheme oldTheme);

        void UpdateColorSlide(EColorManagment eColor);

        void UpdateFontSlide(EFontfamily eFontfamily);
        void UpdateThumbnailBackground(BackgroundItem backgroundItem);
        void IsTitleSlideChanged(bool value, SlideBase layout);
        void IsFootersSlideChanged(bool value, SlideBase layout);
        void IsDateSlideChanged(bool value, SlideBase slide);
        void IsSlideNumberSlideChanged(bool value, SlideBase slide);
        void IsTextSlideChanged(bool value, SlideBase slide);
        bool SaveThumbnailLayout(string imgPath, LayoutBase layoutBase, LayoutBase slideBase, bool isHideBackground, Size size = new Size());
        void UpdateSlideNumber(SlideBase slide);
        void CheckIsShowTextBox(ObjectElement element);
        void SetIndexTheme(int index);
        void CustomizeFormatBackground();
    }

    /// <summary>
    /// Chế độ xem 
    /// </summary>
    public enum DocumentMode
    {
        FormView,
        SlideView,
        PreView
    }

    /// <summary>
    /// Chế đội xem cho slide
    /// </summary>
    public enum SlideViewMode
    {
        Normal,
        SlideMaster,
        FeedbackMaster
    }

    /// <summary>
    /// Lớp lưu trữ các thông tin cập nhập dữ liệu
    /// </summary>
    public class BindingValue
    {
        /// <summary>
        /// Kiểu thuộc tính tác động
        /// </summary>
        public DependencyProperty DependencyProperty { get; set; }

        /// <summary>
        /// Giá trị cần tác động
        /// </summary>
        public object Value { get; set; }
    }

    /// <summary>
    /// Đơn vị độ dài
    /// </summary>
    public enum LenghtUnit
    {
        /// <summary>
        /// Pixel
        /// </summary>
        Px,
        /// <summary>
        /// Inch
        /// </summary>
        Inch,
        /// <summary>
        /// Point
        /// </summary>
        Pt,
        /// <summary>
        /// Centimet
        /// </summary>
        Cm,
        /// <summary>
        /// Milimet
        /// </summary>
        Mm
    }


    /// <summary>
    /// Lớp cấu hình hiển thị
    /// </summary>
    public class ShowConfig : INotifyPropertyChanged
    {
        private bool _isRulerVisible = true;
        /// <summary>
        /// Cờ hiển thị thước
        /// </summary>
        public bool IsRulerVisible
        {
            get { return _isRulerVisible; }
            set
            {
                _isRulerVisible = value;
                OnPropertyChanged("IsRulerVisible");
            }
        }

        private bool _isGridLineVisible;
        /// <summary>
        /// Cờ hiển thị các đường lưới trên trang
        /// </summary>
        public bool IsGridLineVisible
        {
            get { return _isGridLineVisible; }
            set
            {
                _isGridLineVisible = value;
                OnPropertyChanged("IsGridLineVisible");
            }
        }

        private bool _isGuideLineVisible = true;
        /// <summary>
        /// Cờ hiển thị các đường giúp đỡ
        /// </summary>
        public bool IsGuideLineVisible
        {
            get { return _isGuideLineVisible; }
            set
            {
                _isGuideLineVisible = value;
                OnPropertyChanged("IsGuideLineVisible");
            }
        }

        private double _gridSpacing = 8.0;
        /// <summary>
        /// Khoảng cách giữa các điểm chấm
        /// </summary>
        public double GridSpacing
        {
            get { return _gridSpacing; }
            set
            {
                if (value > 0.0)
                {
                    _gridSpacing = value;
                    OnPropertyChanged("GridSpacing");
                }
            }
        }

        private bool _isSnapToGrid;
        /// <summary>
        /// Khóa với lưới
        /// </summary>
        public bool IsSnapToGrid
        {
            get { return _isSnapToGrid; }
            set { _isSnapToGrid = value; OnPropertyChanged("IsSnapToGrid"); }
        }

        private bool _isSnapToOtherObject;
        /// <summary>
        /// Khóa với đối tượng khác
        /// </summary>
        public bool IsSnapToOtherObject
        {
            get { return _isSnapToOtherObject; }
            set { _isSnapToOtherObject = value; OnPropertyChanged("IsSnapToOtherObject"); }
        }

        private bool _isSmartLineVisible = true;
        /// <summary>
        /// Cờ hiển thị các đường lưới thông minh
        /// </summary>
        public bool IsSmartLineVisible
        {
            get { return _isSmartLineVisible; }
            set { _isSmartLineVisible = value; OnPropertyChanged("IsSmartLineVisible"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Kiểm tra có phải defaut
        /// </summary>
        /// <returns></returns>
        public bool IsDefault
        {
            get
            {
                if (IsGridLineVisible)
                    return false;

                if (IsGuideLineVisible)
                    return false;

                if (IsRulerVisible)
                    return false;

                if (IsSmartLineVisible)
                    return false;

                if (IsSnapToGrid)
                    return false;

                if (IsSnapToOtherObject)
                    return false;

                if (GridSpacing != 8.0)
                    return false;

                return true;
            }
        }

        /// <summary>
        /// Cài đặt về mặc định
        /// </summary>
        public void SetDefault()
        {
            IsGridLineVisible = false;
            IsGuideLineVisible = false;
            IsRulerVisible = true;
            IsSmartLineVisible = true;
            IsSnapToGrid = false;
            IsSnapToOtherObject = false;
            GridSpacing = 8.0;
        }

        private ObservableCollection<EGuideLine> _guideLines;
        /// <summary>
        /// Danh sách các đối tượng
        /// </summary>
        public ObservableCollection<EGuideLine> GuideLines
        {
            get { return _guideLines ?? (_guideLines = new ObservableCollection<EGuideLine>()); }
        }
    }

    /// <summary>
    /// Lớp lưu trữ thông tin các đường gióng
    /// </summary>
    public class EGuideLine : RootViewModel
    {
        private Orientation _orientation;
        /// <summary>
        /// Hướng của các đường
        /// </summary>
        public Orientation Orientation
        {
            get => _orientation;
            set
            {
                _orientation = value;
                OnPropertyChanged("Orientation");
            }
        }

        private double _moveX;
        /// <summary>
        /// Giá trị điểm di chuyển X
        /// </summary>
        public double MoveX
        {
            get { return _moveX; }
            set { _moveX = value; OnPropertyChanged("MoveX"); }
        }

        private double _moveY;
        /// <summary>
        /// Giá trị điểm di chuyển Y
        /// </summary>
        public double MoveY
        {
            get { return _moveY; }
            set { _moveY = value; OnPropertyChanged("MoveY"); }
        }

    }
}

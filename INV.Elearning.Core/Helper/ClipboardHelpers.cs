﻿using System;

namespace INV.Elearning.Core.Helper
{
    public class ClipboardHelpers
    {
        public const string CLIPBOARD_DATA_KEY = "AVINA_OBJECT";
        public const string CLIPBOARD_LAYOUT_KEY = "AVINA_LAYOUT";
        public const string CLIPBOARD_SLIDE_KEY = "AVINA_SLIDE";
    }
}

﻿namespace INV.Elearning.Core.Helper
{
    /// <summary>
    /// Quy định cấu hình, lớp chứa thông tin quy định có thể lượt bỏ các thao tác
    /// </summary>
    public class ObjectElementRule
    {
        private bool _canMove;
        private bool _canRotation;
        private bool _canAddToSelectedList;
        private bool _canSetZIndex;
        private bool _canResize;
        private bool _lockAnimation;
        private bool _lockTiming;
        private bool _lockState;
        private bool _isAdornerVisible;
        private bool _canCopy = true;
        private bool _canDelete = true;
        private bool _canGroup = true;
        private bool _canScale = true;

        /// <summary>
        /// Hàm khởi tạo
        /// </summary>
        /// <param name="canAddToSelectedList">Có thể thêm vào danh sách lựa chọn hiện tại</param>
        /// <param name="canRotate">Có thể quay đối tượng</param>
        /// <param name="canUserMove">Người dùng có thể di chuyển</param>
        /// <param name="canUserSetZIndex">Người dùng có thể cài đặt chỉ số xếp lớp</param>
        /// <param name="canUserResize">Người dùng có thể thay đổi kích thước</param>
        /// <param name="lockAnimation">Khóa hiệu ứng chuyển động</param>
        /// <param name="lockTiming">Khóa time line</param>
        /// <param name="lockState">Khóa trạng thái</param>
        public ObjectElementRule(bool canAddToSelectedList = true, bool canRotate = true, bool canUserMove = true, bool canUserSetZIndex = true,
            bool canUserResize = true, bool lockAnimation = false, bool lockTiming = false, bool lockState = false, bool isAdornerVisible = true)
        {
            CanAddToSelectedList = canAddToSelectedList;
            CanRotate = canRotate;
            CanUserMove = canUserMove;
            CanUserSetZIndex = canUserSetZIndex;
            CanUserResize = canUserResize;
            LockAnimation = lockAnimation;
            LockTiming = lockTiming;
            LockState = lockState;
            IsAdornerVisible = isAdornerVisible;
        }

        /// <summary>
        /// Có thể thêm vào danh sách lựa đối tượng
        /// </summary>
        public bool CanAddToSelectedList { get => _canAddToSelectedList; private set => _canAddToSelectedList = value; }
        /// <summary>
        /// Không cho cài đặt góc quay của đối tượng, thuộc tính Angle luôn có giá trị là 0
        /// </summary>
        public bool CanRotate { get => _canRotation; private set => _canRotation = value; }
        /// <summary>
        /// Có thể di chuyển đối tượng
        /// </summary>
        public bool CanUserMove { get => _canMove; private set => _canMove = value; }
        /// <summary>
        /// Có thể cài đặt ZIndex
        /// </summary>
        public bool CanUserSetZIndex { get => _canSetZIndex; private set => _canSetZIndex = value; }
        /// <summary>
        /// Có thể thay đổi kích thước, chỉ có tác dụng với Chrome Resize
        /// </summary>
        public bool CanUserResize { get => _canResize; private set => _canResize = value; }
        /// <summary>
        /// Khóa không cho cài đặt hiệu ứng chuyển động
        /// </summary>
        public bool LockAnimation { get => _lockAnimation; private set => _lockAnimation = value; }
        /// <summary>
        /// Khóa không cho cài đặt timeline
        /// </summary>
        public bool LockTiming { get => _lockTiming; private set => _lockTiming = value; }
        /// <summary>
        /// Khóa không cho cài đặt thuộc tính trạng thái
        /// </summary>
        public bool LockState { get => _lockState; private set => _lockState = value; }
        /// <summary>
        /// Có hiển thị Adorner hay không
        /// </summary>
        public bool IsAdornerVisible { get => _isAdornerVisible; set => _isAdornerVisible = value; }
        /// <summary>
        /// Có thể sao chép
        /// </summary>
        public bool CanCopy { get => _canCopy; set => _canCopy = value; }
        /// <summary>
        /// Có thể xóa đối tượng
        /// </summary>
        public bool CanDelete { get => _canDelete; set => _canDelete = value; }
        /// <summary>
        /// Có thể gôm nhóm đối tượng
        /// </summary>
        public bool CanGroup { get => _canGroup; set => _canGroup = value; }
        /// <summary>
        /// Có thể đảo ngược
        /// </summary>
        public bool CanScale { get => _canScale; set => _canScale = value; }
    }
}

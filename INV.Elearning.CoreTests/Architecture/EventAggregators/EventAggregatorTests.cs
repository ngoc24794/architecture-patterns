﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace INV.Elearning.Core.Architecture.EventAggregators.Tests
{
    [TestClass()]
    public class EventAggregatorTests
    {
        [TestMethod()]
        public void GetEventTest()
        {
            IEventAggregator ea = new EventAggregator();
            Assert.AreEqual<Type>(typeof(PubSubEvent<string>), ea.GetEvent<PubSubEvent<string>>().GetType());
        }

        public void PublishTest()
        {
            IEventAggregator ea = new EventAggregator();
            ea.GetEvent<PubSubEvent<string>>().Subscribe((x) => { });
            ea.GetEvent<PubSubEvent<string>>().Publish("Ngoc");
            //Assert.
        }
    }
}
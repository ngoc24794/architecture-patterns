﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionManagerAccessor.cs
**
** Description: Định nghĩa Interface cho lớp hỗ trợ truy cập các thành viên tĩnh của IRegionManager
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Windows;

namespace Regions
{
    /// <summary>
    /// Định nghĩa lớp hỗ trợ truy cập các thành viên tĩnh của <see cref="IRegionManager"/>
    /// </summary>
    internal class RegionManagerAccessor : IRegionManagerAccessor
    {
        /// <summary>
        /// Sự kiện cập nhật các vùng
        /// </summary>
        public event EventHandler UpdatingRegions
        {
            add { RegionManager.UpdatingRegions += value; }
            remove { RegionManager.UpdatingRegions -= value; }
        }

        /// <summary>
        /// Trả về tên vùng được gán cho đối tượng được chỉ định.
        /// </summary>
        /// <param name="targetElement">Đối tượng được gán tên vùng</param>
        /// <returns>Tền vùng được gán cho đối tượng được chỉ định</returns>
        public string GetRegionName(DependencyObject targetElement)
        {
            return targetElement.GetValue(RegionManager.RegionNameProperty) as string;
        }

        public IRegionManager GetRegionManager(DependencyObject element)
        {
            return element.GetValue(RegionManager.RegionManagerProperty) as IRegionManager;
        }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionManagerAccessor.cs
**
** Description: Định nghĩa Interface cho lớp hỗ trợ truy cập các thành viên tĩnh của IRegionManager
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Windows;

namespace Regions
{
    /// <summary>
    /// Định nghĩa Interface cho lớp hỗ trợ truy cập các thành viên tĩnh của <see cref="IRegionManager"/>
    /// </summary>
    public interface IRegionManagerAccessor
    {
        /// <summary>
        /// Sự kiện cập nhật các vùng
        /// </summary>
        event EventHandler UpdatingRegions;

        /// <summary>
        /// Trả về tên vùng được gán cho đối tượng được chỉ định.
        /// </summary>
        /// <param name="targetElement">Đối tượng được gán tên vùng</param>
        /// <returns>Tền vùng được gán cho đối tượng được chỉ định</returns>
        string GetRegionName(DependencyObject targetElement);
        IRegionManager GetRegionManager(DependencyObject element);
    }
}

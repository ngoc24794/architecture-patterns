﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RegionCreationBehavior.cs
**
** Description: Định nghĩa một lớp thực hiện việc tạo vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Windows;

namespace Regions
{

    /// <summary>
    /// Định nghĩa một lớp thực hiện việc tạo vùng
    /// </summary>
    public class RegionCreationBehavior
    {
        private readonly RegionAdapterMappings _regionAdapterMappings;
        private WeakReference _elementWeakReference;
        private bool _regionCreated;

        public RegionCreationBehavior(RegionAdapterMappings regionAdapterMappings)
        {
            _regionAdapterMappings = regionAdapterMappings;
            RegionManagerAccessor = new RegionManagerAccessor();
        }

        public DependencyObject TargetElement
        {
            get { return _elementWeakReference != null ? (DependencyObject)_elementWeakReference.Target : null; }
            set { _elementWeakReference = new WeakReference(value); }
        }

        /// <summary>
        /// Lấy hoặc đặt Đối tượng truy cập <see cref="IRegionManager"/>
        /// </summary>
        public IRegionManagerAccessor RegionManagerAccessor { get; set; }

        /// <summary>
        /// Khởi tạo một vùng tương ứng với đối tượng chứa và tên vùng được chỉ định.
        /// </summary>
        /// <param name="targetElement">Đối tượng chứa</param>
        /// <param name="regionName">Tên vùng</param>
        /// <returns>Vùng được tạo</returns>
        protected virtual IRegion CreateRegion(DependencyObject targetElement, string regionName)
        {
            IRegionAdapter regionAdapter = _regionAdapterMappings.GetMapping(targetElement.GetType());
            IRegion region = regionAdapter.Initialize(targetElement, regionName);

            return region;
        }

        public void Attach()
        {
            RegionManagerAccessor.UpdatingRegions += OnUpdatingRegions;
            WireUpTargetElement();
        }

        public void OnUpdatingRegions(object sender, EventArgs e)
        {
            TryCreateRegion();
        }

        private void TryCreateRegion()
        {
            DependencyObject targetElement = TargetElement;
            if (targetElement == null)
            {
                Detach();
                return;
            }

            if (targetElement.CheckAccess())
            {
                this.Detach();

                if (!_regionCreated)
                {
                    string regionName = RegionManagerAccessor.GetRegionName(targetElement);
                    CreateRegion(targetElement, regionName);
                    _regionCreated = true;
                }
            }
        }

        public void Detach()
        {
            RegionManagerAccessor.UpdatingRegions -= OnUpdatingRegions;
            UnWireTargetElement();
        }

        private void WireUpTargetElement()
        {
            if (TargetElement is FrameworkElement element)
            {
                element.Loaded += ElementLoaded;
                return;
            }

            if (TargetElement is FrameworkContentElement fcElement)
            {
                fcElement.Loaded += ElementLoaded;
                return;
            }
        }

        private void UnWireTargetElement()
        {
            if (TargetElement is FrameworkElement element)
            {
                element.Loaded -= ElementLoaded;
                return;
            }

            if (TargetElement is FrameworkContentElement fcElement)
            {
                fcElement.Loaded -= ElementLoaded;
                return;
            }
        }

        private void ElementLoaded(object sender, RoutedEventArgs e)
        {
            UnWireTargetElement();
            TryCreateRegion();
        }

    }
}

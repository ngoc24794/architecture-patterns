﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionManager.cs
**
** Description: Định nghĩa Interface cho lớp quản lý vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/


namespace Regions
{
    /// <summary>
    /// Định nghĩa Interface cho lớp quản lý vùng
    /// </summary>
    public interface IRegionManager
    {
        IRegionCollection Regions { get; }
        IRegionManager CreateRegionManager();
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RegionManager.cs
**
** Description: Lớp quản lý vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using IoCPattern;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Regions
{
    /// <summary>
    /// Lớp quản lý vùng
    /// </summary>
    public class RegionManager : IRegionManager
    {
        private static readonly WeakDelegatesManager updatingRegionsListeners = new WeakDelegatesManager();

        /// <summary>
        /// Thuộc tính bổ sung nhằm đánh dấu các cùng trên View
        /// </summary>
        public static readonly DependencyProperty RegionNameProperty =
            DependencyProperty.RegisterAttached("RegionName", typeof(string), typeof(RegionManager), new PropertyMetadata(null, RegionNameChangedCallback));

        /// <summary>
        /// Sự kiện cập nhật các vùng
        /// </summary>
        public static event EventHandler UpdatingRegions
        {
            add { updatingRegionsListeners.AddListener(value); }
            remove { updatingRegionsListeners.RemoveListener(value); }
        }

        private static void RegionNameChangedCallback(DependencyObject element, DependencyPropertyChangedEventArgs e)
        {
            if (!IsInDesignMode(element))
            {
                CreateRegion(element);
            }
        }

        private static void CreateRegion(DependencyObject element)
        {
            IServiceLocator serviceLocator = ServiceLocator.Current;
            RegionCreationBehavior regionCreationBehavior = serviceLocator.Resolve<RegionCreationBehavior>();
            regionCreationBehavior.TargetElement = element;
            regionCreationBehavior.Attach();
        }

        private static bool IsInDesignMode(DependencyObject element)
        {
            return DesignerProperties.GetIsInDesignMode(element);
        }


        /// <summary>
        /// Lấy tên vùng đã đánh dấu trên đối tượng được chỉ định
        /// </summary>
        /// <param name="targetElement">Đối tượng đã được đánh dấu tên vùng</param>
        /// <returns>Tên vùng đã đánh dấu trên đối tượng</returns>
        public static string GetRegionName(DependencyObject targetElement)
        {
            return (string)targetElement.GetValue(RegionNameProperty);
        }

        public static readonly DependencyProperty RegionManagerProperty =
            DependencyProperty.RegisterAttached("RegionManager", typeof(IRegionManager), typeof(RegionManager), new PropertyMetadata(null));

        public RegionManager()
        {
            Regions = new RegionCollection(this);
        }

        public static IRegionManager GetRegionManager(DependencyObject obj)
        {
            return (IRegionManager)obj.GetValue(RegionManagerProperty);
        }

        public static void SetRegionManager(DependencyObject target, IRegionManager value)
        {
            target.SetValue(RegionManagerProperty, value);
        }

        /// <summary>
        /// Đánh dấu tên vùng trên đối tượng được chỉ định
        /// </summary>
        /// <param name="targetElement">Đối tượng được đánh dấu tên vùng</param>
        /// <param name="regionName">Tên vùng sẽ được đánh dấu lên đối tượng được chỉ định</param>
        public static void SetRegionName(DependencyObject targetElement, string regionName)
        {
            targetElement.SetValue(RegionNameProperty, regionName);
        }

        /// <summary>
        /// Cập nhật các vùng
        /// </summary>
        public static void UpdateRegions()
        {
            updatingRegionsListeners.Raise(null, EventArgs.Empty);
        }

        public IRegionManager CreateRegionManager()
        {
            return new RegionManager();
        }

        public IRegionCollection Regions { get; }

        private class RegionCollection : IRegionCollection
        {
            private readonly IRegionManager _regionManager;
            private readonly List<IRegion> _regions;

            public RegionCollection(IRegionManager regionManager)
            {
                _regionManager = regionManager;
                _regions = new List<IRegion>();
            }

            public event NotifyCollectionChangedEventHandler CollectionChanged;

            public IEnumerator<IRegion> GetEnumerator()
            {
                UpdateRegions();

                return _regions.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public IRegion this[string regionName]
            {
                get
                {
                    UpdateRegions();

                    IRegion region = GetRegionByName(regionName);

                    return region;
                }
            }

            public void Add(IRegion region)
            {
                if (region == null)
                    throw new ArgumentNullException(nameof(region));

                UpdateRegions();

                _regions.Add(region);
                region.RegionManager = _regionManager;

                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, region, 0));
            }

            public bool Remove(string regionName)
            {
                UpdateRegions();

                bool removed = false;

                IRegion region = GetRegionByName(regionName);
                if (region != null)
                {
                    removed = true;
                    _regions.Remove(region);
                    region.RegionManager = null;

                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, region, 0));
                }

                return removed;
            }

            public bool ContainsRegionWithName(string regionName)
            {
                UpdateRegions();

                return GetRegionByName(regionName) != null;
            }

            /// <summary>
            /// Adds a region to the regionmanager with the name received as argument.
            /// </summary>
            /// <param name="regionName">The name to be given to the region.</param>
            /// <param name="region">The region to be added to the regionmanager.</param>
            /// <exception cref="ArgumentNullException">Thrown if <paramref name="region"/> is <see langword="null"/>.</exception>
            /// <exception cref="ArgumentException">Thrown if <paramref name="regionName"/> and <paramref name="region"/>'s name do not match and the <paramref name="region"/> <see cref="IRegion.Name"/> is not <see langword="null"/>.</exception>
            public void Add(string regionName, IRegion region)
            {
                if (region.Name == null)
                    region.Name = regionName;

                Add(region);
            }

            private IRegion GetRegionByName(string regionName)
            {
                return _regions.FirstOrDefault(r => r.Name == regionName);
            }

            private void OnCollectionChanged(NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
            {
                CollectionChanged?.Invoke(this, notifyCollectionChangedEventArgs);
            }
        }

    }
}

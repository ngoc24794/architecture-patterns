﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Windows;

namespace Regions
{
    public class ItemMetadata : DependencyObject
    {
        /// <summary>
        /// Tên của phần tử được bao.
        /// </summary>
        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(ItemMetadata), null);

        /// <summary>
        /// Lấy hoặc đặt tên của phần tử được bao.
        /// </summary>
        /// <value>Tên của phần tử được bao</value>
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        /// <summary>
        /// Giá trị xác định phần tử được bao có được đánh giá Đang hiển thị hay không.
        /// </summary>
        public static readonly DependencyProperty IsActiveProperty =
            DependencyProperty.Register("IsActive", typeof(bool), typeof(ItemMetadata), new PropertyMetadata(DependencyPropertyChanged));
        
        /// <summary>
        /// Lấy hoặc đặt thuộc tính <see cref="IsActiveProperty"/>
        /// </summary>
        public bool IsActive
        {
            get { return (bool)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        public ItemMetadata(object item)
        {
            Item = item;
        }

        /// <summary>
        /// Lấy phần tử được bao.
        /// </summary>
        /// <value>Phần tử được bao</value>
        public object Item { get; private set; }

        public event EventHandler MetadataChanged;

        public void InvokeMetadataChanged()
        {
            MetadataChanged?.Invoke(this, EventArgs.Empty);
        }

        private static void DependencyPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            if (dependencyObject is ItemMetadata itemMetadata)
            {
                itemMetadata.InvokeMetadataChanged();
            }
        }
    }
}

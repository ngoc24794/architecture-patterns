﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  ViewsCollection.cs
**
** Description: Tập hợp view
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Regions
{
    /// <summary>
    /// Tập hợp view
    /// </summary>
    public class ViewsCollection : IViewsCollection
    {
        private List<object> _filteredItems = new List<object>();

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public bool Contains(object value)
        {
            return _filteredItems.Contains(value);
        }

        public IEnumerator<object> GetEnumerator()
        {
            return _filteredItems.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  Region.cs
**
** Description: Định nghĩa một vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using Regions.Behaviors;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Regions
{
    /// <summary>
    /// Định nghĩa một vùng
    /// </summary>
    public class Region : IRegion
    {
        private string _name;
        private ViewsCollection _views;
        private ViewsCollection _activeViews;
        private ObservableCollection<ItemMetadata> _itemMetadataCollection;
        private IRegionManager _regionManager;

        public Region()
        {
            Behaviors = new RegionBehaviorCollection(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public IRegionBehaviorCollection Behaviors { get; private set; }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public IViewsCollection Views => _views ?? (_views = new ViewsCollection());

        public IViewsCollection ActiveViews => Views;

        public ObservableCollection<ItemMetadata> ItemMetadataCollection => _itemMetadataCollection ?? (_itemMetadataCollection = new ObservableCollection<ItemMetadata>());

        public IRegionManager RegionManager
        {
            get { return _regionManager; }
            set
            {
                if (_regionManager != value)
                {
                    _regionManager = value;
                    OnPropertyChanged();
                }
            }
        }

        public void Activate(object view)
        {
            ItemMetadata itemMetadata = GetItemMetadata(view);

            if (!itemMetadata.IsActive)
            {
                itemMetadata.IsActive = true;
            }
        }

        private ItemMetadata GetItemMetadata(object view)
        {
            ItemMetadata itemMetadata = this.ItemMetadataCollection.FirstOrDefault(x => x.Item == view);

            return itemMetadata;
        }

        public IRegionManager Add(object view)
        {
            return Add(view, null, false);
        }

        public IRegionManager Add(object view, string viewName)
        {
            return Add(view, viewName, false);
        }

        public IRegionManager Add(object view, string viewName, bool createRegionManagerScope)
        {
            IRegionManager manager = createRegionManagerScope ? RegionManager.CreateRegionManager() : RegionManager;
            InnerAdd(view, viewName, manager);
            return manager;
        }

        public void Deactivate(object view)
        {
            ItemMetadata itemMetadata = GetItemMetadata(view);

            if (!itemMetadata.IsActive)
            {
                itemMetadata.IsActive = false;
            }
        }

        public object GetView(string viewName)
        {
            ItemMetadata metadata = this.ItemMetadataCollection.FirstOrDefault(x => x.Name == viewName);

            if (metadata != null)
            {
                return metadata.Item;
            }

            return null;
        }

        public void Remove(object view)
        {
            ItemMetadata itemMetadata = GetItemMetadata(view);

            ItemMetadataCollection.Remove(itemMetadata);

            if (view is DependencyObject dependencyObject && Regions.RegionManager.GetRegionManager(dependencyObject) == RegionManager)
            {
                dependencyObject.ClearValue(Regions.RegionManager.RegionManagerProperty);
            }
        }

        public void RemoveAll()
        {
            foreach (var view in Views)
            {
                Remove(view);
            }
        }

        private void InnerAdd(object view, string viewName, IRegionManager scopedRegionManager)
        {
            ItemMetadata itemMetadata = new ItemMetadata(view);
            if (!string.IsNullOrEmpty(viewName))
            {
                itemMetadata.Name = viewName;
            }

            if (view is DependencyObject dependencyObject)
            {
                Regions.RegionManager.SetRegionManager(dependencyObject, scopedRegionManager);
            }

            ItemMetadataCollection.Add(itemMetadata);
        }
    }
}

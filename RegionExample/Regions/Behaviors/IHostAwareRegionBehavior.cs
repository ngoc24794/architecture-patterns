﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IHostAwareRegionBehavior.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Windows;

namespace Regions.Behaviors
{
    public interface IHostAwareRegionBehavior : IRegionBehavior
    {
        DependencyObject HostControl { get; set; }
    }
}

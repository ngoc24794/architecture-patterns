﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Collections;
using System.Collections.Generic;

namespace Regions.Behaviors
{
    public class RegionBehaviorCollection : IRegionBehaviorCollection
    {
        private readonly IRegion _region;
        private readonly Dictionary<string, IRegionBehavior> _behaviors = new Dictionary<string, IRegionBehavior>();

        public RegionBehaviorCollection(IRegion region)
        {
            _region = region;
        }

        public IRegionBehavior this[string key]
        {
            get { return _behaviors[key]; }
        }

        public void Add(string key, IRegionBehavior regionBehavior)
        {
            _behaviors.Add(key, regionBehavior);
            regionBehavior.Region = this._region;

            regionBehavior.Attach();
        }

        public bool ContainsKey(string key)
        {
            return _behaviors.ContainsKey(key);
        }

        public IEnumerator<KeyValuePair<string, IRegionBehavior>> GetEnumerator()
        {
            return _behaviors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _behaviors.GetEnumerator();
        }
    }
}

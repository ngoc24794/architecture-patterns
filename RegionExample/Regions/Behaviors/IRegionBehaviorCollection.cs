﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionBehaviorCollection.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Collections.Generic;

namespace Regions.Behaviors
{
    /// <summary>
    /// Định nghĩa Interface cho lớp đại diện cho một tập hợp các hành vi của <see cref="IRegion"/>
    /// </summary>
    public interface IRegionBehaviorCollection : IEnumerable<KeyValuePair<string, IRegionBehavior>>
    {
        void Add(string key, IRegionBehavior regionBehavior);

        bool ContainsKey(string key);

        IRegionBehavior this[string key] { get; }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RegionBehavior.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/


namespace Regions.Behaviors
{
    /// <summary>
    /// Lớp đại diện cho một hành vi của <see cref="IRegion"/>
    /// </summary>
    public abstract class RegionBehavior : IRegionBehavior
    {
        private IRegion _region;
        public IRegion Region
        {
            get { return _region; }
            set
            {
                if (!IsAttached)
                {
                    _region = value;
                }
            }
        }

        public bool IsAttached { get; set; }

        public void Attach()
        {
            IsAttached = true;
            OnAttach();
        }

        protected virtual void OnAttach()
        {
            // các lệnh được thực thi sau khi hành vi đã được gán vào IRegion
        }
    }
}

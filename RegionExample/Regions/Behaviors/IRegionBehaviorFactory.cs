﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionBehaviorFactory.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/


using System;
using System.Collections.Generic;

namespace Regions.Behaviors
{
    public interface IRegionBehaviorFactory : IEnumerable<string>
    {
        void AddIfMissing(string behaviorKey, Type behaviorType);

        bool ContainsKey(string behaviorKey);

        IRegionBehavior CreateFromKey(string key);
    }
}

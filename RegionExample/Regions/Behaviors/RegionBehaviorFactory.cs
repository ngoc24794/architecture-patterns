﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RegionBehaviorFactory.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using IoCPattern;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Regions.Behaviors
{
    public class RegionBehaviorFactory : IRegionBehaviorFactory
    {
        private readonly IServiceLocator _serviceLocator;
        private readonly Dictionary<string, Type> _registeredBehaviors = new Dictionary<string, Type>();

        public RegionBehaviorFactory(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
        }

        public void AddIfMissing(string behaviorKey, Type behaviorType)
        {
            if (_registeredBehaviors.ContainsKey(behaviorKey))
            {
                return;
            }

            _registeredBehaviors.Add(behaviorKey, behaviorType);
        }

        public IRegionBehavior CreateFromKey(string key)
        {
            return (IRegionBehavior)_serviceLocator.Resolve(_registeredBehaviors[key]);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return _registeredBehaviors.Keys.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool ContainsKey(string behaviorKey)
        {
            return _registeredBehaviors.ContainsKey(behaviorKey);
        }
    }
}

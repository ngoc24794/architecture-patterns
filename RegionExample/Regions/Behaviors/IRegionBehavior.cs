﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

namespace Regions.Behaviors
{
    /// <summary>
    /// Định nghĩa Interface cho lớp đại diện cho một hành vi của một <see cref="IRegion"/>
    /// </summary>
    public interface IRegionBehavior
    {
        /// <summary>
        /// Lấy <see cref="IRegion"/> có hành vi này
        /// </summary>
        IRegion Region { get; set; }

        /// <summary>
        /// Gắn hành vi này vào <see cref="IRegion"/> được chỉ định
        /// </summary>
        void Attach();
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  FileName.cs
**
** Description: 
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.ComponentModel;
using System.Windows;

namespace Regions.Behaviors
{
    public class RegionManagerRegistrationBehavior : RegionBehavior, IHostAwareRegionBehavior
    {
        public static readonly string BehaviorKey = "RegionManagerRegistration";

        private WeakReference _attachedRegionManagerWeakReference;
        private DependencyObject _hostControl;

        public RegionManagerRegistrationBehavior()
        {
            RegionManagerAccessor = new RegionManagerAccessor();
        }

        public IRegionManagerAccessor RegionManagerAccessor { get; set; }

        public DependencyObject HostControl
        {
            get
            {
                return _hostControl;
            }
            set
            {
                if (!IsAttached)
                {
                    _hostControl = value;
                }
            }
        }

        protected override void OnAttach()
        {
            if (string.IsNullOrEmpty(Region.Name))
            {
                Region.PropertyChanged += Region_PropertyChanged;
            }
            else
            {
                StartMonitoringRegionManager();
            }
        }

        private void Region_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name" && !string.IsNullOrEmpty(this.Region.Name))
            {
                Region.PropertyChanged -= Region_PropertyChanged;
                StartMonitoringRegionManager();
            }
        }

        private void StartMonitoringRegionManager()
        {
            RegionManagerAccessor.UpdatingRegions += OnUpdatingRegions;
            TryRegisterRegion();
        }

        private void TryRegisterRegion()
        {
            DependencyObject targetElement = HostControl;
            if (targetElement.CheckAccess())
            {
                IRegionManager regionManager = FindRegionManager(targetElement);

                IRegionManager attachedRegionManager = GetAttachedRegionManager();

                if (regionManager != attachedRegionManager)
                {
                    if (attachedRegionManager != null)
                    {
                        _attachedRegionManagerWeakReference = null;
                        attachedRegionManager.Regions.Remove(Region.Name);
                    }

                    if (regionManager != null)
                    {
                        _attachedRegionManagerWeakReference = new WeakReference(regionManager);
                        regionManager.Regions.Add(Region);
                    }
                }
            }
        }

        public void OnUpdatingRegions(object sender, EventArgs e)
        {
            TryRegisterRegion();
        }

        private IRegionManager FindRegionManager(DependencyObject dependencyObject)
        {
            var regionmanager = this.RegionManagerAccessor.GetRegionManager(dependencyObject);
            if (regionmanager != null)
            {
                return regionmanager;
            }

            DependencyObject parent = null;
            parent = LogicalTreeHelper.GetParent(dependencyObject);
            if (parent != null)
            {
                return FindRegionManager(parent);
            }

            return null;
        }

        private IRegionManager GetAttachedRegionManager()
        {
            if (_attachedRegionManagerWeakReference != null)
            {
                return _attachedRegionManagerWeakReference.Target as IRegionManager;
            }

            return null;
        }
    }
}

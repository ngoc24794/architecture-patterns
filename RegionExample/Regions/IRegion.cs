﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegion.cs
**
** Description: Định nghĩa Interface cho lớp định nghĩa một vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using Regions.Behaviors;
using System.ComponentModel;

namespace Regions
{
    /// <summary>
    /// Định nghĩa Interface cho lớp định nghĩa một vùng
    /// </summary>
    public interface IRegion : INotifyPropertyChanged
    {
        /// <summary>
        /// Lấy hoặc đặt Tên vùng
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Lấy tập hợp view
        /// </summary>
        IViewsCollection Views { get; }

        /// <summary>
        /// Lấy các view đang hiển thị trên vùng
        /// </summary>
        /// <value>Tập hợp các view <see cref="IViewsCollection"/></value>
        IViewsCollection ActiveViews { get; }

        /// <summary>
        /// Thêm view được chỉ định vào vùng
        /// </summary>
        /// <param name="view">View để thêm</param>
        IRegionManager Add(object view);

        /// <summary>
        /// Thêm một view mới vào vùng.
        /// </summary>
        /// <param name="view">View để thêm</param>
        /// <param name="viewName">Tên của view được dùng ở <see cref="GetView(string)"/></param>
        IRegionManager Add(object view, string viewName);

        /// <summary>
        /// Adds a new view to the region.
        /// </summary>
        /// <param name="view">The view to add.</param>
        /// <param name="viewName">The name of the view. This can be used to retrieve it later by calling <see cref="GetView"/>.</param>
        /// <param name="createRegionManagerScope">When <see langword="true"/>, the added view will receive a new instance of <see cref="IRegionManager"/>, otherwise it will use the current region manager for this region.</param>
        /// <returns>The <see cref="IRegionManager"/> that is set on the view if it is a <see cref="DependencyObject"/>.</returns>
        IRegionManager Add(object view, string viewName, bool createRegionManagerScope);

        /// <summary>
        /// Xóa view được chỉ định ra khỏi vùng.
        /// </summary>
        /// <param name="view">View để xóa</param>
        void Remove(object view);

        /// <summary>
        /// Xóa tất cả các view khỏi vùng.
        /// </summary>
        void RemoveAll();

        /// <summary>
        /// Đánh dấu view được chỉ định là Đang hiển thị.
        /// </summary>
        /// <param name="view">View để hiển thị</param>
        void Activate(object view);

        /// <summary>
        /// Đánh dấu view được chỉ định là Đang ẩn.
        /// </summary>
        /// <param name="view">View để ẩn</param>
        void Deactivate(object view);

        /// <summary>
        /// Trả về một view đã được thêm vào vùng bằng cách chỉ định tên view.
        /// </summary>
        /// <param name="viewName">Tên của view</param>
        object GetView(string viewName);
        IRegionManager RegionManager { get; set; }
        IRegionBehaviorCollection Behaviors { get; }
    }
}

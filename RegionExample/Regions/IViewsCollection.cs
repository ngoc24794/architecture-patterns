﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IViewsCollection.cs
**
** Description: Định nghĩa Interface cho một tập hợp View
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Collections.Generic;
using System.Collections.Specialized;

namespace Regions
{
    /// <summary>
    /// Định nghĩa Interface cho một tập hợp View
    /// </summary>
    public interface IViewsCollection : IEnumerable<object>, INotifyCollectionChanged
    {
        /// <summary>
        /// Xác định tập hợp có chứa giá trị được chỉ định hay không.
        /// </summary>
        /// <param name="value">Giá trị để kiểm tra</param>
        /// <returns>Trả về true nếu giá trị được chỉ định thuộc tập hợp. Ngược lại, false.</returns>
        bool Contains(object value);
    }
}

﻿using IoCPattern;
using Regions;
using Regions.Behaviors;
using System.Windows;
using System.Windows.Controls;

namespace RegionExample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            IContainer container = new Container();
            container.RegisterSingleton<RegionAdapterMappings>();
            container.RegisterSingleton<RegionCreationBehavior>();
            container.RegisterSingleton<IRegionManager, RegionManager>();
            container.RegisterSingleton<IServiceLocator, ServiceLocator>();
            container.RegisterSingleton<ItemsControlRegionAdapter, ItemsControlRegionAdapter>();
            container.RegisterSingleton<IRegionBehaviorFactory, RegionBehaviorFactory>();
            container.RegisterSingleton<RegionManagerRegistrationBehavior, RegionManagerRegistrationBehavior>();
            container.Resolve<IServiceLocator>();
            RegionAdapterMappings regionAdapterMappings = container.Resolve<RegionAdapterMappings>();
            regionAdapterMappings.RegisterMapping(typeof(ItemsControl), container.Resolve<ItemsControlRegionAdapter>());
            container.Register<MainWindow>();
            var defaultRegionBehaviorTypesDictionary = container.Resolve<IRegionBehaviorFactory>();
            defaultRegionBehaviorTypesDictionary.AddIfMissing(RegionManagerRegistrationBehavior.BehaviorKey,
                                                   typeof(RegionManagerRegistrationBehavior));
            IRegionManager regionManager = container.Resolve<IRegionManager>();
            RegionManager.UpdateRegions();
            Application.Current.MainWindow = container.Resolve<MainWindow>();
            Application.Current.MainWindow.Show();
        }
    }
}

﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  WeakDelegatesManager.cs
**
** Description: Định nghĩa lớp quản lý các tham chiếu yếu
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace Regions
{
    /// <summary>
    /// Định nghĩa lớp quản lý các tham chiếu yếu
    /// </summary>
    internal class WeakDelegatesManager
    {
        private readonly List<DelegateReference> _listeners = new List<DelegateReference>();

        public void AddListener(Delegate listener)
        {
            _listeners.Add(new DelegateReference(listener));
        }

        public void RemoveListener(Delegate listener)
        {
            _listeners.RemoveAll(reference =>
            {
                Delegate target = reference.Target;
                return listener.Equals(target) || target == null;
            });
        }

        public void Raise(params object[] args)
        {
            _listeners.RemoveAll(listener => listener.Target == null);

            foreach (Delegate handler in _listeners.ToList().Select(listener => listener.Target).Where(listener => listener != null))
            {
                handler.DynamicInvoke(args);
            }
        }
    }
}

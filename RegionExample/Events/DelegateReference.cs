﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  DelegateReference.cs
**
** Description: Đại diện cho một tham chiếu đến một Delegate
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Reflection;

namespace Regions
{
    /// <summary>
    /// Đại diện cho một tham chiếu đến một <see cref="Delegate"/>
    /// </summary>
    internal class DelegateReference : IDelegateReference
    {
        private readonly WeakReference _weakReference;
        private readonly MethodInfo _method;
        private readonly Type _delegateType;

        public DelegateReference(Delegate @delegate)
        {
            _weakReference = new WeakReference(@delegate.Target);
            _method = @delegate.GetMethodInfo();
            _delegateType = @delegate.GetType();
        }

        /// <summary>
        /// Trả về <see cref="Delegate"/> được tham chiếu.
        /// </summary>
        public Delegate Target
        {
            get
            {
                return TryGetDelegate();
            }
        }

        private Delegate TryGetDelegate()
        {
            if (_method.IsStatic)
            {
                return _method.CreateDelegate(_delegateType, null);
            }
            object target = _weakReference.Target;
            if (target != null)
            {
                return _method.CreateDelegate(_delegateType, target);
            }
            return null;
        }
    }
}

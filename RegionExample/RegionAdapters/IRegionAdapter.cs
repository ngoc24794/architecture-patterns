﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionAdapter.cs
**
** Description: Định nghĩa Interface cho lớp chuyển đổi vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Windows;

namespace Regions
{
    /// <summary>
    /// Định nghĩa Interface cho lớp chuyển đổi vùng
    /// </summary>
    public interface IRegionAdapter
    {
        /// <summary>
        /// Khởi tạo một vùng tương ứng với đối tượng chứa và tên vùng được chỉ định.
        /// </summary>
        /// <param name="targetElement">Đối tượng chứa</param>
        /// <param name="regionName">Tên vùng</param>
        /// <returns></returns>
        IRegion Initialize(object regionTarget, string regionName);
    }
}

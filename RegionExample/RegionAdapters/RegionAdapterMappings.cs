﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  RegionAdapterMappings.cs
**
** Description: Định nghĩa các ánh xạ vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System;
using System.Collections.Generic;

namespace Regions
{
    /// <summary>
    /// Định nghĩa các ánh xạ vùng
    /// </summary>
    public class RegionAdapterMappings
    {
        private readonly Dictionary<Type, IRegionAdapter> _mappings = new Dictionary<Type, IRegionAdapter>();

        /// <summary>
        /// Đăng ký một ánh xạ vùng
        /// </summary>
        /// <param name="controlType">Kiểu dữ liệu của Control đóng vai trò là vùng chứa</param>
        /// <param name="adapter">Bộ chuyển đổi vùng tương ứng</param>
        public void RegisterMapping(Type controlType, IRegionAdapter adapter)
        {
            if (_mappings.ContainsKey(controlType))
            {
                return;
            }

            _mappings.Add(controlType, adapter);
        }

        /// <summary>
        /// Trả về một <see cref="IRegionAdapter"/> tương ứng với <see cref="Type"/> được chỉ định.
        /// </summary>
        /// <param name="controlType">Kiểu dữ liệu để tìm</param>
        /// <returns><see cref="IRegionAdapter"/> tương ứng với kiểu được chỉ định</returns>
        public IRegionAdapter GetMapping(Type controlType)
        {
            Type currentType = controlType;

            while (currentType != null)
            {
                if (_mappings.ContainsKey(currentType))
                {
                    return _mappings[currentType];
                }
                currentType = currentType.BaseType;
            }
            return null;
        }
    }
}

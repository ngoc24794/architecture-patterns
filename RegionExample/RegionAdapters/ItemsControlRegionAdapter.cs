﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  IRegionManager.cs
**
** Description: Định nghĩa Interface cho lớp quản lý vùng
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using System.Windows.Controls;
using Regions.Behaviors;

namespace Regions
{
    public class ItemsControlRegionAdapter : RegionAdapterBase<ItemsControl>
    {
        public ItemsControlRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory) : base(regionBehaviorFactory)
        {
        }

        protected override void Adapt(IRegion region, ItemsControl regionTarget)
        {
            if (regionTarget.Items.Count > 0)
            {
                foreach (object childItem in regionTarget.Items)
                {
                    region.Add(childItem);
                }
                regionTarget.Items.Clear();
            }

            regionTarget.ItemsSource = region.Views;
        }

        protected override IRegion CreateRegion()
        {
            return new Region();
        }
    }
}

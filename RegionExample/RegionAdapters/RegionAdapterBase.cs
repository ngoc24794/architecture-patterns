﻿// ==++==
// 
//   Copyright (c) Huong Viet.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class:  Container.cs
**
** Description: Kho chứa các kiểu dữ liệu, các đối tượng dùng chung
** 
** Author: Nguyen Van Ngoc
** 
** Hitory: 
===========================================================*/

using Regions.Behaviors;
using System;
using System.Windows;

namespace Regions
{
    public abstract class RegionAdapterBase<T> : IRegionAdapter where T : class
    {
        public RegionAdapterBase(IRegionBehaviorFactory regionBehaviorFactory)
        {
            RegionBehaviorFactory = regionBehaviorFactory;
        }

        protected IRegionBehaviorFactory RegionBehaviorFactory { get; set; }

        public IRegion Initialize(T regionTarget, string regionName)
        {
            IRegion region = CreateRegion();
            region.Name = regionName;
            Adapt(region, regionTarget);
            AttachBehaviors(region, regionTarget);
            AttachDefaultBehaviors(region, regionTarget);
            return region;
        }

        protected void AttachDefaultBehaviors(IRegion region, T regionTarget)
        {
            IRegionBehaviorFactory behaviorFactory = this.RegionBehaviorFactory;
            if (behaviorFactory != null)
            {
                DependencyObject dependencyObjectRegionTarget = regionTarget as DependencyObject;

                foreach (string behaviorKey in behaviorFactory)
                {
                    if (!region.Behaviors.ContainsKey(behaviorKey))
                    {
                        IRegionBehavior behavior = behaviorFactory.CreateFromKey(behaviorKey);

                        if (dependencyObjectRegionTarget != null)
                        {
                            if (behavior is IHostAwareRegionBehavior hostAwareRegionBehavior)
                            {
                                hostAwareRegionBehavior.HostControl = dependencyObjectRegionTarget;
                            }
                        }

                        region.Behaviors.Add(behaviorKey, behavior);
                    }
                }
            }
        }

        protected virtual void AttachBehaviors(IRegion region, T regionTarget)
        {
        }

        public IRegion Initialize(object regionTarget, string regionName)
        {
            return Initialize(regionTarget as T, regionName);
        }

        protected abstract void Adapt(IRegion region, T regionTarget);
        protected abstract IRegion CreateRegion();
    }
}

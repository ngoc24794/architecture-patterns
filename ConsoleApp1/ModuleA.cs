﻿
using INV.Elearning.Core.Architecture.IoC;
using INV.Elearning.Core.Architecture.Modularity;
using INV.Elearning.Core.Model;

namespace ConsoleApp1
{
    public class ModuleA : IModule
    {
        IContainer _container;
        public ModuleA(IContainer container)
        {
            _container = container;
        }

        public void Initialize()
        {
        }

        public void RegisterTypes()
        {
            _container.Register<IElearningElement, EElement>();
        }
    }
}

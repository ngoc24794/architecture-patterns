﻿using EventAggregatorPattern;
using INV.Elearning.Core.Model;
using IoCPattern;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Threading;
using Unity;
using Unity.Injection;

namespace ConsoleApp1
{
    class Program
    {
        static IEventAggregator _ea;
        static void Main(string[] args)
        {
            //Type typeA = typeof(A);
            //typeA.GetConstructors()[1].GetParameters();
            //IUnityContainer unityContainer = new UnityContainer();
            //unityContainer.RegisterType<IA, A>();
            //unityContainer.RegisterType<IB, B>();
            //IA a = unityContainer.Resolve<IA>();
            //Console.WriteLine(a.CallB());
            IContainer container = new Container();
            IEventAggregator ea = container.Resolve<EventAggregator>();

            //List<Assembly> allAssemblies = new List<Assembly>();
            //string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            //foreach (string dll in Directory.GetFiles(path, "*.dll"))
            //    allAssemblies.Add(Assembly.LoadFile(dll));
            //Assembly ioCPattern = allAssemblies.FirstOrDefault(x => x.GetName().Name == "IoCPattern");
            //Type[] types = ioCPattern.GetExportedTypes();
            //Type type = types.FirstOrDefault(x => x.Name == "Container");
            //Container container1 = Activator.CreateInstance(type) as Container;

            //Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            AppDomain childDomain = BuildChildDomain(AppDomain.CurrentDomain);

            try
            {
                Type type1 = typeof(InnerModuleInfoLoader);
                InnerModuleInfoLoader loader = (InnerModuleInfoLoader)childDomain.CreateInstance(type1.Assembly.FullName, type1.FullName).Unwrap();
                List<Assembly> allAssemblies = new List<Assembly>();

                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                foreach (string dll in Directory.GetFiles(path, "*.dll"))
                {
                    loader.LoadAssembly(dll);
                    //allAssemblies.Add(Assembly.LoadFile(dll));
                }
                Assembly ioCPattern = loader.GetAssembly("IoCPattern");
                Type[] types = ioCPattern.GetExportedTypes();
                Type type = types.FirstOrDefault(x => x.Name == "Container");
                Container container1 = Activator.CreateInstance(type) as Container;

            }
            finally
            {
                AppDomain.Unload(childDomain);
            }
        }

        private static T Reflect<T>(Assembly assembly) where T : class
        {
            Type[] types = assembly.GetExportedTypes();
            Type type = types?.FirstOrDefault(x => x.Name == "Container");
            if (type != null)
            {
                T container1 = assembly.CreateInstance(type.FullName) as T;
                return container1;
            }
            return default(T);
        }

        private static AppDomain BuildChildDomain(AppDomain parentDomain)
        {
            if (parentDomain == null)
                throw new ArgumentNullException(nameof(parentDomain));

            Evidence evidence = new Evidence(parentDomain.Evidence);
            AppDomainSetup setup = parentDomain.SetupInformation;
            return AppDomain.CreateDomain("DiscoveryRegion", evidence, setup);
        }

    }

    public class InnerModuleInfoLoader : MarshalByRefObject
    {
        private string _assemblyPath;

        public void LoadAssembly(String assemblyPath)
        {
            try
            {
                _assemblyPath = assemblyPath;
                Assembly.ReflectionOnlyLoadFrom(assemblyPath);
            }
            catch (FileNotFoundException)
            {
                // Continue loading assemblies even if an assembly can not be loaded in the new AppDomain.
            }
        }

        public TResult Reflect<TResult>(Func<Assembly, TResult> func)
        {
            DirectoryInfo directory = new FileInfo(_assemblyPath).Directory;
            ResolveEventHandler resolveEventHandler =
                (s, e) =>
                {
                    return OnReflectionOnlyResolve(
                 e, directory);
                };

            AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += resolveEventHandler;

            var assembly = AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies().FirstOrDefault(a => a.Location.CompareTo(_assemblyPath) == 0);

            if (assembly != null)
            {
                var result = func(assembly);
                return result;
            }

            AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve -= resolveEventHandler;

            return default(TResult);
        }

        private Assembly OnReflectionOnlyResolve(ResolveEventArgs args, DirectoryInfo directory)
        {
            Assembly loadedAssembly =
                AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies()
                    .FirstOrDefault(
                      asm => string.Equals(asm.FullName, args.Name,
                          StringComparison.OrdinalIgnoreCase));

            if (loadedAssembly != null)
            {
                return loadedAssembly;
            }

            AssemblyName assemblyName =
                new AssemblyName(args.Name);
            string dependentAssemblyFilename =
                Path.Combine(directory.FullName,
                assemblyName.Name + ".dll");

            if (File.Exists(dependentAssemblyFilename))
            {
                return Assembly.ReflectionOnlyLoadFrom(
                    dependentAssemblyFilename);
            }
            return Assembly.ReflectionOnlyLoad(args.Name);
        }

        public Assembly GetAssembly(string assemblyName)
        {
            var assembly = AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies().FirstOrDefault(a => a.GetName().Name == assemblyName);
            return assembly;
        }
    }

    public class EElement : IElearningElement
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public IElearningElement Clone()
        {
            return new EElement();
        }
    }
    interface IA
    {
        string CallB();
    }

    interface IB
    {
        string MethodB();
    }

    class A : IA
    {
        private readonly IB _b1, _b2;
        public A()
        {
        }
        public A(string name1, string name2)
        {
        }

        public A(string name, int i)
        {

        }

        public A(IB b)
        {
            _b1 = b;
        }
        public A(IB b1, IB b2)
        {
            _b1 = b1;
            _b2 = b2;
            if (ReferenceEquals(_b1, _b2))
            {

            }
            else
            {

            }
        }

        public A(IB b, IC c, IC c2)
        {

        }


        public A(string name)
        {
        }


        public string CallB()
        {
            return _b1.MethodB();
        }
    }

    class B : IB
    {
        public B()
        {
        }

        public string MethodB() => "Method B has called by A";
    }

    interface IC
    {

    }

    class C : IC
    {

    }
}
